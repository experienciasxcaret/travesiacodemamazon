﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="inscripcionGeneral.aspx.cs" Inherits="travesia.inscripcionGeneral" %>
<%@ Register Assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" namespace="DevExpress.Web.ASPxLoadingPanel" tagprefix="dx" %>

<%@ Register src="~/usrControl/usrUpload.ascx" tagname="usrUpload" tagprefix="usrUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width= device-width, initial-scale=1.0" /> 
    <title>Travesia Sagrada Maya</title>
    <link rel="stylesheet" href="styles/bootstrap.css" />
    <link rel="stylesheet" href="styles/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/triatlon.css" />
    <link rel="stylesheet" href="styles/basic.css" />
    <link rel="stylesheet" href="styles/slider-vertical.css" />
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="styles/wow-alert.css" />
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <script src="Scripts/jquery-1.11.3.js" ></script>
    <script src="Scripts/bootstrap.js" ></script>
    <script src="Scripts/inscripcionGeneral.js" ></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.metadata.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/localization/messages_es.js"></script>    
    <script type="text/javascript" src="Scripts/jquery.form/jquery.form.js"></script>
    <script src="Scripts/wow-alert.js" ></script>
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", ready);
        function ready() {
            if (window.location.protocol.toUpperCase() == 'HTTP:')
                window.location = "https:" + "//" + window.location.host + "" + window.location.pathname + window.location.search;
        }
        window.onload = function () {
            //console.log(window.location.protocol.toUpperCase());
           
        }
        function registrarCompetidor() {
            cbRegistrar.PerformCallback();
        }        

        function validarCompetidor() {
            cbValida.PerformCallback();
        }
        $(document).ready(function () {

            Sys.Browser.WebKit = {}; //Safari 3 is considered WebKit

            if (navigator.userAgent.indexOf('WebKit/') > -1) {
                
                Sys.Browser.agent = Sys.Browser.Firefox;

                Sys.Browser.version = parseFloat(navigator.userAgent.match(/WebKit\/(\d+(\.\d+)?)/)[1]);

                Sys.Browser.name = 'Firefox';
            }
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(initializeRequest);
            prm.add_pageLoaded(pageLoaded);
            function initializeRequest(sender, args) {
                //LoadingPanel.ShowInElement(args._postBackElement);  
                document.body.style.cursor = "wait";                
                pnlinfoproceso.Show();

            }
            function pageLoaded(sender, args) {
                var panels = args.get_panelsUpdated();
                if (panels.length > 0) {
                    document.body.style.cursor = "default";
                    pnlinfoproceso.Hide();
                }
            }            
        })
    </script>
    <script src="Scripts/slider-vertical.js" ></script>
    <script type="text/javascript">
        $(document).ready(function () {
            moverSlider(0);            
        });
    </script>
</head>
<body>    
    <div class="container">
         <div class="cont header b-bottom">
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			   <!-- <object data="img/xelha.svg" type="image/svg+xml" class="img1 hidden-xs"></object> -->
		    </div>
		    <div class="logo col-xs-8 col-sm-4" align="center">
			    <object data="img/logo_tsm.svg" type="image/svg+xml" class="img2"></object>
		    </div>
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			    <object data="img/triatlon_fechas.png" type="image/svg+xml" class="img3 hidden-xs"></object>
		    </div>
       </div>
        <div class="grid_16 wrap">
            <div id="paso" class="c1 hidden-xs">
	            <div id="pasos">
		            <ul>
			            <li id="paso1" style="color:#fff;">PASO 1</li>
			            <li id="paso2">PASO 2</li>
			            <li id="paso3">PASO 3</li>
		            </ul>
	            </div> 
            </div>
            <div id="cont_formularios" class="cont">
                <form class="form-horizontal" id="formRifa" runat="server" enctype="multipart/form-data" method="post">
                    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="True"></asp:ScriptManager>
                    <asp:HiddenField ID="cnIdContactoPrecargado" runat="server" value="0"/>  
                    <asp:HiddenField ID="hdStrCat" runat="server" value=""/>
                    <asp:HiddenField ID="hdIdConfigPago" runat="server" value=""/> 
                    <asp:HiddenField ID="hdDsSession" runat="server" value=""/>
                    <asp:HiddenField ID="hdCena" runat="server" value=""/>
                    <asp:HiddenField ID="hdDsClave" runat="server" value=""/>
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">    
                          <!-- Modal content-->
                              <div class="modal-content">
			                        <div class="modal-header">
			                          <button type="button" class="close" data-dismiss="modal">×</button>
			                          <h4 class="modal-title">Aviso</h4>
			                        </div>
			                        <div class="modal-body">					       
			                            <div class="form-group" style="margin:5px">
			                              <p>
                                              No olvides tener a la mano una foto o imagen escaneada de los siguientes archivos:
                                          </p>
                                              <br />
                                                <ol>
                                                    <li><b>Fotografía del competidor (preferencia de hombros para arriba).</b></li>                                              
                                                    <li><b>Identificación oficial.</b></li>                                                        
                                                </ol>
                                            <br />
                                            <p>
                                              <b>Es importante tenerlos para poder terminar el registro.</b>
                                            </p>
                                             <p>
                                              <b>Tamaño máximo de los archivos 2 Megabytes cada uno.</b>
                                            </p>		                              
			                            </div>    
			                            <!--  <button formnovalidate="formnovalidate" type="submit" formnovalidate name="cancel" value="Cancel" class="btn btn-default btn-info btn-lg cancel" id="btnCupon">Aplicar</button> -->
			                        </div>			                       
                              </div>      
                        </div>
                    </div>
                    <div id="form_paso_a">
                                <fieldset id="info_personal">             
                                        <div class="row">
                                            <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                                                <ContentTemplate>   
                                                    <div class="form-group" style="margin:-20px 5px 5px 5px">                                                
                                                        <div class="col-md-12" >
                                                            <label runat="server" id="lblFechaActual" style="float:right"></label>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>                     
                                       <div class="row" id="divImg">
                                             <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                <ContentTemplate> 
                                                    <div class="form-group" style="margin:5px">
                                                        <div class="col-md-1">
                                                        </div>
                                                        <asp:UpdatePanel ID="photoUpload" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" ClientIDMode="AutoID">
                                                            <ContentTemplate>
                                                            <div class="col-md-2 divImageUpload" style="position: relative;">
                                                                <asp:HiddenField ID="hdImg" runat="server" value="0"/>
                                                                <%--<object data="img/xelha.svg" type="image/svg+xml" class="img1 hidden-xs"></object>--%>
                                                                <span style="color:white;font-weight: bold;font-size:small;position:absolute;background: rgb(0, 0, 0); /* fallback color */;background: rgba(0, 0, 0, 0.4);padding-left:3px;padding-right:3px;padding-bottom:3px">CLICK Y SUBE TU FOTO</span>
                                                                <dx:ASPxImage ID="imgUpload" runat="server" ImageUrl="img/add-picture.png" title="Click para subir tu foto." style="cursor: pointer;position:relative;left: 0;top: 0"></dx:ASPxImage>
                                                            </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <div class="col-md-9">                                                                                                                         
                                                             <div class="form-group" style="margin:5px">
                                                               <div class="row">
                                                                    <div class="col-md-6">
                                                                         &nbsp;
                                                                    </div>
                                                               </div>
                                                               <div class="row">
                                                                    <div class="col-md-6">
                                                                         &nbsp;
                                                                    </div>
                                                               </div>
                                                               <div class="row">
                                                                     <div class="col-md-3">                                                                                             
                                                                        <label for="nombre" class="control-label" >Sede de entrenamiento *</label>
                                                                     </div>
                                                                     <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                                                        <ContentTemplate>
	                                                                        <div class="col-md-4 selectContainer">
		                                                                        <asp:DropDownList  class="{required: true} form-control" ID="ddlLocaciones" runat="server" name="locacion" AutoPostback="true" OnSelectedIndexChanged="ddlLocaciones_SelectedIndexChanged" >				                                                                        
		                                                                        </asp:DropDownList>                           
	                                                                        </div>
                                                                        </ContentTemplate>
                                                                     </asp:UpdatePanel>
                                                                     <div class="col-md-5" runat="server" id="divSliderAgotados" visible="false">
                                                                         <div class="containerSlider">
                                                                            <div class="contenido">
                                                                                <div class="nivel slider-vertical">
    	                                                                            <h4>Sedes Agotadas</h4>
                                                                                    <div class="contenedor-slider">
        	                                                                            <div class="bloque-slider" runat="server" id="divBloqueAgotados">
                                                                                            
                                                                                        </div>
                                                                                        <!-- fin bloque-slider -->
        	                                                                            <!--  <p class="mover-slider-vertical"><a class="subir-slider" >Subir noticia</a> <a class="bajar-slider">Bajar noticia</a></p> -->
                                                                                    </div>
                                                                                    <!-- fin contenedor-noticias-slide -->       	                                                                           
                                                                                </div>
                                                                                <!-- fin nivel slide-vertical -->
                                                                            </div>
                                                                            <!-- fin	contenido -->
                                                                        </div>
                                                                        <!-- fin	container -->
                                                                     </div>
                                                               </div>
                                                            </div>
                                                       </div>                                                                                     
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>  
                                        <div class="row" >
                                            <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                                                <ContentTemplate>                                                   
                                                        <div class="col-md-5">
                                                            <dx:ASPxLabel ID="lblValidacionImagen" runat="server" Text="" Font-Size="Medium" ForeColor="#CC0000"></dx:ASPxLabel>
                                                        </div>                                                    
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>                                      
                                    <asp:UpdatePanel ID="UpdatePanel12" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="row">
                                                <div class="form-group" style="margin:5px">
                                                    <div class="col-md-4">
                                                        <input type="hidden" name="MAX_FILE_SIZE" value="1000" />                                               
                                                        <input type="file" size="45" runat="server" id="FileUpload1" name="uploadPicture" style="width:200px; display:none;" enableviewstate="True" />                                                                                                                
                                                     </div>                                                    
                                                </div>
                                            </div> 
                                         </ContentTemplate>
                                     </asp:UpdatePanel>                            
                                    <br />
                                    <div class="row">
                                        <div class="form-group" style="margin:5px">
                                            <div class="col-md-2">                                                                                             
                                                <label for="nombre" class="control-label" >Nombre *</label>
                                            </div>
                                            <div class="col-md-2">	                                            
                                                <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtGralNombre" placeholder="ingresa tu nombre" name="nombre1" EnableTheming="False"></asp:TextBox>
                                            </div>    
                              
                                            <div class="col-md-2">	
                                                <label for="apellidoPaterno" class="control-label" >Apellido Paterno *</label>
                                            </div>
                                            <div class="col-md-2">	
                                                <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtGralApellidoPaterno" placeholder="ingresa tu apellido paterno" name="apellido1" EnableTheming="False"></asp:TextBox>                            
                                            </div>
                     
                                            <div class="col-md-2">	
                                                <label for="apellidoMaterno" class="control-label" >Apellido Materno</label>
                                            </div>
                                            <div class="col-md-2">	
                                                <asp:TextBox runat="server" class="form-control"  id="txtGralApellidoMaterno" placeholder="ingresa tu apellido materno" name="apellido2" EnableTheming="False"></asp:TextBox>                                    
                                            </div>
                                        </div>
                                    </div>                                    
                                    <div class="row">    
                                       <div class="form-group" style="margin:5px">
                                            <div class="col-md-2">                                            
                                                <label for="email" class="control-label">Email *</label>                                                   
                                            </div>
                                            <div class="col-md-3">                                                                                           
                                                <asp:TextBox runat="server" type="email" class="{required:true} form-control" id="txtGralEmail" placeholder="ingresa tu correo electrónico" name="email" EnableTheming="False" ></asp:TextBox>
                                            </div>
                                        </div>      
                                    </div>
                                    <div class="row">    
                                        <div class="form-group" style="margin:5px">
                                            <div class="col-md-2">                                            
                                                <label for="email" class="control-label">Confirma Email *</label>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:TextBox runat="server" type="email" class="{required:true} form-control" id="txtGralEmail2" onpaste="return false" oncut="return false" placeholder="confirma tu correo electrónico" name="email" OnTextChanged="txtGralEmail2_TextChanged" EnableTheming="False" ></asp:TextBox>
                                                <dx:ASPxLabel ID="lblValidacionEmail" runat="server" Text="" Font-Size="Small" ForeColor="#CC0000"></dx:ASPxLabel>
                                            </div>  
                                        </div>      
                                    </div>                                    
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="form-group" style="margin:5px">
                                                <div class="col-md-2">                                                
                                                    <label for="sexo" class="control-label">Sexo *</label>
                                                </div>
                                                <div class="col-md-3 selectContainer">
                                                    <asp:DropDownList  class="{required: true} form-control" ID="ddlGralSexo" runat="server" name="sexo">
                                                            <asp:ListItem Value="">Selecciona sexo</asp:ListItem>
                                                            <asp:ListItem Value="2">Masculino</asp:ListItem>
                                                            <asp:ListItem Value="3">Femenino</asp:ListItem>
                                                    </asp:DropDownList>                           
                                                </div>
                                                <div class="col-md-1">                                                
                                                    <label for="peso" class="control-label">Peso (kg)*</label>
                                                </div>
                                                <div class="col-md-2">	
                                                    <asp:TextBox runat="server" class="{required: true, number: true} form-control"  id="txtPeso" placeholder="peso (kg)" name="peso" EnableTheming="False"></asp:TextBox>                                    
                                                </div>
                                            </div>                                            
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="form-group" style="margin:5px">
                                                <div class="col-md-2">
                                                        <asp:Label  for="playera" ID="lblTamanioPlayera" runat="server" class="control-label" Text="Tamaño playera" Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="col-md-3 selectContainer">
                                                        <asp:DropDownList  class="{required: false} form-control" ID="ddlGralTallasPlayeras" runat="server" AutoPostBack="true" name="ddlGralTallasPlayeras" OnSelectedIndexChanged="ddlGralTallasPlayeras_SelectedIndexChanged">
                                                                <asp:ListItem Value="">Seleccione Tamaño de playera</asp:ListItem>                                                           
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rvTallaPlayera" runat="server" ControlToValidate="ddlGralTallasPlayeras"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>                                                        
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:Label ID="lblSujetoDisponibilidad" runat="server" class="control-label" Text="*sujeta a disponibilidad" Font-Bold="True"></asp:Label>
                                                </div>                           
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>                                   
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="col-md-3">                                           
                                                    <label for="nacimiento" class="control-label">Fecha de nacimiento : *</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                           <div class="form-group" style="margin:5px">
                                               <div class="col-md-1"> 
                                                </div>
                                                <div class="col-md-1">                                                
                                                    <label for="anio" class="control-label">año</label>
                                                </div>
                                                <div class="col-md-2 selectContainer">
                                                        <asp:DropDownList  class="{required: true} form-control" ID="ddlGralAnio" runat="server" AutoPostBack="True" name="anio" OnSelectedIndexChanged="ddlGralAnio_SelectedIndexChanged">                                                    
                                                        </asp:DropDownList>                                                                                                                              
                                                </div>
                                                <div class="col-md-1">
                                                    <label for="mes" class="control-label">mes</label>
                                                </div>
                                               <div class="col-md-2 selectContainer">
                                                        <asp:DropDownList  class="{required: true} form-control" ID="ddlGralMes" runat="server" AutoPostBack="True" name="mes" OnSelectedIndexChanged="ddlGralMes_SelectedIndexChanged">                                                        
                                                                <asp:ListItem Value="1" Selected="True">Enero</asp:ListItem>
                                                                <asp:ListItem Value="2">Febrero</asp:ListItem>
                                                                <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                                <asp:ListItem Value="4">Abril</asp:ListItem>
                                                                <asp:ListItem Value="5">Mayo</asp:ListItem>
                                                                <asp:ListItem Value="6">Junio</asp:ListItem>
                                                                <asp:ListItem Value="7">Julio</asp:ListItem>
                                                                <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                                <asp:ListItem Value="9">Septiembre</asp:ListItem>
                                                                <asp:ListItem Value="10">Octubre</asp:ListItem>
                                                                <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                                                <asp:ListItem Value="12">Diciembre</asp:ListItem>
                                                        </asp:DropDownList>
                                                </div>
                                                <div class="col-md-1">
                                                     <label for="dia" class="control-label">día</label>
                                                </div>
                                               <div class="col-md-2 selectContainer">
                                                    <asp:DropDownList  class="{required: true} form-control" ID="ddlGralDia" runat="server" AutoPostBack="True" name="dia">
                                                                <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                                <asp:ListItem Value="8">8</asp:ListItem>
                                                                <asp:ListItem Value="9">9</asp:ListItem>
                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                                <asp:ListItem Value="26">26</asp:ListItem>
                                                                <asp:ListItem Value="27">27</asp:ListItem>
                                                                <asp:ListItem Value="28">28</asp:ListItem>
                                                                <asp:ListItem Value="29">29</asp:ListItem>
                                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                                <asp:ListItem Value="31">31</asp:ListItem>                                                            
                                                    </asp:DropDownList>
                                                </div>
                                            </div>                  
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdatePanel ID="upFormulario" runat="server">
                                        <ContentTemplate>
                                                <div class="row">
                                                <div class="form-group" style="margin:5px">                                                                                                            
                                                    <div class="col-md-2">                                                        
                                                        <label for="pais" class="control-label">País de origen*</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:DropDownList  class="{required: true} form-control" ID="ddlGralPais" runat="server" name="pais" OnTextChanged="ddlGralPais_TextChanged" AutoPostBack="True">
                                                        </asp:DropDownList>  
                                                    </div>                                                         
                                                    <div class="col-md-2">                                                        
                                                        <label for="estado" class="control-label">Estado de procedencia</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:DropDownList  class="form-control" ID="ddlGralEstado" runat="server" name="estado">
                                                                <asp:ListItem Value="1">Seleccione Estado</asp:ListItem>                                                                
                                                        </asp:DropDownList>                                 
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </ContentTemplate>
                                     </asp:UpdatePanel>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="col-md-3">                                           
                                                    <label for="nacimiento" class="control-label">Domicilio actual : *</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row">
                                                 <div class="form-group" style="margin:5px">
                                                    <div class="col-md-2">                                                                                                            
                                                        <label for="colonia" class="control-label" >Colonia</label> 
                                                    </div>
                                                    <div class="col-md-3">			                                            
                                                            <asp:TextBox runat="server" class="form-control"   id="txtGralColonia" placeholder="Colonia" name="colonia">
                                                            </asp:TextBox>
                                                    </div>                                                    
                                                    <div class="col-md-2">                                                        
                                                            <label for="calle" class="control-label" >Calle</label>
                                                    </div>
                                                     <div class="col-md-3">
                                                            <asp:TextBox runat="server" class="form-control"   id="txtGralCalle" placeholder="calle" name="calle">
                                                            </asp:TextBox>                                                        
                                                     </div>
                                                </div>
                                            </div>                                            
                                    <div class="row">
                                            <div class="form-group" style="margin:5px">                                                                                                        
                                                <div class="col-md-2">                                                       
                                                    <label for="numeroExterior" class="control-label" >Número Exterior</label>
                                                </div>
                                                <div class="col-md-3">    		                                            
                                                        <asp:TextBox runat="server" class="form-control"   id="txtGralNoExterior" placeholder="Número Exterior" name="numeroExterior">
                                                        </asp:TextBox>
                                                </div>    
                                                     
                                                <div class="col-md-2">                                                        
                                                    <label for="numeroInterior" class="control-label" >Número Interior</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:TextBox runat="server" class="form-control"   id="txtGralNoInterior" placeholder="Número Interior" name="numeroInterior">
                                                    </asp:TextBox>                               
                                                </div>                                                    
                                            </div>
                                    </div>                                            
                                                                                     
                                    <div class="row">
                                        <div class="form-group" style="margin:5px">
                                            <div class="col-md-2">                                                                                                    
                                                <label for="ciudad" class="control-label" >Ciudad *</label> 
                                            </div>
                                            <div class="col-md-3">  			                                            
                                                <asp:TextBox runat="server" class="{required:true} form-control"   id="txtGralCiudad" placeholder="ciudad" name="ciudad">
                                                </asp:TextBox>
                                            </div>                                                                                             
                                        </div>
                                    </div>                                        
                                    <div class="row">
                                        <div class="form-group" style="margin:5px">                                                                                                         
                                            <div class="col-md-2">                                                                                          
                                                <label for="telefono" class="control-label">Teléfono Fijo</label>   
                                            </div>      
                                            <div class="col-md-1">
                                                    <asp:TextBox runat="server" width="60px" class="{number:true} form-control" id="txtGralTelefonofijo1" placeholder="lada" name="lada1">
                                                    </asp:TextBox>
                                            </div>
                                            <div class="col-md-2">
                                                    <asp:TextBox runat="server" class="{number:true} form-control"   id="txtGralTelefonofijo2" placeholder="Numero" name="numero1">
                                                    </asp:TextBox>                                                          
                                            </div>                            
                                        </div>
                                    </div>                                            
                                    <div class="row">
                                        <div class="form-group" style="margin:5px">                                                                                                            
                                            <div class="col-md-2">                                    
                                                    <label for="telefonoCelular" class="control-label">Teléfono Celular *</label>   
                                            </div>
                                            <div class="col-md-1">
                                                    <asp:TextBox runat="server" width="60px" class="{required:true, number:true, rangelength: [2,4]} form-control" id="txtGralCelular1" placeholder="lada" name="lada2">
                                                    </asp:TextBox>
                                            </div>
                                            <div class="col-md-2">
                                                    <asp:TextBox runat="server" class="{required:true, number:true, rangelength: [2,8]} form-control"   id="txtGralCelular2" placeholder="Numero" name="numero2">
                                                    </asp:TextBox>
                                            </div>                                                                                 
                                        </div>
                                    </div>                                  
                                </fieldset>
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                        <fieldset id="botonera_personales">
                                            <div class="col-md-1">
                                                    <asp:button  ID="btnContinuarA" Text="Continuar" class="btn btn-info btn-mini" UseSubmitBehavior="true" AutoPostBack="False" ClientInstanceName="btnContinuarA"/>Continuar
                                            </div>
                                            <div class="col-md-6">
                                                <dx:ASPxLabel ID="lblMensajeValidacion" runat="server" Text="" ClientInstanceName="lblMensajeValidacion" Font-Bold="True" Font-Size="Small" ForeColor="#CC0000"></dx:ASPxLabel>
                                            </div>
			                            </fieldset>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                    </div>
                    
                    <div id="form_paso_b" class="hide">    
                        <fieldset id="InformacionDireccion">
                            <br/>
                             <div class="row">
                                <div class="form-group" style="margin:5px">
                                    <div class="col-md-2">
                                        <label for="participaciones">¿En que travesía has participado?</label>  
                                    </div>
                                    <div class="col-md-3" >                                       
                                        <dx:ASPxDropDownEdit class="form-control" ID="ddlGralNoParticipaciones3" runat="server" ClientInstanceName="checkComboBox" EnableTheming="False" ShowShadow="False" EnableDefaultAppearance="False" AnimationType="None" Text="seleccione los años">
                                            <DropDownWindowStyle BackColor="#EDEDED" />
                                            <DropDownWindowTemplate>
                                                <table style="width:100%;height:300px;">
                                                    <tr style="height:100%;">
                                                        <td style="height:100%;">
                                                            <dx:ASPxListBox ID="lbxAniosParticipaciones" runat="server" class="" ClientInstanceName="checkListBox" SelectionMode="CheckColumn" Width="100%" Height="100%" EnableTheming="false" ShowShadow="false" EnableDefaultAppearance="true">
                                                                <Border BorderStyle="None" />
                                                                <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                                                            <Items>      
                                                                <dx:ListEditItem Text="(Select all)" />                                                  
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged"/>
                                                            </dx:ASPxListBox>    
                                                            <table style="width: 100%;">
                                                                <tr>
                                                                    <td style="padding: 2px;height:100%;">
                                                                        <dx:ASPxButton ID="ASPxButton1" AutoPostBack="False" runat="server" Text="Close" style="float: right">
                                                                            <ClientSideEvents Click="function(s, e){ checkComboBox.HideDropDown(); }" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table> 
                                                        </td>
                                                    </tr> 
                                                </table>                                            
                                            </DropDownWindowTemplate>
                                        </dx:ASPxDropDownEdit>                                               
                                    </div>
                                </div>                                      
                            </div>

                            <div class="row" id="dvRazonParticipacion">
                                <div class="form-group" style="margin:5px">
                                    <div class="col-md-2">                                                
                                        <label for="txtRazonParticipacion">¿Por qué quieres participar en la TSM?</label>
                                    </div>                                     
                                    <div class="col-md-4">		                                            
                                        <asp:TextBox runat="server" class="{required: true} form-control" id="txtRazonParticipacion" name="txtRazonParticipacion" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRazonParticipacion"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                    </div>                                                                                                                                                     
                                </div>
                            </div>

                            <div class="row" id="dvSignificadoTSM">
                                <div class="form-group" style="margin:5px">
                                    <div class="col-md-2">                                                
                                        <label for="txtSignificadoTSM">¿Sabes de que se trata la TSM?</label>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                        <ContentTemplate>
                                            <div class="col-md-1 columns">                                                    
	                                            <asp:RadioButtonList ID="rblSignificadoTsm" runat="server" class="rblSiNo" RepeatDirection="Horizontal" AutoPostBack="true" RepeatLayout="Table" OnSelectedIndexChanged="rblSignificadoTsm_SelectedIndexChanged">                                                
                                                    <asp:ListItem
	                                                    Enabled="True"                                                                                                                                                           
	                                                    Text='Si'
	                                                    Value="1"
	                                                    class="fcheck"
	                                                />                                                                                     
                                                    <asp:ListItem
	                                                    Enabled="True"                            
	                                                    Text='No'
	                                                    Value="0"
	                                                    style="margin-left:10px"
	                                                    class="fcheck"
                                                        Selected="True"
                                                    />
                                                </asp:RadioButtonList>	                                                                                         
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                                        <ContentTemplate>
                                            <div class="col-md-4" runat="server" id="divSignificadoTsm" visible="false">                                         		                                            
                                                <asp:TextBox runat="server" class="form-control" id="txtSignificadoTSM" name="txtSignificadoTSM"  placeholder="¿De que trata?" TextMode="MultiLine"></asp:TextBox>                                            
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>                                                                                                                                                     
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group" style="margin:5px">
                                    <div class="col-md-2">
                                        <label for="participaciones">¿Sabes nadar en aguas abiertas?</label>  
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                        <ContentTemplate>
                                            <div class="col-md-4 columns">                                                    
	                                            <asp:RadioButtonList ID="rblSiNo" runat="server" class="rblSiNo" name="nameRblSiNo" RepeatDirection="Horizontal" AutoPostBack="true" RepeatLayout="Table" OnSelectedIndexChanged="rblSiNo_SelectedIndexChanged">                                                
                                                    <asp:ListItem
	                                                    Enabled="True"                                                                                                                                                           
	                                                    Text='Si'
	                                                    Value="1"
	                                                    class="fcheck"
	                                                />                                                                                     
                                                    <asp:ListItem
	                                                    Enabled="True"                            
	                                                    Text='No'
	                                                    Value="0"
	                                                    style="margin-left:10px"
	                                                    class="fcheck"
                                                        Selected="True"
                                                    />
                                                </asp:RadioButtonList>	                                                                                         
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>                                      
                            </div>
                            <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                            <ContentTemplate>
                                <div runat="server" class="row" id="divDeportes" visible="false">
                                    <div class="form-group" style="margin:5px">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-1">
                                            <label for="participaciones">¿Cual/es?</label>  
                                        </div>
                                        <div class="col-md-4">		                                            
                                            <asp:TextBox runat="server" class="form-control" id="txtDeportes" name="txtDeportes" TextMode="MultiLine"></asp:TextBox>
                                        </div> 
                                    </div>                                      
                                </div>
                             </ContentTemplate>
                             </asp:UpdatePanel>
                            <div class="row">
                                <div class="" style="margin:3px">
                                    <div class="col-md-1"></div>                                    
                                     <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                        <ContentTemplate>
                                            <div runat="server" id="divFrecuencia" visible="false">
                                                <div class="col-md-1" runat="server" id="divFrecuencia2">
                                                    <label for="participaciones">Frecuencia:</label>  
                                                </div>
                                                <div class="col-md-9" runat="server" id="divFrecuencia3">		                                            
                                                    <asp:RadioButtonList ID="rblFrecuencia" runat="server" class="{required: true} rblFrecuencia" EnableTheming="False" RepeatDirection="Horizontal" AutoPostBack="false" RepeatLayout="Flow">                                                
                                                        <asp:ListItem
	                                                        Enabled="True"                                                                                                                                                           
	                                                        Text='Diario'
	                                                        Value="Diario"	                                                        
                                                            Selected="True"
	                                                    />                                                                                     
                                                        <asp:ListItem
	                                                        Enabled="True"                            
	                                                        Text='1 vez por semana'
	                                                        Value="1 vez por semana"
	                                                        style="margin-left:10px"
	                                                       
                                                        />
                                                        <asp:ListItem
	                                                        Enabled="True"                            
	                                                        Text='2 veces por semana'
	                                                        Value="2 veces por semana"
	                                                        style="margin-left:10px"
	                                                        
                                                        />
                                                        <asp:ListItem
	                                                        Enabled="True"                            
	                                                        Text='3 veces por semana'
	                                                        Value="3 veces por semana"
	                                                        style="margin-left:10px"
	                                                       
                                                        />
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel> 
                                </div>                                      
                            </div>
                            <br/>
                            <asp:UpdatePanel ID="UpdatePanel30" runat="server">
                                <ContentTemplate>
                                    <div runat="server" class="row" id="dvNadar" visible="false">
                                        <div class="form-group" style="margin:5px" >
                                             <div class="col-md-2"> 
                                                <asp:Label  for="rblSabeNadar" ID="lbnNado" runat="server" Text="NIVEL DE NADO EN AGUAS ABIERTAS" visible="true" Font-Bold="True"></asp:Label>
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel26" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-md-4 columns">                                                    
	                                                    <asp:RadioButtonList ID="rblSabeNadar" runat="server" class="rblSabeNadar" RepeatDirection="Horizontal" AutoPostBack="false" RepeatLayout="Table">                                                                                                                                       
                                                            <asp:ListItem
	                                                            Enabled="True"                            
	                                                            Text='Intermedio'
	                                                            Value="2"
	                                                            style="margin-left:10px"
	                                                            class="fcheck"
                                                                Selected="True"                                                
                                                            />
                                                            <asp:ListItem
	                                                            Enabled="True"                            
	                                                            Text='Avanzado'
	                                                            Value="3"
	                                                            style="margin-left:10px"
	                                                            class="fcheck"                                                        
                                                            />
                                                        </asp:RadioButtonList>	                                                                                         
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <div class="col-md-4" style="background-color:#d3d3d3;">
                                                <label><b>Nota: </b> Al inicio de los entrenamientos se realizará una prueba de nado en mar abierto.</label>
                                            </div>
                                        </div>
                                    </div> 
                                </ContentTemplate>
                            </asp:UpdatePanel> 
                            <div class="row" id="dvTipoSangre">
                                <div class="form-group" style="margin:5px">                                                                                                                                                                               
                                    <div class="col-md-2"> 
                                        <asp:Label  for="tipoSangre" ID="lblTipoSangre" runat="server" Text="Tipo de sangre" visible="true" Font-Bold="True"></asp:Label>
                                    </div>
                                    <div class="col-md-3 selectContainer">
                                        <asp:DropDownList  class="{required: true} form-control" ID="ddlTipoSangre" runat="server" AutoPostBack="false" name="ddlTipoSangre" Visible="true">                                                                                                                  
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rvSangre" runat="server" ControlToValidate="ddlTipoSangre"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                    </div>                                                                          
                                </div>
                            </div>
                            <div class="row" id="dvVegetariano">
                                <div class="form-group" style="margin:5px">
                                     <div class="col-md-2"> 
                                        <asp:Label  for="rblVegetariano" ID="lblVegetariano" runat="server" Text="¿Eres Vegetariano?" visible="true" Font-Bold="True"></asp:Label>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                                        <ContentTemplate>
                                            <div class="col-md-4 columns">                                                    
	                                            <asp:RadioButtonList ID="rblVegetariano" runat="server" class="rblVegetariano" RepeatDirection="Horizontal" AutoPostBack="false" RepeatLayout="Table">                                                
                                                    <asp:ListItem
	                                                    Enabled="True"                                                                                                                                                           
	                                                    Text='Si'
	                                                    Value="1"
	                                                    class="fcheck"
	                                                />                                                                                     
                                                    <asp:ListItem
	                                                    Enabled="True"                            
	                                                    Text='No'
	                                                    Value="0"
	                                                    style="margin-left:10px"
	                                                    class="fcheck"
                                                        Selected="True"
                                                    />
                                                </asp:RadioButtonList>	                                                                                         
                                            </div>                                           
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="row" id="dvPadecimientos">
                                <div class="form-group" style="margin:5px">
                                    <div class="col-md-2">                                                
                                        <label for="padecimientos">Padecimientos, enfermedad crónica y/o alergias</label>
                                    </div>
                                    <div class="col-md-4">		                                            
                                        <asp:TextBox runat="server" class="form-control" id="txtPadecimiento" name="txtPadecimiento" TextMode="MultiLine"></asp:TextBox>
                                    </div>                                                                                                                                                     
                                </div>
                            </div>
                        </fieldset>  
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate> 
                                <fieldset id="botonera_domicilio">
                                    <div class="col-md-4">
				                        <a id="back_paso_a" class="btn btn-info btn-mini">Regresar</a>                                                                                                          
                                        <asp:Button ID="btnContinuarB" runat="server" Text="Continuar" class="btn btn-info btn-mini"  UseSubmitBehavior="true" AutoPostBack="False" name="NameBtncontinuarB"/>
                                    </div>
			                    </fieldset> 
                            </ContentTemplate>
                        </asp:UpdatePanel>                                     
                    </div>
                                <br />
                                <div id="form_paso_c" class="hide">
                                    <fieldset id="categoria"> 
                                        <asp:UpdatePanel ID="UpdatePanel18" runat="server">
	                                        <ContentTemplate>
		                                        <div class="row hide" >
			                                        <div class="form-group" style="margin:5px">
				                                        <div class="col-md-2">
						                                        <label for="categoria" class="control-label">Categoria</label>
				                                        </div>
				                                        <div class="col-md-3 selectContainer"> 
						                                    <asp:DropDownList  class="{required: true} form-control" ID="ddlGralCategoria" runat="server" AutoPostBack="false" name="categoria">
								                                    <asp:ListItem Value="">Seleccione tu categoria</asp:ListItem>                                                           
						                                    </asp:DropDownList>
						                                    <asp:RequiredFieldValidator ID="rvcategoría" runat="server" ControlToValidate="ddlGralCategoria"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
				                                        </div>                            
			                                        </div>
		                                         </div>
	                                        </ContentTemplate>
                                        </asp:UpdatePanel>                                       
                                       <div class="row">
                                           <div class="form-group" style="margin:5px">                                               
                                               <div class="col-md-3">                                                                                             
                                                    <label for="nombre" class="control-label" >¿Eres colaborador experiencias?</label>
                                                </div>
                                               <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-4 columns">                                                    
	                                                        <asp:RadioButtonList ID="rblColaborador" runat="server" class="{required: true} form-control rblSiNo" RepeatDirection="Horizontal" AutoPostBack="true" RepeatLayout="Table" OnSelectedIndexChanged="rblColaborador_SelectedIndexChanged" >                                                
                                                                <asp:ListItem
	                                                                Enabled="True"                                                                                                                                                           
	                                                                Text='Si'
	                                                                Value="1"
	                                                                class="fcheck"
	                                                            />                                                                                     
                                                                <asp:ListItem
                                                                    Selected="True"
	                                                                Enabled="True"                            
	                                                                Text='No'
	                                                                Value="0"
	                                                                style="margin-left:10px"
	                                                                class="fcheck"                                                                    
                                                                />
                                                            </asp:RadioButtonList>	                                                                                         
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                           </div>
                                       </div>
                                       <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                            <ContentTemplate>                                    
                                               <div class="row" runat="server" id="divInfoColaborador" visible="false">
                                                   <div class="form-group" style="margin:5px" runat="server">
                                                       <div class="col-md-2"></div>
                                                       <div class="col-md-2">                                                                                             
                                                            <label for="nombre" class="control-label" >No. colaborador:*</label>
                                                       </div>
                                                       <div class="col-md-2">
                                                           <asp:TextBox runat="server" class="{required: true, number: true, minlength: 1, maxlength: 8} form-control" MaxLength="8" visible="true" id="txtNoColaborador" placeholder="no. colaborador" name="noFmtri" ValidationGroup="validaGuardar"></asp:TextBox>                                                
                                                           <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNoColaborador"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                       </div>
                                                       <div class="col-md-2">                                                                                             
                                                            <label for="nombre" class="control-label" >Unidad de negocio:*</label>
                                                       </div>
                                                       <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                                            <ContentTemplate>
	                                                            <div class="col-md-3 selectContainer">
		                                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlUnidadNegocioColaborador" runat="server" name="unidadNegocio" AutoPostback="true" >
				
		                                                            </asp:DropDownList>                           
	                                                            </div>
                                                            </ContentTemplate>
                                                       </asp:UpdatePanel> 
                                                   </div>
                                               </div>
                                            </ContentTemplate>
                                       </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <div class="row">
                                                <div class="form-group" style="margin:5px">
                                                    
                                                    <div class="col-md-3">
                                                           <b><label for="categoria" class="control-label">Contacto de emergencias :</label></b>
                                                    </div>                                                                             
                                                </div>
                                             </div>
                                        </ContentTemplate>
                                        </asp:UpdatePanel>                                         
                                        <div class="row" id="contacto">
                                            <div class="form-group" style="margin:5px">
                                                <div class="col-md-2">                                                                                             
                                                    <label for="nombre" class="control-label" >Nombre *</label>
                                                </div>
                                                <div class="col-md-2">	                                            
                                                    <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtNombreContacto" placeholder="nombre" name="nombre1" EnableTheming="False"></asp:TextBox>
                                                </div>    
                              
                                                <div class="col-md-2">	
                                                    <label for="apellidoPaterno" class="control-label" >Apellido Paterno *</label>
                                                </div>
                                                <div class="col-md-2">	
                                                    <asp:TextBox runat="server" class="{required:false, rangelength: [2,50]} form-control"   id="txtPaternoContacto" placeholder="primer apellido" name="apellido1" EnableTheming="False"></asp:TextBox>                            
                                                </div>
                     
                                                <div class="col-md-2">	
                                                    <label for="apellidoMaterno" class="control-label" >Apellido Materno</label>
                                                </div>
                                                <div class="col-md-2">	
                                                    <asp:TextBox runat="server" class="form-control"  id="txtMaternoContacto" placeholder="segundo apellido" name="apellido2" EnableTheming="False"></asp:TextBox>                                    
                                                </div>                                                                                                                                            
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group" style="margin:5px">
                                                <div class="col-md-2">                                                                                             
                                                    <label for="nombre" class="control-label" >Parentesco *</label>
                                                </div>
                                                <asp:UpdatePanel ID="UpdatePanel19" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
	                                                    <div class="col-md-2 selectContainer">
		                                                    <asp:DropDownList  class="{required: true} form-control" ID="ddlParentesco" runat="server" name="parentesco" AutoPostback="true" >
				
		                                                    </asp:DropDownList>
                                                            <label class="control-label" runat="server" id="valDdlParentesco" style="color: #B40404;"></label>                           
	                                                    </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <div class="col-md-2">                                                                                             
                                                    <label for="nombre" class="control-label" >Teléfono *</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <asp:TextBox runat="server" class="{required:false, number:true, rangelength: [6,13]} form-control"   id="txtTelefonoParentesco" placeholder="teléfono" name="numero2">
                                                    </asp:TextBox>
                                                    <label class="control-label" runat="server" id="valTelContacto" style="color: #B40404;"></label> 
                                                </div>
                                                <div class="col-md-4" style="background-color:#d3d3d3;">
                                                    <label><b>Nota: </b>Es importante que tu contacto de emergencia no participe en la TSM y que si su teléfono es celular pongas el 044 ó 045 según corresponda.</label>
                                                </div>
                                            </div>
                                       </div>
                                       <div class="row">
                                            <div class="form-group" style="margin:5px">
                                            </div>
                                       </div>
                                       <div class="row">
                                            <div class="" style="margin-left:8px">                                                
                                                <div class="col-md-7" style="color:#2a78bf;">
                                                    <asp:Label ID="lblmsgArchivos" runat="server" ForeColor="#2a78bf" text="Es importante que subas los siguientes documentos, ya sea en formato pdf, jpg o png, y que no exceda de tamaño por archivo de 2 MB." Font-Bold="True" />
                                                    <br />
                                                    <ol>
                                                        <li><b>Identificación oficial (INE/IFE, pasaporte, visa de residencia, licencia de conducir)</b></li>                                                                                                           
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                      <asp:UpdatePanel ID="UpdatePanel15" runat="server" UpdateMode="Conditional">
                                           <ContentTemplate>
                                               <div class="row">
                                                    <div class="" style="margin-left:5px">
                                                        <div class="col-md-2">
                                                            <asp:Literal ID="uploadMsg" runat="server"></asp:Literal>                                                            
                                                            <div class="fileUpload btn btn-primary">
                                                                <span>Seleccionar Archivo</span>
                                                                <asp:UpdatePanel ID="UpdatePanel29" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                    <input id="FileUpload2" type="file" class="upload classFilesUpload"  name="uploadFiles"/>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-5" style="align-items: center; margin-top: 11px;">
                                                            <asp:Label ID="lblMsgFile1" runat="server" ForeColor="#0033CC" Font-Bold="True" />
                                                        </div>                                                        
                                                    </div>
                                               </div>
                                            </ContentTemplate>
                                       </asp:UpdatePanel>
                                       <!-- <asp:UpdatePanel ID="UpdatePanel14" runat="server" UpdateMode="Conditional">
                                           <ContentTemplate>
                                               <div class="row">
                                                    <div class="" style="margin-left:5px">
                                                        <div class="col-md-2">
                                                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                                            <div class="fileUpload btn btn-primary">
                                                                <span>Seleccionar Archivo</span>
                                                                <asp:UpdatePanel ID="UpdatePanel28" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                    <input type="file" size="45" id="FileUpload3" name="uploadFiles" class="upload classFilesUpload"/>                                                                
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5" style="align-items: center; margin-top: 11px;">
                                                            <asp:Label ID="lblMsgFile2" runat="server" ForeColor="#0033CC" Font-Bold="True"/>
                                                        </div>
                                                    </div>
                                               </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>  -->                                        
                                         <asp:UpdatePanel ID="UpdatePanel16" runat="server" UpdateMode="Conditional">
                                           <Triggers>
                                                <asp:PostBackTrigger ControlID="btnSubmit" />
                                           </Triggers>
                                           <ContentTemplate>
                                               <div class="row">
                                                    <div class="form-group" style="margin:5px">
                                                        <div class="col-md-4">
                                                           <asp:Button ID="btnSubmit" runat="server" Text="Subir archivos" class="btn btn-info btn-mini" ClientInstanceName="btnSubmit" style="display:none" UseSubmitBehavior="false"  AutoPostBack="false" OnClick="btnSubmit_Click1"/>                                                                                              
                                                        </div>                                                         
                                                    </div>
                                               </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                                                                                                                     
                                        <!-- termina la parte de competidores normal -->                                                                                                                       
                                    </fieldset>
                                    <br />
                                     <asp:UpdatePanel ID="UpdatePanel40" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="row">
                                                <div class="form-group" style="margin:5px">
                                                    <div class="col-md-6">
                                                        <asp:CheckBox ID="chkMayorEdad" runat="server" Text="Certifico que soy mayor de 18 años *" class="chkMayorEdad" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group" style="margin:5px">
                                                    <div class="col-md-6">
                                                        <asp:CheckBox ID="chkHeLeido" runat="server" Text="He leído y acepto los terminos y condiciones de uso y aviso de privacidad" class="chkHeLeido" />
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                     <Triggers>
                                                <asp:PostBackTrigger ControlID="btnTerminar" />
                                           </Triggers>
                                    <ContentTemplate>
                                        <fieldset id="botonera_categoria">
                                            <div class="col-md-3">
				                                <a id="back_paso_b" href="" class="btn btn-info btn-mini">Regresar</a>				                                                                        
                                                <asp:button ID="btnTerminar" runat="server" Text="Terminar" class="btn btn-info btn-mini" name="NameBtnContinuarC" UseSubmitBehavior="false" AutoPostBack="False" ClientInstanceName="NameBtnContinuarC" OnClientClick="this.disabled=true" OnClick="btnTerminar_Click" />
                                            </div>
                                            <div class="col-md-6">
                                                <dx:ASPxLabel ID="lblMensaje" runat="server" Text="" ClientInstanceName="lblMensaje" Font-Bold="True" Font-Size="Large"></dx:ASPxLabel>
                                            </div>
			                            </fieldset>
                                        <br />
                                        <asp:HiddenField ID="hdValidationSizeFiles1" runat="server" value="0"/>
                                        <asp:HiddenField ID="hdValidationSizeFiles2" runat="server" value="0"/>
                                        <asp:HiddenField ID="hdValidationSizeFiles3" runat="server" value="0"/>                                        
                                    </ContentTemplate>
                                    </asp:UpdatePanel>                                                                   
                                </div>                      
                </form>
            </div>        
        </div>

         <div class="grid_16 wrap">
            <div id="footer" style="text-align: center; padding-bottom: 10px" >                
                 <!-- <a href="javascript:window.open('TerminosUso.aspx','Términos de uso','width=500,height=150')">open window</a> goclicky(this)-->     
                <div class="row">  
                    <div class="col-md-12">                        
                        <a href="http://www.travesiasagradamaya.com.mx/terminos-uso.php" target='_blank' onclick="openWindow(this); return false;" style="color:blue;text-decoration: underline;">Términos de uso</a>&nbsp;/&nbsp;
                        <a href="http://www.travesiasagradamaya.com.mx/politicas-privacidad.php" target='_blank' onclick="openWindow(this); return false;" style="color:blue;text-decoration: underline;">Aviso de Privacidad Experiencias Xcaret</a>                        
                    </div>            
                </div>
            </div>
        </div>

    <dx:ASPxCallback ID="cbValida" runat="server" ClientInstanceName="cbValida" OnCallback="cbValida_Callback" ClientIDMode="AutoID">
    </dx:ASPxCallback>
        <dx:ASPxLoadingPanel ID="pnlinfo" runat="server" ClientInstanceName="pnlinfoproceso" Modal="True">
        </dx:ASPxLoadingPanel>
    </div>    
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
</body>  
</html>
