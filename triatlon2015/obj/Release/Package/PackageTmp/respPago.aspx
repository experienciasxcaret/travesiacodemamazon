﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="respPago.aspx.cs" Inherits="travesia.respPago" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Travesia Sagrada Maya</title>
    <meta name="viewport" content="width= device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="styles/bootstrap.css" />
    <link rel="stylesheet" href="styles/bootstrap.min.css" />    
    <link rel="stylesheet" href="styles/font-awesome.min.css" />    
    <link rel="stylesheet" href="styles/triatlon.css" />
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="styles/detallePago.css" />
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <script src="Scripts/jquery-1.11.2.min.js" ></script>
    <script src="Scripts/bootstrap.js" ></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.metadata.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/localization/messages_es.js"></script>

</head>
<body>
    <form class="form-horizontal" id="formFinaliza" runat="server">
    <div class="container">
        <div class="row">
            <div class="well col-xs-12 col-sm-12 col-md-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-2">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <div class="waves"></div>
                    </div>
                </div>
                 <div class="row">
                      <div class="text-center">
                        <h3>! Felicidades ya estas inscrito !</h3>
                        <h4>Te hemos enviado por correo tu(s) folio(s) de inscripción</h4>
                      </div>
                 </div>
                <br />
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <div class="col-xs-5 col-md-4">
                            <asp:Label ID="Label1" runat="server" class="etiquetas" Text="Nombre Canoero :" ></asp:Label>
                        </div>
                        <div class="col-xs-7 col-md-7">
                            <asp:Label ID="lblNombre" runat="server" class="etiquetasDato" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <asp:Label class="col-xs-5 col-md-4 etiquetas" ID="Label2" runat="server" Text="Sede de entrenamiento: "></asp:Label>
                        <asp:Label class="col-xs-7 col-md-7 etiquetasDato" ID="lblCategoria" runat="server" Text=""></asp:Label>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <asp:Label class="col-xs-5 col-md-4 etiquetas" ID="Label3" runat="server" Text="Número Canoero : "></asp:Label>
                        <asp:Label class="col-xs-7 col-md-7 etiquetasDato" ID="lblNumeroCompetidor" runat="server" Text=""></asp:Label>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <asp:Label class="col-xs-5 col-md-4 etiquetas" ID="Label4" runat="server" Text="Fecha pago : "></asp:Label>
                        <asp:Label class="col-xs-7 col-md-7 etiquetasDato" ID="lblFechaPago" runat="server" Text=""></asp:Label>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <asp:Label class="col-xs-5 col-md-4 etiquetas" ID="Label5" runat="server" Text="Total pagado : "></asp:Label>
                        <asp:Label class="col-xs-7 col-md-7 etiquetasDato" ID="lblTotal" runat="server" Text=""></asp:Label>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <asp:Label class="col-xs-5 col-md-4 etiquetas" ID="Label6" runat="server" Text="Clave Autorización : "></asp:Label>
                        <asp:Label class="col-xs-7 col-md-7 etiquetasDato" ID="lblNoReferencia" runat="server" Text=""></asp:Label>
                    </div>
                </div>

                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <asp:Label class="col-xs-5 col-md-4 etiquetas" ID="Label7" runat="server" Text="Folio inscripcion : "></asp:Label>
                        <asp:HyperLink class="col-xs-6 col-md-6 etiquetasDato" ID="lnkFolio" runat="server" ForeColor ="Blue" Target="_blank" Font-Underline="True">[lnkFolio]</asp:HyperLink>
                    </div>
                </div>
                <br />
                
                <div class="row">
                    <p class="col-xs-12 col-md-12" style="font-size:medium">
                        <B>En caso de requerir factura favor de ingresar a</B><a href="https://www.xperienciasxcaret.mx/core/facturacion/" style="color:blue"> https://www.aolxcaret.com/core/facturacion </a>
                        <B>en un plazo no mayor a 24 horas después de realizada la compra; de lo contrario no se emitirán comprobantes fiscales posteriores a esa fecha.</B>
                    </p>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <div class="waves2"></div>
                    </div>
                </div>
            </div>            
        </div> <!-- fin del div class row-->       
    </div>
        <asp:HiddenField ID="hdCorreoCompetidor" runat="server" />
        <asp:HiddenField ID="hdIdVenta" runat="server" />
    </form>
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="styles/detallePago.css" />
</body>
</html>
