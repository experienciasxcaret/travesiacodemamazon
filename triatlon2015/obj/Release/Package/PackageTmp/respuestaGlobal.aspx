﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="respuestaGlobal.aspx.cs" Inherits="travesia.respuestaGlobal" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body onload="fnReturnDataMainPage();">
    <form id="form1" runat="server">
    <div>    
        <table style="width:100%;">
            <tr>
                <td colspan="2">XCRM</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><asp:Label ID="lblEtiquetaCalfificacionFraude" runat="server" Text="Calificacion Fraude:"></asp:Label></td>
                <td>
                    <dx:ASPxLabel ID="lblCalificacionFraude" runat="server" Text="" ClientInstanceName="lblCalificacionFraude"></dx:ASPxLabel>                
                </td>

            </tr>

            <tr>
                <td><asp:Label ID="lblEtiquetaCodigoAutorizacion" runat="server" Text="Código Autorizacion:"></asp:Label></td>
                <td>
                    <dx:ASPxLabel ID="lblCodigoAutorizacion" runat="server" Text="" ClientInstanceName="lblCodigoAutorizacion"></dx:ASPxLabel></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblEtiquetaCodigoError" runat="server" Text="Código:"></asp:Label></td>
                <td>
                    <dx:ASPxLabel ID="lblCodigoError" runat="server" Text="" ClientInstanceName="lblCodigoError"></dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="lblEtiquetaDescripcionError" runat="server" Text="Descripción:"></asp:Label></td>
                <td>
                    <dx:ASPxLabel ID="lblDescripcionError" runat="server" Text="" ClientInstanceName="lblDescripcionError"></dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="lblEtiquetaResultadoCVV" runat="server" Text="Resultado CVV:"></asp:Label></td>
                <td>
                    <dx:ASPxLabel ID="lblResultadoCVV" runat="server" Text="" ClientInstanceName="lblResultadoCVV"></dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="lblEtiquetaNumeroTarjeta" runat="server" Text="Numero Tarjeta:"></asp:Label></td>
                <td>
                    <dx:ASPxLabel ID="lblNumeroTarjeta" runat="server" Text="" ClientInstanceName="lblNumeroTarjeta"></dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="lblEtiquetaNoReferencia" runat="server" Text="No. referencia:"></asp:Label></td>
                <td>
                    <dx:ASPxLabel ID="lblNoReferencia" runat="server" Text="" ClientInstanceName="lblNoReferencia"></dx:ASPxLabel>
                </td>
            </tr>
        </table> 
        <dx:ASPxLabel ID="lblUrl" runat="server" Text=""></dx:ASPxLabel>  
    </div>
    </form>
</body>

    <script type="text/javascript">
        function fnReturnDataMainPage() {
            try {
                //var value = document.getElementById('lblCodigoError').value + '|' + document.getElementById('lblDescripcionError').value + '|' + document.getElementById('lblCodigoAutorizacion').value;
                //parent.fnResultadoGlobalCollect(value);
                parent.fnResultadoGlobalCollect('<%=(lblCodigoError.Text.Length > 0)?lblCodigoError.Text:""%>|<%=(lblDescripcionError.Text.Length > 0)?lblDescripcionError.Text:""%>|<%=(lblCodigoAutorizacion.Text.Length > 0)?lblCodigoAutorizacion.Text:""%>|<%=(lblNumeroTarjeta.Text.Length > 0)?lblNumeroTarjeta.Text:""%>|<%=(lblCalificacionFraude.Text.Length > 0)?lblCalificacionFraude.Text:""%>|<%=(lblResultadoCVV.Text.Length > 0)?lblResultadoCVV.Text:""%>|<%=(lblNoReferencia.Text.Length > 0)?lblNoReferencia.Text:""%>');
                }
            catch (excptn) {
                    console.log(excptn);
                    //mandaExcepcion('fnDatosGC - muestraOrdenGlobalCollect<br>', excptn.description);
                }
            }
   </script>
</html>
