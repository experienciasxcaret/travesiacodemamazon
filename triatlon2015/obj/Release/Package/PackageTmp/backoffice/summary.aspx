﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="summary.aspx.cs" Inherits="travesia.summary" ValidateRequest="false" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Travesia Sagrada Maya</title>
    <meta name="viewport" content="width= device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../styles/bootstrap.css" />
    <link rel="stylesheet" href="../styles/bootstrap.min.css" />
    <link rel="stylesheet" href="../styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="../styles/detallePago.css" />
    <link rel="stylesheet" href="../styles/font-awesome.min.css" />    
    <link rel="stylesheet" href="../styles/triatlon.css" />
    <link rel="shortcut icon" type="../image/png" href="favicon.png" />

    <script src="../Scripts/jquery-1.11.2.min.js" ></script>
    <script src="../Scripts/bootstrap.js" ></script>
    <script type="text/javascript" src="../Scripts/jquery-validation/jquery.metadata.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-validation/localization/messages_es.js"></script>

</head>
<body>
    <form class="form-horizontal" id="formFinaliza" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="container">
        <div class="row" style="margin-top:5%">
                    <div class="col-xs-12 col-sm-12">
                        
                    </div>
         </div>
        <div class="row">
            <div class="well col-xs-12 col-sm-12 col-md-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-2">
                              
                 <div class="row">
                      <div class="text-center">
                        <h3>!! Travesia Sagrada Maya!!</h3>                        
                      </div>
                 </div>
                 
                        <div class="row"  style="margin:2px; text-align:justify">
                            <div class="form-group" style="margin:5px">
                                <div class="col-xs-6 col-sm-6">
                                    <asp:Label ID="lblNombre" runat="server" class="etiquetasDato" Text=""></asp:Label>
                                </div> 
                                <div class="col-xs-6 col-sm-6 divImageUpload" style="position: relative;">
                                    <dx:ASPxImage ID="imgAvatar" runat="server" ImageUrl="" title="" style="position:relative;float:right"></dx:ASPxImage>                                
                                </div>  
                            </div>               
                        </div>
                   
                <br />
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <div class="col-xs-1 col-md-1">
                            <asp:Label ID="Label12" runat="server" class="etiquetasDato" Text="Sede:"></asp:Label>
                        </div>
                        <div class="col-xs-3 col-md-3">
                            <asp:Label ID="lblSede" runat="server" class="" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <div class="col-xs-1 col-md-1">
                            <asp:Label ID="Label8" runat="server" class="etiquetasDato" Text="Sexo:"></asp:Label>                            
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <asp:Label ID="lblSexo" runat="server" class="" Text=""></asp:Label>
                        </div>
                        <div class="col-xs-1 col-md-1">
                            <asp:Label ID="Label9" runat="server" class="etiquetasDato" Text="Edad:"></asp:Label>                            
                        </div>
                        <div class="col-xs-1 col-md-1">
                            <asp:Label ID="lblEdad" runat="server" class="" Text=""></asp:Label>
                        </div>
                        <div class="col-xs-1 col-md-1">
                            <asp:Label ID="Label10" runat="server" class="etiquetasDato" Text="Peso:"></asp:Label>                            
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <asp:Label ID="lblPeso" runat="server" class="" Text=""></asp:Label>
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <asp:Label ID="Label11" runat="server" class="etiquetasDato" Text="T. Sangre:"></asp:Label>                            
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <asp:Label ID="lblSangre" runat="server" class="" Text=""></asp:Label>
                        </div>
                    </div>                    
                </div>
                <br />          
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <div class="col-xs-1 col-md-1">
                            <asp:Label ID="Label1" runat="server" class="etiquetasDato" Text="País:"></asp:Label>                            
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <asp:Label ID="lblPais" runat="server" class="" Text=""></asp:Label>
                        </div>
                        <div class="col-xs-1 col-md-1">
                            <asp:Label ID="Label2" runat="server" class="etiquetasDato" Text=" Estado:"></asp:Label>                             
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <asp:Label ID="lblEstado" runat="server" class="" Text=""></asp:Label>
                        </div>
                        <div class="col-xs-1 col-md-1">                            
                            <asp:Label ID="Label3" runat="server" class="etiquetasDato" Text="Ciudad:"></asp:Label>
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <asp:Label ID="lblCiudad" runat="server" class="" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">                      
                        <div class="col-xs-7 col-md-7">
                            <asp:Label ID="Label4" runat="server" class="etiquetasDato" Text="Dirección:"></asp:Label> 
                            <asp:Label ID="lblDireccion" runat="server" class="" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <div class="col-xs-3 col-md-3">
                            <asp:Label ID="Label6" runat="server" class="etiquetasDato" Text="Teléfono:"></asp:Label>
                             <asp:Label ID="lblTelefono" runat="server" class="" Text=""></asp:Label>
                        </div>                        
                        <div class="col-xs-3 col-md-3">
                            <asp:Label ID="Label7" runat="server" class="etiquetasDato" Text="Celular:"></asp:Label>
                            <asp:Label ID="lblCelular" runat="server" class="" Text=""></asp:Label>
                        </div>                        
                    </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <div class="col-xs-1 col-md-1">
                            <asp:Label ID="Label5" runat="server" class="etiquetasDato" Text="Email:"></asp:Label>                            
                        </div>
                        <div class="col-xs-5 col-md-5">
                            <asp:Label ID="lblEmail" runat="server" class="" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
                     <div class="row" style="margin-left:2px">
                    <div class="form-group">
                         <div class="col-xs-2 col-md-2">
                            <asp:Label ID="Label16" runat="server" class="etiquetasDato" Text="Vegetariano:"></asp:Label>                            
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <asp:Label ID="lblVegetariano" runat="server" class="" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                         <div class="col-xs-5 col-md-5">
                            <asp:Label ID="Label13" runat="server" class="etiquetasDato" Text="Frecuencia con que practica deporte:"></asp:Label>                            
                        </div>
                        <div class="col-xs-5 col-md-5">
                            <asp:Label ID="lbFrecuenciaDeporte" runat="server" class="" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <div class="col-xs-4 col-md-4">
                            <asp:Label ID="Label14" runat="server" class="etiquetasDato" Text="Años en que ha participado:"></asp:Label>                            
                        </div>
                        <div class="col-xs-5 col-md-5">
                            <asp:Label ID="lblAnioParticipaciones" runat="server" class="" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <div class="col-xs-3 col-md-3">
                            <asp:Label ID="Label15" runat="server" class="etiquetasDato" Text="Nivel de natación:"></asp:Label>                            
                        </div>
                        <div class="col-xs-3 col-md-3">
                            <asp:Label ID="lblNivelNatacion" runat="server" class="" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <div class="col-xs-4 col-md-4">
                            <asp:Label ID="Label17" runat="server" class="etiquetasDato" Text="Motivo de participacion:"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:2px; text-align:justify">
                    
                        <div class="col-xs-11 col-md-11">
                            <asp:Label ID="lblMotivoParticipacion" runat="server" class="" Text=""></asp:Label>
                        </div>                    
                </div>
                <br />
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <div class="col-xs-5 col-md-5">
                            <asp:Label ID="Label18" runat="server" class="etiquetasDato" Text="¿Sabes de que se trata la TSM:"></asp:Label>
                        </div>
                    </div>
                </div>              
                <div class="row" style="margin-left:2px; text-align:justify">                    
                    <div class="col-xs-11 col-md-11">
                        <asp:Label ID="lblSignificadoTSM" runat="server" class="" Text=""></asp:Label>
                    </div>                    
                </div>
                <br />
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <div class="col-xs-3 col-md-3">
                            <asp:Label ID="Label19" runat="server" class="etiquetasDato" Text="No. colaborador:"></asp:Label>
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <asp:Label ID="lblNumeroColaborador" runat="server" class="" Text=""></asp:Label>
                        </div>
                         <div class="col-xs-3 col-md-3">
                            <asp:Label ID="Label20" runat="server" class="etiquetasDato" Text="Unidad de negocio:"></asp:Label>
                        </div>
                        <div class="col-xs-4 col-md-4">
                            <asp:Label ID="lblUnidadNegocio" runat="server" class="" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
                <br />
                
                <div class="row">
                    <p class="col-xs-12 col-md-12" style="font-size:medium">                        
                    </p>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <div class="waves2"></div>
                    </div>
                </div>
            </div>            
        </div> <!-- fin del div class row-->       
    </div>
        <asp:HiddenField ID="hdCorreoCompetidor" runat="server" />
        <asp:HiddenField ID="hdIdVenta" runat="server" />
    </form>
<script>
    window.print();
</script>
</body>
</html>
