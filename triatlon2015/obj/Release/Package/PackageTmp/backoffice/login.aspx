﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="travesia.backoffice.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BackOffice</title>
    <meta name="viewport" content="width= device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../styles/bootstrap.css" />
    <link rel="stylesheet" href="../styles/bootstrap.min.css" />
    <link rel="stylesheet" href="../styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="../styles/basic.css" />
    <link rel="stylesheet" href="../styles/triatlonBackoffice.css" />
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <script src="../Scripts/jquery-1.11.3.js" ></script>
    <script src="../Scripts/bootstrap.js" ></script>
    <script src="../Scripts/loginBackoffice.js" ></script>
    <script type="text/javascript" src="../Scripts/jquery-validation/jquery.metadata.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-validation/localization/messages_es.js"></script>
</head>
<body>
      <div class="container">
        <div class="grid_16 header b-bottom">
		    <div class="cont">
			    <div class="logo hidden-phone" align="center">
				    <img class="img1" src="../img/tsm.png"/>
			    </div>
			    <div class="logo" align="center">
				
			    </div>
			    <div class="logo hidden-phone" align="center">
				
			    </div>
		    </div>
	    </div>
     
        <div class="grid_16">
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title">Sign In</div>                        
                    </div>     

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <form id="loginForm" class="form-horizontal" role="form" runat="server">
                             <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
                            <div  style="margin-left: 5%; margin-right: 5%" class="form-group">      
                                <div style="margin-bottom: 25px" class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>                                    
                                            <asp:TextBox runat="server" id="txtUsername" class="{required:true} form-control" placeholder="user" name="txtUsername">
                                            </asp:TextBox>                                                                                
                                 </div>
                             </div>                             
                              <div  style="margin-left: 5%; margin-right: 5%" class="form-group">
                                    <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>                                  
                                        <asp:TextBox runat="server" id="txtPass" class="{required:true} form-control" placeholder="password" name="txtPass" type="password">
                                        </asp:TextBox>                                        
                                    </div>
                              </div>                                                                                             

                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                            <div style="margin-top:10px" class="form-group">
                                <!-- Button -->
                                <div style="margin-left: 5%; margin-right: 5%" class="col-sm-12 controls">                                                                                                          
                                    <asp:button ID="btnLogin" runat="server" Text="Login" class="btn btn-success" ClientInstanceName="txtLogin" OnClick="txtLogin_Click"/>                                   
                                </div>
                            </div>
                                </ContentTemplate>
                                </asp:UpdatePanel>
                                
                           
                            </form>
                               <!-- <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                            Don't have an account! 
                                        <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
                                            Sign Up Here
                                        </a>
                                        </div>
                                    </div> -->
                           </div>                            
                        </div>                     
                    </div>           
        </div>    
     </div>  
         <div id="Message" class="scrollable">
                 <div  id="divMsg">
		             <table style="width:100%">
                         <tr>
                             <td style="width:85%">
                                <h3><asp:Label runat="server" ID="lblInfo" Text=""></asp:Label></h3>
                             </td>
                             <td style="width:15%" rowspan="2">
                                 <asp:Image ID="imgok" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/styles/icon-behavior-yes.png" style="display :none;" CssClass="arrowhand" /> 
                             </td>
                         </tr>
                         <tr>
                             <td style="width:85%">
                                    <p><asp:Label runat="server" ID="lblMensaje" Text=""></asp:Label></p>
                             </td>
                         </tr>
                     </table>		 
                </div>
        </div>
</body>
</html>
