﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/menu.Master"  EnableEventValidation="false" ValidateRequest="false"  AutoEventWireup="true" CodeBehind="editCompetidor.aspx.cs" Inherits="travesia.backoffice.editCompetidor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        add-on .input-group-btn > .btn {
          border-left-width:0;left:-2px;
          -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }
        /* stop the glowing blue shadow */
        .add-on .form-control:focus {
         box-shadow:none;
         -webkit-box-shadow:none; 
         border-color:#cccccc; 
        }
    </style>
    <link rel="stylesheet" href="../styles/detallePago.css" />
    <script src="../Scripts/editCompetidor.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="container">
        <form id="formEditCompetidor" role="form" class="form-horizontal" runat="server"> 
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <div class="grid_16 wrap" style="padding-left:5px">
           
           <dx:ASPxPageControl ID="pcEditar" runat="server"  Width="100%" ActiveTabIndex="0" OnActiveTabChanged="pcEditar_ActiveTabChanged" ActivateTabPageAction="Click" ClientIDMode="AutoID" LoadingPanelImagePosition="Left" TabAlign="Left" TabPosition="Top">
                <TabPages>
                    <dx:TabPage Text="Cambios principales">
<ActiveTabImage Align="NotSet"></ActiveTabImage>

<TabImage Align="NotSet"></TabImage>

<ActiveTabStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<HoverStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</HoverStyle>

<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</ActiveTabStyle>

<TabStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<HoverStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</HoverStyle>

<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</TabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="cambioGenerales" runat="server" SupportsDisabledAttribute="True">                                        
                                <div class="form-group">
                                        <div class="col-sm-2">
                                            <label>Nombre</label>
                                             <asp:TextBox runat="server" class="form-control"  id="txtNombre" name="txtNombre">
                                             </asp:TextBox>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>Apellido Paterno</label>                        
                                            <asp:TextBox runat="server" class="form-control"  id="txtPaterno" name="">
                                            </asp:TextBox>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>Apellido Materno</label>                        
                                            <asp:TextBox runat="server" class="form-control"  id="txtMaterno" name="txtMaterno">
                                            </asp:TextBox>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2">                                        
                                        <label>No. Competidor</label>                        
                                        <asp:TextBox runat="server" class="form-control"  id="txtNoCompetidor" name="txtNoCompetidor"  disabled="true">
                                        </asp:TextBox>                                        
                                    </div>                                                                                                                
                                    <div class="col-sm-2">
                                        <label for="feNacimiento" class="control-label">fecha Nacimiento</label>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                 <div class="input-group add-on date datepicker">
                                                    <asp:TextBox runat="server" class="form-control date-picker"  id="txtFeNacimiento" name="txtFeNacimiento" AutoPostBack="True" OnTextChanged="txtFeNacimiento_TextChanged"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <span class="btn btn-default"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>
                                                    <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>-->
                                                    <asp:CalendarExtender ID="ceFeNacimiento" Format="dd/MM/yyyy" TargetControlID="txtFeNacimiento" runat="server">
                                                    </asp:CalendarExtender> 
                                                 </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>                                                                                       
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label for="sexo" class="control-label">Sexo</label>
                                        <asp:DropDownList  class="form-control" ID="ddSexo" runat="server" name="sexo">
                                                <asp:ListItem Value="">Seleccione sexo</asp:ListItem>
                                                <asp:ListItem Value="2">Masculino</asp:ListItem>
                                                <asp:ListItem Value="3">Femenino</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="Estatus" class="control-label">Estatus Cliente</label>
                                        <asp:DropDownList  class="form-control" ID="ddlEstatus" runat="server" name="estatus">
                                                <asp:ListItem Value="">Seleccione Estatus</asp:ListItem>
                                                <asp:ListItem Value="1">Activo</asp:ListItem>
                                                <asp:ListItem Value="3">Inactivo</asp:ListItem>
                                                <asp:ListItem Value="14">En Proceso</asp:ListItem>
                                                <asp:ListItem Value="15">Inscrito</asp:ListItem>                                                
                                                <asp:ListItem Value="17">Registrado</asp:ListItem>                                                
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="Estatus" class="control-label">Forma de pago</label>
                                        <asp:DropDownList  class="form-control" ID="ddlFormaPagoInscripcion" runat="server" name="formaPagoInscripcion">
                                                <asp:ListItem Value="">Seleccione Forma de pago</asp:ListItem>
                                                <asp:ListItem Value="1">Pago normal (si paga)</asp:ListItem>
                                                <asp:ListItem Value="3">Cortesía (competidor no paga)</asp:ListItem>                                                                                                
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">                                        
                                        <label>Email :</label>                        
                                        <asp:TextBox runat="server" type="email" class="form-control"  id="txtEmail" name="txtEmail">
                                        </asp:TextBox>                                        
                                    </div>
                                </div>

                                 <div class="form-group">
                                    
                                    <div class="col-sm-3">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <label for="categoria">Sede:</label>  
                                                <asp:DropDownList  class="form-control" ID="ddlCategoria" runat="server" AutoPostBack="True" name="categoria" OnSelectedIndexChanged="ddlCategoria_SelectedIndexChanged">
                                                <asp:ListItem Value="">Seleccione categoria</asp:ListItem>                                                           
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>                                        
                                    </div>
                                </div>
                              
                                <div class="form-group">
                                      <div class="col-sm-1">
                                        <!--<button type="submit" class="btn btn-info pull-left">Buscar</button>-->   
                                       <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                            <ContentTemplate>                                                                                   
                                              <asp:Button ID="btnGuardar" runat="server" Text="Guardar" class="btn btn-info pull-left" CssPostfix="&quot;btn btn-info pull-left" ClientInstanceName="btnGuardar"  UseSubmitBehavior="false"  AutoPostBack="false" OnClick="btnGuardar_Click"/>
                                             </ContentTemplate>
                                      </asp:UpdatePanel>
                                       </div>
                                       <div class="col-sm-1">
                                          <dx:ASPxButton ID="btnCancelar" runat="server" Text="Cancelar" class="btn btn-info pull-left" name="btnCancelar"  CssPostfix="&quot;btn btn-info pull-left" ClientInstanceName="btnCancelar" AutoPostBack="False" OnClick="btnCancelar_Click">
<Image Align="NotSet"></Image>

<CheckedStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</CheckedStyle>

<PressedStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</PressedStyle>

<HoverStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</HoverStyle>

<FocusRectBorder BorderStyle="NotSet"></FocusRectBorder>

<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>

<DisabledStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</DisabledStyle>
                                          </dx:ASPxButton>
                                        </div>
                                      
                                </div>
                                <div class="form-group">
                                    <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                    <ContentTemplate>                                     
                                        
                                        <asp:Label class="col-md-4" ID="lblFolio2" visible="False" runat="server" Text="Cambios Guardados. Folio inscripcion : " Font-Bold="True" Font-Size="Larger"></asp:Label>
                                        <asp:HyperLink class="col-md-4" ID="lnkFolio2" visible="False" runat="server" ForeColor ="Blue" Target="_blank" Font-Underline="True" Font-Size="Larger">Imprimir Cupon</asp:HyperLink>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>                                     
                                </div>
                                 <div class="form-group">
                                     <div class="col-md-3">
                                         <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                            <ContentTemplate>                                                 
                                                <asp:Button ID="btnGenerarLink" runat="server" Text="Generar Link para cambio categoría" class="btn btn-info pull-left" name="btnGenerarLink"  CssPostfix="&quot;btn btn-info pull-left" ClientInstanceName="btnGenerarLink" UseSubmitBehavior="false"  AutoPostBack="false" OnClick="btnGenerarLink_Click"/>
                                             </ContentTemplate>
                                         </asp:UpdatePanel>
                                     </div>
                                    <div class="col-md-5">
                                         <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                            <ContentTemplate>
                                                <asp:HyperLink ID="lnkCambioCategoria" visible="false" runat="server" ForeColor ="Blue" Target="_blank" Font-Underline="True" Text=""></asp:HyperLink>
                                            </ContentTemplate>
                                         </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div class="form-group">
                                     <div class="col-md-3">
                                         <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                            <ContentTemplate>                                                 
                                                 <span class="btn btn-info pull-left">
                                                    <dx:ASPxHyperLink ID="linkPDF" runat="server" ClientIDMode="AutoID" NavigateUrl='' Text='Imprimir pdf competidor' Target="_blank" Font-Underline="True" Font-Bold="True" ForeColor="white" />
                                                </span>
                                             </ContentTemplate>
                                         </asp:UpdatePanel>
                                     </div>                                   
                                </div>
                               
                                <asp:HiddenField ID="hdIdClienteContactoEventoPrincipales" runat="server" Value="0" />
                                <asp:HiddenField ID="hdIdClienteContactoPrincipales" runat="server" Value="0" />

                                <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                    <ContentTemplate>
                                        <asp:HiddenField ID="hdIdClienteContactoEventoFecha" runat="server" Value="0" />
                                        <asp:HiddenField ID="hdIdClienteContactoFecha" runat="server" Value="0" />
                                         <asp:HiddenField ID="hdMontoDiferencia" runat="server" Value="0" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <div runat="server" id="divPago" visible="false">
                                            
                                                <div class="well col-xs-12 col-sm-12 col-md-8 col-xs-offset-1 col-sm-offset-1 col-md-offset-2">
                                                <div class="row">
                                               
                                                    <div class="form-group" style="padding:5px">
                                                        <div class="col-xs-12 col-sm-12"> 
                                                            <asp:Label class="col-xs-12 col-sm-12" ID="lblMensajeDiferencia" runat="server" Text="" Font-Bold="True" Font-Size="Large" ForeColor="#CC0000"></asp:Label>
                                                        </div>
                                                    </div>
                                               
                                                <div class="form-group" style="padding:5px">
                                                    <div class="col-xs-12 col-sm-12"> 
                                                        <asp:RadioButtonList ID="rblTipoTarjeta" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblTipoTarjeta_SelectedIndexChanged">
                                                            <asp:ListItem
                                                                Enabled="True"                                                
                                                                Text='<img src="https://www.xperienciasxcaret.mx/ecom/webroot/images/visa.png"/>'
                                                                Value="2"
                                                                />                                                                                     
                                                            <asp:ListItem
                                                                Enabled="True"                            
                                                                Text='<img src="https://www.xperienciasxcaret.mx/ecom/webroot/images/master.png"/>'
                                                                Value="1"
                                                            />
                                                            <asp:ListItem
                                                                Enabled="True"                            
                                                                Text='<img src="https://www.xperienciasxcaret.mx/ecom/webroot/images/amex.png"/>'
                                                                Value="3"
                                                            />
                                                        </asp:RadioButtonList>                                                   
                                                    </div>
                                                 </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding-left:5px">
                                                        <div class="col-md-12">
                                                            <div class="col-xs-10 col-sm-10">
                                                                <label class="col-xs-3 col-sm-3">Tipo Moneda</label>
                                                                <div class="col-xs-5 col-sm-5">
                                                                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:DropDownList class="{required: true} form-control" ID="ddlCambioMoneda" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddlCambioMoneda_SelectedIndexChanged" >
                                                                                <asp:ListItem Value="0">--</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                <div class="form-group" style="padding-left:5px">
                                                    <div class="col-md-12">
                                                        <div class="col-xs-10 col-sm-10">
                                                            <label class="col-xs-3 col-sm-3">Forma de pago</label>
                                                            <div class="col-xs-5 col-sm-5">
                                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                            <ContentTemplate>
                                                                <asp:DropDownList class="{required: true} form-control" ID="ddlFormaPago" required runat="server" AutoPostBack="True" ControlToValidate="ddlFormaPago" OnSelectedIndexChanged="ddlFormaPago_SelectedIndexChanged">
                                                                        <asp:ListItem Value="">Seleccione forma de pago</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rvFormaPago" runat="server" ErrorMessage="" ControlToValidate="ddlFormaPago" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                            </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="form-group" style="padding-left:5px">
                                                    <div class="col-md-12">
                                                        <div class="col-md-10">                                
                                                            <asp:Label class="col-xs-3" ID="lblBanco" runat="server" Text="Banco emisor" Font-Bold="True"></asp:Label>
                                                            <div class="col-sm-5">
                                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                            <ContentTemplate>
                                                                <asp:DropDownList class="{required: true} form-control" ID="ddlBanco" runat="server" AutoPostBack="True" required OnSelectedIndexChanged="ddlBanco_SelectedIndexChanged">
                                                                        <asp:ListItem Value="">Seleccione Banco emisor</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rvBanco" runat="server" ErrorMessage="" ControlToValidate="ddlBanco" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                            </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    </div>
                                                <div class="row">
                                                <div class="form-group" style="padding-left:5px">
                                                    <div class="col-md-12">
                                                        <div class ="col-sm-12"	>
                                                            
                                                            <asp:Label style="padding-right:5px" class ="col-sm-3" ID="lblNombreTh" runat="server" Text="Nombre Tarjetahabiente" Font-Bold="True"></asp:Label>
                                                            <div class ="col-sm-4">
                                                                <asp:TextBox runat="server" type="text" class="{required:true, nombreth: /^[A-Za-záéíóúñ,.]{2,}([\s][A-Za-záéíóúñ,.]{2,})+$/ } form-control" id="txtNombreTH" required placeholder="nombre tarjetahabiente" name="nombreTH"> </asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rvNombreTH" runat="server" ErrorMessage="" ControlToValidate="txtNombreTH" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>                                                
                                                    </div>   
                                                 </div>
                                                    </div>
                                                <div class="row">
                                                <div class="form-group" style="padding-left:5px"> 
                                                    <div class="col-md-12">
                                                        <div class="col-sm-12">
                                                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                            <ContentTemplate>                                                                             
                                                                <asp:Label style="padding-right:5px" class ="col-sm-3" ID="lblNumTarjeta" runat="server" Text="Numero de tarjeta" Font-Bold="True"></asp:Label>
                                                                <div class ="col-sm-4">
                                                                    <asp:TextBox runat="server" class="{required:true, creditcard: true} form-control" id="txtNoTarjeta" placeholder="Numero Tarjeta" required data-stripe="number" name="noTarjeta"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rvNoTarjeta" runat="server" ErrorMessage="" ControlToValidate="txtNoTarjeta" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                                 </div>
                                                             </ContentTemplate>
                                                             </asp:UpdatePanel>                                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                                    </div>
                                                <div class="row">
                                                <div class="form-group" style="padding-left:5px">
                                                     <div class="col-md-12">                                                        
                                                        <div class="col-md-12">
                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                                            <ContentTemplate>                                                                                                               
                                                                <asp:Label class ="col-sm-3" ID="lblFechaExpira" runat="server" Text="Fecha de expiración" Font-Bold="True"></asp:Label>
                                                                 <div class ="col-sm-2">
                                                                     <asp:DropDownList class="{required: true} form-control" ID="ddlMesExpira" runat="server" AppendDataBoundItems="True" AutoPostBack="False">
                                                                        <asp:ListItem Value="01">01</asp:ListItem>
                                                                        <asp:ListItem Value="02">02</asp:ListItem>
                                                                        <asp:ListItem Value="03">03</asp:ListItem>
                                                                        <asp:ListItem Value="04">04</asp:ListItem>
                                                                        <asp:ListItem Value="05">05</asp:ListItem>
                                                                        <asp:ListItem Value="06">06</asp:ListItem>
                                                                        <asp:ListItem Value="07">07</asp:ListItem>
                                                                        <asp:ListItem Value="08">08</asp:ListItem>
                                                                        <asp:ListItem Value="09">09</asp:ListItem>
                                                                        <asp:ListItem Value="10">10</asp:ListItem>
                                                                        <asp:ListItem Value="11">11</asp:ListItem>
                                                                        <asp:ListItem Value="12">12</asp:ListItem>
                                                                      </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rvMesExpira" runat="server" ErrorMessage="" ControlToValidate="ddlMesExpira" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                                 </div>
                                                                 <div class ="col-sm-2">
                                                                     <asp:DropDownList class="{required: true} form-control" ID="ddlAnioExpira" runat="server" AppendDataBoundItems="True" AutoPostBack="False" OnSelectedIndexChanged="ddlAnioExpira_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0">--</asp:ListItem>
                                                                     </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rvAnioExpira" runat="server" ErrorMessage="" ControlToValidate="ddlAnioExpira" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>    
                                                    </div>                            
                                                </div>
                                                </div>                                                
                                                <div class="row">
                                                <div class="form-group" style="padding-left:5px">
                                                    <div class="col-md-12">      
                                                        <div class="col-md-12">
                                                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                            <ContentTemplate>                                                                                                                   
                                                                <asp:Label ID="lblCVV" class ="col-sm-3" runat="server" Text="Codigo de seguridad" Font-Bold="True"></asp:Label>
                                                                <div class ="col-sm-2">
                                                                    <asp:TextBox type="password" runat="server" class="{required: true, digits:true} form-control" id="txtCVV" placeholder="CVV" required data-stripe="cvc " name="anio"> </asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rvCVV" runat="server" ErrorMessage="" ControlToValidate="txtCVV" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>                                                                      
                                                    </div>
                                                </div>
                                                    </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding-left:5px">
                                                        <div class="col-md-12">      
                                                            <div class="col-md-12">
                                                                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                                <ContentTemplate>                                                                                                                   
                                                                    <asp:Label ID="lblReferencia" class ="col-sm-3" runat="server" Text="No. Referencias" Font-Bold="True" Visible="false"></asp:Label>
                                                                    <div class ="col-sm-3">
                                                                        <asp:TextBox runat="server" class="form-control" id="txtReferencia" placeholder="No. Referencia" Visible="false" required autofocus name="referencia"> </asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rvReferencia" runat="server" ErrorMessage="" ControlToValidate="txtReferencia" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>                                                                      
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="row">
                                                    <div class="form-group" style="padding-left:5px">
                                                        <div class="col-xs-12 col-sm-12">
                                                            <div class="col-md-12">
                                                            <!--<button class="btn btn-success btn-lg btn-block" type="submit">Start Subscription</button>-->
                                                                <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:HyperLink class="col-md-4" ID="lnkFolio" visible="False" runat="server" ForeColor ="Blue" Target="_blank" Font-Underline="True" Font-Size="Larger">Pago realizado. Imprimir Cupon</asp:HyperLink>
                                                                                                                                        
                                                                </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding-left:5px">
                                                        <div class="col-xs-12 col-sm-12">
                                                            <div class="col-md-12">
                                                            <!--<button class="btn btn-success btn-lg btn-block" type="submit">Start Subscription</button>-->
                                                                <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                                                                <ContentTemplate>                                                                    
                                                                    <asp:Label ID="lblRespuestaPago" runat="server" Text="" Font-Bold="True" Font-Size="Medium"></asp:Label>                                                                    
                                                                </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding-left:5px">
                                                        <div class="col-xs-12 col-sm-12">
                                                            <div class="col-md-12">
                                                            <!--<button class="btn btn-success btn-lg btn-block" type="submit">Start Subscription</button>-->
                                                                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                                                <ContentTemplate>                                                                                                                                        
                                                                    <dx:ASPxButton  ID="btnPagarFinal" runat="server" Text="Pagar" class="btnPagarFinal btn btn-success btn-lg btn-block text-center" name="btnPagarFinal" CssPostfix="&quot;btn btn-success btn-lg btn-block text-center" OnClick="btnPagarFinal_Click" ClientInstanceName="btnPagarName" ValidationGroup="validacionTarjeta"></dx:ASPxButton>                                                                    
                                                                </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <asp:UpdatePanel ID="upGC" runat="server" UpdateMode="Conditional" >
                                                    <ContentTemplate>  
                                                        <div class="dvGlobalOver" style="display:none;">
                                                            <div id='overpay'></div>
                                                                <div id="contentGlobal">
                                                                    <input id="cancel" type="button">
                                                                    <asp:HiddenField ID="hddResultadoPagoGlobalCollect" runat="server" Value="" ClientIDMode="Static"/>
                                                                    <asp:Label ID="lblRespuestaPagoGlobalCollect" runat="server" style="font-weight: 700 font-size: 8pt"></asp:Label>
                                                                    <asp:Panel ID="pnGlobalCollect" runat="server">
                                                                        <div class="insideformas">
                                                                            <table align="left" class="tbCss1" style="height:100%; width:100%">
                                                                                    <caption class="tb1CaptionTop">
                                                                                        <dx:ASPxLabel ID="lblEtiGlobalCollect" runat="server" ClientIDMode="AutoID" Text="Global Collect" Visible="true" style="margin-bottom:15px">
                                                                                        </dx:ASPxLabel>
                                                                                    </caption>
                                                                                    <tr >
                                                                                        <td align="left">
                                                                                            <iframe id="ifrGlobalCollect" runat="server" src="" class="ifrGlobalCollect">
                                                                                            <p>
                                                                                                Your browser does not support iframes.</p>
                                                                                            </iframe>                                
                                                                                            <dx:ASPxButton  ID="btnGlobalCollect" runat="server" UseSubmitBehavior="False" Text="" name="btnGlobalCollect" ClientInstanceName="btnGlobalCollect" OnClick="btnGlobalCollect_Click" CausesValidation="False" AutoPostBack="False" ClientVisible="false"></dx:ASPxButton>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                    </asp:Panel>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            <!-- fin del row -->
                                        </div><!-- fin del divPago -->
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="Archivos">
<ActiveTabImage Align="NotSet"></ActiveTabImage>

<TabImage Align="NotSet"></TabImage>

<ActiveTabStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<HoverStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</HoverStyle>

<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</ActiveTabStyle>

<TabStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<HoverStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</HoverStyle>

<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</TabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="cambioNacimientoCategoria" runat="server" SupportsDisabledAttribute="True">
                                 <div class="form-group">
                                    
                                </div>
                               

                                <div class="row" style="margin:5px;">
                                    <div class="col-sm-12">
                                        <div class="col-sm-9">
                                            <asp:UpdatePanel ID="UpdatePanel29" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                <div class="divTableFiles">
                                                    <table style="border:hidden;">
                                                        <thead>
                                                            <tr><th>Thumb</th><th>Nombre</th><th>Fecha de Alta</th><th>Descargar</th></tr>
                                                        </thead>
                                                        <tbody id="tbodyFiles">
                                                        
                                                        </tbody>
                                                    </table>
                                                </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                                
                               
                                
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="Agregar Cena" Visible="False">
                        <ActiveTabImage Align="NotSet">
                        </ActiveTabImage>
                        <TabImage Align="NotSet">
                        </TabImage>
                        <ActiveTabStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                            <HoverStyle>
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                <BorderLeft BorderStyle="NotSet" />
                                <BorderTop BorderStyle="NotSet" />
                                <BorderRight BorderStyle="NotSet" />
                                <BorderBottom BorderStyle="NotSet" />
                            </HoverStyle>
                            <BackgroundImage Repeat="Repeat" />
                            <Border BorderStyle="NotSet" />
                            <BorderLeft BorderStyle="NotSet" />
                            <BorderTop BorderStyle="NotSet" />
                            <BorderRight BorderStyle="NotSet" />
                            <BorderBottom BorderStyle="NotSet" />
                        </ActiveTabStyle>
                        <TabStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                            <HoverStyle>
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                <BorderLeft BorderStyle="NotSet" />
                                <BorderTop BorderStyle="NotSet" />
                                <BorderRight BorderStyle="NotSet" />
                                <BorderBottom BorderStyle="NotSet" />
                            </HoverStyle>
                            <BackgroundImage Repeat="Repeat" />
                            <Border BorderStyle="NotSet" />
                            <BorderLeft BorderStyle="NotSet" />
                            <BorderTop BorderStyle="NotSet" />
                            <BorderRight BorderStyle="NotSet" />
                            <BorderBottom BorderStyle="NotSet" />
                        </TabStyle>
                        <ContentCollection>
                            <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                asdfasdf
                                <div class="form-group" style="padding:5px">
                                    <div class="col-md-1">
                                        <label>Folio cena</label>
                                    </div>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" class="form-control"  id="txtFolioCena" name="txtFolioCena">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group" style="padding:5px">
                                    <div class="col-xs-12 col-sm-12"> 
                                        <asp:RadioButtonList ID="rdOpciones" runat="server" RepeatDirection="Vertical" AutoPostBack="false">
                                            <asp:ListItem
                                                Enabled="True"                                                
                                                Text='¿Convertir en cena?'
                                                Value="1"
                                                />                                                                                     
                                            <asp:ListItem
                                                Enabled="True"                            
                                                Text='¿Agregar acompañantes?'
                                                Value="2"
                                            />                                           
                                        </asp:RadioButtonList>                                                   
                                    </div>
                                </div>
                                <div class="form-group" style="padding:5px">
                                    <div class="col-md-2">
                                         <dx:ASPxButton ID="btnContinuar" runat="server" Text="Continuar" class="btn btn-info pull-left" name="btnContinuar"  CssPostfix="&quot;btn btn-info pull-left" ClientInstanceName="btnContinuar" AutoPostBack="False" OnClick="btnContinuar_Click">
<Image Align="NotSet"></Image>

<CheckedStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</CheckedStyle>

<PressedStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</PressedStyle>

<HoverStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</HoverStyle>

<FocusRectBorder BorderStyle="NotSet"></FocusRectBorder>

<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>

<DisabledStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</DisabledStyle>
                                         </dx:ASPxButton>
                                    </div>
                                </div>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                </TabPages>

<LoadingPanelImage Align="NotSet"></LoadingPanelImage>

<LoadingPanelStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</LoadingPanelStyle>

<ActiveTabImage Align="NotSet"></ActiveTabImage>

<TabImage Align="NotSet"></TabImage>

<ActiveTabStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<HoverStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</HoverStyle>

<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</ActiveTabStyle>

<TabStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<HoverStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</HoverStyle>

<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</TabStyle>

<ContentStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</ContentStyle>

<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>

<DisabledStyle>
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</DisabledStyle>
            </dx:ASPxPageControl>
            <asp:UpdatePanel ID="UpdatePanel12" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hdIdProductoOriginal" runat="server" Value="0" />
                <asp:HiddenField ID="hdIdVentaDetalle" runat="server" Value="0" />
                <asp:HiddenField ID="hdIdVenta" runat="server" Value="0" />
                <asp:HiddenField ID="hdCnGlobalCollect" runat="server" Value="0" />
                <asp:HiddenField ID="hdCnGlobalCollectI" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdOrdenGlobal" runat="server" Value="0" />
                <asp:HiddenField ID="hdCorreoCompetidor" runat="server" Value="0" />
                <asp:HiddenField ID="hdIdPais" runat="server" Value="0" />
                <asp:HiddenField ID="hdIdEstado" runat="server" Value="0" />
                <asp:HiddenField ID="hdIV" runat="server" Value="" />
                <asp:HiddenField ID="hdEsPagoTarjeta" runat="server" Value="" />
                <asp:HiddenField ID="hddPagoGateWay" runat="server" value="0"/>
                <asp:HiddenField ID="hddPagoWorldPay" runat="server" value="0"/>
                <asp:HiddenField ID="hddResultadoPagoWorldPay" runat="server" Value=""/>
            </ContentTemplate>
            </asp:UpdatePanel>
            </div>
        </form>
    </div>
</asp:Content>
