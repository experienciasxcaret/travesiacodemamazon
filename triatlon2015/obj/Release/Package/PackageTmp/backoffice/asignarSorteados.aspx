﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/menu.Master" AutoEventWireup="true" CodeBehind="asignarSorteados.aspx.cs" Inherits="travesia.backoffice.asignarSorteados" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="grid_16 wrap" style="padding-left:5px">
            <form id="Form1" role="form" class="form-horizontal" runat="server">
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <br />
            <label class="col-sm-1" for="inputNoRifa" style="margin-top:3px">No. rifa</label>
            <div class="col-sm-2">
                 <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>    
                    <asp:TextBox runat="server" class="form-control"  id="txtNoRifa" autofocus placeholder="No. Rifa" name="txtNoRifa">
                    </asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="form-group">
                  <div class="col-sm-6">
                    <!--<button type="submit" class="btn btn-info pull-left">Buscar</button>-->
                      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                          <ContentTemplate>
                               <dx:ASPxButton ID="btnBuscar" runat="server" ClientInstanceName= "btnBuscar" Text="Buscar" class="btn btn-info pull-left" CssPostfix="&quot;btn btn-info pull-left" OnClick="btnBuscar_Click" Visible="False">
                                </dx:ASPxButton>
                          </ContentTemplate>
                      </asp:UpdatePanel>                        
                  </div>
            </div>
            <br />
            
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                    <div class="form-group">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-2" style="margin-top:5px">Nombre</label>
                       
                        <label class="col-sm-3" style="margin-top:5px">Correo</label>
                       
                        <label class="col-sm-2" style="margin-top:5px">Categoría</label>
                        
                         <label class="col-sm-2" style="margin-top:5px">Estatus</label>                        
                    </div>
                    <div class="form-group"> 
                        <div class="col-sm-1"></div>                      
                        <div class="col-sm-2">
                            <dx:ASPxLabel ID="lblNombre" runat="server" Text="" class="form-control" style="padding-bottom:3px" Font-Size="Small"></dx:ASPxLabel>
                        </div>                        
                        <div class="col-sm-3">
                            <dx:ASPxLabel ID="lblCorreo" runat="server" Text="" class="form-control" style="padding-bottom:3px" Font-Size="Small"></dx:ASPxLabel>
                        </div>                        
                        <div class="col-sm-2">
                            <dx:ASPxLabel ID="lblCategoria" runat="server" Text="" class="form-control" style="padding-bottom:3px" Font-Size="Small"></dx:ASPxLabel>
                        </div>                         
                        <div class="col-sm-2">
                            <dx:ASPxLabel ID="lblEstatus" runat="server" Text="" class="form-control" style="padding-bottom:3px" Font-Size="Small"></dx:ASPxLabel>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <div class="col-sm-1"></div>                      
                        <div class="col-sm-2">
                            <dx:ASPxLabel ID="lblNombre2" runat="server" Text="" class="form-control" style="padding-bottom:3px" Font-Size="Small"></dx:ASPxLabel>
                        </div>                        
                        <div class="col-sm-2">
                            <dx:ASPxLabel ID="lblCorreo2" runat="server" Text="" class="form-control" style="padding-bottom:3px" Font-Size="Small"></dx:ASPxLabel>
                        </div>                        
                        <div class="col-sm-2">
                            <dx:ASPxLabel ID="lblCategoria2" runat="server" Text="" class="form-control" style="padding-bottom:3px" Font-Size="Small"></dx:ASPxLabel>
                        </div>                         
                        <div class="col-sm-2">
                            <dx:ASPxLabel ID="lblEstatus2" runat="server" Text="" class="form-control" style="padding-bottom:3px" Font-Size="Small"></dx:ASPxLabel>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <div class="col-sm-1"></div>                      
                        <div class="col-sm-2">
                            <dx:ASPxLabel ID="lblNombre3" runat="server" Text="" class="form-control" style="padding-bottom:3px" Font-Size="Small"></dx:ASPxLabel>
                        </div>                        
                        <div class="col-sm-2">
                            <dx:ASPxLabel ID="lblCorreo3" runat="server" Text="" class="form-control" style="padding-bottom:3px" Font-Size="Small"></dx:ASPxLabel>
                        </div>                        
                        <div class="col-sm-2">
                            <dx:ASPxLabel ID="lblCategoria3" runat="server" Text="" class="form-control" style="padding-bottom:3px" Font-Size="Small"></dx:ASPxLabel>
                        </div>                         
                        <div class="col-sm-2">
                            <dx:ASPxLabel ID="lblEstatus3" runat="server" Text="" class="form-control" style="padding-bottom:3px" Font-Size="Small"></dx:ASPxLabel>
                        </div>
                    </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                                
             <div class="form-group">   
                 <div class="col-sm-1"></div>
                 <div class="col-sm-2">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <dx:ASPxButton ID="btnAsignar" ClientInstanceName="btnAsignar" runat="server" Text="Asignar Sorteado" class="btn btn-info pull-left" CssPostfix="&quot;btn btn-info pull-left" OnClick="btnAsignar_Click" Enabled="true"></dx:ASPxButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                 </div>            
                  <div class="col-sm-9">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>                    
                            <dx:ASPxHyperLink ID="hplRutaSorteado" runat="server" Text="" Visible="false" Font-Size="Medium" Font-Bold="True" ForeColor="Blue" Target="_blank">
                            </dx:ASPxHyperLink>
                          <asp:HiddenField ID="hdIdClienteContacto" runat="server" Value="0" />
                          <asp:HiddenField ID="hdIdClienteContactoEvento" runat="server" Value="0" />
                          <asp:HiddenField ID="hdIdClienteDetalle" runat="server" Value="" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                  </div>
            </div>
            </form>
        <br />
        </div>
     </div>
</asp:Content>