﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/menu.Master" AutoEventWireup="true" CodeBehind="competidores.aspx.cs" Inherits="travesia.backoffice.competidores" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">   
    <script>
        function enfocarDivBusqueda(elementoDom)
        {
            //e.preventDefault();
            console.log(elementoDom);
            var elemento = document.getElementById(elementoDom);
            //document.getElementById(elementoDom).focus();
                $("body, html").animate({
                    scrollTop: $("." + elementoDom).offset().top
                }, 600);
                //return $(document).height() - $("#" + elementoDom).scrollTop() - $("#" + elementoDom).height();
        }

    </script>
</asp:Content>

<asp:Content id="content2" contentPlaceHolderId="MainContent" runat="server">
    
    <div class="container">       
         <form id="Form1" role="form" class="form-horizontal" runat="server">
          <div class="grid_16 wrap" style="padding-left:4%">
             <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                     <div class="form-group">                
                  <label class="col-sm-1" for="inputEmail1" style="margin-top:3px">No. Competidor</label>
                  <div class="col-sm-2">                      
                      <asp:TextBox runat="server" class="form-control"  id="txtNoCompetidor" placeholder="No. Competidor" name="txtNoCompetidor">
                      </asp:TextBox>
                  </div>
                  <label class="col-sm-1" for="noFolio" style="margin-top:5px">Folio</label>
                  <div class="col-sm-2">
                      <asp:TextBox runat="server" class="form-control"  id="txtNoFolio" placeholder="No. Folio" name="txtNoFolio">
                      </asp:TextBox>
                  </div>
            </div>
            <!--<div class="form-group">
                  <label class="col-sm-2" for="inputPassword1">Folio</label>
                  <div class="col-sm-4"><input class="form-control" id="inputPassword1" placeholder="Folio"></div>
            </div>-->
            <div class="form-group">
                    <div class="col-sm-3">
                        <label>Nombre</label>
                         <asp:TextBox runat="server" class="form-control"  id="txtNombre" placeholder="Nombre" name="txtNombre">
                         </asp:TextBox>
                    </div>
                    <div class="col-sm-3">
                        <label>Apellido Paterno</label>                        
                        <asp:TextBox runat="server" class="form-control"  id="txtPaterno" placeholder="Apellido Paterno" name="txtPaterno">
                        </asp:TextBox>
                    </div>
            </div>
            <!-- <div class="form-group">
                  <label class="col-sm-12">Phone number</label>
                  <div class="col-sm-1"><input class="form-control" placeholder="000" type="text"><div class="help">area</div></div>
                  <div class="col-sm-1"><input class="form-control" placeholder="000" type="text"><div class="help">local</div></div>
                  <div class="col-sm-2"><input class="form-control" placeholder="1111" type="text"><div class="help">number</div></div>
                  <div class="col-sm-2"><input class="form-control" placeholder="123" type="text"><div class="help">ext</div></div>
            </div> -->
            <div class="form-group">
                   <label class="col-sm-1" for="inputnoRifa" style="margin-top:5px">No. Rifa</label>
                  <div class="col-sm-2">                      
                      <asp:TextBox runat="server" class="form-control"  id="txtNoRifa" placeholder="No rifa" name="txtNoRifa">
                      </asp:TextBox>
                  </div>
                  <label class="col-sm-1" for="inputEmail" style="margin-top:5px">Email</label>
                  <div class="col-sm-2">                      
                      <asp:TextBox runat="server" class="form-control"  id="txtEmail" placeholder="email" name="txtEmail">
                      </asp:TextBox>
                  </div>
            </div>

            <div class="form-group">
                 <div class="col-md-3 selectContainer">                                                                                                        
                        <label for="categoria" class="control-label">Categoria</label>
                      <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                       <ContentTemplate>
                            <asp:DropDownList  class="form-control" ID="ddlCategoria" runat="server" AutoPostBack="True" name="categoria">
                                    <asp:ListItem Value="">Seleccione Categoria</asp:ListItem>                                                           
                            </asp:DropDownList>    
                    </ContentTemplate>  
                    </asp:UpdatePanel>                                               
                </div>
                <div class="col-md-3">                                                                                                        
                    <label for="feAlta" class="control-label">Fecha Alta</label>
                     <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <div class="input-group add-on date datepicker">                                
                                <div class="input-group-btn">
                                    <asp:TextBox runat="server" class="form-control date-picker txtFeAlta"  id="idTxtFeAlta" placeholder="fecha de alta" name="txtFeAlta" AutoPostBack="True"></asp:TextBox>
                                                                                        
                                </div>
                                <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>-->                                          
                                <asp:CalendarExtender ID="ceFeNacimiento" Format="yyyy-MM-dd" TargetControlID="idTxtFeAlta" runat="server">
                                </asp:CalendarExtender> 
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>                                              
                </div>
            </div>
            <div class="form-group">                 
                     <div class="col-sm-3 selectContainer">                                                                                                      
                        <label for="estatusVenta" class="control-label">Estatus venta</label>
                        <asp:DropDownList  class="form-control" ID="ddlEstatusVenta" runat="server" AutoPostBack="True" name="status">
                                <asp:ListItem Value="">Seleccione Estatus</asp:ListItem>                                                           
                        </asp:DropDownList>  
                     </div>
                     <div class="col-sm-3 selectContainer">
                        <label for="estatus" class="control-label">Estatus Participante</label>
                        <asp:DropDownList  class="form-control" ID="ddlEstatusParticipante" runat="server" AutoPostBack="True" name="status">
                                <asp:ListItem Value="">Seleccione Estatus</asp:ListItem>                                                     
                        </asp:DropDownList>
                     </div>                
            </div>
            
            <div class="form-group">
               
                  <div class="col-sm-6">
                    <!--<button type="submit" class="btn btn-info pull-left">Buscar</button>-->
                       <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                       <ContentTemplate>
                      <dx:ASPxButton ID="btnBuscar" runat="server" Text="Buscar" class="btn btn-info pull-left" name="btnBuscar"  CssPostfix="&quot;btn btn-info pull-left" ClientInstanceName="btnBuscar" AutoPostBack="False" OnClick="btnBuscar_Click">
                      </dx:ASPxButton>
                             </ContentTemplate>
                    </asp:UpdatePanel>
                  </div>
                  
            </div>
            <br />
            <div class="col-xs-12 col-md-12" >
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                         <ContentTemplate>
                <div class="table-responsive gridCompetidores" id="gridCompetidores" runat="server">
                    <dx:ASPxGridView ID="gvCompetidores" runat="server" AutoGenerateColumns="False" ClientIDMode="AutoID" ClientInstanceName="gvCompetidores" KeyFieldName="idClienteContactoEvento" Visible="False" EmptyDataText="Ningun dato encontrado" CssClass="table" OnHtmlRowCreated="gvCompetidores_HtmlRowCreated" OnPageIndexChanged="gvCompetidores_PageIndexChanged" OnHtmlDataCellPrepared="gvCompetidores_HtmlDataCellPrepared">

                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Edit" FixedStyle="None" SortOrder="None" UnboundType="Bound" VisibleIndex="0" Name="edit">
                                <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
                                <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditCellStyle>
                                <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FilterCellStyle>
                                <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditFormCaptionStyle>
                                <DataItemTemplate>                                    
                                    <dx:ASPxButton ID="btnEditar" runat="server" class="btn btn-primary btn-xs glyphicon glyphicon-pencil" name="btnEditar"  CssPostfix="&quot;" ClientInstanceName="btnEditar" AutoPostBack="False" Height="23px" OnClick="btnEditar_Click">                                        
                                    </dx:ASPxButton>
                                </DataItemTemplate>
                                <EditItemTemplate>                                    
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FooterCellStyle>
                                <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </GroupFooterCellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Folio" FieldName="dsClaveVenta" FixedStyle="None" SortOrder="None" UnboundType="Bound" VisibleIndex="1" Name="folio">
                                
                                <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
                                
                                <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditCellStyle>
                                <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FilterCellStyle>
                                <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditFormCaptionStyle>
                                <DataItemTemplate>                                    
                                    <span class="btn btn-primary btn-xs glyphicon glyphicon-print">
                                        <dx:ASPxHyperLink ID="linkFolio" runat="server" ClientIDMode="AutoID" NavigateUrl='<%#string.Concat("https://www.xperienciasxcaret.mx/core/cupon/cuponnew.aspx?lan=ES_MX&id=", Eval("idVenta"),"&base=",ConfigurationManager.AppSettings["bdProduccion"].ToString())%> ' Text='<%# Eval("dsClaveVenta") %>' Target="_blank" Font-Underline="True" Font-Bold="True" ForeColor="White" />
                                    </span>
                                    
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FooterCellStyle>
                                <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </GroupFooterCellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Liga Pago" FixedStyle="None" SortOrder="None" UnboundType="Bound" VisibleIndex="2" Name="liga">
<PropertiesTextEdit><MaskSettings IncludeLiterals="All"></MaskSettings><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></InvalidStyle><ValidationSettings ErrorDisplayMode="ImageWithTooltip" Display="Static" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></HoverStyle><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></Style></PropertiesTextEdit>

<Settings AllowDragDrop="Default" FilterMode="Value" GroupInterval="Default" SortMode="Default" AllowAutoFilterTextInputTimer="Default" AllowAutoFilter="Default" ShowFilterRowMenu="Default" AllowHeaderFilter="Default" ShowInFilterControl="Default" AutoFilterCondition="Default" AllowSort="Default" AllowGroup="Default"></Settings>

<EditFormSettings Visible="Default" CaptionLocation="Default"></EditFormSettings>

<EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</EditCellStyle>

<FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</FilterCellStyle>

<EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</EditFormCaptionStyle>

                                <DataItemTemplate>
                                    <span class="btn btn-primary btn-xs">
                                        <dx:ASPxHyperLink ID="hlinkPago" runat="server" ClientIDMode="AutoID" NavigateUrl='' Text='Liga' Target="_blank" Font-Underline="True" Font-Bold="True" ForeColor="White" />
                                    </span>
                                </DataItemTemplate>

<HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>
</HeaderStyle>

<CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</CellStyle>

<FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</FooterCellStyle>

<GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</GroupFooterCellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Descripcion" FieldName="dsDescripcion" FixedStyle="None" SortOrder="None" UnboundType="Bound" VisibleIndex="3">
                                <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
                                <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditCellStyle>
                                <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FilterCellStyle>
                                <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditFormCaptionStyle>
                                <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FooterCellStyle>
                                <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </GroupFooterCellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="No. Canoero" FieldName="noCompetidor" FixedStyle="None" SortOrder="None" UnboundType="Bound" VisibleIndex="4" ShowInCustomizationForm="True">
                                
                                <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
                                
                                <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditCellStyle>
                                <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FilterCellStyle>
                                <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditFormCaptionStyle>
                                <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FooterCellStyle>
                                <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </GroupFooterCellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Estatus" FieldName="dsEstatusCliente" FixedStyle="None" SortOrder="None" UnboundType="Bound" VisibleIndex="5" ShowInCustomizationForm="True">
                                <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
                                <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditCellStyle>
                                <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FilterCellStyle>
                                <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditFormCaptionStyle>
                                <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FooterCellStyle>
                                <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </GroupFooterCellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Nombre" FieldName="dsContacto" FixedStyle="None" SortOrder="None" UnboundType="Bound" VisibleIndex="6">
                                <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
                                <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditCellStyle>
                                <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FilterCellStyle>
                                <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditFormCaptionStyle>
                                <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FooterCellStyle>
                                <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </GroupFooterCellStyle>
                            </dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="dsApellidoPaterno" UnboundType="Bound" SortOrder="None" ShowInCustomizationForm="True" FixedStyle="None" Caption="Apellido Paterno" VisibleIndex="7">
    <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
    <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
    <EditFormSettings CaptionLocation="Default" Visible="Default" />
    <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
        <BackgroundImage Repeat="Repeat" />
        <Border BorderStyle="NotSet" />
        <BorderLeft BorderStyle="NotSet" />
        <BorderTop BorderStyle="NotSet" />
        <BorderRight BorderStyle="NotSet" />
        <BorderBottom BorderStyle="NotSet" />
    </EditCellStyle>
    <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
        <BackgroundImage Repeat="Repeat" />
        <Border BorderStyle="NotSet" />
        <BorderLeft BorderStyle="NotSet" />
        <BorderTop BorderStyle="NotSet" />
        <BorderRight BorderStyle="NotSet" />
        <BorderBottom BorderStyle="NotSet" />
    </FilterCellStyle>
    <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
        <BackgroundImage Repeat="Repeat" />
        <Border BorderStyle="NotSet" />
        <BorderLeft BorderStyle="NotSet" />
        <BorderTop BorderStyle="NotSet" />
        <BorderRight BorderStyle="NotSet" />
        <BorderBottom BorderStyle="NotSet" />
    </EditFormCaptionStyle>
    <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat" />
    <Border BorderStyle="NotSet" />
    </HeaderStyle>
    <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
        <BackgroundImage Repeat="Repeat" />
        <Border BorderStyle="NotSet" />
        <BorderLeft BorderStyle="NotSet" />
        <BorderTop BorderStyle="NotSet" />
        <BorderRight BorderStyle="NotSet" />
        <BorderBottom BorderStyle="NotSet" />
    </CellStyle>
    <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
        <BackgroundImage Repeat="Repeat" />
        <Border BorderStyle="NotSet" />
        <BorderLeft BorderStyle="NotSet" />
        <BorderTop BorderStyle="NotSet" />
        <BorderRight BorderStyle="NotSet" />
        <BorderBottom BorderStyle="NotSet" />
    </FooterCellStyle>
    <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
        <BackgroundImage Repeat="Repeat" />
        <Border BorderStyle="NotSet" />
        <BorderLeft BorderStyle="NotSet" />
        <BorderTop BorderStyle="NotSet" />
        <BorderRight BorderStyle="NotSet" />
        <BorderBottom BorderStyle="NotSet" />
    </GroupFooterCellStyle>
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="dsApellidoMaterno" UnboundType="Bound" SortOrder="None" ShowInCustomizationForm="True" FixedStyle="None" Caption="Apellido Materno" VisibleIndex="8">
<PropertiesTextEdit><MaskSettings IncludeLiterals="All"></MaskSettings><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></InvalidStyle><ValidationSettings ErrorDisplayMode="ImageWithTooltip" Display="Static" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></HoverStyle><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat"></BackgroundImage><Border BorderStyle="NotSet"></Border><BorderLeft BorderStyle="NotSet"></BorderLeft><BorderTop BorderStyle="NotSet"></BorderTop><BorderRight BorderStyle="NotSet"></BorderRight><BorderBottom BorderStyle="NotSet"></BorderBottom></Style></PropertiesTextEdit>

<Settings AllowDragDrop="Default" FilterMode="Value" GroupInterval="Default" SortMode="Default" AllowAutoFilterTextInputTimer="Default" AllowAutoFilter="Default" ShowFilterRowMenu="Default" AllowHeaderFilter="Default" ShowInFilterControl="Default" AutoFilterCondition="Default" AllowSort="Default" AllowGroup="Default"></Settings>

<EditFormSettings Visible="Default" CaptionLocation="Default"></EditFormSettings>

<EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</EditCellStyle>

<FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</FilterCellStyle>

<EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</EditFormCaptionStyle>

<HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>
</HeaderStyle>

<CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</CellStyle>

<FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</FooterCellStyle>

<GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
<BackgroundImage Repeat="Repeat"></BackgroundImage>

<Border BorderStyle="NotSet"></Border>

<BorderLeft BorderStyle="NotSet"></BorderLeft>

<BorderTop BorderStyle="NotSet"></BorderTop>

<BorderRight BorderStyle="NotSet"></BorderRight>

<BorderBottom BorderStyle="NotSet"></BorderBottom>
</GroupFooterCellStyle>
</dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="fe. Nacimiento" FieldName="feNacimiento" FixedStyle="None" SortOrder="None" UnboundType="Bound" VisibleIndex="9">
                                <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
                                <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditCellStyle>
                                <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FilterCellStyle>
                                <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditFormCaptionStyle>
                                <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FooterCellStyle>
                                <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </GroupFooterCellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="categoria" FieldName="dsProducto" FixedStyle="None" SortOrder="None" UnboundType="Bound" VisibleIndex="10">
                                <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
                                <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditCellStyle>
                                <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FilterCellStyle>
                                <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditFormCaptionStyle>
                                <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FooterCellStyle>
                                <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </GroupFooterCellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="canal" FieldName="dsAgrupador" FixedStyle="None" SortOrder="None" UnboundType="Bound" VisibleIndex="11">
                                
                                <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
                                
                                <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditCellStyle>
                                <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FilterCellStyle>
                                <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditFormCaptionStyle>
                                <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FooterCellStyle>
                                <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </GroupFooterCellStyle>
                                
                                <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
                                
                                <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditCellStyle>
                                <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FilterCellStyle>
                                <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditFormCaptionStyle>
                                <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FooterCellStyle>
                                <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </GroupFooterCellStyle>
                                
                                <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
                                <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditCellStyle>
                                <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FilterCellStyle>
                                <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditFormCaptionStyle>
                                <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FooterCellStyle>
                                <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </GroupFooterCellStyle>
                                <PropertiesTextEdit><MaskSettings IncludeLiterals="All" /><MaskHintStyle Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></MaskHintStyle><NullTextStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></NullTextStyle><ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ReadOnlyStyle><FocusedStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></FocusedStyle><InvalidStyle HorizontalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></InvalidStyle><ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"><ErrorImage Align="NotSet"></ErrorImage><ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></HoverStyle><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></ErrorFrameStyle></ValidationSettings><Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default"><BackgroundImage Repeat="Repeat" /><Border BorderStyle="NotSet" /><BorderLeft BorderStyle="NotSet" /><BorderTop BorderStyle="NotSet" /><BorderRight BorderStyle="NotSet" /><BorderBottom BorderStyle="NotSet" /></Style></PropertiesTextEdit>
                                <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditCellStyle>
                                <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FilterCellStyle>
                                <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </EditFormCaptionStyle>
                                <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                <BackgroundImage Repeat="Repeat" />
                                <Border BorderStyle="NotSet" />
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </FooterCellStyle>
                                <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                    <BackgroundImage Repeat="Repeat" />
                                    <Border BorderStyle="NotSet" />
                                    <BorderLeft BorderStyle="NotSet" />
                                    <BorderTop BorderStyle="NotSet" />
                                    <BorderRight BorderStyle="NotSet" />
                                    <BorderBottom BorderStyle="NotSet" />
                                </GroupFooterCellStyle>
                            </dx:GridViewDataTextColumn>
                        </Columns>
    <SettingsBehavior ColumnResizeMode="Disabled" SortMode="Default"></SettingsBehavior>

    <SettingsPager Mode="ShowPager" Position="Bottom" SEOFriendly="Disabled" EllipsisMode="InsideNumeric" RenderMode="Classic" PageSize="24">
    <AllButton>
    <Image Align="NotSet"></Image>
    </AllButton>

    <FirstPageButton>
    <Image Align="NotSet"></Image>
    </FirstPageButton>

    <LastPageButton>
    <Image Align="NotSet"></Image>
    </LastPageButton>

    <NextPageButton>
    <Image Align="NotSet"></Image>
    </NextPageButton>

    <PrevPageButton>
    <Image Align="NotSet"></Image>
    </PrevPageButton>
    </SettingsPager>

    <SettingsEditing Mode="EditFormAndDisplayRow" NewItemRowPosition="Top" PopupEditFormHorizontalAlign="RightSides" PopupEditFormVerticalAlign="Below"></SettingsEditing>

    <Settings ShowGroupFooter="Hidden" VerticalScrollBarStyle="Standard" ShowStatusBar="Auto" ShowFilterBar="Hidden" GridLines="Both" VerticalScrollableHeight="400"></Settings>

    <SettingsCustomizationWindow PopupHorizontalAlign="RightSides" PopupVerticalAlign="BottomSides"></SettingsCustomizationWindow>

    <SettingsLoadingPanel Mode="ShowAsPopup" ImagePosition="Left"></SettingsLoadingPanel>

    <SettingsDetail ExportMode="None"></SettingsDetail>

    <Images>
    <LoadingPanelOnStatusBar Align="NotSet"></LoadingPanelOnStatusBar>

    <CollapsedButton Align="NotSet"></CollapsedButton>

    <ExpandedButton Align="NotSet"></ExpandedButton>

    <DetailCollapsedButton Align="NotSet"></DetailCollapsedButton>

    <DetailExpandedButton Align="NotSet"></DetailExpandedButton>

    <HeaderFilter Align="NotSet"></HeaderFilter>

    <HeaderActiveFilter Align="NotSet"></HeaderActiveFilter>

    <HeaderSortDown Align="NotSet"></HeaderSortDown>

    <HeaderSortUp Align="NotSet"></HeaderSortUp>

    <DragAndDropArrowDown Align="NotSet"></DragAndDropArrowDown>

    <DragAndDropArrowUp Align="NotSet"></DragAndDropArrowUp>

    <DragAndDropColumnHide Align="NotSet"></DragAndDropColumnHide>

    <ParentGroupRows Align="NotSet"></ParentGroupRows>

    <FilterRowButton Align="NotSet"></FilterRowButton>

    <CustomizationWindowClose Align="NotSet"></CustomizationWindowClose>

    <PopupEditFormWindowClose Align="NotSet"></PopupEditFormWindowClose>

    <WindowResizer Align="NotSet"></WindowResizer>

    <FilterBuilderClose Align="NotSet"></FilterBuilderClose>

    <LoadingPanel Align="NotSet"></LoadingPanel>
    </Images>

    <ImagesEditors>
    <CalendarPrevYear Align="NotSet"></CalendarPrevYear>

    <CalendarPrevMonth Align="NotSet"></CalendarPrevMonth>

    <CalendarNextMonth Align="NotSet"></CalendarNextMonth>

    <CalendarNextYear Align="NotSet"></CalendarNextYear>

    <CalendarFastNavPrevYear Align="NotSet"></CalendarFastNavPrevYear>

    <CalendarFastNavNextYear Align="NotSet"></CalendarFastNavNextYear>

    <CheckBoxChecked Align="NotSet"></CheckBoxChecked>

    <CheckBoxUnchecked Align="NotSet"></CheckBoxUnchecked>

    <CheckBoxUndefined Align="NotSet"></CheckBoxUndefined>

    <RadioButtonChecked Align="NotSet"></RadioButtonChecked>

    <RadioButtonUnchecked Align="NotSet"></RadioButtonUnchecked>

    <RadioButtonUndefined Align="NotSet"></RadioButtonUndefined>

    <ButtonEditEllipsis Align="NotSet"></ButtonEditEllipsis>

    <DropDownEditDropDown Align="NotSet"></DropDownEditDropDown>

    <ImageEmpty Align="NotSet"></ImageEmpty>

    <ListEditItem Align="NotSet"></ListEditItem>

    <SpinEditIncrement Align="NotSet"></SpinEditIncrement>

    <SpinEditDecrement Align="NotSet"></SpinEditDecrement>

    <SpinEditLargeIncrement Align="NotSet"></SpinEditLargeIncrement>

    <SpinEditLargeDecrement Align="NotSet"></SpinEditLargeDecrement>

    <LoadingPanel Align="NotSet"></LoadingPanel>
    </ImagesEditors>

    <ImagesFilterControl>
    <AddButton Align="NotSet"></AddButton>

    <AddButtonHot Align="NotSet"></AddButtonHot>

    <RemoveButton Align="NotSet"></RemoveButton>

    <RemoveButtonHot Align="NotSet"></RemoveButtonHot>

    <AddCondition Align="NotSet"></AddCondition>

    <AddGroup Align="NotSet"></AddGroup>

    <RemoveGroup Align="NotSet"></RemoveGroup>

    <GroupTypeAnd Align="NotSet"></GroupTypeAnd>

    <GroupTypeOr Align="NotSet"></GroupTypeOr>

    <GroupTypeNotAnd Align="NotSet"></GroupTypeNotAnd>

    <GroupTypeNotOr Align="NotSet"></GroupTypeNotOr>

    <OperationAnyOf Align="NotSet"></OperationAnyOf>

    <OperationBeginsWith Align="NotSet"></OperationBeginsWith>

    <OperationBetween Align="NotSet"></OperationBetween>

    <OperationContains Align="NotSet"></OperationContains>

    <OperationDoesNotContain Align="NotSet"></OperationDoesNotContain>

    <OperationDoesNotEqual Align="NotSet"></OperationDoesNotEqual>

    <OperationEndsWith Align="NotSet"></OperationEndsWith>

    <OperationEquals Align="NotSet"></OperationEquals>

    <OperationGreater Align="NotSet"></OperationGreater>

    <OperationGreaterOrEqual Align="NotSet"></OperationGreaterOrEqual>

    <OperationIsNotNull Align="NotSet"></OperationIsNotNull>

    <OperationIsNull Align="NotSet"></OperationIsNull>

    <OperationLess Align="NotSet"></OperationLess>

    <OperationLessOrEqual Align="NotSet"></OperationLessOrEqual>

    <OperationLike Align="NotSet"></OperationLike>

    <OperationNoneOf Align="NotSet"></OperationNoneOf>

    <OperationNotBetween Align="NotSet"></OperationNotBetween>

    <OperationNotLike Align="NotSet"></OperationNotLike>

    <LoadingPanel Align="NotSet"></LoadingPanel>
    </ImagesFilterControl>

    <Styles>
    <Disabled>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Disabled>

    <Customization HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Customization>

    <PopupEditForm HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PopupEditForm>

    <Table HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Table>

    <Header HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>
    </Header>

    <GroupRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </GroupRow>

    <FocusedGroupRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </FocusedGroupRow>

    <Row HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </Row>

    <RowHotTrack HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </RowHotTrack>

    <DetailRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </DetailRow>

    <DetailCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DetailCell>

    <PreviewRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </PreviewRow>

    <EmptyDataRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </EmptyDataRow>

    <AlternatingRow Enabled="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </AlternatingRow>

    <SelectedRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </SelectedRow>

    <FocusedRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </FocusedRow>

    <FilterRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </FilterRow>

    <Cell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Cell>

    <Footer HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Footer>

    <GroupFooter HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </GroupFooter>

    <GroupPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </GroupPanel>

    <HeaderPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HeaderPanel>

    <PagerTopPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PagerTopPanel>

    <PagerBottomPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PagerBottomPanel>

    <DetailButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DetailButton>

    <CustomizationWindow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CustomizationWindow>

    <CustomizationWindowCloseButton Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CustomizationWindowCloseButton>

    <CustomizationWindowContent HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CustomizationWindowContent>

    <CustomizationWindowHeader HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CustomizationWindowHeader>

    <PopupEditFormWindow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PopupEditFormWindow>

    <PopupEditFormWindowCloseButton Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PopupEditFormWindowCloseButton>

    <PopupEditFormWindowContent HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PopupEditFormWindowContent>

    <PopupEditFormWindowHeader HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PopupEditFormWindowHeader>

    <FilterBuilderCloseButton Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterBuilderCloseButton>

    <FilterBuilderHeader HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterBuilderHeader>

    <FilterBuilderMainArea HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterBuilderMainArea>

    <FilterBuilderButtonArea HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterBuilderButtonArea>

    <LoadingPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </LoadingPanel>

    <LoadingDiv>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </LoadingDiv>

    <CommandColumn HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CommandColumn>

    <CommandColumnItem HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CommandColumnItem>

    <InlineEditCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </InlineEditCell>

    <FilterCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterCell>

    <InlineEditRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </InlineEditRow>

    <EditFormDisplayRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </EditFormDisplayRow>

    <EditingErrorRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>
    </EditingErrorRow>

    <EditForm HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </EditForm>

    <EditFormCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </EditFormCell>

    <EditFormTable HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </EditFormTable>

    <EditFormColumnCaption HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </EditFormColumnCaption>

    <TitlePanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </TitlePanel>

    <StatusBar HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </StatusBar>

    <FilterBar HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterBar>

    <FilterBarLink HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterBarLink>

    <FilterBarCheckBoxCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterBarCheckBoxCell>

    <FilterBarImageCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterBarImageCell>

    <FilterBarExpressionCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterBarExpressionCell>

    <FilterBarClearButtonCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterBarClearButtonCell>

    <FilterPopupWindow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterPopupWindow>

    <FilterPopupItemsArea HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterPopupItemsArea>

    <FilterPopupButtonPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterPopupButtonPanel>

    <FilterPopupItem HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterPopupItem>

    <FilterPopupActiveItem HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterPopupActiveItem>

    <FilterPopupSelectedItem HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterPopupSelectedItem>

    <FilterRowMenu HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <GutterBackgroundImage Repeat="Repeat"></GutterBackgroundImage>

    <SeparatorBackgroundImage Repeat="Repeat"></SeparatorBackgroundImage>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterRowMenu>

    <FilterRowMenuItem HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <DropDownButtonStyle>
    <CheckedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CheckedStyle>

    <SelectedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </SelectedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DropDownButtonStyle>

    <CheckedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CheckedStyle>

    <SelectedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </SelectedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </FilterRowMenuItem>
    </Styles>

    <StylesPager>
    <Button HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Button>

    <DisabledButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DisabledButton>

    <CurrentPageNumber HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CurrentPageNumber>

    <PageNumber HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PageNumber>

    <Summary HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Summary>

    <Ellipsis HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Ellipsis>
    </StylesPager>

    <StylesEditors>
    <Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Style>

    <ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </ReadOnlyStyle>

    <ReadOnly HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </ReadOnly>

    <CheckEdit HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CheckEdit>

    <ListBox HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </ListBox>

    <ListBoxItem HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <SelectedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </SelectedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </ListBoxItem>

    <RadioButtonList HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </RadioButtonList>

    <IncrementButtonStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <DisabledStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DisabledStyle>

    <PressedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PressedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </IncrementButtonStyle>

    <SpinEditIncrementButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <DisabledStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DisabledStyle>

    <PressedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PressedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </SpinEditIncrementButton>

    <DecrementButtonStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <DisabledStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DisabledStyle>

    <PressedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PressedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DecrementButtonStyle>

    <SpinEditDecrementButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <DisabledStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DisabledStyle>

    <PressedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PressedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </SpinEditDecrementButton>

    <LargeIncrementButtonStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <DisabledStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DisabledStyle>

    <PressedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PressedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </LargeIncrementButtonStyle>

    <SpinEditLargeIncrementButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <DisabledStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DisabledStyle>

    <PressedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PressedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </SpinEditLargeIncrementButton>

    <LargeDecrementButtonStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <DisabledStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DisabledStyle>

    <PressedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PressedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </LargeDecrementButtonStyle>

    <SpinEditLargeDecrementButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <DisabledStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DisabledStyle>

    <PressedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PressedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </SpinEditLargeDecrementButton>

    <Label HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Label>

    <Hyperlink HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Hyperlink>

    <Image HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Image>

    <Memo HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Memo>

    <TextBox HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </TextBox>

    <ButtonEdit HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </ButtonEdit>

    <ButtonEditButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <DisabledStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DisabledStyle>

    <PressedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PressedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </ButtonEditButton>

    <Calendar HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Calendar>

    <CalendarDayHeader Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarDayHeader>

    <CalendarWeekNumber Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarWeekNumber>

    <CalendarDay Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarDay>

    <CalendarDayOtherMonth Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarDayOtherMonth>

    <CalendarDaySelected Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarDaySelected>

    <CalendarDayWeekEnd Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarDayWeekEnd>

    <CalendarDayOutOfRange Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarDayOutOfRange>

    <CalendarToday Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarToday>

    <CalendarHeader HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarHeader>

    <CalendarFooter HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarFooter>

    <CalendarButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <DisabledStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DisabledStyle>

    <PressedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PressedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarButton>

    <CalendarFastNav Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarFastNav>

    <CalendarFastNavMonthArea Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarFastNavMonthArea>

    <CalendarFastNavYearArea Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarFastNavYearArea>

    <CalendarFastNavMonth Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <SelectedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </SelectedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarFastNavMonth>

    <CalendarFastNavYear Wrap="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet">
    <SelectedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </SelectedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarFastNavYear>

    <CalendarFastNavFooter HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </CalendarFastNavFooter>

    <MaskHint Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </MaskHint>

    <ProgressBar HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </ProgressBar>

    <ProgressBarIndicator>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </ProgressBarIndicator>

    <DropDownWindow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DropDownWindow>

    <ColorTable>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </ColorTable>

    <ColorTableCell>
    <ColorBorder BorderStyle="NotSet"></ColorBorder>

    <ColorBorderLeft BorderStyle="NotSet"></ColorBorderLeft>

    <ColorBorderTop BorderStyle="NotSet"></ColorBorderTop>

    <ColorBorderRight BorderStyle="NotSet"></ColorBorderRight>

    <ColorBorderBottom BorderStyle="NotSet"></ColorBorderBottom>

    <SelectedStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </SelectedStyle>

    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </ColorTableCell>

    <ColorIndicator>
    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </ColorIndicator>

    <DisplayColorIndicator>
    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </DisplayColorIndicator>

    <Focused HorizontalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Focused>

    <NullText HorizontalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </NullText>

    <Invalid HorizontalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Invalid>

    <LoadingPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </LoadingPanel>

    <LoadingDiv>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </LoadingDiv>
    </StylesEditors>

    <StylesFilterControl>
    <Table HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Table>

    <PropertyName HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </PropertyName>

    <GroupType HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </GroupType>

    <Operation HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Operation>

    <Value HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </Value>

    <ImageButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
    <HoverStyle>
    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </HoverStyle>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>
    </ImageButton>
    </StylesFilterControl>

    <BackgroundImage Repeat="Repeat"></BackgroundImage>

    <Border BorderStyle="NotSet"></Border>

    <BorderLeft BorderStyle="NotSet"></BorderLeft>

    <BorderTop BorderStyle="NotSet"></BorderTop>

    <BorderRight BorderStyle="NotSet"></BorderRight>

    <BorderBottom BorderStyle="NotSet"></BorderBottom>

                    </dx:ASPxGridView>
                </div>
                             </ContentTemplate>
                    </asp:UpdatePanel>
            </div>
                </ContentTemplate>
            </asp:UpdatePanel>
           </div>
        </form>
       
    </div> <!-- div contairner-->
</asp:Content>
