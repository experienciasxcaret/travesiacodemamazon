﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="buscaPreinscritos.aspx.cs" Inherits="travesia.buscaPreinscritos" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" namespace="DevExpress.Web.ASPxLoadingPanel" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width= device-width, initial-scale=1.0" /> 
    <title>Triatlon Xel-Ha</title>
    <link rel="stylesheet" href="styles/bootstrap.css" />
    <link rel="stylesheet" href="styles/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="styles/triatlon.css" />
    <link rel="stylesheet" href="styles/basic.css" />
    <link rel="stylesheet" href="styles/detallePago.css" />

    <script src="Scripts/jquery-1.11.3.js" ></script>
    <script src="Scripts/bootstrap.js" ></script>
    <script src="Scripts/inscripcionRifa.js" ></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.metadata.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/localization/messages_es.js"></script>
    <link rel="shortcut icon" type="image/png" href="favicon.png" />
    <script type="text/javascript">

        
        $(document).ready(function () {

            Sys.Browser.WebKit = {}; //Safari 3 is considered WebKit

            if (navigator.userAgent.indexOf('WebKit/') > -1) {
                
                Sys.Browser.agent = Sys.Browser.Firefox;

                Sys.Browser.version = parseFloat(navigator.userAgent.match(/WebKit\/(\d+(\.\d+)?)/)[1]);

                Sys.Browser.name = 'Firefox';
            }
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(initializeRequest);
            prm.add_pageLoaded(pageLoaded);
            function initializeRequest(sender, args) {
                //LoadingPanel.ShowInElement(args._postBackElement);  
                document.body.style.cursor = "wait";                
                pnlinfoproceso.Show();

            }
            function pageLoaded(sender, args) {
                var panels = args.get_panelsUpdated();
                if (panels.length > 0) {
                    document.body.style.cursor = "default";
                    pnlinfoproceso.Hide();
                }
            }            
        })
    </script>
</head>
<body>    
    <div class="container">
       <div class="cont header b-bottom">
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			    <object data="img/xelha.svg" type="image/svg+xml" class="img1 hidden-xs"></object>
		    </div>
		    <div class="logo col-xs-8 col-sm-4" align="center">
			    <object data="img/triatlon.svg" type="image/svg+xml" class="img2"></object>
		    </div>
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			    <object data="img/triatlon_fechas.png" type="image/svg+xml" class="img3 hidden-xs"></object>
		    </div>
       </div>
       <div class="grid_16 wrap">
            <div id="cont_formularios" class="cont" runat="server">
                <form class="form-horizontal" id="formRifa" runat="server">
                    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
                    <br />
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <dx:ASPxLabel ID="lblMensajeCabecera" runat="server" Text="Buscador de Competidores" Font-Size="Large" ForeColor="Black" ClientInstanceName="lblMensajeCabecera"></dx:ASPxLabel>                   
                        </div>
                    </div>
                    <div id="form_paso_a">
                        <fieldset id="info_personal">
                        <br />
                        <div class="row">
                            <div class="form-group" style="margin:5px">
                                    <div class="col-sm-3">
                                        <label>Apellido Paterno</label>
                                         <asp:TextBox runat="server" class="{required:true} form-control"  id="txtPaterno" name="txtPaterno" placeholder="last name">
                                         </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPaterno" runat="server" ErrorMessage="*Requerido" ControlToValidate="txtPaterno" ValidationGroup="validacionBusqueda" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Apellido Materno</label>                        
                                        <asp:TextBox runat="server" class="form-control"  id="txtMaterno" placeholder="Middle name" name="txtMaterno">
                                        </asp:TextBox>
                                    </div>
                            </div>
                        </div>
                        
                       <div class="row">
                            <div class="form-group" style="margin:5px">               
                                  <div class="col-sm-2 col-lg-2">
                                    <!--<button type="submit" class="btn btn-info pull-left">Buscar</button>-->
                                       <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                       <ContentTemplate>
                                      <dx:ASPxButton ID="btnBuscar" runat="server" Text="Buscar" class="btn btn-info pull-left" name="btnBuscar"  CssPostfix="&quot;btn btn-info pull-left" ValidationGroup="validacionBusqueda"  UseSubmitBehavior="False" ClientInstanceName="btnBuscar" AutoPostBack="False" OnClick="btnBuscar_Click">
                                      </dx:ASPxButton>
                                             </ContentTemplate>
                                    </asp:UpdatePanel>
                                  </div>
                                  <div class="col-sm-10 col-lg-10">
                                      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                             <dx:ASPxLabel ID="lblMensajeRespuesta" runat="server" Text="" Font-Size="Medium" ForeColor="#CC0000"></dx:ASPxLabel>
                                            </ContentTemplate>
                                      </asp:UpdatePanel>
                                  </div>                
                            </div> 
                         </div>
                         <br />
                         <div class="row">
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                 <ContentTemplate>
                                    <div class="col-sm-1 col-lg-1"></div>
                                    <div class="col-sm-8 col-lg-8">
                                        <dx:ASPxGridView ID="gvCompetidoresPre" runat="server" AutoGenerateColumns="False" ClientIDMode="AutoID" KeyFieldName="dsLiga">

                                            <Columns>
                                                <dx:GridViewDataTextColumn FixedStyle="None" SortOrder="None" UnboundType="Bound" VisibleIndex="0">
                                                    <PropertiesTextEdit>
                                                        <MaskSettings IncludeLiterals="All" />
                                                        <MaskHintStyle Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </MaskHintStyle>
                                                        <NullTextStyle HorizontalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </NullTextStyle>
                                                        <ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </ReadOnlyStyle>
                                                        <FocusedStyle HorizontalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </FocusedStyle>
                                                        <InvalidStyle HorizontalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </InvalidStyle>
                                                        <ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right">
                                                            <ErrorImage Align="NotSet">
                                                            </ErrorImage>
                                                            <ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                                <HoverStyle>
                                                                    <BackgroundImage Repeat="Repeat" />
                                                                    <Border BorderStyle="NotSet" />
                                                                    <BorderLeft BorderStyle="NotSet" />
                                                                    <BorderTop BorderStyle="NotSet" />
                                                                    <BorderRight BorderStyle="NotSet" />
                                                                    <BorderBottom BorderStyle="NotSet" />
                                                                </HoverStyle>
                                                                <BackgroundImage Repeat="Repeat" />
                                                                <Border BorderStyle="NotSet" />
                                                                <BorderLeft BorderStyle="NotSet" />
                                                                <BorderTop BorderStyle="NotSet" />
                                                                <BorderRight BorderStyle="NotSet" />
                                                                <BorderBottom BorderStyle="NotSet" />
                                                            </ErrorFrameStyle>
                                                        </ValidationSettings>
                                                        <Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </Style>
                                                    </PropertiesTextEdit>
                                                    <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                                    <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                                    <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </EditCellStyle>
                                                    <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </FilterCellStyle>
                                                    <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </EditFormCaptionStyle>
                                                    <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    </HeaderStyle>
                                                    <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </CellStyle>
                                                    <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </FooterCellStyle>
                                                    <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </GroupFooterCellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Nombre" FieldName="dsNombre" FixedStyle="None" Name="Nombre" SortOrder="None" UnboundType="Bound" VisibleIndex="1">
                                                    <PropertiesTextEdit>
                                                        <MaskSettings IncludeLiterals="All" />
                                                        <MaskHintStyle Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </MaskHintStyle>
                                                        <NullTextStyle HorizontalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </NullTextStyle>
                                                        <ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </ReadOnlyStyle>
                                                        <FocusedStyle HorizontalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </FocusedStyle>
                                                        <InvalidStyle HorizontalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </InvalidStyle>
                                                        <ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right">
                                                            <ErrorImage Align="NotSet">
                                                            </ErrorImage>
                                                            <ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                                <HoverStyle>
                                                                    <BackgroundImage Repeat="Repeat" />
                                                                    <Border BorderStyle="NotSet" />
                                                                    <BorderLeft BorderStyle="NotSet" />
                                                                    <BorderTop BorderStyle="NotSet" />
                                                                    <BorderRight BorderStyle="NotSet" />
                                                                    <BorderBottom BorderStyle="NotSet" />
                                                                </HoverStyle>
                                                                <BackgroundImage Repeat="Repeat" />
                                                                <Border BorderStyle="NotSet" />
                                                                <BorderLeft BorderStyle="NotSet" />
                                                                <BorderTop BorderStyle="NotSet" />
                                                                <BorderRight BorderStyle="NotSet" />
                                                                <BorderBottom BorderStyle="NotSet" />
                                                            </ErrorFrameStyle>
                                                        </ValidationSettings>
                                                        <Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </Style>
                                                    </PropertiesTextEdit>
                                                    <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                                    <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                                    <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </EditCellStyle>
                                                    <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </FilterCellStyle>
                                                    <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </EditFormCaptionStyle>
                                                    <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    </HeaderStyle>
                                                    <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </CellStyle>
                                                    <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </FooterCellStyle>
                                                    <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </GroupFooterCellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="link inscripcion" FixedStyle="None" Name="lnkLiga" SortOrder="None" UnboundType="Bound" VisibleIndex="2">
                                                    <PropertiesTextEdit>
                                                        <MaskSettings IncludeLiterals="All" />
                                                        <MaskHintStyle Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </MaskHintStyle>
                                                        <NullTextStyle HorizontalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </NullTextStyle>
                                                        <ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </ReadOnlyStyle>
                                                        <FocusedStyle HorizontalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </FocusedStyle>
                                                        <InvalidStyle HorizontalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </InvalidStyle>
                                                        <ValidationSettings Display="Static" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right">
                                                            <ErrorImage Align="NotSet">
                                                            </ErrorImage>
                                                            <ErrorFrameStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                                <HoverStyle>
                                                                    <BackgroundImage Repeat="Repeat" />
                                                                    <Border BorderStyle="NotSet" />
                                                                    <BorderLeft BorderStyle="NotSet" />
                                                                    <BorderTop BorderStyle="NotSet" />
                                                                    <BorderRight BorderStyle="NotSet" />
                                                                    <BorderBottom BorderStyle="NotSet" />
                                                                </HoverStyle>
                                                                <BackgroundImage Repeat="Repeat" />
                                                                <Border BorderStyle="NotSet" />
                                                                <BorderLeft BorderStyle="NotSet" />
                                                                <BorderTop BorderStyle="NotSet" />
                                                                <BorderRight BorderStyle="NotSet" />
                                                                <BorderBottom BorderStyle="NotSet" />
                                                            </ErrorFrameStyle>
                                                        </ValidationSettings>
                                                        <Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </Style>
                                                    </PropertiesTextEdit>
                                                    <Settings AllowAutoFilter="Default" AllowAutoFilterTextInputTimer="Default" AllowDragDrop="Default" AllowGroup="Default" AllowHeaderFilter="Default" AllowSort="Default" AutoFilterCondition="Default" FilterMode="Value" GroupInterval="Default" ShowFilterRowMenu="Default" ShowInFilterControl="Default" SortMode="Default" />
                                                    <EditFormSettings CaptionLocation="Default" Visible="Default" />
                                                    <EditCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </EditCellStyle>
                                                    <FilterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </FilterCellStyle>
                                                    <EditFormCaptionStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </EditFormCaptionStyle>
                                                    <DataItemTemplate>
                                                        
                                                        <dx:ASPxButton ID="btnLink" runat="server" Text="Click para inscribir" class="btn btn-success pull-left" CssPostfix="&quot;btn btn-success pull-left" OnClick="btnLink_Click"></dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    </HeaderStyle>
                                                    <CellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </CellStyle>
                                                    <FooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </FooterCellStyle>
                                                    <GroupFooterCellStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </GroupFooterCellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <SettingsBehavior ColumnResizeMode="Disabled" SortMode="Default" />
                                            <SettingsPager EllipsisMode="InsideNumeric" Mode="ShowPager" Position="Bottom" RenderMode="Classic" SEOFriendly="Disabled">
                                                <AllButton>
                                                    <Image Align="NotSet">
                                                    </Image>
                                                </AllButton>
                                                <FirstPageButton>
                                                    <Image Align="NotSet">
                                                    </Image>
                                                </FirstPageButton>
                                                <LastPageButton>
                                                    <Image Align="NotSet">
                                                    </Image>
                                                </LastPageButton>
                                                <NextPageButton>
                                                    <Image Align="NotSet">
                                                    </Image>
                                                </NextPageButton>
                                                <PrevPageButton>
                                                    <Image Align="NotSet">
                                                    </Image>
                                                </PrevPageButton>
                                            </SettingsPager>
                                            <SettingsEditing Mode="EditFormAndDisplayRow" NewItemRowPosition="Top" PopupEditFormHorizontalAlign="RightSides" PopupEditFormVerticalAlign="Below" />
                                            <Settings GridLines="Both" ShowFilterBar="Hidden" ShowGroupFooter="Hidden" ShowStatusBar="Auto" VerticalScrollBarStyle="Standard" />
                                            <SettingsCustomizationWindow PopupHorizontalAlign="RightSides" PopupVerticalAlign="BottomSides" />
                                            <SettingsLoadingPanel ImagePosition="Left" Mode="ShowAsPopup" />
                                            <SettingsDetail ExportMode="None" />
                                            <Images>
                                                <LoadingPanelOnStatusBar Align="NotSet">
                                                </LoadingPanelOnStatusBar>
                                                <CollapsedButton Align="NotSet">
                                                </CollapsedButton>
                                                <ExpandedButton Align="NotSet">
                                                </ExpandedButton>
                                                <DetailCollapsedButton Align="NotSet">
                                                </DetailCollapsedButton>
                                                <DetailExpandedButton Align="NotSet">
                                                </DetailExpandedButton>
                                                <HeaderFilter Align="NotSet">
                                                </HeaderFilter>
                                                <HeaderActiveFilter Align="NotSet">
                                                </HeaderActiveFilter>
                                                <HeaderSortDown Align="NotSet">
                                                </HeaderSortDown>
                                                <HeaderSortUp Align="NotSet">
                                                </HeaderSortUp>
                                                <DragAndDropArrowDown Align="NotSet">
                                                </DragAndDropArrowDown>
                                                <DragAndDropArrowUp Align="NotSet">
                                                </DragAndDropArrowUp>
                                                <DragAndDropColumnHide Align="NotSet">
                                                </DragAndDropColumnHide>
                                                <ParentGroupRows Align="NotSet">
                                                </ParentGroupRows>
                                                <FilterRowButton Align="NotSet">
                                                </FilterRowButton>
                                                <CustomizationWindowClose Align="NotSet">
                                                </CustomizationWindowClose>
                                                <PopupEditFormWindowClose Align="NotSet">
                                                </PopupEditFormWindowClose>
                                                <WindowResizer Align="NotSet">
                                                </WindowResizer>
                                                <FilterBuilderClose Align="NotSet">
                                                </FilterBuilderClose>
                                                <LoadingPanel Align="NotSet">
                                                </LoadingPanel>
                                            </Images>
                                            <ImagesEditors>
                                                <CalendarPrevYear Align="NotSet">
                                                </CalendarPrevYear>
                                                <CalendarPrevMonth Align="NotSet">
                                                </CalendarPrevMonth>
                                                <CalendarNextMonth Align="NotSet">
                                                </CalendarNextMonth>
                                                <CalendarNextYear Align="NotSet">
                                                </CalendarNextYear>
                                                <CalendarFastNavPrevYear Align="NotSet">
                                                </CalendarFastNavPrevYear>
                                                <CalendarFastNavNextYear Align="NotSet">
                                                </CalendarFastNavNextYear>
                                                <CheckBoxChecked Align="NotSet">
                                                </CheckBoxChecked>
                                                <CheckBoxUnchecked Align="NotSet">
                                                </CheckBoxUnchecked>
                                                <CheckBoxUndefined Align="NotSet">
                                                </CheckBoxUndefined>
                                                <RadioButtonChecked Align="NotSet">
                                                </RadioButtonChecked>
                                                <RadioButtonUnchecked Align="NotSet">
                                                </RadioButtonUnchecked>
                                                <RadioButtonUndefined Align="NotSet">
                                                </RadioButtonUndefined>
                                                <ButtonEditEllipsis Align="NotSet">
                                                </ButtonEditEllipsis>
                                                <DropDownEditDropDown Align="NotSet">
                                                </DropDownEditDropDown>
                                                <ImageEmpty Align="NotSet">
                                                </ImageEmpty>
                                                <ListEditItem Align="NotSet">
                                                </ListEditItem>
                                                <SpinEditIncrement Align="NotSet">
                                                </SpinEditIncrement>
                                                <SpinEditDecrement Align="NotSet">
                                                </SpinEditDecrement>
                                                <SpinEditLargeIncrement Align="NotSet">
                                                </SpinEditLargeIncrement>
                                                <SpinEditLargeDecrement Align="NotSet">
                                                </SpinEditLargeDecrement>
                                                <LoadingPanel Align="NotSet">
                                                </LoadingPanel>
                                            </ImagesEditors>
                                            <ImagesFilterControl>
                                                <AddButton Align="NotSet">
                                                </AddButton>
                                                <AddButtonHot Align="NotSet">
                                                </AddButtonHot>
                                                <RemoveButton Align="NotSet">
                                                </RemoveButton>
                                                <RemoveButtonHot Align="NotSet">
                                                </RemoveButtonHot>
                                                <AddCondition Align="NotSet">
                                                </AddCondition>
                                                <AddGroup Align="NotSet">
                                                </AddGroup>
                                                <RemoveGroup Align="NotSet">
                                                </RemoveGroup>
                                                <GroupTypeAnd Align="NotSet">
                                                </GroupTypeAnd>
                                                <GroupTypeOr Align="NotSet">
                                                </GroupTypeOr>
                                                <GroupTypeNotAnd Align="NotSet">
                                                </GroupTypeNotAnd>
                                                <GroupTypeNotOr Align="NotSet">
                                                </GroupTypeNotOr>
                                                <OperationAnyOf Align="NotSet">
                                                </OperationAnyOf>
                                                <OperationBeginsWith Align="NotSet">
                                                </OperationBeginsWith>
                                                <OperationBetween Align="NotSet">
                                                </OperationBetween>
                                                <OperationContains Align="NotSet">
                                                </OperationContains>
                                                <OperationDoesNotContain Align="NotSet">
                                                </OperationDoesNotContain>
                                                <OperationDoesNotEqual Align="NotSet">
                                                </OperationDoesNotEqual>
                                                <OperationEndsWith Align="NotSet">
                                                </OperationEndsWith>
                                                <OperationEquals Align="NotSet">
                                                </OperationEquals>
                                                <OperationGreater Align="NotSet">
                                                </OperationGreater>
                                                <OperationGreaterOrEqual Align="NotSet">
                                                </OperationGreaterOrEqual>
                                                <OperationIsNotNull Align="NotSet">
                                                </OperationIsNotNull>
                                                <OperationIsNull Align="NotSet">
                                                </OperationIsNull>
                                                <OperationLess Align="NotSet">
                                                </OperationLess>
                                                <OperationLessOrEqual Align="NotSet">
                                                </OperationLessOrEqual>
                                                <OperationLike Align="NotSet">
                                                </OperationLike>
                                                <OperationNoneOf Align="NotSet">
                                                </OperationNoneOf>
                                                <OperationNotBetween Align="NotSet">
                                                </OperationNotBetween>
                                                <OperationNotLike Align="NotSet">
                                                </OperationNotLike>
                                                <LoadingPanel Align="NotSet">
                                                </LoadingPanel>
                                            </ImagesFilterControl>
                                            <Styles>
                                                <Disabled>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Disabled>
                                                <Customization HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Customization>
                                                <PopupEditForm HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </PopupEditForm>
                                                <Table HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Table>
                                                <Header HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                </Header>
                                                <GroupRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </GroupRow>
                                                <FocusedGroupRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </FocusedGroupRow>
                                                <Row HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </Row>
                                                <RowHotTrack HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </RowHotTrack>
                                                <DetailRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </DetailRow>
                                                <DetailCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </DetailCell>
                                                <PreviewRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </PreviewRow>
                                                <EmptyDataRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </EmptyDataRow>
                                                <AlternatingRow Enabled="Default" HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </AlternatingRow>
                                                <SelectedRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </SelectedRow>
                                                <FocusedRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </FocusedRow>
                                                <FilterRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </FilterRow>
                                                <Cell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Cell>
                                                <Footer HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Footer>
                                                <GroupFooter HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </GroupFooter>
                                                <GroupPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </GroupPanel>
                                                <HeaderPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </HeaderPanel>
                                                <PagerTopPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </PagerTopPanel>
                                                <PagerBottomPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </PagerBottomPanel>
                                                <DetailButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </DetailButton>
                                                <CustomizationWindow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CustomizationWindow>
                                                <CustomizationWindowCloseButton Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CustomizationWindowCloseButton>
                                                <CustomizationWindowContent HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CustomizationWindowContent>
                                                <CustomizationWindowHeader HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CustomizationWindowHeader>
                                                <PopupEditFormWindow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </PopupEditFormWindow>
                                                <PopupEditFormWindowCloseButton Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </PopupEditFormWindowCloseButton>
                                                <PopupEditFormWindowContent HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </PopupEditFormWindowContent>
                                                <PopupEditFormWindowHeader HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </PopupEditFormWindowHeader>
                                                <FilterBuilderCloseButton Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterBuilderCloseButton>
                                                <FilterBuilderHeader HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterBuilderHeader>
                                                <FilterBuilderMainArea HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterBuilderMainArea>
                                                <FilterBuilderButtonArea HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterBuilderButtonArea>
                                                <LoadingPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </LoadingPanel>
                                                <LoadingDiv>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </LoadingDiv>
                                                <CommandColumn HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CommandColumn>
                                                <CommandColumnItem HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CommandColumnItem>
                                                <InlineEditCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </InlineEditCell>
                                                <FilterCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterCell>
                                                <InlineEditRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </InlineEditRow>
                                                <EditFormDisplayRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </EditFormDisplayRow>
                                                <EditingErrorRow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                </EditingErrorRow>
                                                <EditForm HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </EditForm>
                                                <EditFormCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </EditFormCell>
                                                <EditFormTable HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </EditFormTable>
                                                <EditFormColumnCaption HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </EditFormColumnCaption>
                                                <TitlePanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </TitlePanel>
                                                <StatusBar HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </StatusBar>
                                                <FilterBar HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterBar>
                                                <FilterBarLink HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterBarLink>
                                                <FilterBarCheckBoxCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterBarCheckBoxCell>
                                                <FilterBarImageCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterBarImageCell>
                                                <FilterBarExpressionCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterBarExpressionCell>
                                                <FilterBarClearButtonCell HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterBarClearButtonCell>
                                                <FilterPopupWindow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterPopupWindow>
                                                <FilterPopupItemsArea HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterPopupItemsArea>
                                                <FilterPopupButtonPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterPopupButtonPanel>
                                                <FilterPopupItem HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterPopupItem>
                                                <FilterPopupActiveItem HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterPopupActiveItem>
                                                <FilterPopupSelectedItem HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterPopupSelectedItem>
                                                <FilterRowMenu HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <GutterBackgroundImage Repeat="Repeat" />
                                                    <SeparatorBackgroundImage Repeat="Repeat" />
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterRowMenu>
                                                <FilterRowMenuItem HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <DropDownButtonStyle>
                                                        <CheckedStyle>
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </CheckedStyle>
                                                        <SelectedStyle>
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </SelectedStyle>
                                                        <HoverStyle>
                                                            <BackgroundImage Repeat="Repeat" />
                                                            <Border BorderStyle="NotSet" />
                                                            <BorderLeft BorderStyle="NotSet" />
                                                            <BorderTop BorderStyle="NotSet" />
                                                            <BorderRight BorderStyle="NotSet" />
                                                            <BorderBottom BorderStyle="NotSet" />
                                                        </HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </DropDownButtonStyle>
                                                    <CheckedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </CheckedStyle>
                                                    <SelectedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </SelectedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </FilterRowMenuItem>
                                            </Styles>
                                            <StylesPager>
                                                <Button HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Button>
                                                <DisabledButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </DisabledButton>
                                                <CurrentPageNumber HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CurrentPageNumber>
                                                <PageNumber HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </PageNumber>
                                                <Summary HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Summary>
                                                <Ellipsis HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Ellipsis>
                                            </StylesPager>
                                            <StylesEditors>
                                                <Style HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Style>
                                                <ReadOnlyStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </ReadOnlyStyle>
                                                <ReadOnly HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </ReadOnly>
                                                <CheckEdit HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CheckEdit>
                                                <ListBox HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </ListBox>
                                                <ListBoxItem HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <SelectedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </SelectedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </ListBoxItem>
                                                <RadioButtonList HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </RadioButtonList>
                                                <IncrementButtonStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <DisabledStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </DisabledStyle>
                                                    <PressedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </PressedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </IncrementButtonStyle>
                                                <SpinEditIncrementButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <DisabledStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </DisabledStyle>
                                                    <PressedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </PressedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </SpinEditIncrementButton>
                                                <DecrementButtonStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <DisabledStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </DisabledStyle>
                                                    <PressedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </PressedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </DecrementButtonStyle>
                                                <SpinEditDecrementButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <DisabledStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </DisabledStyle>
                                                    <PressedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </PressedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </SpinEditDecrementButton>
                                                <LargeIncrementButtonStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <DisabledStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </DisabledStyle>
                                                    <PressedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </PressedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </LargeIncrementButtonStyle>
                                                <SpinEditLargeIncrementButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <DisabledStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </DisabledStyle>
                                                    <PressedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </PressedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </SpinEditLargeIncrementButton>
                                                <LargeDecrementButtonStyle HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <DisabledStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </DisabledStyle>
                                                    <PressedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </PressedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </LargeDecrementButtonStyle>
                                                <SpinEditLargeDecrementButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <DisabledStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </DisabledStyle>
                                                    <PressedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </PressedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </SpinEditLargeDecrementButton>
                                                <Label HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Label>
                                                <Hyperlink HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Hyperlink>
                                                <Image HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Image>
                                                <Memo HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Memo>
                                                <TextBox HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </TextBox>
                                                <ButtonEdit HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </ButtonEdit>
                                                <ButtonEditButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <DisabledStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </DisabledStyle>
                                                    <PressedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </PressedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </ButtonEditButton>
                                                <Calendar HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Calendar>
                                                <CalendarDayHeader HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarDayHeader>
                                                <CalendarWeekNumber HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarWeekNumber>
                                                <CalendarDay HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarDay>
                                                <CalendarDayOtherMonth HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarDayOtherMonth>
                                                <CalendarDaySelected HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarDaySelected>
                                                <CalendarDayWeekEnd HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarDayWeekEnd>
                                                <CalendarDayOutOfRange HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarDayOutOfRange>
                                                <CalendarToday HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarToday>
                                                <CalendarHeader HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarHeader>
                                                <CalendarFooter HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarFooter>
                                                <CalendarButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <DisabledStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </DisabledStyle>
                                                    <PressedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </PressedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarButton>
                                                <CalendarFastNav HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarFastNav>
                                                <CalendarFastNavMonthArea HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarFastNavMonthArea>
                                                <CalendarFastNavYearArea HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarFastNavYearArea>
                                                <CalendarFastNavMonth HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <SelectedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </SelectedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarFastNavMonth>
                                                <CalendarFastNavYear HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <SelectedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </SelectedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarFastNavYear>
                                                <CalendarFastNavFooter HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </CalendarFastNavFooter>
                                                <MaskHint Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </MaskHint>
                                                <ProgressBar HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </ProgressBar>
                                                <ProgressBarIndicator>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </ProgressBarIndicator>
                                                <DropDownWindow HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </DropDownWindow>
                                                <ColorTable>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </ColorTable>
                                                <ColorTableCell>
                                                    <ColorBorder BorderStyle="NotSet" />
                                                    <ColorBorderLeft BorderStyle="NotSet" />
                                                    <ColorBorderTop BorderStyle="NotSet" />
                                                    <ColorBorderRight BorderStyle="NotSet" />
                                                    <ColorBorderBottom BorderStyle="NotSet" />
                                                    <SelectedStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </SelectedStyle>
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </ColorTableCell>
                                                <ColorIndicator>
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </ColorIndicator>
                                                <DisplayColorIndicator>
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </DisplayColorIndicator>
                                                <Focused HorizontalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Focused>
                                                <NullText HorizontalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </NullText>
                                                <Invalid HorizontalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Invalid>
                                                <LoadingPanel HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </LoadingPanel>
                                                <LoadingDiv>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </LoadingDiv>
                                            </StylesEditors>
                                            <StylesFilterControl>
                                                <Table HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Table>
                                                <PropertyName HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </PropertyName>
                                                <GroupType HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </GroupType>
                                                <Operation HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Operation>
                                                <Value HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </Value>
                                                <ImageButton HorizontalAlign="NotSet" VerticalAlign="NotSet" Wrap="Default">
                                                    <HoverStyle>
                                                        <BackgroundImage Repeat="Repeat" />
                                                        <Border BorderStyle="NotSet" />
                                                        <BorderLeft BorderStyle="NotSet" />
                                                        <BorderTop BorderStyle="NotSet" />
                                                        <BorderRight BorderStyle="NotSet" />
                                                        <BorderBottom BorderStyle="NotSet" />
                                                    </HoverStyle>
                                                    <BackgroundImage Repeat="Repeat" />
                                                    <Border BorderStyle="NotSet" />
                                                    <BorderLeft BorderStyle="NotSet" />
                                                    <BorderTop BorderStyle="NotSet" />
                                                    <BorderRight BorderStyle="NotSet" />
                                                    <BorderBottom BorderStyle="NotSet" />
                                                </ImageButton>
                                            </StylesFilterControl>
                                            <BackgroundImage Repeat="Repeat" />
                                            <Border BorderStyle="NotSet" />
                                            <BorderLeft BorderStyle="NotSet" />
                                            <BorderTop BorderStyle="NotSet" />
                                            <BorderRight BorderStyle="NotSet" />
                                            <BorderBottom BorderStyle="NotSet" />

                                        </dx:ASPxGridView>
                                    </div>
                                 </ContentTemplate>
                             </asp:UpdatePanel>                                                                                                        
                         </div>                                                                                                                                                        
                        <br />    
                        </fieldset>                                   
                    </div>                                      
                    <asp:UpdatePanel ID="UpdatePanelHD" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="hdCnValidation" runat="server" value=""/>
                            <asp:HiddenField ID="cnPrecargado" runat="server" value=""/>
                            <asp:HiddenField ID="hdDsSession" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdClienteContactoEventoPrincipales" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdProductoOriginal" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdVentaDetalle" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdVenta" runat="server" value=""/>
                            <asp:HiddenField ID="hdMontoDiferencia" runat="server" value=""/>
                            <asp:HiddenField ID="hdCnGlobalCollect" runat="server" value=""/>
                            <asp:HiddenField ID="hdCnGlobalCollectI" runat="server" value=""/>
                            <asp:HiddenField ID="hdIV" runat="server" value=""/>
                            <asp:HiddenField ID="hdToTalPagar" runat="server" value=""/>
                            <asp:HiddenField ID="hdEsPagoTarjeta" runat="server" value=""/>
                            <asp:HiddenField ID="hdCorreoCompetidor" runat="server" value=""/>
                            <asp:HiddenField ID="hddIdOrdenGlobal" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdPais" runat="server" value=""/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
       </div>                 
        
        <dx:ASPxLoadingPanel ID="pnlinfo" runat="server" ClientInstanceName="pnlinfoproceso" Modal="True">
        </dx:ASPxLoadingPanel>        
    </div>    
</body>  
</html>
