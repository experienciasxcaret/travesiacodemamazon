﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="cobroDiferencia.aspx.cs" Inherits="travesia.cobroDiferencia" %>

<%@ Register Assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" namespace="DevExpress.Web.ASPxLoadingPanel" tagprefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width= device-width, initial-scale=1.0" /> 
    <title>Triatlon Xel-Ha</title>
    <link rel="stylesheet" href="styles/bootstrap.css" />
    <link rel="stylesheet" href="styles/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="styles/triatlon.css" />
    <link rel="stylesheet" href="styles/basic.css" />
    <link rel="stylesheet" href="styles/detallePago.css" />

    <script src="Scripts/jquery-1.11.3.js" ></script>
    <script src="Scripts/bootstrap.js" ></script>
    <script src="Scripts/inscripcionRifa.js" ></script>
    <script type="text/javascript" src="Scripts/accounting/accounting.js" ></script>
    <script src="Scripts/detallePago.js" ></script>
    <script src="Scripts/detallePago2.js" ></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.metadata.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/localization/messages_es.js"></script>
    <link rel="shortcut icon" type="image/png" href="favicon.png" />



    
    <script type="text/javascript">

        
        $(document).ready(function () {

            Sys.Browser.WebKit = {}; //Safari 3 is considered WebKit

            if (navigator.userAgent.indexOf('WebKit/') > -1) {
                
                Sys.Browser.agent = Sys.Browser.Firefox;

                Sys.Browser.version = parseFloat(navigator.userAgent.match(/WebKit\/(\d+(\.\d+)?)/)[1]);

                Sys.Browser.name = 'Firefox';
            }

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(initializeRequest);
            prm.add_pageLoaded(pageLoaded);
            function initializeRequest(sender, args) {
                //LoadingPanel.ShowInElement(args._postBackElement);  
                document.body.style.cursor = "wait";                
                pnlinfoproceso.Show();

            }
            function pageLoaded(sender, args) {
                var panels = args.get_panelsUpdated();
                if (panels.length > 0) {
                    document.body.style.cursor = "default";
                    pnlinfoproceso.Hide();
                }
            }            
        })
    </script>
    <style type="text/css">
        .auto-style1 {
            height: 37px;
            font:bold;
            font-size:medium;
        }
    </style>
</head>
<body>    
    <div class="container">
        <div class="cont header b-bottom">
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			    <object data="img/xelha.svg" type="image/svg+xml" class="img1 hidden-xs"></object>
		    </div>
		    <div class="logo col-xs-8 col-sm-4" align="center">
			    <object data="img/triatlon.svg" type="image/svg+xml" class="img2"></object>
		    </div>
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			    <object data="img/triatlon_fechas.png" type="image/svg+xml" class="img3 hidden-xs"></object>
		    </div>
       </div>
       <div class="grid_16 wrap">
            <div id="cont_formularios" class="cont" runat="server">
                <form class="form-horizontal" id="formRifa" runat="server">
                    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>

 
<dx:ASPxLabel ID="lblMensajeCabecera" runat="server" Text="¡Bienvenido!" Font-Size="Larger" ForeColor="Black" ClientInstanceName="lblMensajeCabecera"></dx:ASPxLabel>                
<div class="row id="dvDetalle">
                                                <div class="text-center">
                                                    <table class="nav-justified">
                                                        <tr>
                                                            <td class="auto-style1">Folio</td>
                                                            <td class="auto-style1">Nombre del competidor</td>
                                                            <td class="auto-style1">Numero de competidor</td>
                                                            <td class="auto-style1">Producto</td>
                                                            <td class="auto-style1">Importe por cobrar</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblFolio" runat="server" style="font-size: large; font-weight: 700"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCompetidor" runat="server" style="font-size: large; font-weight: 700"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblNoCompetidor" runat="server" style="font-size: large; font-weight: 700"></asp:Label>
                                                            </td>

                                                            <td>
                                                                <asp:Label ID="lblProducto" runat="server" style="font-size: large; font-weight: 700"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblImporte" runat="server" style="font-size: large; font-weight: 700"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                
                                                <table class="table table-hover" id="tblDetalleCompra">                       
                        
                                                </table>                    
                                               <!-- <button type="button" class="btn btn-success btn-lg btn-block" id="btnContinuar">
                                                    Continuar   <span class="glyphicon glyphicon-chevron-right"></span>
                                                </button>  
                                                -->                 
                                            </div>                     
<div class="container" id="dvSecPago">
            <div class="row">
                <div class="well col-xs-12 col-sm-12 col-md-8 col-xs-offset-1 col-sm-offset-1 col-md-offset-2">                    
                      <asp:UpdatePanel ID="upModuloPago" runat="server">
                        <ContentTemplate>
                                <div class="form-group" style="margin-left:3px"> 
                                    <div class="col-md-1">
                                    </div>            
                                     <div class="col-xs-12 col-sm-12"> 
                                       <asp:RadioButtonList ID="rblTipoTarjeta" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblTipoTarjeta_SelectedIndexChanged">                                           
                                               <asp:ListItem
                                                Enabled="True"                                                
                                                Text='<img src="https://www.xperienciasxcaret.mx/ecom/webroot/images/visa.png"/>'
                                                Value="2"
                                                />                                                                                     
                                            <asp:ListItem
                                                Enabled="True"                            
                                                Text='<img src="https://www.xperienciasxcaret.mx/ecom/webroot/images/master.png"/>'
                                                Value="1"
                                            />
                                            <asp:ListItem
                                                Enabled="True"                            
                                                Text='<img src="https://www.xperienciasxcaret.mx/ecom/webroot/images/amex.png"/>'
                                                Value="3"
                                            />
                                       </asp:RadioButtonList>
                                    </div>
                                </div>
                            </ContentTemplate>
                       </asp:UpdatePanel>                    
                    <br />
                    <div class="row" style="margin-left:1px">                        
                            <div class="co col-lg-10">
                                <div class="col-sm-3 col-lg-3" >
                                    <label>Tipo Moneda</label>
                                </div>
                                <div class="col-sm-5 col-md-5">
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList class="{required: true} form-control" ID="ddlCambioMoneda" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddlCambioMoneda_SelectedIndexChanged">
                                                <asp:ListItem Value="0">--</asp:ListItem>
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>                        
                    </div>
                    <br />  
                    <div class="row" style="display:none">
                    
                        <div class="col-sm-10 col-lg-10">
                            <div class="col-sm-3 col-lg-3" >
                                <label>Forma de pago</label>
                            </div>
                            <div class="col-sm-5 col-md-5">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList class="{required: true} form-control" ID="ddlFormaPago" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFormaPago_SelectedIndexChanged" ControlToValidate="ddlFormaPago">
                                        <asp:ListItem Value="">Seleccione forma de pago</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rvFormaPago" runat="server" ErrorMessage="" ControlToValidate="ddlFormaPago" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            </div>
                        </div>
                    
                    </div>
                    <br />
                    <div class="row" style="margin-left:1px">                        
                            <div class="col-sm-10 col-lg-10">                                
                                <asp:Label class="col-sm-3 col-lg-3" ID="lblBanco" runat="server" Text="Banco" Font-Bold="True"></asp:Label>
                                <div class="col-sm-5 col-lg-5">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList class="{required: true} form-control" ID="ddlBanco" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBanco_SelectedIndexChanged">
                                            <asp:ListItem Value="">Seleccione Banco emisor</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rvBanco" runat="server" ErrorMessage="" ControlToValidate="ddlBanco" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                </ContentTemplate>
                                </asp:UpdatePanel>
                                </div>
                            </div>                        
                    </div>
                    <br />
                     <asp:Panel ID="panelDatosTarjeta" runat="server" style="margin-left:5px">
                         <asp:Panel ID="pnNombreTH" runat="server">
                          <div class="row">   
                             <div class="form-group" style="padding:5px">
                                <div class="col-sm-12 col-lg-12">
                                    <div class ="col-sm-12 col-lg-12"	>
                                        <label style="padding-right:5px">Nombre Tarjetahabiente</label>                                    
                                         <asp:TextBox runat="server" type="text" class="{required:true, nombreth: /^[A-Za-záéíóúñ,.]{2,}([\s][A-Za-záéíóúñ,.]{2,})+$/ } form-control" id="txtNombreTH" placeholder="nombre tarjetahabiente" name="nombreTH"> </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rvNombreTH" runat="server" ErrorMessage="" ControlToValidate="txtNombreTH" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>                                        
                                    </div>                                                
                                </div>   
                             </div>                                                                               
                           </div>
                           </asp:Panel>
                         <asp:Panel ID="pnNumTarjeta" runat="server">                        
                            <div class="row">                         
                            <div class="form-group" style="padding:5px"> 
                                <div class="col-sm-12 col-lg-12">
                                    <div class="col-sm-12 col-lg-12">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>                                                                             
                                            <asp:Label style="padding-right:8px" ID="lblNumTarjeta" runat="server" Text="Número de tarjeta" Font-Bold="True"></asp:Label>
                                            <asp:TextBox runat="server" class="{creditcard: true} form-control" id="txtNoTarjeta" placeholder="Número Tarjeta" required autofocus data-stripe="number" name="noTarjeta"> </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rvNoTarjeta" runat="server" ErrorMessage="" ControlToValidate="txtNoTarjeta" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                         </ContentTemplate>
                                         </asp:UpdatePanel>                                                                          
                                    </div>
                                </div>
                            </div>
                            </div>
                         </asp:Panel>
                         <asp:Panel ID="pnDatosSeguridad" runat="server">                                         
                            <div class="row">
                             <div class="form-group" style="padding:5px">
                                 <div class="col-md-12">                                                        
                                    <div class="col-md-12">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>                                                                                                               
                                            <asp:Label ID="lblFechaExpira" runat="server" Text="Fecha de expiración" Font-Bold="True"></asp:Label>
                                             <asp:DropDownList class="{required: true} form-control" ID="ddlMesExpira" runat="server" AppendDataBoundItems="True" AutoPostBack="False">
                                                <asp:ListItem Value="01">01</asp:ListItem>
                                                <asp:ListItem Value="02">02</asp:ListItem>
                                                <asp:ListItem Value="03">03</asp:ListItem>
                                                <asp:ListItem Value="04">04</asp:ListItem>
                                                <asp:ListItem Value="05">05</asp:ListItem>
                                                <asp:ListItem Value="06">06</asp:ListItem>
                                                <asp:ListItem Value="07">07</asp:ListItem>
                                                <asp:ListItem Value="08">08</asp:ListItem>
                                                <asp:ListItem Value="09">09</asp:ListItem>
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                <asp:ListItem Value="12">12</asp:ListItem>
                                              </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rvMesExpira" runat="server" ErrorMessage="" ControlToValidate="ddlMesExpira" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                             <asp:DropDownList class="{required: true} form-control" ID="ddlAnioExpira" runat="server" AppendDataBoundItems="True" AutoPostBack="False">
                                                <asp:ListItem Value="0">--</asp:ListItem>
                                             </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rvAnioExpira" runat="server" ErrorMessage="" ControlToValidate="ddlAnioExpira" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                        </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>    
                                </div>                            
                            </div>
                            </div>
                        
                            <div class="row">
                            <div class="form-group" style="padding:5px">
                                <div class="col-md-12">      
                                    <div class="col-md-12">
                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                        <ContentTemplate>                                                                                                                   
                                            <asp:Label ID="lblCVV" runat="server" Text="Codigo de seguridad" Font-Bold="True"></asp:Label>                             
                                            <asp:TextBox type="password" runat="server" width="60px" class="{digits:true} form-control" id="txtCVV" placeholder="CVV" required autofocus data-stripe="cvc " name="anio"> </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rvCVV" runat="server" ErrorMessage="" ControlToValidate="txtCVV" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                        </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>                                                                      
                                </div>
                            </div>
                            </div>
                         </asp:Panel>
                         <br />
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <!--<button class="btn btn-success btn-lg btn-block" type="submit">Start Subscription</button>-->
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="lblRespuestaPago" runat="server" Text="" Font-Bold="True" Font-Size="Medium"></asp:Label>                                                                           
                                   <dx:ASPxButton  ID="btnPagarFinal" runat="server" Text="Pagar" class="btn btn-success btn-lg btn-block text-center" name="btnPagarFinal" CssPostfix="&quot;btn btn-success btn-lg btn-block text-center" ClientInstanceName="btnPagarName" OnClick="btnPagarFinal_Click" ValidationGroup="validacionTarjeta"></dx:ASPxButton>                                    
                                </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row" style="display:none;">
                            <div class="col-xs-12">
                                <p class="payment-errors"></p>
                            </div>
                        </div>
                         <asp:UpdatePanel ID="upGC" runat="server" UpdateMode="Conditional" >
                            <ContentTemplate>  
                                <div class="dvGlobalOver" style="display:none;">
                                    <div id='overpay'></div>
                                        <div id="contentGlobal">
                                            <input id="cancel" type="button">
                                            <asp:HiddenField ID="hddResultadoPagoGlobalCollect" runat="server" Value=""/>
                                            <asp:Label ID="lblRespuestaPagoGlobalCollect" runat="server" style="font-weight: 700 font-size: 8pt"></asp:Label>
                                            <asp:Panel ID="pnGlobalCollect" runat="server">
                                                <div class="insideformas">
                                                    <table align="left" class="tbCss1" style="height:100%; width:100%">
                                                            <caption class="tb1CaptionTop">
                                                                <dx:ASPxLabel ID="lblEtiGlobalCollect" runat="server" ClientIDMode="AutoID" Text="Global Collect" Visible="true" style="margin-bottom:15px">
                                                                </dx:ASPxLabel>
                                                            </caption>
                                                            <tr >
                                                                <td align="left">
                                                                    <iframe id="ifrGlobalCollect" runat="server" src="">
                                                                    <p>
                                                                        Your browser does not support iframes.</p>
                                                                    </iframe>                                
                                                                    <dx:ASPxButton  ID="btnGlobalCollect" runat="server" UseSubmitBehavior="False" Text="" name="btnGlobalCollect" ClientInstanceName="btnGlobalCollect"  CausesValidation="False" AutoPostBack="False" ClientVisible="false"></dx:ASPxButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
           <asp:UpdatePanel ID="UpdatePanel8" runat="server" >
           <ContentTemplate>
               <asp:HiddenField ID="hdToTalPagar" runat="server" value="0"/>
               <asp:HiddenField ID="hdIdVenta" runat="server" value="0"/>
               <asp:HiddenField ID="hdCnGlobalCollect" runat="server" value="0"/> 
               <asp:HiddenField ID="hdCnGlobalCollectI" runat="server" value="0"/>
               <asp:HiddenField ID="hddIdOrdenGlobal" runat="server" Value="0" />
               <asp:HiddenField ID="hddGlobalCollectReferenciaComercio" runat="server" Value="0" />
               <asp:HiddenField ID="hdIV" runat="server" Value="" />
               <asp:HiddenField ID="cnIdContactoEvento" runat="server" value=""/>
               <asp:HiddenField ID="hdCorreoCompetidor" runat="server" Value="" />
               <asp:HiddenField ID="hddsClaveVenta" runat="server" Value="" />
               <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server" ClientInstanceName="pnlinfoproceso" Modal="True" Text="Por favor, espere / Please wait&amp;hellip;"></dx:ASPxLoadingPanel>
            </ContentTemplate>
            </asp:UpdatePanel>
           </asp:Panel> 
            </div>              
                                        
                </div>                
            </div><!-- fin del container -->      
                </form>
            </div>
       </div>                 
        
        <dx:ASPxLoadingPanel ID="pnlinfo" runat="server" ClientInstanceName="pnlinfoproceso" Modal="True">
        </dx:ASPxLoadingPanel>        
    </div>    
</body>  
</html>
