﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="detallePago.aspx.cs" Inherits="travesia.detallePago" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" namespace="DevExpress.Web.ASPxLoadingPanel" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width= device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="styles/bootstrap.css" />
    <link rel="stylesheet" href="styles/bootstrap.min.css" />   
    <link rel="stylesheet" href="styles/font-awesome.min.css" />    
    <link rel="stylesheet" href="styles/triatlon.css" />
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="styles/detallePago.css" />
    <link rel="stylesheet" href="styles/wow-alert.css" /> 
    <link rel="shortcut icon" type="image/png" href="favicon.png" />
    

    <script src="Scripts/jquery-1.11.2.min.js" ></script>
    <script src="Scripts/bootstrap.js" ></script>
    <script type="text/javascript" src="Scripts/accounting/accounting.js" ></script>
    <script src="Scripts/detallePago.js" ></script>
    <script src="Scripts/detallePago2.js" ></script>
    <script src="Scripts/wow-alert.js" ></script>
    
    
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.metadata.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/localization/messages_es.js"></script>    
    
    <script type="text/javascript">
        $(document).ready(function () {

            Sys.Browser.WebKit = {}; //Safari 3 is considered WebKit

            if (navigator.userAgent.indexOf('WebKit/') > -1) {

                Sys.Browser.agent = Sys.Browser.Firefox;

                Sys.Browser.version = parseFloat(navigator.userAgent.match(/WebKit\/(\d+(\.\d+)?)/)[1]);

                Sys.Browser.name = 'Firefox';
            }
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(initializeRequest);
            prm.add_pageLoaded(pageLoaded);
            function initializeRequest(sender, args) {
                //LoadingPanel.ShowInElement(args._postBackElement);  
                document.body.style.cursor = "wait";
                pnlinfoproceso.Show();

            }
            function pageLoaded(sender, args) {
                var panels = args.get_panelsUpdated();
                if (panels.length > 0) {
                    document.body.style.cursor = "default";
                    pnlinfoproceso.Hide();
                }
            }
        })
    </script>
    <title></title>
</head>
<body>
   
       <form class="form-inline" id="formRifa" runat="server">
           <div class="container">
               <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
               <asp:HiddenField ID="hdCnCena" runat="server" value=""/>
               <asp:HiddenField ID="hdIdPais" runat="server" value=""/>
               <asp:HiddenField ID="hdIdEstado" runat="server" value=""/>
               <asp:HiddenField ID="hdCorreoCompetidor" runat="server" value=""/>
               <asp:HiddenField ID="hdIdConfigPago" runat="server" value=""/>
            <div class="row">
                <div class="well col-xs-12 col-sm-12 col-md-8 col-xs-offset-0 col-xs-offset-0 col-md-offset-2">              

                    <div class="row" id="dvDetalle">
                        <div class="text-center">
                            <h3>Detalle</h3>
                        </div>
                    </div> 
                    <div class="row" >             
                        <table class="table table-hover" id="tblDetalleCompra">                       
                        
                        </table>
                    </div>                  
                       <!-- <button type="button" class="btn btn-success btn-lg btn-block" id="btnContinuar">
                            Continuar   <span class="glyphicon glyphicon-chevron-right"></span>
                        </button>  
                        -->                                 
               </div>
            </div>
            </div><!-- fin del container -->

            <div class="container" id="dvSecPago">
            <div class="row">
                <div class="well col-xs-12 col-sm-12 col-md-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-2">
                    <asp:UpdatePanel ID="updComproPago" runat="server">
                        <ContentTemplate>
                            <div class="col-sm-12 col-lg-12 columns" style="background-color:#79CDCD;margin-top:-10px" runat="server" id="divComproPago" visible="false" >                                                        
                                <div class="col-sm-12 col-lg-12 compropago" style="margin-left:-30px">
                                   
                                    <asp:Label ID="lblComproPago" runat="server" Text="Ahora ya puedes pagar depositando en los siguientes establecimientos." Font-Bold="True"></asp:Label>
                                     <asp:UpdatePanel ID="UpdatePanel12" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:RadioButtonList ID="rblComproPago" runat="server" class="rblComproPago" RepeatDirection="Horizontal" RepeatColumns="5" AutoPostBack="true" RepeatLayout="Table" style="margin-top:-10px" OnSelectedIndexChanged="rblComproPago_SelectedIndexChanged" >                                                                                        
                                            </asp:RadioButtonList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel> 
                                 </div>                            
                            </div>
                      
                                <div class="form-group" id="divRadioFormasPago" runat="server">                                                 
                                     <div class="col-xs-11 col-sm-11" style="z-index:999"> 
                                       <asp:RadioButtonList ID="rblTipoTarjeta" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblTipoTarjeta_SelectedIndexChanged">
                                               <asp:ListItem
                                                Enabled="True"                                                
                                                Text='<img src="https://www.xperienciasxcaret.mx/ecom/webroot/images/visa.png"/>'
                                                Value="2"
                                                />                                                                                     
                                            <asp:ListItem
                                                Enabled="True"                            
                                                Text='<img src="https://www.xperienciasxcaret.mx/ecom/webroot/images/master.png"/>'
                                                Value="1"
                                            />
                                            <asp:ListItem
                                                Enabled="True"                            
                                                Text='<img src="https://www.xperienciasxcaret.mx/ecom/webroot/images/amex.png"/>'
                                                Value="3"
                                            />                                            
                                       </asp:RadioButtonList>
                                   
                                       <asp:RequiredFieldValidator ID="rvTipoTarjeta" runat="server" ErrorMessage="&nbsp;&nbsp;Seleccione tipo de tarjeta" ControlToValidate="rblTipoTarjeta" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#FF0000" Font-Bold="True"></asp:RequiredFieldValidator>
                                     
                                    </div>
                                </div>
                            </ContentTemplate>
                       </asp:UpdatePanel>                    
                        <br />
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                        <div runat="server" id="divModTarjeta" >
                            <asp:Panel ID="panel1DatosTarjetas" runat="server"> 
                                <div class="row" style="margin-left:1px">                        
                                    <div class="co col-lg-10">
                                        <div class="col-sm-3 col-lg-3" >
                                            <label>Tipo Moneda</label>
                                        </div>
                                        <div class="col-sm-5 col-md-5">
                                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList class="{required: true} form-control" ID="ddlCambioMoneda" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddlCambioMoneda_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>                        
                                </div>
                                <br />  
                                <div class="row" style="margin-left:1px;display:block">                    
                                    <div class="col-sm-10 col-lg-10">
                                        <div class="col-sm-3 col-lg-3" >
                                            <label>Forma de pago</label>
                                        </div>
                                        <div class="col-sm-5 col-md-5">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList class="{required: true} form-control" ID="ddlFormaPago" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFormaPago_SelectedIndexChanged" ControlToValidate="ddlFormaPago">
                                                    <asp:ListItem Value="">Seleccione forma de pago</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rvFormaPago" runat="server" ErrorMessage="" ControlToValidate="ddlFormaPago" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                        </ContentTemplate>
                                        </asp:UpdatePanel>
                                        </div>
                                    </div>                    
                                </div>
                                <br />
                                <div class="row" style="margin-left:1px">                        
                                        <div class="col-sm-10 col-lg-10">                                
                                            <asp:Label class="col-sm-3 col-lg-3" ID="lblBanco" runat="server" Text="Banco" Font-Bold="True"></asp:Label>
                                            <div class="col-sm-5 col-lg-5">
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList class="{required: true} form-control" ID="ddlBanco" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBanco_SelectedIndexChanged">
                                                        <asp:ListItem Value="">Seleccione Banco emisor</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rvBanco" runat="server" ErrorMessage="" ControlToValidate="ddlBanco" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                            </ContentTemplate>
                                            </asp:UpdatePanel>
                                            </div>
                                        </div>                        
                                </div>
                                <br />
                            </asp:Panel>
                             <asp:Panel ID="panelDatosTarjeta" runat="server">
                                 <asp:Panel ID="pnNombreTH" runat="server">
                                  <div class="row" style="margin-left:1px">                                
                                        <div class="col-sm-12 col-lg-12">                                    
                                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                                <ContentTemplate>
                                                <div class="col-sm-12 col-lg-12">    
                                                        <asp:Label ID="lblNombreTh" runat="server" Text="Nombre Tarjetahabiente" Font-Bold="True"></asp:Label>  
                                                                        
                                            
                                                        <asp:TextBox runat="server" type="text" class="{required:true, nombreth: /^[A-Za-záéíóúñ,.]{2,}([\s][A-Za-záéíóúñ,.]{2,})+$/ } form-control" id="txtNombreTH" placeholder="nombre tarjetahabiente" name="nombreTH"> </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rvNombreTH" runat="server" ErrorMessage="" ControlToValidate="txtNombreTH" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>                                        
                                                  </div>  
                                                </ContentTemplate>
                                            </asp:UpdatePanel>                                                                                 
                                        </div>   
                                                                                                            
                                   </div>
                                   </asp:Panel>
                                    <br />
                                   <asp:Panel ID="pnNumTarjeta" runat="server">                        
                                    <div class="row" style="margin-left:1px;">                         
                                 
                                            <div class="col-sm-12 col-lg-12">                                    
                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-sm-3 col-md-3">                                                                              
                                                            <asp:Label ID="lblNumTarjeta" runat="server" Text="Número de tarjeta" Font-Bold="True"></asp:Label>
                                                        </div>
                                                        <div class="col-sm-4 col-md-4">
                                                            <asp:TextBox runat="server" class="{creditcard: true} form-control" id="txtNoTarjeta" placeholder="Número Tarjeta" required data-stripe="number" name="noTarjeta"> </asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rvNoTarjeta" runat="server" ErrorMessage="" ControlToValidate="txtNoTarjeta" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                        </div>
                                                     </ContentTemplate>
                                                     </asp:UpdatePanel>                                                                                                              
                                            </div>
                                
                                    </div>
                                 </asp:Panel>
                                 <br />
                                 <asp:Panel ID="pnDatosSeguridad" runat="server">                                         
                                    <div class="row" style="margin-left:1px;">
                                     <div class="form-group" >
                                         <div class="col-md-12">                                                        
                                            <div class="col-md-12">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>                                                                                                               
                                                    <asp:Label ID="lblFechaExpira" runat="server" Text="Fecha de expiración" Font-Bold="True"></asp:Label>
                                                     <asp:DropDownList class="{required: true} form-control" ID="ddlMesExpira" runat="server" AppendDataBoundItems="True" AutoPostBack="False">
                                                        <asp:ListItem Value="01">01</asp:ListItem>
                                                        <asp:ListItem Value="02">02</asp:ListItem>
                                                        <asp:ListItem Value="03">03</asp:ListItem>
                                                        <asp:ListItem Value="04">04</asp:ListItem>
                                                        <asp:ListItem Value="05">05</asp:ListItem>
                                                        <asp:ListItem Value="06">06</asp:ListItem>
                                                        <asp:ListItem Value="07">07</asp:ListItem>
                                                        <asp:ListItem Value="08">08</asp:ListItem>
                                                        <asp:ListItem Value="09">09</asp:ListItem>
                                                        <asp:ListItem Value="10">10</asp:ListItem>
                                                        <asp:ListItem Value="11">11</asp:ListItem>
                                                        <asp:ListItem Value="12">12</asp:ListItem>
                                                      </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rvMesExpira" runat="server" ErrorMessage="" ControlToValidate="ddlMesExpira" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                     <asp:DropDownList class="{required: true} form-control" ID="ddlAnioExpira" runat="server" AppendDataBoundItems="True" AutoPostBack="False">
                                                        <asp:ListItem Value="0">--</asp:ListItem>
                                                     </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rvAnioExpira" runat="server" ErrorMessage="" ControlToValidate="ddlAnioExpira" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>    
                                        </div>                            
                                    </div>
                                    </div>
                                    <br />
                                    <div class="row" style="margin-left:1px;">
                                    <div class="form-group">
                                        <div class="col-md-12">      
                                            <div class="col-md-12">
                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                <ContentTemplate>                                                                                                                   
                                                    <asp:Label ID="lblCVV" runat="server" Text="Codigo de seguridad" Font-Bold="True"></asp:Label>                             
                                                    <asp:TextBox type="password" runat="server" width="60px" class="{digits:true} form-control" id="txtCVV" placeholder="CVV" required data-stripe="cvc " name="anio"> </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rvCVV" runat="server" ErrorMessage="" ControlToValidate="txtCVV" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>                                                                      
                                        </div>
                                    </div>
                                    </div>
                                     <div class="row" style="padding-left:5px">
                                        <div class="form-group" >
                                            <div class="col-md-12">      
                                                <div class="col-md-12">
                                                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                    <ContentTemplate>                                                                                                                   
                                                        <asp:Label ID="lblReferencia" class ="col-sm-3" runat="server" Text="No. Referencias" Font-Bold="True" Visible="false"></asp:Label>
                                                        <div class ="col-sm-3">
                                                            <asp:TextBox runat="server" class="form-control" id="txtReferencia" placeholder="No. Referencia" Visible="false" required autofocus name="referencia">
                                                            </asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rvReferencia" runat="server" ErrorMessage="" ControlToValidate="txtReferencia" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>                                                                      
                                            </div>
                                        </div>
                                    </div>
                                 </asp:Panel>
                                 <br />
                        
                                         <asp:UpdatePanel ID="upGC" runat="server" UpdateMode="Conditional" >
                                            <ContentTemplate>  
                                                <div class="dvGlobalOver" style="display:none;">
                                                    <div id='overpay'></div>
                                                        <div id="contentGlobal">
                                                            <input id="cancel" type="button">
                                                            <asp:HiddenField ID="hddResultadoPagoGlobalCollect" runat="server" Value=""/>
                                                            <asp:Label ID="lblRespuestaPagoGlobalCollect" runat="server" style="font-weight: 700 font-size: 8pt"></asp:Label>
                                                            <asp:Panel ID="pnGlobalCollect" runat="server" style="height:75%">
                                                                <div class="insideformas">
                                                                    <table align="left" class="tbCss1" style="height:100%; width:100%">
                                                                            <caption class="tb1CaptionTop">
                                                                                <dx:ASPxLabel ID="lblEtiGlobalCollect" runat="server" ClientIDMode="AutoID" Text="Global Collect" Visible="true" style="margin-bottom:15px"></dx:ASPxLabel>
                                                                            </caption>
                                                                            <tr >
                                                                                <td align="left">
                                                                                    <iframe id="ifrGlobalCollect" runat="server" src="">
                                                                                    <p>
                                                                                        Your browser does not support iframes.</p>
                                                                                    </iframe>                                
                                                                                    <dx:ASPxButton  ID="btnGlobalCollect" runat="server" UseSubmitBehavior="False" Text="" name="btnGlobalCollect" ClientInstanceName="btnGlobalCollect" OnClick="btnGlobalCollect_Click" CausesValidation="False" AutoPostBack="False" ClientVisible="false"></dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                   
                           </asp:Panel> 
                        </div>
                        </ContentTemplate>
                   </asp:UpdatePanel>
                   <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <!--<button class="btn btn-success btn-lg btn-block" type="submit">Start Subscription</button>-->
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblRespuestaPago" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000"></asp:Label>                                                                           
                                <dx:ASPxButton  ID="btnPagarFinal" runat="server" Text="Pagar" class="btn btn-success btn-lg btn-block text-center" name="btnPagarFinal" CssPostfix="&quot;btn btn-success btn-lg btn-block text-center" ClientInstanceName="btnPagarName" OnClick="btnPagarFinal_Click" ValidationGroup="validacionTarjeta"></dx:ASPxButton>                                    
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="row" style="display:none;">
                        <div class="col-xs-12">
                            <p class="payment-errors"></p>
                        </div>
                    </div>
            </div>              
                                        
                </div>                
            </div><!-- fin del container -->
           <asp:UpdatePanel ID="UpdatePanel8" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hdToTalPagar" runat="server" value="0"/>
                <asp:HiddenField ID="hdIdVenta" runat="server" value="0"/>
                <asp:HiddenField ID="hdCnGlobalCollect" runat="server" value="0"/> 
                <asp:HiddenField ID="hdCnGlobalCollectI" runat="server" value="0"/>
                <asp:HiddenField ID="hddIdOrdenGlobal" runat="server" Value="0" />
                <asp:HiddenField ID="hddGlobalCollectReferenciaComercio" runat="server" Value="0" />
                <asp:HiddenField ID="hdIV" runat="server" Value="" />
                <asp:HiddenField ID="HiddenField1" runat="server" Value="" />
                <asp:HiddenField ID="cnIdContactoEvento" runat="server" value=""/>
                <asp:HiddenField ID="hdEsPagoTarjeta" runat="server" value=""/>
                <asp:HiddenField ID="hdCnCompropago" runat="server" value="0"/> 
                <asp:HiddenField ID="hddPagoGateWay" runat="server" value="0"/>
                <asp:HiddenField ID="hddPagoWorldPay" runat="server" value="0"/>
                <asp:HiddenField ID="hddResultadoPagoWorldPay" runat="server" Value=""/>            
            </ContentTemplate>
            </asp:UpdatePanel>                  
            <dx:ASPxLoadingPanel ID="pnlinfo" runat="server" ClientInstanceName="pnlinfoproceso" Modal="True" Text="Por favor, espere / Please wait&amp;hellip;"></dx:ASPxLoadingPanel>
           <asp:UpdatePanel ID="upMensaje" runat="server" >
                    <ContentTemplate>
                        <div class="dvPopUpOver" style="display:none;">
                            <div id='overpay2'></div>
                            <div id="contentGlobal2" class="popUpMessage">
                                <p runat="server" id="tituloMsgExpirado" style="text-align:center"> </p>
                                <br />
                                <p style="text-align : justify;">
                                <dx:ASPxLabel ID="lblMensaje" style="text-align : justify;" runat="server" Text=""></dx:ASPxLabel>
                                </p>
                                <div runat="server" id="divMensajeFoot" style="text-align:center;" ></div>                                         
                            </div>
                            <dx:ASPxButton ID="btnWorldPay" runat="server" ClientIDMode="AutoID" ClientInstanceName="btnWorldPay" HorizontalAlign="NotSet" ImagePosition="Left" TabIndex="154" UseSubmitBehavior="False" VerticalAlign="NotSet" Wrap="Default" BackColor="White" ForeColor="White" Visible="true" ClientVisible="False" OnClick="btnWorldPay_Click" ></dx:ASPxButton>
                        </div>                                
                    </ContentTemplate>
            </asp:UpdatePanel>
           <asp:UpdatePanel ID="UpdatePanel20" runat="server" >
                <ContentTemplate>
                    <div class="dvPopUpOverComproPago" style="display:none;">
                        <div id='overpay3'></div>
                        <div id="contentComproPago" class="popUpMessageCompropago">
                            <asp:Literal ID="ltlResultCompropago" runat="server"></asp:Literal>                                      
                        </div>
                    </div>                                
                </ContentTemplate>
            </asp:UpdatePanel>            
      </form>
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="styles/detallePago.css" />           
</body>
</html>