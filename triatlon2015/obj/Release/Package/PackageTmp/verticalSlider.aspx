﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="verticalSlider.aspx.cs" Inherits="travesia.verticalSlider" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="styles/slider-vertical.css" />
    <script src="Scripts/jquery-1.11.3.js" ></script>
    <script src="Scripts/slider-vertical.js" ></script>
    <script type="text/javascript">
        $(document).ready(function () {
            moverSlider();
            $(".bajar-slider").click(function () {
                bajarSlider();
            });

            $(".subir-slider").click(function () {
                subirSlider();
            });

            $(".slider-vertical").mouseover(function () {
                verificar = 0;
            });

            $(".slider-vertical").mouseout(function () {
                verificar = 1;
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="containerSlider">
  <div class="contenido">

    <div class="nivel slider-vertical">
    	<h3>Noticias</h3>
        <div class="contenedor-slider">

        	<div class="bloque-slider">
                <div class="modulo-slider">
                    <h4><a href="#">Titular 1</a></h4>
                    <p>Descripción 1</p>
                </div>
                <!-- fin modulo-noticias-slide -->
                <div class="modulo-slider">
                    <h4><a href="#">Titular 2</a></h4>
                    <p>Descripción 2</p>
                </div>
                <!-- fin modulo-noticias-slide -->
                <div class="modulo-slider">
                    <h4><a href="#">Titular 3</a></h4>
                    <p>Descripción 3</p>
                </div>
                <!-- fin modulo-noticias-slide -->
                <div class="modulo-slider">
                    <h4><a href="#">Titular 4</a></h4>
                    <p>Descripción 4</p>
                </div>
                <!-- fin modulo-noticias-slide -->
                <div class="modulo-slider">
                    <h4><a href="#">Titular 5</a></h4>
                    <p>Descripción 5</p>
                </div>
                <!-- fin modulo-noticias-slide -->
            </div>
            <!-- fin bloque-slider -->
        	<p class="mover-slider-vertical"><a class="subir-slider" >Subir noticia</a> <a class="bajar-slider">Bajar noticia</a></p>
        </div>
      <!-- fin contenedor-noticias-slide -->
       	<p class="vinculo-especial2">
        	<a href="#">Ver más noticias</a>
        </p>

    </div>
    <!-- fin nivel slide-vertical -->


  </div>
  <!-- fin	contenido -->
</div>
<!-- fin	container -->
    </form>
</body>
</html>
