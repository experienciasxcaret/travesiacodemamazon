﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="respRegistro.aspx.cs" Inherits="travesia.respRegistro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Travesia Sagrada Maya</title>
    <meta name="viewport" content="width= device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="styles/bootstrap.css" />
    <link rel="stylesheet" href="styles/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/font-awesome.min.css" />    
    <link rel="stylesheet" href="styles/triatlon.css" />
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <script src="Scripts/jquery-1.11.2.min.js" ></script>
    <script src="Scripts/bootstrap.js" ></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.metadata.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/localization/messages_es.js"></script>

</head>
<body>
    <form class="form-horizontal" id="formFinaliza" runat="server">
    <div class="container">
        <div class="row" style="margin-top:5%">
                    <div class="col-xs-12 col-sm-12">
                        
                    </div>
         </div>
        <div class="row">
            <div class="well col-xs-12 col-sm-12 col-md-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-2">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <div class="waves"></div>
                    </div>
                </div>                
                 <div class="row">
                      <div class="text-center">
                        <h3>!! Muchas Gracias!!</h3>                        
                      </div>
                 </div>
                <div class="row"  style="margin:2px; text-align:justify">
                    <h4>Estimado <asp:Label ID="lblNombre" runat="server" class="etiquetasDato" Text=""></asp:Label></h4>
                    <h4>Tu registro para la selección de canoeros de la TSM <%= DateTime.Now.AddYears(1).Year.ToString() %> ha sido completado. Para saber si fuiste seleccionado te enviaremos un correo electrónico a la cuenta
                        que registraste y la liga para realizar el pago de la cuota de inscripción. Recuerda tu lugar es intransferible, solo las personas registradas participarán en la selección.
                    </h4>
                </div>
                <br />
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                        <div class="col-xs-5 col-md-4">
                        </div>
                        <div class="col-xs-7 col-md-7">
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                    </div>
                </div>
                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                    </div>
                </div>

                <div class="row" style="margin-left:2px">
                    <div class="form-group">
                    </div>
                </div>
                <br />
                
                <div class="row">
                    <p class="col-xs-12 col-md-12" style="font-size:medium">                        
                    </p>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <div class="waves2"></div>
                    </div>
                </div>
            </div>            
        </div> <!-- fin del div class row-->       
    </div>
        <asp:HiddenField ID="hdCorreoCompetidor" runat="server" />
        <asp:HiddenField ID="hdIdVenta" runat="server" />
    </form>
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="styles/detallePago.css" />
</body>
</html>
