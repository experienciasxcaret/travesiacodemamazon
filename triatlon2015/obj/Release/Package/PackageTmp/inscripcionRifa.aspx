﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="inscripcionRifa.aspx.cs" Inherits="travesia.inscripcionRifa" %>

<%@ Register Assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" namespace="DevExpress.Web.ASPxLoadingPanel" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width= device-width, initial-scale=1.0" /> 
    <title>Rifa</title>
    <link rel="stylesheet" href="styles/bootstrap.css" />
    <link rel="stylesheet" href="styles/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="styles/triatlon.css" />
    <link rel="stylesheet" href="styles/basic.css" />

    <script src="Scripts/jquery-1.11.3.js" ></script>
    <script src="Scripts/bootstrap.js" ></script>
    <script src="Scripts/inscripcionRifa.js" ></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.metadata.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/localization/messages_es.js"></script>
    <link rel="shortcut icon" type="image/png" href="favicon.png" />
    <script type="text/javascript">

        function registrarCompetidor() {
            cbRegistrar.PerformCallback();
        }        

        function validarCompetidor() {
            cbValida.PerformCallback();
        }
        $(document).ready(function () {

            Sys.Browser.WebKit = {}; //Safari 3 is considered WebKit

            if (navigator.userAgent.indexOf('WebKit/') > -1) {
                
                Sys.Browser.agent = Sys.Browser.Firefox;

                Sys.Browser.version = parseFloat(navigator.userAgent.match(/WebKit\/(\d+(\.\d+)?)/)[1]);

                Sys.Browser.name = 'Firefox';
            }
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(initializeRequest);
            prm.add_pageLoaded(pageLoaded);
            function initializeRequest(sender, args) {
                //LoadingPanel.ShowInElement(args._postBackElement);  
                document.body.style.cursor = "wait";                
                pnlinfoproceso.Show();

            }
            function pageLoaded(sender, args) {
                var panels = args.get_panelsUpdated();
                if (panels.length > 0) {
                    document.body.style.cursor = "default";
                    pnlinfoproceso.Hide();
                }
            }            
        })
    </script>
</head>
<body>    
    <div class="container">
        <div class="cont header b-bottom">
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			    <object data="img/xelha.svg" type="image/svg+xml" class="img1"></object>
		    </div>
		    <div class="logo col-xs-8 col-sm-4" align="center">
			    <object data="img/triatlon.svg" type="image/svg+xml" class="img2"></object>
		    </div>
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			    <object data="img/triatlon_fechas.svg" type="image/svg+xml" class="img3"></object>
		    </div>
       </div>
       <div class="grid_16 wrap">
           <div id="paso" class="c1 hidden-xs">
	                <div id="pasos">
		                <ul>
			                <li id="paso1" style="color:#fff;">PASO 1</li>
			                <li id="paso2">PASO 2</li>
			                <li id="paso3">PASO 3</li>
		                </ul>
	                </div> 
                </div>
            <div id="cont_formularios" class="cont">
                <form class="form-horizontal" id="formRifa" runat="server">
                    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <dx:ASPxLabel ID="lblMensajeCabecera" runat="server" Text="" Font-Size="Large" ForeColor="#CC0000" ClientInstanceName="lblMensajeCabecera"></dx:ASPxLabel>                   
                        </div>
                    </div>
                                <div id="form_paso_a">
                                    <fieldset id="info_personal">
                                    <br />
                                    <div class="row">
                                        <div class="form-group" style="margin:5px">
                                            <div class="col-md-2"> 
                                                <label for="nombre" class="control-label" >Nombre *</label>                                            
                                            </div>	
                                            <div class="col-md-2"> 		                                            
                                                    <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtNombre" placeholder="Nombre" name="nombre1">
                                                    </asp:TextBox>
                                            </div>
                                            <div class="col-md-2"> 
                                                <label for="apellidoPaterno" class="control-label" >Apellido Paterno *</label>
                                            </div>
                                            <div class="col-md-2"> 
                                                <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtApellidoPaterno" placeholder="Apellido paterno" name="apellido1">
                                                </asp:TextBox>
                                            </div>                             
                                            <div class="col-md-2">
                                                <label for="apellidoMaterno" class="control-label" >Apellido Materno</label>
                                            </div>
                                            <div class="col-md-2"> 
                                                <asp:TextBox runat="server" class="form-control"  id="txtApellidoMaterno" placeholder="Apellido materno" name="apellido2">
                                                </asp:TextBox>
                                            </div>                                  
                                            
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row"> 
                                        <div class="form-group"  style="margin:5px">   
                                                            
                                            <div class="col-md-2">
                                                <label for="email" class="control-label">Email *</label>                                                  
                                            </div> 
                                            <div class="col-md-3">
                                                <asp:TextBox runat="server" type="email" class="{required:true} form-control" id="txtEmail" placeholder="Ingresa tu email" name="email" ></asp:TextBox>
                                            </div>
                                        </div>      
                                    </div>                                    
                                    <div class="row" style="margin:5px">                                                           
                                       <div class="form-group">
                                           <div class="col-md-2">
                                                <label for="email" class="control-label">Confirma Email *</label>
                                            </div>
                                           <div class="col-md-3">   
                                                <asp:TextBox runat="server" type="email" class="{required:true} form-control" id="txtEmail2" onpaste="return false" oncut="return false" placeholder="Confirma tu email" name="email" OnTextChanged="txtEmail2_TextChanged" ></asp:TextBox>                                                
                                            </div>  
                                           <div class="col-md-3">
                                               <dx:ASPxLabel ID="lblValidacionEmail" runat="server" Text="" Font-Size="Small" ForeColor="#CC0000"></dx:ASPxLabel>
                                           </div>
                                        </div>      
                                    </div>                                    
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="form-group" style="margin:5px">                                                    
                                                <div class="col-md-2">
                                                    <label for="sexo" class="control-label">Sexo *</label>
                                                </div>
                                                <div class="col-lg-3 selectContainer">                                                
                                                        <asp:DropDownList  class="{required: true} form-control" ID="ddSexo" runat="server" name="sexo">
                                                                <asp:ListItem Value="">Selecciona sexo</asp:ListItem>
                                                                <asp:ListItem Value="2">Masculino</asp:ListItem>
                                                                <asp:ListItem Value="3">Femenino</asp:ListItem>
                                                        </asp:DropDownList>                                                
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>                                    
                                    <div class="row">
                                        <div class="col-md-12">                 
                                            <div class="form-group" style="margin:5px">
                                                <label for="nacimiento" class="control-label">Fecha nacimiento *</label>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="form-group" style="padding:5px">
                                               <div class="col-md-12">  
                                                <div class="col-sm-1">  
                                                   <label for="anio" class="control-label">Año</label>
                                                </div>
                                                <div class="col-md-2 selectContainer">
                                                        
                                                        <asp:DropDownList  class="{required: true} form-control" ID="ddanio" runat="server" AutoPostBack="True" name="anio" OnSelectedIndexChanged="ddanio_SelectedIndexChanged">                                                    
                                                        </asp:DropDownList>                                                                                                                              
                                                </div>
                                               
                                                <div class="col-sm-1">   
                                                    <label for="mes" class="control-label">Mes</label>
                                                </div>
                                                <div class="col-md-2 selectContainer">                                                     
                                                        <asp:DropDownList  class="{required: true} form-control" ID="ddlMes" runat="server" AutoPostBack="True" name="mes" OnSelectedIndexChanged="ddlMes_SelectedIndexChanged">                                                        
                                                                <asp:ListItem Value="1" Selected="True">Enero</asp:ListItem>
                                                                <asp:ListItem Value="2">Febrero</asp:ListItem>
                                                                <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                                <asp:ListItem Value="4">Abril</asp:ListItem>
                                                                <asp:ListItem Value="5">Mayo</asp:ListItem>
                                                                <asp:ListItem Value="6">Junio</asp:ListItem>
                                                                <asp:ListItem Value="7">Julio</asp:ListItem>
                                                                <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                                <asp:ListItem Value="9">Septiembre</asp:ListItem>
                                                                <asp:ListItem Value="10">Octubre</asp:ListItem>
                                                                <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                                                <asp:ListItem Value="12">Diciembre</asp:ListItem>
                                                        </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-1">    
                                                   <label for="dia" class="control-label">Día</label> 
                                                 </div>                                               
                                                <div class="col-md-2 selectContainer">                                                    
                                                    <asp:DropDownList  class="{required: true} form-control" ID="ddldia" runat="server" AutoPostBack="True" name="dia">                                                        
                                                    </asp:DropDownList>
                                                </div>
                                                
                                            </div>                  
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <br />    
                            </fieldset>
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                         <ContentTemplate>
                                            <fieldset id="botonera_personales">				
                                                 <asp:button ID="btnContinuarA" runat="server" Text="Continuar" class="btn btn-info btn-mini" ClientInstanceName="btnContinuarA" UseSubmitBehavior="true" AutoPostBack="False" ValidateRequestMode="Disabled" />
                                                 &nbsp;&nbsp;<dx:ASPxLabel ID="lblMensajeValidacion" runat="server" Text="" ClientInstanceName="lblMensajeValidacion" Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000"></dx:ASPxLabel>
                                                <br />
                                                <asp:Label ID="Label2" runat="server" Text="Todos los campos marcados con asterisco (*), son requeridos" Font-Size="Medium" ForeColor="Red"></asp:Label>
			                                </fieldset>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                    
                                <div id="form_paso_b" class="hide">                                   
                                    <fieldset id="InformacionDireccion">
                                        <br/>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-3">
                                                        
                                                            <label for="colonia" class="control-label" >Colonia</label> 			                                            
                                                                <asp:TextBox runat="server" class="form-control"   id="txtColonia" placeholder="Colonia" name="colonia">
                                                                </asp:TextBox>
                                                        
                                                    </div>
                                                    <div class="col-md-3">
                                                        
                                                            <label for="calle" class="control-label" >Calle</label>		                                            
                                                            <asp:TextBox runat="server" class="form-control"   id="txtCalle" placeholder="calle" name="Calle">
                                                            </asp:TextBox>
                                                        
                                                     </div>
                                                </div>
                                            </div>                                            
                                            <div class="row">
                                                <div class="col-md-12">                                                        
                                                    <div class="col-md-3">
                                                        <!-- <div class="form-group" style="padding:5px"> -->
                                                            <label for="numeroExterior" class="control-label" >Número Exterior</label> 			                                            
                                                                <asp:TextBox runat="server" class="form-control"   id="txtNoExterior" placeholder="Número Exterior" name="numeroInterior">
                                                                </asp:TextBox>
                                                        <!--</div> -->    
                                                     </div>
                                                    <div class="col-md-3">
                                                        
                                                            <label for="numeroInterior" class="control-label" >Número Interior</label>                            
                                                            <asp:TextBox runat="server" class="form-control"   id="txtNoInterior" placeholder="Número Interior" name="numeroInterior">
                                                            </asp:TextBox>                               
                                                        
                                                    </div>
                                                </div>
                                            </div>                                            
                                         <asp:UpdatePanel ID="upFormulario" runat="server">
                                            <ContentTemplate>
                                                <div class="row">
                                                <div class="col-md-12">
                                                                                                          
                                                        <div class="col-md-3 selectContainer">                                                        
                                                            <label for="pais" class="control-label">País *</label>
                                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlPais" runat="server" name="pais" OnTextChanged="ddlPais_TextChanged" AutoPostBack="True">
                                                            </asp:DropDownList>  
                                                        </div>    
                                                     
                                                        <div class="col-md-3 selectContainer">                                                            
                                                            <label for="estado" class="control-label">Estado</label>
                                                            <asp:DropDownList  class="form-control" ID="ddlEstado" runat="server" name="estado">
                                                                    <asp:ListItem Value="1">Seleccione Estado</asp:ListItem>                                                                
                                                            </asp:DropDownList>                                                                                             
                                                        </div>
                                                    
                                                </div>
                                            </div>
                                            </ContentTemplate>
                                         </asp:UpdatePanel>                                            
                                            <div class="row">
                                                <div class="col-md-12"> 
                                                    <div class="col-md-3">                                               
                                                        
                                                            <label for="ciudad" class="control-label" >Ciudad</label> 			                                            
                                                            <asp:TextBox runat="server" class="form-control"   id="txtCiudad" placeholder="Ciudad" name="ciudad">
                                                            </asp:TextBox>
                                                        
                                                      </div>                                                                                         
                                                </div>
                                            </div>                                        
                                            <div class="row">
                                                <div class="form-group" style="padding:5px">
                                                    <div class="col-md-12">
                                                        <div class="col-md-2">                                                                                                                                                                                        
                                                        <label for="telefono" class="control-label">Teléfono Fijo</label>  
                                                        </div>     
                                                        <div class="col-md-1">
                                                        <asp:TextBox runat="server" width="60px" class="{number: true} form-control" id="txtTelefonofijo1" placeholder="Lada" name="lada1">
                                                        </asp:TextBox>
                                                        </div>
                                                        <div class="col-md-2">
                                                        <asp:TextBox runat="server" class="{number: true} form-control"   id="txtTelefonofijo2" placeholder="Número" name="numero1">
                                                        </asp:TextBox>
                                                        </div>                                                                                                                                                                           
                                                         </div>                            
                                                </div>
                                            </div>
                                                                                   
                                            <div class="row">
                                                <div class="form-group" style="padding:5px">
                                                        <div class="col-md-12">                                                        
                                                            <div class="col-md-2">                                    
                                                                <label for="telefonoCelular" class="control-label">Teléfono Celular *</label>   
                                                              </div>
                                                            <div class="col-md-1">                                              
                                                                <asp:TextBox runat="server" width="60px" class="{required:true, number: true, rangelength: [2,4]} form-control" id="txtCelular1" placeholder="Lada" name="lada2">
                                                                    </asp:TextBox>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <asp:TextBox runat="server" class="{required:true, number: true, rangelength: [2,8]} form-control"   id="txtCelular2" placeholder="Número" name="numero2">
                                                                </asp:TextBox>
                                                            </div>    
                                                         </div>                            
                                                 </div>
                                            </div>
                                            <br />
                                    </fieldset>  
                                           <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate> 
                                                    <fieldset id="botonera_domicilio">
                                                        <div class="col-md-12">
				                                            <a id="back_paso_a" class="btn btn-info btn-mini">Regresar</a>                                                 
                                                            <asp:Button ID="btnContinuarB" runat="server" Text="Continuar" class="btn btn-info btn-mini" name="NameBtncontinuarB"  UseSubmitBehavior="true" AutoPostBack="False"/>
                                                            <br />
                                                            <asp:Label ID="Label1" runat="server" Text="Todos los campos marcados con asterisco (*), son requeridos" Font-Size="Medium" ForeColor="Red"></asp:Label>                  
                                                         </div>
			                                        </fieldset> 
                                                </ContentTemplate>
                                           </asp:UpdatePanel>                                                
                                </div>                            
                            
                                <div id="form_paso_c" class="hide">
                                    <fieldset id="categoria">
                                        <br />
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            
                                            <div class="row">                                                                                  
                                                <div class="form-group" style="padding:5px">
                                                    <div class="col-md-12">                                                        
                                                        <div class="col-md-3">
                                                            <label for="categoria" class="control-label">Categoría a participar *</label>
                                                        </div>
                                                        <div class="col-md-3 selectContainer">
                                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlCategoria" runat="server" AutoPostBack="True" name="categoria" OnSelectedIndexChanged="ddlCategoria_SelectedIndexChanged" CausesValidation="True">
                                                                    <asp:ListItem Value="">Seleccione categoria</asp:ListItem>                                                           
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                     <asp:RequiredFieldValidator ID="rvCategoria" ControlToValidate="ddlCategoria" runat="server" ErrorMessage="requerido" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000" EnableClientScript="False"></asp:RequiredFieldValidator>
                                                </div>                                                
                                            </div>
                                        </ContentTemplate>
                                        </asp:UpdatePanel>

                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                        <ContentTemplate>                                                                                                                                  
                                            <div class="form-group" style="padding:5px">
                                                <div class="row" id="dvPlayera">
                                                    <div class="col-md-12">
                                                        <div class="col-md-3">
                                                            <asp:Label  for="playera" ID="lblTamanioPlayera" runat="server" class="control-label" Text="Tamaño de playera *" visible="False" Font-Bold="True"></asp:Label>
                                                        </div>
                                                        <div class="col-md-3 selectContainer">
                                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlTallasPlayeras" runat="server" AutoPostBack="false" name="ddlTallasPlayeras" Visible="False" CausesValidation="True">
                                                                    <asp:ListItem Value="">Seleccione Tamaño de playera *</asp:ListItem>                                                           
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="rvTallaPlayera" ControlToValidate="ddlTallasPlayeras" runat="server" Display="Dynamic" ErrorMessage="requerido" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>                                                    
                                                </div>                                                
                                            </div>
                                        </ContentTemplate>
                                        </asp:UpdatePanel>
                                        
                                                                                                                         
                                                <div class="form-group" style="padding:5px">
                                                    <div class="row" >
                                                        <div class="col-md-12">
                                                            <div class="col-md-4" >
                                                                <label for="participaciones" class="control-label">¿Cuántas veces has participado?</label>
                                                            </div>
                                                    
                                                            <div class="col-md-2">
                                                            <asp:DropDownList  class="form-control" ID="ddlNoParticipaciones" runat="server" name="participaciones">
                                                                    <asp:ListItem Value="0">0</asp:ListItem>
                                                                    <asp:ListItem Value="1">1</asp:ListItem>
                                                                    <asp:ListItem Value="2">2</asp:ListItem>
                                                                    <asp:ListItem Value="3">3</asp:ListItem>
                                                                    <asp:ListItem Value="4">4</asp:ListItem>
                                                                    <asp:ListItem Value="5">5</asp:ListItem>
                                                                    <asp:ListItem Value="6">6</asp:ListItem>
                                                                    <asp:ListItem Value="7">7</asp:ListItem>
                                                           </asp:DropDownList>  
                                                            </div>
                                                        </div>
                                                </div>                            
                                            
                                        </div>                                        
                                        <asp:UpdatePanel ID="upRelevo2" runat="server">
                                            <ContentTemplate>                                                
                                                <div class="row" style="display: inline-block;width: 200px;padding: 5px;">
                                                    <div class="col-md-10">
                                                        <asp:Label ID="lblRelevo2" runat="server" class="control-label" Text="Datos Relevo 2" visible="False" Font-Bold="True"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">                                                    
                                                        <div class="form-group" style="margin:5px">                                                            
                                                                <div class="col-md-2">                                                        
                                                                    <asp:Label ID="lblNombreRelevo2" runat="server" class="control-label" Text="Nombre *" visible="False" Font-Bold="True"></asp:Label>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"  visible="False" id="txtNombre2" placeholder="ingresa tu nombre" name="nombre2">
                                                                    </asp:TextBox>
                                                                </div>
                                                                <div class="col-md-2">                                                   
                                                                    <asp:Label ID="lblPaternoRelevo2" runat="server" class="control-label" Text="Apellido Paterno *" visible="False" Font-Bold="True"></asp:Label>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtPaterno2" visible="False" placeholder="ingresa tu apellido paterno" name="apellido1">
                                                                    </asp:TextBox>
                                                                </div>   
                                                                <div class="col-md-2">                                                                              
                                                                    <asp:Label ID="lblMaternoRelevo2" runat="server" class="control-label" Text="Apellido Materno" visible="False" Font-Bold="True"></asp:Label>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <asp:TextBox runat="server" class="form-control"  id="txtMaterno2" visible="False" placeholder="ingresa tu apellido materno" name="apellido2">
                                                                    </asp:TextBox>
                                                                </div>                                                            
                                                        </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="margin:5px">
                                                        <div class="col-lg-2">                                                                                                                    
                                                            <asp:Label ID="lblSexoRelevo2" runat="server" class="control-label" Text="Sexo *" visible="False" Font-Bold="True"></asp:Label>
                                                        </div>
                                                        <div class="col-lg-2 selectContainer"> 
                                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlSexo2" runat="server" name="sexo" visible="False">
                                                                    <asp:ListItem Value="">Seleccione sexo</asp:ListItem>
                                                                    <asp:ListItem Value="2">Masculino</asp:ListItem>
                                                                    <asp:ListItem Value="3">Femenino</asp:ListItem>
                                                            </asp:DropDownList>                           
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" id="dvPlayera2">                                                                                                                                    
                                                    <div class="form-group" style="margin:5px">
                                                        <div class="col-md-2">
                                                            <asp:Label  for="playera" ID="lblPlayeraRelevo2" runat="server" class="control-label" Text="Tamaño playera *" visible="False" Font-Bold="True"></asp:Label>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlPlayera2" runat="server" AutoPostBack="false" name="ddlPlayera2" Visible="False">
                                                                    <asp:ListItem Value="">Seleccione Tamaño de playera *</asp:ListItem>                                                           
                                                            </asp:DropDownList>
                                                        </div>                            
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="upRelevo3" runat="server">
                                            <ContentTemplate>                                                
                                                <div class="row" style="display: inline-block;width: 200px;padding: 5px;">
                                                    <div class="col-md-10">
                                                        <asp:Label ID="lblRelevo3" runat="server" class="control-label" Text="Datos Relevo 3" visible="False" Font-Bold="True"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="margin:5px">                                                        
                                                            <div class="col-md-2">                                                                                                                     
                                                                <asp:Label ID="lblNombreRelevo3" runat="server" class="control-label" Text="Nombre *" visible="False" Font-Bold="True"></asp:Label>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control" id="txtNombre3" visible="False" placeholder="ingresa tu nombre" name="nombre2">
                                                                </asp:TextBox>
                                                            </div>
                                                            <div class="col-md-2">                                                    
                                                                <asp:Label ID="lblPaternoRelevo3" runat="server" class="control-label" Text="Apellido Paterno *" visible="False" Font-Bold="True"></asp:Label>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtPaterno3" visible="False" placeholder="ingresa tu apellido paterno" name="apellido1">
                                                                </asp:TextBox>
                                                            </div>
                                                            <div class="col-md-2">                                             
                                                                <asp:Label ID="lblMaternoRelevo3" runat="server" class="control-label" Text="Apellido Materno" visible="False" Font-Bold="True"></asp:Label>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <asp:TextBox runat="server" class="form-control"  id="txtMaterno3" visible="False" placeholder="ingresa tu apellido materno" name="apellido2">
                                                                </asp:TextBox>
                                                            </div>                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="margin:5px">
                                                        <div class="col-md-2">                                                                                                                    
                                                            <asp:Label ID="lblSexoRelevo3" runat="server" class="control-label" Text="Sexo *" visible="False" Font-Bold="True"></asp:Label>
                                                        </div>
                                                        <div class="col-lg-2 selectContainer">
                                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlSexo3" visible="False" runat="server" name="sexo">
                                                                    <asp:ListItem Value="">Seleccione sexo</asp:ListItem>
                                                                    <asp:ListItem Value="2">Masculino</asp:ListItem>
                                                                    <asp:ListItem Value="3">Femenino</asp:ListItem>
                                                            </asp:DropDownList>                           
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" id="Div1">
                                                    <div class="form-group" style="margin:5px">
                                                        <div class="col-md-2">
                                                            <asp:Label  for="playera" ID="lblPlayeraRelevo3" runat="server" class="control-label" Text="Tamaño playera *" visible="False" Font-Bold="True"></asp:Label>
                                                        </div>
                                                        <div class="col-md-2 selectContainer">
                                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlPlayera3" runat="server" AutoPostBack="false" name="ddlPlayera3" Visible="False">
                                                                    <asp:ListItem Value="">Seleccione Tamaño de playera *</asp:ListItem>                                                           
                                                            </asp:DropDownList>
                                                        </div>                            
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </fieldset>
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset id="botonera_categoria">
				                                    <a id="back_paso_b" href="" class="btn btn-info btn-mini">Regresar</a>
				                        
                                                    <dx:ASPxButton ID="btnTerminar" runat="server" Text="Terminar" class="btn btn-info btn-mini" name="NameBtnContinuarC"  CssPostfix="&quot;btn btn-info btn-mini" ClientInstanceName="NameBtnContinuarC" UseSubmitBehavior="False" AutoPostBack="False" ValidationGroup="validaGuardar">
                                                    </dx:ASPxButton>
			                                    </fieldset>
                                                <br />
                                                <dx:ASPxLabel ID="lblMensaje" runat="server" Text="" ClientInstanceName="lblMensaje" Font-Bold="True" Font-Size="Medium"></dx:ASPxLabel>
                                                <br />
                                                <asp:Label ID="lblInformativo" runat="server" Text="Todos los campos marcados con asterisco (*), son requeridos" Font-Size="Medium" ForeColor="Red"></asp:Label>
                                            </div>
                                        </div>              
                                    </ContentTemplate>
                                    </asp:UpdatePanel>                                
                                </div>
                    <asp:UpdatePanel ID="UpdatePanelHD" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="hdCnValidation" runat="server" value="0"/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
       </div>                 
    <dx:ASPxCallback ID="cbRegistrar" runat="server" ClientInstanceName="cbRegistrar" OnCallback="cbRegistrar_Callback" ClientIDMode="AutoID">
        <ClientSideEvents EndCallback="function(s, e) {
pnlinfoproceso.Hide();
if (s.cpIsMessage != '') 
{		
    responseRifa(s.cpIsMessage)
}
else
{
    alert('vacío');
}
}" BeginCallback="function(s, e) {
    
	pnlinfoproceso.Show();
}" />
        </dx:ASPxCallback>
    <dx:ASPxCallback ID="cbValida" runat="server" ClientInstanceName="cbValida" OnCallback="cbValida_Callback" ClientIDMode="AutoID">
    </dx:ASPxCallback>
        <dx:ASPxLoadingPanel ID="pnlinfo" runat="server" ClientInstanceName="pnlinfoproceso" Modal="True">
        </dx:ASPxLoadingPanel>        
    </div>    
</body>  
</html>
