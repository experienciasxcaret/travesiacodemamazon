﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="usrUpload.ascx.cs" Inherits="travesia.usrControl.usrUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
    <form id="form1">
    <div id="uploadarea">
        <asp:Literal ID="uploadMsg" runat="server"></asp:Literal>
        <div id="Div1" runat="server">
            <input type="file" size="45" runat="server" id="FileUpload1" />
            <asp:Label ID="Label1" runat="server" />
        </div>
        <div id="Div2" runat="server">
            <input type="file" size="45" runat="server" id="FileUpload2" />
            <asp:Label ID="Label2" runat="server" />
        </div>
        <div id="Div3" runat="server">
            <input type="file" size="45" runat="server" id="FileUpload3" />
            <asp:Label ID="Label3" runat="server" />
        </div>
        <br />
        <span>
            <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" 
            Text="Upload Images" /></span>
    </div>
    
    <p></p>
    <p></p>
    
    <asp:Repeater ID="PhotoRepeater" runat="server">
  	    <ItemTemplate>
  	        <div>
                <img src="/Storage/<%#DataBinder.Eval(Container.DataItem, "FolderName")%>/100x100_<%#DataBinder.Eval(Container.DataItem, "URL")%>" alt='<%#DataBinder.Eval(Container.DataItem, "URL")%>' />
  	            <asp:LinkButton ID="DeleteImageBtn" runat="server" Text="Delete" 
                    CommandName="mediaId" OnCommand="DeleteImageBtn_Click" 
                    CommandArgument='<%#DataBinder.Eval(Container.DataItem, "MediaId")%>'>
                    </asp:LinkButton>
  	        </div>
  	    </ItemTemplate>
  	</asp:Repeater>
    </form>
</body>

</html>

