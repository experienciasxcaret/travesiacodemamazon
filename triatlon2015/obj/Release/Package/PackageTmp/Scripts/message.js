﻿$(document).ready(function () {

    $('#divMsg').click(function () {
        $(this).animate({ bottom: -$(this).outerHeight() }, 500);
    });
});

var myMessages = ['info', 'warning', 'error', 'success']; // Mensajes definidos	 
function hideAllMessages() {
    var messagesHeights = new Array();

    for (i = 0; i < myMessages.length; i++) {
        messagesHeights[i] = $('.' + myMessages[i]).outerHeight();
        $('.' + myMessages[i]).css('bottom', -messagesHeights[i]); //mover el div fuera de la vista	  
    }
}

function showMessage(type, info, message) {
    alert(message);
    var dv = document.getElementsByTagName('fcmessage'); // Busca el Div
    console.log(dv);
    var lblInfo = document.getElementsByTagName('fclblInfo'); // Busca el label de info
    var lblMessage = document.getElementsByTagName('fcldivMsg'); // Busca el label de mensaje
    lblInfo.innerHTML = info;
    lblMessage.innerHTML = message;

    dv.className = type + ' message'; // Asigna el Css del tipo de mensaje

    hideAllMessages();
    $('.' + type).animate({ bottom: "0" }, 500);    
}