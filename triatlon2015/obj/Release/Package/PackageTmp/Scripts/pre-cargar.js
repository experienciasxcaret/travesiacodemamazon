﻿$(document).ready(function () {

    $('body').on('click', '#btnTerminar', function (event) {
        //$("#btnTerminar").click(function (event) {
        setTimeout(function () { pnlinfoproceso.Show(); }, 1000);
        //pnlinfoproceso.Show();
        event.preventDefault();
        $('#formPreCargar').submit();
    });
    $("#formPreCargar").submit(function (e) {
        // alert('lp');
        e.preventDefault();
    }).validate({
        //$('#formRifa').validate({
        submitHandler: function (boton) {
            //alert('paso');
            //__doPostBack('btnContinuarA', 'btnContinuarA_Click');
            pnlinfoproceso.Show();
            var email = $('#txtEmail').val().trim();
            var email2 = $('#txtEmail2').val().trim();
            $('#lblValidacionEmail').text('');
            // si cnPrecargado tiene distinto de cero quiere decir que tiene un idContactoEvento y no se valida los correos
            if (email != 'infotriatlon@experienciasxcaret.com.mx' && email != 'nfigueroa@experienciasxcaret.com' && email != 'hspech@gmail.com' && email != 'cgonzalezm86@aol.com' && email != 'rcarrillo@experienciasxcaret.com.mx' && email != 'lherrera@experienciasxcaret.com.mx' && email != 'cgonzalezm@experienciasxcaret.com.mx' && email != "faraujo@experienciasxcaret.com" && email != 'lrecinos@experienciasxcaret.com.mx') {
                if (email.toUpperCase() == email2.toUpperCase()) {

                    $('#lblValidacionEmail').text('');
                   // __doPostBack('btnContinuarA', 'btnContinuarA_Click');
                    var dataIn = '{' + '"strEmail":"' + email + '"}';
                    console.log('antes del post');
                    $.ajax({
                        url: "../ajax/ajaxRifa.svc/validaCompetidorRifa",
                        type: "POST",
                        //async: false,
                        contentType: "application/json; charset=utf-8",
                        data: dataIn,
                        dataType: "json",
                        success: function (data) {

                            var object = JSON.parse(data.d);
                            //console.log(object);
                            if (object.Error == '0') {
                                //console.log(object.Response);
                                // si pasó la validacion de existendia del correo ingresado nos vamos a la siguiente sección
                                if (object.Response == 'True') {
                                    __doPostBack('btnContinuarA', 'btnContinuarA_Click');
                                }
                                else {
                                    $('#lblMensajeValidacion').text('Ya existe registro con el correo ' + email);
                                    pnlinfoproceso.Hide();
                                    return false;
                                }
                                //Alert the persons name                                
                            }
                        },
                        error: function (error) {
                            //alert("Error: " + error.Error);
                            $('#lblMensajeValidacion').text(error.Error);
                            return false;
                        }
                    });
                }
                else {
                    console.log('no paso validación');
                    $('#lblValidacionEmail').text('Los correos ingresados no coincides favor de validarlos');
                    $('#txtGralEmail2').focus();
                    pnlinfoproceso.Hide();
                    return false;
                }
            }
            else {
                __doPostBack('btnContinuarA', 'btnContinuarA_Click');
                pnlinfoproceso.Hide();
                return false;
            }
        },
        errorPlacement: function (error, element) {
            pnlinfoproceso.Hide();
            error.appendTo(element.parent().append());
            return false;
        }
    });
});