﻿$(document).ready(function () {
    var allowedExtensions = /(\jpg|\pdf|\png|\jpeg)$/i;
    console.log('ready');
    // detectar que botón se esta accionando
    $('body').on('click', '#cont_formularios a', function (elemento) {
        elemento.preventDefault();

        mostrar = $(this).attr('id');
        //console.log(mostrar);
        // Seleccionar la sección a mostrar
        if (mostrar == 'back_paso_a') {
            //alert('back');
            $('#form_paso_b').removeClass("show");
            $('#form_paso_a').removeClass("hide");
            $('#form_paso_a').addClass("show");
            $('#form_paso_b').addClass("hide");
            if ($("#paso").length) {
                //...
                //alert("si existe #paso")
                $('#paso').addClass("c1");
                $('#paso').removeClass("c2");
                $('#paso1').css("color", "#fff");
                $('#paso2').css("color", "#000000");
            }
            
        }
        else if (mostrar == 'back_paso_b') {
            $('#form_paso_c').removeClass("show");
            $('#form_paso_b').removeClass("hide");
            $('#form_paso_b').addClass("show");
            $('#form_paso_c').addClass("hide");
            if ($("#paso").length) {
                $('#paso').removeClass("c3");
                $('#paso').addClass("c2");
                $('#paso2').css("color", "#fff");
                $('#paso3').css("color", "#000000");
            }
        }
        else if (mostrar == 'go_terminar') {
            var dataForm = $('#form_a, #form_b').serialize();
            //alert(dataForm);
        }
    });
    //$(".Corredor").click(function () {    
    //$("input[type=checkbox] .Corredor").click(function(){
    $('body').on('click', '.Corredor', function (elemento) {
        //elemento.preventDefault();
        mostrar = $(this).attr('id');
        var contadorCheked = 0;
       // console.log($(this));
        console.log(mostrar);
        //var checkboxPulsado = $(this).find('input:checkbox');
        //console.log("chek" + checkboxPulsado.id);

        $('.Corredor').each(function () {
            //$(this).//this is the checked checkbox
            //console.log($(this).attr('id'));
            if ($(this).is(":checked")) {
                contadorCheked = contadorCheked + 1;
                if (parseInt(contadorCheked) > 1) {
                    alert("Ya tiene seleccionado corredor");
                    elemento.preventDefault()
                }
            }            
        });

       /* if (parseInt(contadorCheked) > 1) {            
            alert("mas de uno " + contadorCheked)
        }
        else {
            alert("solo uno " + contadorCheked);
        }*/


        /*if ($("#" + mostrar).is(':checked')) {
            alert('Se hizo check en el checkbox.');
        } else {
            alert('Se destildo el checkbox');
        }*/
    });

    $('body').on('click', '.Nadador', function (elemento) {        
        mostrar = $(this).attr('id');
        var contadorCheked = 0;        
        console.log(mostrar);
        $('.Nadador').each(function () {            
            if ($(this).is(":checked")) {
                contadorCheked = contadorCheked + 1;
                if (parseInt(contadorCheked) > 1) {
                    alert("Ya un competidor tiene seleccionado Nadador");
                    elemento.preventDefault()
                }
            }
        });        
    });

    $('body').on('click', '.Ciclista', function (elemento) {        
        mostrar = $(this).attr('id');
        var contadorCheked = 0;        
        console.log(mostrar);
        $('.Ciclista').each(function () {
            //$(this).//this is the checked checkbox
            //console.log($(this).attr('id'));
            if ($(this).is(":checked")) {
                contadorCheked = contadorCheked + 1;
                if (parseInt(contadorCheked) > 1) {
                    alert("Ya un competidor tiene seleccionado Ciclista");
                    elemento.preventDefault()
                }
            }
        });
    });

    /*$("#back_paso_a").on("click",function () {
        alert('back');
        $('#form_paso_b').removeClass("show");
        $('#form_paso_a').removeClass("hide");
        $('#form_paso_a').addClass("show");
        $('#form_paso_b').addClass("hide");
    });*/

    $("#formRifa").submit(function (e) {
       // alert('lp');
        e.preventDefault();
    }).validate({
    //$('#formRifa').validate({
        submitHandler: function(boton) {                        

            if ($("#form_paso_a").is(":visible") == true) {
                //alert('paso validacion');                
                //$("#ddlCategoria option[text=Olimpico]").attr("selected", "selected");                
                /*request = $.ajax({
                url: "forms.php",
                type: "post",
                data: serializedData
                });*/
                var cnPrecargado = $("#cnIdContactoPrecargado").val().trim();
                var email = $('#txtGralEmail').val().trim();                
                var email2 = $('#txtGralEmail2').val().trim();
                //console.log('correo1 ' + email.toUpperCase() + ' correo2 : ' + email2.toUpperCase());
                // si cnPrecargado tiene distinto de cero quiere decir que tiene un idContactoEvento y no se valida los correos
                if (cnPrecargado == "0" && email != 'rhernandezmi@experienciasxcaret.com.mx' && email != 'nfigueroa@experienciasxcaret.com.mx' && email != 'hspech@gmail.com' && email != 'cgonzalezm86@aol.com' && email != 'rcarrillo@experienciasxcaret.com.mx' && email != 'lherrera@experienciasxcaret.com.mx' && email != 'cgonzalezm@experienciasxcaret.com.mx' && email != "faraujo@experienciasxcaret.com" && email != 'lrecinos@experienciasxcaret.com.mx') {
                    if (email.toUpperCase() == email2.toUpperCase()) {
                        
                        $('#lblValidacionEmail').text('');
                        var dataIn = '{' + '"strEmail":"' + email + '"}';
                        //console.log('antes del post');
                        $.ajax({
                            url: "ajax/ajaxRifa.svc/validaCompetidorRifa",
                            type: "POST",
                            //async: false,
                            contentType: "application/json",
                            data: dataIn,
                            dataType: "json",
                            success: function (data) {

                                var object = JSON.parse(data.d);
                                //console.log(object);
                                if (object.Error == '0') {
                                    //console.log(object.Response);
                                    // si pasó la validacion de existendia del correo ingresado nos vamos a la siguiente sección
                                    if (object.Response == 'True') {
                                        if ($("#paso").length) {                                           
                                            //alert("si existe #paso")
                                            $('#paso').removeClass("c1");
                                            $('#paso').addClass("c2");
                                            $('#paso1').css("color", "#000000"); // negro
                                            $('#paso2').css("color", "#fff");
                                            $('#paso3').css("color", "#000000");
                                        }
                                        $('#lblMensajeValidacion').text('');
                                        $('#form_paso_a').removeClass("show");
                                        $('#form_paso_a').addClass("hide");//#
                                        $('#form_paso_b').addClass("show");
                                        //// parte para el cambio dinamico de la barra de navegación de pasos                                        
                                        ////
                                        pnlinfoproceso.Hide();
                                        __doPostBack('btnContinuarA', 'btnContinuarA_Click');                                        
                                        return false;
                                    }
                                    else if (object.Response == 'False'){
                                        $('#lblMensajeValidacion').text('Ya existe registro con el correo ' + email);
                                        pnlinfoproceso.Hide();
                                        return false;
                                    }
                                    else {
                                        pnlinfoproceso.Hide();
                                        alert("se refrescará la pantalla para continuar");                                        
                                        location.reload(true);
                                        return false;
                                    }

                                    //Alert the persons name                                
                                }
                            },
                            error: function (error) {
                                //alert("Error: " + error.Error);
                                $('#lblMensajeValidacion').text(error.Error);
                                return false;
                            }
                        });
                    }
                    else {
                        $('#lblValidacionEmail').text('Los correos ingresados no coinciden favor de validarlos');
                        $('#txtGralEmail2').focus();
                        pnlinfoproceso.Hide();                        
                    }
                }
                else {
                    //var btn = document.getElementById("btnContinuarA");
                    //console.log(btn);                    
                    //btnContinuarA.DoClick();          
                    if ($("#paso").length) {
                        //... $('#paso').removeClass("c1");
                        /*;if ($("#paso").length) {                                           
                                            //alert("si existe #paso")
                                            $('#paso').removeClass("c1");
                                            $('#paso').addClass("c2");
                                            $('#paso1').css("color", "#000000"); // negro
                                            $('#paso2').css("color", "#fff");
                                            $('#paso3').css("color", "#000000");
                                        }
                        $('#paso1').css("color", "#000000"); // negro
                        $('#paso2').css("color", "#fff");
                        $('#paso3').css("color", "#000000");*/
                        //alert("si existe #paso")
                        $('#paso').removeClass("c1");
                        $('#paso').addClass("c2");
                       // $('.paso').addClass("c3");
                        $('#paso1').css("color", "#000000"); // negro
                        $('#paso2').css("color", "#fff");
                        $('#paso3').css("color", "#000000");
                    }
                    $('#lblMensajeValidacion').text('');
                    $('#form_paso_a').removeClass("show");
                    $('#form_paso_a').addClass("hide");
                    $('#form_paso_b').addClass("show");
                    //;
                    __doPostBack('btnContinuarA', 'btnContinuarA_Click');
                    //__doPostBack('FileUpload', 'btnSubmit_Click1');
                    return false;
                }
                    //btnContinuarA.DoClick();
                    //__doPostBack('btnContinuarA', 'OnClick');                                                           
            } // fin if ($("#form_paso_a").is(":visible") == true) {            
            if ($("#form_paso_b").is(":visible") == true) {
                //alert('paso validacion 2');
                if ($("#paso").length) {
                    //...
                    //alert("si existe #paso")
                    $('#paso').removeClass("c2");
                    $('#paso').addClass("c3");
                    $('#paso2').css("color", "#000000");
                    $('#paso3').css("color", "#fff");
                }
                $('#form_paso_b').removeClass("show");
                $('#form_paso_b').addClass("hide");
                $('#form_paso_c').addClass("show");
                return false;
            }
            if ($("#form_paso_c").is(":visible") == true) {
                console.log('form_paso_c');
                //document.getElementById('btnSubmit').click();
                //Button2.DoClick();
                //$(this).ajaxSubmit(function (data) {
                    //$('#updateDiv').html(data); // or append/prepend/whatever
                //})
                //__doPostBack('btnTerminar', 'btnTerminar_Click');
                var data = new FormData();
                var files = $("#FileUpload2").get(0).files;
                console.log(files);
                // Add the uploaded image content to the form data collection
                if (files.length > 0) {
                    data.append("UploadedFile", files[0]);
                }
                //showStepTres('true');
                // Make Ajax request with the contentType = false, and procesDate = false
                /*var ajaxRequest = $.ajax({
                    type: "POST",
                    url: "inscripcionGeneral.aspx/uploadFiles2",
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    data: data
                });*/
                /*var options = {};
                options.url = "FileUploadHandler.ashx";
                options.type = "POST";
                options.data = data;
                options.contentType = false;
                options.processData = false;
                options.success = function (result) { alert(result); };
                options.error = function (err) { alert(err.statusText); };
                $.ajax(options); */               
                return false;
            }
        },        
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().append());            
        }
    });   

   /* $('#formRifa').validate({
        submitHandler: function () {
            alert('paso validacion 2');
            $('#form_paso_b').hide();
            $('#form_paso_c').addClass("show");
            return false;
        },
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().append());
        }
    });*/    
    //$('#divMsg').click(function () {
    $('body').on('click', '#divMsg', function (elemento) {
        $(this).animate({ bottom: -$(this).outerHeight() }, 500);
        setTimeout(function () {
            // Add to document using html, rather than tmpContainer
            $(this).remove();
        }, 500); // 0 milliseconds
    });

    $('body').on('click', '#btnContinuarA', function (elemento) {
    //$("#btnContinuarA").click(function (event) {        
        //event.preventDefault();  
        var valueHdImg = $("#hdImg").val();
        if (valueHdImg == "1")
            $('#formRifa').submit();
        else{
            $("#imgUpload").focus();
            
            showMessage('errorMsg', 'Falta Foto', 'Favor de subir tu foto para continuar con el proceso');

            $('html, body').animate({
                scrollTop: $("#form_paso_a").offset().top
            }, 600);

           /* $("body, html").animate({
                scrollTop: $($('.divImageUpload').attr('href')).offset().top
            }, 600);*/
        }
        //$(this).trigger('click');        
    });

    $('body').on('click', '#btnContinuarB', function (elemento) {
        //$("#btnContinuarA").click(function (event) {
        //alert("hola");       
        var valRespNado = ($("input:radio[name='rblSiNo']:checked").val());
        console.log(valRespNado);
        if (valRespNado == "0") {
            alert('<img src="img/Warning.png" width="50" height="50">Por tu seguridad es importante saber nadar en aguas abiertas, por lo que no podrás seguir con el registro<br>');
            return false;
        }
        else
        {
            event.preventDefault();
            $('#formRifa').submit();
        }        
        //$(this).trigger('click');
        //do stuff here
    });

    $('body').on('click', '#btnTerminar', function (event){
        //$("#btnTerminar").click(function (event) {
        //alert("prevent")
         // antes de hacer post al form validar los archivos que se van a subir y pos subir
        //__doPostBack('btnTerminar', 'uploadImage');
        //__doPostBack('FileUpload2', 'uploadFiles');
        pnlinfoproceso.Show();
        event.preventDefault();
        var element = document.getElementById("formRifa");
        var cnMayorEdad = false;
        var cnLeidoPoliticas = false;
        if ($('.chkMayorEdad').is(':checked')) {
            cnMayorEdad = true;
        } else {
            alert('Debe aceptar de que es mayor de edad');
            $(this).focus();
            return;
        }

        if ($(".chkHeLeido").is(":checked"))
            cnLeidoPoliticas = true;
        else {
            alert('Debe aceptar las politicas y condiciones');
            $(".chkHeLeido").focus();
            return;
        }
        if (cnMayorEdad == true && cnLeidoPoliticas == true)
        //element.onsubmit();
            $('#formRifa').submit();
    });

    $('body').on('click', '#imgUpload', function (event) {
        //alert('hi');        
        $('#FileUpload1').click();
        //var fileupload = document.getElementById('FileUpload1');
        
    });
   
    $('body').on('change', '#FileUpload1', function (event){
    //jQuery("input#FileUpload1").change(function () {
        //alert(jQuery(this).val())
        var fsize = ($(this)[0].files[0].size);
        console.log(fsize);
        pnlinfoproceso.Show();
        //           403293
        if (fsize <= 2097152) {
            var file = $(this).val();
            //<span id="field2_area"> <input type="file" size="45" runat="server" id="FileAux" name="uploadPicture" style="width:200px; display:none;" /> </span>
            //$('#FileAux').val(file);
            /*var clone = $(this).clone();
            clone.attr('id', 'FileAux');
            $('#field2_area').html(clone);*/

            $("#lblValidacionImagen").text("");
            __doPostBack('<%= photoUpload.ClientID %>', 'uploadImage');
            return false;            
        }
        else {
            $("#lblValidacionImagen").text("El archivo excede el máximo de tamaño de 2 Megas");
            pnlinfoproceso.Hide();
        }
        //
        
    });

    $('body').on('change', '#FileUpload2', function (event) {
        var file = $(this)[0].files[0];
        var fsize = ($(this)[0].files[0].size);
       
        var filePath = ($(this)[0].files[0].name);
        var Extension = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();        
        $("#lblMsgFile1").text('');

        if (!allowedExtensions.exec(Extension)) {
            $("#lblMsgFile1").text('Solo se aceptan archivos con formato .jpeg/.jpg/.png/.pdf .');
            $("#hdValidationSizeFiles1").val("1");
            return false;
        }
        else if (fsize <= 2097152) {
            $("#lblMsgFile1").text(file.name);
            $("#hdValidationSizeFiles1").val("0");
        }
        else {
            $("#lblMsgFile1").text("El archivo excede el máximo de tamaño de 2 Megas");
            $("#hdValidationSizeFiles1").val("1");
        }
    });

    $('body').on('change', '#FileUpload3', function (event) {
        var file = $(this)[0].files[0];
        var fsize = ($(this)[0].files[0].size);
        var filePath = ($(this)[0].files[0].name);
        var Extension = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
        $("#lblMsgFile2").text('');

        if (!allowedExtensions.exec(Extension)) {
            $("#lblMsgFile1").text('Solo se aceptan archivos con formato .jpeg/.jpg/.png/.pdf .');
            $("#hdValidationSizeFiles2").val("1");
            return false;
        }
        else if (fsize <= 2097152) {
            $("#lblMsgFile2").text(file.name);
            $("#hdValidationSizeFiles2").val("0");
        }
        else {
            $("#lblMsgFile2").text("El archivo excede el máximo de tamaño de 2 Megas");
            $("#hdValidationSizeFiles2").val("1");
        }
    });

    $('body').on('change', '#FileUpload4', function (event) {
        var file = $(this)[0].files[0];
        var fsize = ($(this)[0].files[0].size);
        if (fsize <= 2097152) {
            $("#lblMsgFile3").text(file.name);
            $("#hdValidationSizeFiles3").val("0");
        }
        else {
            $("#lblMsgFile3").text("El archivo excede el máximo de tamaño de 2 Megas");
            $("#hdValidationSizeFiles3").val("1");
        }
    });
    var date = new Date();
    var minutes = 10;
    date.setTime(date.getTime() + (minutes * 60 * 1000));
    if ($.cookie('loadedAlready') != 'YES') {
        //run function here
        $('#myModal').modal();
        $.cookie('loadedAlready', 'YES', { expires: date }); //set cookie
    }

    $('body').on('change', '#ddlParentesco', function (event) {
    //$("#ddlParentesco").change(function () {
        $('#valDdlParentesco').html("");
        $('#valDdlParentesco').text("");
    });

    $('body').on('change', '#txtTelefonoParentesco', function (event) {
        $('#valTelContacto').html("");
        $('#valTelContacto').text("");
    });
});

window.onload = function () {
    // do stuff when the page has loaded
    //this will override any other scripts that may have been assigned to .onload
   // $('#myModal').modal();
};

function toBeExecutedOnFirstLoad() {
    $('#myModal').modal();
}

function showStepTres(respUpload, hdValidateSize1, hdValidateSize2, hdValidateSize3) {
    console.log(respUpload);

    if (hdValidateSize1 == undefined)        
    {        
        opcional = "0";        
    }
    if (hdValidateSize2 == undefined)        
    {        
        opcional = "0";        
    }
    if (hdValidateSize3 == undefined)        
    {        
        opcional = "0";        
    }
    respUpload = Boolean(respUpload);
        //alert('paso validacion 2');
        if ($("#paso").length) {
            //...
            //alert("si existe #paso")
            $('#paso').removeClass("c2");
            $('#paso').addClass("c3");
            $('#paso1').css("color", "#000000");
            $('#paso2').css("color", "#000000");
            $('#paso3').css("color", "#fff");
        }
        $('#form_paso_a').removeClass("show");
        $('#form_paso_b').removeClass("show");
        $('#form_paso_a').addClass("hide");
        $('#form_paso_b').addClass("hide");
        $('#form_paso_c').addClass("show");
        if (!respUpload) {
            $("#btnTerminar").attr('disabled', true);
        }
        else {
            $("#btnTerminar").attr('disabled', false);
        }
        if (hdValidateSize1 == '1')
        {
            $("#lblMsgFile1").text("El archivo excede el máximo de tamaño de 2 Megas");
        }
        if (hdValidateSize2 == '1') {
            $("#lblMsgFile2").text("El archivo excede el máximo de tamaño de 2 Megas");
        }
        if (hdValidateSize3 == '1') {
            $("#lblMsgFile3").text("El archivo excede el máximo de tamaño de 2 Megas");
        }
        return false;
    
}

function reloadPage(msg)
{
    alert(msg);
    location.reload(true);
    return false;
}

function responseRifa(responseMesagge) {
    
    var elem = responseMesagge.split('/');
    //console.log(elem);
    $('#lblMensaje').text(elem[1]);

    if (elem[0] == "1") {
        $('#back_paso_b').attr('disabled', true);
        $('#btnTerminar').attr('disabled', true);
        $('#ddlCategoria').attr('disabled', true);
    }    
}

var myMessages = ['info', 'warning', 'error', 'success']; // Mensajes definidos	 
function hideAllMessages() {
    var messagesHeights = new Array();

    for (i = 0; i < myMessages.length; i++) {
        messagesHeights[i] = $('.' + myMessages[i]).outerHeight();
        $('.' + myMessages[i]).css('bottom', -messagesHeights[i]); //mover el div fuera de la vista	  
    }
}

function showMessage(type, info, message) {
    //alert(message);
    console.log(message);
    var dv = document.getElementById('divMsg'); // Busca el Div
    var lblInfo = document.getElementById('lblInfo'); // Busca el label de info
    var lblMessage = document.getElementById('lblMensajeException'); // Busca el label de mensaje
    $("#MessageError").html("");
    if (lblInfo != null) {
        lblInfo.innerHTML = info;
        lblMessage.innerHTML = message;

        dv.className = type + ' message'; // Asigna el Css del tipo de mensaje
    }
    else {
        var cuerpoHtml = '<div id="Message" class="scrollable">'+
                        '<div  id="divMsg" class="' + type + ' message" >' +
		                        '<table style="width:100%">'+
                                    '<tr>'+
                                        '<td style="width:85%">'+
                                        '<h3><asp:Label runat="server" ID="lblInfo" Text="' + info + '">' + info + '</asp:Label></h3>' +
                                        '</td>'+
                                        '<td style="width:15%" rowspan="2">'+
                                            '<asp:Image ID="imgok" runat="server" ImageAlign="AbsMiddle" ImageUrl="styles/icon-behavior-yes.png" style="display :block;" CssClass="arrowhand" /> '+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td style="width:85%">'+
                                            '<p><asp:Label runat="server" ID="lblMensajeException" Text="' + message + '">' + message + '</asp:Label></p>' +
                                        '</td>'+
                                    '</tr>'+
                                '</table>'+
                            '</div>'+
                        '</div>';
        $("body").append(cuerpoHtml);;
    }

    hideAllMessages();
    $('.' + type).animate({ bottom: "0" }, 500);
}

var textSeparator = ";";
function OnListBoxSelectionChanged(listBox, args) {
    if (args.index == 0)
    {
        args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
        checkListBox.UnselectIndices([0]);
    }
    UpdateSelectAllItemState();
    UpdateText();
}
function UpdateSelectAllItemState() {
    IsAllSelected() ? checkListBox.SelectIndices([0]) : checkListBox.UnselectIndices([0]);
}
function IsAllSelected() {
    var selectedDataItemCount = checkListBox.GetItemCount() - (checkListBox.GetItem(0).selected ? 0 : 1);
    return checkListBox.GetSelectedItems().length == selectedDataItemCount;
}
function UpdateText() {
    var selectedItems = checkListBox.GetSelectedItems();
    checkComboBox.SetText(GetSelectedItemsText(selectedItems));
}
function SynchronizeListBoxValues(dropDown, args) {
    checkListBox.UnselectAll();
    var texts = dropDown.GetText().split(textSeparator);
    var values = GetValuesByTexts(texts);
    checkListBox.SelectValues(values);
    UpdateSelectAllItemState();
    UpdateText(); // for remove non-existing texts
}
function GetSelectedItemsText(items) {
    var texts = [];
    for (var i = 0; i < items.length; i++)
        if (items[i].index != 0)
            texts.push(items[i].text);
    return texts.join(textSeparator);
}
function GetValuesByTexts(texts) {
    var actualValues = [];
    var item;
    for (var i = 0; i < texts.length; i++) {
        item = checkListBox.FindItemByText(texts[i]);
        if (item != null)
            actualValues.push(item.value);
    }
    return actualValues;
}

/*$('body').on('click', '.chkMayorEdad', function (elemento) {
    //elemento.preventDefault();
    mostrar = $(this).attr('id');
    var cnMayorEdad = false;
    var cnLeidoPoliticas = false;
    
    if ($(this).is(':checked')) {
        cnMayorEdad = true;
    }     

    if ($(".chkHeLeido").is(":checked"))
        cnLeidoPoliticas = true;    
    
    if (cnMayorEdad == true && cnLeidoPoliticas == true) 
        $(".btnTerminar").removeAttr('disabled');
    else
        $(".btnTerminar").attr('disabled', 'disabled');

});

$('body').on('click', '.chkHeLeido', function (elemento) {
    //elemento.preventDefault();
    mostrar = $(this).attr('id');
    var cnMayorEdad = false;
    var cnLeidoPoliticas = false;

    if ($(this).is(':checked')) {
        cnLeidoPoliticas = true;
    }

    if ($(".chkMayorEdad").is(":checked"))
        cnMayorEdad = true;

    if (cnMayorEdad == true && cnLeidoPoliticas == true)
        $(".btnTerminar").removeAttr('disabled');
    else
        $(".btnTerminar").attr('disabled', 'disabled');

});

$('body').on('click', '.btnTerminar', function (elemento) {
    var cnMayorEdad = false;
    var cnLeidoPoliticas = false;
    if ($('.chkMayorEdad').is(':checked')) {
        cnMayorEdad = true;
    } else {
        alert('Debe aceptar de que es mayor de edad');
        $(this).focus();
        return;
    }

    if ($(".chkHeLeido").is(":checked"))
        cnLeidoPoliticas = true;
    else {
        alert('Debe aceptar las politicas y condiciones');
        $(".chkHeLeido").focus();
        return;
    }
});*/