﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;

namespace travesia
{
    public partial class respRegistro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int intIdClienteContactoEvento, intidVenta = 0;
                string strIdVenta = "";
                DataSet result, resultCompetidor;
                DataSet dsDetalleVenta;
                utils.Login("usrTriatlon", "eXperiencias");
                int intUltimoPago = 0;
                //int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                int.TryParse(Request.QueryString["token"], out intidVenta);
                int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                int idClienteDetalle = 0;
                if (intIdClienteContactoEvento > 0)
                {
                    result = null;
                    try
                    {
                        result = utils.buscarCompetidor(getDsSession(), "", "", "", 0, "", "", 0, 0, 0, intIdClienteContactoEvento);
                    }
                    catch (Exception ex)
                    {
                        result = utils.buscarCompetidor(getDsSession(), "", "", "", 0, "", "", 0, 0, 0, intIdClienteContactoEvento);
                        string msg = ex.Message.ToString();
                        string strginnermessage = "";
                        string strCuerpoCorreo = "respRegistro. Page_load(). El Primer intento carga de competidor falló " + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                        string strExcepcion = "";
                        if (ex.ToString().Length > 799)
                            strExcepcion = ex.ToString().Substring(0, 800);
                        else
                            strExcepcion = ex.ToString();
                        strCuerpoCorreo += strExcepcion;
                        //MensajeAlert(strCuerpoCorreo);
                    }

                    if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                    {
                        //cParticipante.dtCompedidor = resultCompetidor.Tables[0];
                        hdCorreoCompetidor.Value = result.Tables[0].Rows[0]["dsCorreoElectronico"].ToString();
                        idClienteDetalle = int.Parse(result.Tables[0].Rows[0]["idClienteDetalle"].ToString());

                        wscClienteDetalle.IcClienteDetalleClient cteCLiente = new wscClienteDetalle.IcClienteDetalleClient();
                        wscClienteDetalle.cClienteDetalle ClienteDet;
                        ClienteDet = cteCLiente.SeleccionarcClienteDetallePorId((idClienteDetalle), getDsSession());
                        // cambiamos el estatus del participante en progeso
                        ClienteDet.idEstatusCliente = 14;
                        int resModificaDetalle = 0;
                        try
                        {
                            resModificaDetalle = cteCLiente.ModificarcClienteDetalle(ClienteDet, getDsSession());
                        }
                        catch (Exception ex)
                        {
                            resModificaDetalle = cteCLiente.ModificarcClienteDetalle(ClienteDet, getDsSession());
                        }
                        wsBusinessRules.BusinessRulesServicesClient ruleOper = new wsBusinessRules.BusinessRulesServicesClient();
                        Dictionary<string, string> dicret = new Dictionary<string, string>();

                        lblNombre.Text = result.Tables[0].Rows[0]["dsContacto"].ToString() + " " + result.Tables[0].Rows[0]["dsApellidoPaterno"].ToString() + " " + result.Tables[0].Rows[0]["dsApellidoMaterno"].ToString();                        
                    }
                }
            }
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
                //hdDsSession.Value = getDsSession();
            }
            return cGlobals.dsSession;
        }
    }
}