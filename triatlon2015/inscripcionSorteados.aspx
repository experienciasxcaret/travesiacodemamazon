﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="inscripcionSorteados.aspx.cs" Inherits="travesia.inscripcionSorteados" %>

<%@ Register Assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" namespace="DevExpress.Web.ASPxLoadingPanel" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width= device-width, initial-scale=1.0" /> 
    <title>Triatlon Xelha 2015</title>
    <link rel="stylesheet" href="styles/bootstrap.css" />
    <link rel="stylesheet" href="styles/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="styles/triatlon.css" />
    <link rel="stylesheet" href="styles/basic.css" />
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <script src="Scripts/jquery-1.11.3.js" ></script>
    <script src="Scripts/bootstrap.js" ></script>
    <script src="Scripts/inscripcionGeneral.js" ></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.metadata.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/localization/messages_es.js"></script>    
    <script type="text/javascript">

        function registrarCompetidor() {
            cbRegistrar.PerformCallback();
        }        

        function validarCompetidor() {
            cbValida.PerformCallback();
        }
        $(document).ready(function () {

            Sys.Browser.WebKit = {}; //Safari 3 is considered WebKit

            if (navigator.userAgent.indexOf('WebKit/') > -1) {
                
                Sys.Browser.agent = Sys.Browser.Firefox;

                Sys.Browser.version = parseFloat(navigator.userAgent.match(/WebKit\/(\d+(\.\d+)?)/)[1]);

                Sys.Browser.name = 'Firefox';
            }
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(initializeRequest);
            prm.add_pageLoaded(pageLoaded);
            function initializeRequest(sender, args) {
                //LoadingPanel.ShowInElement(args._postBackElement);  
                document.body.style.cursor = "wait";                
                pnlinfoproceso.Show();

            }
            function pageLoaded(sender, args) {
                var panels = args.get_panelsUpdated();
                if (panels.length > 0) {
                    document.body.style.cursor = "default";
                    pnlinfoproceso.Hide();
                }
            }            
        })
    </script>
</head>
<body>    
    <div class="container">
         <div class="cont header b-bottom">
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			    <object data="img/xelha.svg" type="image/svg+xml" class="img1"></object>
		    </div>
		    <div class="logo col-xs-8 col-sm-4" align="center">
			    <object data="img/triatlon.svg" type="image/svg+xml" class="img2"></object>
		    </div>
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			    <object data="img/triatlon_fechas.svg" type="image/svg+xml" class="img3"></object>
		    </div>
       </div>
        <div class="grid_16 wrap">
            <div id="paso" class="c1 hidden-xs">
	            <div id="pasos">
		            <ul>
			            <li id="paso1" style="color:#fff;">PASO 1</li>
			            <li id="paso2">PASO 2</li>
			            <li id="paso3">PASO 3</li>
		            </ul>
	            </div> 
            </div>
            <div id="cont_formularios" class="cont">
                <form class="form-horizontal" id="formRifa" runat="server">
                    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
                    <asp:HiddenField ID="cnIdContactoPrecargado" runat="server" value="0"/>  
                    <asp:HiddenField ID="hdStrCat" runat="server" value=""/>
                    <asp:HiddenField ID="hdIdConfigPago" runat="server" value=""/>  
                    <div id="form_paso_a">
                                    <fieldset id="info_personal">                                    
                                    <br />
                                    <div class="row">
                                        <div class="form-group" style="margin:5px">
                                            <div class="col-md-2">                                                                                             
                                                <label for="nombre" class="control-label" >Nombre *</label>
                                            </div>
                                            <div class="col-md-2">	                                            
                                                <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtGralNombre" placeholder="ingresa tu nombre" name="nombre1" EnableTheming="False"></asp:TextBox>
                                            </div>    
                              
                                            <div class="col-md-2">	
                                                <label for="apellidoPaterno" class="control-label" >Apellido Paterno *</label>
                                            </div>
                                            <div class="col-md-2">	
                                                <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtGralApellidoPaterno" placeholder="ingresa tu apellido paterno" name="apellido1" EnableTheming="False"></asp:TextBox>                            
                                            </div>
                     
                                            <div class="col-md-2">	
                                                <label for="apellidoMaterno" class="control-label" >Apellido Materno</label>
                                            </div>
                                            <div class="col-md-2">	
                                                <asp:TextBox runat="server" class="form-control"  id="txtGralApellidoMaterno" placeholder="ingresa tu apellido materno" name="apellido2" EnableTheming="False"></asp:TextBox>                                    
                                            </div>
                                        </div>
                                    </div>                                    
                                    <div class="row">    
                                       <div class="form-group" style="margin:5px">
                                            <div class="col-md-2">                                            
                                                <label for="email" class="control-label">Email *</label>                                                   
                                            </div>
                                            <div class="col-md-3">                                                                                           
                                                <asp:TextBox runat="server" type="email" class="{required:true} form-control" id="txtGralEmail" placeholder="ingresa tu email" name="email" EnableTheming="False" ></asp:TextBox>
                                            </div>
                                        </div>      
                                    </div>
                                    <div class="row">    
                                        <div class="form-group" style="margin:5px">
                                            <div class="col-md-2">                                            
                                                <label for="email" class="control-label">Confirma Email *</label>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:TextBox runat="server" type="email" class="{required:true} form-control" id="txtGralEmail2" onpaste="return false" oncut="return false" placeholder="confirma tu email" name="email" OnTextChanged="txtGralEmail2_TextChanged" EnableTheming="False" ></asp:TextBox>
                                                <dx:ASPxLabel ID="lblValidacionEmail" runat="server" Text="" Font-Size="Small" ForeColor="#CC0000"></dx:ASPxLabel>
                                            </div>  
                                        </div>      
                                    </div>                                    
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="form-group" style="margin:5px">
                                                <div class="col-md-2">                                                
                                                    <label for="sexo" class="control-label">Sexo *</label>
                                                </div>
                                                <div class="col-md-2 selectContainer">
                                                    <asp:DropDownList  class="{required: true} form-control" ID="ddlGralSexo" runat="server" name="sexo">
                                                            <asp:ListItem Value="">Seleccione sexo</asp:ListItem>
                                                            <asp:ListItem Value="2">Masculino</asp:ListItem>
                                                            <asp:ListItem Value="3">Femenino</asp:ListItem>
                                                    </asp:DropDownList>                           
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="col-md-3">                                           
                                                    <label for="nacimiento" class="control-label">Fecha nacimiento : *</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                           <div class="form-group" style="margin:5px">
                                                <div class="col-md-1">                                                
                                                    <label for="anio" class="control-label">año</label>
                                                </div>
                                                <div class="col-md-2 selectContainer">
                                                        <asp:DropDownList  class="{required: true} form-control" ID="ddlGralAnio" runat="server" AutoPostBack="True" name="anio" OnSelectedIndexChanged="ddlGralAnio_SelectedIndexChanged">                                                    
                                                        </asp:DropDownList>                                                                                                                              
                                                </div>
                                                <div class="col-md-1">
                                                    <label for="mes" class="control-label">mes</label>
                                                </div>
                                               <div class="col-md-2 selectContainer">
                                                        <asp:DropDownList  class="{required: true} form-control" ID="ddlGralMes" runat="server" AutoPostBack="True" name="mes" OnSelectedIndexChanged="ddlGralMes_SelectedIndexChanged">                                                        
                                                                <asp:ListItem Value="1" Selected="True">Enero</asp:ListItem>
                                                                <asp:ListItem Value="2">Febrero</asp:ListItem>
                                                                <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                                <asp:ListItem Value="4">Abril</asp:ListItem>
                                                                <asp:ListItem Value="5">Mayo</asp:ListItem>
                                                                <asp:ListItem Value="6">Junio</asp:ListItem>
                                                                <asp:ListItem Value="7">Julio</asp:ListItem>
                                                                <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                                <asp:ListItem Value="9">Septiembre</asp:ListItem>
                                                                <asp:ListItem Value="10">Octubre</asp:ListItem>
                                                                <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                                                <asp:ListItem Value="12">Diciembre</asp:ListItem>
                                                        </asp:DropDownList>
                                                </div>
                                                <div class="col-md-1">
                                                     <label for="dia" class="control-label">día</label>
                                                </div>
                                               <div class="col-md-2 selectContainer">
                                                    <asp:DropDownList  class="{required: true} form-control" ID="ddlGralDia" runat="server" AutoPostBack="True" name="dia">
                                                                <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                                <asp:ListItem Value="8">8</asp:ListItem>
                                                                <asp:ListItem Value="9">9</asp:ListItem>
                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                                <asp:ListItem Value="26">26</asp:ListItem>
                                                                <asp:ListItem Value="27">27</asp:ListItem>
                                                                <asp:ListItem Value="28">28</asp:ListItem>
                                                                <asp:ListItem Value="29">29</asp:ListItem>
                                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                                <asp:ListItem Value="31">31</asp:ListItem>                                                            
                                                    </asp:DropDownList>
                                                </div>
                                            </div>                  
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>                                    
                            </fieldset>
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                         <ContentTemplate>
                                            <fieldset id="botonera_personales">
                                                <div class="col-md-1">
                                                     <asp:button ID="btnContinuarA" Text="Continuar" class="btn btn-info btn-mini" UseSubmitBehavior="true" AutoPostBack="False" ClientInstanceName="btnContinuarA"/>Continuar
                                                </div>
                                                <div class="col-md-6">
                                                    <dx:ASPxLabel ID="lblMensajeValidacion" runat="server" Text="" ClientInstanceName="lblMensajeValidacion" Font-Bold="True" Font-Size="Small" ForeColor="#CC0000"></dx:ASPxLabel>
                                                </div>
			                                </fieldset>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                    
                                <div id="form_paso_b" class="hide">    
                                    <fieldset id="InformacionDireccion">
                                        <br/>
                                            <div class="row">
                                                 <div class="form-group" style="margin:5px">
                                                    <div class="col-md-2">                                                                                                            
                                                        <label for="colonia" class="control-label" >Colonia</label> 
                                                    </div>
                                                    <div class="col-md-3">			                                            
                                                            <asp:TextBox runat="server" class="form-control"   id="txtGralColonia" placeholder="Colonia" name="colonia">
                                                            </asp:TextBox>
                                                    </div>                                                    
                                                    <div class="col-md-2">                                                        
                                                            <label for="calle" class="control-label" >calle</label>
                                                    </div>
                                                     <div class="col-md-3">
                                                            <asp:TextBox runat="server" class="form-control"   id="txtGralCalle" placeholder="calle" name="calle">
                                                            </asp:TextBox>                                                        
                                                     </div>
                                                </div>
                                            </div>                                            
                                            <div class="row">
                                                 <div class="form-group" style="margin:5px">                                                                                                        
                                                        <div class="col-md-2">                                                       
                                                            <label for="numeroExterior" class="control-label" >Numero Exterior</label>
                                                        </div>
                                                        <div class="col-md-3">    		                                            
                                                                <asp:TextBox runat="server" class="form-control"   id="txtGralNoExterior" placeholder="Numero Exterior" name="numeroExterior">
                                                                </asp:TextBox>
                                                        </div>    
                                                     
                                                        <div class="col-md-2">                                                        
                                                            <label for="numeroInterior" class="control-label" >Numero Interior</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <asp:TextBox runat="server" class="form-control"   id="txtGralNoInterior" placeholder="numero Interior" name="numeroInterior">
                                                            </asp:TextBox>                               
                                                        </div>                                                    
                                                 </div>
                                            </div>                                            
                                         <asp:UpdatePanel ID="upFormulario" runat="server">
                                            <ContentTemplate>
                                                 <div class="row">
                                                    <div class="form-group" style="margin:5px">                                                                                                            
                                                        <div class="col-md-2">                                                        
                                                            <label for="pais" class="control-label">País *</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlGralPais" runat="server" name="pais" OnTextChanged="ddlGralPais_TextChanged" AutoPostBack="True">
                                                            </asp:DropDownList>  
                                                        </div>                                                         
                                                        <div class="col-md-2">                                                        
                                                            <label for="estado" class="control-label">Estado</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <asp:DropDownList  class="form-control" ID="ddlGralEstado" runat="server" name="estado">
                                                                    <asp:ListItem Value="1">Seleccione Estado</asp:ListItem>                                                                
                                                            </asp:DropDownList>                                 
                                                        </div>
                                                    </div>
                                                </div>                                            
                                            </ContentTemplate>
                                         </asp:UpdatePanel>                                            
                                            <div class="row">
                                                <div class="form-group" style="margin:5px">
                                                    <div class="col-md-2">                                                                                                    
                                                        <label for="ciudad" class="control-label" >Ciudad</label> 
                                                    </div>
                                                    <div class="col-md-3">  			                                            
                                                        <asp:TextBox runat="server" class="form-control"   id="txtGralCiudad" placeholder="ciudad" name="ciudad">
                                                        </asp:TextBox>
                                                    </div>                                                                                             
                                                </div>
                                            </div>                                        
                                            <div class="row">
                                                <div class="form-group" style="margin:5px">                                                                                                         
                                                    <div class="col-md-2">                                                                                          
                                                        <label for="telefono" class="control-label">Telefono Fijo</label>   
                                                    </div>      
                                                    <div class="col-md-1">
                                                            <asp:TextBox runat="server" width="60px" class="form-control" id="txtGralTelefonofijo1" placeholder="lada" name="lada1">
                                                            </asp:TextBox>
                                                    </div>
                                                    <div class="col-md-2">
                                                            <asp:TextBox runat="server" class="form-control"   id="txtGralTelefonofijo2" placeholder="Numero" name="numero1">
                                                            </asp:TextBox>                                                          
                                                    </div>                            
                                                </div>
                                            </div>                                            
                                            <div class="row">
                                                <div class="form-group" style="margin:5px">                                                                                                            
                                                    <div class="col-md-2">                                    
                                                            <label for="telefonoCelular" class="control-label">Telefono Celular *</label>   
                                                   </div>
                                                   <div class="col-md-1">
                                                            <asp:TextBox runat="server" width="60px" class="{required:true, rangelength: [2,4]} form-control" id="txtGralCelular1" placeholder="lada" name="lada2">
                                                            </asp:TextBox>
                                                   </div>
                                                    <div class="col-md-2">
                                                            <asp:TextBox runat="server" class="{required:true, rangelength: [2,8]} form-control"   id="txtGralCelular2" placeholder="Numero" name="numero2">
                                                            </asp:TextBox>
                                                    </div>                                                                                 
                                                </div>
                                            </div>                                            
                                    </fieldset>  
                                           <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate> 
                                                    <fieldset id="botonera_domicilio">
                                                        <div class="col-md-4">
				                                            <a id="back_paso_a" class="btn btn-info btn-mini">Regresar</a>                                                                                                          
                                                            <asp:Button ID="btnContinuarB" runat="server" Text="Continuar" class="btn btn-info btn-mini"  UseSubmitBehavior="true" AutoPostBack="False" name="NameBtncontinuarB"/>
                                                        </div>
			                                        </fieldset> 
                                                </ContentTemplate>
                                           </asp:UpdatePanel>                                     
                                </div>
                                <br />
                                <div id="form_paso_c" class="hide">
                                    <fieldset id="categoria">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <div class="row">
                                                <div class="form-group" style="margin:5px">
                                                    <div class="col-md-2">
                                                            <label for="categoria" class="control-label">Categoria</label>
                                                    </div>
                                                    <div class="col-md-3 selectContainer"> 
                                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlGralCategoria" runat="server" AutoPostBack="True" name="categoria" OnSelectedIndexChanged="ddlGralCategoria_SelectedIndexChanged">
                                                                    <asp:ListItem Value="">Seleccione categoria</asp:ListItem>                                                           
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="rvcategoría" runat="server" ControlToValidate="ddlGralCategoria"  Display="Dynamic" ErrorMessage="requerido" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                    </div>                            
                                                </div>
                                             </div>
                                        </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                        <ContentTemplate>
                                            <div class="row" id="dvPlayera">
                                                <div class="form-group" style="margin:5px">
                                                    <div class="col-md-2">
                                                            <asp:Label  for="playera" ID="lblTamanioPlayera" runat="server" class="control-label" Text="Tamaño playera" visible="False" Font-Bold="True"></asp:Label>
                                                    </div>
                                                    <div class="col-md-3 selectContainer">
                                                         <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                                         <ContentTemplate>
                                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlGralTallasPlayeras" runat="server" AutoPostBack="true" name="ddlGralTallasPlayeras" Visible="False" OnSelectedIndexChanged="ddlGralTallasPlayeras_SelectedIndexChanged">
                                                                    <asp:ListItem Value="">Seleccione Tamaño de playera</asp:ListItem>                                                           
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                            <asp:RequiredFieldValidator ID="rvTallaPlayera" runat="server" ControlToValidate="ddlGralTallasPlayeras"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>                                                        
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:Label ID="lblSujetoDisponibilidad" runat="server" class="control-label" Text="*sujeto a disponibilidad" visible="False" Font-Bold="True"></asp:Label>
                                                    </div>                           
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div class="row" id="numFmtri">
                                            <div class="form-group" style="margin:5px">
                                                <div class="col-md-2">
                                                    <label for="noFmtri">No. Fmtri</label>
                                                </div>
                                                <div class="col-md-3">
                                                        <table border="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                <asp:TextBox runat="server" class="{required: true, number: true, minlength: 9, maxlength:12} form-control"   id="txtNoFmtri" placeholder="número fmtri" name="noFmtri" ValidationGroup="validaGuardar" MaxLength="12"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="fmtri" runat="server" ControlToValidate="txtNoFmtri"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                            </td>
                                                            </tr>                                                        
                                                        </table>
                                                  </div>                                                                                                                                            
                                            </div>
                                        </div>
                                        <div class="row" id="alias">
                                            <div class="form-group" style="margin:5px"> 
                                                <div class="col-md-2">                                                                                               
                                                    <label for="alias">Alias</label>
                                                </div>                                                
                                                <div class="col-md-3">                                                                                                	                                            
                                                    <asp:TextBox runat="server" class="form-control" id="txtAlias" placeholder="Alias (nickname)" name="txtAlias">
                                                    </asp:TextBox>
                                                </div>                                                                                                                                                                                      
                                            </div>
                                        </div>
                                        <div class="row" id="dvTipoSangre">
                                            <div class="form-group" style="margin:5px">                                                                                                                                                                               
                                                <div class="col-md-2"> 
                                                    <asp:Label  for="tipoSangre" ID="lblTipoSangre" runat="server" Text="Tipo de sangre" visible="true" Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="col-md-3 selectContainer">
                                                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                    <ContentTemplate>
                                                    <asp:DropDownList  class="{required: true} form-control" ID="ddlTipoSangre" runat="server" AutoPostBack="true" name="ddlTipoSangre" Visible="true" OnSelectedIndexChanged="ddlTipoSangre_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    <asp:RequiredFieldValidator ID="rvSangre" runat="server" ControlToValidate="ddlTipoSangre"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                </div>                                                                          
                                            </div>
                                        </div>
                                        <div class="row" id="dvPadecimientos">
                                            <div class="form-group" style="margin:5px">
                                                <div class="col-md-2">                                                
                                                    <label for="padecimientos">Padecimientos</label>
                                                </div>
                                                <div class="col-md-4">		                                            
                                                    <asp:TextBox runat="server" class="form-control" id="txtDsPadecimientos" placeholder="padecimientos" name="txtDsPadecimientos" TextMode="MultiLine"></asp:TextBox>
                                                </div>                                                                                                                                           
                                            </div>
                                        </div>
                                        <div class="row" >
                                            <div class="form-group" style="margin:5px">
                                                <div class="col-md-2">
                                                    <label for="participaciones">¿Cuantas veces has participado?</label>  
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:DropDownList  class="form-control" ID="ddlGralNoParticipaciones" runat="server" name="participaciones">
                                                            <asp:ListItem Value="1">1</asp:ListItem>
                                                            <asp:ListItem Value="2">2</asp:ListItem>
                                                            <asp:ListItem Value="3">3</asp:ListItem>
                                                            <asp:ListItem Value="4">4</asp:ListItem>
                                                            <asp:ListItem Value="5">5</asp:ListItem>
                                                            <asp:ListItem Value="6">6</asp:ListItem>
                                                            <asp:ListItem Value="7">7</asp:ListItem>
                                                    </asp:DropDownList>                                                
                                                </div>                            
                                            </div>
                                        </div>
                                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div class="form-group" style="margin-left:15%">
                                                         <div class="col-md-10">                                                    
                                                             <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                                             <ContentTemplate>
                                                                 <asp:Label ID="lblCorredor1"  class="radio-inline" runat="server" visible="false">
                                                                    <asp:CheckBox ID="cbCorredor1" runat="server"  Text="Corredor" CssClass="Corredor"/>
                                                                 </asp:Label>                                           
                                                                 <asp:Label ID="lblNadador1"  class="radio-inline" runat="server" visible="false">
                                                                 <asp:CheckBox ID="cbNadador1" runat="server" Text="Nadador"/>
                                                                  </asp:Label>                                                    
                                                                <asp:Label ID="lblCiclista1"  class="radio-inline" runat="server" visible="false">                                                       
                                                                    <asp:CheckBox ID="cbCiclista1" runat="server" Text="Ciclista" />
                                                                </asp:Label>
                                                                </ContentTemplate>  
                                                              </asp:UpdatePanel>                                                   
                                                          </div>
                                                    </div>
                                                </div>
                                             </ContentTemplate>
                                        </asp:UpdatePanel>                             
                                        <!-- termina la parte de competidores normal -->
                                        <asp:UpdatePanel ID="upRelevo2" runat="server" RenderMode="Inline">
                                            <ContentTemplate>                                                
                                                <div runat="server" id="divRelevo2" visible="false">
                                                    <div class="row" style="display: inline-block;width: 200px;padding: 5px;">
                                                        <div class="col-md-10">
                                                            <asp:Label ID="lblRelevo2" runat="server" class="control-label" Text="Datos Relevo 2" visible="False" Font-Bold="True"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="row">                                                    
                                                            <div class="form-group" style="margin:5px">                                                            
                                                                    <div class="col-md-2">                                                        
                                                                        <asp:Label ID="lblNombreRelevo2" runat="server" class="control-label" Text="Nombre *" visible="False" Font-Bold="True"></asp:Label>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"  visible="False" id="txtNombre2" placeholder="ingresa tu nombre" name="nombre2">
                                                                        </asp:TextBox>
                                                                    </div>
                                                                    <div class="col-md-2">                                                   
                                                                        <asp:Label ID="lblPaternoRelevo2" runat="server" class="control-label" Text="Apellido Paterno *" visible="False" Font-Bold="True"></asp:Label>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtPaterno2" visible="False" placeholder="ingresa tu apellido paterno" name="apellido1">
                                                                        </asp:TextBox>
                                                                    </div>   
                                                                    <div class="col-md-2">                                                                              
                                                                        <asp:Label ID="lblMaternoRelevo2" runat="server" class="control-label" Text="Apellido Materno" visible="False" Font-Bold="True"></asp:Label>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <asp:TextBox runat="server" class="form-control"  id="txtMaterno2" visible="False" placeholder="ingresa tu apellido materno" name="apellido2">
                                                                        </asp:TextBox>
                                                                    </div>                                                            
                                                            </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group" style="margin:5px">
                                                            <div class="col-md-2">                                                                                                                    
                                                                <asp:Label ID="lblSexoRelevo2" runat="server" class="control-label" Text="Sexo *" visible="False" Font-Bold="True"></asp:Label>
                                                            </div>
                                                            <div class="col-md-3 selectContainer"> 
                                                                <asp:DropDownList  class="{required: true} form-control" ID="ddlSexo2" runat="server" name="sexo" visible="False">
                                                                        <asp:ListItem Value="">Seleccione sexo</asp:ListItem>
                                                                        <asp:ListItem Value="2">Masculino</asp:ListItem>
                                                                        <asp:ListItem Value="3">Femenino</asp:ListItem>
                                                                </asp:DropDownList>                           
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" id="dvPlayera2">                                                                                                                                    
                                                        <div class="form-group" style="margin:5px">
                                                            <div class="col-md-2">
                                                                <asp:Label  for="playera" ID="lblPlayeraRelevo2" runat="server" class="control-label" Text="Tamaño playera *" visible="False" Font-Bold="True"></asp:Label>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <asp:DropDownList  class="{required: true} form-control" ID="ddlPlayera2" runat="server" AutoPostBack="false" name="ddlPlayera2" Visible="False">
                                                                        <asp:ListItem Value="">Seleccione Tamaño de playera *</asp:ListItem>                                                           
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlPlayera2"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                            </div>                     
                                                        </div>
                                                    </div>
                                                    <div class="row" id="Div2">
                                                        <div class="form-group" style="margin:5px">
                                                            <div class="col-md-2">
                                                                <asp:Label ID="lblNoFmtri2" runat="server" class="control-label" Text="No. Fmtri" visible="False" Font-Bold="True"></asp:Label>
                                                            </div>
                                                                <div class="col-md-3">
                                                                    <table border="0" width="100%">
                                                                     <tr>
                                                                         <td>
                                                                            <asp:TextBox runat="server" class="{required: true, number: true, minlength: 9} form-control" visible="False" id="txtNoFmtri2" MaxLength="12" placeholder="número fmtri" name="noFmtri" ValidationGroup="validaGuardar"></asp:TextBox>                                                
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNoFmtri2"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                                        </td>
                                                                     </tr>                                                                   
                                                                    </table>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" id="Div3">
                                                        <div class="form-group" style="margin:5px">                                                        
                                                            <div class="col-md-2">                                                            
                                                                <asp:Label ID="lblAlias2" runat="server" class="control-label" Text="Alias" visible="False" Font-Bold="True"></asp:Label>
                                                            </div>                                                                                                                                                                   
                                                            <div class="col-md-3">                                                                                                	                                            
                                                                <asp:TextBox runat="server" class="form-control" visible="False" id="txtAlias2" placeholder="Alias (nickname)" name="txtAlias">
                                                                </asp:TextBox>
                                                            </div>                                                                                                                                                                                           
                                                        </div>
                                                    </div>
                                                    <div class="row" id="Div4">
                                                        <div class="form-group" style="margin:5px">
                                                            <div class="col-md-2">                                                                                                                                        
                                                                <asp:Label  for="tipoSangre" ID="lblTipoSangre2" runat="server" Text="Tipo de sangre" visible="false" Font-Bold="True"></asp:Label>
                                                            </div>
                                                            <div class="col-md-3 SelectContainer">                                                            
                                                                <asp:DropDownList  class="{required: true} form-control" ID="ddlTipoSangre2" runat="server" visible="False" AutoPostBack="false" name="ddlTipoSangre2">                                                                                                                  
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlTipoSangre2"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                             </div>                                                                                  
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group" style="margin-left:15%">
                                                             <div class="col-md-10 columns"> 
                                                    
                                                                <asp:Label ID="lblCorredor2"  class="radio-inline" runat="server" visible="false">
                                                                    <asp:CheckBox ID="cbCorredor2" runat="server" Text="Corredor"/>
                                                                </asp:Label>                                                                                                       
                                                                <asp:Label ID="lblNadador2"  class="radio-inline" runat="server" visible="false">                                                      
                                                                    <asp:CheckBox ID="cbNadador2" runat="server" Text="Nadador" />
                                                                </asp:Label>
                                                    
                                                                <asp:Label ID="lblCiclista2"  class="radio-inline" runat="server" visible="false">                                                       
                                                                    <asp:CheckBox ID="cbCiclista2" runat="server" Text="Ciclista"/>
                                                                </asp:Label>                                                     
                                                              </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <!-- relevo 3 comienza -->
                                        <asp:UpdatePanel ID="upRelevo3" runat="server" RenderMode="Inline">
	                                        <ContentTemplate>                                                
                                                <div runat="server" id="divRelevo3" visible="false">
		                                            <div class="row" style="display: inline-block;width: 200px;padding: 5px;">
			                                            <div class="col-md-10">
				                                            <asp:Label ID="lblRelevo3" runat="server" class="control-label" Text="Datos Relevo 3" visible="False" Font-Bold="True"></asp:Label>
			                                            </div>
		                                            </div>
		                                            <div class="row">
			                                            <div class="form-group" style="margin:5px">                                                        
					                                            <div class="col-md-2">                                                                                                                     
						                                            <asp:Label ID="lblNombreRelevo3" runat="server" class="control-label" Text="Nombre *" visible="False" Font-Bold="True"></asp:Label>
					                                            </div>
					                                            <div class="col-md-2">
						                                            <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control" id="txtNombre3" visible="False" placeholder="ingresa tu nombre" name="nombre2">
						                                            </asp:TextBox>
					                                            </div>
					                                            <div class="col-md-2">                                                    
						                                            <asp:Label ID="lblPaternoRelevo3" runat="server" class="control-label" Text="Apellido Paterno *" visible="False" Font-Bold="True"></asp:Label>
					                                            </div>
					                                            <div class="col-md-2">
						                                            <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtPaterno3" visible="False" placeholder="ingresa tu apellido paterno" name="apellido1">
						                                            </asp:TextBox>
					                                            </div>
					                                            <div class="col-md-2">                                             
						                                            <asp:Label ID="lblMaternoRelevo3" runat="server" class="control-label" Text="Apellido Materno" visible="False" Font-Bold="True"></asp:Label>
					                                            </div>
					                                            <div class="col-md-2">
						                                            <asp:TextBox runat="server" class="form-control"  id="txtMaterno3" visible="False" placeholder="ingresa tu apellido materno" name="apellido2">
						                                            </asp:TextBox>
					                                            </div>                                                        
			                                            </div>
		                                            </div>
		                                            <div class="row">
			                                            <div class="form-group" style="margin:5px">
				                                            <div class="col-md-2">                                                                                                                    
					                                            <asp:Label ID="lblSexoRelevo3" runat="server" class="control-label" Text="Sexo *" visible="False" Font-Bold="True"></asp:Label>
				                                            </div>
				                                            <div class="col-md-3 selectContainer">
					                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlSexo3" visible="False" runat="server" name="sexo">
							                                            <asp:ListItem Value="">Seleccione sexo</asp:ListItem>
							                                            <asp:ListItem Value="2">Masculino</asp:ListItem>
							                                            <asp:ListItem Value="3">Femenino</asp:ListItem>
					                                            </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlSexo3"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>          
				                                            </div>
			                                            </div>
		                                            </div>
		                                            <div class="row" id="Div1">
			                                            <div class="form-group" style="margin:5px">
				                                            <div class="col-md-2">
					                                            <asp:Label  for="playera" ID="lblPlayeraRelevo3" runat="server" class="control-label" Text="Tamaño playera *" visible="False" Font-Bold="True"></asp:Label>
				                                            </div>
				                                            <div class="col-md-3 selectContainer">
					                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlPlayera3" runat="server" AutoPostBack="false" name="ddlPlayera3" Visible="False">
							                                            <asp:ListItem Value="">Seleccione Tamaño de playera *</asp:ListItem>                                                           
					                                            </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlPlayera3"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
				                                            </div>                            
			                                            </div>
		                                            </div>
		                                            <div class="row">                                                
				                                        <div class="form-group" style="margin:5px">
                                                            <div class="col-md-2">					                                    
                                                            <asp:Label  for="playera" ID="lblNoFmtri3" runat="server" class="control-label" Text="No. Fmtri *" visible="False" Font-Bold="True"></asp:Label>
                                                            </div>
						                                    <div class="col-md-3">
					                                            <table border="0" width="100%">
						                                            <tr>
							                                            <td>
							                                                <asp:TextBox runat="server" class="{required: true, number: true, minlength: 9} form-control" visible="False" id="txtNoFmtri3" MaxLength="12" placeholder="número fmtri" name="noFmtri" ValidationGroup="validaGuardar"></asp:TextBox>                                                
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNoFmtri3"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
						                                                </td>
						                                            </tr>					                                               
					                                            </table>
						                                    </div>                                  
				                                        </div>                                                                                             			                                        
		                                            </div>
		                                            <div class="row" id="Div5">
                                                        <div class="form-group" style="margin:5px">
			                                                <div class="col-md-2">				                                                                                        					                                        
                                                                <asp:Label  for="playera" ID="lblAlias3" runat="server" class="control-label" Text="Alias" visible="False" Font-Bold="True"></asp:Label>
                                                            </div>
					                                        <div class="col-md-3">                                                                                                	                                            
					                                            <asp:TextBox runat="server" class="form-control" id="txtAlias3" visible="False" placeholder="Alias (nickname)" name="txtAlias">
					                                            </asp:TextBox>
					                                        </div>				                                                                                                                                                                            
			                                            </div>
		                                            </div>
		                                            <div class="row" id="Div6">
                                                        <div class="form-group" style="margin:5px">
			                                                <div class="col-md-2">				                                        
					                                            <asp:Label  for="tipoSangre" ID="lblTipoSangre3" runat="server" Text="Tipo de sangre" visible="false" Font-Bold="True"></asp:Label>
                                                            </div>
					                                        <div class="col-md-3">
					                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlTipoSangre3" runat="server" visible="False" AutoPostBack="false" name="ddlTipoSangre">                                                                                                                  
					                                            </asp:DropDownList>
					                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlTipoSangre3"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
					                                        </div>				                                        
			                                            </div>
		                                            </div>
                                                    <div class="row">
                                                        <div class="form-group" style="margin-left:15%">
                                                             <div class="col-md-10 columns">                                                    
                                                                <asp:Label ID="lblCorredor3"  class="radio-inline" runat="server" visible="false">
                                                                    <asp:CheckBox ID="cbCorredor3" runat="server" Text="Corredor"/>
                                                                </asp:Label>                                                                                                       
                                                                <asp:Label ID="lblNadador3"  class="radio-inline" runat="server" visible="false">                                                      
                                                                    <asp:CheckBox ID="cbNadador3" runat="server" Text="Nadador"/>
                                                                </asp:Label>
                                                    
                                                                <asp:Label ID="lblCiclista3"  class="radio-inline" runat="server" visible="false">                                                       
                                                                    <asp:CheckBox ID="cbCiclista3" runat="server" Text="Ciclista"/>
                                                                </asp:Label>                                                     
                                                              </div>
                                                        </div>
                                                    </div>
                                                </div>
	                                        </ContentTemplate>
                                        </asp:UpdatePanel>
                                        
                                    </fieldset>
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <fieldset id="botonera_categoria">
                                            <div class="col-md-4">
				                                <a id="back_paso_b" href="" class="btn btn-info btn-mini">Regresar</a>
				                        
                                                <dx:ASPxButton ID="btnTerminar" runat="server" Text="Terminar" class="btn btn-info btn-mini" name="NameBtnContinuarC"  CssPostfix="&quot;btn btn-info btn-mini" ClientInstanceName="NameBtnContinuarC"  UseSubmitBehavior="False"  AutoPostBack="false" ValidationGroup="validaGuardar" OnClick="btnTerminar_Click">
                                                </dx:ASPxButton>
                                            </div>
			                            </fieldset>
                                        <br />
                                        <dx:ASPxLabel ID="lblMensaje" runat="server" Text="" ClientInstanceName="lblMensaje" Font-Bold="True" Font-Size="Medium"></dx:ASPxLabel>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>                                
                                </div>                      
                </form>
            </div>        
        </div>
    <dx:ASPxCallback ID="cbRegistrar" runat="server" ClientInstanceName="cbRegistrar" OnCallback="cbRegistrar_Callback" ClientIDMode="AutoID">
        <ClientSideEvents EndCallback="function(s, e) {
pnlinfoproceso.Hide();
if (s.cpIsMessage != '') 
{		
    responseRifa(s.cpIsMessage)
}
else
{
    alert('vacío');
}
}" BeginCallback="function(s, e) {
	pnlinfoproceso.Show();
}" />
        </dx:ASPxCallback>
    <dx:ASPxCallback ID="cbValida" runat="server" ClientInstanceName="cbValida" OnCallback="cbValida_Callback" ClientIDMode="AutoID">
    </dx:ASPxCallback>
        <dx:ASPxLoadingPanel ID="pnlinfo" runat="server" ClientInstanceName="pnlinfoproceso" Modal="True">
        </dx:ASPxLoadingPanel>
    </div>    

</body>  
</html>
