﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxEditors;
using System.Configuration;

namespace travesia
{
    public partial class busquedaCompetidor : System.Web.UI.Page
    {
        DataSet result;
        DataTable dtCompetidor;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.MaintainScrollPositionOnPostBack = true;
                int noCapacidad = 0;
                int noUsados = 0;
                int intIdClienteContactoEvento = 0;
                string strDsNoRifa = "";
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "btnContinuarA_Click")
                { }
                if (!IsPostBack)
                {
                    dtCompetidor = null;
                    int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                    strDsNoRifa = Request.QueryString["token"];
                    //cnPrecargado.Value = intIdClienteContactoEvento.ToString();
                    //hdIdClienteContactoEventoPrincipales.Value = intIdClienteContactoEvento.ToString();
                    utils.Login("usrTriatlon", "eXperiencias");
                    hdDsSession.Value = getDsSession();
                    int intIdSexo = 0;
                    int edad = 0;
                    DateTime fechaEvento = new DateTime(2015, 11, 15);                    
                }
            }
            catch (Exception ex)
            {
                mensaje("Problema en carga de pagina "+ ex.Message, "Ha ocurrido un error");
            }
        }

        

        protected int getMaxAgrupador(DataTable dtCompetidor)
        {
            int maxAgrupador = 0;
            maxAgrupador = Convert.ToInt32(dtCompetidor.Compute("max(noAgrupadorVenta)", "cnModificacion = 0"));
            return maxAgrupador;
        }
    
       
        public void mensaje(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }

        public void mensajeyOcultarDiv(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessageAndHide('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }
        
        public void mostrarOcultarDiv(string divMostrar, string divOcultar, string divOcultar2)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>mostrarOcultarDiv('" + divMostrar + "','" + divOcultar + "','" + divOcultar2 + "');</script>", false);
        }
        public void alert(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + errMensaje + "');</script>", false);
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.dsSession;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                string apellidoPaterno = "";
                string nombre = "";
                string noCompetidor = "";
                string dsFolioVenta = "";
                int idVenta = 0;
                string strNoRifa = "";
                string dsCorreo = "";
                int idEventoClasificacion = 0;
                int idEstatusVenta = 0;
                int idEstatusParticipante = 15; /*15 inscrito*/
                int idClienteContactoEvento = 0;

                apellidoPaterno = txtPaterno.Text;
                nombre = txtNombre.Text;                
                dsCorreo = txtEmail.Text;
                //noCompetidor = (txtNoCompetidor.Text != "") ? int.Parse(txtNoCompetidor.Text) : 0;
                noCompetidor = txtNoCompetidor.Text;
                DataTable dtSoloInscritos = new DataTable();

                DataSet result = utils.buscarCompetidor(cGlobals.dsSession, apellidoPaterno, nombre, noCompetidor, idVenta, strNoRifa, dsCorreo, idEventoClasificacion, idEstatusVenta, idEstatusParticipante);
                
                if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                {
                    dtSoloInscritos = result.Tables[0].Select("idEstatusCliente = 15").CopyToDataTable();
                    gridCompetidores.Visible = true;
                    gridCompetidor.DataSource = dtSoloInscritos;
                    gridCompetidor.DataBind();
                }
            }
            catch (Exception ex)
            { }
        }               
    }
}