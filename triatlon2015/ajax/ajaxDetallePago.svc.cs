﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

using System.Globalization;
using System.Web.Script.Serialization;
using System.Data;

namespace travesia.ajax
{
    //[ServiceContract(SessionMode = SessionMode.Allowed)]
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    
    public class ajaxDetallePago
    {
        
        [OperationContract]
        public string recalcular(string idProducto, string caso, double valor, int idClienteContactoEvento, string idTipoCliente = "")
        {
            cResponseAjax respuesta = new cResponseAjax();
            var serializer = new JavaScriptSerializer();
            try
            {
                // Add your operation implementation here 
              
                double mnPrecioVenta = 0;
                string error = "";
                string timeExecuted = "";
                string response = "";
                DataTable dtCobro = null;
                String jsonDetalleUpgrade = "";
                if (System.Web.HttpContext.Current.Session["dtCompetidorCobro" + idClienteContactoEvento] != null)
                {
                    dtCobro = (DataTable)System.Web.HttpContext.Current.Session["dtCompetidorCobro" + idClienteContactoEvento];
                    switch (caso)
                    {
                        case "cantidad":
                            //cParticipante.dtDataCobro
                            for (int i = 0; i < dtCobro.Rows.Count; i++)
                            {
                                if (dtCobro.Rows[i]["idProducto"].ToString() == idProducto && dtCobro.Rows[i]["idTipoCliente"].ToString() == idTipoCliente)
                                {
                                    // aqui tengo que preguntar si mnPrecio_MS es distinto de null y cadena vacia tome de aui el precio
                                    if (dtCobro.Rows[i]["mnPrecio_MS"].ToString() != "")
                                        mnPrecioVenta = double.Parse(dtCobro.Rows[i]["mnPrecio_MS"].ToString()) * valor;
                                    else
                                        mnPrecioVenta = double.Parse(dtCobro.Rows[i]["mnPrecio_2"].ToString()) * valor;
                                    dtCobro.Rows[i]["mnPrecioVenta"] = mnPrecioVenta;
                                    dtCobro.Rows[i]["cantidad"] = valor;
                                    // se comentó las siguientes lineas 21072015 por indicaciones de Huami
                                    // si es CENA BENEFICIO CRUZ ROJA ACOMPAÑANTE
                                    /*if (cParticipante.dtDataCobro.Rows[i]["dsClave"].ToString() == "CBATXHCR")
                                    {                                    
                                        // entradas xelha adulto
                                        DataRow[] drEntradasXelha = cParticipante.dtDataCobro.Select("dsClave = 'XHETI' and idTipoCliente = 1");
                                        double intCantidadEntradas = double.Parse(drEntradasXelha[0]["cantidad"].ToString());
                                        intCantidadEntradas = intCantidadEntradas + valor;
                                        drEntradasXelha[0]["cantidad"] = intCantidadEntradas;
                                    }
                                    else if (cParticipante.dtDataCobro.Rows[i]["dsClave"].ToString() == "CBAMTXHCR")
                                    {
                                        DataRow[] drEntradasXelha = cParticipante.dtDataCobro.Select("dsClave = 'XHETI' and idTipoCliente = 2");
                                        double intCantidadEntradas = double.Parse(drEntradasXelha[0]["cantidad"].ToString());
                                        intCantidadEntradas = intCantidadEntradas + valor;
                                        drEntradasXelha[0]["cantidad"] = intCantidadEntradas;
                                    }*/
                                }
                            }
                            error = "0";
                            timeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                            response = "ok";
                            break;
                        case "donativo":
                            for (int i = 0; i < dtCobro.Rows.Count; i++)
                            {
                                if (dtCobro.Rows[i]["idProducto"].ToString() == idProducto)
                                {
                                    // aqui tengo que preguntar si mnPrecio_MS es distinto de null y cadena vacia tome de aui el precio
                                    if (dtCobro.Rows[i]["mnPrecio_MS"].ToString() != "")
                                    { }
                                    mnPrecioVenta = double.Parse(valor.ToString());
                                    dtCobro.Rows[i]["mnPrecioVenta"] = mnPrecioVenta;
                                    dtCobro.Rows[i]["cantidad"] = 1;
                                    if (mnPrecioVenta <= 0)
                                    {
                                        dtCobro.Rows[i]["cantidad"] = 0;
                                    }
                                }
                            }
                            error = "0";
                            timeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                            response = "ok";
                            break;
                        default:
                            error = "0";
                            timeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                            response = "ok";
                            break;
                    }
                    System.Web.HttpContext.Current.Session["dtCompetidorCobro" + idClienteContactoEvento] = dtCobro;
                    decimal nuevoTotal = getNuevoTotal(dtCobro);
                    respuesta.Error = error;
                    respuesta.TimeExecuted = timeExecuted;
                    respuesta.Response = nuevoTotal.ToString("N2");
                    jsonDetalleUpgrade = utils.getJson(dtCobro);
                    respuesta.Response2 = jsonDetalleUpgrade;
                    return serializer.Serialize(respuesta);
                }
                respuesta.Error = "1";
                respuesta.TimeExecuted = timeExecuted;
                respuesta.Response = "DATATABLE NULL";
                //jsonDetalleUpgrade = utils.getJson(dtCobro);
                respuesta.Response2 = "DATATABLE NULL";
                return serializer.Serialize(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.Error = "1";
                respuesta.TimeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                respuesta.Response = ex.Message.ToString();
                return serializer.Serialize(respuesta);
            }
        }

        public decimal getNuevoTotal(DataTable dtCobro)
        {
            decimal respuesta = 0;
            decimal respuesta1 = 0;
            double respuesta2 = 0;
            string strRespuesta = "";

            for (int i = 0; i < dtCobro.Rows.Count; i++)
            {
                /*respuesta1 = (decimal)(cParticipante.dtDataCobro.Rows[i]["mnPrecioVenta"]);
                strRespuesta = respuesta1.ToString("N2");
                respuesta2 = double.Parse(strRespuesta);
                respuesta += respuesta2;*/
                respuesta += decimal.Parse(dtCobro.Rows[i]["mnPrecioVenta"].ToString());
                decimal x = Math.Truncate(respuesta * 100) / 100;
            }
            return respuesta;
        }
    }
}
