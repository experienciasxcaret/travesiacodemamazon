﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

using System.Data;
using System.Globalization;
using System.Web.Script.Serialization;

namespace travesia.ajax
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ajaxMenu
    {
        [OperationContract]
        public string getMenu()
        {
            // Add your operation implementation here
            if (cGlobals.dsSession == null || cGlobals.dsSession == "" || cGlobals.dsUsuario == "usrTravesia")
                try
                {
                    utils.LoginU("rhernandezmi", "eXperiencias");
                }
                catch (Exception ex)
                {
                    string msg = ex.Message.ToString();
                    string strginnermessage = "";
                    string strCuerpoCorreo = "getMenu. Falló intento de logeo por falta de session" + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                    string strExcepcion = "";
                    if (ex.ToString().Length > 799)
                        strExcepcion = ex.ToString().Substring(0, 800);
                    else
                        strExcepcion = ex.ToString();
                    strCuerpoCorreo += strExcepcion;
                    MensajeAlert(strCuerpoCorreo);
                }
            var serializer = new JavaScriptSerializer();
            wsBusinessRules.BusinessRulesServicesClient ruleOper = new wsBusinessRules.BusinessRulesServicesClient();
            ruleOper.Open();
            Dictionary<string, string> dicret = new Dictionary<string, string>();
            dicret.Add("@TIPOCONSULTA", "TRAVESIA");
            dicret.Add("@idMenu", "0");
            dicret.Add("@idClienteUsuario", cGlobals.idUsuario.ToString());

            DataSet lista = null;
            try
            {
                lista = ruleOper.ExecuteRule("spGETcMenu", dicret, cGlobals.dsSession);                
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                string strginnermessage = "";
                string strCuerpoCorreo = "getMenu. Falló primer intento intento spGETcMenu " + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                string strExcepcion = "";
                if (ex.ToString().Length > 799)
                    strExcepcion = ex.ToString().Substring(0, 800);
                else
                    strExcepcion = ex.ToString();
                strCuerpoCorreo += strExcepcion;
                MensajeAlert(strCuerpoCorreo);
                lista = ruleOper.ExecuteRule("spGETcMenu", dicret, cGlobals.dsSession);
            }
            String jsonMenu = utils.getJson(lista.Tables[0]);
            return jsonMenu;            
        }
        // Add more operations here and mark them with [OperationContract]
        public void MensajeAlert(string errMensaje)
        {

            wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();
            iGeneral.Open();
            string strCorreos = "nfigueroa@experienciasxcaret.com.mx";
            bool blnBanderaCorreo = iGeneral.SendEmail(strCorreos, "Error en Aplicacion Travesia", errMensaje, true /*es html*/, "", false, 0, "");
            iGeneral.Close();
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + strCuerpoCorreo + "');", false);
        }
    }
}
