﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Globalization;
using System.IO;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Web;
using System.Text.RegularExpressions;
using System.Net;

namespace travesia.ajax
{
    [ServiceContract(Namespace = "ajaxRifa")]//
    //[AspNetCompatibilityRequirements(RequirementsMode=AspNetCompatibilityRequirementsMode.Allowed)] 
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]    
    public class ajaxRifa
    {
       
        [OperationContract]
        public string validaCompetidorRifa(string strEmail)
        {
            var serializer = new JavaScriptSerializer();
            cResponseAjax respuesta = new cResponseAjax();
            try
            {
                bool resultvalidaExisteRegisto = utils.validaExisteRegistro(strEmail, getDsSession()); 
                respuesta.Error = "0";
                respuesta.TimeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                respuesta.Response = resultvalidaExisteRegisto.ToString();
                return serializer.Serialize(respuesta);
                // Add your operation implementation here                
            }
            catch (Exception ex)
            {
                try
                {
                    bool resultvalidaExisteRegisto = utils.validaExisteRegistro(strEmail, getDsSession());
                    respuesta.Error = "0";
                    respuesta.TimeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                    respuesta.Response = resultvalidaExisteRegisto.ToString();
                    return serializer.Serialize(respuesta);
                }
                catch(Exception ex2)
                {
                    respuesta.Error = "1";
                    respuesta.TimeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                    respuesta.Response = ex2.ToString();
                    return serializer.Serialize(respuesta);
                }                
            }
        }

        [OperationContract]
        public string validaCompetidorRifaApellidosYFeNacimiento(string apellidoPaterno, string apellidoMaterno, string feNacimiento)
        {
            var serializer = new JavaScriptSerializer();
            cResponseAjax respuesta = new cResponseAjax();
            try
            {

                //bool resultvalidaExisteRegisto = utils.validaExisteRegistro(strEmail, cGlobals.dsSession);
                bool resultvalidaExisteRegisto = utils.validaExisteRegistroApellidosYFeNacimiento(apellidoPaterno, apellidoMaterno, feNacimiento, cGlobals.dsSession);

                respuesta.Error = "0";
                respuesta.TimeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                respuesta.Response = resultvalidaExisteRegisto.ToString();
                return serializer.Serialize(respuesta);
                // Add your operation implementation here                
            }
            catch (Exception ex)
            {
                respuesta.Error = "1";
                respuesta.TimeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                respuesta.Response = ex.ToString();
                return serializer.Serialize(respuesta);
            }
        }
        // Add more operations here and mark them with [OperationContract]
        [OperationContract]
        // HttpContext context
        public string upload()
        {
            var serializer = new JavaScriptSerializer();
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                // Get the uploaded image from the Files collection
                var httpPostedFile = HttpContext.Current.Request.Files["UploadedFile"];

                if (httpPostedFile != null)
                {
                    // Validate the uploaded image(optional)

                    // Get the complete file path
                    var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles"), httpPostedFile.FileName);

                    // Save the uploaded file to "UploadedFiles" folder
                    httpPostedFile.SaveAs(fileSavePath);
                }
            }
            //HttpFileCollection Files;
            //var File = HttpContext.Current.Request.Files;
            //var postedFile = context.Request.Files;
            return "1";
        }

        [OperationContract]
        //[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "/downloadFile")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public string downloadFile(string url, string fileName)
        {
            var serializer = new JavaScriptSerializer();
            cResponseAjax respuesta = new cResponseAjax();
            //Create a stream for the file
            Stream stream = null;
            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 30000;
            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            try
            {
                //Create a WebRequest to get the file
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(url);
                //File Path and File Name                
                string _DownloadableProductFileName = fileName;
                //Create a response for this request
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();
                if (fileResp.ContentLength > 0)
                {
                    //Get the Stream returned from the response
                    stream = fileResp.GetResponseStream();
                    // prepare the response to the client. resp is the client Response
                    var resp = HttpContext.Current.Response;

                    //Indicate the type of data being sent
                    resp.ContentType = "application/octet-stream";
                    //Name the file 
                    String Header = "Attachment; Filename=" + fileName;
                    resp.AddHeader("Content-Disposition", "attachment; filename=\"" + _DownloadableProductFileName + "\"");
                    resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());
                    resp.AppendHeader("Content-Disposition", Header);
                    int length;
                    do
                    {
                        // Verify that the client is connected.
                        if (resp.IsClientConnected)
                        {
                            // Read data into the buffer.
                            length = stream.Read(buffer, 0, bytesToRead);
                            // and write it out to the response's output stream
                            resp.OutputStream.Write(buffer, 0, length);
                            // Flush the data
                            resp.Flush();
                            //Clear the buffer
                            buffer = new Byte[bytesToRead];
                        }
                        else
                        {
                            // cancel the download if client has disconnected
                            length = -1;
                        }
                    } while (length > 0); //Repeat until no data is read
                }
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
            respuesta.Error = "0";
            return serializer.Serialize(respuesta);
            /*HttpContext.Current.Response.ContentType = "APPLICATION/OCTET-STREAM";
            String Header = "Attachment; Filename=" + fileName;
            HttpContext.Current.Response.AppendHeader("Content-Disposition", Header);
            System.IO.FileInfo Dfile = new System.IO.FileInfo(HttpContext.Current.Server.MapPath(fileName));
            HttpContext.Current.Response.WriteFile(Dfile.FullName);
            HttpContext.Current.Response.End();*/
            //return;
        }
        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {//"usrTriatlon", "eXperiencias"
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.dsSession;
        }
    }
}
