﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

using System.Globalization;
using System.Web.Script.Serialization;

namespace travesia.ajax
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ajaxAgregarCliente
    {
        // To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
        // To create an operation that returns XML,
        //     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     and include the following line in the operation body:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        public string agregarCliente(string nombre, string paterno, string materno, string rfc, string razonSocial, string razonComercial, string telefono, string email, string pais, string estado, string calle, string colonia, string ciudad, string interior, string exterior, string codigoPostal)
        {
            var serializer = new JavaScriptSerializer();
            cResponseAjax respuesta = new cResponseAjax();            
            try
            {
                int respClienteNivelCliente = 0;
                wscCliente.IcClienteClient iClienteClient = new wscCliente.IcClienteClient();
                iClienteClient.Open();
                wscCliente.cCliente cCliente = new wscCliente.cCliente();
                cCliente.cnEsAgencia = false;
                cCliente.idCliente = 1;
                cCliente.dsRazonComercial = razonComercial;
                cCliente.dsRazonSocial = razonSocial;
                cCliente.dsRFC = rfc;
                cCliente.dsTelefono = telefono;
                cCliente.feAlta = DateTime.Now;
                cCliente.dsMunicipio = ciudad;
                cCliente.idPais = int.Parse(pais);
                cCliente.idEstado = int.Parse(estado);
                cCliente.dsCalle = calle;
                cCliente.dsCiudad = ciudad;
                cCliente.dsCodigoPostal = codigoPostal;
                cCliente.dsColonia = colonia;
                cCliente.dsContacto = nombre + " " + paterno + " " + materno;
                cCliente.dsContacto = cCliente.dsContacto.Trim();
                cCliente.dsNumInterior = interior;
                cCliente.dsNumExterior = exterior;
                cCliente.idTipoMonedaFacturacion = 2;
                cCliente.dsCorreoElectronico = email;
                cCliente.dsClave = "";
                cCliente.cnCredito = false;
                cCliente.cnBloqueado = false;
                cCliente.cnAceptaPromociones = false;
                cCliente.cnAccesoWeb = false;
                cCliente.cnCadenaEmpresas = false;
                cCliente.cnEmiteCupon = true;
                cCliente.cnRequiereFactura = true;
                cCliente.idEstatusCliente = 14; // en proceso

                int respIdCliente = iClienteClient.InsertarcCliente(cCliente, cGlobals.dsSession);

                wscClienteNivelCliente.IcClienteNivelClienteClient iClienteNivelCliente = new wscClienteNivelCliente.IcClienteNivelClienteClient();
                wscClienteNivelCliente.cClienteNivelCliente cClienteNivelCliente = new wscClienteNivelCliente.cClienteNivelCliente();

                cClienteNivelCliente.idCliente = respIdCliente;
                cClienteNivelCliente.idNivelCliente = 34;

                iClienteNivelCliente.Open();
                respClienteNivelCliente = iClienteNivelCliente.InsertarcClienteNivelCliente(cClienteNivelCliente, cGlobals.dsSession);
                if (respClienteNivelCliente > 0)
                {
                    respuesta.Error = "0";
                    respuesta.TimeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                    respuesta.Response = respClienteNivelCliente.ToString();
                    // ahora enviar correo a ingresos para que se entere de que se dio de alta un cliente
                    string mensajeMail = cGlobals.notifClienteNuevo;
                    string strCorreos = "nfigueroa@experienciasxcaret.com.mx;hpech@experienciasxcaret.com.mx";
                    mensajeMail = mensajeMail.Replace("[nombre]", cCliente.dsContacto).Replace("[razonsocial]", cCliente.dsRazonSocial).Replace("[RFC]", cCliente.dsRFC);
                    bool respEnvioCorreo = utils.enviaCorreo(strCorreos, "Cliente Nuevo Especial Triatlon 2015", mensajeMail, "fimox.reformer@gmail.com");
                    //lblMensajeRespuesta.Text = "El cliente ya fue dado de alta";
                }
                return serializer.Serialize(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.Error = "1";
                respuesta.TimeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                respuesta.Response = ex.Message;
                return serializer.Serialize(respuesta);
            }        
        }
        // Add more operations here and mark them with [OperationContract]
        [OperationContract]
        public string getRFC(string rfc)
        {
            var serializer = new JavaScriptSerializer();
            cResponseAjax respuesta = new cResponseAjax();
            try
            {
                string strRespuesta= "";
                strRespuesta = utils.getRFC(rfc, 0);
                respuesta.Error = "0";
                respuesta.TimeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                respuesta.Response = strRespuesta;
                return serializer.Serialize(respuesta);
            }
            catch (Exception ex)
            {
                respuesta.Error = "1";
                respuesta.TimeExecuted = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                respuesta.Response = ex.Message;
                return serializer.Serialize(respuesta);
            }
        }
    }
}
