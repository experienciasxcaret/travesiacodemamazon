﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Web;

namespace travesia.ajax
{
    [ServiceContract(Namespace = "ajaxBackoffice")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ajaxBackoffice
    {
        // To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
        // To create an operation that returns XML,
        //     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     and include the following line in the operation body:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        //[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "/downloadFile")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public void downloadFile(string url, string fileName)
        {
            //Create a stream for the file
            Stream stream = null;
            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 30000;
            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

             try
            {
                //Create a WebRequest to get the file
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(url);
                //File Path and File Name                
                string _DownloadableProductFileName = fileName;
                //Create a response for this request
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();
                if (fileResp.ContentLength > 0)
                {
                     //Get the Stream returned from the response
                    stream = fileResp.GetResponseStream();
                     // prepare the response to the client. resp is the client Response
                    var resp = HttpContext.Current.Response;

                    //Indicate the type of data being sent
                    resp.ContentType = "application/octet-stream";
                    //Name the file 
                    String Header = "Attachment; Filename=" + fileName;
                    resp.AddHeader("Content-Disposition", "attachment; filename=\"" + _DownloadableProductFileName + "\"");
                    resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());
                    resp.AppendHeader("Content-Disposition", Header);
                    int length;
                    do
                    {
                        // Verify that the client is connected.
                        if (resp.IsClientConnected)
                        {
                            // Read data into the buffer.
                            length = stream.Read(buffer, 0, bytesToRead);
                            // and write it out to the response's output stream
                            resp.OutputStream.Write(buffer, 0, length);
                            // Flush the data
                            resp.Flush();
                            //Clear the buffer
                            buffer = new Byte[bytesToRead];
                        }
                        else
                        {
                            // cancel the download if client has disconnected
                            length = -1;
                        }
                    } while (length > 0); //Repeat until no data is read
                }
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
            /*HttpContext.Current.Response.ContentType = "APPLICATION/OCTET-STREAM";
            String Header = "Attachment; Filename=" + fileName;
            HttpContext.Current.Response.AppendHeader("Content-Disposition", Header);
            System.IO.FileInfo Dfile = new System.IO.FileInfo(HttpContext.Current.Server.MapPath(fileName));
            HttpContext.Current.Response.WriteFile(Dfile.FullName);
            HttpContext.Current.Response.End();*/
            return;
        }

        // Add more operations here and mark them with [OperationContract]
    }
}
