﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Image = System.Drawing.Image;


namespace travesia
{
    public partial class upload : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // gets an HttpFileCollection of all the files that have been uploaded
            var uploadFilCol = Request.Files;

            // so now we need to iterate through the list of files and deal with each one in 
            // turn
            for (var i = 0; i < uploadFilCol.Count; i++)
            {
                // chuck the current file in the loop in a variable called file
                var file = uploadFilCol[i];

                // call CheckFileIsAnImage to make sure some muppet hasn't tried to upload a 
                // txt file or something
                var msg = CheckFileIsAnImage(file);

                // if msg is empty we know our file is an ok image, if not we need to show a 
                // message to the user
                if (!string.IsNullOrEmpty(msg))
                    ShowMessage(msg, i);
                else
                {
                    // our file is ok. so now we get the filename of the file we are looking at
                    var fileName = Path.GetFileName(file.FileName);

                    // then we call CheckAndCreateDirectory which sees if the directory 
                    // structure we want to save to exists.
                    // if it doesn't exist it creates the directory for us.
                    // either way, the method will return the path to save the image to.
                    //
                    // this is where you would do some work on creating a custom folder root 
                    // if that's what you wanted (for example if you wanted another folder 
                    // within the storage system that was the artists name, or a gallery
                    // id, etc, etc.
                    var savePath = CheckAndCreateDirectory(
                           Server.MapPath(GlobalConstants.Imaging.SaveFolderNameServerPath));

                    // now we save the image to the server. in the process we allocate a new 
                    // filename (in this case a guid).
                    // you can use whatever you like here but remember all image names must 
                    // be unique.
                    var newFilename = SaveUploadedImageToServer(savePath, file);

                    // here is where you would save the newFilename to the db

                    // we've got this far so we can tell the user this file was saved ok
                    ShowMessage(" " + fileName + " uploaded successfully", i);
                }
            }

            // refresh the repeater with all images in the test folder
            //GetImagesCurrentlyInTestFolder();
        }

        /// <summary>
        /// Checks the file is an image.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        public static string CheckFileIsAnImage(HttpPostedFile file)
        {
            // if the file is null or has no content it's obviously a dirty wrongun
            if (file == null) return "file is invalid, upload must have failed";
            if (file.ContentLength == 0) return "no file specified";

            // get the filename extension (e.g .jpg, .gif, etc) from the filename
            var extension = Path.GetExtension(file.FileName).ToLower();

            // we're only supporting jpg, png and gif in this (you can support more if you 
            // like though)
            if (extension != ".jpg" && extension != ".gif" && extension != ".png")
                return "unsupported image type";

            // if we got this far the file looks good, so return an empty string (which 
            // means everything ok)
            return string.Empty;
        }

        /// <summary>
        /// Checks the directory exists and creates if not.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <returns></returns>
        public static string CheckAndCreateDirectory(string directory)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            return directory;
        }

        /// <summary>
        /// Shows the file upload message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="fileUploadPos">The file upload pos.</param>
        private void ShowMessage(string message, int fileUploadPos)
        {
            // show messages for each file being uploaded (3 files, so have to have a 
            // message for each)
            switch (fileUploadPos)
            {
                case 0:
                    Label1.Text = message;
                    break;
                case 1:
                    Label2.Text = message;
                    break;
                default:
                    //Label3.Text = message;
                    break;
            }
        }

        /// <summary>
        /// Saves the uploaded image to the server.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        public string SaveUploadedImageToServer(String filePath, HttpPostedFile file)
        {
            // first thing to do is create a new filename root by using a guid and the 
            // current file extension.
            // i use the guid as the filename and as the unique media id in the database, 
            // but you don't have to do it this way
            var mediaId = Guid.NewGuid();
            var fileNameExtension = Path.GetExtension(file.FileName).ToLower();

            // now we iterate through our image dimensions list so we can create the image 
            // sizes we want from the image uploaded.
            // we are creating 3 images - 100x100, 250x250 and 450x450
            foreach (var imageDim in GlobalConstants.Imaging.ImageDims)
            {
                // get the file as an image
                var uploadedImage = Image.FromStream(file.InputStream);
                // create our new filename using the media id, file extension and image 
                // dimensions.
                var fileName = imageDim + "x" + imageDim + "_" + mediaId + fileNameExtension;
                // now get the full server path and add on the filename
                var fullSavePath = filePath + "\\" + fileName;
                // we need to get the right ImageFormat so we don't try to save a jpg as a 
                // png, etc...
                var imageType = GetImageType(fileNameExtension);

                // if the height and width of the uploaded image are less that the size we 
                // are aiming for (e.g we want 100x100, they uploaded 80x80), just save the 
                // image as is.
                if (uploadedImage.Height <= imageDim && uploadedImage.Width <= imageDim)
                {
                    // save the image
                    uploadedImage.Save(fullSavePath, imageType);
                    // then get rid of it from memory
                    uploadedImage.Dispose();
                }
                else
                {
                    var newWidth = imageDim;
                    var newHeight = imageDim;

                    // check the heigh and width ratio. if we are making a 100x100 image, 
                    // we may need to make it 70x100 or 100x70 depending
                    // whether the image is landscape or portrait
                    if (uploadedImage.Height > uploadedImage.Width)
                        newWidth = (Int32)Math.Ceiling(
                            ((double)uploadedImage.Width / uploadedImage.Height) * imageDim);
                    else
                        newHeight = (Int32)Math.Ceiling(
                            ((double)uploadedImage.Height / uploadedImage.Width) * imageDim);

                    // now write a new image out using the dimensions we've calculated
                    var smallerImg = new Bitmap(newWidth, newHeight);
                    var g = Graphics.FromImage(smallerImg);

                    // a few options, no need to worry about these
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    foreach (var pItem in uploadedImage.PropertyItems)
                    {
                        smallerImg.SetPropertyItem(pItem);
                    }

                    // draw the image
                    g.DrawImage(uploadedImage, new Rectangle(0, 0, newWidth, newHeight));

                    // get rid of the uploaded image stream from memory
                    uploadedImage.Dispose();

                    // save the new file
                    smallerImg.Save(fullSavePath, imageType);

                    // get rid of the new file from memory
                    smallerImg.Dispose();
                }
            }

            // return the new filename
            return mediaId + fileNameExtension;
        }

        /// <summary>
        /// Gets the type of the image.
        /// </summary>
        /// <param name="fileExt">The file ext.</param>
        /// <returns></returns>
        private static ImageFormat GetImageType(string fileExt)
        {
            switch (fileExt)
            {
                case ".jpg":
                    return ImageFormat.Jpeg;
                case ".gif":
                    return ImageFormat.Gif;
                default: // (png)
                    return ImageFormat.Png;
            }
        }

        /// <summary>
        /// Handles the Click event of the DeleteImageBtn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> 
        /// instance containing the event data.</param>
        public void DeleteImageBtn_Click(Object sender, CommandEventArgs e)
        {
            DeleteImageFromDb(e);

            DeleteImageFromServer(e);

            //GetImagesCurrentlyInTestFolder();
        }

        /// <summary>
        /// Deletes the image from server.
        /// </summary>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> 
        /// instance containing the event data.</param>
        private void DeleteImageFromServer(CommandEventArgs e)
        {
            DeleteImage(Server.MapPath(GlobalConstants.Imaging.SaveFolderNameServerPath),
                e.CommandArgument.ToString());
        }

        /// <summary>
        /// Deletes the image.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        /// <param name="mediaId">The media id.</param>
        public static void DeleteImage(String directoryPath, String mediaId)
        {
            DeleteImagesInList(Directory.GetFiles(directoryPath, "*" + mediaId + "*"));
        }

        /// <summary>
        /// Deletes the images in list.
        /// </summary>
        /// <param name="imgList">The img list.</param>
        private static void DeleteImagesInList(IEnumerable<string> imgList)
        {
            foreach (var img in imgList)
            {
                var imgInfo = new FileInfo(img);
                imgInfo.Delete();
            }
        }

        /// <summary>
        /// Deletes the image from db.
        /// </summary>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> 
        /// instance containing the event data.</param>
        private void DeleteImageFromDb(CommandEventArgs e)
        {
            // here you would remove all references to the particular image in the db
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string fuck ="fuck";
        }

        /// <summary>
        /// Gets the images currently in test folder and bind to the photo repeater.
        /// </summary>
       
        
    }
}