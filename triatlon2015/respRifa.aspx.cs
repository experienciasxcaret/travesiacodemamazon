﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxEditors;
using System.Configuration;

namespace travesia
{
    public partial class respRifa : System.Web.UI.Page
    {
        DataSet result;
        DataTable dtCompetidor;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.MaintainScrollPositionOnPostBack = true;
                int noCapacidad = 0;
                int noUsados = 0;
                int intIdClienteContactoEvento = 0;
                string strDsNoRifa = "";
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "btnContinuarA_Click")
                { }
                if (!IsPostBack)
                {
                    dtCompetidor = null;
                    int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                    strDsNoRifa = Request.QueryString["token"];
                    //cnPrecargado.Value = intIdClienteContactoEvento.ToString();
                    //hdIdClienteContactoEventoPrincipales.Value = intIdClienteContactoEvento.ToString();
                    utils.Login("usrTriatlon", "eXperiencias");
                    hdDsSession.Value = getDsSession();
                    int intIdSexo = 0;
                    int edad = 0;
                    DateTime fechaEvento = new DateTime(2015, 11, 15);
                    if (strDsNoRifa != "")
                    {
                        //result = utils.buscarCompetidor(getDsSession(), "", "", 0, 0, strDsNoRifa, "", 0, 0);
                        result = utils.buscarCompetidor(getDsSession(), "", "", "", 0, "", "", 0, 0, 0, intIdClienteContactoEvento);
                        if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                        {
                            if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                            {
                                result.Tables[0].Columns.Add("feVisitaNuevo", typeof(string));                                
                                result.Tables[0].Columns.Add("idEventoClasificacionNuevo", typeof(int));
                                result.Tables[0].Columns.Add("dsCodigoNuevo", typeof(string));
                                result.Tables[0].Columns.Add("idProductoPrecioNuevo", typeof(int));
                                result.Tables[0].Columns.Add("idProductoNuevo", typeof(int));
                                result.Tables[0].Columns.Add("precioListaNuevo", typeof(decimal));
                                result.Tables[0].Columns.Add("mnTipoCambioNuevo", typeof(decimal));
                                result.Tables[0].Columns.Add("idTipoClienteNuevo", typeof(int));
                                result.Tables[0].Columns.Add("idTipoMonedaNuevo", typeof(int));
                                dtCompetidor = result.Tables[0];
                               
                            }


                            DataRow[] dtArrComponente = dtCompetidor.Select("idClienteContactoEvento = '" + intIdClienteContactoEvento.ToString() + "'");
                            foreach (var data in dtArrComponente)
                            {
                                lblMensajeCabecera.Text = "¡ Felicidades " + data["dsContacto"].ToString() + ", te haz inscrito a la rifa !";
                                lblMensajeRifa.Text = "Número de rifa generado : " + data["dsNorifa"].ToString() + ".";
                                lblMensajeRifa2.Text = "Se le envió correo a : " + data["dsCorreoElectronico"].ToString();
                                hdIdProductoOriginal.Value = data["idProducto"].ToString();
                                hdIdClienteContactoEventoPrincipales.Value = data["idClienteContactoEvento"].ToString();                              
                                hdIdProductoOriginal.Value = data["idProducto"].ToString();
                                hdIdVentaDetalle.Value = data["idVentaDetalle"].ToString();
                                hdIdVenta.Value = data["idVenta"].ToString();
                                hdIdPais.Value = data["idPais"].ToString();
                                hdCorreoCompetidor.Value = data["dsCorreoElectronico"].ToString();
                            }                                                
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                mensaje("Problema en carga de pagina "+ ex.Message, "Ha ocurrido un error");
            }
        }

        

        protected int getMaxAgrupador(DataTable dtCompetidor)
        {
            int maxAgrupador = 0;
            maxAgrupador = Convert.ToInt32(dtCompetidor.Compute("max(noAgrupadorVenta)", "cnModificacion = 0"));
            return maxAgrupador;
        }
    
       
        public void mensaje(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }

        public void mensajeyOcultarDiv(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessageAndHide('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }
        
        public void mostrarOcultarDiv(string divMostrar, string divOcultar, string divOcultar2)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>mostrarOcultarDiv('" + divMostrar + "','" + divOcultar + "','" + divOcultar2 + "');</script>", false);
        }
        public void alert(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + errMensaje + "');</script>", false);
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.dsSession;
        }               
    }
}