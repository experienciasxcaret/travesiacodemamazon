﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxEditors;
using System.Configuration;
using travesia.wsBusinessRules;

namespace travesia
{
    public partial class cobroDiferencia : System.Web.UI.Page
    {
        DataSet result;
        DataTable dtCompetidor;
        cRespuestaPago cRespuestaPagoCompetidor = new cRespuestaPago();
        cParticipante cCompetidorPagar = new cParticipante();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    int intIdVenta = 0;
                    utils.Login("usrTriatlon", "eXperiencias");

                    int.TryParse(Request.QueryString["refVenta"], out intIdVenta);

                    utils.llenaAnioUp(ddlAnioExpira);

                    utils.getFormasPago(ddlFormaPago, int.Parse(ConfigurationManager.AppSettings["idCanalVenta"]), getDsSession());
                    utils.getBancos(ddlBanco, getDsSession());
                    utils.getMoneda(ddlCambioMoneda, getDsSession());
                    ddlCambioMoneda.Attributes.Add("readonly", "readonly");
                    ddlCambioMoneda.Attributes.Add("disabled", "disabled");

                    if (intIdVenta != 0)
                    {
                        try
                        {


                            wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();
                            Dictionary<string, string> dicret = new Dictionary<string, string>();
                            dicret.Clear();
                            dicret.Add("@idVenta", intIdVenta.ToString());
                            DataSet datos = reglas.ExecuteRule("SpTriatlonDatosPendienteCobro", dicret, getDsSession());
                            dicret.Clear();
                            reglas.Close();

                            if (datos.Tables[0].Rows.Count >= 1)
                            {
                                double dimporte = double.Parse(datos.Tables[0].Rows[0][4].ToString());
                                lblFolio.Text = datos.Tables[0].Rows[0][1].ToString();
                                lblCompetidor.Text = datos.Tables[0].Rows[0][2].ToString();
                                lblProducto.Text = datos.Tables[0].Rows[0][3].ToString();
                                lblNoCompetidor.Text = datos.Tables[0].Rows[0][6].ToString();
                                hdCorreoCompetidor.Value = datos.Tables[0].Rows[0][5].ToString();
                                lblImporte.Text = dimporte.ToString("N2") + " MXN";
                                hdToTalPagar.Value = dimporte.ToString("N2");
                                hddsClaveVenta.Value = datos.Tables[0].Rows[0][1].ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            mensaje("Error en el numero de referencia ** no existe ó ya esta pagado ***", "Ha ocurrido un error");
                        }
                    }
                    else
                    {
                        mensaje("Error en el numero de referencia ** no existe ***", "Ha ocurrido un error");
                    }

                }


            }
            catch (Exception ex)
            {
                mensaje("Problema en carga de pagina "+ ex.Message, "Ha ocurrido un error");
            }
        }

        protected void rblTipoTarjeta_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                /*
                 * si el valor es :
                 * 1 = visa
                 * 2 = mastercard
                 * 3 = amex   
                 * <option value="">Seleccionar Forma de pago</option>
	                <option value="36">American Express TPV Virtual</option>
	                <option value="28">Tarjeta Credito / Mastercard TPV Virtual</option>
	                <option value="27">Tarjeta Credito / Visa TPV Virtual</option>
	                <option value="30">Tarjeta Debito / Mastercard TPV Virtual</option>
	                <option value="29">Tarjeta Debito / Visa TPV Virtual</option>
                 * */
                string strTipoTarjeta = rblTipoTarjeta.SelectedValue.ToString();

                foreach (ListItem li in ddlFormaPago.Items)
                {
                    li.Enabled = true;
                }
                ddlFormaPago.SelectedIndex = -1;
                ddlBanco.Attributes.Remove("readonly");
                ddlBanco.Attributes.Remove("disabled");
                ddlBanco.Items.FindByValue("35").Enabled = true;
                ddlBanco.SelectedIndex = -1;

                switch (strTipoTarjeta)
                {   // si el seleccionado es 1 tenemos que dejar solo visible en las formas de pago las que son de mastercard
                    case "1":
                        //ddlFormaPago.Items.FindByValue("36").Attributes.Add("style", "visibility: hidden");
                        //ddlFormaPago.Items.FindByValue("27").Attributes.Add("style", "visibility: hidden");
                        //ddlFormaPago.Items.FindByValue("29").Attributes.Add("style", "visibility: hidden");
                        ddlFormaPago.Items.FindByValue("36").Enabled = false;
                        ddlFormaPago.Items.FindByValue("27").Enabled = false;
                        ddlFormaPago.Items.FindByValue("29").Enabled = false;
                        ddlFormaPago.Items.FindByValue("28").Selected = true;
                        ddlBanco.Items.FindByValue("35").Enabled = false;
                        break;
                    // si el seleccionado es 2 tenemos que dejar solo visible en las formas de pago las que son de visa
                    case "2":
                        ddlFormaPago.Items.FindByValue("36").Enabled = false;
                        ddlFormaPago.Items.FindByValue("28").Enabled = false;
                        ddlFormaPago.Items.FindByValue("30").Enabled = false;
                        ddlFormaPago.Items.FindByValue("27").Selected = true;
                        ddlBanco.Items.FindByValue("35").Enabled = false;
                        break;
                    // si el seleccionado es 3 tenemos que dejar solo visible en las formas de pago las que son de amex
                    case "3":
                        ddlFormaPago.Items.FindByValue("27").Enabled = false;
                        ddlFormaPago.Items.FindByValue("29").Enabled = false;
                        ddlFormaPago.Items.FindByValue("28").Enabled = false;
                        ddlFormaPago.Items.FindByValue("30").Enabled = false;
                        ddlFormaPago.Items.FindByValue("36").Selected = true;
                        rblTipoTarjeta.Items.FindByValue("3").Selected = true;
                        ddlBanco.Items.FindByValue("35").Selected = true;
                        ddlBanco.Attributes.Add("readonly", "readonly");
                        ddlBanco.Attributes.Add("disabled", "disabled");
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void ddlFormaPago_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strIdFormaPago = ddlFormaPago.SelectedValue.ToString();
                lblNumTarjeta.Visible = true;
                txtNoTarjeta.Visible = true;
                lblFechaExpira.Visible = true;
                //txtMes.Visible = true;
                //txtAnio.Visible = true;
                ddlAnioExpira.Visible = true;
                ddlMesExpira.Visible = true;
                lblCVV.Visible = true;
                txtCVV.Visible = true;

                rblTipoTarjeta.ClearSelection();
                ddlFormaPago.Enabled = true;

                ddlBanco.Attributes.Remove("readonly");
                ddlBanco.Attributes.Remove("disabled");
                ddlBanco.Items.FindByValue("35").Enabled = true;
                ddlBanco.SelectedIndex = -1;

                if (strIdFormaPago == "36")
                {
                    ddlBanco.SelectedIndex = -1;
                    rblTipoTarjeta.Items.FindByValue("3").Selected = true;
                    ddlBanco.Items.FindByValue("35").Selected = true;
                    ddlBanco.Attributes.Add("readonly", "readonly");
                    ddlBanco.Attributes.Add("disabled", "disabled");
                    //ddlBanco.Enabled = false;
                }
                else if (strIdFormaPago == "28" || strIdFormaPago == "30")
                {
                    rblTipoTarjeta.Items.FindByValue("1").Selected = true;
                    ddlBanco.Items.FindByValue("35").Enabled = false;
                }
                else if (strIdFormaPago == "27" || strIdFormaPago == "29")
                {
                    rblTipoTarjeta.Items.FindByValue("2").Selected = true;
                    ddlBanco.Items.FindByValue("35").Enabled = false;
                }
                string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
                string dsIsoMoneda = selectedValueMoneda[1].ToString();
                string idMoneda = selectedValueMoneda[0].ToString();
                string idBanco = ddlBanco.SelectedValue.ToString();
                string idFormaPago = ddlFormaPago.SelectedValue.ToString();
                hdCnGlobalCollect.Value = "0";
                hdCnGlobalCollectI.Value = "0";
                if (idBanco != "" && idBanco != "35" && idFormaPago != "" && idFormaPago != "36")
                    validaCamposGlobal(idMoneda, idBanco);

            }
            catch (Exception ex)
            {
            }
        }

        protected void ddlBanco_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
            string dsIsoMoneda = selectedValueMoneda[1].ToString();
            string idMoneda = selectedValueMoneda[2].ToString();
            string idBanco = selectedValueMoneda[0].ToString();
            validaCamposGlobal(idMoneda, idBanco);
        }

        protected void ddlCambioMoneda_SelectedIndexChanged(object sender, EventArgs e)
        {
            // cParticipante.dtCompedidor = result.Tables[0];
            bool cnCruzRoja = false;
            // wscMoneda.IcMonedaClient IcMoneda = new wscMoneda.IcMonedaClient();
            //IcMoneda.Open();
            //wscMoneda.ListacMonedas lstMonedas = IcMoneda.SeleccionarcMonedaPorId();
            //IcMoneda.Close();

            string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
            string dsIsoMoneda = selectedValueMoneda[1].ToString();
            string strDsCodigo = dtCompetidor.Rows[0]["dsCodigo"].ToString();
            string idMoneda = selectedValueMoneda[2].ToString();
            string idBanco = ddlBanco.SelectedValue.ToString();
            string idFormaPago = ddlFormaPago.SelectedValue.ToString();
            bool cnInternational = false;
            decimal dcTipoCambioNuevo = 0;
            decimal dcTipoCambioPesoDolar = 0;
            string idTipoMoneda = "";
            // Ahora tengo que investigar el precio del nuevo tipo de moneda para que se actualice en la pantalla           
            DataSet result = utils.getPrecio(getDsSession(),141, strDsCodigo, 1, cnCruzRoja, dsIsoMoneda);
            DataTable dtCena = null;
            DataTable dtProductoInscripcion = null;
            dtProductoInscripcion = result.Tables[0];
            //  preguntamos si la tabla 2 trae info para esto saber el tamaño del datatable
            int sizeDataSet = result.Tables.Count;
            if (sizeDataSet > 1)
            {
                dtProductoInscripcion.Merge(result.Tables[1]);
            }

            ///////////*******************************************************************////////////////////////

            

            if (idBanco != "" && idFormaPago != "" && idFormaPago != "36")
                validaCamposGlobal(idMoneda, idBanco);

            // Ahora voy a cambiar el valor de dropdownlist, al valor que le corresponde al nuevo tipo de cambio
            decimal dcValorDdlDonativo = 0;
            DropDownList ResultsLabel = FindControl("1616") as DropDownList;

            
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "myScript", "construyeTablaDetalle(" + jsonDetalleUpgrade + ", 1);actualizaCombo('" + cnInternational + "'," + dcTipoCambioNuevo + "," + dcTipoCambioPesoDolar + "," + idTipoMoneda + ")", true);
        }
        public cRespuestaPago getObjetoRespuestaPago()
        {
            if (cRespuestaPagoCompetidor == null)
                cRespuestaPagoCompetidor = (cRespuestaPago)Session["cRespuestaPagoCobro" + cnIdContactoEvento.Value];
            return cRespuestaPagoCompetidor;
        }

        public DataTable getDatatableCompetidor()
        {
            if (cCompetidorPagar.DtCompetidor == null)
                cCompetidorPagar.DtCompetidor = (DataTable)Session["dtCompetidorPago" + cnIdContactoEvento.Value];
            return cCompetidorPagar.DtCompetidor;
        }

        protected bool fnGuardarPago(int intIdCliente, int intIdVenta, decimal dcTotal, string idMoneda)
        {
            int intIdTipoMoneda = 0;
            bool banderaPago = true;
            int intIdCanalVenta = int.Parse(ConfigurationManager.AppSettings["idCanalVenta"]);
            string totalPagar = dcTotal.ToString("N2");

            string strFolios = hddsClaveVenta.Value;
            string strIdVenta = hdIdVenta.Value;
            string[] strIdVentas = { strIdVenta};
            string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
            string dsIsoMoneda = selectedValueMoneda[1].ToString();
            // string idMoneda = selectedValueMoneda[0].ToString();            
            string idTipoMoneda = selectedValueMoneda[2].ToString();

            cRespuestaPagoCompetidor = getObjetoRespuestaPago();
            for (int l = 0; l <= (strIdVentas.Length - 1); l++)
            {
                wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                wsTransactions.kVenta laVenta = new wsTransactions.kVenta();

                ikventa.Open();

                laVenta = ikventa.SeleccionarkVentaPorIdVenta(int.Parse(strIdVentas[l].ToString()), getDsSession());
                //wsTransactions.ListakVentaDetalles laVentaDetalles = ikventa.SeleccionarkVentasDetallePorIdVenta(int.Parse(strIdVentas[l].ToString()), cGlobals.dsSession);
                ikventa.Close();
                intIdTipoMoneda = int.Parse(idTipoMoneda);

                //------------------------------------------------------------------------------------------------
                // PAGO
                //------------------------------------------------------------------------------------------------

                wsTransactions.IkPagoClient IkPago = new wsTransactions.IkPagoClient();
                wsTransactions.ListakPagoTransacciones lstkPagoTransa = new wsTransactions.ListakPagoTransacciones();
                wsTransactions.kPagoTransaccion PagoTran = new wsTransactions.kPagoTransaccion();
                wsTransactions.kPago objkPago = new wsTransactions.kPago();

                objkPago.idPago = 0;
                objkPago.idVenta = int.Parse(strIdVentas[l].ToString());
                objkPago.idCanalPago = intIdCanalVenta; //11; // fijo ?
                objkPago.feTransaccionTotal = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
                objkPago.hrTransaccionTotal = DateTime.Now;
                //objkPago.mnTransaccionTotal = Math.Round(dcTotal, 2);
                //objkPago.mnTransaccionTotal = decimal.Parse(totalPagar);
                objkPago.mnTransaccionTotal = dcTotal;
                objkPago.cnEsPrepago = false;
                objkPago.noAgrupadorVenta = 1;
                objkPago.idFormaPago = Int32.Parse(ddlFormaPago.SelectedItem.Value);
                objkPago.idEstatusPago = Int32.Parse(cRespuestaPagoCompetidor.strIdEstatusPago); //intIdEstatusPago;
                objkPago.feAlta = DateTime.Now;
                objkPago.idClienteUsuarioAlta = (cGlobals.idUsuario);

                PagoTran.idPago = objkPago.idPago; //
                // En idMoneda se guarda el IdTipoMoneda (Gabriel 20082013)
                PagoTran.idTipoMoneda = intIdTipoMoneda; //Int32.Parse(cmbMoneda.SelectedItem.Value);

                PagoTran.feTransaccion = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
                PagoTran.hrTransaccion = DateTime.Now;
                PagoTran.dsRespuesta = cRespuestaPagoCompetidor.strDsRespuesta; // strDsRespuesta;
                PagoTran.dsReferencia = cRespuestaPagoCompetidor.strDsReferencia; // strDsReferencia;
                PagoTran.dsJustificacion = cRespuestaPagoCompetidor.strDsJustificacion; // strDsJustificacion;

                //PagoTran.mnTransaccion = Math.Round(dcTotal, 2);
                //PagoTran.mnTransaccion = decimal.Parse(totalPagar);
                PagoTran.mnTransaccion = dcTotal;

                PagoTran.dsTransaccion = cRespuestaPagoCompetidor.strDsTransaccion; // strDsTransaccion;
                PagoTran.dsCorrelacion = cRespuestaPagoCompetidor.strDsCorrelacion; // strDsCorrelacion;
                PagoTran.noPagosMSI = 0;
                objkPago.cnPlanDePagos = false;

                PagoTran.cnPrepago = false;
                //if (intIdClienteTarjeta > 0)
                if (Int32.Parse(cRespuestaPagoCompetidor.strIdClienteTarjeta) > 0)
                    PagoTran.idClienteTarjeta = Int32.Parse(cRespuestaPagoCompetidor.strIdClienteTarjeta); // intIdClienteTarjeta;
                PagoTran.mnBancoComision = 0;
                PagoTran.prBancoComision = 0;
                PagoTran.feAlta = DateTime.Now;
                PagoTran.idClienteUsuarioAlta = (cGlobals.idUsuario);
                PagoTran.idPagoTransaccion = 0;
                PagoTran.idEstatusPago = Int32.Parse(cRespuestaPagoCompetidor.strIdEstatusPago);
                if (Int32.Parse(cRespuestaPagoCompetidor.strIdBancoReceptor) > 0)
                    PagoTran.idBancoReceptor = Int32.Parse(cRespuestaPagoCompetidor.strIdBancoReceptor);

                PagoTran.dsAfiliacion = cRespuestaPagoCompetidor.strAfiliacion;

                if (Int32.Parse(ddlBanco.SelectedItem.Value) > 0) // temporal en lo que se corrige la clase
                    PagoTran.idBanco = Int32.Parse(ddlBanco.SelectedItem.Value);

                lstkPagoTransa.Add(PagoTran);
                int intIdPago;
                try
                {
                    // 1 er intento
                    intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());

                }
                catch (Exception err)
                {
                    // mensaje("OE fallo primer intento, intIdTipoMoneda=" + intIdTipoMoneda.ToString() + strCadenaResultadoPagoTarjeta, err);
                    try
                    {
                        // 2 er intento
                        intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                    }
                    catch (Exception err2)
                    {
                        // 3 er intento
                        // mensaje("OE fallo segundo intento, intIdTipoMoneda=" + intIdTipoMoneda.ToString() + strCadenaResultadoPagoTarjeta, err);
                        intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                    }
                }
                IkPago.Close();
            }

            //if (intIdEstatusPago == 1) // Pago con tarjeta aprobada  --  2 /*Aceptado*/: 1 /*Declinado*/
            if (Int32.Parse(cRespuestaPagoCompetidor.strIdEstatusPago) == 1)
            {
                banderaPago = false;
            }

            return banderaPago;
        }

        protected bool correoPago(string strIdVenta, string strFolios, string strIdioma, string strCorreoCliente)
        {
            //string strParametros = "<br>Parametros pasados a la funcion correoPago: strIdVenta:" + strIdVenta + "<br>strFolios:" + strFolios + "<br>strIdioma:" + strIdioma + "<br>strCorreoCliente:" + strCorreoCliente + "<br>lblMensajeCorreo:" + lblMensajeCorreo.Text;
            int intIdClienteContacto = 0;
            try
            {
                wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();
                iGeneral.Open();
                // Textos Correos
                wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                icConfiguracion.Open();

                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto2CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto2CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto10CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto10CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto11CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto11CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto12CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto12CorreoPagoIdioma_" + strIdioma, getDsSession());
                // wscConfiguracionAplicacion.cConfiguracionAplicacion Texto13CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto13CorreoPagoIdioma_" + strIdioma, Globals.dsSession);
                wscConfiguracionAplicacion.cConfiguracionAplicacion TextoUrlCuponIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-URLCUPON_CanalVenta_27_Id_" + strIdioma, getDsSession());

                icConfiguracion.Close();
                string nombre = lblCompetidor.Text;
                string numeroCompetidor = lblNoCompetidor.Text;
                string strCuerpoCorreo = "<table>";
                string strCuerpoCorreoLinksFolios = "";
                //strIdVenta = strIdVenta.Remove(strIdVenta.Length - 1);
                string[] arrStrIdVenta = strIdVenta.Split(',');

                for (int ab = 0; ab < arrStrIdVenta.Length; ab++)
                {
                    wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                    ikventa.Open();
                    wsTransactions.kVenta laVenta = ikventa.SeleccionarkVentaPorIdVenta(int.Parse(arrStrIdVenta[ab]), getDsSession());
                    strFolios += laVenta.dsClaveVenta + ",";
                    strCuerpoCorreoLinksFolios += "<a href=\"" + TextoUrlCuponIdioma.dsValor.Replace("<idVenta>", arrStrIdVenta[ab]) + "\" >" + laVenta.dsClaveVenta + "</a>" + ((arrStrIdVenta.Length == 1 || ab == (arrStrIdVenta.Length - 1)) ? "" : ",");
                }
                //strFolios = strFolios.Remove(strFolios.Length - 1);
                strCuerpoCorreo += "<tr><td colspan=\"2\"> Estimado/a  <b>" + nombre + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"> Te informamos que de acuerdo a tu solicitud, hemos confirmado tu pagó de la diferencia. </td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto2CorreoPagoIdioma.dsValor + " " + strFolios + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>Numero Competidor: </b>" + lblNoCompetidor.Text +"</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + "Importante: Por favor lee detenidamente e imprime el documento del enlace (";
                strCuerpoCorreo += strCuerpoCorreoLinksFolios;


                //string[] arrStrFolios = strFolios.Split(',');


                //strFolios = strFolios.Remove(strFolios.Length - 1);

                strCuerpoCorreo += ") y presentalo al momento de recoger tu paquete de competidor  </b></td></tr>";

                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">Si deseas una factura, por favor entra a: https://www.aolxcaret.com/core/facturacion, recuerda que tienes 24 horas para solicitar tu factura </td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                //strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto10CorreoPagoIdioma.dsValor + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto11CorreoPagoIdioma.dsValor + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">" + Texto12CorreoPagoIdioma.dsValor + "</td></tr>";

                strCuerpoCorreo += "</table>";

                string strCorreoAgente = "";

                /* wscClienteUsuario.IcClienteUsuarioClient IcclienteUsuario = new wscClienteUsuario.IcClienteUsuarioClient();
                 IcclienteUsuario.Open();
                 wscClienteContacto.IcClienteContactoClient IcClienteContacto = new wscClienteContacto.IcClienteContactoClient();
                 IcClienteContacto.Open();

                 wscClienteUsuario.cClienteUsuario cclienteUsuario = IcclienteUsuario.SeleccionarcClienteUsuarioPorId(Int32.Parse(cGlobals.idUsuario.ToString()), cGlobals.dsSession);
                 intIdClienteContacto = (int)cclienteUsuario.idClienteContacto;

                 wscClienteContacto.cClienteContacto cClienteContacto = IcClienteContacto.SeleccionarcClienteContactoPorId(intIdClienteContacto, cGlobals.dsSession);
                 strCorreoAgente = cClienteContacto.dsCorreoElectronico;

                 IcclienteUsuario.Close();
                 IcClienteContacto.Close();*/

                bool blnBanderaCorreo = iGeneral.SendEmail(strCorreoCliente, "Confirmación de Pago de diferencia Triatlón Xel-Há", utils.armaHTMLBaseCorreo("Confirmación de Pago de diferencia Triatlón Xel-Há", strCuerpoCorreo), true /*es html*/, "", false, 0, strCorreoAgente);
                bool blnBanderaCorreo1 = iGeneral.SendEmail("faraujo@experienciasxcaret.com.mx", "Confirmación de Pago de diferencia Triatlón Xel-Há", utils.armaHTMLBaseCorreo("Confirmación de Pago de diferencia Triatlón Xel-Há", strCuerpoCorreo), true /*es html*/, "", false, 0, strCorreoAgente);

                if (blnBanderaCorreo)
                {
                    //    lblMensajeCorreo.Text = lblMensajeCorreo.Text.Trim() + ((lblMensajeCorreo.Text.Trim().Length > 0) ? " y " : "") + "Correo Enviado";
                }

                iGeneral.Close();

                return blnBanderaCorreo;
            }
            catch (Exception err)
            {
                //mensaje("correoPago <br>intIdClienteContacto pasado a SeleccionarcClienteContactoPorId:" + intIdClienteContacto.ToString() + " " + strParametros, err);
                return false;
            }
        }
        protected void btnPagarFinal_Click(object sender, EventArgs e)
        {
            try
            {
                //btnPagarFinal.Visible = false;
                hdIV.Value = "";
                int intIdCliente = cGlobals.idUsuario;
                decimal dcTotal = decimal.Parse(hdToTalPagar.Value);
                //                string idMoneda = ddlCambioMoneda.SelectedValue.ToString();
                string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
                string dsIsoMoneda = selectedValueMoneda[1].ToString();
                string idMoneda = selectedValueMoneda[0].ToString();
                string strIdClienteCompetidor = "";
                bool respGuardaPago = false;
                int intRespIdVenta = int.Parse(Request.QueryString["refVenta"].ToString());
                hdIdVenta.Value = intRespIdVenta.ToString();
                string strFolios = "";
                string strIdVenta = "";
                string[] strIdVentas = hdIV.Value.Split(',');
                int respActualizaCompetidor = 0;

                fnGuardaDatosTarjeta(intIdCliente, dcTotal, idMoneda);

                fnPagar(intIdCliente, dcTotal, idMoneda, intRespIdVenta);
               cRespuestaPagoCompetidor = getObjetoRespuestaPago();
               cCompetidorPagar.DtCompetidor = getDatatableCompetidor();
               if (cRespuestaPagoCompetidor.strGlobalCollectActivo != "1")
                {
                    
                    respGuardaPago = fnGuardarPago(intIdCliente, intRespIdVenta, dcTotal, idMoneda);
                    if (respGuardaPago)
                    {
                        try
                        {
                            // aqui voy a meter un try catch si cae en catch paso no termino de actualizar de un pago que si pasó
                           // int resultActualizar = actualizar(intIdCliente, dcTotal, idMoneda, intRespIdVenta);
                            int resultActualizar = 1;
                            int intIdClienteContactoEvento = 0;
                            int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                            // si el resultado es mayor a cero quiere decir que actualizo
                            if (resultActualizar > 0)
                            {
                                respActualizaCompetidor = 1; // ActualizarCompetidor(intIdCliente, dcTotal, idMoneda, intRespIdVenta);
                                DataTable dtCompetidor = getDatatableCompetidor();
                                if (respActualizaCompetidor > 0)
                                {
                                    // si todo pasó bien ahora enviamos correo al competidor
                                    // El idVenta que se envía es el primero ya que es el que 
                                    // es el de la inscripción
                                    //protected bool correoPago(string strIdVenta, string strFolios, string strIdioma, string strCorreoCliente, DataTable dtCompedidor)
                                    //string strIdClienteContactoCompetidorEvento = dtCompetidor.Rows[0]["idClienteContactoEvento"].ToString();
                                    //wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                                    //wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                                    //clienteContactoEventoClient.Open();
                                    //cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(strIdClienteContactoCompetidorEvento), getDsSession());


                                    bool respCorreoConfirmacion = correoPago(intRespIdVenta.ToString(), "", "1", hdCorreoCompetidor.Value);
                                    //cParticipante.dtCompedidor = null;
                                    //clienteContactoEventoClient.Close();
                                    Session["dtCompetidorPago" + cnIdContactoEvento.Value] = null;
                                    cCompetidorPagar.DtCompetidor = null;
                                    string idVentasDetalles = intRespIdVenta.ToString();
                                    
                                    //Actualizar el registro a Pagado


                                        wsBusinessRules.BusinessRulesServicesClient ruleOper = new wsBusinessRules.BusinessRulesServicesClient();
                                        Dictionary<string, string> dicret = new Dictionary<string, string>();
                                        dicret.Clear();
                                        dicret.Add("@idVenta", intRespIdVenta.ToString());
                                        DataSet dsConfiguracionPagosGC = ruleOper.ExecuteRule("SpUPDTriatlonDatosPendienteCobro", dicret, getDsSession());
                
                                        dicret.Clear();
                                        ruleOper.Close();
                                        if (dsConfiguracionPagosGC.Tables[0].Rows[0]["RetValue"].ToString() == "1")
                                        {

                                            Response.Redirect("respPago.aspx?token=" + idVentasDetalles);
                                        }
                                }
                                else
                                {
                                    lblRespuestaPago.Text = "Pago realizado con cargo a su tarjeta, pero ocurrió error al actualizar al competidor. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                                }
                            }
                            else
                            {
                                lblRespuestaPago.Text = "Pago realizado, pero ocurrió error en conexión, favor de contactarnos. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                            }
                        }// fin del try
                        catch (Exception ex)
                        {
                            lblRespuestaPago.Text = "Pago realizado con cargo a su tarjeta, pero ocurrió error al actualizar al competidor. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                            btnPagarFinal.Attributes.Add("readonly", "readonly");
                            btnPagarFinal.Attributes.Add("disabled", "disabled");

                            string msg = ex.Message.ToString();
                            string strginnermessage = "";
                            string strCuerpoCorreo = msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                            string strExcepcion = "";
                            if (ex.ToString().Length > 799)
                                strExcepcion = ex.ToString().Substring(0, 800);
                            else
                                strExcepcion = ex.ToString();
                            strCuerpoCorreo += strExcepcion;
                           // MensajeAlert(strCuerpoCorreo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblRespuestaPago.Text = ex.Message.ToString();
            }
        }

        protected void fnPagar(int intIdCliente, decimal dcTotal, string idMoneda, int idVenta)
        {
            int intFechaHoy = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
            string strDcTotalDolar = "0";
            decimal dcTotalPesos = 0;
            int intIdTipoMonedaPagar = 0;
            string strIdTipoMoneda = "";
            string strFolios = hddsClaveVenta.Value;
            string strIdVenta = hdIdVenta.Value;
            string[] strIdVentas = hdIV.Value.Split(',');

            string strCanalVentaId = (ConfigurationManager.AppSettings["idCanalVenta"].ToString());
            string strIdEstatusPago = "";
            /*
             * wsTransactions.IkVentaClient vc = new wsTransactions.IkVentaClient();
            vc.Open();
            
            for (int l = 0; l < (strIdVentas.Length - 1); l++)
            {
                wsTransactions.kVenta laVenta = vc.SeleccionarkVentaPorIdVenta(int.Parse(strIdVentas[l].ToString()), getDsSession());
                strFolios += laVenta.dsClaveVenta + ",";
                strIdVenta += laVenta.idVenta.ToString() + "|";
            }

            strFolios = strFolios.Remove(strFolios.Length - 1);
            strIdVenta = strIdVenta.Remove(strIdVenta.Length - 1);

            vc.Close();
             */
            wscMoneda.IcMonedaClient IcMoneda = new wscMoneda.IcMonedaClient();
            IcMoneda.Open();

            wscMoneda.ListacMonedas lstMonedas = IcMoneda.SeleccionarcMonedas(getDsSession());
            IcMoneda.Close();

            decimal dcTipoCambioPesosDolar = 0;
            decimal dcTipoCambioMonedaPagar = 0;

            for (int cntMoneda = 0; cntMoneda < lstMonedas.Count; cntMoneda++)
            {
                if ((int)lstMonedas[cntMoneda].idMoneda == Int32.Parse(idMoneda))
                {
                    strIdTipoMoneda = lstMonedas[cntMoneda].idTipoMoneda.ToString();
                    dcTipoCambioMonedaPagar = (decimal)lstMonedas[cntMoneda].noConversion;
                }
            }

            // tengo que averiguar el valor de dolar actual
            DataSet listValorPesosDolar = null;
            wsBusinessRules.BusinessRulesServicesClient ruleOper2 = new wsBusinessRules.BusinessRulesServicesClient();
            ruleOper2.Open();
            Dictionary<string, string> dicret2 = new Dictionary<string, string>();
            dicret2.Clear();
            dicret2.Add("@TIPOCONSULTA", "TIPOCAMBIO");
            listValorPesosDolar = ruleOper2.ExecuteRule("spGETcMoneda", dicret2, getDsSession());
            dicret2.Clear();
            ruleOper2.Close();
            dcTipoCambioPesosDolar = decimal.Parse(listValorPesosDolar.Tables[0].Rows[0]["noConversion"].ToString().Trim());
            // si el id tipo de moneda es distinto de 2 Pesos, tenemos que pasar el total a pesos, por los casos de amex
            dcTotalPesos = dcTotal;
            if (strIdTipoMoneda != "2")
            {
                dcTotalPesos = Math.Round(((dcTotal * dcTipoCambioPesosDolar) / dcTipoCambioMonedaPagar), 2, MidpointRounding.ToEven);
            }
            //cRespuestaPagoCompetidor
            wsTransactions.IkCajeroClient IkCajero = new wsTransactions.IkCajeroClient();
            IkCajero.Open();
            //cResultadoGuardadoPago.strGlobalCollectActivo = "0";
            cRespuestaPagoCompetidor.strGlobalCollectActivo = "0";
            #region global
            if (hdCnGlobalCollect.Value == "1")
            {
                if (hdCnGlobalCollectI.Value == "1")
                {
                    strCanalVentaId = strCanalVentaId.ToString() + "_INT"; //"11_INT";
                }
                cRespuestaPagoCompetidor.strGlobalCollectActivo = "1";
                wscTipoMoneda.IcTipoMonedaClient IcTipoMoneda = new wscTipoMoneda.IcTipoMonedaClient();
                IcTipoMoneda.Open();
                wscTipoMoneda.cTipoMoneda cTipoMoneda = IcTipoMoneda.SeleccionarcTipoMonedaPorId(int.Parse(strIdTipoMoneda), getDsSession());
                IcTipoMoneda.Close();

                wscPais.IcPaisClient IcPaisClient = new wscPais.IcPaisClient();
                IcPaisClient.Open();
                wscPais.cPais cPais = IcPaisClient.SeleccionarcPaisPorId(Int32.Parse("484"), getDsSession());
                IcPaisClient.Close();

                string strDcTotal = dcTotal.ToString().Replace(".", "");
                wsTransactions.GlobalCollect globalCollect = new wsTransactions.GlobalCollect();
                wsTransactions.GlobalCollectResponse globalCollectResponse = new wsTransactions.GlobalCollectResponse();

                wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                icConfiguracion.Open();
                wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionIPAddressGlobalCollect = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-XCRM-ClientIPAddressGlobalCollect", getDsSession());
                icConfiguracion.Close();

                globalCollect.Apellido = txtNombreTH.Text.Trim();
                globalCollect.Nombre = txtNombreTH.Text.Trim();
                globalCollect.CanalVentaId = strCanalVentaId;
                globalCollect.ClienteId = intIdCliente.ToString();
                globalCollect.CodigoIdioma = "es";
                globalCollect.CodigoMoneda = cTipoMoneda.dsIso;  // "USD";  "MXN"
                globalCollect.CodigoPais = cPais.dsIso2; // "US";
                globalCollect.CorreoElectronico = hdCorreoCompetidor.Value; //"faraujo@experienciasxcaret.com.mx"; // del cliente
                globalCollect.Monto = strDcTotal;
                globalCollect.NumeroPagos = 1;
                globalCollect.ReferenciaComercio = strFolios; // Folio Venta
                globalCollect.IdVenta = strIdVenta; // string con pipes
                globalCollect.DeviceType = "7"; // 7-Desktop
                globalCollect.CustomerIPAddress = cConfiguracionIPAddressGlobalCollect.dsValor.Trim(); //Utils.GetIpAddress();
                globalCollect.DireccionIP = utils.GetIpAddress();

                string strAbsoluteUri = HttpContext.Current.Request.Url.AbsoluteUri;
                string strHost = HttpContext.Current.Request.Url.Host;
                string[] strspliturl = strAbsoluteUri.Split('/');
                strAbsoluteUri = strAbsoluteUri.Replace(strspliturl[strspliturl.Length - 1], "");
                strAbsoluteUri += "respuestaGlobal.aspx";

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]) && strHost != "localhost")
                {
                    strAbsoluteUri = strAbsoluteUri.Replace("http", "https");
                }
                globalCollect.UrlRetorno = strAbsoluteUri;

                //lblMensajeError.Text = strAbsoluteUri;

                // TipoProducto 2=american 1=visa 3=master

                if (ddlBanco.SelectedItem.Value == "36")// Amex
                {
                    globalCollect.TipoProducto = "2";
                }
                else
                {
                    //cmbTipoTarjeta.SelectedValue 1:Visa 2:mastercard

                    if (ddlFormaPago.SelectedValue == "27" || ddlFormaPago.SelectedValue == "29") // Visa
                        globalCollect.TipoProducto = "1";
                    if (ddlFormaPago.SelectedValue == "28" || ddlFormaPago.SelectedValue == "30") // mastercard
                        globalCollect.TipoProducto = "3";
                }

                try
                {
                    globalCollectResponse = IkCajero.GlobalCollect_Pago(globalCollect, getDsSession());
                }
                catch (Exception err)
                {
                    //mensaje("GuardaPagoTarjeta GlobalCollect_Pago<br>cmbPromosionMSI:" + cmbPromosionMSI.SelectedItem.Value + "<br>hdIdPais:" + hdIdPais.Value, err);
                }

                // si todo bien hasta aqui
                if (globalCollectResponse.CodigoError == "0")
                {
                    //btnGuardarPago.Visible = false;
                    //btnGlobalCollect.Visible = true;
                    pnGlobalCollect.Visible = true;
                    upGC.Update();

                    lblRespuestaPagoGlobalCollect.Text = globalCollectResponse.OrderId;
                    hddIdOrdenGlobal.Value = globalCollectResponse.OrderId;
                    // validando que Global regrese URL
                    if (globalCollectResponse.URL.Trim().Length > 0)
                    {
                        ifrGlobalCollect.Attributes["src"] = globalCollectResponse.URL;
                        upGC.Update();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "myScript", "showGlobal();", true);
                        //UpdatePanelGlobalCollect.Update();
                    }
                    else
                    {
                        //lblMensajeError.Text = "Global Collect no regreso URL de cobro";
                        //mensaje(lblMensajeError.Text, null);
                    }
                    //cbGlobalCollect.JSProperties["cp_pagoGuargadoGlobalCollect"] = "0"; // bandera pago Globalcollect
                }
                else
                {
                    //lblRespuestaPagoGlobalCollect.Text = globalCollectResponse.CodigoError + " " + globalCollectResponse.DescripcionError;
                    //UpdatePanelGlobalCollect.Update();
                }
            }
            #endregion
            else
            {
                #region BANCOS
                switch (ddlBanco.SelectedItem.Value)
                {
                    case "35":
                        // Amex
                        // SIEMPRE EN PESOS

                        strDcTotalDolar = dcTotalPesos.ToString("N2");
                        strDcTotalDolar = strDcTotalDolar.Replace(",", "");
                        //strDcTotalDolar = strDcTotalDolar.Replace(".", "");
                        wsTransactions.Amex amex = new wsTransactions.Amex();
                        wsTransactions.AmexResponse amexresponse = new wsTransactions.AmexResponse();

                        amex.anioExpiracion = ddlAnioExpira.SelectedItem.Value.Substring(ddlAnioExpira.SelectedItem.Value.Length - 2);
                        amex.mesExpiracion = ddlMesExpira.SelectedItem.Value;

                        amex.codigoSeguridad = txtCVV.Text;
                        /* * * * * * * * * * * * * * * * * * MONTO A PAGAR - total * * * * * * * * * * * * */
                        //amex.importe = "3000.00";
                        amex.importe = strDcTotalDolar;
                        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

                        amex.ip = "192.168.24.19";
                        amex.moneda = "MXN";
                        amex.numeroTarjeta = txtNoTarjeta.Text;
                        amex.ordenId = strFolios; // Folio Venta
                        amex.referencia = strFolios; // Folio Venta
                        amex.referenciaOrden = strFolios; // Folio Venta
                        amex.transaccionId = strFolios; // Folio Venta
                        //si no es a MSI estos dos campos no se llenas ó se pasan en nulo.
                        //if (Int32.Parse(cmbPromosionMSI.SelectedItem.Value) > 1)
                        //{
                        // amex.planPagos = "AMEX_PLANN";
                        // amex.numeroPagos = 1;
                        //}

                        try
                        {
                            //checaCreaSessionBD();
                            //amexresponse = IkCajero.PagoAMEX(amex, strDsSession);
                            amexresponse = IkCajero.PagoAMEXM(amex, getDsSession(), "9351670303");

                        }
                        catch (Exception err)
                        {
                            strIdEstatusPago = "1" /*Declinado*/;
                            string strParametros = "GuardaPagoTarjeta - Amex 1 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                            //mensaje(strParametros, err);
                        }

                        lblRespuestaPago.Text = ((amexresponse.gatewayCode != null) ? amexresponse.gatewayCode : "") + " " + ((amexresponse.authorizationCode != null) ? amexresponse.authorizationCode : "") + " " + ((amexresponse.errorCode != null) ? amexresponse.errorCode : "") + " " + ((amexresponse.errorMessage != null) ? amexresponse.errorMessage : "");

                        strIdEstatusPago = "1" /*Declinado*/;

                        if (amexresponse.gatewayCode.ToString().Trim().Length > 0)
                        {
                            if (amexresponse.gatewayCode.ToString().Trim() == "APPROVED")
                            {
                                strIdEstatusPago = "2" /*Aceptado*/;
                            }
                        }
                        cRespuestaPagoCompetidor.strIdEstatusPago = strIdEstatusPago;
                        cRespuestaPagoCompetidor.strAfiliacion = (amexresponse.dsAfiliacion != null) ? amexresponse.dsAfiliacion : "";
                        cRespuestaPagoCompetidor.strDsRespuesta = (amexresponse.gatewayCode != null) ? amexresponse.gatewayCode.ToString() : "";
                        cRespuestaPagoCompetidor.strDsJustificacion = (amexresponse.gatewayCode != null) ? amexresponse.gatewayCode.ToString() : "";
                        cRespuestaPagoCompetidor.strDsReferencia = (amexresponse.gatewayCode != null) ? amexresponse.gatewayCode.ToString() : "";
                        cRespuestaPagoCompetidor.strDsTransaccion = (amexresponse.authorizationCode != null) ? amexresponse.authorizationCode.ToString() : "";
                        cRespuestaPagoCompetidor.strDsCorrelacion = ((amexresponse.recipient != null) ? amexresponse.recipient.ToString().Trim() : "") + "-" + ((amexresponse.terminal != null) ? amexresponse.terminal.ToString().Trim() : "");
                        cRespuestaPagoCompetidor.strIdBancoReceptor = "35";
                        break;
                    case "5":
                        // Bancomer
                        //IkCajeroClient cajero = new IkCajeroClient();
                        string strDsRespuesta = "";
                        string strDsJustificacion = "";
                        string strDsTransaccion = "";
                        string strDsReferencia = "";
                        string strDsCorrelacion = "";
                        string srtdsAfiliacion = "";
                        try
                        {

                            //wskTransactiones.IkCajeroClient cajero = new wskTransactiones.IkCajeroClient();
                            // strDcTotal agregadó el 03/08/2015 segun hay que enviar siempre los 2 decimales sin el punto, aunque sea .00
                            string strDcTotal = dcTotal.ToString("N2");
                            strDcTotal = strDcTotal.Replace(",", "");
                            //strDcTotal = strDcTotal.Replace(".", "");
                            wsTransactions.Adquira_EMV adquiraemv = new wsTransactions.Adquira_EMV();
                            wsTransactions.Adquira_EMVResponse resp = new wsTransactions.Adquira_EMVResponse();
                            adquiraemv.s_transm = DateTime.Now.ToString("yyyyMMddHHmmss"); // aaaammddhhmmss
                            adquiraemv.c_referencia = strFolios; // Folio Venta
                            adquiraemv.val_1 = 12; //unidad de negocio - 12= computadora
                            adquiraemv.clave_entidad = 10778;
                            adquiraemv.t_servicio = "1121"; //cc

                            /* * * * * * * * * * * * * * * * * * MONTO A PAGAR - total * * * * * * * * * * * * * * * * */
                            if (strIdTipoMoneda != "2") // diferente de Pesos
                            {
                                adquiraemv.c_cur = 1; // 0-PESOS, 1-DOLARES 
                                adquiraemv.t_importe = strDcTotal.ToString();
                            }
                            else //pesos
                            {
                                adquiraemv.c_cur = 0; //?? 0-PESOS, 1-DOLARES
                                adquiraemv.t_importe = strDcTotal.ToString();
                            }
                            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

                            adquiraemv.tarjetahabiente = txtNombreTH.Text.Trim();// +" " + txtApellidoTH.Text.Trim(); // nombre tarjetahabiente
                            adquiraemv.val_3 = txtNoTarjeta.Text.Trim(); //No de tarjeta
                            adquiraemv.val_4 = ddlAnioExpira.SelectedItem.Value.Substring(ddlAnioExpira.SelectedItem.Value.Length - 2) + ddlMesExpira.SelectedItem.Value; // fecha vencimiento yymm
                            adquiraemv.val_5 = txtCVV.Text.Trim(); // codigo seguridad
                            adquiraemv.val_6 = adquiraemv.s_transm + adquiraemv.c_referencia + adquiraemv.t_importe + txtCVV.Text;
                            adquiraemv.val_11 = ""; // correo electronico Cliente
                            adquiraemv.val_12 = ""; // Telefono Cliente
                            //adquiraemv.clave_entidad = int.Parse(clave_entidad.Text) // 1730 ? 
                            adquiraemv.val_16 = rblTipoTarjeta.SelectedValue; //"1"; //? 1:Visa 2:mastercard  
                            adquiraemv.val_17 = "0"; // 0: digitada 
                            adquiraemv.val_18 = ""; // vacio

                            adquiraemv.val_19 = "0"; //si tiene msi 1 sino 0
                            adquiraemv.val_20 = "0"; // no. de meses
                            /* if (Int32.Parse(cmbPromosionMSI.SelectedItem.Value) > 1)
                             {
                                 adquiraemv.val_19 = "1"; //si tiene msi 1 sino 0
                                 adquiraemv.val_20 = cmbPromosionMSI.SelectedItem.Value; // no. de meses
                             }*/

                            adquiraemv.email_admin = "faraujo@experienciasxcaret.com.mx";
                            adquiraemv.accion = "PAGO";
                            adquiraemv.nu_afiliacion = "";
                            adquiraemv.nu_plataforma = "7";

                            try
                            {
                                //resp = cajero.PagoAdquiraEMV(adquiraemv, strDsSession, Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"])); // true:produccion, false:pruebas
                                resp = IkCajero.PagoAdquiraEMV(adquiraemv, getDsSession(), Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"])); // true:produccion, false:pruebas
                            }
                            catch (Exception err)
                            {
                                strIdEstatusPago = "1" /*Declinado*/;
                                string strParametros = "GuardaPagoTarjeta - Bancomer 1 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + strDcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                                //mensaje(strParametros, err);

                            }

                            lblRespuestaPago.Text = ((resp.mensaje != null) ? resp.mensaje : "") + " " + ((resp.autorizacion != null) ? resp.autorizacion : "") + " - " + ((resp.status != null) ? resp.status : "");

                            string strIdBancoReceptor = ddlBanco.SelectedItem.Value;

                            strIdEstatusPago = "1" /*Declinado*/;
                            if (resp != null)
                            {
                                //    MessageBox.Show("Autorizacion : " + resp.autorizacion + " Mensaje :" + resp.mensaje + " Status :" + resp.status + " Imprimir: " + resp.imprimir + " Fecha : " + resp.fecha + " Hora:" + resp.hora);
                                if (resp.mensaje == "APROBADA")
                                {
                                    strIdEstatusPago = "2" /*Aceptado*/;
                                    if (resp.autorizacion != null && resp.autorizacion == "000000")
                                    {
                                        strIdEstatusPago = "1" /*Declinado*/;
                                        lblRespuestaPago.Text += " - No se cobro hasta su autorización";
                                    }
                                }
                            }

                            strDsRespuesta = (resp.mensaje != null) ? resp.mensaje.ToString() : "";
                            strDsJustificacion = (resp.mensaje != null) ? resp.mensaje.ToString() : "";
                            strDsTransaccion = (resp.autorizacion != null) ? resp.autorizacion.ToString() : "";
                            strDsReferencia = (resp.mensaje != null) ? resp.mensaje.ToString() : "";
                            strDsCorrelacion = ((resp.autorizacion != null) ? resp.autorizacion.ToString() : "");
                            srtdsAfiliacion = ((resp.dsAfiliacion != null) ? resp.dsAfiliacion.ToString() : "");

                            cRespuestaPagoCompetidor.strIdEstatusPago = strIdEstatusPago;
                            cRespuestaPagoCompetidor.strAfiliacion = srtdsAfiliacion;
                            cRespuestaPagoCompetidor.strDsRespuesta = strDsRespuesta;
                            cRespuestaPagoCompetidor.strDsJustificacion = strDsJustificacion;
                            cRespuestaPagoCompetidor.strDsReferencia = strDsReferencia;
                            cRespuestaPagoCompetidor.strDsTransaccion = strDsTransaccion;
                            cRespuestaPagoCompetidor.strDsCorrelacion = strDsCorrelacion;
                            cRespuestaPagoCompetidor.strIdBancoReceptor = "5";
                        }
                        catch (Exception err)
                        {
                            lblRespuestaPago.Text = "";
                            strIdEstatusPago = "1";
                            strDsJustificacion = "Revisar";
                            strDsJustificacion = err.Message;
                            string strParametros = "GuardaPagoTarjeta - Bancomer 2 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                            //mensaje(strParametros, err);
                        }
                        break;
                    default:
                        try
                        {
                            string strDcTotal = dcTotal.ToString("N2");
                            strDcTotal = strDcTotal.Replace(",", "");
                            //string strDcTotal = dcTotal.ToString().Replace(".", "");
                            strDcTotal = strDcTotal.Replace(".", "");
                            strDcTotalDolar = strDcTotal.Replace(".", "");
                            wsTransactions.CSVPC2Party C2Party = new wsTransactions.CSVPC2Party();
                            wsTransactions.CSVPC2PartyResponse C2PartyRespo = new wsTransactions.CSVPC2PartyResponse();


                            /* * * * * * * * * * * * * * * * * * MONTO A PAGAR - total * * * * * * * * * * * * * * * * */
                            //adquiraemv.t_importe = "300000"; //txtMontoTemporal.Text; // importe 10000
                            string strCurrency = "MXN";

                            if (strIdTipoMoneda != "2") // diferente de Pesos
                            {
                                strCurrency = "USD";
                                C2Party.vpc_Amount = strDcTotalDolar;
                            }
                            else //pesos
                            {
                                strCurrency = "MXN";
                                C2Party.vpc_Amount = strDcTotal;
                            }
                            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
                            ///* * * * * * * * * * * * * * * * * * MONTO A PAGAR - total * * * * * * * * * * * * * * * * */
                            //C2Party.vpc_Amount = strDcTotal;
                            ///* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

                            C2Party.vpc_CardExp = ddlAnioExpira.SelectedItem.Value.Substring(ddlAnioExpira.SelectedItem.Value.Length - 2) + ddlMesExpira.SelectedItem.Value;
                            C2Party.vpc_CardNum = txtNoTarjeta.Text;
                            C2Party.vpc_MerchTxnRef = strFolios; // Folio Venta
                            C2Party.vpc_OrderInfo = C2Party.vpc_MerchTxnRef; //txtNoReferencia.Text; // cualquier texto
                            C2Party.vpc_Version = "1";
                            C2Party.vpc_CardSecurityCode = txtCVV.Text;
                            C2Party.vpc_TicketNo = strFolios; // Folio Venta
                            C2Party.vpc_Type = "bmx";
                            //C2Party.vpc_Type = cmbTipoTarjeta.SelectedItem.Text;
                            C2Party.vpc_NumPayments = "1";

                            try
                            {
                                //C2PartyRespo = IkCajero.PagoCSVPC2Party(C2Party, strDsSession);
                                C2PartyRespo = IkCajero.PagoBanamexM(C2Party, getDsSession(), "4010641", strCurrency);
                            }
                            catch (Exception err)
                            {
                                strIdEstatusPago = "1";
                                string strParametros = "GuardaPagoTarjeta - Banamex 1 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                                //mensaje(strParametros, err);
                            }

                            lblRespuestaPago.Text = ((C2PartyRespo.vpc_TxnResponseCode != null) ? C2PartyRespo.vpc_TxnResponseCode.ToString() : "") + " " + ((C2PartyRespo.vpc_AuthorizeId != null) ? C2PartyRespo.vpc_AuthorizeId.ToString() : "") + " - " + ((C2PartyRespo.vpc_Message != null) ? C2PartyRespo.vpc_Message.ToString() : "") + " - " + ((C2PartyRespo.vpc_TxnResponseDescription != null) ? C2PartyRespo.vpc_TxnResponseDescription.ToString() : "");

                            string strIdBancoReceptor = "108"; // Banamex
                            string strDsAfiliacionBanamex = (C2PartyRespo.dsAfiliacion != null) ? C2PartyRespo.dsAfiliacion.ToString() : "";

                            strIdEstatusPago = "1";
                            if (C2PartyRespo != null)
                            {
                                strIdEstatusPago = (C2PartyRespo.vpc_TxnResponseCode.ToString() == "0") ? "2" /*Aceptado*/: "1" /*Declinado*/;

                                if (strIdEstatusPago == "2" && C2PartyRespo.vpc_AuthorizeId != null && C2PartyRespo.vpc_AuthorizeId == "000000")
                                {
                                    strIdEstatusPago = "1";/*Declinado*/;
                                    lblRespuestaPago.Text += " - No se cobro hasta su autorización";
                                }
                            }

                            strDsRespuesta = (C2PartyRespo.vpc_AVSResultDescription != null) ? C2PartyRespo.vpc_AVSResultDescription.ToString() : "";
                            strDsJustificacion = (C2PartyRespo.vpc_AVSResultDescription != null) ? C2PartyRespo.vpc_AVSResultDescription.ToString() : "";
                            strDsTransaccion = (C2PartyRespo.vpc_AuthorizeId != null) ? C2PartyRespo.vpc_AuthorizeId.ToString() : "";
                            strDsReferencia = (C2PartyRespo.vpc_AVSResultDescription != null) ? C2PartyRespo.vpc_AVSResultDescription.ToString() : "";
                            strDsCorrelacion = ((C2PartyRespo.vpc_ReceiptNo != null) ? C2PartyRespo.vpc_ReceiptNo.ToString() : "") + "-" + ((C2PartyRespo.vpc_TransactionNo != null) ? C2PartyRespo.vpc_TransactionNo.ToString() : ""); //C2PartyRespo.vpc_AuthorizeId + "-" + C2PartyRespo.vpc_TransactionNo;

                            cRespuestaPagoCompetidor.strIdEstatusPago = strIdEstatusPago;
                            cRespuestaPagoCompetidor.strAfiliacion = strDsAfiliacionBanamex;
                            cRespuestaPagoCompetidor.strDsRespuesta = strDsRespuesta;
                            cRespuestaPagoCompetidor.strDsJustificacion = strDsJustificacion;
                            cRespuestaPagoCompetidor.strDsReferencia = strDsReferencia;
                            cRespuestaPagoCompetidor.strDsTransaccion = strDsTransaccion;
                            cRespuestaPagoCompetidor.strDsCorrelacion = strDsCorrelacion;
                            cRespuestaPagoCompetidor.strIdBancoReceptor = strIdBancoReceptor;

                        }
                        catch (Exception err)
                        {
                            lblRespuestaPago.Text = "";
                            strIdEstatusPago = "1";
                            strDsJustificacion = "Revisar";
                            strDsJustificacion = err.Message;
                            string strParametros = "GuardaPagoTarjeta - Banamex 2 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                            //mensaje(strParametros, err);
                        }
                        break;

                }
                #endregion Bancos
            }// end else

            /*if (ddlBanco.SelectedItem.Value != "35") // distinto del banco amex
            {
                cResultadoGuardadoPago.strIdBancoReceptor = "32";
            }*/
            /*switch (ddlBanco.SelectedItem.Value)
            {
                case "35":
                    // Amex
                    cRespuestaPagoCompetidor.strIdBancoReceptor = ddlBanco.SelectedItem.Value;
                    break;
                case "5":
                    // Bancomer
                    cRespuestaPagoCompetidor.strIdBancoReceptor = ddlBanco.SelectedItem.Value;
                    break;
                case "108":
                    // Banamex
                    cRespuestaPagoCompetidor.strIdBancoReceptor = ddlBanco.SelectedItem.Value;
                    break;
                default:
                    // Otros
                    cRespuestaPagoCompetidor.strIdBancoReceptor = "32"; // Banorte
                    break;
            }*/
            Session["cRespuestaPagoCobro" + cnIdContactoEvento.Value] = cRespuestaPagoCompetidor;
        }
        protected void fnGuardaDatosTarjeta(int intIdCliente, decimal dcTotal, string idMoneda, string strNumeroTarjeta = "")
        {
            string strIdClienteTarjeta;
            string idTipoTarjeta = rblTipoTarjeta.SelectedValue.ToString();
            if (rblTipoTarjeta.SelectedValue.ToString() == "2")
                idTipoTarjeta = "1";
            else if (rblTipoTarjeta.SelectedValue.ToString() == "1")
                idTipoTarjeta = "2";
            // Se guarda datos en cliente tarjeta
            wscClienteTarjeta.IcClienteTarjetaClient IcClienteTarjeta = new wscClienteTarjeta.IcClienteTarjetaClient();
            IcClienteTarjeta.Open();
            wscClienteTarjeta.cClienteTarjeta cClienteTarj = new wscClienteTarjeta.cClienteTarjeta();
            cClienteTarj.idCliente = cGlobals.idCliente;
            cClienteTarj.idEstatusCliente = 1; // Activo
            cClienteTarj.dsTarjeta = (strNumeroTarjeta == "") ? txtNoTarjeta.Text.Trim() : strNumeroTarjeta;
            cClienteTarj.idBanco = Int32.Parse(ddlBanco.SelectedItem.Value);
            cClienteTarj.idTipoTarjeta = int.Parse(idTipoTarjeta);
            cClienteTarj.dsNombre = txtNombreTH.Text.Trim().ToUpper(); //+ " " + txtApellidoTH.Text.Trim().ToUpper();
            cClienteTarj.feAlta = DateTime.Now;
            cClienteTarj.idClienteUsuarioAlta = (cGlobals.idUsuario);
            cClienteTarj.idClienteTarjeta = 0;
            cClienteTarj.cnActivo = true;

            strIdClienteTarjeta = IcClienteTarjeta.InsertarcClienteTarjeta(cClienteTarj, getDsSession()).ToString();
            cRespuestaPagoCompetidor = getObjetoRespuestaPago();
            cRespuestaPagoCompetidor.strIdClienteTarjeta = strIdClienteTarjeta;
            Session["cRespuestaPagoCobro" + cnIdContactoEvento.Value] = cRespuestaPagoCompetidor;
            IcClienteTarjeta.Close();
        }
        protected void validaCamposGlobal(string strIdMoneda, string idBanco)
        {
            try
            {
                DataSet dsConfiguracionPagosGC;
                string strIdTipoMoneda = "";
                string strIdCanalVenta = ConfigurationManager.AppSettings["idCanalVenta"].ToString();
                //string strIdCanalVenta = "11";
                bool blBanderaGlobalCollect = false;
                bool blBanderaGlobalCollectInternationalLove = false;

                lblNumTarjeta.Visible = true;
                txtNoTarjeta.Visible = true;
                lblFechaExpira.Visible = true;
                //txtMes.Visible = true;
                //txtAnio.Visible = true;
                ddlAnioExpira.Visible = true;
                ddlMesExpira.Visible = true;
                lblCVV.Visible = true;


                wscMoneda.IcMonedaClient IcMoneda = new wscMoneda.IcMonedaClient();
                IcMoneda.Open();
                wscMoneda.ListacMonedas lstMonedas = IcMoneda.SeleccionarcMonedas(getDsSession());
                IcMoneda.Close();

                hdCnGlobalCollectI.Value = "0";
                hdCnGlobalCollect.Value = "0";

                for (int cntMoneda = 0; cntMoneda < lstMonedas.Count; cntMoneda++)
                {
                    if ((int)lstMonedas[cntMoneda].idMoneda == Int32.Parse(strIdMoneda))
                    {
                        strIdTipoMoneda = lstMonedas[cntMoneda].idTipoMoneda.ToString();
                    }
                }
                // Se consulta la tabla de condiciones para pagar con Global Collect
                wsBusinessRules.BusinessRulesServicesClient ruleOper = new wsBusinessRules.BusinessRulesServicesClient();
                Dictionary<string, string> dicret = new Dictionary<string, string>();
                dicret.Clear();
                //dicret.Add("@TIPOCONSULTA", "CANAL");
                dicret.Add("@TIPOCONSULTA", "VALIDA");
                dicret.Add("@idCanalVenta", strIdCanalVenta);
                dicret.Add("@idTipoMoneda", strIdTipoMoneda);
                dsConfiguracionPagosGC = ruleOper.ExecuteRule("spGETcConfiguracionPagosGC", dicret, getDsSession());
                Session["cConfiguracionPagosGC" + strIdTipoMoneda] = dsConfiguracionPagosGC;
                dicret.Clear();
                ruleOper.Close();
                if (dsConfiguracionPagosGC.Tables[0].Rows[0]["RetValue"].ToString() == "1")
                {
                    //string strPromosionMSI = (cmbPromosionMSI.SelectedItem.Value == "1") ? "0" : cmbPromosionMSI.SelectedItem.Value;
                    string strPromosionMSI = "0";
                    DataRow[] dtArrComponentes = dsConfiguracionPagosGC.Tables[0].Select("idBanco=" + idBanco + " and idTipoMoneda=" + strIdTipoMoneda + " and idCanalVenta=" + strIdCanalVenta + " and noPagosMSI=" + strPromosionMSI);

                    if (dtArrComponentes != null && dtArrComponentes.Length > 0)
                    {
                        blBanderaGlobalCollect = true;
                        hdCnGlobalCollect.Value = "1";
                        if (dtArrComponentes[0]["cnInternacional"].ToString() == "True")
                        {
                            blBanderaGlobalCollectInternationalLove = true;
                            hdCnGlobalCollectI.Value = "1";
                            //lblEtiGlobalCollect.Text = "Global Collect Internacional";
                        }
                    }
                }
                if (blBanderaGlobalCollect)
                {
                    lblNumTarjeta.Visible = false;
                    txtNoTarjeta.Visible = false;
                    lblFechaExpira.Visible = false;
                    // txtMes.Visible = false;
                    // txtAnio.Visible = false;
                    ddlAnioExpira.Visible = false;
                    ddlMesExpira.Visible = false;
                    lblCVV.Visible = false;
                    txtCVV.Visible = false;
                }
            }
            catch (Exception ex)
            {

            }
        }
        protected int getMaxAgrupador(DataTable dtCompetidor)
        {
            int maxAgrupador = 0;
            maxAgrupador = Convert.ToInt32(dtCompetidor.Compute("max(noAgrupadorVenta)", "cnModificacion = 0"));
            return maxAgrupador;
        }
    
       
        public void mensaje(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }

        public void mensajeyOcultarDiv(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessageAndHide('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }
        
        public void mostrarOcultarDiv(string divMostrar, string divOcultar, string divOcultar2)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>mostrarOcultarDiv('" + divMostrar + "','" + divOcultar + "','" + divOcultar2 + "');</script>", false);
        }
        public void alert(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + errMensaje + "');</script>", false);
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.dsSession;
        }               
    }
}