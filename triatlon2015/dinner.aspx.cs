﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace travesia
{
    public partial class dinner : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string loc = "";
                Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                loc = myUri.AbsoluteUri.ToString();
                loc = loc.Replace("dinner.aspx", "");
                loc += "generalRegistration.aspx";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script> window.location = '" + loc + "'</script>", false);
            }
        }
    }
}