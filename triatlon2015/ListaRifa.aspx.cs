﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxEditors;
using System.Configuration;
using travesia.wsBusinessRules;

namespace travesia
{
    public partial class ListaRifa : System.Web.UI.Page
    {
        DataSet result;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    int intAnio = 0;
                    var tabla = new System.Text.StringBuilder();
                    tabla.Append("<table rules='rows' class='auto-style1'>");
                    

                    utils.Login("usrTriatlon", "eXperiencias");

                    int.TryParse(Request.QueryString["anio"], out intAnio);

                    if (intAnio==0)
                    {
                        intAnio = DateTime.Now.Year;
                    }

                    if (intAnio != 0)
                    {
                        try
                        {


                            wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();
                            Dictionary<string, string> dicret = new Dictionary<string, string>();
                            dicret.Clear();
                            dicret.Add("@anio", intAnio.ToString());
                            DataSet datos = reglas.ExecuteRule("SpTriatlonListaParaImpresionSorteoRifa", dicret, getDsSession());
                            dicret.Clear();
                            reglas.Close();

                            if (datos.Tables[0].Rows.Count >= 1)
                            {
                                foreach (DataRow row in datos.Tables[0].Rows)
                                {
                                    tabla.Append("<tr class='trTable' style='border: solid #CCCCCC;border-width: 1px 0;'>" +
                                        "<td class='auto-style3'><strong class='auto-style2'>" + row[1].ToString().Trim().Remove(0,3) + " </strong></td>" +
                                        "<td class='auto-style5'>" + row[2].ToString() + "</td>" +
                                        "<td class='auto-style6'>" + row[3].ToString() + "</td>" +
                                        "<td class='auto-style6'>" + row[4].ToString() + "</td><td>");
                                    tabla.Append("<img src='"+ @"https://www.xperienciasxcaret.mx/ecom/thirdparties/barcode/image.php?code=" + row[1].ToString() + "&style=452&type=C128A&width=140&height=80&xres=1"+"' />");

                                    tabla.Append("</td></tr>");
                                    
                                }
                            }

                            tabla.Append( " </table>");

                            lblLista.Text = Server.HtmlDecode(Server.HtmlEncode(tabla.ToString()));

                            /*string filename = "ExportExcel.xls";
                            System.IO.StringWriter tw = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

                            //Get the HTML for the control.             
                            //Table1.RenderControl(hw);
                            //Write the HTML back to the browser.
                            //Response.ContentType = application/vnd.ms-excel;
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
                            this.EnableViewState = false;
                            Response.Write(lblLista.Text);
                            Response.End();*/

                            /*.IO.StreamWriter writer = new System.IO.StreamWriter("abc.xls", false);
                            writer.WriteLine("");
                            writer.WriteLine("");
                            writer.WriteLine("<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"");
                            writer.WriteLine(" xmlns:o=\"urn:schemas-microsoft-com:office:office\"");
                            writer.WriteLine(" xmlns:x=\"urn:schemas-microsoft-com:office:excel\"");
                            writer.WriteLine(" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"");
                            writer.WriteLine(" xmlns:html=\"http://www.w3.org/TR/REC-html40/\">");
                            writer.WriteLine(" <DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">");
                            writer.WriteLine("  <Author>Automated Report Generator Example</Author>");
                            writer.WriteLine(string.Format("  <Created>{0}T{1}Z</Created>", DateTime.Now.ToString("yyyy-mm-dd"), DateTime.Now.ToString("HH:MM:SS")));
                            writer.WriteLine("  <Company>51aspx.com</Company>");
                            writer.WriteLine("  <Version>11.6408</Version>");
                            writer.WriteLine(" </DocumentProperties>");
                            writer.WriteLine(" <ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">");
                            writer.WriteLine("  <WindowHeight>8955</WindowHeight>");
                            writer.WriteLine("  <WindowWidth>11355</WindowWidth>");
                            writer.WriteLine("  <WindowTopX>480</WindowTopX>");
                            writer.WriteLine("  <WindowTopY>15</WindowTopY>");
                            writer.WriteLine("  <ProtectStructure>False</ProtectStructure>");
                            writer.WriteLine("  <ProtectWindows>False</ProtectWindows>");
                            writer.WriteLine(" </ExcelWorkbook>");
                            writer.WriteLine(" <Styles>");
                            writer.WriteLine("  <Style ss:ID=\"Default\" ss:Name=\"Normal\">");
                            writer.WriteLine("   <Alignment ss:Vertical=\"Bottom\"/>");
                            writer.WriteLine("   <Borders/>");
                            writer.WriteLine("   <Font/>");
                            writer.WriteLine("   <Interior/>");
                            writer.WriteLine("   <Protection/>");
                            writer.WriteLine("  </Style>");
                            writer.WriteLine("  <Style ss:ID=\"s21\">");
                            writer.WriteLine("   <Alignment ss:Vertical=\"Bottom\" ss:WrapText=\"1\"/>");
                            writer.WriteLine("  </Style>");
                            writer.WriteLine(" </Styles>");
                            writer.WriteLine(" <Worksheet ss:Name=\"MyReport\">");
                            writer.WriteLine(string.Format("  <Table ss:ExpandedColumnCount=\"{0}\" ss:ExpandedRowCount=\"{1}\" x:FullColumns=\"1\"", "cols", "rows"));
                            writer.WriteLine("   x:FullRows=\"1\">");

                            //generate title
                            //writer.WriteLine("<Row>");
                            /*foreach (DataColumn eachCloumn in dsBook.Tables[0].Columns)
                            {
                                writer.Write("<Cell ss:StyleID=\"s21\"><Data ss:Type=\"String\">");
                                writer.Write(eachCloumn.ColumnName.ToString());
                                writer.WriteLine("</Data></Cell>");
                            }*/
                            //writer.WriteLine("</Row>");

                            //generate data
                            /*foreach (DataRow eachRow in dsBook.Tables[0].Rows)
                            {
                                writer.WriteLine("<Row>");
                                for (int currentRow = 0; currentRow != cols; currentRow++)
                                {
                                    writer.Write("<Cell ss:StyleID=\"s21\"><Data ss:Type=\"String\">");
                                    writer.Write(eachRow[currentRow].ToString());
                                    writer.WriteLine("</Data></Cell>");
                                }
                                writer.WriteLine("</Row>");
                            }*/
                            /*writer.WriteLine(lblLista.Text);
                            writer.WriteLine("  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">");
                            writer.WriteLine("   <Selected/>");
                            writer.WriteLine("   <Panes>");
                            writer.WriteLine("    <Pane>");
                            writer.WriteLine("     <Number>3</Number>");
                            writer.WriteLine("     <ActiveRow>1</ActiveRow>");
                            writer.WriteLine("    </Pane>");
                            writer.WriteLine("   </Panes>");
                            writer.WriteLine("   <ProtectObjects>False</ProtectObjects>");
                            writer.WriteLine("   <ProtectScenarios>False</ProtectScenarios>");
                            writer.WriteLine("  </WorksheetOptions>");
                            writer.WriteLine(" </Worksheet>");
                            writer.WriteLine(" <Worksheet ss:Name=\"Sheet2\">");
                            writer.WriteLine("  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">");
                            writer.WriteLine("   <ProtectObjects>False</ProtectObjects>");
                            writer.WriteLine("   <ProtectScenarios>False</ProtectScenarios>");
                            writer.WriteLine("  </WorksheetOptions>");
                            writer.WriteLine(" </Worksheet>");
                            writer.WriteLine(" <Worksheet ss:Name=\"Sheet3\">");
                            writer.WriteLine("  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">");
                            writer.WriteLine("   <ProtectObjects>False</ProtectObjects>");
                            writer.WriteLine("   <ProtectScenarios>False</ProtectScenarios>");
                            writer.WriteLine("  </WorksheetOptions>");
                            writer.WriteLine(" </Worksheet>");
                            writer.WriteLine("</Workbook>");
                            writer.Close();
                            Response.Write("<script language=\"javascript\">" + "alert('" + "convert completed!')" + "</script>");*/
                        }
                        catch (Exception ex)
                        {
                            mensaje("Error en el numero de referencia ** no existe ó ya esta pagado ***", "Ha ocurrido un error");
                        }
                    }
                    else
                    {
                        mensaje("Error en el numero de referencia ** no existe ***", "Ha ocurrido un error");
                    }

                }


            }
            catch (Exception ex)
            {
                mensaje("Problema en carga de pagina " + ex.Message, "Ha ocurrido un error");
            }
        }

        public void mensaje(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }
        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.dsSession;
        }               
    }
}