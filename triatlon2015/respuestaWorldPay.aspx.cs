﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace travesia
{
    public partial class respuestaWorldPay : System.Web.UI.Page
    {
        String strDsSession = "";
        String strRespuesta = "";
        String strMensaje = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            String strDsSession = "";

            if (Session["strDsSession"] == null)
            {
                if (Request.QueryString["S"] == null)
                {
                    checaCreaSessionBD();
                }
                else
                {
                    strDsSession = Request.QueryString["S"];
                }

            }
            else
            {
                strDsSession = Session["strDsSession"].ToString();
                ViewState["strDsSession"] = strDsSession;
            }

            if (Request.QueryString["respuesta"] != null)
            {
                strRespuesta = Request.QueryString["respuesta"];
            }


            if (Request.QueryString["mensaje"] != null)
            {
                strMensaje = Request.QueryString["mensaje"];
            }

            //if (strRespuesta == "APROBADA")
            //{
            lblRespuesta.Text = strRespuesta;
            lblMensaje.Text = strMensaje;
        }

        public void checaCreaSessionBD()
        {
            wsSecurity.IsSecurityClient seguridad = new wsSecurity.IsSecurityClient();
            wsSecurity.Security seguridadObject = new wsSecurity.Security();
            string strDispositivo = utils.getDevice();
            bool? cnQa = null;
            seguridadObject = seguridad.Login("usrTriatlon", "eXperiencias", strDispositivo, utils.getResolution(), Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]), cnQa);
            if (seguridadObject != null)
            {
                strDsSession = seguridadObject.dsSessionId;
                ViewState["strDsSession"] = strDsSession;
                Session["strDsSession"] = strDsSession;
            }
        }

        public void mensaje(string strCadenaValores, Exception exception)
        {
            //lblMensajeError.Text = exception.Message + " - " + exception.InnerException;

            string strExcepcion = "";
            if (exception.ToString().Length > 599)
                strExcepcion = exception.ToString().Substring(0, 600);
            else
                strExcepcion = exception.ToString();

            strCadenaValores += "<br>";

            wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();
            iGeneral.Open();
            string strCorreos = "nfigueroa@experienciasxcaret.com.mx";
            //bool blnBanderaCorreo = iGeneral.SendEmail(strCorreos, "Error en Aplicacion Triatlon", errMensaje, true /*es html*/, "", false, 0, "");
            /*iGeneral.Close();
            wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();*/
            iGeneral.SendEmail(strCorreos, "Error en Aplicacion Travesia " + DateTime.Now.AddYears(1).Year.ToString() + ". Página respuesta worldpay.", strExcepcion, false /*es html*/, "", false, 0, "");
            iGeneral.Close();

        }
    }
}