﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using SelectPdf;
using Newtonsoft.Json;
namespace travesia.backoffice
{
    public partial class editCompetidor : System.Web.UI.Page
    {
        DataTable gdtCompetidor = new DataTable();
        string gstrClienteContactoEvento = "";
        cParticipante gparticipanteModificar = new cParticipante();
        int gintNoCompetidor = 0;
        int gintNoRifa = 0;
        int gintIdSexo = 0;
        int gintIdEstatusCliente = 0;
        string gStringEmail = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            MaintainScrollPositionOnPostBack = true;
            string parameter = Request["__EVENTARGUMENT"];
            string[] arrParameter = null;
            if (parameter != null)
                arrParameter = parameter.Trim().Split('|');
            if (parameter == "btnPagarFinal_Click")
            {
                btnPagarFinal_Click(sender, e);
            }
            if (arrParameter != null)
            { 
                if (arrParameter[0] == "donwloadFile")
                {
                    donwloadFile(arrParameter[1], arrParameter[2]);
                }
            }
            if (!IsPostBack)
            {
                btnPagarFinal.Enabled = true;
                //btnGuardarComplejos.Enabled = true;
                txtCVV.Text = "";
                txtNoTarjeta.Text = "";
                txtNombreTH.Text = "";
                gdtCompetidor = (DataTable)Session["cCompetidoresBusqueda"];
                string strIdLocacion = "";
                string strIdContactoEventoArchivos = "";
                int intidConfigPagoPreregistro = 0;
                if (Page.Request.QueryString["cce"] != null)
                {
                    gstrClienteContactoEvento = Page.Request.QueryString["cce"].ToString();
                    // dtArrComponente Aqui va a estar el que se eligió editar, el principal, el headliner
                    hdIdClienteContactoEventoPrincipales.Value = gstrClienteContactoEvento;
                    DataRow[] dtArrComponente = gdtCompetidor.Select("idClienteContactoEvento = '" + gstrClienteContactoEvento + "'");

                    #region llenarTabla
                    foreach (var data in dtArrComponente)
                    {
                        gintNoRifa = (data["noRifa"].ToString() != "")? int.Parse(data["noRifa"].ToString()):0;
                        gintNoCompetidor = (data["noCompetidor"].ToString() != "")? int.Parse(data["noCompetidor"].ToString()):0;
                        gintIdSexo = int.Parse(data["idSexo"].ToString());
                        gintIdEstatusCliente = int.Parse(data["idEstatusCliente"].ToString());
                        gStringEmail = data["dsCorreoElectronico"].ToString();
                        txtNombre.Text = data["dsContacto"].ToString();
                        txtPaterno.Text = data["dsApellidoPaterno"].ToString();
                        txtMaterno.Text = data["dsApellidoMaterno"].ToString();
                        txtNoCompetidor.Text = data["noCompetidor"].ToString();
                        strIdContactoEventoArchivos = data["dsFolioFmtri"].ToString();
                        //txtNoCompetidor.Enabled = false;
                        intidConfigPagoPreregistro = (data["idConfigPagoPreregistro"].ToString() != "")? int.Parse(data["idConfigPagoPreregistro"].ToString()):0;
                        hdIdClienteContactoEventoPrincipales.Value = data["idClienteContactoEvento"].ToString();
                        hdIdClienteContactoEventoFecha.Value = data["idClienteContactoEvento"].ToString();
                        hdIdClienteContactoPrincipales.Value = data["idClienteContacto"].ToString();
                        hdIdClienteContactoFecha.Value = data["idClienteContacto"].ToString();
                        hdIdProductoOriginal.Value = data["idProducto"].ToString();
                        hdIdVentaDetalle.Value = data["idVentaDetalle"].ToString();
                        hdIdVenta.Value = data["idVenta"].ToString();
                        hdIdPais.Value = data["idPais"].ToString();
                        hdIdEstado.Value = data["idEstado"].ToString();
                        hdCorreoCompetidor.Value = data["dsCorreoElectronico"].ToString();
                        ddSexo.Items.FindByValue(gintIdSexo.ToString()).Selected = true;
                        ddlEstatus.Items.FindByValue(gintIdEstatusCliente.ToString()).Selected = true;
                        txtFeNacimiento.Text = data["feNacimiento"].ToString();
                        txtEmail.Text = gStringEmail;
                        if (intidConfigPagoPreregistro > 0)
                        {
                            try{
                                ddlFormaPagoInscripcion.Items.FindByValue(intidConfigPagoPreregistro.ToString()).Selected = true;
                            }
                            catch (Exception ex) { }
                        }
                        
                        DateTime fechaEvento = new DateTime(2015, 12, 31);
                        int anio = 1916;
                        int mes = 01;
                        int dia = 01;
                        string[] feNacimiento;
                        if (data["feNacimiento"].ToString() != "")
                        {
                            feNacimiento = data["feNacimiento"].ToString().Split('/');
                            int.TryParse(feNacimiento[2].ToString(), out anio);
                            int.TryParse(feNacimiento[1].ToString(), out mes);
                            int.TryParse(feNacimiento[0].ToString(), out dia);
                        }
                        strIdLocacion = data["idLocacion"].ToString();
                        anio = (anio > 0) ? anio : 1900;
                        mes = (mes > 0) ? mes : 01;
                        dia = (dia > 0) ? dia : 01;
                        DateTime fechaNacimiento = new DateTime(anio, mes, dia);
                       
                        int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;

                        utils.getCategorias(getDsSession(),int.Parse(data["idSexo"].ToString()), edad, ddlCategoria, null, "ALL", 1);
                        //utils.getLocaciones(getDsSession(), ddlCategoria);

                        string valueCategoria = data["idEventoClasificacion"].ToString();
                        try
                        {
                            ddlCategoria.Items.FindByValue(valueCategoria).Selected = true;
                        }
                        catch (Exception ex) { }
                        break;
                    }
                    #endregion

                    DataRow[] dtArrComponentes;
                    // Primero damos priridad sobre el numero de competidor para saber si se trata de uno que ya esté inscrito
                    if (gintNoCompetidor != null && gintNoCompetidor != 0)
                        dtArrComponentes = gdtCompetidor.Select("noCompetidor = '" + gintNoCompetidor.ToString() + "'");
                    // si no, entonces se trata de uno incrito, entonces debe de ser uno por rifa
                    else if (gintNoCompetidor != null && gintNoCompetidor != 0)
                        dtArrComponentes = gdtCompetidor.Select("noRifa = '" + gintNoRifa.ToString() + "'");

                    utils.getFormasPago(ddlFormaPago, 27, getDsSession());
                    //ddlFormaPago
                    utils.getBancos(ddlBanco,getDsSession());
                    utils.getMoneda(ddlCambioMoneda, getDsSession());
                    ddlCambioMoneda.Attributes.Add("readonly", "readonly");
                    ddlCambioMoneda.Attributes.Add("disabled", "disabled");
                    utils.llenaAnioUp(ddlAnioExpira);

                    #region getArchivos
                    if (strIdContactoEventoArchivos != "")
                        getFiles(strIdContactoEventoArchivos);
                    #endregion

                    int intIdClienteContactoEvento = int.Parse(hdIdClienteContactoEventoPrincipales.Value);
                    string strAbsoluteUri = HttpContext.Current.Request.Url.AbsoluteUri;
                    string strHost = HttpContext.Current.Request.Url.Host;
                    string[] strspliturl = strAbsoluteUri.Split('/');
                    strAbsoluteUri = strAbsoluteUri.Replace(strspliturl[strspliturl.Length - 1], "");
                    strAbsoluteUri += "summary.aspx?contEvent=" + intIdClienteContactoEvento.ToString();

                    string strPrimerosCaracteres = strAbsoluteUri.Substring(0, 5);
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]) && strHost != "localhost")
                    {
                        if (strPrimerosCaracteres == "http:")
                            strAbsoluteUri = strAbsoluteUri.Replace("http", "https");
                        else if (strPrimerosCaracteres != "https")
                            strAbsoluteUri.Trim().Insert(0, "https://");
                    }
                    linkPDF.NavigateUrl = strAbsoluteUri;
                }
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                int intIdClienteContactoEvento = int.Parse(hdIdClienteContactoEventoPrincipales.Value);
                int intIdClienteContacto = int.Parse(hdIdClienteContactoPrincipales.Value);
                int intIdProducto = 0;
                int.TryParse(hdIdProductoOriginal.Value, out intIdProducto);
                int intIdVentaDetalle = (hdIdVentaDetalle.Value != "") ? int.Parse(hdIdVentaDetalle.Value) : 0;
                int intIdVenta = (hdIdVenta.Value != "")? int.Parse(hdIdVenta.Value) : 0;
                DateTime feNacimientoOriginal = new DateTime();
                bool cnCambiaClienteContactoEvento = false;
                decimal dcPrDescuento = 0;
                wscClienteContactoEvento.IcClienteContactoEventoClient CompClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                CompClient.Open();
                wscClienteContactoEvento.cClienteContactoEvento competidor = new wscClienteContactoEvento.cClienteContactoEvento();                
                competidor = CompClient.SeleccionarcClienteContactoEventoPorId(intIdClienteContactoEvento, getDsSession());                
                CompClient.Close();

                wscClienteContacto.IcClienteContactoClient clienteContacto = new wscClienteContacto.IcClienteContactoClient();
                clienteContacto.Open();
                wscClienteContacto.cClienteContacto cClienteContacto = new wscClienteContacto.cClienteContacto();
                cClienteContacto = clienteContacto.SeleccionarcClienteContactoPorId(intIdClienteContacto, getDsSession());
                clienteContacto.Close();

                wscClienteDetalle.IcClienteDetalleClient iClienteDetalle = new wscClienteDetalle.IcClienteDetalleClient();
                iClienteDetalle.Open();
                wscClienteDetalle.cClienteDetalle cClienteDetalle = new wscClienteDetalle.cClienteDetalle();
                cClienteDetalle = iClienteDetalle.SeleccionarcClienteDetallePorId((int)cClienteContacto.idClienteDetalle, getDsSession());

                // validamos si se cambío el estatus del cliente
                if (cClienteDetalle.idEstatusCliente.ToString() != ddlEstatus.SelectedValue.ToString().Trim())
                {
                    cClienteDetalle.idEstatusCliente = int.Parse(ddlEstatus.SelectedValue.ToString().Trim());
                    iClienteDetalle.ModificarcClienteDetalle(cClienteDetalle, getDsSession());
                }
                iClienteDetalle.Close();

                wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                ikventa.Open();
                wsTransactions.kVenta laVenta = new wsTransactions.kVenta();
                wsTransactions.ListakVentaDetalles listaDetalle = new wsTransactions.ListakVentaDetalles();
                wsTransactions.ListakVentaDetalles listKventandetalleRespuesta = new wsTransactions.ListakVentaDetalles();

                cClienteContacto.dsContacto = txtNombre.Text;
                cClienteContacto.dsApellidoPaterno = txtPaterno.Text;
                cClienteContacto.dsApellidoMaterno = txtMaterno.Text;
                cClienteContacto.dsCorreoElectronico = txtEmail.Text.Trim();
                // Tengo que averiguar si cambia de categoria, por haber cambiado de sexo lo que impica:
                // * cambiarle el sexo en cclientecontacto
                // * cambiarle el nuevo idEventoClasificacion                 

                if (competidor.idSexo.ToString() != ddSexo.SelectedValue.ToString())
                {                    
                    if (competidor.idTipoSangre == null || competidor.idTipoSangre == 0)
                        competidor.idTipoSangre = 5;
                    
                    cClienteContacto.idSexo = int.Parse(ddSexo.SelectedValue.ToString());
                    competidor.idSexo = cClienteContacto.idSexo;
                    cnCambiaClienteContactoEvento = true;         
                    //if (competidor.idventa == 0)
                }

                if (ddlFormaPagoInscripcion.SelectedValue != "")
                {
                    competidor.idConfigPagoPreregistro = int.Parse(ddlFormaPagoInscripcion.SelectedValue);
                    if (competidor.idTipoSangre == null || competidor.idTipoSangre == 0)
                        competidor.idTipoSangre = 5;
                     cnCambiaClienteContactoEvento = true;
                }

                if (txtNoCompetidor.Text.Trim() != "")
                {
                    competidor.NoCompetidor = int.Parse(txtNoCompetidor.Text.Trim());
                    cnCambiaClienteContactoEvento = true;
                }
                if (competidor.idVenta == 0)
                    competidor.idVenta = null;
                if (competidor.idEventoModalidad == 0)
                    competidor.idEventoModalidad = null;
                if (competidor.idEventoClasificacion == 0)
                    competidor.idEventoClasificacion = null;
                if (competidor.idTalla == 0)
                    competidor.idTalla = null;                
                if (competidor.idVentaDetalle == 0)
                    competidor.idVentaDetalle = null;


                DataTable dtCompetidor = (DataTable)Session["cCompetidoresBusqueda"];
                DataRow[] drClienteEvento = dtCompetidor.Select("idClienteContactoEvento = " + intIdClienteContactoEvento);

                string strFeNacimientoNuevo =  "";
                
                strFeNacimientoNuevo = drClienteEvento[0]["feNacimientoNuevo"].ToString();
                decimal.TryParse(drClienteEvento[0]["prDescuento"].ToString(), out dcPrDescuento);//
                if (strFeNacimientoNuevo != "")
                    cClienteContacto.feNacimiento = DateTime.Parse(strFeNacimientoNuevo);

               

                string resultadoAtualizar = cParticipante.actualizaCompetidor(competidor, cClienteContacto, cnCambiaClienteContactoEvento);

                // si tiene idventa y idventadetalle quiere decir que ya tiene registros en es tabla, por ende tambien hay que actualizarla
                if (intIdVentaDetalle > 0 && intIdVenta > 0)
                {                                        
                    // obtengo la lista de kventa
                    wsTransactions.kVentaDetalle kventaDetalle = new wsTransactions.kVentaDetalle();
                    kventaDetalle = ikventa.SeleccionarkVentaDetallePorIdVentaDetalle(intIdVentaDetalle,getDsSession());
                    kventaDetalle.dsNombreVisitante = txtNombre.Text;
                    kventaDetalle.dsApellidoPaternoVisitante = txtPaterno.Text;
                    kventaDetalle.dsApellidoMaternoVisitante = txtMaterno.Text;
                    listaDetalle.Add(kventaDetalle);
                    listKventandetalleRespuesta = utils.modificarVenta(listaDetalle, intIdVenta, intIdVentaDetalle);
                }

                

                string dsCodigoOrinal = drClienteEvento[0]["dsCodigo"].ToString();
                string dsCodigoNuevo = drClienteEvento[0]["dsCodigoNuevo"].ToString();
                int idEventoClasificacionNuevo = 0;
                int.TryParse(drClienteEvento[0]["idEventoClasificacionNuevo"].ToString(), out idEventoClasificacionNuevo);
                // int.TryParse(feNacimiento[2].ToString(), out anio);
                decimal dcDiferencia = 0;
                #region actualizarEvento
                if (idEventoClasificacionNuevo > 0)
                {
                    string strIdVenta = "";
                    strIdVenta = drClienteEvento[0]["idVenta"].ToString();
                    //string feNacimientoOriginal = "";
                    //feNacimientoOriginal = drClienteEvento[0]["feNacimiento"].ToString();
                    int idTipoMonedaNuevo = (drClienteEvento[0]["idTipoMonedaNuevo"].ToString() != "" && drClienteEvento[0]["idTipoMonedaNuevo"] != null) ? int.Parse(drClienteEvento[0]["idTipoMonedaNuevo"].ToString()) : (drClienteEvento[0]["idTipoMonedaNuevo"].ToString() != "" && drClienteEvento[0]["idTipoMonedaNuevo"] != null) ? int.Parse(drClienteEvento[0]["idTipoMoneda"].ToString()) : 2;
                    int idTipoMonedaOriginal = (drClienteEvento[0]["idTipoMoneda"].ToString() != "") ? int.Parse(drClienteEvento[0]["idTipoMoneda"].ToString()) : 2;
                    string strDsAgrupador = drClienteEvento[0]["dsAgrupador"].ToString().Trim();
                    int idLocacionOriginal = int.Parse(drClienteEvento[0]["idLocacion"].ToString().Trim());
                    int idLocacionNuevo = int.Parse(drClienteEvento[0]["idLocacionNuevo"].ToString().Trim());

                    wscTipoMoneda.IcTipoMonedaClient clientTipoMoneda = new wscTipoMoneda.IcTipoMonedaClient();
                    clientTipoMoneda.Open();
                    wscTipoMoneda.cTipoMoneda cTipoMoneda = clientTipoMoneda.SeleccionarcTipoMonedaPorId(idTipoMonedaOriginal, getDsSession());
                    clientTipoMoneda.Close();

                    DataSet resultPrecioOrinal = utils.getPrecio(getDsSession(), idLocacionOriginal, dsCodigoOrinal, 1, false, cTipoMoneda.dsIso);
                    DataSet resultPrecioNuevo = utils.getPrecio(getDsSession(), idLocacionNuevo, dsCodigoNuevo, 1, false, cTipoMoneda.dsIso);

                    

                    // si es mayor a 4, entonces es moneda internacional
                    if (idTipoMonedaOriginal > 4)
                    {
                        if (dcPrDescuento != 0)
                        {
                            dcDiferencia = ((decimal)resultPrecioNuevo.Tables[0].Rows[0]["mnPrecio_MS"] - (decimal)resultPrecioOrinal.Tables[0].Rows[0]["mnPrecio_MS"]) * ( dcPrDescuento / 100);
                            dcDiferencia = (dcDiferencia) - (dcDiferencia * (dcPrDescuento / 100));
                        }
                        else
                            dcDiferencia = (decimal)resultPrecioNuevo.Tables[0].Rows[0]["mnPrecio_MS"] - (decimal)resultPrecioOrinal.Tables[0].Rows[0]["mnPrecio_MS"];
                    }
                    else
                    {
                        if (dcPrDescuento != 0)
                        {
                            dcDiferencia = (decimal)resultPrecioNuevo.Tables[0].Rows[0]["mnPrecio_" + idTipoMonedaOriginal.ToString()] - (decimal)resultPrecioOrinal.Tables[0].Rows[0]["mnPrecio_" + idTipoMonedaOriginal.ToString()];
                            dcDiferencia = (dcDiferencia) - (dcDiferencia * (dcPrDescuento / 100));
                        }
                        else
                            dcDiferencia = (decimal)resultPrecioNuevo.Tables[0].Rows[0]["mnPrecio_" + idTipoMonedaOriginal.ToString()] - (decimal)resultPrecioOrinal.Tables[0].Rows[0]["mnPrecio_" + idTipoMonedaOriginal.ToString()];
                    }

                    int idMoneda = int.Parse(resultPrecioNuevo.Tables[0].Rows[0]["idMoneda_2"].ToString());
                    // decimal dcDiferencia = (decimal)resultPrecioNuevo.Tables[0].Rows[0]["mnPrecio_2"] - (decimal)resultPrecioOrinal.Tables[0].Rows[0]["mnPrecio_2"];
                    string stringFeVisitaNuevo = resultPrecioNuevo.Tables[0].Rows[0]["FeVisita"].ToString();
                    int idProductoNuevo = int.Parse(resultPrecioNuevo.Tables[0].Rows[0]["idProducto"].ToString());
                    drClienteEvento[0]["idProductoNuevo"] = idProductoNuevo;
                    int idProductoPrecioNuevo = int.Parse(resultPrecioNuevo.Tables[0].Rows[0]["idProductoPrecio"].ToString());
                    int idTipoClienteNuevo = int.Parse(resultPrecioNuevo.Tables[0].Rows[0]["idTipoCliente"].ToString());
                    decimal precioListaNuevo = decimal.Parse(resultPrecioNuevo.Tables[0].Rows[0]["mnPrecio_2"].ToString());
                    decimal mnTipoCambio = decimal.Parse(resultPrecioNuevo.Tables[0].Rows[0]["mnTipoCambio_2"].ToString());
                    int feVisitaNuevo = int.Parse(resultPrecioNuevo.Tables[0].Rows[0]["feVisita"].ToString());
                    string[] strMonedaSelected = ddlCambioMoneda.SelectedValue.Trim().Split('-');

                    drClienteEvento[0]["idProductoPrecioNuevo"] = idProductoPrecioNuevo;
                    drClienteEvento[0]["feVisitaNuevo"] = feVisitaNuevo;
                    drClienteEvento[0]["precioListaNuevo"] = precioListaNuevo;
                    drClienteEvento[0]["mnTipoCambioNuevo"] = mnTipoCambio;
                    drClienteEvento[0]["idTipoClienteNuevo"] = idTipoClienteNuevo;
                    drClienteEvento[0]["idTipoMonedaNuevo"] = int.Parse(strMonedaSelected[2].ToString());

                    string respuestaModificaContactoEvento = "";
                    int maxAgrupador = getMaxAgrupador(dtCompetidor);
                    int intIdCliente = cGlobals.idUsuario;
                    hdMontoDiferencia.Value = dcDiferencia.ToString();
                    lblMensajeDiferencia.Text = "";
                    if (dcDiferencia != 0)
                    {
                        if (dcDiferencia > 0)
                        {
                            divPago.Visible = true;
                            lblMensajeDiferencia.Text = "Diferencia de pago por cambio de sede por: " + dcDiferencia.ToString("N2") + " " + cTipoMoneda.dsTipoMoneda;
                        }
                        else if (dcDiferencia < 0)
                        {
                            bool respInsertaPago = insertaPagoDiferenciaNegativos(dcDiferencia, strIdVenta, maxAgrupador);
                            if (respInsertaPago)
                            {
                                int resultActualizar = fnUpgradeCompetidor(intIdCliente, dcDiferencia, idMoneda.ToString(), int.Parse(strIdVenta), maxAgrupador, intIdVentaDetalle, idProductoPrecioNuevo, intIdClienteContactoEvento, idEventoClasificacionNuevo, feVisitaNuevo, precioListaNuevo, mnTipoCambio, idTipoClienteNuevo, idTipoMonedaNuevo, idLocacionNuevo);
                                if (resultActualizar > 0)
                                {

                                    //lblMensajeActualizado.Text = "Datos actualizados";
                                    //Response.Redirect("~/respuestapago.aspx");
                                    string strBaseDatos = (ConfigurationManager.AppSettings["bdProduccion"]);
                                    lblFolio2.Visible = true;
                                    lnkFolio2.Visible = true;
                                    lnkFolio2.NavigateUrl = "https://www.aolxcaret.com/core/cupon/cuponnew.aspx?id=" + intIdVenta.ToString() + "&base=" + strBaseDatos + "&lan=ES_MX";
                                }
                                else
                                { }
                                //lblMensajeActualizado.Text = "Los datos no se actualizaron correctamente";
                            }
                            else
                            {
                                //lblMensajeActualizado.Text = "Los pagos no se insertaron correctamente";
                            }
                        }
                    }// fin diferencia != 0
                    else
                    {
                        bool respInsertaPago = insertaPagoDiferenciaNegativos(dcDiferencia, strIdVenta, maxAgrupador);
                        if (respInsertaPago)
                        {
                            int resultActualizar = fnUpgradeCompetidor(intIdCliente, dcDiferencia, idMoneda.ToString(), int.Parse(strIdVenta), maxAgrupador, intIdVentaDetalle, idProductoPrecioNuevo, intIdClienteContactoEvento, idEventoClasificacionNuevo, feVisitaNuevo, precioListaNuevo, mnTipoCambio, idTipoClienteNuevo, idTipoMonedaNuevo, idLocacionNuevo);
                            if (resultActualizar > 0)
                            {
                                //lblMensajeActualizado.Text = "Datos actualizados";
                                //Response.Redirect("~/respuestapago.aspx");
                                string strBaseDatos = (ConfigurationManager.AppSettings["bdProduccion"]);
                                lblFolio2.Visible = true;
                                lnkFolio2.Visible = true;
                                lnkFolio2.NavigateUrl = "https://www.aolxcaret.com/core/cupon/cuponnew.aspx?id=" + intIdVenta.ToString() + "&base=" + strBaseDatos + "&lan=ES_MX";
                               // btnGuardarComplejos.Enabled = false;
                            }
                            else
                            { }
                            //lblMensajeActualizado.Text = "Los datos no se actualizaron correctamente";
                        }                        
                    }
                }
                #endregion


                if (resultadoAtualizar == "1" && dcDiferencia <= 0)
                {
                    if (listKventandetalleRespuesta.Count > 0 && (intIdVentaDetalle > 0 && intIdVenta > 0))
                    {
                        mensaje("Los datos del competidor fueron actualizados", "successMsg");
                    }
                    else if ((intIdVentaDetalle > 0 && intIdVenta > 0))
                    {
                        mensaje("Los datos del competidor fueron actualizados, pero no en el detalle de la venta, comuniquese con sistemas", "warningMsg");
                    }
                    else {
                        mensaje("Los datos del competidor fueron actualizados", "successMsg");
                    }
                   
                }
                else if (dcDiferencia <= 0)
                {
                    mensaje(resultadoAtualizar, "errorMsg");
                }
            }
            catch (Exception ex)
            {
                mensaje(ex.Message.ToString(), "errorMsg");
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                string apellidoPaterno = "";
                string nombre = "";
                string noCompetidor = "";
                string dsFolioVenta = "";
                int idVenta = 0;
                string strNoRifa = "";
                string dsCorreo = "";
                int idEventoClasificacion = 0;
                int idEstatusVenta = 0;
                int idEstatusParticipante = 0; /*15 inscrito*/                
                int intIdClienteContactoEvento = int.Parse(hdIdClienteContactoEventoPrincipales.Value);

                string loc = "";
                Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                loc = myUri.AbsoluteUri.ToString();
                loc = loc.Replace(myUri.PathAndQuery.ToString(), "/backoffice/competidores.aspx");

                DataSet result = utils.buscarCompetidor(getDsSession(),apellidoPaterno, nombre, noCompetidor, idVenta, strNoRifa, dsCorreo, idEventoClasificacion, idEstatusVenta, idEstatusParticipante, intIdClienteContactoEvento);
                if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                {
                    result.Tables[0].Columns.Add("feVisitaNuevo", typeof(string));
                    result.Tables[0].Columns.Add("feNacimientoNuevo", typeof(string));
                    result.Tables[0].Columns.Add("idEvento", typeof(int));
                    result.Tables[0].Columns.Add("idEventoClasificacionNuevo", typeof(int));
                    result.Tables[0].Columns.Add("dsCodigoNuevo", typeof(string));
                    result.Tables[0].Columns.Add("idProductoPrecioNuevo", typeof(int));
                    result.Tables[0].Columns.Add("idProductoNuevo", typeof(int));
                    result.Tables[0].Columns.Add("precioListaNuevo", typeof(decimal));
                    result.Tables[0].Columns.Add("mnTipoCambioNuevo", typeof(decimal));
                    result.Tables[0].Columns.Add("idTipoClienteNuevo", typeof(int));
                    result.Tables[0].Columns.Add("idTipoMonedaNuevo", typeof(int));
                    result.Tables[0].Columns.Add("idLocacionNuevo", typeof(int));
                    Session["cCompetidoresBusqueda"] = result.Tables[0];
                    Response.Redirect(loc);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void mensaje(string mensaje, string type)
        {
            string strTitutlo = "";
            if (type == "errorMsg")
                strTitutlo = "Ha ocurrido un error";
            if (type == "successMsg")
                strTitutlo = "Cambios realizados satisfactoriamente";
            if (type == "warningMsg")
                strTitutlo = "ATENCION";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('" + type + "','" + strTitutlo + "','" + mensaje + "');</script>", false);
        }

        protected void ddlCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int intIdClienteContactoEvento = int.Parse(hdIdClienteContactoEventoPrincipales.Value);
                DataTable dtCompetidor = (DataTable)Session["cCompetidoresBusqueda"];
                DataRow[] drClienteEvento = dtCompetidor.Select("idClienteContactoEvento = " + intIdClienteContactoEvento);
                string[] strSelectedValue = ddlCategoria.SelectedValue.Split('|');
                string[] strSelectedText = ddlCategoria.SelectedItem.Text.Split('/');
                ListItem listSelected = ddlCategoria.SelectedItem;
                ddlCategoria.DataBind();
                string[] propertiesDrop = getPropertiesDrop();
                /*string evento = "";
                string sexo = "";

                int j = 0;
                string[][] attributes = new string[listSelected.Attributes.Count][];
                foreach (string attribute in listSelected.Attributes.Keys)
                {
                    attributes[j++] = new string[] { attribute, listSelected.Attributes[attribute] };
                }
                if (attributes.Length > 0)
                {
                    evento = attributes[1][1].ToString();
                    sexo = attributes[2][1].ToString();                    
                }*/

                /*foreach (ListItem item in this.ddlCategoria.Items)
                {
                    int j = 0;
                    string[][] attributes = new string[item.Attributes.Count][];
                    if (item.Selected)
                    {
                        foreach (string attribute in item.Attributes.Keys)
                        {
                            attributes[j++] = new string[] { attribute, item.Attributes[attribute] };
                        }
                        if (attributes.Length > 0)
                        {
                            evento = attributes[1][1].ToString();
                            sexo = attributes[2][1].ToString();                            
                        }
                    }
                }*/

                drClienteEvento[0]["idEventoClasificacionNuevo"] = int.Parse(strSelectedValue[0].ToString());
                drClienteEvento[0]["idLocacionNuevo"] = utils.getIdLocacionByIdEventoClasificacion(getDsSession(), strSelectedValue[0].ToString());
                drClienteEvento[0]["dsCodigoNuevo"] = strSelectedText[1].Trim();
                Session["cCompetidoresBusqueda"] = dtCompetidor;

            }
            catch (Exception ex)
            {                
            }
        }

        protected string[] getPropertiesDrop()
        {
            string[] respuesta = new string[3];
            ddlCategoria.DataBind();
            ListItem listSelected = ddlCategoria.SelectedItem;

            string evento = listSelected.Attributes["evento"];
            string sexo = listSelected.Attributes["sexo"];

            int j = 0;
            string[][] attributes = new string[listSelected.Attributes.Count][];
            foreach (string attribute in listSelected.Attributes.Keys)
            {
                attributes[j++] = new string[] { attribute, listSelected.Attributes[attribute] };
            }
            if (attributes.Length > 0)
            {
                evento = attributes[1][1].ToString();
                sexo = attributes[2][1].ToString();
            }
            respuesta[0] = evento;
            respuesta[1] = sexo;
            return respuesta;
        }

        protected void txtFeNacimiento_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int intIdClienteContactoEvento = int.Parse(hdIdClienteContactoEventoPrincipales.Value); //feVisitaNuevo
                int idSexo = 0;
                int idEvento = 0;
                int idProducto = 0;
                DataTable dtCompetidor = (DataTable)Session["cCompetidoresBusqueda"];
                DataRow[] drClienteEvento = dtCompetidor.Select("idClienteContactoEvento = " + intIdClienteContactoEvento);
                drClienteEvento[0]["feNacimientoNuevo"] = txtFeNacimiento.Text;
                idSexo = int.Parse(drClienteEvento[0]["idSexo"].ToString());
                idEvento = int.Parse(drClienteEvento[0]["idEvento"].ToString());
                idProducto = int.Parse(drClienteEvento[0]["idProducto"].ToString());

                DateTime fechaEvento = new DateTime(2015, 12, 31);
                int anio = 1900;
                int mes = 01;
                int dia = 01;
                string[] feNacimiento = txtFeNacimiento.Text.Split('/');
                int.TryParse(feNacimiento[2].ToString(), out anio);
                int.TryParse(feNacimiento[1].ToString(), out mes);
                int.TryParse(feNacimiento[0].ToString(), out dia);

                anio = (anio > 0) ? anio : 1900;
                mes = (mes > 0) ? mes : 01;
                dia = (dia > 0) ? dia : 01;
                DateTime fechaNacimiento = new DateTime(anio, mes, dia);

                int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;

                //utils.getCategorias(getDsSession(),idSexo, edad, ddlCategoria, null, "ALL", 0);
                //utils.getLocaciones(getDsSession(), ddlCategoria);
                int index = 0;
                ddlCategoria.DataBind();
                string evento ="";
                string sexo = "";
                string[] valueItem = null;

                foreach (ListItem item in this.ddlCategoria.Items)
                {
                    int j = 0;
                    string[][] attributes = new string[item.Attributes.Count][];
                    foreach (string attribute in item.Attributes.Keys)
                    {
                        attributes[j++] = new string[] { attribute, item.Attributes[attribute] };
                    }
                    if (attributes.Length > 0)
                    {
                        evento = attributes[1][1].ToString();
                        sexo = attributes[2][1].ToString();
                        valueItem = item.Value.Split('-');
                    }
                    if (sexo == idSexo.ToString() && evento == idEvento.ToString() && idProducto.ToString() == valueItem[1].ToString())
                        break;
                    index++;
                }
                ddlCategoria.SelectedIndex = index;
                string[] strSelectedText = ddlCategoria.SelectedItem.Text.Split('/');
                drClienteEvento[0]["feNacimientoNuevo"] = txtFeNacimiento.Text;
                drClienteEvento[0]["idEventoNuevo"] = evento;
                drClienteEvento[0]["idEventoClasificacionNuevo"] = valueItem[0].ToString();
                drClienteEvento[0]["dsCodigoNuevo"] = strSelectedText[1].Trim();
                Session["cCompetidoresBusqueda"] = dtCompetidor;
            }
            catch (Exception ex)
            {
            }
        }

        protected void pcEditar_ActiveTabChanged(object source, DevExpress.Web.ASPxTabControl.TabControlEventArgs e)
        {

        }

        protected void btnGuardarComplejos_Click(object sender, EventArgs e)
        {
            try
            {
                int intIdClienteContactoEvento = int.Parse(hdIdClienteContactoEventoPrincipales.Value);
                int intIdClienteContacto = int.Parse(hdIdClienteContactoPrincipales.Value);
                int intIdProducto = int.Parse(hdIdProductoOriginal.Value);
                int intIdVentaDetalle = (hdIdVentaDetalle.Value != "") ? int.Parse(hdIdVentaDetalle.Value) : 0;
                int intIdVenta = (hdIdVenta.Value != "") ? int.Parse(hdIdVenta.Value) : 0;

                DataTable dtCompetidor = (DataTable)Session["cCompetidoresBusqueda"];
                DataRow[] drClienteEvento = dtCompetidor.Select("idClienteContactoEvento = " + intIdClienteContactoEvento);

                string dsCodigoOrinal = drClienteEvento[0]["dsCodigo"].ToString();
                string dsCodigoNuevo = drClienteEvento[0]["dsCodigoNuevo"].ToString();
                int idEventoClasificacionNuevo = int.Parse(drClienteEvento[0]["idEventoClasificacionNuevo"].ToString());
                string strIdVenta = "";
                strIdVenta = drClienteEvento[0]["idVenta"].ToString();
                string feNacimientoOriginal = "";
                feNacimientoOriginal = drClienteEvento[0]["feNacimiento"].ToString();
                int idTipoMonedaNuevo = (drClienteEvento[0]["idTipoMonedaNuevo"].ToString() != "" && drClienteEvento[0]["idTipoMonedaNuevo"] != null) ? int.Parse(drClienteEvento[0]["idTipoMonedaNuevo"].ToString()) : (drClienteEvento[0]["idTipoMonedaNuevo"].ToString() != "" && drClienteEvento[0]["idTipoMonedaNuevo"] != null)? int.Parse(drClienteEvento[0]["idTipoMoneda"].ToString()) : 2;
                int idTipoMonedaOriginal = (drClienteEvento[0]["idTipoMoneda"].ToString() != "") ? int.Parse(drClienteEvento[0]["idTipoMoneda"].ToString()) : 2;
                string strDsAgrupador = drClienteEvento[0]["dsAgrupador"].ToString().Trim();
                wscTipoMoneda.IcTipoMonedaClient clientTipoMoneda = new wscTipoMoneda.IcTipoMonedaClient();
                clientTipoMoneda.Open();
                wscTipoMoneda.cTipoMoneda cTipoMoneda = clientTipoMoneda.SeleccionarcTipoMonedaPorId(idTipoMonedaOriginal, getDsSession());
                clientTipoMoneda.Close();
                int idLocacionNuevo = int.Parse(drClienteEvento[0]["idLocacionNuevo"].ToString());

                DataSet resultPrecioOrinal = utils.getPrecio(getDsSession(), 141, dsCodigoOrinal, 1, false, cTipoMoneda.dsIso);
                DataSet resultPrecioNuevo = utils.getPrecio(getDsSession(), 141, dsCodigoNuevo, 1, false, cTipoMoneda.dsIso);

                decimal dcDiferencia = 0;

                // si es un dsAgrupador de cena, no se le va a combrar la diferencia, no calculamos diferencia
                if (strDsAgrupador != "cena" && strDsAgrupador != "Patrocinadores" && strDsAgrupador != "G14" && strDsAgrupador != "Patrocinador")
                {
                    // si es mayor a 4, entonces es moneda internacional
                    if (idTipoMonedaOriginal > 4)
                    {
                        dcDiferencia = (decimal)resultPrecioNuevo.Tables[0].Rows[0]["mnPrecio_MS"] - (decimal)resultPrecioOrinal.Tables[0].Rows[0]["mnPrecio_MS"];
                    }
                    else
                    {
                        dcDiferencia = (decimal)resultPrecioNuevo.Tables[0].Rows[0]["mnPrecio_" + idTipoMonedaOriginal.ToString()] - (decimal)resultPrecioOrinal.Tables[0].Rows[0]["mnPrecio_" + idTipoMonedaOriginal.ToString()];
                    }
                }
                int idMoneda = int.Parse(resultPrecioNuevo.Tables[0].Rows[0]["idMoneda_2"].ToString());
               // decimal dcDiferencia = (decimal)resultPrecioNuevo.Tables[0].Rows[0]["mnPrecio_2"] - (decimal)resultPrecioOrinal.Tables[0].Rows[0]["mnPrecio_2"];
                string stringFeVisitaNuevo = resultPrecioNuevo.Tables[0].Rows[0]["FeVisita"].ToString();
                int idProductoNuevo = int.Parse(resultPrecioNuevo.Tables[0].Rows[0]["idProducto"].ToString());
                drClienteEvento[0]["idProductoNuevo"] = idProductoNuevo;
                int idProductoPrecioNuevo = int.Parse(resultPrecioNuevo.Tables[0].Rows[0]["idProductoPrecio"].ToString());
                int idTipoClienteNuevo = int.Parse(resultPrecioNuevo.Tables[0].Rows[0]["idTipoCliente"].ToString());
                decimal precioListaNuevo = decimal.Parse(resultPrecioNuevo.Tables[0].Rows[0]["mnPrecio_2"].ToString());
                decimal mnTipoCambio = decimal.Parse(resultPrecioNuevo.Tables[0].Rows[0]["mnTipoCambio_2"].ToString());
                int feVisitaNuevo = int.Parse(resultPrecioNuevo.Tables[0].Rows[0]["feVisita"].ToString());
                string[] strMonedaSelected = ddlCambioMoneda.SelectedValue.Trim().Split('-');

                drClienteEvento[0]["idProductoPrecioNuevo"] = idProductoPrecioNuevo;
                drClienteEvento[0]["feVisitaNuevo"] = feVisitaNuevo;
                drClienteEvento[0]["precioListaNuevo"] = precioListaNuevo;
                drClienteEvento[0]["mnTipoCambioNuevo"] = mnTipoCambio;
                drClienteEvento[0]["idTipoClienteNuevo"] = idTipoClienteNuevo;
                drClienteEvento[0]["idTipoMonedaNuevo"] = int.Parse(strMonedaSelected[2].ToString());

                // si diferencia mayor a cero, mostar modulo de cobrar y mostrar liga
                // si es menor de cero, pos guardar los cambios insertando los nuevos registros que tienen que ir
                // si es igual a cero pos solo actualizamos
                string respuestaModificaContactoEvento = "";
                int maxAgrupador = getMaxAgrupador(dtCompetidor);
                int intIdCliente = cGlobals.idUsuario;
                hdMontoDiferencia.Value = dcDiferencia.ToString();
                lblMensajeDiferencia.Text = "";
                if (dcDiferencia != 0)
                {
                    if (dcDiferencia > 0)
                    {
                        divPago.Visible = true;
                        lblMensajeDiferencia.Text = "Diferencia de pago por cambio de sede por: " + dcDiferencia.ToString("N2") + " "+cTipoMoneda.dsTipoMoneda;
                    }
                    else if (dcDiferencia < 0)
                    {
                        bool respInsertaPago = insertaPagoDiferenciaNegativos(dcDiferencia, strIdVenta, maxAgrupador);
                        if (respInsertaPago)
                        {
                            int resultActualizar = fnUpgradeCompetidor(intIdCliente, dcDiferencia, idMoneda.ToString(), int.Parse(strIdVenta), maxAgrupador, intIdVentaDetalle, idProductoPrecioNuevo, intIdClienteContactoEvento, idEventoClasificacionNuevo, feVisitaNuevo, precioListaNuevo, mnTipoCambio, idTipoClienteNuevo, idTipoMonedaNuevo, idLocacionNuevo);
                            if (resultActualizar > 0)
                            {

                                //lblMensajeActualizado.Text = "Datos actualizados";
                                //Response.Redirect("~/respuestapago.aspx");
                                string strBaseDatos = (ConfigurationManager.AppSettings["bdProduccion"]);
                                lblFolio2.Visible = true;
                                lnkFolio2.Visible = true;
                                lnkFolio2.NavigateUrl = "https://www.aolxcaret.com/core/cupon/cuponnew.aspx?id=" + intIdVenta.ToString() + "&base=" + strBaseDatos + "&lan=ES_MX";
                            }
                            else 
                            { }
                                //lblMensajeActualizado.Text = "Los datos no se actualizaron correctamente";
                        }
                        else
                        {
                            //lblMensajeActualizado.Text = "Los pagos no se insertaron correctamente";
                        }
                    }
                }                
                else
                {
                    bool respInsertaPago = insertaPagoDiferenciaNegativos(dcDiferencia, strIdVenta, maxAgrupador);
                    if (respInsertaPago)
                    {
                        int resultActualizar = fnUpgradeCompetidor(intIdCliente, dcDiferencia, idMoneda.ToString(), int.Parse(strIdVenta), maxAgrupador, intIdVentaDetalle, idProductoPrecioNuevo, intIdClienteContactoEvento, idEventoClasificacionNuevo, feVisitaNuevo, precioListaNuevo, mnTipoCambio, idTipoClienteNuevo, idTipoMonedaNuevo, idLocacionNuevo);
                        if (resultActualizar > 0)
                        {
                            //lblMensajeActualizado.Text = "Datos actualizados";
                            //Response.Redirect("~/respuestapago.aspx");
                            string strBaseDatos = (ConfigurationManager.AppSettings["bdProduccion"]);
                            lblFolio2.Visible = true;
                            lnkFolio2.Visible = true;
                            lnkFolio2.NavigateUrl = "https://www.aolxcaret.com/core/cupon/cuponnew.aspx?id=" + intIdVenta.ToString() + "&base=" + strBaseDatos + "&lan=ES_MX";
                            //btnGuardarComplejos.Enabled = false;
                        }
                        else
                        { }
                        //lblMensajeActualizado.Text = "Los datos no se actualizaron correctamente";
                    }
                    // es igual a cero solo actualizo
                    // primero al 
                    /*wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                    wscClienteContactoEvento.cClienteContactoEvento cCompetidor;

                    wscClienteContacto.IcClienteContactoClient clienteContacto = new wscClienteContacto.IcClienteContactoClient();
                    wscClienteContacto.cClienteContacto cClienteContacto = new wscClienteContacto.cClienteContacto();
                    clienteContactoEventoClient.Open();
                    cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId((intIdClienteContactoEvento), getDsSession());
                    cClienteContacto = clienteContacto.SeleccionarcClienteContactoPorId((int)cCompetidor.idClienteContacto, getDsSession());
                    if (txtFeNacimiento.Text != feNacimientoOriginal)
                    {                       
                        
                        cClienteContacto.feNacimiento = DateTime.Parse(txtFeNacimiento.Text);
                    }
                    cCompetidor.idEventoClasificacion = int.Parse(drClienteEvento[0]["idEventoClasificacionNuevo"].ToString());
                    if (cCompetidor.idTipoSangre == 0)
                    {
                        cCompetidor.idTipoSangre = null;
                    }
                    try
                    {
                        respuestaModificaContactoEvento = cParticipante.actualizaCompetidor(cCompetidor, cClienteContacto, true);
                    }
                    catch (Exception ex)
                    {

                    }
                    // ahora
                    
                    wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                    ikventa.Open();
                    wsTransactions.kVenta laVenta = ikventa.SeleccionarkVentaPorIdVenta(intIdVenta, getDsSession());                    
                    wsTransactions.kVentaDetalle listaDetalle = ikventa.SeleccionarkVentaDetallePorIdVentaDetalle(intIdVentaDetalle, getDsSession());
                    wsTransactions.ListakVentaDetalles listaParaEnvio = new wsTransactions.ListakVentaDetalles();
                    wsTransactions.ListakVentaDetalles listKventandetalle2;
                    
                    listaDetalle.feAutoriza = DateTime.Now;
                    listaDetalle.idProductoPrecio = idProductoPrecioNuevo;
                    listaDetalle.feVisita = feVisitaNuevo;
                    listaDetalle.cnAccesado = false;
                    if (listaDetalle.cnFacturado == null)
                        listaDetalle.cnFacturado = false;
                    if (listaDetalle.cnPaquete == null)
                        listaDetalle.cnPaquete = false;
                    if (listaDetalle.cnPromocion == null)
                        listaDetalle.cnPromocion = false;
                    if (listaDetalle.cnNewsletterVisitante == null)
                        listaDetalle.cnNewsletterVisitante = false;
                    if (listaDetalle.cnControlInterno == null)
                        listaDetalle.cnControlInterno = false;

                    listaParaEnvio.Add(listaDetalle);
                    listKventandetalle2 = ikventa.ModificarkVentaLista(laVenta, listaParaEnvio, null, null, getDsSession());
                    ikventa.Close();
                    if (listKventandetalle2.Count > 0)
                    {
                        // todo ok muestro liga para imprimir cupon nuevo
                        string strBaseDatos = (ConfigurationManager.AppSettings["bdProduccion"]);
                        lblFolio2.Visible = true;
                        lnkFolio2.Visible = true;
                        lnkFolio2.NavigateUrl = "https://www.aolxcaret.com/core/cupon/cuponnew.aspx?id=" + intIdVenta.ToString() + "&base=" + strBaseDatos + "&lan=ES_MX";
                    }*/
                }

            }
            catch (Exception ex)
            {
 
            }

        }

        protected bool insertaPagoDiferenciaNegativos(decimal montoDiferencia, string strIdVenta, int NoAgrupadorVenta)
        {
            try
            {
                int intRepGuardaCambioVenta = 0;
                //if (montoDiferencia < 0)
                //{                   
                    int intUltimoPago = 0;
                    int intUltimaTransaccion = 0;

                    wsTransactions.IkPagoClient IkPago = new wsTransactions.IkPagoClient();
                    wsTransactions.ListakPagoTransacciones lstkPagoTransa;
                    wsTransactions.ListakPagos ListakPagos = IkPago.SeleccionarkPagoPorIdVenta(Int32.Parse(strIdVenta), getDsSession());
                    wsTransactions.kPagoTransaccion PagoTran = new wsTransactions.kPagoTransaccion();
                    wsTransactions.kPago objkPago = new wsTransactions.kPago();
                    wsTransactions.ListakPagoTransacciones lstPagoTransacciones;
                    lstkPagoTransa = new wsTransactions.ListakPagoTransacciones();

                    // se identifica el ultimo pago
                    for (int intIndicePago = 0; intIndicePago < ListakPagos.Count; intIndicePago++)
                    {
                        if (ListakPagos[intIndicePago].idEstatusPago == 2 && ListakPagos[intIndicePago].mnTransaccionTotal > 0)
                        {
                            intUltimoPago = intIndicePago;
                        }
                    }
                    // se buscan las transacciones del último pago
                    lstPagoTransacciones = IkPago.SeleccionarkPagosTransaccionesPorIdPago((int)ListakPagos[intUltimoPago].idPago, getDsSession());
                    NoAgrupadorVenta = NoAgrupadorVenta + 1;
                    objkPago = ListakPagos[intUltimoPago];
                    objkPago.idPago = 0;
                    objkPago.idVenta = Int32.Parse(strIdVenta);
                    objkPago.idCanalPago = int.Parse((ConfigurationManager.AppSettings["idCanalVenta"])); // fijo ?
                    objkPago.feTransaccionTotal = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
                    objkPago.hrTransaccionTotal = DateTime.Now;
                    objkPago.mnTransaccionTotal = montoDiferencia;
                    objkPago.cnEsPrepago = false;
                    objkPago.noAgrupadorVenta = NoAgrupadorVenta;
                    objkPago.idFormaPago = objkPago.idFormaPago;
                    objkPago.idEstatusPago = 2; // Aceptado
                    objkPago.feAlta = DateTime.Now;
                    objkPago.idClienteUsuarioAlta = Int32.Parse(cGlobals.idUsuario.ToString());

                    // se identifica la ultima transacción del último pago
                    for (int intIndiceTransaccion = 0; intIndiceTransaccion < lstPagoTransacciones.Count; intIndiceTransaccion++)
                    {
                        if (lstPagoTransacciones[intIndiceTransaccion].idEstatusPago == 2 && lstPagoTransacciones[intIndiceTransaccion].mnTransaccion > 0)
                        {
                            intUltimaTransaccion = intIndiceTransaccion;
                        }
                    }

                    PagoTran = lstPagoTransacciones[intUltimaTransaccion];
                    PagoTran.idPago = objkPago.idPago; // hay que buscarlo
                    PagoTran.idEstatusPago = 2; // Aceptado
                    PagoTran.feTransaccion = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
                    PagoTran.hrTransaccion = DateTime.Now;
                    PagoTran.mnTransaccion = montoDiferencia;
                    PagoTran.noPagosMSI = 0;
                    PagoTran.mnBancoComision = 0;
                    PagoTran.prBancoComision = 0;
                    PagoTran.feAlta = DateTime.Now;
                    PagoTran.idClienteUsuarioAlta = Int32.Parse(cGlobals.idUsuario.ToString());
                    PagoTran.idPagoTransaccion = 0;
                    PagoTran.idTipoMoneda = PagoTran.idTipoMoneda;
                    lstkPagoTransa.Add(PagoTran);
                    int intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                    IkPago.Close();                                      
                //}
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected int getMaxAgrupador(DataTable dtCompetidor)
        {
            int maxAgrupador = 0;
            maxAgrupador = Convert.ToInt32(dtCompetidor.Compute("max(noAgrupadorVenta)", "cnModificacion = 0"));
            return maxAgrupador;
        }

        protected void btnPagarFinal_Click(object sender, EventArgs e)
        {
            try
            {

                int intIdClienteContactoEvento = int.Parse(hdIdClienteContactoEventoPrincipales.Value);
                int intIdClienteContacto = int.Parse(hdIdClienteContactoPrincipales.Value);
                int intIdProducto = int.Parse(hdIdProductoOriginal.Value);
                int intIdVentaDetalle = (hdIdVentaDetalle.Value != "") ? int.Parse(hdIdVentaDetalle.Value) : 0;
                int intIdVenta = (hdIdVenta.Value != "") ? int.Parse(hdIdVenta.Value) : 0;

                DataTable dtCompetidor = (DataTable)Session["cCompetidoresBusqueda"];
                DataRow[] drClienteEvento = dtCompetidor.Select("idClienteContactoEvento = " + intIdClienteContactoEvento);

                int idProductoPrecioNuevo = int.Parse(drClienteEvento[0]["idProductoPrecioNuevo"].ToString());

                int idEventoClasificacionNuevo = int.Parse(drClienteEvento[0]["idEventoClasificacionNuevo"].ToString());

                int feVisitaNuevo = int.Parse(drClienteEvento[0]["feVisitaNuevo"].ToString());
                decimal dcPrecioListaNuevo = decimal.Parse(drClienteEvento[0]["precioListaNuevo"].ToString());
                decimal dcMnTipoCambioNuevo = decimal.Parse(drClienteEvento[0]["mnTipoCambioNuevo"].ToString());
                int idTipoClienteNuevo = int.Parse(drClienteEvento[0]["idTipoClienteNuevo"].ToString());
                int idTipoMonedaNuevo = (drClienteEvento[0]["idTipoMonedaNuevo"].ToString() != "") ? int.Parse(drClienteEvento[0]["idTipoMonedaNuevo"].ToString()) : int.Parse(drClienteEvento[0]["idTipoMoneda"].ToString());
                int intIdLocacionNuevo = int.Parse(drClienteEvento[0]["idLocacionNuevo"].ToString());

                int intIdCliente = cGlobals.idUsuario;
                decimal dcTotal = decimal.Parse(hdMontoDiferencia.Value);
                //                string idMoneda = ddlCambioMoneda.SelectedValue.ToString();
                string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
                string dsIsoMoneda = selectedValueMoneda[1].ToString();
                string idMoneda = selectedValueMoneda[0].ToString();
                string strIdClienteCompetidor = "";
                bool respGuardaPago = false;
                int maxAgrupador = getMaxAgrupador(dtCompetidor);
                int respActualizaCompetidor = 0;
               
                cResultadoGuardadoPago.strIdEstatusPago = "2";
                cResultadoGuardadoPago.strAfiliacion = "";
                cResultadoGuardadoPago.strDsRespuesta = "Aprobado";
                cResultadoGuardadoPago.strDsJustificacion = "";
                cResultadoGuardadoPago.strDsReferencia =  txtReferencia.Text;
                cResultadoGuardadoPago.strDsTransaccion = txtReferencia.Text;
                cResultadoGuardadoPago.strDsCorrelacion = "";
                cResultadoGuardadoPago.strIdBancoReceptor = ddlBanco.SelectedValue;
                 // si la forma de pago es distinta de 3 que quiere decir distinta de deposito
                if (hdEsPagoTarjeta.Value != "3")
                {
                    fnGuardaDatosTarjeta(intIdCliente, dcTotal, idMoneda);
                    if (hdEsPagoTarjeta.Value == "1")
                        fnPagar(intIdCliente, dcTotal, idMoneda, intIdVenta, maxAgrupador);
                }

                if (cResultadoGuardadoPago.strGlobalCollectActivo != "1" && cResultadoGuardadoPago.strGlobalCollectActivo != "2")
                {
                    if (hddPagoGateWay.Value != "4")// si es algo distindo de ingenico entonces si guarda los datos, como antes
                    {                           
                        if (ddlFormaPago.SelectedValue == "14")
                            dcTotal = 0;
                        respGuardaPago = fnGuardarPago(intIdCliente, intIdVenta, dcTotal, idMoneda, maxAgrupador);
                        if (respGuardaPago)
                        {
                            if (cResultadoGuardadoPago.strIdEstatusPago == "2")
                            {
                                int resultActualizar = fnUpgradeCompetidor(intIdCliente, dcTotal, idMoneda, intIdVenta, maxAgrupador, intIdVentaDetalle, idProductoPrecioNuevo, intIdClienteContactoEvento, idEventoClasificacionNuevo, feVisitaNuevo, dcPrecioListaNuevo, dcMnTipoCambioNuevo, idTipoClienteNuevo, idTipoMonedaNuevo, intIdLocacionNuevo);
                                //int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                                // si el resultado es mayor a cero quiere decir que actualizo
                                if (resultActualizar > 0)
                                {
                                    string strBaseDatos = (ConfigurationManager.AppSettings["bdProduccion"]);
                                    //lblFolio.Visible = true;
                                    lnkFolio.Visible = true;
                                    txtCVV.Text = "";
                                    txtNoTarjeta.Text = "";
                                    txtNombreTH.Text = "";
                                    lnkFolio.NavigateUrl = "https://www.xperienciasxcaret.mx/core/cupon/cuponnew.aspx?id=" + intIdVenta.ToString() + "&base=" + strBaseDatos + "&lan=ES_MX";
                                    btnPagarFinal.Enabled = false;                                    
                                }
                            }
                        }
                    }// fin de if (hddPagoGateWay.Value != "4")
                    else
                    {
                        respGuardaPago = fnGuardarPago(intIdCliente, intIdVenta, dcTotal, idMoneda, maxAgrupador);
                        if (respGuardaPago)
                        {
                            if (cResultadoGuardadoPago.strIdEstatusPago == "2")
                            {
                                int resultActualizar = fnUpgradeCompetidor(intIdCliente, dcTotal, idMoneda, intIdVenta, maxAgrupador, intIdVentaDetalle, idProductoPrecioNuevo, intIdClienteContactoEvento, idEventoClasificacionNuevo, feVisitaNuevo, dcPrecioListaNuevo, dcMnTipoCambioNuevo, idTipoClienteNuevo, idTipoMonedaNuevo, intIdLocacionNuevo);
                                //int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                                // si el resultado es mayor a cero quiere decir que actualizo
                                if (resultActualizar > 0)
                                {
                                    string strBaseDatos = (ConfigurationManager.AppSettings["bdProduccion"]);
                                    //lblFolio.Visible = true;
                                    lnkFolio.Visible = true;
                                    txtCVV.Text = "";
                                    txtNoTarjeta.Text = "";
                                    txtNombreTH.Text = "";
                                    lnkFolio.NavigateUrl = "https://www.xperienciasxcaret.mx/core/cupon/cuponnew.aspx?id=" + intIdVenta.ToString() + "&base=" + strBaseDatos + "&lan=ES_MX";
                                    btnPagarFinal.Enabled = false;
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            { 
            }
        }

        protected int fnUpgradeCompetidor(int intIdCliente, decimal dcTotal, string idMoneda, int intIdVenta, int maxAgrupador, int intIdVentaDetalle, int idProductoPrecioNuevo, int idClienteContactoEvento, int idEventoClasificacionNuevo, int feVisitaNuevo, decimal dcPrecioListaNuevo, decimal dcMnTipoCambioNuevo, int idTipoClienteNuevo, int idTipoMonedaNuevo, int intIdLocacionNuevo)
        {
            try
            {
                int tamanioKventaDetalle = 0;
                int intIdVentaDetalleUltimo = 0;
                wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                ikventa.Open();
                wsTransactions.kVenta laVenta = ikventa.SeleccionarkVentaPorIdVenta(intIdVenta, getDsSession());
                laVenta.feVenta = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
                laVenta.hrVenta = DateTime.Now;
                laVenta.dsNotasTransporte = "";
                laVenta.mnMontoTotal = (laVenta.mnMontoTotal + (dcTotal));
                wsTransactions.ListakVentaDetalles listaDetalle = ikventa.SeleccionarkVentasDetallePorIdVenta(intIdVenta, getDsSession());
                wsTransactions.kVentaDetalle kventaDetalle = listaDetalle.Find(kventDetalle => kventDetalle.idVentaDetalle == intIdVentaDetalle);
                wsTransactions.kVentaDetalle detalle = new wsTransactions.kVentaDetalle();
                wsTransactions.ListakVentaDetalles listaParaEnvio = new wsTransactions.ListakVentaDetalles();
                wsTransactions.ListakVentaDetalles listKventandetalle2;
                string strCanalVentaId = (ConfigurationManager.AppSettings["idCanalVenta"].ToString());
                int intIdVentaDetalleNuevo = 0;

                listKventandetalle2 = ikventa.ModificarkVentaLista(laVenta, listaParaEnvio, null, null, getDsSession());

                // AHORA INVESTIGAMOS EL kVentadetalle ultimo que se genero ya que es el cancelado y se guarda en la variable intIdVentaDetalleUltimo               
                tamanioKventaDetalle = listKventandetalle2.Count() - 1;
                intIdVentaDetalleUltimo = (int)listKventandetalle2[tamanioKventaDetalle].idVentaDetalle;

                //AHORA viene la insercion del kventaDetalle pero como pagado y con todos los registros nuevo
                listaParaEnvio.Clear();
                maxAgrupador = maxAgrupador + 1;
                wsTransactions.kVentaDetalle detalle2 = new wsTransactions.kVentaDetalle();
                wsTransactions.kVentaDetalle detalleNuevo = new wsTransactions.kVentaDetalle();
                //wskTransactiones.kVentaDetalle kventaDetalle2 = ikventa.SeleccionarkVentaDetallePorIdVentaDetalle(Int32.Parse(strIdVentaDetalle), strDsSession);
                wsTransactions.kVentaDetalle kventaDetalle2 = listKventandetalle2.Find(kventDetalle => kventDetalle.idVentaDetalle == intIdVentaDetalle);
                detalle2 = kventaDetalle2;
                // si es cortesía yo tengo que cancelar e insertar uno nuevo
                if (ddlFormaPago.SelectedValue == "14" || kventaDetalle2.idEstatusVenta == 23)
                {
                    kventaDetalle2.idEstatusVenta = 1;
                    kventaDetalle2.dsMotivoCancelacionReasignacion = "CAMBIO DE CATEGORIA realizado el " + DateTime.Now.ToString("g");
                    kventaDetalle2.feAutoriza = DateTime.Now;
                    //Agregamos el cancelado
                    listaParaEnvio.Add(kventaDetalle2);
                    wsTransactions.ListakVentaDetalles listKventandetalle3 = ikventa.ModificarkVentaLista(laVenta, listaParaEnvio, null, null, getDsSession());
                    listaParaEnvio.Clear();
                    // en nuevo con estatus de cortesía
                    detalle2.feAlta = DateTime.Now;
                    detalle2.feAutoriza = DateTime.Now;
                    detalle2.noAgrupadorVenta = maxAgrupador;
                    detalle2.idEstatusVenta = 23;
                    detalle2.feVisita = feVisitaNuevo;
                    detalle2.idProductoPrecio = idProductoPrecioNuevo;
                    /*if (dcTotal < 0)
                        dcTotal *= -1;*/
                    detalle.dsMotivoCancelacionReasignacion = "CAMBIO DE CATEGORIA realizado el " + DateTime.Now.ToString("g");
                    //if (detalle2.idEstatusVenta == 23)
                    //  detalle2.mnPrecioVenta =  dcTotal;
                    // else
                    detalle2.mnPrecioVenta = 0;
                    detalle2.mnPrecioLista = dcPrecioListaNuevo;
                    detalle2.mnDescuento = detalle2.mnPrecioLista - detalle2.mnPrecioVenta;
                    detalle2.prDescuento = (decimal)((detalle2.mnDescuento * 100) / detalle2.mnPrecioLista);
                    detalle2.idVentaDetalle = 0;
                    detalle2.idVentaDetallePadre = Convert.ToInt32(intIdVentaDetalle);
                    detalle2.idClienteUsuarioAlta = cGlobals.idUsuario;
                    detalle2.noPax = 1;
                    detalle2.mnTipoCambio = dcMnTipoCambioNuevo;
                    detalle2.cnAccesado = false;
                    detalle2.cnControlInterno = false;
                    detalle2.cnPaquete = false;
                    detalle2.cnPromocion = false;
                    detalle2.idSegmento = 4;
                    detalle2.mnIva = null;
                    detalle2.idTipoCliente = idTipoClienteNuevo;
                    detalle2.idTipoMoneda = idTipoMonedaNuevo;
                    detalle2.idCanalVenta = int.Parse(strCanalVentaId);
                    detalle2.idLocacion = intIdLocacionNuevo;
                    listaParaEnvio.Add(detalle2);

                    wsTransactions.ListakVentaDetalles listKventandetalle4 = ikventa.ModificarkVentaLista(laVenta, listaParaEnvio, null, null, getDsSession());
                    //listKventandetalle3.Count;
                    int sizeKVentaDetalle = listKventandetalle4.Count;

                    for (int k = sizeKVentaDetalle - 1; k >= 0; k--)
                    {
                        if (listKventandetalle4[k].idEstatusVenta == 23)
                        {
                            intIdVentaDetalleNuevo = int.Parse(listKventandetalle4[k].idVentaDetalle.ToString());
                            k = 0;
                            break;
                        }
                    }
                    listaParaEnvio.Clear();
                }
                else // no es cortesía
                {
                    detalle2.feAlta = DateTime.Now;
                    detalle2.feAutoriza = DateTime.Now;
                    detalle2.noAgrupadorVenta = maxAgrupador;                
                    detalle2.idEstatusVenta = 3;
                    detalle2.feVisita = feVisitaNuevo;
                    detalle2.idProductoPrecio = idProductoPrecioNuevo;
                    /*if (dcTotal < 0)
                        dcTotal *= -1;*/
                    detalle.dsMotivoCancelacionReasignacion = "CAMBIO DE CATEGORIA realizado el " + DateTime.Now.ToString("g");
                    //if (detalle2.idEstatusVenta == 23)
                      //  detalle2.mnPrecioVenta =  dcTotal;
                   // else
                    detalle2.mnPrecioVenta = (detalle2.mnPrecioVenta + (dcTotal));
                    detalle2.mnPrecioLista =  (detalle2.mnPrecioLista + (dcTotal));
                    detalle2.mnDescuento = detalle2.mnPrecioLista - detalle2.mnPrecioVenta;
                    detalle2.prDescuento = (decimal)((detalle2.mnDescuento * 100) / detalle2.mnPrecioLista);
                    detalle2.idVentaDetalle = 0;
                    detalle2.idVentaDetallePadre = Convert.ToInt32(intIdVentaDetalle);
                    detalle2.idClienteUsuarioAlta = cGlobals.idUsuario;
                    detalle2.noPax = 1;
                    detalle2.mnTipoCambio = dcMnTipoCambioNuevo;
                    detalle2.cnAccesado = false;
                    detalle2.cnControlInterno = false;
                    detalle2.cnPaquete = false;
                    detalle2.cnPromocion = false;
                    detalle2.idSegmento = 4;
                    detalle2.mnIva = null;
                    detalle2.idTipoCliente = idTipoClienteNuevo;
                    detalle2.idTipoMoneda = idTipoMonedaNuevo;
                    detalle2.idCanalVenta = int.Parse(strCanalVentaId);
                    detalle2.idLocacion = intIdLocacionNuevo;
                    listaParaEnvio.Add(detalle2);

                    wsTransactions.ListakVentaDetalles listKventandetalle3 = ikventa.ModificarkVentaLista(laVenta, listaParaEnvio, null, null, getDsSession());
                    //int kventadetalle = ikventa.ModificarkVenta(laVenta, listaParaEnvio, null, null, getDsSession());
                    //listKventandetalle3.Count;
                    int sizeKVentaDetalle = listKventandetalle3.Count;
                    
                    for (int k = sizeKVentaDetalle - 1; k >= 0; k-- )
                    {
                        if (listKventandetalle3[k].idEstatusVenta == 7 || listKventandetalle3[k].idEstatusVenta == 3)
                        {
                            intIdVentaDetalleNuevo = int.Parse(listKventandetalle3[k].idVentaDetalle.ToString());
                            k = 0;
                            break;
                        }
                    }
                    
                    listaParaEnvio.Clear();
                }

                wscClienteContactoEvento.IcClienteContactoEventoClient CompClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                wscClienteContactoEvento.cClienteContactoEvento cCompetidor = new wscClienteContactoEvento.cClienteContactoEvento();
                wscClienteContactoEvento.cClienteContactoEvento cCompetidorOriginal = new wscClienteContactoEvento.cClienteContactoEvento();
                               
                //cClienteContactoEvento newCompetidor = new wscClienteContactoEvento.cClienteContactoEvento();
                cCompetidor = CompClient.SeleccionarcClienteContactoEventoPorId((idClienteContactoEvento), getDsSession());
                cCompetidorOriginal = CompClient.SeleccionarcClienteContactoEventoPorId((idClienteContactoEvento), getDsSession());
                //newCompetidor = CompClient.SeleccionarcClienteContactoEventoPorId(Convert.ToInt16(idClienteContactoEvento), Globals.dsSession);
                //Competidor.idVenta = null;

                int intRespModificaCompetidor;
                cCompetidor.idEventoClasificacion = idEventoClasificacionNuevo;
                cCompetidor.idVentaDetalle = intIdVentaDetalleNuevo;
                cCompetidor.idClienteContactoEvento = 0;
                if (cCompetidor.idEventoModalidad == 0)
                    cCompetidor.idEventoModalidad = null;               
                if (cCompetidor.idTalla == 0)
                    cCompetidor.idTalla = null;                
                //intRespModificaCompetidor = CompClient.InsertarcClienteContactoEvento(newCompetidor, Globals.dsSession);
                //cCompetidor.idTipoSangre = 7;
                if (cCompetidor.idTipoSangre == 0)
                    cCompetidor.idTipoSangre = null;               
                cCompetidor.feAlta = DateTime.Now;
                cCompetidor.idLocacion = intIdLocacionNuevo;
                intRespModificaCompetidor = CompClient.InsertarcClienteContactoEvento(cCompetidor, getDsSession());


                wscClienteContacto.IcClienteContactoClient clienteContacto = new wscClienteContacto.IcClienteContactoClient();
                wscClienteContacto.cClienteContacto cClienteContacto = new wscClienteContacto.cClienteContacto();               
                cClienteContacto = clienteContacto.SeleccionarcClienteContactoPorId((int)cCompetidor.idClienteContacto, getDsSession());
                cClienteContacto.feNacimiento = DateTime.Parse(txtFeNacimiento.Text);

                int intRespuestaOriginalModificaCompetidor = 0;
                cCompetidorOriginal.cnActivo = false;
                if (cCompetidorOriginal.idEventoModalidad == 0)
                    cCompetidorOriginal.idEventoModalidad = null;
                if (cCompetidorOriginal.idTalla == 0)
                    cCompetidorOriginal.idTalla = null;
                //intRespModificaCompetidor = CompClient.InsertarcClienteContactoEvento(newCompetidor, Globals.dsSession);
                //cCompetidor.idTipoSangre = 7;
                if (cCompetidorOriginal.idTipoSangre == 0)
                    cCompetidorOriginal.idTipoSangre = null; 
                string respuestaModificaContactoEvento = "";
                try
                {
                    respuestaModificaContactoEvento = cParticipante.actualizaCompetidor(cCompetidorOriginal, cClienteContacto, true);
                }
                catch (Exception ex)
                {

                }

                // Ahora che petición por ingresos que cada vez que se cambie de categoria, tambien actualizar los contactoEvento
                // de los idVentas que sean de cenas o donativos
                string[] strFechaAlta = cCompetidorOriginal.feAlta.ToString().Split('/');
                string[] strAnioAlta = strFechaAlta[2].ToString().Split(' ');
                int intAnioAlta = int.Parse(strAnioAlta[0].ToString());
                wsGeneralServices.GeneralServicesClient gralCliente = new wsGeneralServices.GeneralServicesClient();
                wsGeneralServices.ListacCombo listaC = new wsGeneralServices.ListacCombo();
                listaC = gralCliente.SeleccionarcCombo("cClienteContactoEvento", "idClienteContactoEvento", "cast(idVenta as varchar(10))", "where idVenta is not null and idVenta != " + cCompetidorOriginal.idVenta.ToString() + " and noCompetidor = " + cCompetidorOriginal.NoCompetidor.ToString() + " and idClienteContacto = " + cCompetidorOriginal.idClienteContacto.ToString() + " and year(feAlta) = " + intAnioAlta, " order by idClienteContactoEvento", getDsSession());
                int intRespModificaCompetidorDonativo = 0;
                for (int cnt = 0; cnt < listaC.cCombo.Count(); cnt++)
                {
                    //listaPais.Items.Add(new ListItem(listaC.cCombo[cnt].dsValor, listaC.cCombo[cnt].idValor.ToString()));
                    //if (listaC.cCombo[cnt].idValor == 484)    
                    wscClienteContactoEvento.cClienteContactoEvento cCompetidorDonativo = new wscClienteContactoEvento.cClienteContactoEvento();
                    cCompetidorDonativo = CompClient.SeleccionarcClienteContactoEventoPorId(int.Parse(listaC.cCombo[cnt].idValor.ToString()), getDsSession());
                    cCompetidorDonativo.idEventoClasificacion = idEventoClasificacionNuevo;
                    if (cCompetidorDonativo.idEventoModalidad == 0)
                        cCompetidorDonativo.idEventoModalidad = null;
                    if (cCompetidorDonativo.idTalla == 0)
                        cCompetidorDonativo.idTalla = null;
                    //intRespModificaCompetidor = CompClient.InsertarcClienteContactoEvento(newCompetidor, Globals.dsSession);
                    //cCompetidor.idTipoSangre = 7;
                    if (cCompetidorDonativo.idTipoSangre == 0)
                        cCompetidorDonativo.idTipoSangre = null;
                    intRespModificaCompetidorDonativo = CompClient.ModificarcClienteContactoEvento(cCompetidorDonativo, getDsSession());
                }
                //intRespuestaOriginalModificaCompetidor = CompClient.ModificarcClienteContactoEvento(cCompetidorOriginal, getDsSession());
                if (intRespModificaCompetidor > 0)
                {
                    return 1;
                }                


                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void fnGuardaDatosTarjeta(int intIdCliente, decimal dcTotal, string idMoneda)
        {
            string strIdClienteTarjeta;
            
            string idTipoTarjeta = rblTipoTarjeta.SelectedValue.ToString();
            if (rblTipoTarjeta.SelectedValue.ToString() == "2")
                idTipoTarjeta = "1";
            else if (rblTipoTarjeta.SelectedValue.ToString() == "1")
                idTipoTarjeta = "2";
            // Se guarda datos en cliente tarjeta
            wscClienteTarjeta.IcClienteTarjetaClient IcClienteTarjeta = new wscClienteTarjeta.IcClienteTarjetaClient();
            IcClienteTarjeta.Open();
            wscClienteTarjeta.cClienteTarjeta cClienteTarj = new wscClienteTarjeta.cClienteTarjeta();
            cClienteTarj.idCliente = cGlobals.idCliente;
            cClienteTarj.idEstatusCliente = 1; // Activo
            cClienteTarj.dsTarjeta = txtNoTarjeta.Text.Trim();
            cClienteTarj.idBanco = Int32.Parse(ddlBanco.SelectedItem.Value);
            cClienteTarj.idTipoTarjeta = int.Parse(idTipoTarjeta);
            cClienteTarj.dsNombre = txtNombreTH.Text.Trim().ToUpper();// +" " + txtApellidoTH.Text.Trim().ToUpper();
            cClienteTarj.feAlta = DateTime.Now;
            cClienteTarj.idClienteUsuarioAlta = (cGlobals.idUsuario);
            cClienteTarj.idClienteTarjeta = 0;
            cClienteTarj.cnActivo = true;

            strIdClienteTarjeta = IcClienteTarjeta.InsertarcClienteTarjeta(cClienteTarj, getDsSession()).ToString();
            cResultadoGuardadoPago.strIdClienteTarjeta = strIdClienteTarjeta;
            IcClienteTarjeta.Close();
        }

        protected void fnPagar(int intIdCliente, decimal dcTotal, string idMoneda, int idVenta, int maxAgrupador)
        {
            int intFechaHoy = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
            string strDcTotalDolar = "0";
            decimal dcTotalDolar = 0;
            int intIdTipoMonedaPagar = 0;
            string strIdTipoMoneda = "";
            string strFolios = "";
            string strIdVenta = "";
            //string[] strIdVentas = hdIV.Value.Split(',');
            string strIdPago = "";


            string strCanalVentaId = (ConfigurationManager.AppSettings["idCanalVenta"].ToString());
            string strIdEstatusPago = "";
            wsTransactions.IkVentaClient vc = new wsTransactions.IkVentaClient();
            vc.Open();

            //for (int l = 0; l < (strIdVentas.Length - 1); l++)
            //{
            wsTransactions.kVenta laVenta = vc.SeleccionarkVentaPorIdVenta(idVenta, getDsSession());
             //   strFolios += laVenta.dsClaveVenta + ",";
              //  strIdVenta += laVenta.idVenta.ToString() + "|";
            //}

            //strFolios = strFolios.Remove(strFolios.Length - 1);
            //strIdVenta = strIdVenta.Remove(strIdVenta.Length - 1);

            vc.Close();
            wscMoneda.IcMonedaClient IcMoneda = new wscMoneda.IcMonedaClient();
            IcMoneda.Open();

            wscMoneda.ListacMonedas lstMonedas = IcMoneda.SeleccionarcMonedas(getDsSession());
            IcMoneda.Close();

            for (int cntMoneda = 0; cntMoneda < lstMonedas.Count; cntMoneda++)
            {
                if ((int)lstMonedas[cntMoneda].idMoneda == Int32.Parse(idMoneda))
                {
                    strIdTipoMoneda = lstMonedas[cntMoneda].idTipoMoneda.ToString();
                }
            }

            wsTransactions.IkCajeroClient IkCajero = new wsTransactions.IkCajeroClient();
            IkCajero.Open();
            cResultadoGuardadoPago.strGlobalCollectActivo = "0";

            bool cnCayoError = false;
            wsSecurity.IsSecurityClient security = new wsSecurity.IsSecurityClient();
            Response.BufferOutput = true;
            string strCadenaEncriptada = "";
            string strDsRespuesta = "";
            string strDsJustificacion = "";
            string strDsTransaccion = "";
            string strDsReferencia = "";
            string strDsCorrelacion = "";
            string srtdsAfiliacion = "";

            switch (hddPagoGateWay.Value)
            {
                case "1":
                #region BANCOS
                    switch (ddlBanco.SelectedItem.Value)
                    {
                        case "35":
                            // Amex
                            // SIEMPRE EN PESOS
                            strDcTotalDolar = dcTotal.ToString("N2");
                            strDcTotalDolar = strDcTotalDolar.Replace(",", "");
                            //strDcTotalDolar = strDcTotalDolar.Replace(".", "");
                            wsTransactions.Amex amex = new wsTransactions.Amex();
                            wsTransactions.AmexResponse amexresponse = new wsTransactions.AmexResponse();

                            amex.anioExpiracion = ddlAnioExpira.SelectedItem.Value.Substring(ddlAnioExpira.SelectedItem.Value.Length - 2);
                            amex.mesExpiracion = ddlMesExpira.SelectedItem.Value;

                            amex.codigoSeguridad = txtCVV.Text;
                            /* * * * * * * * * * * * * * * * * * MONTO A PAGAR - total * * * * * * * * * * * * */
                            //amex.importe = "3000.00";
                            amex.importe = strDcTotalDolar;
                            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

                            amex.ip = "192.168.24.19";
                            amex.moneda = "MXN";
                            amex.numeroTarjeta = txtNoTarjeta.Text;
                            amex.ordenId = laVenta.dsClaveVenta; // Folio Venta
                            amex.referencia = laVenta.dsClaveVenta; // Folio Venta
                            amex.referenciaOrden = laVenta.dsClaveVenta; // Folio Venta
                            amex.transaccionId = laVenta.dsClaveVenta; // Folio Venta
                            //si no es a MSI estos dos campos no se llenas ó se pasan en nulo.
                            //if (Int32.Parse(cmbPromosionMSI.SelectedItem.Value) > 1)
                            //{
                            // amex.planPagos = "AMEX_PLANN";
                            // amex.numeroPagos = 1;
                            //}

                            try
                            {
                                //checaCreaSessionBD();
                                //amexresponse = IkCajero.PagoAMEX(amex, strDsSession);
                                amexresponse = IkCajero.PagoAMEXM(amex, getDsSession(), "9351670303");

                            }
                            catch (Exception err)
                            {
                                strIdEstatusPago = "1" /*Declinado*/;
                                string strParametros = "GuardaPagoTarjeta - Amex 1 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                                //mensaje(strParametros, err);
                            }

                            lblRespuestaPago.Text = ((amexresponse.gatewayCode != null) ? amexresponse.gatewayCode : "") + " " + ((amexresponse.authorizationCode != null) ? amexresponse.authorizationCode : "") + " " + ((amexresponse.errorCode != null) ? amexresponse.errorCode : "") + " " + ((amexresponse.errorMessage != null) ? amexresponse.errorMessage : "");

                            strIdEstatusPago = "1" /*Declinado*/;

                            if (amexresponse.gatewayCode.ToString().Trim().Length > 0)
                            {
                                if (amexresponse.gatewayCode.ToString().Trim() == "APPROVED")
                                {
                                    strIdEstatusPago = "2" /*Aceptado*/;
                                }
                            }
                            cResultadoGuardadoPago.strIdEstatusPago = strIdEstatusPago;
                            cResultadoGuardadoPago.strAfiliacion = (amexresponse.dsAfiliacion != null) ? amexresponse.dsAfiliacion : "";
                            cResultadoGuardadoPago.strDsRespuesta = (amexresponse.gatewayCode != null) ? amexresponse.gatewayCode.ToString() : "";
                            cResultadoGuardadoPago.strDsJustificacion = (amexresponse.gatewayCode != null) ? amexresponse.gatewayCode.ToString() : "";
                            cResultadoGuardadoPago.strDsReferencia = (amexresponse.gatewayCode != null) ? amexresponse.gatewayCode.ToString() : "";
                            cResultadoGuardadoPago.strDsTransaccion = (amexresponse.authorizationCode != null) ? amexresponse.authorizationCode.ToString() : "";
                            cResultadoGuardadoPago.strDsCorrelacion = ((amexresponse.recipient != null) ? amexresponse.recipient.ToString().Trim() : "") + "-" + ((amexresponse.terminal != null) ? amexresponse.terminal.ToString().Trim() : "");
                            cResultadoGuardadoPago.strIdBancoReceptor = "35";
                            break;
                        case "5":
                            // Bancomer
                            //IkCajeroClient cajero = new IkCajeroClient();
                       
                            try
                            {

                                //wskTransactiones.IkCajeroClient cajero = new wskTransactiones.IkCajeroClient();
                                string strDcTotal = dcTotal.ToString("N2");
                                strDcTotal = strDcTotal.Replace(",", "");

                                wsTransactions.Adquira_EMV adquiraemv = new wsTransactions.Adquira_EMV();
                                wsTransactions.Adquira_EMVResponse resp = new wsTransactions.Adquira_EMVResponse();
                                adquiraemv.s_transm = DateTime.Now.ToString("yyyyMMddHHmmss"); // aaaammddhhmmss
                                adquiraemv.c_referencia = laVenta.dsClaveVenta; // Folio Venta
                                adquiraemv.val_1 = 12; //unidad de negocio - 12= computadora
                                adquiraemv.clave_entidad = 10778;
                                adquiraemv.t_servicio = "1121"; //cc

                                /* * * * * * * * * * * * * * * * * * MONTO A PAGAR - total * * * * * * * * * * * * * * * * */
                                if (intIdTipoMonedaPagar != 2) // diferente de Pesos
                                {
                                    adquiraemv.c_cur = 1; // 0-PESOS, 1-DOLARES 
                                    adquiraemv.t_importe = strDcTotal.ToString();
                                }
                                else //pesos
                                {
                                    adquiraemv.c_cur = 0; //?? 0-PESOS, 1-DOLARES
                                    adquiraemv.t_importe = strDcTotal.ToString();
                                }
                                /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

                                adquiraemv.tarjetahabiente = txtNombreTH.Text.Trim();// +" " + txtApellidoTH.Text.Trim(); // nombre tarjetahabiente
                                adquiraemv.val_3 = txtNoTarjeta.Text.Trim(); //No de tarjeta
                                adquiraemv.val_4 = ddlAnioExpira.SelectedItem.Value.Substring(ddlAnioExpira.SelectedItem.Value.Length - 2) + ddlMesExpira.SelectedItem.Value; // fecha vencimiento yymm
                                adquiraemv.val_5 = txtCVV.Text.Trim(); // codigo seguridad
                                adquiraemv.val_6 = adquiraemv.s_transm + adquiraemv.c_referencia + adquiraemv.t_importe + txtCVV.Text;
                                adquiraemv.val_11 = ""; // correo electronico Cliente
                                adquiraemv.val_12 = ""; // Telefono Cliente
                                //adquiraemv.clave_entidad = int.Parse(clave_entidad.Text) // 1730 ? 
                                adquiraemv.val_16 = rblTipoTarjeta.SelectedValue; //"1"; //? 1:Visa 2:mastercard  
                                adquiraemv.val_17 = "0"; // 0: digitada 
                                adquiraemv.val_18 = ""; // vacio

                                adquiraemv.val_19 = "0"; //si tiene msi 1 sino 0
                                adquiraemv.val_20 = "0"; // no. de meses
                                /* if (Int32.Parse(cmbPromosionMSI.SelectedItem.Value) > 1)
                                 {
                                     adquiraemv.val_19 = "1"; //si tiene msi 1 sino 0
                                     adquiraemv.val_20 = cmbPromosionMSI.SelectedItem.Value; // no. de meses
                                 }*/

                                adquiraemv.email_admin = "faraujo@experienciasxcaret.com.mx";
                                adquiraemv.accion = "PAGO";
                                adquiraemv.nu_afiliacion = "";
                                adquiraemv.nu_plataforma = "7";

                                try
                                {
                                    //resp = cajero.PagoAdquiraEMV(adquiraemv, strDsSession, Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"])); // true:produccion, false:pruebas
                                    resp = IkCajero.PagoAdquiraEMV(adquiraemv, getDsSession(), Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"])); // true:produccion, false:pruebas
                                }
                                catch (Exception err)
                                {
                                    strIdEstatusPago = "1" /*Declinado*/;
                                    string strParametros = "GuardaPagoTarjeta - Bancomer 1 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                                    //mensaje(strParametros, err);

                                }

                                lblRespuestaPago.Text = ((resp.mensaje != null) ? resp.mensaje : "") + " " + ((resp.autorizacion != null) ? resp.autorizacion : "") + " - " + ((resp.status != null) ? resp.status : "");

                                string strIdBancoReceptor = ddlBanco.SelectedItem.Value;

                                strIdEstatusPago = "1" /*Declinado*/;
                                if (resp != null)
                                {
                                    //    MessageBox.Show("Autorizacion : " + resp.autorizacion + " Mensaje :" + resp.mensaje + " Status :" + resp.status + " Imprimir: " + resp.imprimir + " Fecha : " + resp.fecha + " Hora:" + resp.hora);
                                    if (resp.mensaje == "APROBADA")
                                    {
                                        strIdEstatusPago = "2" /*Aceptado*/;
                                        if (resp.autorizacion != null && resp.autorizacion == "000000")
                                        {
                                            strIdEstatusPago = "1" /*Declinado*/;
                                            lblRespuestaPago.Text += " - No se cobro hasta su autorización";
                                        }
                                    }
                                }

                                strDsRespuesta = (resp.mensaje != null) ? resp.mensaje.ToString() : "";
                                strDsJustificacion = (resp.mensaje != null) ? resp.mensaje.ToString() : "";
                                strDsTransaccion = (resp.autorizacion != null) ? resp.autorizacion.ToString() : "";
                                strDsReferencia = (resp.mensaje != null) ? resp.mensaje.ToString() : "";
                                strDsCorrelacion = ((resp.autorizacion != null) ? resp.autorizacion.ToString() : "");
                                srtdsAfiliacion = ((resp.dsAfiliacion != null) ? resp.dsAfiliacion.ToString() : "");                            
                            }
                            catch (Exception err)
                            {
                                lblRespuestaPago.Text = "";
                                strIdEstatusPago = "1";
                                strDsJustificacion = "Revisar";
                                strDsJustificacion = err.Message;
                                string strParametros = "GuardaPagoTarjeta - Bancomer 2 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                                //mensaje(strParametros, err);

                            }
                            cResultadoGuardadoPago.strIdEstatusPago = strIdEstatusPago;
                            cResultadoGuardadoPago.strAfiliacion = srtdsAfiliacion;
                            cResultadoGuardadoPago.strDsRespuesta = strDsRespuesta;
                            cResultadoGuardadoPago.strDsJustificacion = strDsJustificacion;
                            cResultadoGuardadoPago.strDsReferencia = strDsReferencia;
                            cResultadoGuardadoPago.strDsTransaccion = strDsTransaccion;
                            cResultadoGuardadoPago.strDsCorrelacion = strDsCorrelacion;
                            cResultadoGuardadoPago.strIdBancoReceptor = "5";
                            break;
                        default:
                            string strDsAfiliacionBanamex = "";
                            string strIdBancoReceptorBanamex = "";
                            try
                            {

                                string strDcTotal = dcTotal.ToString("N2");
                                strDcTotal = strDcTotal.Replace(",", "");
                                //string strDcTotal = dcTotal.ToString().Replace(".", "");
                                strDcTotal = strDcTotal.Replace(".", "");
                                strDcTotalDolar = strDcTotal.Replace(".", "");
                                                        
                                wsTransactions.CSVPC2Party C2Party = new wsTransactions.CSVPC2Party();
                                wsTransactions.CSVPC2PartyResponse C2PartyRespo = new wsTransactions.CSVPC2PartyResponse();


                                /* * * * * * * * * * * * * * * * * * MONTO A PAGAR - total * * * * * * * * * * * * * * * * */
                                //adquiraemv.t_importe = "300000"; //txtMontoTemporal.Text; // importe 10000
                                string strCurrency = "MXN";

                                if (strIdTipoMoneda != "2") // diferente de Pesos
                                {
                                    strCurrency = "USD";
                                    C2Party.vpc_Amount = strDcTotalDolar;
                                }
                                else //pesos
                                {
                                    strCurrency = "MXN";
                                    C2Party.vpc_Amount = strDcTotal;
                                }
                                /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
                                ///* * * * * * * * * * * * * * * * * * MONTO A PAGAR - total * * * * * * * * * * * * * * * * */
                                //C2Party.vpc_Amount = strDcTotal;
                                ///* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

                                C2Party.vpc_CardExp = ddlAnioExpira.SelectedItem.Value.Substring(ddlAnioExpira.SelectedItem.Value.Length - 2) + ddlMesExpira.SelectedItem.Value;
                                C2Party.vpc_CardNum = txtNoTarjeta.Text;
                                C2Party.vpc_MerchTxnRef = laVenta.dsClaveVenta; // Folio Venta
                                C2Party.vpc_OrderInfo = C2Party.vpc_MerchTxnRef; //txtNoReferencia.Text; // cualquier texto
                                C2Party.vpc_Version = "1";
                                C2Party.vpc_CardSecurityCode = txtCVV.Text;
                                C2Party.vpc_TicketNo = laVenta.dsClaveVenta; // Folio Venta
                                C2Party.vpc_Type = "bmx";
                                //C2Party.vpc_Type = cmbTipoTarjeta.SelectedItem.Text;
                                C2Party.vpc_NumPayments = "1";

                                try
                                {
                                    //C2PartyRespo = IkCajero.PagoCSVPC2Party(C2Party, strDsSession);
                                    C2PartyRespo = IkCajero.PagoBanamexM(C2Party, getDsSession(), "4010641", strCurrency);
                                }
                                catch (Exception err)
                                {
                                    strIdEstatusPago = "1";
                                    string strParametros = "GuardaPagoTarjeta - Banamex 1 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                                    //mensaje(strParametros, err);
                                }

                                lblRespuestaPago.Text = ((C2PartyRespo.vpc_TxnResponseCode != null) ? C2PartyRespo.vpc_TxnResponseCode.ToString() : "") + " " + ((C2PartyRespo.vpc_AuthorizeId != null) ? C2PartyRespo.vpc_AuthorizeId.ToString() : "") + " - " + ((C2PartyRespo.vpc_Message != null) ? C2PartyRespo.vpc_Message.ToString() : "") + " - " + ((C2PartyRespo.vpc_TxnResponseDescription != null) ? C2PartyRespo.vpc_TxnResponseDescription.ToString() : "");

                                strIdBancoReceptorBanamex = "108"; // Banamex
                                strDsAfiliacionBanamex = (C2PartyRespo.dsAfiliacion != null) ? C2PartyRespo.dsAfiliacion.ToString() : "";

                                strIdEstatusPago = "1";
                                if (C2PartyRespo != null)
                                {
                                    strIdEstatusPago = (C2PartyRespo.vpc_TxnResponseCode.ToString() == "0") ? "2" /*Aceptado*/: "1" /*Declinado*/;

                                    if (strIdEstatusPago == "2" && C2PartyRespo.vpc_AuthorizeId != null && C2PartyRespo.vpc_AuthorizeId == "000000")
                                    {
                                        strIdEstatusPago = "1";/*Declinado*/;
                                        lblRespuestaPago.Text += " - No se cobro hasta su autorización";
                                    }
                                }

                                strDsRespuesta = (C2PartyRespo.vpc_AVSResultDescription != null) ? C2PartyRespo.vpc_AVSResultDescription.ToString() : "";
                                strDsJustificacion = (C2PartyRespo.vpc_AVSResultDescription != null) ? C2PartyRespo.vpc_AVSResultDescription.ToString() : "";
                                strDsTransaccion = (C2PartyRespo.vpc_AuthorizeId != null) ? C2PartyRespo.vpc_AuthorizeId.ToString() : "";
                                strDsReferencia = (C2PartyRespo.vpc_AVSResultDescription != null) ? C2PartyRespo.vpc_AVSResultDescription.ToString() : "";
                                strDsCorrelacion = ((C2PartyRespo.vpc_ReceiptNo != null) ? C2PartyRespo.vpc_ReceiptNo.ToString() : "") + "-" + ((C2PartyRespo.vpc_TransactionNo != null) ? C2PartyRespo.vpc_TransactionNo.ToString() : ""); //C2PartyRespo.vpc_AuthorizeId + "-" + C2PartyRespo.vpc_TransactionNo;                          

                            }
                            catch (Exception err)
                            {
                                lblRespuestaPago.Text = "";
                                strIdEstatusPago = "1";
                                strDsJustificacion = "Revisar";
                                strDsJustificacion = err.Message;
                                string strParametros = "GuardaPagoTarjeta - Banamex 2 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                                //mensaje(strParametros, err);
                            }
                             cResultadoGuardadoPago.strIdEstatusPago = strIdEstatusPago;
                            cResultadoGuardadoPago.strAfiliacion = strDsAfiliacionBanamex;
                            cResultadoGuardadoPago.strDsRespuesta = strDsRespuesta;
                            cResultadoGuardadoPago.strDsJustificacion = strDsJustificacion;
                            cResultadoGuardadoPago.strDsReferencia = strDsReferencia;
                            cResultadoGuardadoPago.strDsTransaccion = strDsTransaccion;
                            cResultadoGuardadoPago.strDsCorrelacion = strDsCorrelacion;
                            cResultadoGuardadoPago.strIdBancoReceptor = strIdBancoReceptorBanamex;
                            break;
                    }
                    #endregion Bancos
                    break;
                case "2":
                    #region global
                    if (hdCnGlobalCollect.Value == "1")
                    {
                        if (hdCnGlobalCollectI.Value == "1")
                        {
                            strCanalVentaId = strCanalVentaId.ToString() + "_INT"; //"11_INT";
                        }
                        cResultadoGuardadoPago.strGlobalCollectActivo = "1";
                        wscTipoMoneda.IcTipoMonedaClient IcTipoMoneda = new wscTipoMoneda.IcTipoMonedaClient();
                        IcTipoMoneda.Open();
                        wscTipoMoneda.cTipoMoneda cTipoMoneda = IcTipoMoneda.SeleccionarcTipoMonedaPorId(int.Parse(strIdTipoMoneda), getDsSession());
                        IcTipoMoneda.Close();

                        wscPais.IcPaisClient IcPaisClient = new wscPais.IcPaisClient();
                        IcPaisClient.Open();
                        wscPais.cPais cPais = IcPaisClient.SeleccionarcPaisPorId(Int32.Parse(hdIdPais.Value), getDsSession());
                        IcPaisClient.Close();

                        string strDcTotal = dcTotal.ToString("N2").Replace(".", "");
                        strDcTotal = strDcTotal.ToString().Replace(",", "");
                        wsTransactions.GlobalCollect globalCollect = new wsTransactions.GlobalCollect();
                        wsTransactions.GlobalCollectResponse globalCollectResponse = new wsTransactions.GlobalCollectResponse();

                        wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                        icConfiguracion.Open();
                        wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionIPAddressGlobalCollect = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-XCRM-ClientIPAddressGlobalCollect", getDsSession());
                        icConfiguracion.Close();

                        string nombre = "";
                        string Apellidos = "";
                        string[] arrNomBre = txtNombreTH.Text.Trim().Split(' ');
                        nombre = arrNomBre[0].ToString();
                        if (arrNomBre.Count() > 1)
                            for (int l = 1; l < arrNomBre.Count(); l++)
                                Apellidos += arrNomBre[l].ToString() + " ";

                        globalCollect.Apellido = Apellidos;
                        globalCollect.Nombre = nombre;
                        globalCollect.CanalVentaId = strCanalVentaId;
                        globalCollect.ClienteId = intIdCliente.ToString();
                        globalCollect.CodigoIdioma = "es";
                        globalCollect.CodigoMoneda = cTipoMoneda.dsIso;  // "USD";  "MXN"
                        globalCollect.CodigoPais = cPais.dsIso2; // "US";
                        globalCollect.CorreoElectronico = hdCorreoCompetidor.Value; //"faraujo@experienciasxcaret.com.mx"; // del cliente
                        globalCollect.Monto = strDcTotal;
                        globalCollect.NumeroPagos = 1;
                        globalCollect.ReferenciaComercio = laVenta.dsClaveVenta; // Folio Venta
                        globalCollect.IdVenta = idVenta.ToString(); // string con pipes
                        globalCollect.DeviceType = "7"; // 7-Desktop
                        globalCollect.CustomerIPAddress = cConfiguracionIPAddressGlobalCollect.dsValor.Trim(); //Utils.GetIpAddress();
                        globalCollect.DireccionIP = utils.GetIpAddress();

                        string strAbsoluteUri = HttpContext.Current.Request.Url.AbsoluteUri;
                        string strHost = HttpContext.Current.Request.Url.Host;
                        string[] strspliturl = strAbsoluteUri.Split('/');
                        strAbsoluteUri = strAbsoluteUri.Replace(strspliturl[strspliturl.Length - 1], "");
                        strAbsoluteUri = strAbsoluteUri.Replace(strspliturl[strspliturl.Length - 2] + "/", "");
                        strAbsoluteUri += "respuestaGlobal.aspx";
                        string strPrimerosCaracteres = strAbsoluteUri.Substring(0, 5);
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]) && strHost != "localhost")
                        {
                            if (strPrimerosCaracteres == "http:")
                                strAbsoluteUri = strAbsoluteUri.Replace("http", "https");
                            else if (strPrimerosCaracteres != "https")
                                strAbsoluteUri.Trim().Insert(0, "https://");
                        }
                        globalCollect.UrlRetorno = strAbsoluteUri;

                        //lblMensajeError.Text = strAbsoluteUri;

                        // TipoProducto 2=american 1=visa 3=master

                        if (ddlBanco.SelectedItem.Value == "36")// Amex
                        {
                            globalCollect.TipoProducto = "2";
                        }
                        else
                        {
                            //cmbTipoTarjeta.SelectedValue 1:Visa 2:mastercard

                            if (ddlFormaPago.SelectedValue == "27" || ddlFormaPago.SelectedValue == "29") // Visa
                                globalCollect.TipoProducto = "1";
                            if (ddlFormaPago.SelectedValue == "28" || ddlFormaPago.SelectedValue == "30") // mastercard
                                globalCollect.TipoProducto = "3";
                        }

                        try
                        {
                            globalCollectResponse = IkCajero.GlobalCollect_Pago(globalCollect, getDsSession());
                        }
                        catch (Exception err)
                        {
                            if (strIdTipoMoneda == "1" || strIdTipoMoneda == "2")
                            {
                                hdCnGlobalCollect.Value = "0";
                                hdCnGlobalCollectI.Value = "0";

                                // volvemos a mostrar los campos para ingresar datos de tarjeta
                                lblNumTarjeta.Visible = true;
                                txtNoTarjeta.Visible = true;
                                lblFechaExpira.Visible = true;
                                //txtMes.Visible = true;
                                //txtAnio.Visible = true;
                                ddlAnioExpira.Visible = true;
                                ddlMesExpira.Visible = true;
                                lblCVV.Visible = true;
                                txtCVV.Visible = true;
                                btnPagarFinal.Attributes.Remove("disabled");
                                btnPagarFinal.Enabled = true;
                                //strRespuestaBanco = lang.Map["PAY_NO_RESPUESTA_BANCO_" + strIdIdioma] + " " + globalCollectResponse.CodigoError + " " + globalCollectResponse.DescripcionError;
                                //strRespuestaBanco += "<br>" + lblRespuestaPagoGlobalCollect.Text;
                                //correoIntentoPago(strIdVenta, strFolios, "1", "", strRespuestaBanco);
                            }
                            else // si no muestra el error al cliente
                            {
                                lblNumTarjeta.Visible = false;
                                txtNoTarjeta.Visible = false;
                                lblFechaExpira.Visible = false;
                                // txtMes.Visible = false;
                                // txtAnio.Visible = false;
                                ddlAnioExpira.Visible = false;
                                ddlMesExpira.Visible = false;
                                lblCVV.Visible = false;
                                txtCVV.Visible = false;
                                //btnPagarFinal.Attributes.Remove("disabled");
                                //btnPagarFinal.Enabled = true;

                                string strginnermessage = "";
                                string strCuerpoCorreo = "BtnPagarFinal. <br> idVenta: " + strIdVenta + " <br> folios : " + strFolios + "<b>Global no regreso url y pago es distindo Pesos y dolares</b>";
                                lblRespuestaPago.Text = "Error método GlobalCollect_Pago. Pago es distindo Pesos y dolares";
                                //MensajeAlert(strCuerpoCorreo);
                                //correoIntentoPago(strIdVenta, strFolios, "1", "", strCuerpoCorreo);
                            }
                            //mensaje("GuardaPagoTarjeta GlobalCollect_Pago<br>cmbPromosionMSI:" + cmbPromosionMSI.SelectedItem.Value + "<br>hdIdPais:" + hdIdPais.Value, err);
                        }

                        // si todo bien hasta aqui
                        if (globalCollectResponse.CodigoError == "0")
                        {
                            //btnGuardarPago.Visible = false;
                            //btnGlobalCollect.Visible = true;
                            pnGlobalCollect.Visible = true;
                            upGC.Update();

                            lblRespuestaPagoGlobalCollect.Text = globalCollectResponse.OrderId;
                            hddIdOrdenGlobal.Value = globalCollectResponse.OrderId;
                            // validando que Global regrese URL
                            if (globalCollectResponse.URL.Trim().Length > 0)
                            {
                                ifrGlobalCollect.Attributes["src"] = globalCollectResponse.URL;
                                upGC.Update();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "myScript", "showGlobal();", true);
                                //UpdatePanelGlobalCollect.Update();
                            }
                            else
                            {
                                //lblMensajeError.Text = "Global Collect no regreso URL de cobro";
                                //mensaje(lblMensajeError.Text, null);
                                hdCnGlobalCollect.Value = "0";
                                hdCnGlobalCollectI.Value = "0";

                                // volvemos a mostrar los campos para ingresar datos de tarjeta
                                lblNumTarjeta.Visible = true;
                                txtNoTarjeta.Visible = true;
                                lblFechaExpira.Visible = true;
                                //txtMes.Visible = true;
                                //txtAnio.Visible = true;
                                ddlAnioExpira.Visible = true;
                                ddlMesExpira.Visible = true;
                                lblCVV.Visible = true;
                                txtCVV.Visible = true;
                                btnPagarFinal.Attributes.Remove("disabled");
                                btnPagarFinal.Enabled = true;
                            }
                            //cbGlobalCollect.JSProperties["cp_pagoGuargadoGlobalCollect"] = "0"; // bandera pago Globalcollect
                        }
                        else
                        {
                            hdCnGlobalCollect.Value = "0";
                            hdCnGlobalCollectI.Value = "0";

                            // volvemos a mostrar los campos para ingresar datos de tarjeta
                            lblNumTarjeta.Visible = true;
                            txtNoTarjeta.Visible = true;
                            lblFechaExpira.Visible = true;
                            //txtMes.Visible = true;
                            //txtAnio.Visible = true;
                            ddlAnioExpira.Visible = true;
                            ddlMesExpira.Visible = true;
                            lblCVV.Visible = true;
                            txtCVV.Visible = true;
                            btnPagarFinal.Attributes.Remove("disabled");
                            btnPagarFinal.Enabled = true;
                            //lblRespuestaPagoGlobalCollect.Text = globalCollectResponse.CodigoError + " " + globalCollectResponse.DescripcionError;
                            //UpdatePanelGlobalCollect.Update();
                        }
                    }
                    #endregion
                    break;
                case "3":
                    #region WORLDPAY
                    // se manda el pago a Worldpay

                    btnPagarFinal.Enabled = false;

                    pnGlobalCollect.Visible = true;

                    pnGlobalCollect.Visible = true;
                    //upGC.Update();
                    lblEtiGlobalCollect.Text = "World pay";
                    cResultadoGuardadoPago.strGlobalCollectActivo = "2";  // World pay

                    wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracionApp = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                    icConfiguracionApp.Open();
                    wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionURLWorldPay = icConfiguracionApp.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-URLWorldPay", cGlobals.dsSession);
                    icConfiguracionApp.Close();

                    string strCadena = "";
                    string strUrlComproPago = "";
                    string strDcTotalWorld = Math.Round(dcTotal, 2).ToString("N2");
                    strDcTotalWorld = strDcTotalWorld.Replace(",", "");

                    if (cConfiguracionURLWorldPay != null)
                        strUrlComproPago = cConfiguracionURLWorldPay.dsValor.Trim();



                    strCadena += "id=" + idVenta.ToString();
                    strCadena += "&base=" + ConfigurationManager.AppSettings["bdProduccion"];
                    strCadena += "&noAGV=" + "1";

                    if (intIdTipoMonedaPagar != 2) // diferente de Pesos
                    {
                        strCadena += "&amount=" + strDcTotalWorld;
                    }
                    else //pesos
                    {
                        strCadena += "&amount=" + strDcTotalWorld;
                    }

                    strCadena += "&session=" + cGlobals.dsSession;

                    strCadena += "&tNombre=" + txtNombreTH.Text.Trim(); //tNombre
                    strCadena += "&email=" + hdCorreoCompetidor.Value.Trim(); //a@b.com
                    strCadena += "&ipComprador=1.1.1.11"; //
                    strCadena += "&idPais=" + hdIdPais.Value; //1
                    strCadena += "&idEstado=" + hdIdEstado.Value; //1
                    strCadena += "&idCanalVenta=" + ConfigurationManager.AppSettings["idCanalVenta"].ToString();
                    strCadena += "&idTipoMoneda=" + strIdTipoMoneda; //1
                    strCadena += "&idFormaPago=" + ddlFormaPago.SelectedValue.Trim();
                    strCadena += "&noPagosMSI=0";//0
                    strCadena += "&idBanco=" + ddlBanco.SelectedValue.Trim();
                    strCadena += "&idIdioma=1"; // 1 - español, 2 ingles

                    string strAbsoluteUriWP = HttpContext.Current.Request.Url.AbsoluteUri;
                    string strHostWP = HttpContext.Current.Request.Url.Host;
                    string[] strspliturlWP = strAbsoluteUriWP.Split('/');
                    strAbsoluteUriWP = strAbsoluteUriWP.Replace(strspliturlWP[strspliturlWP.Length - 1], "");
                    strAbsoluteUriWP += "respuestaWorldPay.aspx";
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]) && strHostWP != "localhost")
                    {
                        if (strAbsoluteUriWP.Substring(0, 5) == "http:")
                            strAbsoluteUriWP = strAbsoluteUriWP.Replace("http:", "https:");
                    }

                    strCadena += "&respuestaURL=" + strAbsoluteUriWP; //{loquesea}

                    //Response.Redirect(strUrlComproPago + "?data=" + security.Encrypt(strCadena), false);
                    strCadenaEncriptada = security.Encrypt(strCadena);
                    ifrGlobalCollect.Attributes["src"] = strUrlComproPago + "?data=" + strCadenaEncriptada;
                    lblEtiGlobalCollect.ToolTip = strUrlComproPago + "?data=" + strCadenaEncriptada;
                    upGC.Update();
                    //string cadenaNoEncriptada = security.Decrypt(strCadenaEncriptada);

                    strIdEstatusPago = "1" /*Declinado*/;
                    cResultadoGuardadoPago.strIdEstatusPago = strIdEstatusPago;
                    #endregion WORLDPAY
                    break;

                case "4":
                    #region Ingenico

                    // temporal
                    strIdEstatusPago = "1" /*Declinado*/;
                    lblRespuestaPago.Text = "";
                    //strDsRespuesta = "error";

                    string strCadenaIngenico = "";
                    wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracionAp = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                    icConfiguracionAp.Open();

                    wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionUrlIngenico = icConfiguracionAp.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-UrlIngenico", cGlobals.dsSession);
                    wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionCodigosErrorGlobal = icConfiguracionAp.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-CodigosErrorGlobal", cGlobals.dsSession);

                    icConfiguracionAp.Close();

                    string strUrlIngenico = cConfiguracionUrlIngenico.dsValor.ToString();
                    List<string> lstCodigosErrorGlobal = cConfiguracionCodigosErrorGlobal.dsValor.Split(',').ToList();

                    /*strDcTotal = dcTotal.ToString("N2");
                    strDcTotal = strDcTotal.Replace(",", "");
                    strDcTotal = strDcTotal.ToString().Replace(".", "");*/
                    string strDcTotalIng = dcTotal.ToString("N2").Replace(".", "");// string strDcTotal = Math.Round(dcTotal, 2).ToString("N2");
                    strDcTotalIng = strDcTotalIng.Replace(",", "");

                    //string strDcTotaal = utils.fnRedondeaMonto(strDcTotalWorld);

                    string strAmount = strDcTotalIng;

                    //maxAgrupador = maxAgrupador + 1;
                    strCadenaIngenico += "id=" + idVenta.ToString();
                    strCadenaIngenico += "&locale=es_MX";
                    strCadenaIngenico += "&env=" + ConfigurationManager.AppSettings["bdProduccion"];
                    strCadenaIngenico += "&noAGV=" + maxAgrupador.ToString();

                    strCadenaIngenico += "&amount=" + strAmount; //strDcTotal;                    

                    strCadenaIngenico += "&cname=" + txtNombreTH.Text.Trim();
                    strCadenaIngenico += "&email=" + hdCorreoCompetidor.Value.Trim();
                    strCadenaIngenico += "&buyerip=10.10.0.0";
                    strCadenaIngenico += "&countryid=" + hdIdPais.Value;
                    strCadenaIngenico += "&stateid=" + hdIdEstado.Value;
                    //strCadenaIngenico += "&saleschannel=" + ConfigurationManager.AppSettings["idCanalVenta"].ToString();
                    strCadenaIngenico += "&saleschannel=11"; //para pruebas
                    strCadenaIngenico += "&cointypeid=" + strIdTipoMoneda;
                    strCadenaIngenico += "&paywayid=" + ddlFormaPago.SelectedItem.Value;
                    strCadenaIngenico += "&nmsi=0";
                    strCadenaIngenico += "&bankid=" + ddlBanco.SelectedItem.Value;
                    strCadenaIngenico += "&CardNumber=" + txtNoTarjeta.Text;
                    strCadenaIngenico += "&expiryDate=" + ddlMesExpira.SelectedItem.Value + "/" + ddlAnioExpira.SelectedItem.Value.Substring(ddlAnioExpira.SelectedItem.Value.Length - 2);
                    strCadenaIngenico += "&cvv=" + txtCVV.Text.Trim();
                    strCadenaIngenico += "&session=" + cGlobals.dsSession;
                    strCadenaIngenico += "&cnupgrade=true"; //  true - es upgrade solo guarda kpago y kpagotransaccion, false - es alta actualiza estatus, allotment, kventa, kventadetalle
                    strCadenaIngenico += "&returnurl=";
                    //strCadena += "&netpay=XXX

                    strCadenaEncriptada = security.Encrypt(strCadenaIngenico);

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(strUrlIngenico);
                    httpWebRequest.ContentType = "application/json; charset=utf-8";
                    httpWebRequest.Headers.Add("Authorization", "TBsmtsDRvL6JpElMq2zYrdVS0EazVjgvTmg180uxlYo=");
                    httpWebRequest.Method = "POST";

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string json = "{\"qsvars\":\"" + strCadenaEncriptada + "\"}";

                        streamWriter.Write(json);
                        streamWriter.Flush();
                    }

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    string strResulJson = "";
                    rootIngenico dtjson = new rootIngenico();
                    try
                    {
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            strResulJson = streamReader.ReadToEnd();
                        }
                        dtjson = JsonConvert.DeserializeObject<rootIngenico>(strResulJson);
                    }
                    catch (Exception ex)
                    {
                        //WebRoleProductos.XRM.utilXcrm.mensaje("Ingenico strResulJson: " + strResulJson, exc);
                        //result = utils.buscarCompetidor(getDsSession(), "", "", 0, 0, "", "", 0, 0, 0, intIdClienteContactoEvento);
                        string msg = ex.Message.ToString();
                        string strginnermessage = " Error intento Pago travesia por ingenico";
                        string strCuerpoCorreo = "Ingenico strResulJson: " + strResulJson + "<br>Error intento Pago travesia por ingenico." + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                        string strExcepcion = "";
                        if (ex.ToString().Length > 799)
                            strExcepcion = ex.ToString().Substring(0, 999);
                        else
                            strExcepcion = ex.ToString();
                        strCuerpoCorreo += strExcepcion;
                        MensajeAlert(strCuerpoCorreo);

                    }

                    if (dtjson.Payment == null)
                    {
                        strIdEstatusPago = "1" /*Declinado*/;
                        lblRespuestaPago.Text = "404 - error en la transacción";
                        strDsRespuesta = "404 - error en la transacción. Sin respuesta del servicio.";
                    }
                    else if (Int32.Parse(dtjson.Payment.StatusOutput.StatusCode.ToString()) >= 625 && !lstCodigosErrorGlobal.Contains(dtjson.Payment.StatusOutput.StatusCode.ToString()))
                    {
                        strIdEstatusPago = "2" /*Aceptado*/;
                        lblRespuestaPago.Text = "Pago aprobado";
                        strDsRespuesta = dtjson.Payment.StatusOutput.StatusCode.ToString();
                        if (dtjson.Payment.PaymentOutput.References.MerchantReference != null)
                        {
                            string[] arrbuscaIdPago = dtjson.Payment.PaymentOutput.References.MerchantReference.ToString().Split('-');
                            string[] arrbuscaIdPagoDos = arrbuscaIdPago[0].Split(',');
                            strIdPago = arrbuscaIdPagoDos[0];
                        }
                        if (dtjson.Payment.PaymentOutput.CardPaymentMethodSpecificOutput.AuthorisationCode != null)
                        {
                            if (dtjson.Payment.PaymentOutput.CardPaymentMethodSpecificOutput.AuthorisationCode.ToString().Trim().Length == 0) // si noy código de autorizacion se declina
                            {
                                strIdEstatusPago = "1" /*Declinado*/;
                                lblRespuestaPago.Text = "No hay código de Autorización";
                            }
                        }
                    }
                    if (dtjson.Payment != null)
                    {
                        if (dtjson.Payment.StatusOutput.Errors != null)
                        {
                            lblRespuestaPago.Text = dtjson.Payment.StatusOutput.Errors[0].Message.ToString();
                            strDsRespuesta = lblRespuestaPago.Text;
                        }
                    }
                    cResultadoGuardadoPago.strIdEstatusPago = strIdEstatusPago;
                    cResultadoGuardadoPago.strAfiliacion = "";
                    cResultadoGuardadoPago.strDsRespuesta = strDsRespuesta;
                    cResultadoGuardadoPago.strDsJustificacion = "";
                    cResultadoGuardadoPago.strDsReferencia = "";
                    cResultadoGuardadoPago.strDsTransaccion = "";
                    cResultadoGuardadoPago.strDsCorrelacion = "";
                    cResultadoGuardadoPago.strIdBancoReceptor = "";
                    cResultadoGuardadoPago.strIdPagoIngenico = strIdPago;
                    #endregion Ingenico
                    break;
                default:
                    break;
            }           
        }

        protected bool fnGuardarPago(int intIdCliente, int intIdVenta, decimal dcTotal, string idMoneda, int maxAgrupador)
        {
            int intIdTipoMoneda = 0;
            bool banderaPago = true;
            int intIdCanalVenta = int.Parse(ConfigurationManager.AppSettings["idCanalVenta"]);
            string totalPagar = dcTotal.ToString("N2");

            string strFolios = "";
            string strIdVenta = "";
            //string[] strIdVentas = hdIV.Value.Split(',');
            string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
            string dsIsoMoneda = selectedValueMoneda[1].ToString();
            // string idMoneda = selectedValueMoneda[0].ToString();            
            string idTipoMoneda = selectedValueMoneda[2].ToString();

            //for (int l = 0; l < (strIdVentas.Length - 1); l++)
            //{
            wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
            wsTransactions.kVenta laVenta = new wsTransactions.kVenta();
            if (cResultadoGuardadoPago.strIdPagoIngenico == null || cResultadoGuardadoPago.strIdPagoIngenico == "")
            {
                ikventa.Open();

                //laVenta = ikventa.SeleccionarkVentaPorIdVenta(intIdVenta, getDsSession());
                //wsTransactions.ListakVentaDetalles laVentaDetalles = ikventa.SeleccionarkVentasDetallePorIdVenta(int.Parse(strIdVentas[l].ToString()), getDsSession());
                ikventa.Close();
                intIdTipoMoneda = int.Parse(idTipoMoneda);
                maxAgrupador = maxAgrupador + 1;
                //------------------------------------------------------------------------------------------------
                // PAGO
                //------------------------------------------------------------------------------------------------

                wsTransactions.IkPagoClient IkPago = new wsTransactions.IkPagoClient();
                wsTransactions.ListakPagoTransacciones lstkPagoTransa = new wsTransactions.ListakPagoTransacciones();
                wsTransactions.kPagoTransaccion PagoTran = new wsTransactions.kPagoTransaccion();
                wsTransactions.kPago objkPago = new wsTransactions.kPago();

                objkPago.idPago = 0;
                objkPago.idVenta = intIdVenta;
                objkPago.idCanalPago = intIdCanalVenta; //11; // fijo ?
                objkPago.feTransaccionTotal = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
                objkPago.hrTransaccionTotal = DateTime.Now;
                //objkPago.mnTransaccionTotal = Math.Round(dcTotal, 2);
                //objkPago.mnTransaccionTotal = decimal.Parse(totalPagar);
                objkPago.mnTransaccionTotal = dcTotal;
                objkPago.cnEsPrepago = false;
                objkPago.noAgrupadorVenta = maxAgrupador;
                objkPago.idFormaPago = Int32.Parse(ddlFormaPago.SelectedItem.Value);
                objkPago.idEstatusPago = Int32.Parse(cResultadoGuardadoPago.strIdEstatusPago); //intIdEstatusPago;
                objkPago.feAlta = DateTime.Now;
                objkPago.idClienteUsuarioAlta = (cGlobals.idUsuario);

                PagoTran.idPago = objkPago.idPago; //
                // En idMoneda se guarda el IdTipoMoneda (Gabriel 20082013)
                PagoTran.idTipoMoneda = intIdTipoMoneda; //Int32.Parse(cmbMoneda.SelectedItem.Value);

                PagoTran.feTransaccion = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
                PagoTran.hrTransaccion = DateTime.Now;
                PagoTran.dsRespuesta = cResultadoGuardadoPago.strDsRespuesta; // strDsRespuesta;
                PagoTran.dsReferencia = cResultadoGuardadoPago.strDsReferencia; // strDsReferencia;
                PagoTran.dsJustificacion = cResultadoGuardadoPago.strDsJustificacion; // strDsJustificacion;

                //PagoTran.mnTransaccion = Math.Round(dcTotal, 2);
                //PagoTran.mnTransaccion = decimal.Parse(totalPagar);
                PagoTran.mnTransaccion = dcTotal;

                PagoTran.dsTransaccion = cResultadoGuardadoPago.strDsTransaccion; // strDsTransaccion;
                PagoTran.dsCorrelacion = cResultadoGuardadoPago.strDsCorrelacion; // strDsCorrelacion;
                PagoTran.noPagosMSI = 0;
                objkPago.cnPlanDePagos = false;

                PagoTran.cnPrepago = false;
                //if (intIdClienteTarjeta > 0)
                if (cResultadoGuardadoPago.strIdClienteTarjeta != null)
                    PagoTran.idClienteTarjeta = Int32.Parse(cResultadoGuardadoPago.strIdClienteTarjeta); // intIdClienteTarjeta;
                PagoTran.mnBancoComision = 0;
                PagoTran.prBancoComision = 0;
                PagoTran.feAlta = DateTime.Now;
                PagoTran.idClienteUsuarioAlta = (cGlobals.idUsuario);
                PagoTran.idPagoTransaccion = 0;
                PagoTran.idEstatusPago = Int32.Parse(cResultadoGuardadoPago.strIdEstatusPago);
                if (cResultadoGuardadoPago.strIdBancoReceptor != "")
                    PagoTran.idBancoReceptor = Int32.Parse(cResultadoGuardadoPago.strIdBancoReceptor);

                PagoTran.dsAfiliacion = cResultadoGuardadoPago.strAfiliacion;

                if (ddlBanco.SelectedItem.Value != "")
                    if (Int32.Parse(ddlBanco.SelectedItem.Value) > 0) // temporal en lo que se corrige la clase
                        PagoTran.idBanco = Int32.Parse(ddlBanco.SelectedItem.Value);

                lstkPagoTransa.Add(PagoTran);
                int intIdPago;
                try
                {
                    // 1 er intento
                    intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                }
                catch (Exception err)
                {
                    // mensaje("OE fallo primer intento, intIdTipoMoneda=" + intIdTipoMoneda.ToString() + strCadenaResultadoPagoTarjeta, err);
                    try
                    {
                        // 2 er intento
                        intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                    }
                    catch (Exception err2)
                    {
                        // 3 er intento
                        // mensaje("OE fallo segundo intento, intIdTipoMoneda=" + intIdTipoMoneda.ToString() + strCadenaResultadoPagoTarjeta, err);
                        intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                    }
                }
                IkPago.Close();
            }// fin del if (cResultadoGuardadoPago.strIdPagoIngenico == null || cResultadoGuardadoPago.strIdPagoIngenico == "")
            else
            {
                wsTransactions.IkPagoClient IkPago = new wsTransactions.IkPagoClient();
                wsTransactions.ListakPagoTransacciones lstkPagoTransa = new wsTransactions.ListakPagoTransacciones();
                wsTransactions.kPagoTransaccion PagoTran = new wsTransactions.kPagoTransaccion();
                wsTransactions.kPago objkPago = new wsTransactions.kPago();
                wsTransactions.ListakPagoTransacciones lstPagoTransacciones = new wsTransactions.ListakPagoTransacciones();
                IkPago.Open();
                objkPago = IkPago.SeleccionarkPagoPorIdPago(int.Parse(cResultadoGuardadoPago.strIdPagoIngenico), cGlobals.dsSession);
                objkPago.noAgrupadorVenta = maxAgrupador;
                lstPagoTransacciones = IkPago.SeleccionarkPagosTransaccionesPorIdPago(int.Parse(cResultadoGuardadoPago.strIdPagoIngenico), cGlobals.dsSession);
                int intIdPago = IkPago.ModificarkPago(objkPago, lstPagoTransacciones, cGlobals.dsSession);
                IkPago.Close();
            }               

            //if (intIdEstatusPago == 1) // Pago con tarjeta aprobada  --  2 /*Aceptado*/: 1 /*Declinado*/
            if (Int32.Parse(cResultadoGuardadoPago.strIdEstatusPago) == 1)
            {
                banderaPago = false;
            }

            return banderaPago;
        }

        protected void rblTipoTarjeta_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                /*
                 * si el valor es :
                 * 1 = visa
                 * 2 = mastercard
                 * 3 = amex   
                 * <option value="">Seleccionar Forma de pago</option>
	                <option value="36">American Express TPV Virtual</option>
	                <option value="28">Tarjeta Credito / Mastercard TPV Virtual</option>
	                <option value="27">Tarjeta Credito / Visa TPV Virtual</option>
	                <option value="30">Tarjeta Debito / Mastercard TPV Virtual</option>
	                <option value="29">Tarjeta Debito / Visa TPV Virtual</option>
                 * */
                string strTipoTarjeta = rblTipoTarjeta.SelectedValue.ToString();

                foreach (ListItem li in ddlFormaPago.Items)
                {
                    li.Enabled = true;
                }

                switch (strTipoTarjeta)
                {   // si el seleccionado es 1 tenemos que dejar solo visible en las formas de pago las que son de mastercard
                    case "1":
                        //ddlFormaPago.Items.FindByValue("36").Attributes.Add("style", "visibility: hidden");
                        //ddlFormaPago.Items.FindByValue("27").Attributes.Add("style", "visibility: hidden");
                        //ddlFormaPago.Items.FindByValue("29").Attributes.Add("style", "visibility: hidden");
                        foreach (ListItem item in this.ddlFormaPago.Items)
                        {
                            if (item.Value != "7" && item.Value != "18" && item.Value != "28" && item.Value != "30" && item.Value != "")
                            {
                                item.Enabled = false;
                            }
                        }
                        /*ddlFormaPago.Items.FindByValue("36").Enabled = false;
                        ddlFormaPago.Items.FindByValue("27").Enabled = false;
                        ddlFormaPago.Items.FindByValue("29").Enabled = false;*/
                        break;
                    // si el seleccionado es 2 tenemos que dejar solo visible en las formas de pago las que son de visa
                    case "2":
                        foreach (ListItem item in this.ddlFormaPago.Items)
                        {
                            if (item.Value != "6" && item.Value != "27" && item.Value != "17" && item.Value != "29" && item.Value != "")
                            {
                                item.Enabled = false;
                            }
                        }
                        /*ddlFormaPago.Items.FindByValue("36").Enabled = false;
                        ddlFormaPago.Items.FindByValue("28").Enabled = false;
                        ddlFormaPago.Items.FindByValue("30").Enabled = false;*/
                        break;
                    // si el seleccionado es 3 tenemos que dejar solo visible en las formas de pago las que son de amex
                    case "3":
                        foreach(ListItem item in this.ddlFormaPago.Items)
                        {
                            if (item.Value != "5" && item.Value != "36" && item.Value != "")
                            {
                                item.Enabled = false;
                            }
                        }
                        /*ddlFormaPago.Items.FindByValue("27").Enabled = false;
                        ddlFormaPago.Items.FindByValue("29").Enabled = false;
                        ddlFormaPago.Items.FindByValue("28").Enabled = false;
                        ddlFormaPago.Items.FindByValue("30").Enabled = false;*/
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {

            }

        }

        protected void ddlFormaPago_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                /*                   
                 * <option value="">Seleccionar Forma de pago</option>
	                <option value="36">American Express TPV Virtual</option>
	                <option value="28">Tarjeta Credito / Mastercard TPV Virtual</option>
	                <option value="27">Tarjeta Credito / Visa TPV Virtual</option>
	                <option value="30">Tarjeta Debito / Mastercard TPV Virtual</option>
	                <option value="29">Tarjeta Debito / Visa TPV Virtual</option>
                 * */
                string strIdFormaPago = ddlFormaPago.SelectedValue.ToString();
                bool cnValidaGlobal = true;
                lblNumTarjeta.Visible = false;
                txtNoTarjeta.Visible = false;
                lblFechaExpira.Visible = false;
                //txtMes.Visible = true;
                //txtAnio.Visible = true;
                ddlAnioExpira.Visible = false;
                ddlMesExpira.Visible = false;
                lblCVV.Visible = false;
                txtCVV.Visible = false;
                lblNombreTh.Visible = false;
                txtNombreTH.Visible = false;
                //txtApellidoTH.Visible = false;
                lblReferencia.Visible = true;
                txtReferencia.Visible = true;
                rvReferencia.Enabled = true;
                lblBanco.Visible = false;
                ddlBanco.Visible = false;

                rblTipoTarjeta.ClearSelection();
                ddlFormaPago.Enabled = true;

                ddlBanco.Attributes.Remove("readonly");
                ddlBanco.Attributes.Remove("disabled");
                ddlBanco.Items.FindByValue("35").Enabled = true;
                ddlBanco.SelectedIndex = -1;
                string strIdCanalVenta = "27";
                wscFormaPago.ListacFormaPagos lstFormaPagos;
                if (Session["lstFormaPagosAltaCotizacion" + strIdCanalVenta] == null)
                {
                    wscFormaPago.IcFormaPagoClient IcFormaPago = new wscFormaPago.IcFormaPagoClient();
                    IcFormaPago.Open();
                    lstFormaPagos = IcFormaPago.SeleccionarcFormaPagosPorIdCanalVenta(int.Parse(strIdCanalVenta), getDsSession());
                    Session["lstFormaPagosAltaCotizacion" + strIdCanalVenta] = lstFormaPagos;
                    IcFormaPago.Close();

                }
                else
                {
                    lstFormaPagos = (wscFormaPago.ListacFormaPagos)Session["lstFormaPagosAltaCotizacion" + strIdCanalVenta];
                }

                #region listFormaPagos
                for (int cnt = 0; cnt < lstFormaPagos.Count; cnt++)
                {
                    if (lstFormaPagos[cnt].idFormaPago.ToString() == ddlFormaPago.SelectedItem.Value) // idFormaPago
                    {
                        if (lstFormaPagos[cnt].idTipoPago == 2) // si es pago con tarjeta
                        {
                            lblBanco.Visible = true;
                            ddlBanco.Visible = true;
                            //rfvBanco.Enabled = true;
                            lblNumTarjeta.Visible = true;
                            txtNoTarjeta.Visible = true;
                            //PanelPago.Visible = true;
                            //rfvNoTarjeta16dig.Enabled = true;
                            //rfvNoTarjeta2.Enabled = true;
                            lblFechaExpira.Visible = true;
                            ddlAnioExpira.Visible = true;
                            ddlMesExpira.Visible = true;
                            lblCVV.Visible = true;
                            txtCVV.Visible = true;
                            lblNombreTh.Visible = true;
                            txtNombreTH.Visible = true;
                            //txtApellidoTH.Visible = true;

                            // Seria mejor si en la tabla cFormaPago se agrega la referencia al campo idTipoTarjeta de la tabla cTipoTarjeta
                            wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                            icConfiguracion.Open();
                            wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionFormaPagoVisa = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("BACKOFFICE_Visa_idFormaPago-idTipoTarjeta", getDsSession());
                            wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionFormaPagoMasterCard = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("BACKOFFICE_MasterCard_idFormaPago-idTipoTarjeta", getDsSession());
                            wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionFormaPagoAmex = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("BACKOFFICE_Amex_idFormaPago-idTipoTarjeta", getDsSession());

                            wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionFormaPagoDevitoMasterCard = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("BACKOFFICE_Dev_MasterCard_idFormaPago-idTipoTarjeta", getDsSession());
                            wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionFormaPagoDevitoVisa = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("BACKOFFICE_Dev_Visa_idFormaPago-idTipoTarjeta", getDsSession());


                            icConfiguracion.Close();

                            ddlBanco.Enabled = true;

                            //ddlFormaPago.Enabled = true;
                            ddlBanco.Items.FindByValue("35").Enabled = true;
                            //ddlFormaPago.Items.FindByValue("3").Enabled = true;
                            //  American Express
                            if (ddlFormaPago.SelectedItem.Value == "5" || ddlFormaPago.SelectedItem.Value == "36")
                            {
                                //ddlBanco.SelectedValue = "35";
                                ///ddlBanco.Enabled = false;
                                ddlBanco.SelectedIndex = -1;
                                rblTipoTarjeta.Items.FindByValue("3").Selected = true;
                                ddlBanco.Items.FindByValue("35").Selected = true;
                                ddlBanco.Attributes.Add("readonly", "readonly");
                                ddlBanco.Attributes.Add("disabled", "disabled");                               
                            }
                            else
                            {
                                ddlBanco.Items.FindByValue("35").Enabled = false;
                                // ddlFormaPago.Items.FindByValue("3").Enabled = false;

                                ddlBanco.SelectedIndex = -1;
                                //ddlFormaPago.SelectedIndex = -1;

                                if (ddlBanco.SelectedItem.Value == "48")
                                {
                                    // habilitaValidaciónLongitudTarjeta(3); // Diners Club
                                }
                                else
                                {
                                    //  habilitaValidaciónLongitudTarjeta(1); //  Normal
                                }
                            }
                            
                            if (lstFormaPagos[cnt].idTipoTarjeta.ToString().Length > 0)
                            {
                                //cmbTipoTarjeta.SelectedValue = lstFormaPagos[cnt].idTipoTarjeta.ToString();
                                //cmbTipoTarjeta.Enabled = false;
                                hdEsPagoTarjeta.Value = "2"; // Si es de tipo tarjera de inicio todos son terminal
                            }

                            string[] strVisaIdFormaPagoIdTipoTarjeta = cConfiguracionFormaPagoVisa.dsValor.Split('-');
                            string[] strMasterIdFormaPagoIdTipoTarjeta = cConfiguracionFormaPagoMasterCard.dsValor.Split('-');
                            string[] strAmexIdFormaPagoIdTipoTarjeta = cConfiguracionFormaPagoAmex.dsValor.Split('-');

                            string[] strMasterIdFormaPagoDevitoIdTipoTarjeta = cConfiguracionFormaPagoDevitoMasterCard.dsValor.Split('-');
                            string[] strVisaIdFormaPagoDevitoIdTipoTarjeta = cConfiguracionFormaPagoDevitoVisa.dsValor.Split('-');

                            if (ddlFormaPago.SelectedItem.Value == strVisaIdFormaPagoIdTipoTarjeta[0] || ddlFormaPago.SelectedItem.Value == strMasterIdFormaPagoIdTipoTarjeta[0] || ddlFormaPago.SelectedItem.Value == strAmexIdFormaPagoIdTipoTarjeta[0] || ddlFormaPago.SelectedItem.Value == strMasterIdFormaPagoDevitoIdTipoTarjeta[0] || ddlFormaPago.SelectedItem.Value == strVisaIdFormaPagoDevitoIdTipoTarjeta[0]) // Tipo de Tarjeta Credito: Visa o Master Lectora : Amex
                            {
                                lblBanco.Visible = true;
                                ddlBanco.Visible = true;
                                lblNumTarjeta.Visible = true;
                                txtNoTarjeta.Visible = true;
                                lblFechaExpira.Visible = true;                                
                                ddlAnioExpira.Visible = true;
                                ddlMesExpira.Visible = true;
                                lblCVV.Visible = true;
                                txtCVV.Visible = true;
                                lblReferencia.Visible = false;
                                txtReferencia.Visible = false;
                                rvReferencia.Enabled = false;
                                hdEsPagoTarjeta.Value = "1"; // Se cambia a tarjeta por que son por TPV Virtual

                                if (ddlFormaPago.SelectedItem.Value == strAmexIdFormaPagoIdTipoTarjeta[0]) //  Amex Tarjeta Credito
                                {
                                    //cmbTipoTarjeta.SelectedValue = strAmexIdFormaPagoIdTipoTarjeta[1];
                                    //cmbTipoTarjeta.Enabled = false;
                                    //rfvNoTarjeta15dig.Enabled = true;//  Amex Tarjeta Credito
                                    //rfvNoTarjeta16dig.Enabled = false;
                                    //rfvNoTarjeta14dig.Enabled = false;
                                    //habilitaValidaciónLongitudTarjeta(2); //  American Express
                                }
                                // -----------------------------------------------------------------------
                                // -----      Global collect
                                // -----------------------------------------------------------------------
                                //if (cConfigGlobalCollectActivo.dsValor == "1")  // Esta activo Global collect
                                //{                                ;
                                //validaCamposGlobal(string strIdMoneda, string idBanco);
                                //}
                            } // if Tipo de Tarjeta Credito
                        }// if idTipoPago == 2 Tarjeta
                        else //if (lstFormaPagos[cnt].idTipoPago == 1)
                        {                           
                            // se deshabilita el campo de referencia
                            List<int> lstFormaPagoEfectivo = new List<int>(new int[] { 1, 2, 3, 4 });
                            if (lstFormaPagoEfectivo.Contains(int.Parse(ddlFormaPago.SelectedItem.Value)))
                            {
                                lblReferencia.Visible = false;
                                txtReferencia.Visible = false;
                                rvReferencia.Enabled = false;
                                hdEsPagoTarjeta.Value = "3";
                            }
                        }
                        cnt = lstFormaPagos.Count;
                    }                    
                }// for
                #endregion listFormaPagos

                /*if (strIdFormaPago == "36" || strIdFormaPago == "5")
                {
                    ddlBanco.SelectedIndex = -1;
                    rblTipoTarjeta.Items.FindByValue("3").Selected = true;
                    ddlBanco.Items.FindByValue("35").Selected = true;
                    ddlBanco.Attributes.Add("readonly", "readonly");
                    ddlBanco.Attributes.Add("disabled", "disabled");
                    //ddlBanco.Enabled = false;
                }
                else*/ if (strIdFormaPago == "28" || strIdFormaPago == "30" || strIdFormaPago == "7" || strIdFormaPago == "18")
                {
                    rblTipoTarjeta.Items.FindByValue("1").Selected = true;
                    ddlBanco.Items.FindByValue("35").Enabled = false;
                }
                else if (strIdFormaPago == "27" || strIdFormaPago == "29" || strIdFormaPago == "6" || strIdFormaPago == "17")
                {
                    rblTipoTarjeta.Items.FindByValue("2").Selected = true;
                    ddlBanco.Items.FindByValue("35").Enabled = false;
                }
                string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
                string dsIsoMoneda = selectedValueMoneda[1].ToString();
                string idMoneda = selectedValueMoneda[0].ToString();
                string idBanco = ddlBanco.SelectedValue.ToString();
                string idFormaPago = ddlFormaPago.SelectedValue.ToString();

                // formas de pago de deposito o cortesía
                if (strIdFormaPago == "21" || strIdFormaPago == "22" || strIdFormaPago == "23" || strIdFormaPago == "24" || strIdFormaPago == "14")                
                {
                    lblReferencia.Visible = true;
                    txtReferencia.Visible = true;
                    cnValidaGlobal = false;
                    hdEsPagoTarjeta.Value = "3";
                }
                // si son terminal
                if (strIdFormaPago == "5" || strIdFormaPago == "50" || strIdFormaPago == "7" || strIdFormaPago == "6" || strIdFormaPago == "18" || strIdFormaPago == "17")
                {
                    /*lblFechaExpira.Visible = false;
                    ddlAnioExpira.Visible = false;
                    ddlMesExpira.Visible = false;*/
                    lblCVV.Visible = false;
                    txtCVV.Visible = false;
                    /*lblReferencia.Visible = true;
                    txtReferencia.Visible = true;*/
                    cnValidaGlobal = false;
                }
                hdCnGlobalCollect.Value = "0";
                hdCnGlobalCollectI.Value = "0";
                if (idBanco != "" && idBanco != "35" && idFormaPago != "" && idFormaPago != "36" && cnValidaGlobal)
                    validaCamposGlobal(idMoneda, idBanco);
            }
            catch (Exception ex)
            {
            }
        }

        protected void ddlBanco_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
            string strIdFormaPago = ddlFormaPago.SelectedValue.ToString();
            string dsIsoMoneda = selectedValueMoneda[1].ToString();
            string idMoneda = selectedValueMoneda[0].ToString();
            string idBanco = ddlBanco.SelectedValue.ToString();
            hdCnGlobalCollect.Value = "0";
            hdCnGlobalCollectI.Value = "0";
            // si son terminal no valides global
            if (strIdFormaPago != "5" && strIdFormaPago != "50" && strIdFormaPago != "7" && strIdFormaPago != "6" && strIdFormaPago != "18" && strIdFormaPago != "17")
            {
                validaCamposGlobal(idMoneda, idBanco);
            }
            
        }

        protected void validaCamposGlobal(string strIdMoneda, string idBanco)
        {
            try
            {
                DataSet dsConfiguracionPagosGC;
                string strIdTipoMoneda = "";
                //string strIdCanalVenta = ConfigurationManager.AppSettings["idCanalVenta"].ToString();
                string strIdCanalVenta = "11";
                bool blBanderaGlobalCollect = false;
                bool blBanderaGlobalCollectInternationalLove = false;

                lblNumTarjeta.Visible = true;
                txtNoTarjeta.Visible = true;
                lblFechaExpira.Visible = true;
                //txtMes.Visible = true;
                //txtAnio.Visible = true;
                ddlAnioExpira.Visible = true;
                ddlMesExpira.Visible = true;
                lblCVV.Visible = true;


                wscMoneda.IcMonedaClient IcMoneda = new wscMoneda.IcMonedaClient();
                IcMoneda.Open();
                wscMoneda.ListacMonedas lstMonedas = IcMoneda.SeleccionarcMonedas(getDsSession());
                IcMoneda.Close();

                hdCnGlobalCollectI.Value = "0";
                hdCnGlobalCollect.Value = "0";
                bool blBanderaIngenico = false;
                bool blBanderaWorldpay = false;

                for (int cntMoneda = 0; cntMoneda < lstMonedas.Count; cntMoneda++)
                {
                    if ((int)lstMonedas[cntMoneda].idMoneda == Int32.Parse(strIdMoneda))
                    {
                        strIdTipoMoneda = lstMonedas[cntMoneda].idTipoMoneda.ToString();
                    }
                }
                
                string strPromosionMSI = "0";
                wsBusinessRules.BusinessRulesServicesClient ruleOper = new wsBusinessRules.BusinessRulesServicesClient();
                Dictionary<string, string> dicret = new Dictionary<string, string>();
                dicret.Add("@TIPOCONSULTA", "VALIDA");
                dicret.Add("@idCanalVenta", strIdCanalVenta);
                dicret.Add("@idTipoMoneda", strIdTipoMoneda);
                dicret.Add("@idBanco", ddlBanco.SelectedItem.Value);
                dicret.Add("@noPagosMSI", strPromosionMSI); 
                dsConfiguracionPagosGC = ruleOper.ExecuteRule("spGETcConfiguracionPagosGC", dicret, getDsSession());
                Session["cConfiguracionPagosGC" + strIdTipoMoneda] = dsConfiguracionPagosGC;
                ruleOper.Close();
                /*if (dsConfiguracionPagosGC.Tables[0].Rows[0]["RetValue"].ToString() == "1")
                {
                    //string strPromosionMSI = (cmbPromosionMSI.SelectedItem.Value == "1") ? "0" : cmbPromosionMSI.SelectedItem.Value;
                   
                    DataRow[] dtArrComponentes = dsConfiguracionPagosGC.Tables[0].Select("idBanco=" + idBanco + " and idTipoMoneda=" + strIdTipoMoneda + " and idCanalVenta=" + strIdCanalVenta + " and noPagosMSI=" + strPromosionMSI);

                    if (dtArrComponentes != null && dtArrComponentes.Length > 0)
                    {
                        blBanderaGlobalCollect = true;
                        hdCnGlobalCollect.Value = "1";
                        if (dtArrComponentes[0]["cnInternacional"].ToString() == "True")
                        {
                            blBanderaGlobalCollectInternationalLove = true;
                            hdCnGlobalCollectI.Value = "1";
                            //lblEtiGlobalCollect.Text = "Global Collect Internacional";
                        }
                    }
                }*/
                if (dsConfiguracionPagosGC.Tables[0].Rows[0]["RetValue"].ToString() == "1")
                {
                    //string strPromosionMSI = (cmbPromosionMSI.SelectedItem.Value == "1") ? "0" : cmbPromosionMSI.SelectedItem.Value;
                    hddPagoGateWay.Value = "1"; // Tarjetas (Amex, Bancomer, Banamex)                    
                    DataRow[] dtArrComponentes = dsConfiguracionPagosGC.Tables[0].Select("idBanco=" + idBanco + " and idTipoMoneda=" + strIdTipoMoneda + " and idCanalVenta=" + strIdCanalVenta + " and noPagosMSI=" + strPromosionMSI);
                    wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                    icConfiguracion.Open();
                    wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionIngenicoActivo = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-IngenicoActivo", getDsSession());
                    icConfiguracion.Close();
                    switch (dtArrComponentes[0]["dsGateway"].ToString())
                    {
                        case "GLOBAL COLLECT / INGENICO":                           
                            if (cConfiguracionIngenicoActivo != null)
                            {
                                if (cConfiguracionIngenicoActivo.dsValor == "0")
                                {
                                    blBanderaGlobalCollect = true;
                                    hdCnGlobalCollect.Value = "1";
                                    lblEtiGlobalCollect.Text = "Global Collect";
                                    if (dtArrComponentes[0]["cnInternacional"].ToString() == "True")
                                    {
                                        hdCnGlobalCollectI.Value = "1";
                                        lblEtiGlobalCollect.Text = "Global Collect Internacional";
                                    }
                                    hddPagoGateWay.Value = "2"; // GLOBALCOLLECT
                                }
                                else
                                {
                                    blBanderaIngenico = true;
                                    hddPagoGateWay.Value = "4"; // Ingenico
                                    //hddPagoGateWay.Value = "3"; // WORLD PAY Para pruebas
                                    //blBanderaWorldpay = true;
                                }
                            }
                            break;
                        case "WORLDPAY":
                            /*hddPagoWorldPay.Value = "1";
                            blBanderaWorldpay = true;
                            lblEtiGlobalCollect.Text = "WORLD PAY";
                            hddPagoGateWay.Value = "3"; // WORLD PAY    */
                            if (cConfiguracionIngenicoActivo.dsValor == "0")
                            {
                                blBanderaGlobalCollect = true;
                                hdCnGlobalCollect.Value = "1";
                                lblEtiGlobalCollect.Text = "Global Collect";
                                if (dtArrComponentes[0]["cnInternacional"].ToString() == "True")
                                {
                                    hdCnGlobalCollectI.Value = "1";
                                    lblEtiGlobalCollect.Text = "Global Collect Internacional";
                                }
                                hddPagoGateWay.Value = "2"; // GLOBALCOLLECT
                            }
                            else
                            {
                                //blBanderaIngenico = true;
                                hddPagoGateWay.Value = "4"; // Ingenico
                            }       
                            break;
                        default:
                            hddPagoGateWay.Value = "1"; // Tarjetas (Amex, Bancomer, Banamex)
                            break;
                    }
                }
                else
                {
                    hddPagoGateWay.Value = "1";  // Tarjetas (Amex, Bancomer, Banamex)
                }
                if (blBanderaGlobalCollect)
                {
                    lblNumTarjeta.Visible = false;
                    txtNoTarjeta.Visible = false;
                    lblFechaExpira.Visible = false;
                    // txtMes.Visible = false;
                    // txtAnio.Visible = false;
                    ddlAnioExpira.Visible = false;
                    ddlMesExpira.Visible = false;
                    lblCVV.Visible = false;
                    txtCVV.Visible = false;
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnGlobalCollect_Click(object sender, EventArgs e)
        {
            try
            {
                if (hddResultadoPagoGlobalCollect.Value.Trim().Length > 0)
                {
                    String[] strParametros = hddResultadoPagoGlobalCollect.Value.Split('|');
                    int intIdClienteContactoEvento = int.Parse(hdIdClienteContactoEventoPrincipales.Value);
                    string strIdEstatusPago = "2";  /*Aceptado*/
                    string strCodigoError = strParametros[0];
                    string strDescripcionError = strParametros[1];
                    string strCodigoAutorizacion = strParametros[2];
                    string strNumeroTarjeta = strParametros[3];
                    int intRespIdVenta = int.Parse(hdIdVenta.Value.ToString());
                    string strFolios = "";
                    string strIdVenta = hdIdVenta.Value;
                    //string[] strIdVentas = hdIV.Value.Split(',');
                    decimal dcTotal = decimal.Parse(hdMontoDiferencia.Value);
                    int intIdVentaDetalle = (hdIdVentaDetalle.Value != "") ? int.Parse(hdIdVentaDetalle.Value) : 0;

                    DataTable dtCompetidor = (DataTable)Session["cCompetidoresBusqueda"];
                    DataRow[] drClienteEvento = dtCompetidor.Select("idClienteContactoEvento = " + intIdClienteContactoEvento);

                    int idProductoPrecioNuevo = int.Parse(drClienteEvento[0]["idProductoPrecioNuevo"].ToString());
                    int idEventoClasificacionNuevo = int.Parse(drClienteEvento[0]["idEventoClasificacionNuevo"].ToString());
                    int feVisitaNuevo = int.Parse(drClienteEvento[0]["feVisitaNuevo"].ToString());
                    decimal dcPrecioListaNuevo = decimal.Parse(drClienteEvento[0]["precioListaNuevo"].ToString());
                    decimal dcMnTipoCambioNuevo = decimal.Parse(drClienteEvento[0]["mnTipoCambioNuevo"].ToString());
                    int idTipoClienteNuevo = int.Parse(drClienteEvento[0]["idTipoClienteNuevo"].ToString());
                    int idTipoMonedaNuevo = int.Parse(drClienteEvento[0]["idTipoMonedaNuevo"].ToString());
                    int intIdLocacionNuevo = int.Parse(drClienteEvento[0]["idLocacionNuevo"].ToString());
                    string strDsCorrelacion = strParametros[6]; // no de referencia

                    string strIdBancoReceptor = "32";	// Banorte

                    if (hdCnGlobalCollectI.Value != "0")
                    {
                        strIdBancoReceptor = "112"; // GlobalCollect Internacional
                    }

                    int intCodigoErrorGlobal = 0;
                    if (Int32.TryParse(strCodigoError.Trim(), out intCodigoErrorGlobal))
                    {
                        if (intCodigoErrorGlobal >= 625)
                        {
                            strIdEstatusPago = "2" /*Aceptado*/;
                        }
                        else
                        {
                            strIdEstatusPago = "1" /*Declinado*/;
                            btnPagarFinal.Visible = true;
                        }
                    }

                    cResultadoGuardadoPago.strIdEstatusPago = strIdEstatusPago;
                    //cResultadoGuardadoPago.strAfiliacion = (amexresponse.dsAfiliacion != null) ? amexresponse.dsAfiliacion : "";
                    cResultadoGuardadoPago.strDsRespuesta = strDescripcionError;
                    cResultadoGuardadoPago.strDsJustificacion = strDescripcionError;
                    cResultadoGuardadoPago.strDsReferencia = strDescripcionError;
                    cResultadoGuardadoPago.strDsTransaccion = strCodigoAutorizacion;
                    cResultadoGuardadoPago.strDsCorrelacion = strDsCorrelacion;
                    cResultadoGuardadoPago.strIdBancoReceptor = strIdBancoReceptor;

                    int intIdCliente = cGlobals.idUsuario;                    
                    //                string idMoneda = ddlCambioMoneda.SelectedValue.ToString();
                    string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
                    string dsIsoMoneda = selectedValueMoneda[1].ToString();
                    string idMoneda = selectedValueMoneda[0].ToString();
                    int respActualizaCompetidor = 0;
                    fnGuardaDatosTarjeta(intIdCliente, dcTotal, idMoneda);
                    int maxAgrupador = getMaxAgrupador(dtCompetidor);

                    bool respGuardaPago = fnGuardarPago(intIdCliente, int.Parse(strIdVenta), dcTotal, idMoneda, maxAgrupador);

                    if (respGuardaPago)
                    {
                        if (cResultadoGuardadoPago.strIdEstatusPago == "2")
                        {
                            int resultActualizar = fnUpgradeCompetidor(intIdCliente, dcTotal, idMoneda, int.Parse(strIdVenta), maxAgrupador, intIdVentaDetalle, idProductoPrecioNuevo, intIdClienteContactoEvento, idEventoClasificacionNuevo, feVisitaNuevo, dcPrecioListaNuevo, dcMnTipoCambioNuevo, idTipoClienteNuevo, idTipoMonedaNuevo, intIdLocacionNuevo);
                            //int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                            // si el resultado es mayor a cero quiere decir que actualizo
                            if (resultActualizar > 0)
                            {
                                string strBaseDatos = (ConfigurationManager.AppSettings["bdProduccion"]);
                                //lblFolio.Visible = true;
                                lnkFolio.Visible = true;
                                lnkFolio.NavigateUrl = "https://www.aolxcaret.com/core/cupon/cuponnew.aspx?id=" + int.Parse(strIdVenta) + "&base=" + strBaseDatos + "&lan=ES_MX";                               
                            }
                        }
                        else
                        {

                        }
                    }                                   
                }
            }
            catch (Exception ex)
            {

            }
        }      

        protected void ddlAnioExpira_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnGenerarLink_Click(object sender, EventArgs e)
        {
            try
            {
                int intIdClienteContactoEvento = int.Parse(hdIdClienteContactoEventoPrincipales.Value);                
                string strAbsoluteUri = HttpContext.Current.Request.Url.AbsoluteUri;
                string strHost = HttpContext.Current.Request.Url.Host;
                string[] strspliturl = strAbsoluteUri.Split('/');
                strAbsoluteUri = strAbsoluteUri.Replace(strspliturl[strspliturl.Length - 1], "");
                strAbsoluteUri = strAbsoluteUri.Replace(strspliturl[strspliturl.Length - 2] + "/", "");
                strAbsoluteUri += "cambioCategoria.aspx?contEvent=" + intIdClienteContactoEvento.ToString();

                string strPrimerosCaracteres = strAbsoluteUri.Substring(0, 5);
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]) && strHost != "localhost")
                {
                    if (strPrimerosCaracteres == "http:")
                        strAbsoluteUri = strAbsoluteUri.Replace("http", "https");
                    else if (strPrimerosCaracteres != "https")
                        strAbsoluteUri.Trim().Insert(0, "https://");
                }

                lnkCambioCategoria.Visible = true;
                lnkCambioCategoria.Text = strAbsoluteUri;
                lnkCambioCategoria.NavigateUrl = strAbsoluteUri;
            }
            catch (Exception ex)
            { 
            }
        }

        protected void ddlCambioMoneda_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int intIdClienteContactoEvento = int.Parse(hdIdClienteContactoEventoPrincipales.Value);
                int intIdClienteContacto = int.Parse(hdIdClienteContactoPrincipales.Value);
                int intIdProducto = int.Parse(hdIdProductoOriginal.Value);
                int intIdVentaDetalle = (hdIdVentaDetalle.Value != "") ? int.Parse(hdIdVentaDetalle.Value) : 0;
                int intIdVenta = (hdIdVenta.Value != "") ? int.Parse(hdIdVenta.Value) : 0;

                DataTable dtCompetidor = (DataTable)Session["cCompetidoresBusqueda"];
                DataRow[] drClienteEvento = dtCompetidor.Select("idClienteContactoEvento = " + intIdClienteContactoEvento);

                string dsCodigoOrinal = drClienteEvento[0]["dsCodigo"].ToString();
                string dsCodigoNuevo = drClienteEvento[0]["dsCodigoNuevo"].ToString();
                int idEventoClasificacionNuevo = int.Parse(drClienteEvento[0]["idEventoClasificacionNuevo"].ToString());
                string strIdVenta = "";
                strIdVenta = drClienteEvento[0]["idVenta"].ToString();
                string feNacimientoOriginal = "";
                feNacimientoOriginal = drClienteEvento[0]["feNacimiento"].ToString();
                int idTipoMonedaNuevo = (drClienteEvento[0]["idTipoMonedaNuevo"].ToString() != "")? int.Parse(drClienteEvento[0]["idTipoMonedaNuevo"].ToString()) : 0;
                string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
                string dsIsoMoneda = selectedValueMoneda[1].ToString();
                string idMonedaNuevo = selectedValueMoneda[2].ToString();
                int intIdMonedaNuevo = int.Parse(selectedValueMoneda[2].ToString());

                DataSet resultPrecioOrinal = utils.getPrecio(getDsSession(),141, dsCodigoOrinal, 1, false, dsIsoMoneda);
                DataSet resultPrecioNuevo = utils.getPrecio(getDsSession(), 141,dsCodigoNuevo, 1, false, dsIsoMoneda);

                int idMoneda = int.Parse(resultPrecioNuevo.Tables[0].Rows[0]["idMoneda_2"].ToString());

                decimal dcDiferencia = 0;
                
                // si es mayor a 4, entonces es moneda internacional
                if (intIdMonedaNuevo > 4)
                {
                    dcDiferencia = (decimal)resultPrecioNuevo.Tables[0].Rows[0]["mnPrecio_MS"] - (decimal)resultPrecioOrinal.Tables[0].Rows[0]["mnPrecio_MS"];
                }
                else
                {
                    dcDiferencia = (decimal)resultPrecioNuevo.Tables[0].Rows[0]["mnPrecio_" + idMonedaNuevo] - (decimal)resultPrecioOrinal.Tables[0].Rows[0]["mnPrecio_" + idMonedaNuevo];
                }


                
                if (dcDiferencia > 0)
                {
                    divPago.Visible = true;
                    lblMensajeDiferencia.Text = "Diferencia de pago por cambio de sede por: " + dcDiferencia.ToString("N2");
                }
            }
            catch (Exception ex)
            {
 
            }
        }

        protected void btnContinuar_Click(object sender, EventArgs e)
        {

        }

        protected void getFiles(string idClienteContactoEvento) 
        {
            try
            {
                string url = "https://www.xperienciasxcaret.mx/core/archivos/Travesia/Storage/files/" + idClienteContactoEvento +"/";
                //string url = "http://ServerDirPath/";
                string fullPath = "";
               // fullPath = Path.GetFullPath(url);
                //fullPath = Server.MapPath(url);
                //string baseURL = "http://download.example.org/export/dump/";
                WebClient client = new WebClient();
                //string content = client.DownloadString(url);
                /*using (var webClient = new WebClient())
                {
                    webClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36");
                    var response = webClient.DownloadString(url);                    
                }*/
                //var files = Directory.GetFiles(url);l
                wsGeneralServices.GeneralServicesClient gralCliente = new wsGeneralServices.GeneralServicesClient();
                wsGeneralServices.ListaArchivo lista = new wsGeneralServices.ListaArchivo();
                lista = gralCliente.ObtenerListaArchivo(idClienteContactoEvento.ToString(), getDsSession());
                int intContMatch = 0;
                string htmlBodyTable = "";
                string[] arryNombreArchivo = null;
                foreach(var listArchivos in lista)
                {
                    arryNombreArchivo = listArchivos.nombreArchivo.Split('.');
                    if ((intContMatch % 2) > 0)
                        htmlBodyTable += "<tr>";
                    else
                        htmlBodyTable += "<tr class='alt'>";
                    if (arryNombreArchivo[(arryNombreArchivo.Length - 1)].ToString().ToUpper() != "PDF")
                        htmlBodyTable += "<td><div class='imagenes'> <img src=" + url + listArchivos.nombreArchivo + " /></div></td>";
                    else
                        htmlBodyTable += "<td><div class='imagenes'> <img src='../img/pdf-icon.png' /></div></td>";
                    htmlBodyTable += "<td>" + listArchivos.nombreArchivo + "</td>";
                    htmlBodyTable += "<td>" + listArchivos.ultimaModificacion + "</td>";
                    //htmlBodyTable += "<td><a href='#' url='" + url + fileName + "' filename = '" + fileName + "' class= 'downlaodFile' style='width: 100px;height: 100px;overflow: hidden;'><img src='../img/download2.png' /></a></td>";
                    htmlBodyTable += "<td><asp:Button class= 'downlaodFile' url='" + url + listArchivos.nombreArchivo + "' filename = '" + listArchivos.nombreArchivo + "' runat='server' BorderStyle='None' CausesValidation='False' EnableTheming='False'  style='cursor: pointer;'><img src='../img/download2.png' /></button>";
                    htmlBodyTable += "</tr>";
                    intContMatch = intContMatch + 1;
                }
                #region parteviejaarchivos
                /*HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                //fullPath = request.PhysicalPath;              
                request.UserAgent = "Code Sample Web Client";
                // request.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36";
                //request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                // if we have a proxy set its creds as well
                /*if (request.Proxy != null)
                {
                    request.Proxy.Credentials = CredentialCache.DefaultCredentials;
                }*/
                //request.Credentials = new NetworkCredential("APPLICATION\\usexpemx", "sIy?4f12");
                //request.Accept = "*/*";
                //request.Accept = "text/html";
                /*using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {                    
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string html = reader.ReadToEnd();                        
                        Regex regex = new Regex(GetDirectoryListingRegexForUrl(url));
                        MatchCollection matches = regex.Matches(html);
                        if (matches.Count > 0)
                        {
                            foreach (Match match in matches)
                            {                                
                                // vamos a ignorar el primer match, ya que el primero es la raiz
                                if (intContMatch > 0)
                                {
                                    if (match.Success)
                                    {
                                        string[] arrMatches = match.ToString().Trim().Replace("\"", "").Split('/');
                                        int intSizeArrayMatches = arrMatches.Length;
                                        string fileName = arrMatches[intSizeArrayMatches - 1].ToString().Trim();
                                        var extension = Path.GetExtension(fileName).ToLower();
                                        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url + fileName);
                                        HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                                        DateTime lastModification = res.LastModified;                                        
                                        // AQUI ES DONDE SE VA A CONSTRUIR EL CUERPO DE LA TABLA DE ARCHIVOS DINAMICAMENTE
                                        if ((intContMatch % 2) > 0)
                                            htmlBodyTable += "<tr>";
                                        else
                                            htmlBodyTable += "<tr class='alt'>";
                                        if (extension != ".pdf")
                                            htmlBodyTable += "<td><div class='imagenes'> <img src=" + url + fileName + " /></div></td>";
                                        else
                                            htmlBodyTable += "<td><div class='imagenes'> <img src='../img/pdf-icon.png' /></div></td>";
                                        htmlBodyTable += "<td>" + fileName + "</td>";
                                        htmlBodyTable += "<td>" + lastModification.ToString() +"</td>";
                                        //htmlBodyTable += "<td><a href='#' url='" + url + fileName + "' filename = '" + fileName + "' class= 'downlaodFile' style='width: 100px;height: 100px;overflow: hidden;'><img src='../img/download2.png' /></a></td>";
                                        htmlBodyTable += "<td><asp:Button class= 'downlaodFile' url='" + url + fileName + "' filename = '" + fileName + "' runat='server' BorderStyle='None' CausesValidation='False' EnableTheming='False'  style='cursor: pointer;'><img src='../img/download2.png' /></button>";
                                        htmlBodyTable += "</tr>";
                                    }
                                }
                                intContMatch = intContMatch + 1;
                            }
                        }                        
                    }
                }//  fin using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                //tbodyFiles.InnerHtml = htmlBodyTable.Trim();
               // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>fnFillTableFiles('" + htmlBodyTable.Trim() + "');</script>", false);*/
                #endregion
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>fnFillTableFiles(\"" + htmlBodyTable.Trim() + "\");</script>", false);
            }
            catch (Exception ex)
            {
 
            }
        }

        public static void Download(string sFileName, string sFilePath)
        {
            HttpContext.Current.Response.ContentType = "APPLICATION/OCTET-STREAM";
            String Header = "Attachment; Filename=" + sFileName;
            HttpContext.Current.Response.AppendHeader("Content-Disposition", Header);
            System.IO.FileInfo Dfile = new System.IO.FileInfo(HttpContext.Current.Server.MapPath(sFilePath));
            HttpContext.Current.Response.WriteFile(Dfile.FullName);
            HttpContext.Current.Response.End();
        }

        protected void donwloadFile(string url, string fileName)
        {
            //Button btn = (Button)sender;
            //Create a stream for the file
            Stream stream = null;
            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 30000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];
            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(url);
                //File Path and File Name                
                string _DownloadableProductFileName = fileName;

                //Create a response for this request
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileResp.ContentLength > 0)
                {
                    //fileResp.ContentLength = fileReq.ContentLength;
                    //Get the Stream returned from the response
                    stream = fileResp.GetResponseStream();

                    // prepare the response to the client. resp is the client Response
                    var resp = HttpContext.Current.Response;

                    //Indicate the type of data being sent
                    resp.ContentType = "application/octet-stream";

                    //Name the file 
                    resp.AddHeader("Content-Disposition", "attachment; filename=\"" + _DownloadableProductFileName + "\"");
                    resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                    int length;
                    do
                    {
                        // Verify that the client is connected.
                        if (resp.IsClientConnected)
                        {
                            // Read data into the buffer.
                            length = stream.Read(buffer, 0, bytesToRead);

                            // and write it out to the response's output stream
                            resp.OutputStream.Write(buffer, 0, length);

                            // Flush the data
                            resp.Flush();

                            //Clear the buffer
                            buffer = new Byte[bytesToRead];
                        }
                        else
                        {
                            // cancel the download if client has disconnected
                            length = -1;
                        }
                    } while (length > 0); //Repeat until no data is read

                }
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }

        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.LoginU("rhernandezmi", "eXperiencias");                
            }
            return cGlobals.dsSession; /* cambiar*/
        }

        public static string GetDirectoryListingRegexForUrl(string url)
        {
            //if (url.Equals("https://www.xperienciasxcaret.mx/core/archivos/Travesia/Storage/files/17500/"))
            //{
                //return "<a href=\".*\">(?<name>.*)</a>";
                return "\\\"([^\"]*)\\\"";
            //}
            //throw new NotSupportedException();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void btnGenerarPdf_Click(object sender, EventArgs e)
        {                        

        }

        public void MensajeAlert(string errMensaje)
        {
            wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();
            iGeneral.Open();
            string strCorreos = "nfigueroa@experienciasxcaret.com.mx";
            bool blnBanderaCorreo = iGeneral.SendEmailTicket(strCorreos, "Error en Aplicacion Travesía " + DateTime.Now.Year.ToString() + " ", errMensaje, true /*es html*/, "", false, 0, "");
            iGeneral.Close();
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>console.log('" + errMensaje + "');", false);
        } 
    }
}