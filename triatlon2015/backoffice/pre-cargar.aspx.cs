﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace travesia.backoffice
{
    public partial class pre_asignar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = true;
            string parameter = Request["__EVENTARGUMENT"];
            if (parameter == "btnContinuarA_Click")
                btnBuscar_Click(sender, e);
            if (!IsPostBack)
            {
                utils.getClientes(33,ddlClienteFacturar);
                //utils.llenaAnio(ddanio);                
            }
        }

        protected void rblFormaPago_SelectedIndexChanged(object sender, EventArgs e)
        {
            // si es dos quiere decir que la forma de pago va a ser c xc
            if (rblFormaPago.SelectedValue.ToString() == "2")
            {
             //   updClienteFacturar.Visible = true;
                lblClienteFacturar.Visible = true;
                ddlClienteFacturar.Visible = true;
            }
            else
            {
               // updClienteFacturar.Visible = false;
                lblClienteFacturar.Visible = false;
                ddlClienteFacturar.Visible = false;
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                {
                    string strFormaPago = rblFormaPago.SelectedValue.ToString();
                    string loc = "";
                    // = HttpContext.Current.Request.ApplicationPath + "/NewDestinationPage.aspx";
                    //Response.Redirect(Request.Url.AbsoluteUri);
                    string strAbsoluteUri = HttpContext.Current.Request.Url.AbsoluteUri;
                    string strHost = HttpContext.Current.Request.Url.Host;
                    string[] strspliturl = strAbsoluteUri.Split('/');
                    strAbsoluteUri = strAbsoluteUri.Replace(strspliturl[strspliturl.Length - 1], "");
                    strAbsoluteUri = strAbsoluteUri.Replace(strspliturl[strspliturl.Length - 2] + "/", "");
                    strAbsoluteUri += "inscripcionGeneral.aspx";
                    Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                    loc = myUri.AbsoluteUri.ToString();
                    loc = loc.Replace(myUri.AbsolutePath.ToString(), "/inscripcionGeneral.aspx");
                    //Response.Redirect(loc, true);
                    //guardamos la información obtenida al momento
                    cParticipante objectParticipante = new cParticipante();
                    objectParticipante.dsNombreVisitante = txtPreNombre.Text;
                    objectParticipante.nombre = txtPreNombre.Text;
                    objectParticipante.apellidoPaterno = txtPreApellidoPaterno.Text;
                    objectParticipante.apellidoMaterno = txtPreApellidoMaterno.Text;
                    objectParticipante.email = txtEmail.Text;
                    objectParticipante.telefono1 = txtPreTelefonofijo1.Text + txtPreTelefonofijo2.Text;
                    objectParticipante.telefono2 = txtTelefonoMovil1.Text + txtTelefonoMovil2.Text;
                    objectParticipante.idSexo = int.Parse(ddSexo.SelectedValue.ToString());
                    objectParticipante.formaPago = int.Parse(rblFormaPago.SelectedValue);
                    objectParticipante.idClienteFacturar = (ddlClienteFacturar.SelectedValue.ToString() != "") ? int.Parse(ddlClienteFacturar.SelectedValue) : 0;
                    if (cbFederacion.Checked)
                        objectParticipante.dsAgrupador = "xelhafmtri";
                    else
                        objectParticipante.dsAgrupador = "TriatlonBackoffice";
                    if (txtAgrupador.Text != "")
                        objectParticipante.dsAgrupador = txtAgrupador.Text;
                    objectParticipante.idEstatusCliente = 16; // Preasignado
                    objectParticipante.llenarLista(objectParticipante);
                    // quiere decir que será cxc o coretesia
                    // mostramos el link para terminar de inscribir al cliente
                    var listResp = objectParticipante.guardarCompetidor(objectParticipante.listCompetidores, cGlobals.dsSession, cGlobals.idCliente, cGlobals.idUsuario);
                    //if (strFormaPago == "2" || strFormaPago == "3")
                    // {
                    int respuestaModificaContactoEvento = 0;
                    if (cbFederacion.Checked)
                    {
                        wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                        wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                        clienteContactoEventoClient.Open();
                        cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(listResp[0].ToString()), cGlobals.dsSession);
                        cCompetidor.idEventoClasificacion = (cCompetidor.idEventoClasificacion == 0) ? null : cCompetidor.idEventoClasificacion;
                        cCompetidor.idCategoria = (cCompetidor.idCategoria == 0) ? null : cCompetidor.idCategoria;
                        cCompetidor.idEventoModalidad = (cCompetidor.idEventoModalidad == 0) ? null : cCompetidor.idEventoModalidad;
                        cCompetidor.idSexo = (cCompetidor.idSexo == 0) ? null : cCompetidor.idSexo;
                        cCompetidor.idTalla = (cCompetidor.idTalla == 0) ? null : cCompetidor.idTalla;
                        cCompetidor.idTipoSangre = (cCompetidor.idTipoSangre == 0) ? null : cCompetidor.idTipoSangre;
                        
                        cCompetidor.dsLiga = strAbsoluteUri + "?contEvent=" + listResp[0].ToString();
                        
                        respuestaModificaContactoEvento = clienteContactoEventoClient.ModificarcClienteContactoEvento(cCompetidor,  cGlobals.dsSession);
                    }
                    string strAbsoluteUriElite = "";
                    string strAbsoluteUriNovata = "";
                    if (cbElite.Checked)
                    {


                        //string loc = "";
                        // = HttpContext.Current.Request.ApplicationPath + "/NewDestinationPage.aspx";
                        //Response.Redirect(Request.Url.AbsoluteUri);
                        strAbsoluteUriElite = HttpContext.Current.Request.Url.AbsoluteUri;

                        string[] strspliturl2 = strAbsoluteUriElite.Split('/');
                        strAbsoluteUriElite = strAbsoluteUriElite.Replace(strspliturl2[strspliturl2.Length - 1], "");
                        strAbsoluteUriElite = strAbsoluteUriElite.Replace(strspliturl2[strspliturl2.Length - 2] + "/", "");
                        strAbsoluteUriElite += "inscripcionElite.aspx";
                        //Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                        //loc = myUri.AbsoluteUri.ToString();
                        //loc = loc.Replace(myUri.AbsolutePath.ToString(), "/inscripcionGeneral.aspx");

                        wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                        wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                        clienteContactoEventoClient.Open();
                        cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(listResp[0].ToString()), cGlobals.dsSession);
                        cCompetidor.idEventoClasificacion = (cCompetidor.idEventoClasificacion == 0) ? null : cCompetidor.idEventoClasificacion;
                        cCompetidor.idCategoria = (cCompetidor.idCategoria == 0) ? null : cCompetidor.idCategoria;
                        cCompetidor.idEventoModalidad = (cCompetidor.idEventoModalidad == 0) ? null : cCompetidor.idEventoModalidad;
                        cCompetidor.idSexo = (cCompetidor.idSexo == 0) ? null : cCompetidor.idSexo;
                        cCompetidor.idTalla = (cCompetidor.idTalla == 0) ? null : cCompetidor.idTalla;
                        cCompetidor.idTipoSangre = (cCompetidor.idTipoSangre == 0) ? null : cCompetidor.idTipoSangre;

                        cCompetidor.dsLiga = strAbsoluteUriElite + "?contEvent=" + listResp[0].ToString();
                        respuestaModificaContactoEvento = clienteContactoEventoClient.ModificarcClienteContactoEvento(cCompetidor, cGlobals.dsSession);
                    }
                    hplLink.Visible = true;
                    objectParticipante.VaciarLista();
                    if (cbElite.Checked)
                    {
                        hplLink.Text = strAbsoluteUriElite + "?contEvent=" + listResp[0].ToString();
                        hplLink.NavigateUrl = strAbsoluteUriElite + "?contEvent=" + listResp[0].ToString();
                        strAbsoluteUri = strAbsoluteUriElite;
                    }
                    else if (cbNovata.Checked)
                    {
                        strAbsoluteUriNovata = HttpContext.Current.Request.Url.AbsoluteUri;

                        string[] strspliturl2 = strAbsoluteUriNovata.Split('/');
                        strAbsoluteUriNovata = strAbsoluteUriNovata.Replace(strspliturl2[strspliturl2.Length - 1], "");
                        strAbsoluteUriNovata = strAbsoluteUriNovata.Replace(strspliturl2[strspliturl2.Length - 2] + "/", "");
                        strAbsoluteUriNovata += "inscripcionNovatas.aspx";
                        hplLink.Text = strAbsoluteUriNovata + "?contEvent=" + listResp[0].ToString();
                        hplLink.NavigateUrl = strAbsoluteUriNovata + "?contEvent=" + listResp[0].ToString();
                        strAbsoluteUri = strAbsoluteUriNovata;
                    }
                    else
                    {
                        hplLink.Text = strAbsoluteUri + "?contEvent=" + listResp[0].ToString();
                        hplLink.NavigateUrl = strAbsoluteUri + "?contEvent=" + listResp[0].ToString();
                    }
                    if (cbCorreoConfirmacion.Checked)
                    {
                        if (txtEmail.Text != "")
                        {
                            string strNombreCompleto = objectParticipante.nombre + " " + objectParticipante.apellidoPaterno + " " + objectParticipante.apellidoMaterno;
                            string mensajeMail = cGlobals.notifIncripcionDirectos;
                            mensajeMail = mensajeMail.Replace("[nombre]", strNombreCompleto).Replace("[url]", strAbsoluteUri + "?contEvent=" + listResp[0].ToString());
                            bool respEnvioCorreo = utils.enviaCorreo(txtEmail.Text, "´Confirmación inscripción Triatlón Xel-Há", mensajeMail, "nfigueroa@experienciasxcaret.com.mx");
                        }
                    }
                    // }
                    // else
                    //{
                    //  hplLink.Visible = false;
                    // hplLink.Text = "";
                    //hplLink.NavigateUrl = "";
                    //}
                }
            }
            catch (Exception ex)
            {
 
            }
        }
    }
}