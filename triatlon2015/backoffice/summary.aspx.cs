﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;
using SelectPdf;
namespace travesia
{
    public partial class summary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int intIdClienteContactoEvento, intidVenta = 0;
                string strIdVenta = "";
                DataSet result, resultCompetidor;
                DataSet dsDetalleVenta;
                utils.Login("usrTriatlon", "eXperiencias");
                int intUltimoPago = 0;
                //int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                int.TryParse(Request.QueryString["token"], out intidVenta);
                int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                string url = "https://www.xperienciasxcaret.mx/core/archivos/Travesia/Storage/files/" + intIdClienteContactoEvento.ToString().Trim() + "/AVATAR.jpg";
                if (intIdClienteContactoEvento > 0)
                {
                    result = null;
                    try
                    {
                        result = utils.buscarCompetidor(getDsSession(), "", "", "", 0, "", "", 0, 0, 0, intIdClienteContactoEvento);
                    }
                    catch (Exception ex)
                    {
                        result = utils.buscarCompetidor(getDsSession(), "", "", "", 0, "", "", 0, 0, 0, intIdClienteContactoEvento);
                        string msg = ex.Message.ToString();
                        string strginnermessage = "";
                        string strCuerpoCorreo = "respRegistro. Page_load(). El Primer intento carga de competidor falló " + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                        string strExcepcion = "";
                        if (ex.ToString().Length > 799)
                            strExcepcion = ex.ToString().Substring(0, 800);
                        else
                            strExcepcion = ex.ToString();
                        strCuerpoCorreo += strExcepcion;
                        //MensajeAlert(strCuerpoCorreo);
                    }

                    if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                    {
                        //cParticipante.dtCompedidor = resultCompetidor.Tables[0];
                        hdCorreoCompetidor.Value = result.Tables[0].Rows[0]["dsCorreoElectronico"].ToString();
                        imgAvatar.ImageUrl = url;
                        wsBusinessRules.BusinessRulesServicesClient ruleOper = new wsBusinessRules.BusinessRulesServicesClient();
                        Dictionary<string, string> dicret = new Dictionary<string, string>();

                        lblNombre.Text = result.Tables[0].Rows[0]["dsContacto"].ToString() + " " + result.Tables[0].Rows[0]["dsApellidoPaterno"].ToString() + " " + result.Tables[0].Rows[0]["dsApellidoMaterno"].ToString();
                        lblCiudad.Text = result.Tables[0].Rows[0]["dsCiudad"].ToString();
                        lblEstado.Text = result.Tables[0].Rows[0]["dsEstado"].ToString();
                        lblPais.Text = result.Tables[0].Rows[0]["dsPais"].ToString();
                        lblDireccion.Text = "Calle " + result.Tables[0].Rows[0]["dsCalle"].ToString() + " Colonia " + result.Tables[0].Rows[0]["dsColonia"].ToString() + " # exterior " + result.Tables[0].Rows[0]["dsNumExterior"].ToString() + ", # interior " + result.Tables[0].Rows[0]["dsNumInterior"].ToString();
                        lblEmail.Text = result.Tables[0].Rows[0]["dsCorreoElectronico"].ToString();
                        lblTelefono.Text = result.Tables[0].Rows[0]["dsTelefono"].ToString();
                        lblCelular.Text = result.Tables[0].Rows[0]["dsCelular"].ToString();
                        lblSexo.Text = result.Tables[0].Rows[0]["dsSexo"].ToString();
                        lblEdad.Text = result.Tables[0].Rows[0]["noEdad"].ToString();
                        lblPeso.Text = result.Tables[0].Rows[0]["noPeso"].ToString();
                        lblSangre.Text = result.Tables[0].Rows[0]["dsTipoSangre"].ToString();
                        lblSede.Text = result.Tables[0].Rows[0]["dsProducto"].ToString();
                        lbFrecuenciaDeporte.Text = result.Tables[0].Rows[0]["dsFrecuenciaDeporte"].ToString();
                        lblAnioParticipaciones.Text = result.Tables[0].Rows[0]["dsAniosParticipaciones"].ToString();
                        lblNivelNatacion.Text = result.Tables[0].Rows[0]["nivelNatacion"].ToString();
                        if (result.Tables[0].Rows[0]["cnVegetariano"] != null)
                            lblVegetariano.Text = (result.Tables[0].Rows[0]["cnVegetariano"].ToString().Trim() == "1") ? "Si" : "No";
                        lblMotivoParticipacion.Text = result.Tables[0].Rows[0]["dsMotivoParticipacion"].ToString();
                        lblSignificadoTSM.Text = result.Tables[0].Rows[0]["dsSignificadoTsm"].ToString();
                        lblNumeroColaborador.Text = result.Tables[0].Rows[0]["noColaborador"].ToString();
                        lblUnidadNegocio.Text = result.Tables[0].Rows[0]["dsUnidadNegocioColaborador"].ToString();                        
                    }
                }
            }

        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
                //hdDsSession.Value = getDsSession();
            }
            return cGlobals.dsSession;
        }
    }
}