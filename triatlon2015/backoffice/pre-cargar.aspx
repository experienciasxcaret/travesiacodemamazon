﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/menu.Master" AutoEventWireup="true" CodeBehind="pre-cargar.aspx.cs" Inherits="travesia.backoffice.pre_asignar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../styles/preCargar.css" />
    <script src="../Scripts/pre-cargar.js" ></script>
</asp:Content>

<asp:Content id="content2" contentPlaceHolderId="MainContent" runat="server">

    <div class="container">
        <div class="grid_16 wrap" style="padding-left:5px">
            <form id="formPreCargar" role="form" class="form-horizontal" runat="server">
                
                <div class="container">             
                    <div class="form-group" style="padding-top:5px">
                        <div class="col-md-9">
                            <p>
                            <a href="pre-cargar.aspx">pre-cargar.aspx</a>
                            </p>
                            <div class ="col-sm-3"	>
                                <label>Nombre</label>                                    
                                <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtPreNombre" placeholder="ingresa nombre" name="nombre1">
                                </asp:TextBox>
                            </div>
                    
                            <div class ="col-sm-3">
                                <label>Apellido Paterno</label>
                                <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtPreApellidoPaterno" placeholder="ingresa apellido paterno" name="apellido1">
                                </asp:TextBox>
                            </div>   
                    
                            <div class ="col-sm-3">
                                <label>Apellido Materno</label>          
                                <asp:TextBox runat="server" class="form-control"  id="txtPreApellidoMaterno" placeholder="ingresa apellido materno" name="apellido2">
                                </asp:TextBox>
                            </div>
                        </div>                     
                    </div>
                                                  
                    <div class="form-group">
                        <div class="col-md-7">
                            <label for="email" class="col-md-3">Email</label> 
                            <div class ="col-md-4">                        
                                <asp:TextBox runat="server" type="email" class="{required:true} form-control" id="txtEmail" placeholder="ingresa tu email" name="email" ClientIDMode="Static"></asp:TextBox>
                            </div>
                        </div>
                    </div>                 
               
                    <div class="form-group">
                        <div class="col-md-7">
                            <label for="email" class="col-md-3">Confirma Email</label>
                            <div class ="col-md-4">
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate> 
                                    <asp:TextBox runat="server" type="email" class="{required:true} form-control" id="txtEmail2" onpaste="return false" oncut="return false" placeholder="confirma tu email" name="email" ClientIDMode="Static"></asp:TextBox>                        
                                    <dx:ASPxLabel ID="lblValidacionEmail" runat="server" Text="" Font-Size="Small" ForeColor="#CC0000" ClientIDMode="Static"></dx:ASPxLabel>
                                </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                         </div>
                    </div>  
            
                    <div class="form-group">                     
                        <div class="col-md-8">
                            <label for="telefono" class="col-sm-3">Telefono Fijo</label>   
                            <div class="col-sm-1">                                                                                      
                                <asp:TextBox runat="server" width="60px" class="form-control" id="txtPreTelefonofijo1" placeholder="lada" name="lada1">
                                </asp:TextBox>
                            </div>
                            <div class="col-sm-3">
                                 <asp:TextBox runat="server" class="form-control"   id="txtPreTelefonofijo2" placeholder="Numero" name="numero1">
                                </asp:TextBox> 
                            </div>                                                                                      
                         </div>    
                    </div>
                    <div class="form-group">                     
                        <div class="col-md-8">
                            <label for="telefono" class="col-sm-3">Telefono Movil</label>   
                            <div class="col-sm-1">                                                                                      
                                <asp:TextBox runat="server" width="60px" class="form-control" id="txtTelefonoMovil1" placeholder="lada" name="lada1">
                                </asp:TextBox>
                            </div>
                            <div class="col-sm-3">
                                 <asp:TextBox runat="server" class="form-control"   id="txtTelefonoMovil2" placeholder="Numero" name="numero1">
                                </asp:TextBox> 
                            </div>                                                                                      
                         </div>    
                    </div>

                    <div class="form-group">
                        <div class="col-md-7">
                            <label for="txtAgrupador" class="col-md-3">Origen</label> 
                            <div class ="col-md-4">                        
                                <asp:TextBox runat="server" class="form-control" id="txtAgrupador" placeholder="Origen del competidor" name="txtAgrupador" ClientIDMode="Static"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>               
                            <div class="form-group">           
                                <div class="col-md-7 selectContainer">                    
                                    <label for="sexo" class="col-sm-3">Sexo</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList  class="{required: true} form-control" ID="ddSexo" runat="server" name="sexo">
                                                <asp:ListItem Value="">Seleccione sexo</asp:ListItem>
                                                <asp:ListItem Value="2">Masculino</asp:ListItem>
                                                <asp:ListItem Value="3">Femenino</asp:ListItem>
                                        </asp:DropDownList>
                                     </div>                          
                                </div>
                            </div>               
                    </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="form-group">
                        <div class="col-sm-12">
                              <div class="col-sm-10">
                                  <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBox ID="cbElite" runat="server" Text="¿Es competidor Elite?" checked="false"/>
                                    </ContentTemplate>
                                   </asp:UpdatePanel>                                                  
                              </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                              <div class="col-sm-10">
                                  <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBox ID="cbNovata" runat="server" Text="¿Es competidor Novata?" checked="false"/>
                                    </ContentTemplate>
                                   </asp:UpdatePanel>                                                  
                              </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8">
                             <div class="col-md-8">
                                 <label>Seleccione opción pago de inscripción :</label>
                             </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="form-group"> 
                            <div class="col-md-1">
                            </div>                
                             <div class="col-md-6"> 
                                <asp:RadioButtonList ID="rblFormaPago" runat="server" OnSelectedIndexChanged="rblFormaPago_SelectedIndexChanged" AutoPostBack="True">                    
                                        <asp:ListItem
                                            Enabled="True"
                                            Selected="True"
                                            Text="Pago normal (si paga)"
                                            Value="1"
                                        />
                                        <asp:ListItem
                                            Enabled="True"                            
                                            Text="C x C (competidor no paga al momento)"
                                            Value="2"
                                        />
                                        <asp:ListItem
                                            Enabled="True"                            
                                            Text="Cortesía (competidor no paga)"
                                            Value="3"
                                        />
                                 </asp:RadioButtonList>
                            </div>                
                        </div>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="updClienteFacturar" runat="server">
                    <ContentTemplate>               
                            <div class="form-group">           
                                <div class="col-md-8 selectContainer">
                                    <div class="col-md-8" >
                                        <asp:Label ID="lblClienteFacturar" runat="server" Text="En caso de facturar seleccione el cliente a facturar" Visible="False" Font-Bold="True"></asp:Label>
                                        <asp:DropDownList  class="form-control" ID="ddlClienteFacturar" runat="server" name="clienteFacturar" Visible="false">
                                                <asp:ListItem Value="">Seleccione Cliente</asp:ListItem>                                        
                                        </asp:DropDownList>
                                    </div>                   
                                </div>
                            </div>               
                    </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="form-group">
                        <div class="col-sm-12">
                              <div class="col-sm-10">
                                  <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBox ID="cbFederacion" runat="server" Text="¿Ganador enviado por la FMTRI?" checked="false"/>
                                      </ContentTemplate>
                                    </asp:UpdatePanel>                                                  
                              </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                              <div class="col-sm-10">
                                  <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBox ID="cbCorreoConfirmacion" runat="server" Text="¿Enviar correo confirmacion al cliente?" checked="true"/>
                                      </ContentTemplate>
                                    </asp:UpdatePanel>                                                  
                              </div>
                        </div>
                    </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                              <div class="col-sm-2">                        
                                    <!--<button type="submit" class="btn btn-info pull-left">Buscar</button>-->
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>                                      
                                     <dx:ASPxButton ID="btnTerminar" runat="server" Text="Terminar" class="btn btn-info btn-mini" name="NameBtnContinuarC"  CssPostfix="&quot;btn btn-info btn-mini" ClientInstanceName="NameBtnContinuarC"  UseSubmitBehavior="False"  AutoPostBack="false" ValidationGroup="validaGuardar" ClientIDMode="Static">
                                     </dx:ASPxButton>
                                     
                                    
                                                
                                    </ContentTemplate>
                                    </asp:UpdatePanel>                                                  
                              </div>
                              <div class="col-sm-10">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <dx:ASPxLabel ID="lblMensajeValidacion" runat="server" Text="" ClientInstanceName="lblMensajeValidacion" Font-Bold="True" Font-Size="Small" ForeColor="#CC0000" ClientIDMode="Static"></dx:ASPxLabel>
                                        <asp:HyperLink ID="hplLink" runat="server" Target="_blank" Font-Size="Medium" ForeColor="Black"></asp:HyperLink>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                              </div>
                            </div>                    
                        </div>
                     
                </div>      
            </form>  
        </div>
    </div>
</asp:Content>