﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/menu.Master" AutoEventWireup="true" CodeBehind="agregarCliente.aspx.cs" Inherits="travesia.backoffice.agregarCliente" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/agregarCliente.js" ></script>
    <script type="text/javascript">

        function agregarCliente() {
            //cbAgregar.PerformCallback();
            pnlinfoproceso.Show();
        }        
        $(document).ready(function () {
           /* $('.classTxtRfc').change(function () {
                alert($(this).val());
            });*/
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="grid_16 wrap" style="padding-left:5px">
             <form id="formAgregarCliente" role="form" class="form-horizontal" runat="server">
        
        <div class="container">             
            <div class="form-group" style="padding-top:5px">
                <div class="col-md-9">
                    <div class ="col-sm-3"	>
                        <label>Nombre</label>                                    
                        <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtNombre" placeholder="ingresa nombre" name="nombre1"  ClientIDMode="Static">
                        </asp:TextBox>
                    </div>
                    
                    <div class ="col-sm-3">
                        <label>Apellido Paterno</label>
                        <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtApellidoPaterno" placeholder="ingresa apellido paterno" name="apellido1" ClientIDMode="Static">
                        </asp:TextBox>
                    </div>   
                    
                    <div class ="col-sm-3">
                        <label>Apellido Materno</label>          
                        <asp:TextBox runat="server" class="form-control"  id="txtApellidoMaterno" placeholder="ingresa apellido materno" name="apellido2" ClientIDMode="Static">
                        </asp:TextBox>
                    </div>
                </div>                                          
            </div>
            <div class="form-group">
                <div class="col-md-12">                        
                    <label for="rfc" class="col-md-2">RFC(solo méxico)</label> 
                    <div class ="col-md-4">                        
                        <asp:TextBox runat="server" class="{required:true, rangelength: [11,13], rfc:true} form-control classTxtRfc" id="txtRfc" placeholder="ingresa tu RFC" name="txtRfc" ClientIDMode="Static"></asp:TextBox>
                    </div>                        
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">                        
                    <label for="razonSocial" class="col-md-2">Razon social</label> 
                    <div class ="col-md-4">                        
                        <asp:TextBox runat="server" class="{required:true} form-control" id="txtRazonSocial" placeholder="Razon social" name="razonSocial" ClientIDMode="Static"></asp:TextBox>
                    </div>                        
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">                        
                    <label for="razonComercial" class="col-md-2">Razon comercial</label> 
                    <div class ="col-md-4">                        
                        <asp:TextBox runat="server" class="{required:true} form-control" id="txtRazonComercial" placeholder="Razon comercial" name="razonComercial" ClientIDMode="Static"></asp:TextBox>
                    </div>                        
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">                        
                    <label for="telefono" class="col-md-2">Telefono</label> 
                    <div class ="col-md-4">                        
                        <asp:TextBox runat="server" class="{required:true} form-control" id="txtTelefono" placeholder="Telefono" name="telefono" ClientIDMode="Static"></asp:TextBox>
                    </div>                        
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="email" class="col-md-2">Email</label> 
                    <div class ="col-md-4">                        
                        <asp:TextBox runat="server" type="email" class="{required:true} form-control classEmail1" id="txtEmail" placeholder="ingresa tu email" name="email" ClientIDMode="Static"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="email" class="col-md-2">Confirma Email</label>
                    <div class ="col-md-4">
                        <asp:TextBox runat="server" type="email" class="{required:true} form-control classEmail2" id="txtEmail2" onpaste="return false" oncut="return false" placeholder="confirma tu email" name="email" ClientIDMode="Static"></asp:TextBox>                        
                        <dx:ASPxLabel ID="lblValidacionEmail" runat="server" Text="" Font-Size="Small" ForeColor="#CC0000"></dx:ASPxLabel>
                        </div>
                 </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
            <ContentTemplate>
            <div class="form-group">
                <div class="col-md-12">                                            
                    <label for="pais" class="col-sm-1">País</label>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                    <div class="col-sm-3">
                    <asp:DropDownList  class="form-control classDdlPais" ID="ddlPais" runat="server" name="pais" AutoPostBack="True" OnSelectedIndexChanged="ddlPais_SelectedIndexChanged">
                    </asp:DropDownList>
                    </div>    
                    </ContentTemplate>
                    </asp:UpdatePanel>                         
                    <label for="estado" class="col-sm-1">Estado</label>
                        <div class="col-sm-3">
                            <asp:DropDownList  class="form-control" ID="ddlEstado" runat="server" name="estado" ClientIDMode="Static">
                            <asp:ListItem Value="1">Seleccione Estado</asp:ListItem>                                                                
                    </asp:DropDownList>       
                    </div>                                                                                           
                </div>
            </div>
            </ContentTemplate>
            </asp:UpdatePanel>
            <div class="form-group">
                <div class="col-md-12">                                            
                    <label for="calle" class="col-sm-1">Calle</label>
                    <div class="col-sm-3">
                        <asp:TextBox runat="server" class="{required:true} form-control" id="txtCalle" placeholder="calle" name="calle" ClientIDMode="Static"></asp:TextBox>
                    </div>                             
                    <label for="colonia" class="col-sm-1">Colonia</label>                        
                    <div class="col-sm-3">
                        <asp:TextBox runat="server" class="{required:true} form-control" id="txtColonia" placeholder="colonia" name="colonia" ClientIDMode="Static"></asp:TextBox>     
                    </div>                                                                                           
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">                                            
                    <label for="ciudad" class="col-sm-1">Ciudad</label>
                    <div class="col-sm-3">
                        <asp:TextBox runat="server" class="{required:true} form-control" id="txtCiudad" placeholder="ciudad" name="ciudad" ClientIDMode="Static"></asp:TextBox>
                    </div>                                                                                                               
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">                                            
                    <label for="calle" class="col-sm-1">No. Interior</label>
                    <div class="col-sm-1">
                        <asp:TextBox runat="server" class=" form-control" id="txtNoInterior" placeholder="no.Interior" name="interior" ClientIDMode="Static"></asp:TextBox>
                    </div>                             
                    <label for="colonia" class="col-sm-1">No. Exterior</label>                        
                    <div class="col-sm-1">
                        <asp:TextBox runat="server" class="{required:true} form-control" id="txtNoExterior" placeholder="no. Exterior" name="exterior" ClientIDMode="Static"></asp:TextBox>     
                    </div>                                                                                           
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">                                            
                    <label for="codigoPostal" class="col-sm-1">Codigo Postal</label>
                    <div class="col-sm-3">
                        <asp:TextBox runat="server" class="{required:true} form-control" id="txtCodigoPostal" placeholder="codigo postal" name="codigoPostal" ClientIDMode="Static"></asp:TextBox>
                    </div>                                                                                                               
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-md-12">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                  <div class="col-sm-2">
                    <!--<button type="submit" class="btn btn-info pull-left">Buscar</button>-->
                      <dx:ASPxButton ID="btnAgregarCliente" runat="server" Text="Agregar" class="btn btn-info pull-left" name="btnAgregar"  CssPostfix="&quot;btn btn-info pull-left" ClientInstanceName="btnAgregarCliente" AutoPostBack="false" ClientIDMode="Static" >
                      </dx:ASPxButton>
                     
                  </div>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <dx:ASPxLabel   class="col-sm-8 lblMensaje" ID="lblMensajeRespuesta" runat="server" Text="" Font-Size="Medium" ClientInstanceName="lblMensaje" name="lblMensaje"  ClientIDMode="Static"></dx:ASPxLabel>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
                    
        </div>
        <asp:HiddenField ID="hdValidaRFC" runat="server" ClientIDMode="Static" Value="0" />
        <br />
        <dx:ASPxCallback ID="cbAgregar" runat="server" ClientInstanceName="cbAgregar" OnCallback="cbAgregar_Callback">
        </dx:ASPxCallback>
    </form>
        </div>
    </div>
    <br />
</asp:Content>
