﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web.ASPxEditors;

namespace travesia.backoffice
{
    public partial class agregarCliente : System.Web.UI.Page
    {

        const String callbackAgregarCliente = "agregarCliente();";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                utils.llenaPais(ddlPais);
            }
        }

        protected void ddlPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            utils.llenaEstados(ddlEstado, ddlPais.SelectedValue.ToString(), cGlobals.dsSession);
        }       

        protected void btnAgregarCliente_Init(object sender, EventArgs e)
        {
            ASPxButton btnAgregar = sender as ASPxButton;
            btnAgregar.Attributes.Add("onClick", String.Format(callbackAgregarCliente));
        }

        protected void cbAgregar_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            try
            {
                int respClienteNivelCliente = 0;
                wscCliente.IcClienteClient iClienteClient = new wscCliente.IcClienteClient();
                iClienteClient.Open();
                wscCliente.cCliente cCliente = new wscCliente.cCliente();
                cCliente.cnEsAgencia = false;
                cCliente.idCliente = 1;
                cCliente.dsRazonComercial = txtRazonComercial.Text;
                cCliente.dsRazonSocial = txtRazonSocial.Text;
                cCliente.dsRFC = txtRfc.Text;
                cCliente.dsTelefono = txtTelefono.Text;
                cCliente.feAlta = DateTime.Now;
                cCliente.dsMunicipio = txtCiudad.Text;
                cCliente.idPais = int.Parse(ddlPais.SelectedValue.ToString());
                cCliente.idEstado = int.Parse(ddlEstado.SelectedValue.ToString());
                cCliente.dsCalle = txtCalle.Text;
                cCliente.dsCiudad = txtCiudad.Text;
                cCliente.dsCodigoPostal = txtCiudad.Text;
                cCliente.dsColonia = txtColonia.Text;
                cCliente.dsContacto = txtNombre.Text + " " + txtApellidoPaterno.Text + " " + txtApellidoMaterno.Text;
                cCliente.dsNumInterior = txtNoInterior.Text;
                cCliente.dsNumExterior = txtNoExterior.Text;
                cCliente.idTipoMonedaFacturacion = 2;
                cCliente.dsCorreoElectronico = txtEmail.Text;
                cCliente.idEstatusCliente = 14; // en proceso
                cCliente.dsClave = "";
                cCliente.cnCredito = false;
                cCliente.cnEmiteCupon = false;
                cCliente.cnBloqueado = false;
                cCliente.cnCadenaEmpresas = false;
                cCliente.cnAceptaPromociones = false;
                cCliente.cnRequiereFactura = true;
                cCliente.cnAccesoWeb = false;

                int respIdCliente = iClienteClient.InsertarcCliente(cCliente, cGlobals.dsSession);

                wscClienteNivelCliente.IcClienteNivelClienteClient iClienteNivelCliente = new wscClienteNivelCliente.IcClienteNivelClienteClient();
                wscClienteNivelCliente.cClienteNivelCliente cClienteNivelCliente = new wscClienteNivelCliente.cClienteNivelCliente();

                cClienteNivelCliente.idCliente = respIdCliente;
                cClienteNivelCliente.idNivelCliente = 34;

                iClienteNivelCliente.Open();
                respClienteNivelCliente = iClienteNivelCliente.InsertarcClienteNivelCliente(cClienteNivelCliente, cGlobals.dsSession);
                if (respClienteNivelCliente > 0)
                {
                    // ahora enviar correo a ingresos para que se entere de que se dio de alta un cliente
                    string mensajeMail = cGlobals.notifClienteNuevo;
                    string strCorreos = "nfigueroa@experienciasxcaret.com.mx;fimox.reformer@gmail.com";
                    mensajeMail = mensajeMail.Replace("[nombre]", cCliente.dsContacto).Replace("[razonsocial]", cCliente.dsRazonSocial).Replace("[RFC]", cCliente.dsRFC);
                    bool respEnvioCorreo = utils.enviaCorreo(strCorreos, "Cliente Nuevo Especial Triatlon 2015", mensajeMail, "fimox.reformer@gmail.com");
                    lblMensajeRespuesta.Text = "El cliente ya fue dado de alta";
                }
            }
            catch (Exception ex)
            {
                lblMensajeRespuesta.Text = ex.Message;
            }
        }        
    }
}