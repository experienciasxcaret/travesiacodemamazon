﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

namespace travesia.backoffice
{
    public partial class competidores : System.Web.UI.Page
    {
        cParticipante cCompetidoresBusqueda = new cParticipante();
        public string strlinkCupon;
        protected void Page_Load(object sender, EventArgs e)
        {
            strlinkCupon =  utils.strGetUrlCupon(getDsSession(), "1");
            if (!IsPostBack)
            {
                utils.getCategorias(getDsSession(),1, 1, ddlCategoria, null, "ALL", 1);
                utils.getEstatusVenta(ddlEstatusVenta);
                utils.getEstatusParticipante(ddlEstatusParticipante);
                gvCompetidores.Visible = false;
                DataTable dtCompetidorBusqueda;
                //if (Session["cCompetidoresBusqueda"] != null)
                if (Session["cCompetidoresBusqueda"] != null)
                {
                    dtCompetidorBusqueda = (DataTable)Session["cCompetidoresBusqueda"];
                    if (dtCompetidorBusqueda.Rows[0]["retValue"].ToString() == "1")
                    {
                        gvCompetidores.DataSource = cParticipante.dtCompedidor;
                        //gvCompetidores.DataBind();
                        gvCompetidores.Visible = true;                        
                    }
                }
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {   /// string apellidoPaterno, string nombre, int noCompetidor, int idVenta, int noRifa, string dsCorreo, int idEventoClasificacion, int idEstatusVenta, int idEstatusParticipante = 15 /*15 inscrito*/)
            try
            {
                string apellidoPaterno = "";
                string nombre = "";
                string noCompetidor = "";
                string dsFolioVenta = "";
                int idVenta = 0;
                string strNoRifa = "";
                string dsCorreo = "";
                int idEventoClasificacion = 0;
                int idEstatusVenta = 0;
                int idEstatusParticipante = 15; /*15 inscrito*/
                int idClienteContactoEvento = 0;
                string strFechaAlta = "";

                apellidoPaterno = txtPaterno.Text;
                nombre = txtNombre.Text;
                //noCompetidor = (txtNoCompetidor.Text != "" )? int.Parse(txtNoCompetidor.Text) : 0;
                noCompetidor = txtNoCompetidor.Text;
                dsFolioVenta = txtNoFolio.Text;
                strNoRifa = txtNoRifa.Text.Trim();
                dsCorreo = txtEmail.Text;
                string[] strIdEventoClasificacion = ddlCategoria.SelectedValue.ToString().Split('-');
                //int intIdEventoClasificacion = int.Parse(strIdEventoClasificacion[0].ToString());

                idEventoClasificacion = (strIdEventoClasificacion[0].ToString() != "")? int.Parse(strIdEventoClasificacion[0].ToString()) : 0;
                idEstatusVenta = (ddlEstatusVenta.SelectedValue.ToString() != "")?  int.Parse(ddlEstatusVenta.SelectedValue.ToString()) :0;
                idEstatusParticipante = (ddlEstatusParticipante.SelectedValue.ToString()!= "")? int.Parse(ddlEstatusParticipante.SelectedValue.ToString()): 0;
                strFechaAlta = idTxtFeAlta.Text.Trim();
                if (dsFolioVenta != "")
                {
                    //wsTransactions.IkVentaClient kventa = new wsTransactions.IkVentaClient();
                    //idVenta = kventa.ObtenerIdVentaPorClaveVenta(dsFolioVenta, getDsSession());
                    wsGeneralServices.GeneralServicesClient gralCliente = new wsGeneralServices.GeneralServicesClient();
                    wsGeneralServices.ListacCombo listaC = new wsGeneralServices.ListacCombo();
                    listaC = gralCliente.SeleccionarcCombo("kVenta", "top 1 idVenta", "dsClaveVenta", "where dsClaveVenta = '" + dsFolioVenta +"'", "order by idEstatusVenta desc", getDsSession());
                    idVenta = listaC.cCombo[0].idValor;
                }
                DataSet result = utils.buscarCompetidor(getDsSession(), apellidoPaterno, nombre, noCompetidor, idVenta, strNoRifa, dsCorreo, idEventoClasificacion, idEstatusVenta, idEstatusParticipante,0, strFechaAlta);

                if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                {
                    result.Tables[0].Columns.Add("feVisitaNuevo", typeof(string));
                    result.Tables[0].Columns.Add("feNacimientoNuevo", typeof(string));
                    result.Tables[0].Columns.Add("idEventoNuevo", typeof(int));
                    result.Tables[0].Columns.Add("idEventoClasificacionNuevo", typeof(int));
                    result.Tables[0].Columns.Add("dsCodigoNuevo", typeof(string));
                    result.Tables[0].Columns.Add("idProductoPrecioNuevo", typeof(int));
                    result.Tables[0].Columns.Add("idProductoNuevo", typeof(int));
                    result.Tables[0].Columns.Add("precioListaNuevo", typeof(decimal));
                    result.Tables[0].Columns.Add("mnTipoCambioNuevo", typeof(decimal));
                    result.Tables[0].Columns.Add("idTipoClienteNuevo", typeof(int));
                    result.Tables[0].Columns.Add("idTipoMonedaNuevo", typeof(int));
                    result.Tables[0].Columns.Add("idLocacionNuevo", typeof(int));

                    gvCompetidores.Visible = true;
                    gvCompetidores.DataSource = result.Tables[0];
                    cCompetidoresBusqueda.DtCompetidor = result.Tables[0];
                    Session["cCompetidoresBusqueda"] = result.Tables[0];
                    gvCompetidores.DataBind();
                    //cParticipante.dtCompedidor = result.Tables[0];
                    //ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>javascript:alert('hola soy goku');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "myScript", "enfocarDivBusqueda('gridCompetidores');", true);
                }
                else
                {
                    gvCompetidores.DataSource = null;
                    gvCompetidores.DataBind();
                }
                //ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>$('.' + type).animate({ bottom: '0' }, 500);</script>");
                //gridCompetidores.Focus();
               // ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>javascript:enfocarDivBusqueda(" + gridCompetidores + ");</script>");
                //ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>javascript:lart('hola soy goku');</script>");
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();                
            }
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                ASPxButton btnEditar = (ASPxButton)sender;
                GridViewDataItemTemplateContainer container = btnEditar.NamingContainer as GridViewDataItemTemplateContainer;
                string strMasterRowKeyValue = container.KeyValue.ToString();
                if (strMasterRowKeyValue != "")
                {
                    Response.Redirect("editCompetidor.aspx?cce=" + strMasterRowKeyValue);
                }
            }
            catch (Exception ex)
            {


            }
        }

        public DataTable getDatatableCompetidor()
        {
            if (cCompetidoresBusqueda.DtCompetidor == null)
                cCompetidoresBusqueda.DtCompetidor = (DataTable)Session["cCompetidoresBusqueda"];
            return cCompetidoresBusqueda.DtCompetidor;
        }

        protected void gvCompetidores_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            int cnModificacion = 0;
            
            if (e.RowType == GridViewRowType.Data)
            {
                int Fila = e.VisibleIndex;
                cnModificacion = Convert.ToInt32(((ASPxGridView)sender).GetRowValues(Fila, new string[] { "cnModificacion" }));
                
                /*if (cnModificacion > 0)
                {
                    e.Row.Visible = false;
                }*/               
            }
        }

        protected void gvCompetidores_PageIndexChanged(object sender, EventArgs e)
        {

            cCompetidoresBusqueda.DtCompetidor = getDatatableCompetidor();

            if (cCompetidoresBusqueda.DtCompetidor.Rows[0]["retValue"].ToString() == "1")
            {
                gvCompetidores.DataSource = cCompetidoresBusqueda.DtCompetidor;
                gvCompetidores.DataBind();
                //gvCompetidores.Visible = true;
            }
        }

        protected void gvCompetidores_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            try
            {
                string strNameRow = "";
                string strDsliga = "";
                int intIdEstatusVenta = 0;
                int idVenta = 0;
                int Fila = e.VisibleIndex;
                string strDsClave = "";
                //strDsliga = ((ASPxGridView)sender).GetRowValues(Fila, new string[] { "dsLiga" }).ToString();               
                strNameRow = e.DataColumn.FieldName;
                string strUrlCuponFinal = "";
                
                // si estamos en la columna de liga, buscamos su hyperlink para concatenar la liga para pago
                if (e.DataColumn.Name == "liga")
                {
                    strDsliga = e.GetValue("dsLiga").ToString();
                    ASPxHyperLink hlinkPago = gvCompetidores.FindRowCellTemplateControl(e.VisibleIndex, null, "hlinkPago") as ASPxHyperLink;
                    //ASPxHyperLink hlinkPago = ((ASPxGridView)sender).FindRowCellTemplateControl(e.VisibleIndex, ((ASPxGridView)sender).Columns[strNameRow] as GridViewDataColumn, "hlinkPago") as ASPxHyperLink;
                    hlinkPago.NavigateUrl = strDsliga;
                    hlinkPago.Dispose();
                }

                if (e.DataColumn.Name == "edit")
                {
                    strDsClave = (e.GetValue("dsClave").ToString());
                    //if (Int32.TryParse(strCodigoError.Trim(), out intCodigoErrorGlobal))
                    int.TryParse(e.GetValue("idEstatusVenta").ToString(), out intIdEstatusVenta);
                    ASPxButton btnEdit = gvCompetidores.FindRowCellTemplateControl(e.VisibleIndex, null, "btnEditar") as ASPxButton;
                    if (intIdEstatusVenta == 3 || intIdEstatusVenta == 7 || intIdEstatusVenta == 23)
                    if (strDsClave != "ITSM")
                    {
                        btnEdit.Visible = false;
                    }
                }
                if (e.DataColumn.Name == "folio")
                {
                    int.TryParse(e.GetValue("idEstatusVenta").ToString(), out intIdEstatusVenta);
                    int.TryParse(e.GetValue("idVenta").ToString(), out idVenta);
                    strUrlCuponFinal = strlinkCupon.Replace("<idVenta>", idVenta.ToString());
                    if (intIdEstatusVenta == 3 || intIdEstatusVenta == 7 || intIdEstatusVenta == 23)
                    {
                        ASPxHyperLink link = gvCompetidores.FindRowCellTemplateControl(e.VisibleIndex, null, "linkFolio") as ASPxHyperLink;
                        link.NavigateUrl = strUrlCuponFinal;
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.LoginU("rhernandezmi", "eXperiencias");
            }
            return cGlobals.dsSession; /* cambiar*/
        }
    }
}