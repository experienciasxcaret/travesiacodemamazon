﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace travesia.backoffice
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        

        protected void txtLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string respLoggeo = "";
                if (txtUsername.Text.Trim() != "" && txtPass.Text != "")
                {
                    respLoggeo = utils.LoginU(txtUsername.Text.Trim(), txtPass.Text.Trim());
                    if (respLoggeo == "true")
                    {
                        Session["usr"] = txtUsername.Text.Trim();
                        Session["pas"] = txtPass.Text.Trim();
                        Response.Redirect("home.aspx");
                        // todo bien
                    }
                    else
                    {
                        mensaje(respLoggeo + ". Favor de validarlo.");
                    }
                }
                /*else
                {
                    mensaje("Ingrese usuario y contraseña" + ". Favor de validarlo.");
                }*/
            }
            catch (Exception ex)
            {
               mensaje("Problema con el loggeo" + ex.ToString());                
            }
        }

        public void mensaje(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('errorMsg','Ha ocurrido un error',\'" + errMensaje + "\');</script>", false);
        }
    }
}