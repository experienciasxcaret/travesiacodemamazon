﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace travesia.backoffice
{
    public partial class asignarSorteados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                string strNoRifa = "";
                DataSet result = new DataSet();
                string strTresPrimeros = txtNoRifa.Text.Trim().Substring(0, 3);
                if (strTresPrimeros == "000")
                    strNoRifa = txtNoRifa.Text.Trim().Remove(0, 3);
                else
                    strNoRifa = txtNoRifa.Text.Trim();
                hplRutaSorteado.Text = "";
                hdIdClienteContacto.Value = "";
                hdIdClienteContactoEvento.Value = "";
                hdIdClienteDetalle.Value = "";
                if (strNoRifa != "")
                {
                    result = utils.buscarCompetidor(getDsSession(), "", "", "", 0, strNoRifa, "", 0, 0, 0);
                }
                if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            lblNombre.Text = result.Tables[0].Rows[i]["dsContacto"].ToString() + " " + result.Tables[0].Rows[i]["dsApellidoPaterno"].ToString() + " " + result.Tables[0].Rows[i]["dsApellidoMaterno"].ToString();
                            lblCorreo.Text = result.Tables[0].Rows[i]["dsCorreoElectronico"].ToString();
                            lblCategoria.Text = result.Tables[0].Rows[i]["dsCodigo"].ToString() + " " + result.Tables[0].Rows[i]["dsProducto"].ToString();
                            hdIdClienteContacto.Value = result.Tables[0].Rows[i]["idClienteContacto"].ToString();
                            hdIdClienteContactoEvento.Value = result.Tables[0].Rows[i]["idClienteContactoEvento"].ToString();
                            hdIdClienteDetalle.Value += result.Tables[0].Rows[i]["idClienteDetalle"].ToString() +",";
                            lblEstatus.Text = result.Tables[0].Rows[i]["dsEstatusCliente"].ToString();
                        }
                        if (i == 1)
                        {
                            lblNombre2.Text = result.Tables[0].Rows[i]["dsContacto"].ToString() + " " + result.Tables[0].Rows[i]["dsApellidoPaterno"].ToString() + " " + result.Tables[0].Rows[i]["dsApellidoMaterno"].ToString();
                            lblCorreo2.Text = result.Tables[0].Rows[i]["dsCorreoElectronico"].ToString();
                            lblCategoria2.Text = result.Tables[0].Rows[i]["dsCodigo"].ToString() + " " + result.Tables[0].Rows[i]["dsProducto"].ToString();
                            lblEstatus2.Text = result.Tables[0].Rows[i]["dsEstatusCliente"].ToString();
                            hdIdClienteContacto.Value = result.Tables[0].Rows[i]["idClienteContacto"].ToString();
                            //hdIdClienteContactoEvento.Value = result.Tables[0].Rows[i]["idClienteContactoEvento"].ToString();
                            hdIdClienteDetalle.Value += result.Tables[0].Rows[i]["idClienteDetalle"].ToString() + ",";                            
                        }
                        if (i == 2)
                        {
                            lblNombre3.Text = result.Tables[0].Rows[i]["dsContacto"].ToString() + " " + result.Tables[0].Rows[i]["dsApellidoPaterno"].ToString() + " " + result.Tables[0].Rows[i]["dsApellidoMaterno"].ToString();
                            lblCorreo3.Text = result.Tables[0].Rows[i]["dsCorreoElectronico"].ToString();
                            lblCategoria3.Text = result.Tables[0].Rows[i]["dsCodigo"].ToString() + " " + result.Tables[0].Rows[i]["dsProducto"].ToString();
                            lblEstatus3.Text = result.Tables[0].Rows[i]["dsEstatusCliente"].ToString();
                            hdIdClienteContacto.Value = result.Tables[0].Rows[i]["idClienteContacto"].ToString();
                            //hdIdClienteContactoEvento.Value = result.Tables[0].Rows[i]["idClienteContactoEvento"].ToString();
                            hdIdClienteDetalle.Value += result.Tables[0].Rows[i]["idClienteDetalle"].ToString() + ",";
                        }
                    }
                    btnAsignar.Enabled = true;
                }
                else
                {
                    //btnAsignar.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                mensaje(ex.Message, "errorMsg");
            }
        }        

        protected void btnAsignar_Click(object sender, EventArgs e)
        {
            try
            {
                btnBuscar_Click(sender, e);
                string[] strIdClienteDetalle = hdIdClienteDetalle.Value.Split(',');
                string strIdClienteContactoEvento = hdIdClienteContactoEvento.Value;
                int respModificarcClienteDetalle = 0;
                for (int l = 0; l < (strIdClienteDetalle.Length - 1); l++)
                {
                    wscClienteDetalle.IcClienteDetalleClient IcClienteContactoDetalle = new wscClienteDetalle.IcClienteDetalleClient();
                    IcClienteContactoDetalle.Open();
                    wscClienteDetalle.cClienteDetalle cClienteDetalle;
                    cClienteDetalle = IcClienteContactoDetalle.SeleccionarcClienteDetallePorId(int.Parse(strIdClienteDetalle[l]), getDsSession());
                    /* 17	Registrado
                     * 18	Sorteado
                       15	Inscrito */
                    cClienteDetalle.idEstatusCliente = 18;
                    respModificarcClienteDetalle = cParticipante.ModificaClienteDetalleCompetidor(cClienteDetalle);
                    IcClienteContactoDetalle.Close();
                }
                if (respModificarcClienteDetalle > 0)
                {
                    string strAbsoluteUri = HttpContext.Current.Request.Url.AbsoluteUri;
                    string strHost = HttpContext.Current.Request.Url.Host;
                    string[] strspliturl = strAbsoluteUri.Split('/');
                    strAbsoluteUri = strAbsoluteUri.Replace(strspliturl[strspliturl.Length - 1], "");
                    strAbsoluteUri = strAbsoluteUri.Replace(strspliturl[strspliturl.Length - 2] + "/", "");
                    strAbsoluteUri += "inscripcionSorteados.aspx?contEvent=" + hdIdClienteContactoEvento.Value;

                    wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                    wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                    clienteContactoEventoClient.Open();
                    cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(strIdClienteContactoEvento), getDsSession());
                    cCompetidor.dsLiga = strAbsoluteUri;
                    if (cCompetidor.idTipoSangre == 0)
                        cCompetidor.idTipoSangre = null;
                    if (cCompetidor.idCategoria == 0)
                        cCompetidor.idCategoria = null;

                    int respuestaModificaContactoEvento = clienteContactoEventoClient.ModificarcClienteContactoEvento(cCompetidor, getDsSession());
                    clienteContactoEventoClient.Close();
                    // enviar correo al cliente de que fue ganador de la rifa
                    if (lblCorreo.Text != "")
                    {
                        string strNombreCompleto = lblNombre.Text;
                        string mensajeMail = cGlobals.notifGanadorRifa;
                        mensajeMail = mensajeMail.Replace("[nombre]", strNombreCompleto);
                        bool respEnvioCorreo = utils.enviaCorreo(lblCorreo.Text, "Sorteo de la rifa Triatlón Xel-Há", mensajeMail, "");
                    }
                    //Response.Redirect(Request.Url.AbsoluteUri);
                    
                    hplRutaSorteado.Visible = true;
                    hplRutaSorteado.Text = strAbsoluteUri;
                    hplRutaSorteado.NavigateUrl = strAbsoluteUri;
                    txtNoRifa.Text = "";
                    txtNoRifa.Focus();
                    mensaje("competidor asignado", "successMsg");
                    // mensaje("Competidor asignado correctamente", "successMsg");
                }
                else                
                {
                    mensaje("Error al asignar al cliente", "errorMsg");
                }


            }
            catch (Exception ex)
            {
                //mensaje(ex.Message, "errorMsg");
            }
        }        

        public void mensaje(string mensaje, string type)
        {
            txtNoRifa.Text = "";
            txtNoRifa.Focus();
            string strTitutlo = "";
            if (type == "errorMsg")
                strTitutlo = "Ha ocurrido un error";
            if (type == "successMsg")
                strTitutlo = "Cambios realizados satisfactoriamente";
            if (type == "warningMsg")
                strTitutlo = "ATENCION";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('" + type + "','" + strTitutlo + "','" + mensaje + "');</script>", false);
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.dsSession;
        }
    }
}