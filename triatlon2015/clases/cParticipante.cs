﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

namespace travesia
{
    public class cParticipante
    {
        public int idClienteContacto;
        public int idClienteContactoEvento;
        public string nombre;
        public string apellidoPaterno;
        public string apellidoMaterno;
        public string email;
        public int idSexo;
        public string fechaNacimiento;
        public string calle;
        public string colonia;
        public string ciudad;
        public string pais;
        public int idPais; 
        public string estado;
        public int idEstado;
        public string municipio;
        public string numInterior;
        public string numExterior;
        public string telefono1;
        public string telefono2;
        public int idEventoClasificacion;
        public string dsEventoClasificacion;
        public string dsCodigoEventoClasificacion;
        public string dsFolioFmtri;
        public int idEventoModalidad;
        public int idProducto;
        public string dsProducto;
        public string dsCategoriar;       
        public int vecesParticipaciones;
        public int idTipoMoneda;
        public int noRifa;
        public int noCompetidor;
        public int cnNatacion;
        public int cnCarrera;
        public int cnCiclismo;
        public int cnEquipo;
        public string dsClaveVenta;
        public int idVenta;
        public int idEstatusVenta;
        public int cnRequiereFactura;
        public int idVentaDetalle;
        public int idProductoPrecio;
        public int idTipoCliente;
        public string dsNombreVisitante;
        public int noPax;
        public int idTallaPlayera;
        public string dsTallaPlayera;
        public int formaPago;
        public int idClienteFacturar;
        public string dsAlias;
        public string dsPadecimientos;
        public string dsAgrupador;
        public int idTipoSangre;
        public int idEstatusCliente;
        public int edad;
        public int idClienteContactoEventoPapa;
        public static int cnPaga;
        public static int cnCena;
        public decimal peso;
        public string aniosParticipaciones;
        public bool cnPracticaDeporte;
        public string dsDetalleDeporte;
        public string dsFrecuenciaDeporte;
        public bool cnEsColaborador;
        public string noColaborador;
        public int idUnidadNegocioColaborador;
        public int idLocacion;
        public int idClienteContactoEmergencia;
        public int idParentesco;
       //public string dsNumeroCompetidor;
        public string dsMotivoParticipacion;
        public string dsSignificadoTsm;
        public int idNivelNatacion;
        public bool cnVegetariano;

        public static DataTable dtCompedidor;
        private DataTable dtCompetidor;

        public DataTable DtCompetidor
        {
            get { return dtCompetidor; }
            set { dtCompetidor = value; }
        }
        public static DataTable dtDataCobro;

        private DataTable dtCobro;

        public DataTable DtCobro
        {
            get { return dtCobro; }
            set { dtCobro = value; }
        }

        public List<cParticipante> listCompetidores = new List<cParticipante>();

        public void llenarLista(cParticipante datos)
        {
            listCompetidores.Add(datos);
        }

        public void VaciarLista()
        {
            listCompetidores.Clear();
        }

        public List<object> guardarCompetidor(List<cParticipante> datos, string dsSession, int idCliente, int idUsuario)
        {
            List<object> listResp = new List<object>();
            listResp.Clear();
            
                int idContactoEvento = 0;
                if (datos.Count > 0)
                {
                    for (int i = 0; i < datos.Count; i++)
                    {
                        wscClienteDetalle.cClienteDetalle ClienteDet = new wscClienteDetalle.cClienteDetalle();
                        ClienteDet.cnPaqueteBienvenida = true;
                        ClienteDet.feAlta = DateTime.Now;
                        ClienteDet.idCliente = idCliente;// Globals.ClienteIdCliente;
                        ClienteDet.idClienteUsuarioAlta = idUsuario;
                        ClienteDet.cnFacturacionDefault = false;
                        ClienteDet.dsCalle = datos[i].calle;
                        ClienteDet.dsColonia = datos[i].colonia;
                        ClienteDet.dsNumExterior = datos[i].numExterior;
                        ClienteDet.dsNumInterior = datos[i].numInterior;
                        ClienteDet.dsCiudad = datos[i].municipio;
                        ClienteDet.idEstatusCliente =  datos[i].idEstatusCliente;
                        if (datos[i].idEstado != null && datos[i].idEstado !=0)
                        {
                            ClienteDet.idEstado = datos[i].idEstado;
                        }
                        if (datos[i].idPais != null && datos[i].idPais != 0)
                        {
                            ClienteDet.idPais = datos[i].idPais;
                        }
                        ClienteDet.idClienteDetalle = 0;
                        wscClienteDetalle.IcClienteDetalleClient cteCLiente = new wscClienteDetalle.IcClienteDetalleClient();
                        int idObjeto = cteCLiente.InsertarcClienteDetalle(ClienteDet, dsSession);
                        ClienteDet.idClienteDetalle = idObjeto;
                        cteCLiente.Close();
                        // YA SE INSERTO EL DETALLE DEL CLIENTE, AHORA VAMOS CON EL CLIENTECONTACTO

                        wscClienteContacto.IcClienteContactoClient contactoclient = new wscClienteContacto.IcClienteContactoClient();
                        wscClienteContacto.cClienteContacto elObjeto = new wscClienteContacto.cClienteContacto();
                        wscClienteDetalle.IcClienteDetalleClient detalleclient = new wscClienteDetalle.IcClienteDetalleClient();
                        wscClienteDetalle.ListacClienteDetalles listadetalle = new wscClienteDetalle.ListacClienteDetalles();

                        detalleclient.Close();

                        elObjeto.idClienteContacto = 0;
                        if (datos[i].idClienteFacturar != null && datos[i].idClienteFacturar != 0)
                        {
                            elObjeto.idCliente = datos[i].idClienteFacturar;                            
                        }
                        else
                            elObjeto.idCliente = 1;
                        elObjeto.idClienteDetalle = idObjeto;
                        elObjeto.dsContacto = datos[i].nombre;
                        elObjeto.dsApellidoPaterno = datos[i].apellidoPaterno;
                        elObjeto.dsApellidoMaterno = datos[i].apellidoMaterno;
                        if (datos[i].fechaNacimiento != null && datos[i].fechaNacimiento != "")
                            elObjeto.feNacimiento = DateTime.Parse(datos[i].fechaNacimiento);

                        elObjeto.dsTelefono = datos[i].telefono1;
                        elObjeto.dsCelular = datos[i].telefono2;
                        elObjeto.dsCorreoElectronico = datos[i].email;

                        HttpContext.Current.Session["Email"] = datos[0].email;

                        elObjeto.cnActivo = true;
                        elObjeto.cnNewsletter = false;
                        elObjeto.cnContactoDefault = true;
                        elObjeto.feAlta = DateTime.Now;
                        elObjeto.idClienteUsuarioAlta = idUsuario;
                        elObjeto.idTipoContacto = 1;
                        elObjeto.idTituloContacto = null;
                        if (datos[i].idSexo != null && datos[i].idSexo != 0)
                            elObjeto.idSexo = datos[i].idSexo;

                        if (datos[i].idPais != null && datos[i].idPais != 0)
                            elObjeto.idPais = datos[i].idPais;
                        else
                            elObjeto.idPais = null;
                        if (datos[i].idEstado != null && datos[i].idEstado != 0)
                            elObjeto.idEstado = datos[i].idEstado;
                        else
                            elObjeto.idEstado = null;

                        int objetoContactoGuardado = contactoclient.InsertarcClienteContacto(elObjeto, dsSession);
                        if (objetoContactoGuardado > 0)
                        {
                            elObjeto.idClienteContacto = objetoContactoGuardado;
                        }
                        contactoclient.Close();
                        // YA SE GUARDO EL CLIENTE CONTACTO AHORA VA EN CLIENTE CONTACTO EVENTO
                        wsBusinessRules.BusinessRulesServicesClient traerPago = new wsBusinessRules.BusinessRulesServicesClient();
                        
                        string feAlta = DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Year.ToString();
                        Dictionary<string, string> dicret = new Dictionary<string, string>();
                        //string fullname = this.nombre + " " + this.apellidoPaterno + " " + this.apellidoMaterno;

                        wscClienteContactoEvento.IcClienteContactoEventoClient eventoclient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                        wscClienteContactoEvento.cClienteContactoEvento EVentoComp = new wscClienteContactoEvento.cClienteContactoEvento();

                        eventoclient.Open();
                        EVentoComp.idClienteContacto = objetoContactoGuardado;
                        //EVentoComp.idConfigPagoPreregistro;
                        if (datos[i].idEventoClasificacion != null && datos[i].idEventoClasificacion !=0)
                            EVentoComp.idEventoClasificacion = datos[i].idEventoClasificacion;
                        if (datos[i].idEventoModalidad != null && datos[i].idEventoModalidad != 0)
                            EVentoComp.idEventoModalidad = datos[i].idEventoModalidad;
                        else
                            EVentoComp.idEventoModalidad = 2;
                        EVentoComp.idSexo = datos[i].idSexo;
                        if (datos[i].vecesParticipaciones != null && datos[i].vecesParticipaciones != 0)
                            EVentoComp.noFrecuencia = datos[i].vecesParticipaciones;
                        if (datos[i].noRifa != null && datos[i].noRifa != 0)
                        {
                            EVentoComp.NoRifa = datos[i].noRifa;
                            EVentoComp.dsNoRifa = datos[i].dsCodigoEventoClasificacion + datos[i].noRifa.ToString();
                        }
                        if (datos[i].idTallaPlayera != null && datos[i].idTallaPlayera != 0)
                            EVentoComp.idTalla = datos[i].idTallaPlayera;
                        if (datos[i].edad != null && datos[i].edad != 0)
                            EVentoComp.noEdad = datos[i].edad;
                        if (datos[i].idTipoSangre != null && datos[i].idTipoSangre != 0)
                            EVentoComp.idTipoSangre = datos[i].idTipoSangre;
                        if (datos[i].dsFolioFmtri != null && datos[i].dsFolioFmtri != "")
                            EVentoComp.dsFolioFmtri = datos[i].dsFolioFmtri;
                        if (datos[i].dsAlias != null && datos[i].dsAlias != null)
                            EVentoComp.dsAlias = datos[i].dsAlias;
                        if (datos[i].formaPago != null && datos[i].formaPago != 0)
                            EVentoComp.idConfigPagoPreregistro = datos[i].formaPago;
                        
                        //EVentoComp.dsAgrupador = "";
                        if (datos[i].dsAgrupador != "")
                            EVentoComp.dsAgrupador = datos[i].dsAgrupador;
                        EVentoComp.idClienteUsuarioAlta = idUsuario;
                        EVentoComp.feAlta = DateTime.Now;
                        //EVentoComp.dsAlergia = datos[i].dsPadecimientos;
                        EVentoComp.dsPadecimiento = datos[i].dsPadecimientos;
                        if (datos[i].cnCarrera != null && datos[i].cnCarrera != 0)
                            EVentoComp.cnCarrera = true;
                        if (datos[i].cnCiclismo != null && datos[i].cnCiclismo != 0)
                            EVentoComp.cnCiclismo = true;
                        if (datos[i].cnNatacion != null && datos[i].cnNatacion != 0)
                            EVentoComp.cnNatacion = true;
                        if (datos[i].idClienteContactoEventoPapa != null && datos[i].idClienteContactoEventoPapa != 0)
                        {
                            EVentoComp.idEquipo = datos[i].idClienteContactoEventoPapa;
                        }                       
                        else if (i > 0)
                        {
                            EVentoComp.idEquipo = int.Parse(listResp[0].ToString());
                        }
                        

                        //EVentoComp.dsAniosParticipaciones = datos[i].dsa
                        EVentoComp.noPeso = datos[i].peso;
                        EVentoComp.cnPracticaDeporte = datos[i].cnPracticaDeporte;
                        EVentoComp.dsFrecuenciaDeporte = datos[i].dsFrecuenciaDeporte;
                        EVentoComp.cnEsColaborador = datos[i].cnEsColaborador;
                        if (datos[i].noColaborador != null)
                            EVentoComp.noColaborador = int.Parse(datos[i].noColaborador);
                        if (datos[i].idUnidadNegocioColaborador > 0)
                            EVentoComp.idUnidadNegocioColaborador = datos[i].idUnidadNegocioColaborador;                      
                        EVentoComp.idClienteContactoEmergencia = datos[i].idClienteContactoEmergencia;
                        EVentoComp.idParentesco = datos[i].idParentesco;
                        EVentoComp.dsAniosParticipaciones = datos[i].aniosParticipaciones;
                        EVentoComp.idLocacion = datos[i].idLocacion;
                        //EVentoComp.dsNumeroCompetidor = ;

                        EVentoComp.dsMotivoParticipacion = datos[i].dsMotivoParticipacion;
                        EVentoComp.idNivelNatacion = datos[i].idNivelNatacion;
                        EVentoComp.dsSignificadoTSM = datos[i].dsSignificadoTsm;
                        EVentoComp.cnVegetariano = datos[i].cnVegetariano;
                        idContactoEvento = eventoclient.InsertarcClienteContactoEvento(EVentoComp, dsSession);
                        eventoclient.Close();
                        if (i == 0)
                        {
                            listResp.Add(idContactoEvento);
                            listResp.Add(datos[i].noRifa);
                            listResp.Add(datos[i].dsCodigoEventoClasificacion + datos[i].noRifa.ToString());
                        }
                    }// fin del for
                }                                                           
                return listResp;
            
        }

        public List<object> guardarContacto(List<cParticipante> datos, string dsSession, int idCliente, int idUsuario)
        {
            List<object> listResp = new List<object>();
            listResp.Clear();

            int idContactoEvento = 0;
            if (datos.Count > 0)
            {
                for (int i = 0; i < datos.Count; i++)
                {
                    wscClienteDetalle.cClienteDetalle ClienteDet = new wscClienteDetalle.cClienteDetalle();
                    ClienteDet.cnPaqueteBienvenida = true;
                    ClienteDet.feAlta = DateTime.Now;
                    ClienteDet.idCliente = idCliente;// Globals.ClienteIdCliente;
                    ClienteDet.idClienteUsuarioAlta = idUsuario;
                    ClienteDet.cnFacturacionDefault = false;                    
                    ClienteDet.idEstatusCliente = datos[i].idEstatusCliente;                    
                    ClienteDet.idClienteDetalle = 0;
                    wscClienteDetalle.IcClienteDetalleClient cteCLiente = new wscClienteDetalle.IcClienteDetalleClient();
                    int idObjeto = cteCLiente.InsertarcClienteDetalle(ClienteDet, dsSession);
                    ClienteDet.idClienteDetalle = idObjeto;
                    cteCLiente.Close();
                    // YA SE INSERTO EL DETALLE DEL CLIENTE, AHORA VAMOS CON EL CLIENTECONTACTO

                    wscClienteContacto.IcClienteContactoClient contactoclient = new wscClienteContacto.IcClienteContactoClient();
                    wscClienteContacto.cClienteContacto elObjeto = new wscClienteContacto.cClienteContacto();
                    wscClienteDetalle.IcClienteDetalleClient detalleclient = new wscClienteDetalle.IcClienteDetalleClient();
                    wscClienteDetalle.ListacClienteDetalles listadetalle = new wscClienteDetalle.ListacClienteDetalles();

                    detalleclient.Close();

                    elObjeto.idClienteContacto = 0;
                    if (datos[i].idClienteFacturar != null && datos[i].idClienteFacturar != 0)
                    {
                        elObjeto.idCliente = datos[i].idClienteFacturar;
                    }
                    else
                        elObjeto.idCliente = 1;
                    elObjeto.idClienteDetalle = idObjeto;
                    elObjeto.dsContacto = datos[i].nombre;
                    elObjeto.dsApellidoPaterno = datos[i].apellidoPaterno;
                    elObjeto.dsApellidoMaterno = datos[i].apellidoMaterno;
                    /*if (datos[i].fechaNacimiento != null && datos[i].fechaNacimiento != "")
                        elObjeto.feNacimiento = DateTime.Parse(datos[i].fechaNacimiento);*/

                    elObjeto.dsTelefono = datos[i].telefono1;
                    elObjeto.dsCelular = datos[i].telefono2;                    

                    HttpContext.Current.Session["Email"] = datos[0].email;
                    elObjeto.cnActivo = true;
                    elObjeto.cnNewsletter = false;
                    elObjeto.cnContactoDefault = true;
                    elObjeto.feAlta = DateTime.Now;
                    elObjeto.idClienteUsuarioAlta = idUsuario;
                    elObjeto.idTipoContacto = 1;
                    elObjeto.idTituloContacto = null;                    
                    int objetoContactoGuardado = contactoclient.InsertarcClienteContacto(elObjeto, dsSession);                   
                    contactoclient.Close();                                       
                    listResp.Add(objetoContactoGuardado);
                    // YA SE GUARDO EL CLIENTE CONTACTO AHORA VA EN CLIENTE CONTACTO EVENTO
                   
                }// fin del for
            }
            return listResp;

        }

        public static int actualizaClienteContactoEvento(wscClienteContactoEvento.cClienteContactoEvento cCompetidor)
        {
            int respuestaModificaContactoEvento = 0;
            wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
            clienteContactoEventoClient.Open();
            respuestaModificaContactoEvento = clienteContactoEventoClient.ModificarcClienteContactoEvento(cCompetidor, cGlobals.dsSession);
            clienteContactoEventoClient.Close();
            return respuestaModificaContactoEvento;
        }

        public static string actualizaCompetidor(wscClienteContactoEvento.cClienteContactoEvento competidor, wscClienteContacto.cClienteContacto cClienteContacto, bool cnCambiaClienteContactoEvento = false)
        {
            try
            {
                wscClienteContacto.IcClienteContactoClient clienteContacto = new wscClienteContacto.IcClienteContactoClient();
                wscClienteContactoEvento.IcClienteContactoEventoClient CompetidorClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                clienteContacto.Open();
                int respuestaModificaContactoEvento = 0;
                int intRespModificaCompetidor = clienteContacto.ModificarcClienteContacto(cClienteContacto, cGlobals.dsSession);
                // si se realizo un cambio que incluye a la cClienteContactoEvento, entonces modificamos, si no 
                // mejor ahorramos chamba a nuestra super base de datos
                if (cnCambiaClienteContactoEvento)
                {
                   respuestaModificaContactoEvento = CompetidorClient.ModificarcClienteContactoEvento(competidor, cGlobals.dsSession);
                   return "1";
                }
                else
                {
                    return "1";
                }                
            }
            catch (Exception ex)
            {                
                return ex.Message;
            }
        }

        public static int ModificaClienteDetalleCompetidor(wscClienteDetalle.cClienteDetalle cClienteDetalle)
        {
            wscClienteDetalle.IcClienteDetalleClient IcClienteContactoDetalle = new wscClienteDetalle.IcClienteDetalleClient();
            IcClienteContactoDetalle.Open();
            int respuestaModifica = IcClienteContactoDetalle.ModificarcClienteDetalle(cClienteDetalle, cGlobals.dsSession);
            IcClienteContactoDetalle.Close();
            return respuestaModifica;
        }

        public static string agregaNoUsados(string strDsClave, int intCantidad, int idLocacion = 0, int idEventoClasificacion = 0)
        {
            try
            {
                wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();
                Dictionary<string, string> dicret = new Dictionary<string, string>();
                dicret.Add("@cantidad", intCantidad.ToString());
                dicret.Add("@idLocacion", idLocacion.ToString());
                dicret.Add("@idEventoClasificacion", idEventoClasificacion.ToString());
                DataSet datos = reglas.ExecuteRule("spAgregaNoUsadosTravesia", dicret, cGlobals.dsSession);
                return "1";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static DataSet getPreInscritos(string dsSession, string strPaterno, string strMaterno = "")
        {
            wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();
            Dictionary<string, string> dicret = new Dictionary<string, string>();
            dicret.Clear();
            dicret.Add("@dsApellidoP", strPaterno);
            dicret.Add("@dsApellidoM", strMaterno);
            DataSet datos = reglas.ExecuteRule("spGETcClienteContactoTriatlon", dicret, dsSession);
            dicret.Clear();
            reglas.Close();
            return datos;
        }
    }    

    public static class listParticipantes
    {
 
    }
}