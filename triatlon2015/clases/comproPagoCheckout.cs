﻿using System;
using System.IO;
using System.Web;
using System.Net;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace travesia
{
    public class comproPagoCheckout
    {
        public static string makeRequest(string url, string postData)
        {
            string response = string.Empty;
            ASCIIEncoding encoding = new ASCIIEncoding();

            Stream streamRequest;
            StreamReader responseRead;
            HttpWebRequest webrequest;
            HttpWebResponse webresponse;

            byte[] data = encoding.GetBytes(postData);
            webrequest = (HttpWebRequest)WebRequest.Create(url);

            webrequest.Method = "POST";
            webrequest.ContentType = "application/x-www-form-urlencoded";

            webrequest.ContentLength = data.Length;

            streamRequest = webrequest.GetRequestStream();

            streamRequest.Write(data, 0, data.Length);
            streamRequest.Close();

            webresponse = (HttpWebResponse)webrequest.GetResponse();

            Encoding encode = Encoding.GetEncoding("utf-8");

            responseRead = new StreamReader(webresponse.GetResponseStream(), encode);

            response = responseRead.ReadToEnd();

            responseRead.Close();
            webresponse.Close();

            response = response.Replace("&lt;", "<");
            response = response.Replace("&gt;", ">");

            response = response.Replace("</string>", "");
            response = response.Substring(response.IndexOf('>') + 1);

            return response;
        }

        public static string getBusinessEstablishments(string dsSessionId, string db, string strTotal)
        {
            string response;
            string establishments = string.Empty;

            StringBuilder postValues = new StringBuilder();

            postValues.Append("{");
            postValues.Append(string.Format("\"{0}\":\"{1}\",", "importe", strTotal));
            postValues.Append(string.Format("\"{0}\":\"{1}\",", "session", dsSessionId));
            postValues.Append(string.Format("\"{0}\":\"{1}\"", "produccion", db));
            postValues.Append("}");

            response = makeRequest("http://www.aolxcaret.com/core/Sivex.Pagos/ComproPago.svc/GetComproPagoProviders", postValues.ToString());

            if (response.ToUpper().IndexOf("ERROR") == -1)
            {
                StringBuilder strAux;
                List<Dictionary<string, string>> providersList;
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

                providersList = jsSerializer.Deserialize<List<Dictionary<string, string>>>(response);

                int i = 0;
                HtmlTextWriter writerDom;
                StringWriter strWriter;

                strWriter = new StringWriter();
                writerDom = new HtmlTextWriter(strWriter);
                /*writerDom.AddAttribute(HtmlTextWriterAttribute.Class, "row");
                writerDom.RenderBeginTag(HtmlTextWriterTag.Div);
                writerDom.Indent++;*/
                writerDom.AddAttribute(HtmlTextWriterAttribute.Class, "col-md-12");

                writerDom.RenderBeginTag(HtmlTextWriterTag.Div);
                writerDom.Indent++;
                writerDom.AddAttribute(HtmlTextWriterAttribute.Class, "payment text--aling-center");
                writerDom.RenderBeginTag(HtmlTextWriterTag.Div);
                writerDom.Indent++;

                foreach (Dictionary<string, string> provider in providersList)
                {
                    strAux = new StringBuilder();

                    strAux.Append("compropago|");
                    strAux.Append(provider["internal_name"]);
                    strAux.Append("|");
                    strAux.Append(provider["name"]);

                    writerDom.AddAttribute(HtmlTextWriterAttribute.Class, "" + provider["name"].ToLower());
                    writerDom.AddStyleAttribute(HtmlTextWriterStyle.Padding, "2px");
                    writerDom.AddStyleAttribute(HtmlTextWriterStyle.Display, "inline-block");
                    writerDom.RenderBeginTag(HtmlTextWriterTag.Div);

                    writerDom.Indent++;

                    writerDom.AddAttribute(HtmlTextWriterAttribute.Type, "radio");
                    writerDom.AddAttribute(HtmlTextWriterAttribute.Name, "rblTipoTarjeta");
                    writerDom.AddAttribute(HtmlTextWriterAttribute.Class, "fcheckCompropago");
                    writerDom.AddStyleAttribute(HtmlTextWriterStyle.Display, "inline-block");
                    writerDom.AddAttribute(HtmlTextWriterAttribute.Value, strAux.ToString());
                    //writerDom.AddAttribute("runat", "server");
                    writerDom.RenderBeginTag(HtmlTextWriterTag.Input);
                    writerDom.RenderEndTag();

                    writerDom.AddAttribute("class", "image");
                    writerDom.AddStyleAttribute(HtmlTextWriterStyle.Width, "65px");
                    writerDom.AddStyleAttribute(HtmlTextWriterStyle.Height, "50px");
                    writerDom.AddAttribute(HtmlTextWriterAttribute.Src, provider["image_medium"]);
                    writerDom.AddStyleAttribute(HtmlTextWriterStyle.Display, "inline-block");
                    writerDom.RenderBeginTag(HtmlTextWriterTag.Img);
                    writerDom.RenderEndTag();

                    writerDom.Indent--;
                    writerDom.RenderEndTag();

                    i++;

                    if (i == 6)
                    {
                        writerDom.Indent--;
                        writerDom.RenderEndTag();
                        writerDom.Indent--;
                        writerDom.RenderEndTag();

                        /*writerDom.AddAttribute("class", "row");
                        writerDom.RenderBeginTag(HtmlTextWriterTag.Div);                        
                        writerDom.Indent++;*/
                        writerDom.AddAttribute(HtmlTextWriterAttribute.Class, "col-md-12");
                        writerDom.RenderBeginTag(HtmlTextWriterTag.Div);
                        writerDom.Indent++;
                        writerDom.AddAttribute("class", "payment text--aling-center");
                        writerDom.RenderBeginTag(HtmlTextWriterTag.Div);
                        writerDom.Indent++;
                        i = 0;
                    }
                }

                writerDom.Indent--;
                writerDom.RenderEndTag();
                writerDom.Indent--;
                writerDom.RenderEndTag();

                establishments = strWriter.ToString();
            }

            return establishments;
        }

        public static List<Dictionary<string, string>> getBusinessEstablishments2(string dsSessionId, string db, string strTotal)
        {
            string response;
            string establishments = string.Empty;

            StringBuilder postValues = new StringBuilder();
            List<Dictionary<string, string>> providersList = new List<Dictionary<string, string>>();
            postValues.Append("{");
            postValues.Append(string.Format("\"{0}\":\"{1}\",", "importe", strTotal));
            postValues.Append(string.Format("\"{0}\":\"{1}\",", "session", dsSessionId));
            postValues.Append(string.Format("\"{0}\":\"{1}\"", "produccion", db));
            postValues.Append("}");

            response = makeRequest("https://www.xperienciasxcaret.com/core/Sivex.Pagos/ComproPago.svc/GetComproPagoProviders", postValues.ToString());

            if (response.ToUpper().IndexOf("ERROR") == -1)
            {
                StringBuilder strAux;
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                string strCuerpoHtml = "";
                providersList = jsSerializer.Deserialize<List<Dictionary<string, string>>>(response);
            }
            return providersList;
        }
        public static string getOrder(string dsSessionId, string db, string idVenta, string provider, string strTotal, string idCanalVenta)
        {
            string response = "200";
            string detail = "";

            Dictionary<string, string> orderDetails;
            StringBuilder postValues = new StringBuilder();

            #region split venta
            int idventa = Int32.Parse(idVenta);
            int idCanal = Int32.Parse(idCanalVenta);

            wsTransactions.IkVentaClient iVenta = new wsTransactions.IkVentaClient();
            wsTransactions.kVenta kVenta = new wsTransactions.kVenta();
            wsTransactions.IkWorkersClient kWorker = new wsTransactions.IkWorkersClient();
            wsTransactions.Listakventamultifolio lstMultiFolio = new wsTransactions.Listakventamultifolio();


            //lstMultiFolio = kWorker.GeneraVentaMultiFolio(idventa, idCanalVenta,dsSessionId);

            if (lstMultiFolio.Count() > 0)
            {
                idVenta = string.Empty;
                for (int i = 0; i < lstMultiFolio.Count(); i++)
                {
                    kVenta = iVenta.SeleccionarkVentaPorIdVenta((int)lstMultiFolio[i].idVenta,
                                                            dsSessionId);

                    idVenta += lstMultiFolio[i].idVenta.ToString() + ",";
                }
            }
            #endregion

            postValues.Append("{");
            postValues.Append(string.Format("\"{0}\":\"{1}\",", "idventa", idVenta));
            postValues.Append(string.Format("\"{0}\":\"{1}\",", "agrupadorventa", "1"));
            postValues.Append(string.Format("\"{0}\":\"{1}\",", "importe", strTotal));
            postValues.Append(string.Format("\"{0}\":\"{1}\",", "proveedor", provider.Split('|')[1].Trim().ToUpper()));
            postValues.Append(string.Format("\"{0}\":\"{1}\",", "session", dsSessionId));
            postValues.Append(string.Format("\"{0}\":\"{1}\"", "produccion", db));
            postValues.Append("}");

            response = makeRequest("https://www.xperienciasxcaret.com/core/Sivex.Pagos/ComproPago.svc/ComproPagoPlaceOrder", postValues.ToString());

            if (response.ToUpper().IndexOf("ERROR") > -1)
            {
                response = "500";
                detail = response.ToUpper().IndexOf("ERROR").ToString();
                detail = "Error intento de cobro.";
            }

            orderDetails = new Dictionary<string, string>();

            orderDetails.Add("option", provider.Split('|')[1]);
            orderDetails.Add("ticket", response);
            orderDetails.Add("detail", detail);
            //globals.orderComproPago = orderDetails;

            return response;
        }
    }
}