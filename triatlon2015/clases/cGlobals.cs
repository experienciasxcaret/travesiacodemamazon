﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace travesia
{
    #region cGlobals
    public class cGlobals
    {
        public static string notifRegistroRifa_1 = @"<p><strong>Confirmación de registro para la rifa </strong></p>
   <br />
  <p>Estimado/a: <strong>[nombre] </strong></p>
  <p>Número para rifa: <b>[numero]</b></p>

<p>¡Muchas gracias por tu interés!</p>
  <p>Te confirmamos que tu registro a la rifa del <strong>Triatlón Xel-Há 2015</strong> fue exitoso. <strong>La rifa se realizará el día sábado 5 de Septiembre del 2015</strong>, en las instalaciones del parque Xel-Há ante notario público. Podrás ver los  resultados en nuestra página web <a href='http://www.triatlonxelha.com'>www.triatlonxelha.com</a> </p>
    
<p>En caso de salir premiado te enviaremos un correo electrónico a la cuenta que usaste para registrarte. Recuerda por favor que tu participación a la rifa es intransferible, solo las personas registradas en la rifa podrán inscribirse al Triatlón y competir en el mismo; si tienes alguna duda sobre el procedimiento, favor de consultar <a href='http://www.triatlonxelha.com/registro-a-la-rifa.php'>este espacio</a>.</p>  

<p><b>Las inscripciones al Triatlón serán del 7 al 19 de septiembre</b>; ¡de antemano te deseamos la mejor de las suertes!</p>

<p><em>Este mensaje ha sido generado automáticamente, no intente responder al remitente.</em></p>"
        ;

        public static string notifGanadorRifa = @"<p>Muchas felicidades, [nombre]</p>
<p>
  ¡Has ganado una oportunidad para inscribirte  al Triatlón Xel-Há!<br />
  Pronto estarás recibiendo un correo con las  instrucciones necesarias para tu inscripción.<br />
  Recuerda que tu inscripción al Triatlón es  intransferible, solo tú podrás competir en el mismo. <br />
  Las inscripciones al Triatlón serán del 7 al  19 de septiembre. ¡Te esperamos!<br />
  Este mensaje ha sido generado automáticamente, no intente responder al  remitente.</p>
<p>&nbsp;</p>"
        ;

        public static string notifNoGanadorRifa2 = @"<p>Estimado, [nombre]</p>
        <p>
          Tu número de rifa [numero] no resultó ganador del sorteo para el Triatlón de Xel-Há, <br />
          sin embargo aún tienes la oportunidad de participar inscribiéndote en nuestra página web <br />
          por medio de la cena a beneficio de la Cruz Roja. ¡Estos lugares tienen cupo limitado! <br /></p>
        <p>Este mensaje ha sido generado  automáticamente, no intente responder al remitente.</p>";

        public static string notifPerdedorRifa = @"<p>Estimado [nombre]:</p>
<p>Tu número  de rifa no  resultó ganador.<br/>
Agradecemos tu interés y entusiasmo, y ¡te invitamos a participar el  próximo año!<br/>
Recibe un cordial saludo de  la Familia Xel-Há.<br />
Comité Organizador  Triatlón Xel-Há </p>
<p>Este mensaje ha sido generado  automáticamente, no intente responder al remitente.</p>";

        public static string notifIncripcionDirectos = @"<p>Estimado [nombre]:</p>
<p>¡Felicidades! &nbsp;Te confirmamos  que hemos reservado un lugar para ti en el Triatlón Xel Há.</p>
<p>Para que quedes inscrito te pedimos  que ingreses&nbsp;al siguiente link &nbsp;<a href='[url]' target='_blank'>[url]</a>&nbsp;y completes  tus datos.&nbsp; </p>
<p><strong>Tienes 5 días para inscribirte a  partir de hoy.</strong>&nbsp;De no hacerlo dentro de esta fecha perderás tu lugar y no podrás  reclamarlo posteriormente.&nbsp;Recuerda que esta invitación no es  transferible.</p>
<p>¡Te esperamos en la máxima justa  deportiva del Caribe Mexicano en Xel-Há!<br />
<br />
Este Mensaje ha sido generado automáticamente, no intentes responder al  remitente.</p>";

        public static string notifClienteNuevo = @"
            <title>::Experiencias Xcaret::</title>

            <h2>NUEVO CLIENTE ESPECIAL TRIATLON.</h2>
            <p style='font-size:16px'>
                Se ha afiliado un nuevo cliente al sistema.<br/>
                <b>NOMBRE: </b>  [nombre] .<br/>
                <b>RAZ&Oacute;N SOCIAL: </b> [razonsocial] <br/>
                <b>RFC:  </b> [RFC] <br/><br/>                
            </p>
            <br/>
            Este Mensaje ha sido generado autom&aacute;ticamente, no intentes responder al remitente";




        public static string dsSession
        {
            get
            {

                return (string)HttpContext.Current.Session["_dsSession"];
            }
            set
            {

                HttpContext.Current.Session["_dsSession"] = value;
            }
        }
       
        public static string dsUsuario
        {
            get
            {
                //return _dsUsuario;
                return (string)HttpContext.Current.Session["dsUsuario"];
            }

            set
            {
                // _dsUsuario = value;
                HttpContext.Current.Session["dsUsuario"] = value;

            }
        }

        public static int idUsuario
        {
            get
            {
                //return _idUsuario;
                return (int)HttpContext.Current.Session["idUsuario"];
            }

            set
            {
                //_idUsuario = value;
                HttpContext.Current.Session["idUsuario"] = value;
            }
        }

        public static int idCliente
        {
            get
            {

                return (int)HttpContext.Current.Session["_idCliente"];
            }
            set
            {

                HttpContext.Current.Session["_idCliente"] = value;
            }
        }

        public static string dsNombreUsuario
        {
            get
            {

                return (string)HttpContext.Current.Session["_dsNombreUsuario"];
            }
            set
            {

                HttpContext.Current.Session["_dsNombreUsuario"] = value;
            }
        }

        public static string strIdCanal
        {
            get
            {

                return (string)HttpContext.Current.Session["_strIdCanal"];
            }
            set
            {

                HttpContext.Current.Session["_strIdCanal"] = value;
            }
        }

        public static string dsBasedeDatos
        {
            get
            {

                return (string)HttpContext.Current.Session["_dsBasedeDatos"];
            }
            set
            {

                HttpContext.Current.Session["_dsBasedeDatos"] = value;
            }
        }

        private static string prueba;

        public static string Prueba
        {
            get { return cGlobals.prueba; }
            set { cGlobals.prueba = value; }
        }
    }
    #endregion

    #region cResponseAjax
    public class cResponseAjax
    {
        public string Response;
        public string Response2;
        public string TimeExecuted;
        public string Error;
    }
    #endregion    

    public static class cResultadoGuardadoPago
    {
        public static string strIdClienteTarjeta;
        public static string strIdEstatusPago;
        public static string strDsJustificacion;
        public static string strDsTransaccion;
        public static string strDsCorrelacion;
        public static string strDsRespuesta;
        public static string strDsReferencia;
        public static string strAfiliacion;
        public static string strGlobalCollectActivo;
        public static string strIdBancoReceptor;
        public static string strIdPagoIngenico;
    }

    public class cRespuestaPago
    {
        public string strIdClienteTarjeta;
        public string strIdEstatusPago;
        public string strDsJustificacion;
        public string strDsTransaccion;
        public string strDsCorrelacion;
        public string strDsRespuesta;
        public string strDsReferencia;
        public string strAfiliacion;
        public string strGlobalCollectActivo;
        public string strIdBancoReceptor;
    }

    public class GlobalConstants
    {
        public class Imaging
        {
            public const int ThumbnailDim = 100;
            public const int ProfileDim = 250;
            public const int StandardDim = 450;

            public const string SaveFolderName = "pictures";
            public const string SaveFilesFolderName = "files";
            //public const string SaveFolderNameServerPath = "Travesia\\Storage\\" + SaveFolderName;
            //public const string SaveFilesFolderNameServerPath = "core/archivos/Travesia/Storage/" + SaveFilesFolderName;
            public const string SaveFilesFolderNameServerPath = "Travesia/Storage/" + SaveFilesFolderName;

            //public const string SaveFolderNameServerPath = "Travesia\\Storage\\" + SaveFolderName;
            //public const string SaveFolderNameServerPath = "core/archivos/Travesia/Storage/" + SaveFolderName;
            public const string SaveFolderNameServerPath = "Travesia/Storage/" + SaveFolderName + "/";

            public static string ThumbnailUrlPrefix = GetImageUrlPrefix(ThumbnailDim);
            public static string ProfileUrlPrefix = GetImageUrlPrefix(ProfileDim);
            public static string StandardUrlPrefix = GetImageUrlPrefix(StandardDim);

            public static string ThumbnailServerUrlPrefix =
                GetImageServerUrlPrefix(ThumbnailDim);
            public static string ProfileServerUrlPrefix = GetImageServerUrlPrefix(ProfileDim);
            public static string StandardServerUrlPrefix =
                GetImageServerUrlPrefix(StandardDim);

            public static List<int> ImageDims = new List<int>
                                                    {
                                                        //ThumbnailDim, 
                                                        //ProfileDim, 
                                                        StandardDim
                                                    };

            public static List<string> ImageServerUrlPrefixes =
                GetImageServerUrlPrefixes_List();
            public static List<string> ImageUrlPrefixes = GetImageUrlPrefixes_List();
        }

        private static string GetImageUrlPrefix(int dim)
        {
            return "/" + dim + "x" + dim + "_";
        }

        private static List<string> GetImageUrlPrefixes_List()
        {
            return Imaging.ImageDims.Select(GetImageUrlPrefix).ToList();
        }

        private static string GetImageServerUrlPrefix(int dim)
        {
            return "\\" + dim + "x" + dim + "_";
        }

        private static List<string> GetImageServerUrlPrefixes_List()
        {
            return Imaging.ImageDims.Select(GetImageServerUrlPrefix).ToList();
        }
    }

    public struct ftpSetting
    {
        public string server { get; set; }
        public string username { get; set; }
        public string password { get; set; }        
    }
}