﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace travesia.wscClienteDetalle {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="cClienteDetalle", Namespace="SIVEX.Core.Services.BussinesModel")]
    [System.SerializableAttribute()]
    public partial class cClienteDetalle : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private System.Nullable<int> idClienteDetalleField;
        
        private System.Nullable<int> idClienteField;
        
        private System.Nullable<int> idEstatusClienteField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string dsCalleField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string dsNumInteriorField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string dsNumExteriorField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string dsColoniaField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string dsCiudadField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<int> idEstadoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<int> idPaisField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<int> idUbicacionGeograficaField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string dsCodigoPostalField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string dsNombreSucursalField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string dsRFCField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<int> idClienteTipoPagoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<decimal> mnMonederoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<decimal> mnCreditoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<decimal> mnTipoCambioField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<bool> cnPaqueteBienvenidaField;
        
        private System.Nullable<bool> cnFacturacionDefaultField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string dsNotasField;
        
        private System.Nullable<System.DateTime> feAltaField;
        
        private System.Nullable<System.DateTime> feSuspendidoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<int> idClienteUsuarioAltaField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string dsMunicipioField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string dsPoBoxField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public System.Nullable<int> idClienteDetalle {
            get {
                return this.idClienteDetalleField;
            }
            set {
                if ((this.idClienteDetalleField.Equals(value) != true)) {
                    this.idClienteDetalleField = value;
                    this.RaisePropertyChanged("idClienteDetalle");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=1)]
        public System.Nullable<int> idCliente {
            get {
                return this.idClienteField;
            }
            set {
                if ((this.idClienteField.Equals(value) != true)) {
                    this.idClienteField = value;
                    this.RaisePropertyChanged("idCliente");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public System.Nullable<int> idEstatusCliente {
            get {
                return this.idEstatusClienteField;
            }
            set {
                if ((this.idEstatusClienteField.Equals(value) != true)) {
                    this.idEstatusClienteField = value;
                    this.RaisePropertyChanged("idEstatusCliente");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public string dsCalle {
            get {
                return this.dsCalleField;
            }
            set {
                if ((object.ReferenceEquals(this.dsCalleField, value) != true)) {
                    this.dsCalleField = value;
                    this.RaisePropertyChanged("dsCalle");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public string dsNumInterior {
            get {
                return this.dsNumInteriorField;
            }
            set {
                if ((object.ReferenceEquals(this.dsNumInteriorField, value) != true)) {
                    this.dsNumInteriorField = value;
                    this.RaisePropertyChanged("dsNumInterior");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=5)]
        public string dsNumExterior {
            get {
                return this.dsNumExteriorField;
            }
            set {
                if ((object.ReferenceEquals(this.dsNumExteriorField, value) != true)) {
                    this.dsNumExteriorField = value;
                    this.RaisePropertyChanged("dsNumExterior");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=6)]
        public string dsColonia {
            get {
                return this.dsColoniaField;
            }
            set {
                if ((object.ReferenceEquals(this.dsColoniaField, value) != true)) {
                    this.dsColoniaField = value;
                    this.RaisePropertyChanged("dsColonia");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=7)]
        public string dsCiudad {
            get {
                return this.dsCiudadField;
            }
            set {
                if ((object.ReferenceEquals(this.dsCiudadField, value) != true)) {
                    this.dsCiudadField = value;
                    this.RaisePropertyChanged("dsCiudad");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=8)]
        public System.Nullable<int> idEstado {
            get {
                return this.idEstadoField;
            }
            set {
                if ((this.idEstadoField.Equals(value) != true)) {
                    this.idEstadoField = value;
                    this.RaisePropertyChanged("idEstado");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=9)]
        public System.Nullable<int> idPais {
            get {
                return this.idPaisField;
            }
            set {
                if ((this.idPaisField.Equals(value) != true)) {
                    this.idPaisField = value;
                    this.RaisePropertyChanged("idPais");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=10)]
        public System.Nullable<int> idUbicacionGeografica {
            get {
                return this.idUbicacionGeograficaField;
            }
            set {
                if ((this.idUbicacionGeograficaField.Equals(value) != true)) {
                    this.idUbicacionGeograficaField = value;
                    this.RaisePropertyChanged("idUbicacionGeografica");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=11)]
        public string dsCodigoPostal {
            get {
                return this.dsCodigoPostalField;
            }
            set {
                if ((object.ReferenceEquals(this.dsCodigoPostalField, value) != true)) {
                    this.dsCodigoPostalField = value;
                    this.RaisePropertyChanged("dsCodigoPostal");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=12)]
        public string dsNombreSucursal {
            get {
                return this.dsNombreSucursalField;
            }
            set {
                if ((object.ReferenceEquals(this.dsNombreSucursalField, value) != true)) {
                    this.dsNombreSucursalField = value;
                    this.RaisePropertyChanged("dsNombreSucursal");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=13)]
        public string dsRFC {
            get {
                return this.dsRFCField;
            }
            set {
                if ((object.ReferenceEquals(this.dsRFCField, value) != true)) {
                    this.dsRFCField = value;
                    this.RaisePropertyChanged("dsRFC");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=14)]
        public System.Nullable<int> idClienteTipoPago {
            get {
                return this.idClienteTipoPagoField;
            }
            set {
                if ((this.idClienteTipoPagoField.Equals(value) != true)) {
                    this.idClienteTipoPagoField = value;
                    this.RaisePropertyChanged("idClienteTipoPago");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=15)]
        public System.Nullable<decimal> mnMonedero {
            get {
                return this.mnMonederoField;
            }
            set {
                if ((this.mnMonederoField.Equals(value) != true)) {
                    this.mnMonederoField = value;
                    this.RaisePropertyChanged("mnMonedero");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=16)]
        public System.Nullable<decimal> mnCredito {
            get {
                return this.mnCreditoField;
            }
            set {
                if ((this.mnCreditoField.Equals(value) != true)) {
                    this.mnCreditoField = value;
                    this.RaisePropertyChanged("mnCredito");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=17)]
        public System.Nullable<decimal> mnTipoCambio {
            get {
                return this.mnTipoCambioField;
            }
            set {
                if ((this.mnTipoCambioField.Equals(value) != true)) {
                    this.mnTipoCambioField = value;
                    this.RaisePropertyChanged("mnTipoCambio");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=18)]
        public System.Nullable<bool> cnPaqueteBienvenida {
            get {
                return this.cnPaqueteBienvenidaField;
            }
            set {
                if ((this.cnPaqueteBienvenidaField.Equals(value) != true)) {
                    this.cnPaqueteBienvenidaField = value;
                    this.RaisePropertyChanged("cnPaqueteBienvenida");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=19)]
        public System.Nullable<bool> cnFacturacionDefault {
            get {
                return this.cnFacturacionDefaultField;
            }
            set {
                if ((this.cnFacturacionDefaultField.Equals(value) != true)) {
                    this.cnFacturacionDefaultField = value;
                    this.RaisePropertyChanged("cnFacturacionDefault");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=20)]
        public string dsNotas {
            get {
                return this.dsNotasField;
            }
            set {
                if ((object.ReferenceEquals(this.dsNotasField, value) != true)) {
                    this.dsNotasField = value;
                    this.RaisePropertyChanged("dsNotas");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=21)]
        public System.Nullable<System.DateTime> feAlta {
            get {
                return this.feAltaField;
            }
            set {
                if ((this.feAltaField.Equals(value) != true)) {
                    this.feAltaField = value;
                    this.RaisePropertyChanged("feAlta");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=22)]
        public System.Nullable<System.DateTime> feSuspendido {
            get {
                return this.feSuspendidoField;
            }
            set {
                if ((this.feSuspendidoField.Equals(value) != true)) {
                    this.feSuspendidoField = value;
                    this.RaisePropertyChanged("feSuspendido");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=23)]
        public System.Nullable<int> idClienteUsuarioAlta {
            get {
                return this.idClienteUsuarioAltaField;
            }
            set {
                if ((this.idClienteUsuarioAltaField.Equals(value) != true)) {
                    this.idClienteUsuarioAltaField = value;
                    this.RaisePropertyChanged("idClienteUsuarioAlta");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=24)]
        public string dsMunicipio {
            get {
                return this.dsMunicipioField;
            }
            set {
                if ((object.ReferenceEquals(this.dsMunicipioField, value) != true)) {
                    this.dsMunicipioField = value;
                    this.RaisePropertyChanged("dsMunicipio");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=25)]
        public string dsPoBox {
            get {
                return this.dsPoBoxField;
            }
            set {
                if ((object.ReferenceEquals(this.dsPoBoxField, value) != true)) {
                    this.dsPoBoxField = value;
                    this.RaisePropertyChanged("dsPoBox");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ListacClienteDetalles", Namespace="SIVEX.Core.Services.BussinesModel", ItemName="cClienteDetalle")]
    [System.SerializableAttribute()]
    public class ListacClienteDetalles : System.Collections.Generic.List<travesia.wscClienteDetalle.cClienteDetalle> {
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="cClienteDetalle", ConfigurationName="wscClienteDetalle.IcClienteDetalle")]
    public interface IcClienteDetalle {
        
        [System.ServiceModel.OperationContractAttribute(Action="cClienteDetalle/IcClienteDetalle/InsertarcClienteDetalle", ReplyAction="cClienteDetalle/IcClienteDetalle/InsertarcClienteDetalleResponse")]
        int InsertarcClienteDetalle(travesia.wscClienteDetalle.cClienteDetalle cclientedetalle, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cClienteDetalle/IcClienteDetalle/InsertarcClienteDetalle", ReplyAction="cClienteDetalle/IcClienteDetalle/InsertarcClienteDetalleResponse")]
        System.Threading.Tasks.Task<int> InsertarcClienteDetalleAsync(travesia.wscClienteDetalle.cClienteDetalle cclientedetalle, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cClienteDetalle/IcClienteDetalle/ModificarcClienteDetalle", ReplyAction="cClienteDetalle/IcClienteDetalle/ModificarcClienteDetalleResponse")]
        int ModificarcClienteDetalle(travesia.wscClienteDetalle.cClienteDetalle cclientedetalle, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cClienteDetalle/IcClienteDetalle/ModificarcClienteDetalle", ReplyAction="cClienteDetalle/IcClienteDetalle/ModificarcClienteDetalleResponse")]
        System.Threading.Tasks.Task<int> ModificarcClienteDetalleAsync(travesia.wscClienteDetalle.cClienteDetalle cclientedetalle, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cClienteDetalle/IcClienteDetalle/EliminarcClienteDetalle", ReplyAction="cClienteDetalle/IcClienteDetalle/EliminarcClienteDetalleResponse")]
        int EliminarcClienteDetalle(int idClienteDetalle, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cClienteDetalle/IcClienteDetalle/EliminarcClienteDetalle", ReplyAction="cClienteDetalle/IcClienteDetalle/EliminarcClienteDetalleResponse")]
        System.Threading.Tasks.Task<int> EliminarcClienteDetalleAsync(int idClienteDetalle, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cClienteDetalle/IcClienteDetalle/SeleccionarcClienteDetallePorId", ReplyAction="cClienteDetalle/IcClienteDetalle/SeleccionarcClienteDetallePorIdResponse")]
        travesia.wscClienteDetalle.cClienteDetalle SeleccionarcClienteDetallePorId(int idClienteDetalle, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cClienteDetalle/IcClienteDetalle/SeleccionarcClienteDetallePorId", ReplyAction="cClienteDetalle/IcClienteDetalle/SeleccionarcClienteDetallePorIdResponse")]
        System.Threading.Tasks.Task<travesia.wscClienteDetalle.cClienteDetalle> SeleccionarcClienteDetallePorIdAsync(int idClienteDetalle, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cClienteDetalle/IcClienteDetalle/SeleccionarcClienteDetalles", ReplyAction="cClienteDetalle/IcClienteDetalle/SeleccionarcClienteDetallesResponse")]
        travesia.wscClienteDetalle.ListacClienteDetalles SeleccionarcClienteDetalles(string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cClienteDetalle/IcClienteDetalle/SeleccionarcClienteDetalles", ReplyAction="cClienteDetalle/IcClienteDetalle/SeleccionarcClienteDetallesResponse")]
        System.Threading.Tasks.Task<travesia.wscClienteDetalle.ListacClienteDetalles> SeleccionarcClienteDetallesAsync(string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cClienteDetalle/IcClienteDetalle/SeleccionarcClienteDetallesPorIdCliente", ReplyAction="cClienteDetalle/IcClienteDetalle/SeleccionarcClienteDetallesPorIdClienteResponse")]
        travesia.wscClienteDetalle.ListacClienteDetalles SeleccionarcClienteDetallesPorIdCliente(int idcliente, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cClienteDetalle/IcClienteDetalle/SeleccionarcClienteDetallesPorIdCliente", ReplyAction="cClienteDetalle/IcClienteDetalle/SeleccionarcClienteDetallesPorIdClienteResponse")]
        System.Threading.Tasks.Task<travesia.wscClienteDetalle.ListacClienteDetalles> SeleccionarcClienteDetallesPorIdClienteAsync(int idcliente, string sesion);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IcClienteDetalleChannel : travesia.wscClienteDetalle.IcClienteDetalle, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class IcClienteDetalleClient : System.ServiceModel.ClientBase<travesia.wscClienteDetalle.IcClienteDetalle>, travesia.wscClienteDetalle.IcClienteDetalle {
        
        public IcClienteDetalleClient() {
        }
        
        public IcClienteDetalleClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public IcClienteDetalleClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public IcClienteDetalleClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public IcClienteDetalleClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int InsertarcClienteDetalle(travesia.wscClienteDetalle.cClienteDetalle cclientedetalle, string sesion) {
            return base.Channel.InsertarcClienteDetalle(cclientedetalle, sesion);
        }
        
        public System.Threading.Tasks.Task<int> InsertarcClienteDetalleAsync(travesia.wscClienteDetalle.cClienteDetalle cclientedetalle, string sesion) {
            return base.Channel.InsertarcClienteDetalleAsync(cclientedetalle, sesion);
        }
        
        public int ModificarcClienteDetalle(travesia.wscClienteDetalle.cClienteDetalle cclientedetalle, string sesion) {
            return base.Channel.ModificarcClienteDetalle(cclientedetalle, sesion);
        }
        
        public System.Threading.Tasks.Task<int> ModificarcClienteDetalleAsync(travesia.wscClienteDetalle.cClienteDetalle cclientedetalle, string sesion) {
            return base.Channel.ModificarcClienteDetalleAsync(cclientedetalle, sesion);
        }
        
        public int EliminarcClienteDetalle(int idClienteDetalle, string sesion) {
            return base.Channel.EliminarcClienteDetalle(idClienteDetalle, sesion);
        }
        
        public System.Threading.Tasks.Task<int> EliminarcClienteDetalleAsync(int idClienteDetalle, string sesion) {
            return base.Channel.EliminarcClienteDetalleAsync(idClienteDetalle, sesion);
        }
        
        public travesia.wscClienteDetalle.cClienteDetalle SeleccionarcClienteDetallePorId(int idClienteDetalle, string sesion) {
            return base.Channel.SeleccionarcClienteDetallePorId(idClienteDetalle, sesion);
        }
        
        public System.Threading.Tasks.Task<travesia.wscClienteDetalle.cClienteDetalle> SeleccionarcClienteDetallePorIdAsync(int idClienteDetalle, string sesion) {
            return base.Channel.SeleccionarcClienteDetallePorIdAsync(idClienteDetalle, sesion);
        }
        
        public travesia.wscClienteDetalle.ListacClienteDetalles SeleccionarcClienteDetalles(string sesion) {
            return base.Channel.SeleccionarcClienteDetalles(sesion);
        }
        
        public System.Threading.Tasks.Task<travesia.wscClienteDetalle.ListacClienteDetalles> SeleccionarcClienteDetallesAsync(string sesion) {
            return base.Channel.SeleccionarcClienteDetallesAsync(sesion);
        }
        
        public travesia.wscClienteDetalle.ListacClienteDetalles SeleccionarcClienteDetallesPorIdCliente(int idcliente, string sesion) {
            return base.Channel.SeleccionarcClienteDetallesPorIdCliente(idcliente, sesion);
        }
        
        public System.Threading.Tasks.Task<travesia.wscClienteDetalle.ListacClienteDetalles> SeleccionarcClienteDetallesPorIdClienteAsync(int idcliente, string sesion) {
            return base.Channel.SeleccionarcClienteDetallesPorIdClienteAsync(idcliente, sesion);
        }
    }
}
