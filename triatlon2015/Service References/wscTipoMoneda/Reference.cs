﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace travesia.wscTipoMoneda {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="cTipoMoneda", Namespace="SIVEX.Core.Services.BussinesModel")]
    [System.SerializableAttribute()]
    public partial class cTipoMoneda : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private System.Nullable<int> idTipoMonedaField;
        
        private string dsIsoField;
        
        private string dsTipoMonedaField;
        
        private System.Nullable<bool> cnDefaultField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string dsSimboloField;
        
        private System.Nullable<System.DateTime> feAltaField;
        
        private System.Nullable<int> idClienteUsuarioAltaField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public System.Nullable<int> idTipoMoneda {
            get {
                return this.idTipoMonedaField;
            }
            set {
                if ((this.idTipoMonedaField.Equals(value) != true)) {
                    this.idTipoMonedaField = value;
                    this.RaisePropertyChanged("idTipoMoneda");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=1)]
        public string dsIso {
            get {
                return this.dsIsoField;
            }
            set {
                if ((object.ReferenceEquals(this.dsIsoField, value) != true)) {
                    this.dsIsoField = value;
                    this.RaisePropertyChanged("dsIso");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public string dsTipoMoneda {
            get {
                return this.dsTipoMonedaField;
            }
            set {
                if ((object.ReferenceEquals(this.dsTipoMonedaField, value) != true)) {
                    this.dsTipoMonedaField = value;
                    this.RaisePropertyChanged("dsTipoMoneda");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public System.Nullable<bool> cnDefault {
            get {
                return this.cnDefaultField;
            }
            set {
                if ((this.cnDefaultField.Equals(value) != true)) {
                    this.cnDefaultField = value;
                    this.RaisePropertyChanged("cnDefault");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public string dsSimbolo {
            get {
                return this.dsSimboloField;
            }
            set {
                if ((object.ReferenceEquals(this.dsSimboloField, value) != true)) {
                    this.dsSimboloField = value;
                    this.RaisePropertyChanged("dsSimbolo");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public System.Nullable<System.DateTime> feAlta {
            get {
                return this.feAltaField;
            }
            set {
                if ((this.feAltaField.Equals(value) != true)) {
                    this.feAltaField = value;
                    this.RaisePropertyChanged("feAlta");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=6)]
        public System.Nullable<int> idClienteUsuarioAlta {
            get {
                return this.idClienteUsuarioAltaField;
            }
            set {
                if ((this.idClienteUsuarioAltaField.Equals(value) != true)) {
                    this.idClienteUsuarioAltaField = value;
                    this.RaisePropertyChanged("idClienteUsuarioAlta");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ListacTipoMoneda", Namespace="SIVEX.Core.Services.BussinesModel", ItemName="cTipoMoneda")]
    [System.SerializableAttribute()]
    public class ListacTipoMoneda : System.Collections.Generic.List<travesia.wscTipoMoneda.cTipoMoneda> {
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="cTipoMoneda", ConfigurationName="wscTipoMoneda.IcTipoMoneda")]
    public interface IcTipoMoneda {
        
        [System.ServiceModel.OperationContractAttribute(Action="cTipoMoneda/IcTipoMoneda/InsertarcTipoMoneda", ReplyAction="cTipoMoneda/IcTipoMoneda/InsertarcTipoMonedaResponse")]
        int InsertarcTipoMoneda(travesia.wscTipoMoneda.cTipoMoneda ctipomoneda, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cTipoMoneda/IcTipoMoneda/InsertarcTipoMoneda", ReplyAction="cTipoMoneda/IcTipoMoneda/InsertarcTipoMonedaResponse")]
        System.Threading.Tasks.Task<int> InsertarcTipoMonedaAsync(travesia.wscTipoMoneda.cTipoMoneda ctipomoneda, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cTipoMoneda/IcTipoMoneda/ModificarcTipoMoneda", ReplyAction="cTipoMoneda/IcTipoMoneda/ModificarcTipoMonedaResponse")]
        int ModificarcTipoMoneda(travesia.wscTipoMoneda.cTipoMoneda ctipomoneda, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cTipoMoneda/IcTipoMoneda/ModificarcTipoMoneda", ReplyAction="cTipoMoneda/IcTipoMoneda/ModificarcTipoMonedaResponse")]
        System.Threading.Tasks.Task<int> ModificarcTipoMonedaAsync(travesia.wscTipoMoneda.cTipoMoneda ctipomoneda, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cTipoMoneda/IcTipoMoneda/EliminarcTipoMoneda", ReplyAction="cTipoMoneda/IcTipoMoneda/EliminarcTipoMonedaResponse")]
        int EliminarcTipoMoneda(int idTipoMoneda, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cTipoMoneda/IcTipoMoneda/EliminarcTipoMoneda", ReplyAction="cTipoMoneda/IcTipoMoneda/EliminarcTipoMonedaResponse")]
        System.Threading.Tasks.Task<int> EliminarcTipoMonedaAsync(int idTipoMoneda, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cTipoMoneda/IcTipoMoneda/SeleccionarcTipoMonedaPorId", ReplyAction="cTipoMoneda/IcTipoMoneda/SeleccionarcTipoMonedaPorIdResponse")]
        travesia.wscTipoMoneda.cTipoMoneda SeleccionarcTipoMonedaPorId(int idTipoMoneda, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cTipoMoneda/IcTipoMoneda/SeleccionarcTipoMonedaPorId", ReplyAction="cTipoMoneda/IcTipoMoneda/SeleccionarcTipoMonedaPorIdResponse")]
        System.Threading.Tasks.Task<travesia.wscTipoMoneda.cTipoMoneda> SeleccionarcTipoMonedaPorIdAsync(int idTipoMoneda, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cTipoMoneda/IcTipoMoneda/SeleccionarcTipoMoneda", ReplyAction="cTipoMoneda/IcTipoMoneda/SeleccionarcTipoMonedaResponse")]
        travesia.wscTipoMoneda.ListacTipoMoneda SeleccionarcTipoMoneda(string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cTipoMoneda/IcTipoMoneda/SeleccionarcTipoMoneda", ReplyAction="cTipoMoneda/IcTipoMoneda/SeleccionarcTipoMonedaResponse")]
        System.Threading.Tasks.Task<travesia.wscTipoMoneda.ListacTipoMoneda> SeleccionarcTipoMonedaAsync(string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cTipoMoneda/IcTipoMoneda/SeleccionarcTipoMonedaPorCanalVenta", ReplyAction="cTipoMoneda/IcTipoMoneda/SeleccionarcTipoMonedaPorCanalVentaResponse")]
        travesia.wscTipoMoneda.ListacTipoMoneda SeleccionarcTipoMonedaPorCanalVenta(int idCanalVenta, string dsAgrupador, string sesion);
        
        [System.ServiceModel.OperationContractAttribute(Action="cTipoMoneda/IcTipoMoneda/SeleccionarcTipoMonedaPorCanalVenta", ReplyAction="cTipoMoneda/IcTipoMoneda/SeleccionarcTipoMonedaPorCanalVentaResponse")]
        System.Threading.Tasks.Task<travesia.wscTipoMoneda.ListacTipoMoneda> SeleccionarcTipoMonedaPorCanalVentaAsync(int idCanalVenta, string dsAgrupador, string sesion);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IcTipoMonedaChannel : travesia.wscTipoMoneda.IcTipoMoneda, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class IcTipoMonedaClient : System.ServiceModel.ClientBase<travesia.wscTipoMoneda.IcTipoMoneda>, travesia.wscTipoMoneda.IcTipoMoneda {
        
        public IcTipoMonedaClient() {
        }
        
        public IcTipoMonedaClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public IcTipoMonedaClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public IcTipoMonedaClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public IcTipoMonedaClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int InsertarcTipoMoneda(travesia.wscTipoMoneda.cTipoMoneda ctipomoneda, string sesion) {
            return base.Channel.InsertarcTipoMoneda(ctipomoneda, sesion);
        }
        
        public System.Threading.Tasks.Task<int> InsertarcTipoMonedaAsync(travesia.wscTipoMoneda.cTipoMoneda ctipomoneda, string sesion) {
            return base.Channel.InsertarcTipoMonedaAsync(ctipomoneda, sesion);
        }
        
        public int ModificarcTipoMoneda(travesia.wscTipoMoneda.cTipoMoneda ctipomoneda, string sesion) {
            return base.Channel.ModificarcTipoMoneda(ctipomoneda, sesion);
        }
        
        public System.Threading.Tasks.Task<int> ModificarcTipoMonedaAsync(travesia.wscTipoMoneda.cTipoMoneda ctipomoneda, string sesion) {
            return base.Channel.ModificarcTipoMonedaAsync(ctipomoneda, sesion);
        }
        
        public int EliminarcTipoMoneda(int idTipoMoneda, string sesion) {
            return base.Channel.EliminarcTipoMoneda(idTipoMoneda, sesion);
        }
        
        public System.Threading.Tasks.Task<int> EliminarcTipoMonedaAsync(int idTipoMoneda, string sesion) {
            return base.Channel.EliminarcTipoMonedaAsync(idTipoMoneda, sesion);
        }
        
        public travesia.wscTipoMoneda.cTipoMoneda SeleccionarcTipoMonedaPorId(int idTipoMoneda, string sesion) {
            return base.Channel.SeleccionarcTipoMonedaPorId(idTipoMoneda, sesion);
        }
        
        public System.Threading.Tasks.Task<travesia.wscTipoMoneda.cTipoMoneda> SeleccionarcTipoMonedaPorIdAsync(int idTipoMoneda, string sesion) {
            return base.Channel.SeleccionarcTipoMonedaPorIdAsync(idTipoMoneda, sesion);
        }
        
        public travesia.wscTipoMoneda.ListacTipoMoneda SeleccionarcTipoMoneda(string sesion) {
            return base.Channel.SeleccionarcTipoMoneda(sesion);
        }
        
        public System.Threading.Tasks.Task<travesia.wscTipoMoneda.ListacTipoMoneda> SeleccionarcTipoMonedaAsync(string sesion) {
            return base.Channel.SeleccionarcTipoMonedaAsync(sesion);
        }
        
        public travesia.wscTipoMoneda.ListacTipoMoneda SeleccionarcTipoMonedaPorCanalVenta(int idCanalVenta, string dsAgrupador, string sesion) {
            return base.Channel.SeleccionarcTipoMonedaPorCanalVenta(idCanalVenta, dsAgrupador, sesion);
        }
        
        public System.Threading.Tasks.Task<travesia.wscTipoMoneda.ListacTipoMoneda> SeleccionarcTipoMonedaPorCanalVentaAsync(int idCanalVenta, string dsAgrupador, string sesion) {
            return base.Channel.SeleccionarcTipoMonedaPorCanalVentaAsync(idCanalVenta, dsAgrupador, sesion);
        }
    }
}
