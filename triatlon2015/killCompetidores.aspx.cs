﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace travesia
{
    public partial class killCompetidores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                utils.Login("usrTriatlon", "eXperiencias");                
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int intRespKillCompetidor = utils.fnKillCompetidores(cGlobals.dsSession);
            lblMsgRespuesta.Text = intRespKillCompetidor.ToString() + " Row(s) affected";
        }
    }
}