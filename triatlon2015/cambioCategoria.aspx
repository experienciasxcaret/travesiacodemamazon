﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cambioCategoria.aspx.cs" Inherits="travesia.cambioCategoria" %>

<%@ Register Assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" namespace="DevExpress.Web.ASPxLoadingPanel" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width= device-width, initial-scale=1.0" /> 
    <title>Triatlon Xel-Ha</title>
    <link rel="stylesheet" href="styles/bootstrap.css" />
    <link rel="stylesheet" href="styles/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="styles/triatlon.css" />
    <link rel="stylesheet" href="styles/basic.css" />
    <link rel="stylesheet" href="styles/detallePago.css" />

    <script src="Scripts/jquery-1.11.3.js" ></script>
    <script src="Scripts/bootstrap.js" ></script>
    <script src="Scripts/inscripcionRifa.js" ></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.metadata.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/localization/messages_es.js"></script>
    <link rel="shortcut icon" type="image/png" href="favicon.png" />
    <script type="text/javascript">

        
        $(document).ready(function () {

            Sys.Browser.WebKit = {}; //Safari 3 is considered WebKit

            if (navigator.userAgent.indexOf('WebKit/') > -1) {

                Sys.Browser.agent = Sys.Browser.Firefox;

                Sys.Browser.version = parseFloat(navigator.userAgent.match(/WebKit\/(\d+(\.\d+)?)/)[1]);

                Sys.Browser.name = 'Firefox';
            }
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(initializeRequest);
            prm.add_pageLoaded(pageLoaded);
            function initializeRequest(sender, args) {
                //LoadingPanel.ShowInElement(args._postBackElement);  
                document.body.style.cursor = "wait";
                pnlinfoproceso.Show();

            }
            function pageLoaded(sender, args) {
                var panels = args.get_panelsUpdated();
                if (panels.length > 0) {
                    document.body.style.cursor = "default";
                    pnlinfoproceso.Hide();
                }
            }
            $('body').on('click', '#cancel', function (elemento) {
                $(".dvGlobalOver").hide();
            });
        });
        function showGlobal() {
            //console.log("showg");
            $(".dvGlobalOver").show();
            return false;
        }
        function closeGlobal() {
            $(".dvGlobalOver").hide();
        }

        function fnResultadoGlobalCollect(paremtrosGlobalCollect) {
            try {
                //alert(paremtrosGlobalCollect);
                console.log("fnResultadoGlobalCollect " + paremtrosGlobalCollect);
                var control = $('[id$=_hddResultadoPagoGlobalCollect]').attr('id');
                //var btnGlobalCollect = $('[id$=_btnGlobalCollect]').attr('id');
                console.log(control);
                var btnGlobalCollectOject = document.getElementById(btnGlobalCollect);
                //console.log(btnGlobalCollectOject);
                $('#hddResultadoPagoGlobalCollect').val(paremtrosGlobalCollect);
                //$('#' + btnGlobalCollect).click();
                btnGlobalCollect.DoClick();
                //btnGlobalCollect.DoClick();
            }
            catch (excptn) {
                console.log(excptn);
            }
        }
    </script>
    <script type="text/javascript">
         $('body').on('click', '.btnPagarFinal', function (elemento) {
        //$('.btnPagarFinal').click(function () {
            alert("btnPagarFinal");
            elemento.preventDefault();
            __doPostBack('btnPagarFinal', 'btnPagarFinal_Click');
        })
        function fi()
        {
            alert("fa");
        }
    </script>
</head>
<body>    
    <div class="container">
       <div class="cont header b-bottom">
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			   <!-- <object data="img/xelha.svg" type="image/svg+xml" class="img1 hidden-xs"></object> -->
		    </div>
		    <div class="logo col-xs-8 col-sm-4" align="center">
			    <object data="img/logo_tsm.svg" type="image/svg+xml" class="img2"></object>
		    </div>
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			    <object data="img/triatlon_fechas.png" type="image/svg+xml" class="img3 hidden-xs"></object>
		    </div>
       </div>
       <div class="grid_16 wrap">
            <div id="cont_formularios" class="cont" runat="server">
                <form class="form-horizontal" id="formCambioCategoria" runat="server">
                    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <dx:ASPxLabel ID="lblMensajeCabecera" runat="server" Text="" Font-Size="Large" ForeColor="#CC0000" ClientInstanceName="lblMensajeCabecera"></dx:ASPxLabel>                   
                        </div>
                    </div>
                    <div id="form_paso_a">
                                    <fieldset id="info_personal">
                                    <br />
                                    <div class="row">
                                        <div class="form-group" style="margin:5px">
                                            <div class="col-md-2"> 
                                                <label for="nombre" class="control-label" >Nombre *</label>                                            
                                            </div>	
                                            <div class="col-md-2"> 		                                            
                                                    <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtNombre" placeholder="nombre" name="nombre1">
                                                    </asp:TextBox>
                                            </div>
                                                                   
                                            
                                                <div class="col-md-2"> 
                                                    <label for="apellidoPaterno" class="control-label" >Apellido Paterno *</label>
                                                </div>
                                                 <div class="col-md-2"> 
                                                    <asp:TextBox runat="server" class="{required:true, rangelength: [2,50]} form-control"   id="txtApellidoPaterno" placeholder="apellido paterno" name="apellido1">
                                                    </asp:TextBox>
                                                 </div>                             
                                            
                     
                                            
                                                <div class="col-md-2">
                                                    <label for="apellidoMaterno" class="control-label" >Apellido Materno</label>
                                                </div>
                                                <div class="col-md-2"> 
                                                    <asp:TextBox runat="server" class="form-control"  id="txtApellidoMaterno" placeholder="apellido materno" name="apellido2">
                                                    </asp:TextBox>
                                                </div>                                            
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <div class="row">
                                                <div class="form-group" style="margin:5px">
                                                    <div class="col-md-2">
                                                            <label for="categoria" class="control-label">Sede</label>
                                                    </div>
                                                    <div class="col-md-3 selectContainer"> 
                                                            <asp:DropDownList  class="{required: true} form-control" ID="ddlGralCategoria" runat="server" AutoPostBack="True" name="categoria" OnSelectedIndexChanged="ddlGralCategoria_SelectedIndexChanged">
                                                                    <asp:ListItem Value="">Seleccione sede</asp:ListItem>                                                           
                                                            </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rvcategoría" runat="server" ControlToValidate="ddlGralCategoria"  Display="Dynamic" ErrorMessage="*" style="font-size: small" ValidationGroup="validaGuardar" Enabled="true" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                    </div>                            
                                                </div>
                                             </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>                                                                                                                                        
                                    <br />    
                                    </fieldset>
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                         <ContentTemplate>
                                            <fieldset id="botonera_personales">				
                                                 
                                                <dx:ASPxButton ID="btnGuardar" runat="server" Text="Continuar" class="btn btn-info btn-mini" name="btnGuardar"  CssPostfix="&quot;btn btn-info btn-mini" ClientInstanceName="btnGuardar"  UseSubmitBehavior="False"  AutoPostBack="false" ValidationGroup="validaGuardar" OnClick="btnGuardar_Click">
                                                </dx:ASPxButton>
                                                 &nbsp;&nbsp;<dx:ASPxLabel ID="lblMensajeValidacion" runat="server" Text="" ClientInstanceName="lblMensajeValidacion" Font-Bold="True" Font-Size="Small" ForeColor="#CC0000"></dx:ASPxLabel>
                                                <asp:Label class="col-md-4" ID="lblFolio2" visible="False" runat="server" Text="Cambios Guardados. Folio inscripcion : " Font-Bold="True" Font-Size="Larger"></asp:Label>
                                                <asp:HyperLink class="col-md-4" ID="lnkFolio2" visible="False" runat="server" ForeColor ="Blue" Target="_blank" Font-Underline="True" Font-Size="Larger">Imprimir Cupon</asp:HyperLink>
			                                </fieldset>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                   <div class="row">
                                    <asp:UpdatePanel ID="updPago" runat="server">
                                    <ContentTemplate>
                                        <div runat="server" id="divPago" visible="false">
                                            
                                                <div class="well col-xs-12 col-sm-12 col-md-8 col-xs-offset-1 col-sm-offset-1 col-md-offset-2">
                                                <div class="row">
                                               
                                                    <div class="form-group" style="padding:5px">
                                                        <div class="col-xs-12 col-sm-12"> 
                                                            <asp:Label class="col-xs-12 col-sm-12" ID="lblMensajeDiferencia" runat="server" Text="" Font-Bold="True" Font-Size="Large" ForeColor="#CC0000"></asp:Label>
                                                        </div>
                                                    </div>
                                               
                                                <div class="form-group" style="padding:5px">
                                                    <div class="col-xs-12 col-sm-12">
                                                        
                                                                <asp:RadioButtonList ID="rblTipoTarjeta" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblTipoTarjeta_SelectedIndexChanged" >
                                                                    <asp:ListItem
                                                                        Enabled="True"                                                
                                                                        Text='<img src="https://www.xperienciasxcaret.mx/ecom/webroot/images/visa.png"/>'
                                                                        Value="2"
                                                                        />                                                                                     
                                                                    <asp:ListItem
                                                                        Enabled="True"                            
                                                                        Text='<img src="https://www.xperienciasxcaret.mx/ecom/webroot/images/master.png"/>'
                                                                        Value="1"
                                                                    />
                                                                    <asp:ListItem
                                                                        Enabled="True"                            
                                                                        Text='<img src="https://www.xperienciasxcaret.mx/ecom/webroot/images/amex.png"/>'
                                                                        Value="3"
                                                                    />
                                                                </asp:RadioButtonList>
                                                             
                                                    </div>
                                                 </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding:2px">
                                                        <div class="col-md-12">
                                                            <div class="col-xs-10 col-sm-10">
                                                                <label class="col-xs-3 col-sm-3">Tipo Moneda</label>
                                                                <div class="col-xs-5 col-sm-5">
                                                                    
                                                                            <asp:DropDownList class="{required: true} form-control" ID="ddlCambioMoneda" runat="server" AppendDataBoundItems="True" AutoPostBack="True"  >
                                                                                <asp:ListItem Value="0">--</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                <div class="form-group" style="padding:2px">
                                                    <div class="col-md-12">
                                                        <div class="col-xs-10 col-sm-10">
                                                            <label class="col-xs-3 col-sm-3">Forma de pago</label>
                                                            <div class="col-xs-5 col-sm-5">
                                                           
                                                                <asp:DropDownList class="{required: true} form-control" ID="ddlFormaPago" runat="server" AutoPostBack="True" ControlToValidate="ddlFormaPago" OnSelectedIndexChanged="ddlFormaPago_SelectedIndexChanged" >
                                                                        <asp:ListItem Value="">Seleccione forma de pago</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rvFormaPago" runat="server" ErrorMessage="" Enabled="true" ControlToValidate="ddlFormaPago" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                          
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="form-group" style="padding:2px">
                                                    <div class="col-md-12">
                                                        <div class="col-md-10">                                
                                                            <asp:Label class="col-xs-3" ID="lblBanco" runat="server" Text="Banco emisor" Font-Bold="True"></asp:Label>
                                                            <div class="col-sm-5">
                                                            
                                                                <asp:DropDownList class="{required: true} form-control" ID="ddlBanco" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBanco_SelectedIndexChanged" >
                                                                        <asp:ListItem Value="">Seleccione Banco emisor</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rvBanco" runat="server" ErrorMessage="" ControlToValidate="ddlBanco" Enabled="true" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                           
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    </div>
                                                <div class="row">
                                                <div class="form-group" style="padding:2px">
                                                    <div class="col-md-12">
                                                        <div class ="col-sm-12"	>
                                                            
                                                            <asp:Label style="padding-right:5px" class ="col-sm-3" ID="lblNombreTh" runat="server" Text="Nombre Tarjetahabiente" Font-Bold="True"></asp:Label>
                                                            <div class ="col-sm-4">
                                                                <asp:TextBox runat="server" type="text" class="{required:true, nombreth: /^[A-Za-záéíóúñ,.]{2,}([\s][A-Za-záéíóúñ,.]{2,})+$/ } form-control" id="txtNombreTH" placeholder="nombre tarjetahabiente" name="nombreTH">
                                                                </asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rvNombreTH" runat="server" ErrorMessage="" Enabled="true" ControlToValidate="txtNombreTH" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                            </div>                                                            
                                                        </div>                                                
                                                    </div>   
                                                 </div>
                                                    </div>
                                                <div class="row">
                                                <div class="form-group" style="padding:2px"> 
                                                    <div class="col-md-12">
                                                        <div class="col-sm-12">
                                                                                                                                      
                                                                <asp:Label style="padding-right:5px" class ="col-sm-3" ID="lblNumTarjeta" runat="server" Text="Numero de tarjeta" Font-Bold="True"></asp:Label>
                                                                <div class ="col-sm-4">
                                                                    <asp:TextBox runat="server" class="{required:true, creditcard: true} form-control" id="txtNoTarjeta" placeholder="Numero Tarjeta" required data-stripe="number" name="noTarjeta">                                                                    </asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rvNoTarjeta" runat="server" ErrorMessage="" Enabled="true" ControlToValidate="txtNoTarjeta" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                                 </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                    </div>
                                                <div class="row">
                                                <div class="form-group" style="padding:2px">
                                                     <div class="col-md-12">                                                        
                                                        <div class="col-md-12">
                                                                                                                                                                           
                                                                <asp:Label class ="col-sm-3" ID="lblFechaExpira" runat="server" Text="Fecha de expiración" Font-Bold="True"></asp:Label>
                                                                 <div class ="col-sm-2">
                                                                     <asp:DropDownList class="{required: true} form-control" ID="ddlMesExpira" runat="server" AppendDataBoundItems="True" AutoPostBack="False">
                                                                        <asp:ListItem Value="01">01</asp:ListItem>
                                                                        <asp:ListItem Value="02">02</asp:ListItem>
                                                                        <asp:ListItem Value="03">03</asp:ListItem>
                                                                        <asp:ListItem Value="04">04</asp:ListItem>
                                                                        <asp:ListItem Value="05">05</asp:ListItem>
                                                                        <asp:ListItem Value="06">06</asp:ListItem>
                                                                        <asp:ListItem Value="07">07</asp:ListItem>
                                                                        <asp:ListItem Value="08">08</asp:ListItem>
                                                                        <asp:ListItem Value="09">09</asp:ListItem>
                                                                        <asp:ListItem Value="10">10</asp:ListItem>
                                                                        <asp:ListItem Value="11">11</asp:ListItem>
                                                                        <asp:ListItem Value="12">12</asp:ListItem>
                                                                      </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rvMesExpira" runat="server" ErrorMessage="" Enabled="true" ControlToValidate="ddlMesExpira" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                                 </div>
                                                                 <div class ="col-sm-2">
                                                                     <asp:DropDownList class="{required: true} form-control" ID="ddlAnioExpira" runat="server" AppendDataBoundItems="True" AutoPostBack="False" >
                                                                        <asp:ListItem Value="0">--</asp:ListItem>
                                                                     </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rvAnioExpira" runat="server" ErrorMessage="" Enabled="true" ControlToValidate="ddlAnioExpira" ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                                </div>
                                                          
                                                        </div>    
                                                    </div>                            
                                                </div>
                                                </div>                                                
                                                <div class="row">
                                                <div class="form-group" style="padding:2px">
                                                    <div class="col-md-12">      
                                                        <div class="col-md-12">
                                                                                                                                                                          
                                                                <asp:Label ID="lblCVV" class ="col-sm-3" runat="server" Text="Codigo de seguridad" Font-Bold="True"></asp:Label>
                                                                <div class ="col-sm-2">
                                                                    <asp:TextBox type="password" runat="server" class="{required: true, digits:true} form-control" id="txtCVV" placeholder="CVV" required data-stripe="cvc " name="anio">
                                                                    </asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rvCVV" runat="server" ErrorMessage="" ControlToValidate="txtCVV" Enabled="true"  ValidationGroup="validacionTarjeta" Font-Size="Small" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                                </div>
                                                           
                                                        </div>                                                                      
                                                    </div>
                                                </div>
                                                    </div>
                                                 <div class="row">
                                                     <div class="form-group" style="padding:2px">
                                                        <div class="col-xs-12 col-md-12">
                                                            <div class="col-md-12">                                                               
                                                                    <asp:Label class="col-md-10" ID="lblRespuestaPago" runat="server" Text="" Font-Bold="True" Font-Size="Medium"></asp:Label>                                                                                                                                   
                                                            </div>
                                                        </div>
                                                    </div>
                                                 </div>
                                                <div class="row">
                                                     <div class="form-group" style="padding:2px">
                                                        <div class="col-xs-12 col-md-12">
                                                            <div class="col-md-12">
                                                                <asp:UpdatePanel ID="UpdatePanel17" runat="server" UpdateMode="always">
                                                                <ContentTemplate>                                                                                                                                   
                                                                    <asp:HyperLink class="col-md-6" ID="lnkFolio" visible="False" runat="server" ForeColor ="Blue" Target="_blank" Font-Underline="True" Font-Size="Larger">Cambio realizado. Imprimir Cupon</asp:HyperLink>                                                                    
                                                                </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                 </div>
                                                <div class="row">
                                                    <div class="form-group" style="padding:2px">
                                                        <div class="col-xs-12 col-sm-12">
                                                            <div class="col-md-12">
                                                            <!--<button class="btn btn-success btn-lg btn-block" type="submit">Start Subscription</button>-->
                                                                                                                                                                                                     
                                                                <dx:ASPxButton  ID="btnPagarFinal" runat="server" Text="Pagar" AutoPostBack="False" UseSubmitBehavior="False" class="btnPagarFinal btn btn-success btn-lg btn-block text-center" name="btnPagarFinal" CssPostfix="&quot;btn btn-success btn-lg btn-block text-center" ClientInstanceName="btnPagarName" ValidationGroup="validacionTarjeta" ClientIDMode="AutoID" HorizontalAlign="NotSet" ImagePosition="Left" VerticalAlign="NotSet" Wrap="Default">                                                                    
                                                                </dx:ASPxButton>
                                                                    
                                                              
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <asp:UpdatePanel ID="upGC" runat="server" UpdateMode="Conditional" >
                                                    <ContentTemplate>  
                                                        <div class="dvGlobalOver" style="display:none;">
                                                            <div id='overpay'></div>
                                                                <div id="contentGlobal">
                                                                    <input id="cancel" type="button">
                                                                    <asp:HiddenField ID="hddResultadoPagoGlobalCollect" runat="server" Value="" ClientIDMode="Static"/>
                                                                    <asp:Label ID="lblRespuestaPagoGlobalCollect" runat="server" style="font-weight: 700 font-size: 8pt"></asp:Label>
                                                                    <asp:Panel ID="pnGlobalCollect" runat="server">
                                                                        <div class="insideformas">
                                                                            <table align="left" class="tbCss1" style="height:100%; width:100%">
                                                                                    <caption class="tb1CaptionTop">
                                                                                        <dx:ASPxLabel ID="lblEtiGlobalCollect" runat="server" ClientIDMode="AutoID" Text="Global Collect" Visible="true" style="margin-bottom:15px">
                                                                                        </dx:ASPxLabel>
                                                                                    </caption>
                                                                                    <tr >
                                                                                        <td align="left">
                                                                                            <iframe id="ifrGlobalCollect" runat="server" src="" class="ifrGlobalCollect">
                                                                                            <p>
                                                                                                Your browser does not support iframes.</p>
                                                                                            </iframe>                                
                                                                                            <dx:ASPxButton  ID="btnGlobalCollect" runat="server" UseSubmitBehavior="False" Text="" name="btnGlobalCollect" OnClick="btnGlobalCollect_Click" ClientInstanceName="btnGlobalCollect"  CausesValidation="False" AutoPostBack="False" ClientVisible="false" ></dx:ASPxButton>
                                                                                        </td>
                                                                                    </tr>
                                                                              </table>
                                                                         </div>
                                                                    </asp:Panel>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            <!-- fin del row -->
                                        </div><!-- fin del divPago -->
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div><!-- fin row -->
                    </div> <!-- fin paso a -->       
                                                 
                    <asp:UpdatePanel ID="UpdatePanelHD" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="hdCnValidation" runat="server" value=""/>
                            <asp:HiddenField ID="cnPrecargado" runat="server" value=""/>
                            <asp:HiddenField ID="hdDsSession" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdClienteContactoEventoPrincipales" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdProductoOriginal" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdVentaDetalle" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdVenta" runat="server" value=""/>
                            <asp:HiddenField ID="hdMontoDiferencia" runat="server" value=""/>
                            <asp:HiddenField ID="hdCnGlobalCollect" runat="server" value=""/>
                            <asp:HiddenField ID="hdCnGlobalCollectI" runat="server" value=""/>
                            <asp:HiddenField ID="hdIV" runat="server" value=""/>
                            <asp:HiddenField ID="hdToTalPagar" runat="server" value=""/>
                            <asp:HiddenField ID="hdEsPagoTarjeta" runat="server" value=""/>
                            <asp:HiddenField ID="hdCorreoCompetidor" runat="server" value=""/>
                            <asp:HiddenField ID="hddIdOrdenGlobal" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdPais" runat="server" value=""/>
                            <asp:HiddenField ID="cnIdContactoEvento" runat="server" value=""/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
       </div>                 
                                        
        <dx:ASPxLoadingPanel ID="pnlinfo" runat="server" ClientInstanceName="pnlinfoproceso" Modal="True">
        </dx:ASPxLoadingPanel>        
    </div>    
</body>  
</html>
