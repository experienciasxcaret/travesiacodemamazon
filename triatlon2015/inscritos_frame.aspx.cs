﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Timers;
using System.ComponentModel;
using System.Threading;
//using GeneralService;
using System.Configuration;

namespace travesia
{
    public partial class inscritos_frame : System.Web.UI.Page
    {

        int FolioSort;

        int MinRango;
        Int64 MaxRango;
        int idTipo;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = true;
            if (!IsPostBack)
            {
                Login1_Authenticate();
            }
            LlenaListas();
        }

        protected void Login1_Authenticate()
        {
            wsSecurity.IsSecurityClient seguridad = new wsSecurity.IsSecurityClient();
            wsSecurity.Security seguridadObject = new wsSecurity.Security();

            try
            {
                //TriatlonXelha
                seguridadObject = seguridad.Login("usrTriatlon", "eXperiencias", utils.getDevice(), utils.getResolution(), Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]),false);

                if (seguridadObject != null)
                {

                    cGlobals.dsSession = seguridadObject.dsSessionId;     // Guardar la variable global con el numero de sesion          
                    cGlobals.dsUsuario = seguridadObject.dsUsuario;       // Guardar la variable global con el usuario
                    cGlobals.idUsuario = seguridadObject.idClienteUsuario; // GUardar la variable global con el id del usuario;
                    cGlobals.idCliente = seguridadObject.idCliente;       //Guardar la variable global con el id del cliente;
                    cGlobals.dsNombreUsuario = seguridadObject.dsNombreUsuario;
                    cGlobals.dsBasedeDatos = seguridadObject.dsBasedeDatos;

                }
                else
                {
                    //e.Authenticated = false;
                }

                seguridad.Close();

                // Todo Correcto, pasamos al menu principal

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "msgLoad", "<script type='text/javascript'>showMessage('error','Ha ocurrido un error','" + ex.Message.ToString().Replace("'", "") + "');</script>", false);
            }
            finally
            {
            }
            seguridad.Close();
        }

        //protected  void OnTimedEvent(object source, ElapsedEventArgs e)
        //{
        //    btn_Click(btn, null);
        //}

        protected void LlenaListas()
        {
            wsBusinessRules.BusinessRulesServicesClient traerPago = new wsBusinessRules.BusinessRulesServicesClient();

            Dictionary<string, string> dicret = new Dictionary<string, string>();
            dicret.Add("@idEvento", "1");

            DataSet datos = traerPago.ExecuteRule("spGetListaGeneralInscritos", dicret, cGlobals.dsSession);

            if (datos != null)
            {
                if (datos.Tables.Count > 0)
                {
                    if (datos.Tables[0].Rows.Count > 0)
                    {
                        //grdInfantil9.DataSource = datos.Tables[0];
                        //grdInfantil9.DataBind();
                        ASPxgrdInfantil9.DataSource = datos.Tables[0];
                        ASPxgrdInfantil9.DataBind();
                    }
                }
                if (datos.Tables.Count > 1)
                {
                    if (datos.Tables[1].Rows.Count > 0)
                    {
                        grdInfantil12.DataSource = datos.Tables[1];
                        grdInfantil12.DataBind();
                    }
                }
                if (datos.Tables.Count > 2)
                {
                    if (datos.Tables[2].Rows.Count > 0)
                    {
                        grdSprint.DataSource = datos.Tables[2];
                        grdSprint.DataBind();
                    }
                }
                if (datos.Tables.Count > 3)
                {
                    if (datos.Tables[3].Rows.Count > 0)
                    {
                        grdSprintR.DataSource = datos.Tables[3];
                        grdSprintR.DataBind();
                    }
                    else
                    {
                        grdSprintR.DataSource = null;
                        grdSprintR.DataBind();
                    }
                }

                if (datos.Tables.Count > 4)
                {
                    if (datos.Tables[4].Rows.Count > 0)
                    {
                        grdOlimp.DataSource = datos.Tables[4];
                        grdOlimp.DataBind();
                    }
                }
                if (datos.Tables.Count > 5)
                {
                    if (datos.Tables[5].Rows.Count > 0)
                    {
                        grdOlimpR.DataSource = datos.Tables[5];
                        grdOlimpR.DataBind();
                    }
                }


            }

        }


    }
}


    
