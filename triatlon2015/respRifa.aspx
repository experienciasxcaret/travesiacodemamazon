﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="respRifa.aspx.cs" Inherits="travesia.respRifa" %>

<%@ Register Assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" namespace="DevExpress.Web.ASPxLoadingPanel" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width= device-width, initial-scale=1.0" /> 
    <title>Triatlon Xel-Ha</title>
    <link rel="stylesheet" href="styles/bootstrap.css" />
    <link rel="stylesheet" href="styles/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/inscripcionRifa.css" />
    <link rel="stylesheet" href="styles/triatlon.css" />
    <link rel="stylesheet" href="styles/basic.css" />
    <link rel="stylesheet" href="styles/detallePago.css" />

    <script src="Scripts/jquery-1.11.3.js" ></script>
    <script src="Scripts/bootstrap.js" ></script>
    <script src="Scripts/inscripcionRifa.js" ></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.metadata.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="Scripts/jquery-validation/localization/messages_es.js"></script>
    <link rel="shortcut icon" type="image/png" href="favicon.png" />
    <script type="text/javascript">

        
        $(document).ready(function () {

            Sys.Browser.WebKit = {}; //Safari 3 is considered WebKit

            if (navigator.userAgent.indexOf('WebKit/') > -1) {
                
                Sys.Browser.agent = Sys.Browser.Firefox;

                Sys.Browser.version = parseFloat(navigator.userAgent.match(/WebKit\/(\d+(\.\d+)?)/)[1]);

                Sys.Browser.name = 'Firefox';
            }
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(initializeRequest);
            prm.add_pageLoaded(pageLoaded);
            function initializeRequest(sender, args) {
                //LoadingPanel.ShowInElement(args._postBackElement);  
                document.body.style.cursor = "wait";                
                pnlinfoproceso.Show();

            }
            function pageLoaded(sender, args) {
                var panels = args.get_panelsUpdated();
                if (panels.length > 0) {
                    document.body.style.cursor = "default";
                    pnlinfoproceso.Hide();
                }
            }            
        })
    </script>
</head>
<body>    
    <div class="container">
       <div class="cont header b-bottom">
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			    <object data="img/xelha.svg" type="image/svg+xml" class="img1"></object>
		    </div>
		    <div class="logo col-xs-8 col-sm-4" align="center">
			    <object data="img/triatlon.svg" type="image/svg+xml" class="img2"></object>
		    </div>
		    <div class="logo hidden-phone col-xs-2 col-sm-4" align="center">
			    <object data="img/triatlon_fechas.svg" type="image/svg+xml" class="img3"></object>
		    </div>
       </div>
       <div class="grid_16 wrap">
            <div id="cont_formularios" class="cont" runat="server">
                <form class="form-horizontal" id="formRifa" runat="server">
                    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
                    <div class="row">
                        <div class="col-sm-4 col-md-4"></div>
                        <div class="col-sm-5 col-md-5">
                            <dx:ASPxLabel ID="lblMensajeCabecera" runat="server" Text="¡ Felicidades, ya estas inscrito a la rifa !" Font-Size="Large" ForeColor="Black" ClientInstanceName="lblMensajeCabecera"></dx:ASPxLabel>                   
                        </div>
                    </div>
                    <div id="form_paso_a">
                        <fieldset id="info_personal">
                        <br />
                        <div class="row">
                            <div class="form-group" style="margin:5px">
                                <div class="col-sm-1 col-md-1"></div>
                                <div class="col-sm-8 col-md-8">
                                    <dx:ASPxLabel ID="lblMensajeRifa" runat="server" Text="" Font-Size="Large" ForeColor="#0033CC" ClientInstanceName="lblMensajeRifa"></dx:ASPxLabel>                                    
                                </div>                                             
                            </div>
                            <div class="form-group" style="margin:5px">
                                <div class=""></div>
                                <div class="col-sm-1 col-md-1"></div>
                                <div class="col-sm-8 col-md-8">
                                    <dx:ASPxLabel ID="lblMensajeRifa2" runat="server" Text="" Font-Size="Large" ForeColor="#0033CC" ClientInstanceName="lblMensajeRifa2"></dx:ASPxLabel>
                                </div>
                            </div>
                        </div>                                                                                                                                                                           
                        <br />    
                        </fieldset>                                   
                    </div>                                      
                    <asp:UpdatePanel ID="UpdatePanelHD" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="hdCnValidation" runat="server" value=""/>
                            <asp:HiddenField ID="cnPrecargado" runat="server" value=""/>
                            <asp:HiddenField ID="hdDsSession" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdClienteContactoEventoPrincipales" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdProductoOriginal" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdVentaDetalle" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdVenta" runat="server" value=""/>
                            <asp:HiddenField ID="hdMontoDiferencia" runat="server" value=""/>
                            <asp:HiddenField ID="hdCnGlobalCollect" runat="server" value=""/>
                            <asp:HiddenField ID="hdCnGlobalCollectI" runat="server" value=""/>
                            <asp:HiddenField ID="hdIV" runat="server" value=""/>
                            <asp:HiddenField ID="hdToTalPagar" runat="server" value=""/>
                            <asp:HiddenField ID="hdEsPagoTarjeta" runat="server" value=""/>
                            <asp:HiddenField ID="hdCorreoCompetidor" runat="server" value=""/>
                            <asp:HiddenField ID="hddIdOrdenGlobal" runat="server" value=""/>
                            <asp:HiddenField ID="hdIdPais" runat="server" value=""/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
       </div>                 
        
        <dx:ASPxLoadingPanel ID="pnlinfo" runat="server" ClientInstanceName="pnlinfoproceso" Modal="True">
        </dx:ASPxLoadingPanel>        
    </div>    
</body>  
</html>
