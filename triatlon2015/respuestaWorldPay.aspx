﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="respuestaWorldPay.aspx.cs" Inherits="travesia.respuestaWorldPay" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.1, Version=10.1.5.0, Culture=neutral, PublicKeyToken=940cfcde86f32efd" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../Scripts/jquery/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery/jquery-ui-1.8.22.custom.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.blockUI.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.timers.js"></script>
    <script type="text/javascript">
        var strmensaje = '<br>Host: ' + window.location.host + '<br>pathname: ' + window.location.pathname;
        //alert(strmensaje);
    </script>
</head>
<body onload="fnDatosWorldPay();">
    <form id="form1" runat="server">
    <div>
         <table style="width:100%;">
            <tr>
                <td colspan="2">XCRM WORLDPAY</td>
            </tr>

            <tr>
                <td>&nbsp;<dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="ASPxLabel"></dx:ASPxLabel>
                </td>
                <td>&nbsp;</td>                
            </tr>
            <tr>
                <td><asp:Label ID="lblEtiRespuesta" runat="server" Text="Respuesta:"></asp:Label></td>
                <td>
                    <dx:aspxlabel ID="lblRespuesta" runat="server" Text="" ClientInstanceName="lblRespuesta"></dx:aspxlabel>
                </td>
                
            </tr>

            <tr>
                <td><asp:Label ID="lblEtiMensaje" runat="server" Text="Mensaje:"></asp:Label></td>
                <td>
                    <dx:ASPxLabel ID="lblMensaje" runat="server" Text="" ClientInstanceName="lblMensaje"></dx:ASPxLabel></td>
            </tr>
        </table>
    </div>
    </form>
    <script type="text/javascript">
        function fnDatosWorldPay() {
            try {
                parent.fnResultadoWorldPay('<%=(lblRespuesta.Text.Length > 0)?lblRespuesta.Text:""%>|<%=(lblMensaje.Text.Length > 0)?lblMensaje.Text:""%>');
            }
            catch (excptn) {
                mandaExcepcion('fnDatosWorldPay - consultaOrdenWORLDPAY<br>', excptn.description);
            }
        }
   </script>
</body>
</html>
