﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxEditors;
using System.Configuration;

namespace travesia
{
    public partial class generalRegistration : System.Web.UI.Page
    {
        
        const String callbackRegistrarCompetidor = "registrarCompetidor();";
        const String callbackValidaCompetidor = "validarCompetidor();";
        cParticipante cCompetidorGeneral = new cParticipante();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                DataSet result;
                result = null;
                Page.MaintainScrollPositionOnPostBack = true;
                int intIdClienteContactoEvento = 0;
                string strCat = "";

                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "btnContinuarA_Click")
                    btnContinuarA_Click(sender, e);
                
                if (!IsPostBack)
                {
                    Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = null;
                    cParticipante.cnCena = 0;
                    cCompetidorGeneral.DtCompetidor = null;
                    string prevPage = "";
                    string[] arrPre;
                    int intlengArrPre = 0;
                    if (Request.UrlReferrer != null)
                    {
                        prevPage = Request.UrlReferrer.AbsolutePath.ToString();
                        arrPre = prevPage.Split('/');
                        intlengArrPre = arrPre.Length;
                        prevPage = arrPre[intlengArrPre - 1];
                        //lblMensajeValidacion.Text = prevPage;
                        Console.WriteLine(prevPage);
                        if (prevPage == "dinner.aspx")
                        {
                            cParticipante.cnCena = 1;
                            strCat = "PES";
                            hdCena.Value = "1";
                        }
                        if (prevPage == "cenaEsp.aspx")
                        {
                            cParticipante.cnCena = 1;
                            strCat = "CENAESP";
                            hdCena.Value = "1";
                        }
                    }
                    cCompetidorGeneral.DtCompetidor = null;
                    int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                    if (Request.QueryString["cat"] != null)
                        strCat = Request.QueryString["cat"];
                    hdStrCat.Value = strCat;                    
                    cnIdContactoPrecargado.Value = intIdClienteContactoEvento.ToString();
                    // Aqui debe de preguntar si viene de cena O con intIdClienteContactoEvento > 0, 
                    // así si y solo si lo dejo seguir en la pantalla general
                    #region validacionDeCenaOPrecargado
                    if (hdCena.Value == "1" || intIdClienteContactoEvento > 0)
                    {
                        utils.Login("usrTriatlon", "eXperiencias");
                        hdDsSession.Value = cGlobals.dsSession;
                        // si trae strCat entonces quiere decir que viene de una pagina especifica como la de novatas                    
                        switch (strCat.ToUpper())
                        {
                            // solo novatas
                            case "NOV":
                                ddlGralSexo.Items.FindByValue("3").Selected = true;
                                ddlGralSexo.Attributes.Add("readonly", "readonly");
                                ddlGralSexo.Attributes.Add("disabled", "disabled");
                                utils.llenaAnio_en(ddlGralAnio, 25);
                                break;
                            case "PES":
                                utils.llenaAnio_en(ddlGralAnio);
                                cParticipante.cnCena = 1;
                                break;
                            default:
                                utils.llenaAnio_en(ddlGralAnio);
                                //utils.llenaDias(int.Parse(ddlGralAnio.SelectedValue.ToString()), int.Parse(ddlGralMes.SelectedValue.ToString()), ddlGralDia);                           
                                break;
                        }
                        utils.llenaPais(ddlGralPais);
                        utils.LlenaTipoSangre_en(ddlTipoSangre);
                        utils.LlenaTipoSangre_en(ddlTipoSangre2);
                        utils.LlenaTipoSangre_en(ddlTipoSangre3);
                        utils.llenaEstados(ddlGralEstado, ddlGralPais.SelectedValue.ToString(), getDsSession());
                        // si intIdClienteContactoEvento es mayor a cero quiere decir que viene de una precarga
                        // y hay que poner los datos ya precargados
                        if (intIdClienteContactoEvento > 0)
                        {
                            result = utils.buscarCompetidor(getDsSession(), "", "", "", 0, "", "", 0, 0, 0, intIdClienteContactoEvento);

                            if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                            {
                                // si es un cliente con estatus de preasignado, entonces podrá seguir con el proceso
                                // if (result.Tables[0].Rows[0]["retValue"].ToString() == "18")
                                if (result.Tables[0].Rows[0]["idEstatusCliente"].ToString() == "16")
                                {
                                    cCompetidorGeneral.DtCompetidor = null;
                                    cCompetidorGeneral.DtCompetidor = result.Tables[0];
                                    Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = null;
                                    Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = result.Tables[0];
                                    try
                                    {
                                        llenaFormulario(result.Tables[0]);
                                    }
                                    catch (Exception ex)
                                    {
                                        mensaje("Problems with the form " + ex.Message);
                                    }
                                }
                                else if (result.Tables[0].Rows[0]["idEstatusCliente"].ToString() == "15")
                                {
                                    alertRedirect("El competidor ya se encuentra inscrito al Triatlon. Gracias");
                                }
                                else
                                {
                                    alertRedirect("Lo sentimos, no has sido Pre-Asignado para seguir el proceso. Gracias");
                                }
                            }
                        }
                        btnTerminar.JSProperties["cpIsMessage"] = "";
                    }
                    else
                    {
                        alertRedirect("Sorry, you must enter from the Red Cross banner. Thanks");
                    }
                    #endregion
                }                
            }
            catch (Exception ex)
            {
                mensaje("Problema en la carga de pagina "+ ex.Message);
            }
        }

        protected void llenaFormulario(DataTable dtCompetidor, bool cnLlenaCategoria = false)
        {
            if (!cnLlenaCategoria)
            {
                if (dtCompetidor.Rows[0]["retValue"].ToString() == "1")
                {
                    for (int i = 0; i < dtCompetidor.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            if (dtCompetidor.Rows[i]["idConfigPagoPreregistro"] != null)
                                hdIdConfigPago.Value = dtCompetidor.Rows[i]["idConfigPagoPreregistro"].ToString();
                            if (dtCompetidor.Rows[i]["dsContacto"].ToString() != "")
                            {
                                txtGralNombre.Text = dtCompetidor.Rows[i]["dsContacto"].ToString();
                                txtGralNombre.ReadOnly = true;
                            }
                            txtGralApellidoPaterno.Text = ((dtCompetidor.Rows[i]["dsApellidoPaterno"].ToString() != "") ? dtCompetidor.Rows[i]["dsApellidoPaterno"].ToString() : "");
                            txtGralApellidoPaterno.ReadOnly = true;
                            txtGralApellidoMaterno.Text = ((dtCompetidor.Rows[i]["dsApellidoMaterno"].ToString() != "") ? dtCompetidor.Rows[i]["dsApellidoMaterno"].ToString() : "");
                            txtGralApellidoMaterno.ReadOnly = true;
                            txtGralEmail.Text = ((dtCompetidor.Rows[i]["dsCorreoElectronico"].ToString() != "") ? dtCompetidor.Rows[i]["dsCorreoElectronico"].ToString() : "");
                            if (dtCompetidor.Rows[i]["dsCorreoElectronico"].ToString() != "")
                                    txtGralEmail.ReadOnly = true;
                            txtGralEmail2.Text = ((dtCompetidor.Rows[i]["dsCorreoElectronico"].ToString() != "") ? dtCompetidor.Rows[i]["dsCorreoElectronico"].ToString() : "");
                            if (dtCompetidor.Rows[i]["dsCorreoElectronico"].ToString() != "")
                                    txtGralEmail2.ReadOnly = true;
                            int intIdSexo = ((dtCompetidor.Rows[i]["idSexo"].ToString() != "") ? int.Parse(dtCompetidor.Rows[i]["idSexo"].ToString()) : 0);
                            if (intIdSexo > 0)
                            {
                                ddlGralSexo.Items.FindByValue(intIdSexo.ToString()).Selected = true;
                                ddlGralSexo.Attributes.Add("readonly", "readonly");
                                ddlGralSexo.Attributes.Add("disabled", "disabled");

                            }
                            DateTime dtFeNacimiento = new DateTime();
                            int anio = 0;
                            int mes = 0;
                            int dia = 0;
                            if (dtCompetidor.Rows[0]["feNacimiento"].ToString() != "")
                            {
                                try
                                {
                                    dtFeNacimiento = DateTime.Parse(dtCompetidor.Rows[i]["feNacimiento"].ToString());
                                }
                                catch (Exception ex)
                                {
                                    mensaje("Problema al leer la fecha " + dtCompetidor.Rows[i]["feNacimiento"].ToString()+ " " + ex.Message);
                                }
                                anio = dtFeNacimiento.Year;
                                mes = dtFeNacimiento.Month;
                                dia = dtFeNacimiento.Day;
                                ddlGralAnio.SelectedIndex = -1;
                                ddlGralAnio.Items.FindByValue(anio.ToString()).Selected = true;
                                ddlGralAnio.Attributes.Add("readonly", "readonly");
                                ddlGralAnio.Attributes.Add("disabled", "disabled");
                                ddlGralMes.SelectedIndex = -1;
                                ddlGralMes.Items.FindByValue(mes.ToString()).Selected = true;
                                ddlGralMes.Attributes.Add("readonly", "readonly");
                                ddlGralMes.Attributes.Add("disabled", "disabled");
                                ddlGralDia.SelectedIndex = -1;
                                ddlGralDia.Items.FindByValue(dia.ToString()).Selected = true;
                                ddlGralDia.Attributes.Add("readonly", "readonly");
                                ddlGralDia.Attributes.Add("disabled", "disabled");
                            }
                            int intIdPais = (dtCompetidor.Rows[0]["idPais"].ToString() != "") ? int.Parse(dtCompetidor.Rows[0]["idPais"].ToString()) : 0;
                            if (intIdPais > 0)
                            {
                                ddlGralPais.SelectedIndex = -1;
                                ddlGralPais.Items.FindByValue(intIdPais.ToString()).Selected = true;
                                utils.llenaEstados(ddlGralEstado, ddlGralPais.SelectedValue.ToString(), getDsSession());
                                int intIdEstado = (dtCompetidor.Rows[i]["idEstado"].ToString() != "") ? int.Parse(dtCompetidor.Rows[i]["idEstado"].ToString()) : 0;
                                if (intIdEstado > 0)
                                {
                                    ddlGralEstado.SelectedIndex = -1;
                                    ddlGralEstado.Items.FindByValue(intIdEstado.ToString()).Selected = true;
                                }
                            }
                            txtGralColonia.Text = (dtCompetidor.Rows[i]["dsColonia"].ToString() != "") ? dtCompetidor.Rows[i]["dsColonia"].ToString() : "";
                            txtGralCiudad.Text = (dtCompetidor.Rows[i]["dsCiudad"].ToString() != "") ? dtCompetidor.Rows[i]["dsCiudad"].ToString() : "";
                            txtGralCalle.Text = (dtCompetidor.Rows[i]["dsCalle"].ToString() != "") ? dtCompetidor.Rows[i]["dsCalle"].ToString() : "";
                            txtGralNoInterior.Text = (dtCompetidor.Rows[i]["dsNumInterior"].ToString() != "") ? dtCompetidor.Rows[i]["dsNumInterior"].ToString() : "";
                            txtGralNoExterior.Text = (dtCompetidor.Rows[i]["dsNumExterior"].ToString() != "") ? dtCompetidor.Rows[i]["dsNumExterior"].ToString() : "";
                            string strTelefono = (dtCompetidor.Rows[i]["dsTelefono"].ToString() != "") ? dtCompetidor.Rows[i]["dsTelefono"].ToString() : "";
                            string strCelular = (dtCompetidor.Rows[i]["dsCelular"].ToString() != "") ? dtCompetidor.Rows[i]["dsCelular"].ToString() : "";
                            if (strTelefono.Length > 0)
                            {
                                txtGralTelefonofijo1.Text = strTelefono.Remove(3, strTelefono.Length - 3);
                                txtGralTelefonofijo2.Text = strTelefono.Remove(0, 2);
                            }

                            if (strCelular.Length > 0)
                            {
                                txtGralCelular1.Text = strCelular.Remove(3, strCelular.Length - 3);
                                txtGralCelular2.Text = strCelular.Remove(0, 3);
                            }
                            hdDsClave.Value = dtCompetidor.Rows[i]["dsAlergia"].ToString();
                        }
                        if (i == 1)
                        {
                            divRelevo2.Visible = true;                            
                            lblRelevo2.Visible = true;
                            if (dtCompetidor.Rows[i]["dsContacto"].ToString() != "")
                            {
                                lblNombreRelevo2.Visible = true;
                                txtNombre2.Visible = true;
                                txtNombre2.Text = dtCompetidor.Rows[i]["dsContacto"].ToString();
                                txtNombre2.ReadOnly = true;
                            }
                            lblPaternoRelevo2.Visible = true;
                            txtPaterno2.Visible = true;
                            txtPaterno2.Text = ((dtCompetidor.Rows[i]["dsApellidoPaterno"].ToString() != "") ? dtCompetidor.Rows[i]["dsApellidoPaterno"].ToString() : "");
                            txtPaterno2.ReadOnly = true;

                            lblMaternoRelevo2.Visible = true;
                            txtMaterno2.Visible = true;
                            txtMaterno2.Text = ((dtCompetidor.Rows[i]["dsApellidoMaterno"].ToString() != "") ? dtCompetidor.Rows[i]["dsApellidoMaterno"].ToString() : "");
                            txtMaterno2.ReadOnly = true;

                            lblSexoRelevo2.Visible = true;
                            ddlSexo2.Visible = true;
                            int intIdSexo2 = ((dtCompetidor.Rows[i]["idSexo"].ToString() != "") ? int.Parse(dtCompetidor.Rows[i]["idSexo"].ToString()) : 0);
                            if (intIdSexo2 > 0)
                            {
                                ddlSexo2.Items.FindByValue(intIdSexo2.ToString()).Selected = true;
                                ddlSexo2.Attributes.Add("readonly", "readonly");
                                ddlSexo2.Attributes.Add("disabled", "disabled");
                            }

                            cbCorredor1.Attributes.Add("class", "Corredor");

                            cbCorredor1.InputAttributes["class"] = "Corredor";
                            cbNadador1.InputAttributes["class"] = "Nadador";
                            cbCiclista1.InputAttributes["class"] = "Ciclista";

                            cbCorredor2.InputAttributes["class"] = "Corredor";
                            cbNadador2.InputAttributes["class"] = "Nadador";
                            cbCiclista2.InputAttributes["class"] = "Ciclista";                            

                            lblNadador1.Visible = true;
                            cbNadador1.Visible = true;
                            lblCorredor1.Visible = true;
                            cbCorredor1.Visible = true;                            
                            lblCiclista1.Visible = true;
                            cbCiclista1.Visible = true;

                            lblPlayeraRelevo2.Visible = true;
                            ddlPlayera2.Visible = true;

                            lblNoFmtri2.Visible = true;
                            txtNoFmtri2.Visible = true;

                            lblAlias2.Visible = true;
                            txtAlias2.Visible = true;

                            lblTipoSangre2.Visible = true;
                            ddlTipoSangre2.Visible = true;

                            lblCorredor2.Visible = true;
                            lblNadador2.Visible = true;
                            lblCiclista2.Visible = true;
                        }
                        if (i == 2)
                        {                            
                            divRelevo3.Visible = true;
                            lblRelevo3.Visible = true;
                            if (dtCompetidor.Rows[i]["dsContacto"].ToString() != "")
                            {
                                lblNombreRelevo3.Visible = true;
                                txtNombre3.Visible = true;
                                txtNombre3.Text = dtCompetidor.Rows[i]["dsContacto"].ToString();
                                txtNombre3.ReadOnly = true;
                            }
                            lblPaternoRelevo3.Visible = true;
                            txtPaterno3.Visible = true;
                            txtPaterno3.Text = ((dtCompetidor.Rows[i]["dsApellidoPaterno"].ToString() != "") ? dtCompetidor.Rows[i]["dsApellidoPaterno"].ToString() : "");
                            txtPaterno3.ReadOnly = true;

                            lblMaternoRelevo3.Visible = true;
                            txtMaterno3.Visible = true;
                            txtMaterno3.Text = ((dtCompetidor.Rows[i]["dsApellidoMaterno"].ToString() != "") ? dtCompetidor.Rows[i]["dsApellidoMaterno"].ToString() : "");
                            txtMaterno3.ReadOnly = true;

                            lblSexoRelevo3.Visible = true;
                            ddlSexo3.Visible = true;
                            int intIdSexo2 = ((dtCompetidor.Rows[i]["idSexo"].ToString() != "") ? int.Parse(dtCompetidor.Rows[i]["idSexo"].ToString()) : 0);
                            if (intIdSexo2 > 0)
                            {
                                ddlSexo3.Items.FindByValue(intIdSexo2.ToString()).Selected = true;
                                ddlSexo3.Attributes.Add("readonly", "readonly");
                                ddlSexo3.Attributes.Add("disabled", "disabled");
                            }
                            lblPlayeraRelevo3.Visible = true;
                            ddlPlayera3.Visible = true;

                            lblNoFmtri3.Visible = true;
                            txtNoFmtri3.Visible = true;

                            lblAlias3.Visible = true;
                            txtAlias3.Visible = true;

                            lblTipoSangre3.Visible = true;
                            ddlTipoSangre3.Visible = true;

                            cbCorredor3.InputAttributes["class"] = "Corredor";
                            cbNadador3.InputAttributes["class"] = "Nadador";
                            cbCiclista3.InputAttributes["class"] = "Ciclista";

                            lblCorredor3.Visible = true;
                            lblNadador3.Visible = true;
                            lblCiclista3.Visible = true;
                        }
                    }                    
                }               
                
            }// fin if cnLlenaCategoria
            else
            {
                if (dtCompetidor.Rows[0]["idEventoClasificacion"].ToString() != "")
                {
                    string idCategoria = dtCompetidor.Rows[0]["idEventoClasificacion"].ToString() + "-" + dtCompetidor.Rows[0]["idProducto"].ToString();
                    ddlGralCategoria.Items.FindByValue(idCategoria).Selected = true;
                    ddlGralCategoria.Attributes.Add("readonly", "readonly");
                    ddlGralCategoria.Attributes.Add("disabled", "disabled");
                }
            }
        }

        protected void ddlGralPais_TextChanged(object sender, EventArgs e)
        {
            utils.llenaEstados(ddlGralEstado, ddlGralPais.SelectedValue.ToString(), getDsSession());
        }

        protected void btnTerminar_Click(object sender, EventArgs e)
        {
            try
            {
                cParticipante objectParticipante = new cParticipante();
                cParticipante objectParticipante2 = new cParticipante();
                cParticipante objectParticipante3 = new cParticipante();
                string[] strDsEventoClasificacion;
                bool blActualiza = false;
                bool blTodoOK = true;
                string strIdClienteContactoEvento = "";
                string strIdConfigPagoPreregistro = hdIdConfigPago.Value;

                DateTime fechaEvento = new DateTime(2015, 12, 31);
                int anio = 1900;
                int mes = 01;
                int dia = 01;
                int.TryParse(ddlGralAnio.SelectedValue.ToString(), out anio);
                int.TryParse(ddlGralMes.SelectedValue.ToString(), out mes);
                int.TryParse(ddlGralDia.SelectedValue.ToString(), out dia);

                anio = (anio > 0) ? anio : 1900;
                mes = (mes > 0) ? mes : 01;
                dia = (dia > 0) ? dia : 01;
                DateTime fechaNacimiento = new DateTime(anio, mes, dia);
                int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;

                //if (ddlGralCategoria.SelectedValue.ToString() != "")
              
                // SI HAY VALOR EN LA PROPIEDAD dtCompetidor de nuestra clase cParticipante
                // QUIERE DECIR QUE VINO INFORMACION DE UN PRECARGADO.
                cCompetidorGeneral.DtCompetidor = getDatatableCompetidor();
                if (cCompetidorGeneral.DtCompetidor != null)
                {
                    strIdClienteContactoEvento = cCompetidorGeneral.DtCompetidor.Rows[0]["idClienteContactoEvento"].ToString();
                    blActualiza = true;
                }
                //string strAbsoluteUri = HttpContext.Current.Request.Url.AbsoluteUri;
                //string strHost = HttpContext.Current.Request.Url.Host;
                //string[] strspliturl = strAbsoluteUri.Split('/');
                //strAbsoluteUri = strAbsoluteUri.Replace(strspliturl[strspliturl.Length - 1], "");
                string loc = "";
                Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                loc = myUri.AbsoluteUri.ToString();
                string[] strspliturl = loc.Split('/');
                loc = loc.Replace("/"+strspliturl[strspliturl.Length - 1] , "/detallePago.aspx");
                #region categoriaSeleccionada
                if (ddlGralCategoria.SelectedValue.ToString() != "")
                {
                    #region llenaObjetoCompetidor
                    string[] strIdEventoClasificacion = ddlGralCategoria.SelectedValue.ToString().Split('-');
                    int intIdEventoClasificacion = int.Parse(strIdEventoClasificacion[0].ToString());
                    objectParticipante.nombre = txtGralNombre.Text;
                    objectParticipante.apellidoPaterno = txtGralApellidoPaterno.Text;
                    if (txtGralApellidoMaterno.Text != "")
                        objectParticipante.apellidoMaterno = txtGralApellidoMaterno.Text;
                    objectParticipante.email = txtGralEmail.Text;
                    objectParticipante.idSexo = int.Parse(ddlGralSexo.SelectedValue.ToString());
                    if (txtGralCiudad.Text != "")
                        objectParticipante.municipio = txtGralCiudad.Text.ToString();
                    if (txtGralNoInterior.Text != "")
                        objectParticipante.numInterior = (txtGralNoInterior.Text.ToString());
                    if (txtGralNoExterior.Text != "")
                        objectParticipante.numExterior = (txtGralNoExterior.Text.ToString());
                    if (txtGralCalle.Text != "")
                        objectParticipante.calle = txtGralCalle.Text.ToString();
                    if (txtGralColonia.Text != "")
                        objectParticipante.colonia = txtGralColonia.Text.ToString();
                    objectParticipante.fechaNacimiento = ddlGralAnio.SelectedValue.ToString() + "/" + ddlGralMes.SelectedValue.ToString() + "/" + ddlGralDia.SelectedValue.ToString();
                    if (ddlGralPais.SelectedValue.ToString() != "")
                    {
                        objectParticipante.idPais = int.Parse(ddlGralPais.SelectedValue.ToString());
                        objectParticipante.pais = ddlGralPais.SelectedItem.ToString();
                    }
                    if (ddlGralEstado.SelectedValue.ToString() != "")
                    {
                        objectParticipante.idEstado = int.Parse(ddlGralEstado.SelectedValue.ToString());
                        objectParticipante.estado = ddlGralEstado.SelectedItem.ToString();
                    }
                    if (txtGralTelefonofijo1.Text != "")
                        objectParticipante.telefono1 = txtGralTelefonofijo1.Text.ToString() + txtGralTelefonofijo2.Text.ToString();
                    objectParticipante.telefono2 = txtGralCelular1.Text.ToString() + txtGralCelular2.Text.ToString();
                    objectParticipante.idEventoClasificacion = intIdEventoClasificacion;
                    strDsEventoClasificacion = ddlGralCategoria.SelectedItem.ToString().Split('/');
                    objectParticipante.dsEventoClasificacion = strDsEventoClasificacion[0].ToString().Trim();
                    objectParticipante.dsCodigoEventoClasificacion = strDsEventoClasificacion[1].ToString().Trim();
                    objectParticipante.vecesParticipaciones = int.Parse(ddlGralNoParticipaciones.SelectedValue.ToString());
                    objectParticipante.idTipoMoneda = 2;
                    //objectParticipante.noRifa = utils.getNoRifa(intIdEventoClasificacion);
                    if (ddlGralTallasPlayeras.SelectedValue != null && ddlGralTallasPlayeras.SelectedValue != "")
                        objectParticipante.idTallaPlayera = int.Parse(ddlGralTallasPlayeras.SelectedValue.ToString());                                            
                    if (ddlTipoSangre.SelectedValue != null && ddlTipoSangre.SelectedValue != "")
                        objectParticipante.idTipoSangre = int.Parse(ddlTipoSangre.SelectedValue.ToString());
                    objectParticipante.dsTallaPlayera = ddlGralTallasPlayeras.SelectedItem.ToString();
                    objectParticipante.dsFolioFmtri = txtNoFmtri.Text;
                    objectParticipante.dsAlias = txtAlias.Text;
                    objectParticipante.dsPadecimientos = txtDsPadecimientos.Text;
                    objectParticipante.idTipoSangre = int.Parse(ddlTipoSangre.SelectedValue.ToString());
                    if (hdCena.Value == "1")
                        objectParticipante.dsAgrupador = "cena";
                    else
                        objectParticipante.dsAgrupador = "Triatlon";
                    objectParticipante.idEstatusCliente = 16; // PreAsignado;
                    objectParticipante.edad = edad; // se agregó el 29042015
                    if (cbCorredor1.Checked)
                        objectParticipante.cnCarrera = 1;
                    if (cbNadador1.Checked)
                        objectParticipante.cnNatacion = 1;
                    if (cbCiclista1.Checked)
                        objectParticipante.cnCiclismo = 1;
                    // YA TENEMOS LLENA LAS PROPIEDADES DE NUESTRO OBJETO PARTICIPANTE.
                    objectParticipante.llenarLista(objectParticipante);
                    #endregion
                    
                    if (txtNombre2.Text.Trim() != "")
                    {
                        objectParticipante2.nombre = txtNombre2.Text;
                        objectParticipante2.dsNombreVisitante = txtNombre2.Text;
                        objectParticipante2.apellidoPaterno = txtPaterno2.Text;
                        objectParticipante2.apellidoMaterno = txtMaterno2.Text;
                        objectParticipante2.idSexo = int.Parse(ddlSexo2.SelectedValue);
                        if (ddlPlayera2.SelectedValue != null && ddlPlayera2.SelectedValue != "")
                            objectParticipante2.idTallaPlayera = int.Parse(ddlPlayera2.SelectedValue);                        
                        if (ddlTipoSangre2.SelectedValue != null && ddlTipoSangre2.SelectedValue != "")
                            objectParticipante2.idTipoSangre = int.Parse(ddlTipoSangre2.SelectedValue);                       
                        objectParticipante2.dsAlias = txtAlias2.Text;
                        objectParticipante2.dsFolioFmtri = txtNoFmtri2.Text;
                        if (hdCena.Value == "1")
                            objectParticipante2.dsAgrupador = "cena";
                        else
                            objectParticipante2.dsAgrupador = "Triatlon";
                        objectParticipante2.idEventoClasificacion = intIdEventoClasificacion;
                        objectParticipante2.dsEventoClasificacion = strDsEventoClasificacion[0].ToString().Trim();
                        objectParticipante2.dsCodigoEventoClasificacion = strDsEventoClasificacion[1].ToString().Trim();
                        objectParticipante2.idTipoMoneda = 2;
                        objectParticipante2.idEstatusCliente = 16; // PreAsignado;

                        if (cbCorredor2.Checked)
                            objectParticipante2.cnCarrera = 1;
                        if (cbNadador2.Checked)
                            objectParticipante2.cnNatacion = 1;
                        if (cbCiclista2.Checked)
                            objectParticipante2.cnCiclismo = 1;
                        objectParticipante.llenarLista(objectParticipante2);
                    }

                    if (txtNombre3.Text.Trim() != "")
                    {
                        objectParticipante3.nombre = txtNombre3.Text;
                        objectParticipante3.dsNombreVisitante = txtNombre3.Text;
                        objectParticipante3.apellidoPaterno = txtPaterno3.Text;
                        objectParticipante3.apellidoMaterno = txtMaterno3.Text;
                        objectParticipante3.idSexo = int.Parse(ddlSexo3.SelectedValue);
                        objectParticipante3.idTallaPlayera = int.Parse(ddlPlayera3.SelectedValue);
                        objectParticipante3.idTipoSangre = int.Parse(ddlTipoSangre3.SelectedValue);
                        objectParticipante3.dsAlias = txtAlias3.Text;
                        objectParticipante3.dsFolioFmtri = txtNoFmtri3.Text;
                        if (hdCena.Value == "1")
                            objectParticipante3.dsAgrupador = "cena";
                        else
                            objectParticipante3.dsAgrupador = "Triatlon";
                        objectParticipante3.idEventoClasificacion = intIdEventoClasificacion;
                        objectParticipante3.dsEventoClasificacion = strDsEventoClasificacion[0].ToString().Trim();
                        objectParticipante3.dsCodigoEventoClasificacion = strDsEventoClasificacion[1].ToString().Trim();
                        objectParticipante3.idTipoMoneda = 2;
                        objectParticipante3.idEstatusCliente = 16; // PreAsignado;
                        if (cbCorredor3.Checked)
                            objectParticipante3.cnCarrera = 1;
                        if (cbNadador3.Checked)
                            objectParticipante3.cnNatacion = 1;
                        if (cbCiclista3.Checked)
                            objectParticipante3.cnCiclismo = 1;
                        objectParticipante.llenarLista(objectParticipante3);
                    }

                    // si paga quiere decir que antes de ir a la ventana de pagar, se guarda el competidor
                    // pero con estatus en proceso y sin numero y ni nada
                    if (blTodoOK)
                    {
                        //antes de ir a pagar tenemos que ver si es nuevo se inserta y si no es nuevo se actualiza
                        // pero el estatus original no se toca
                        #region actualiza
                        string resultadoAtualizar = "";
                        string strPrimerIdClienteContactoEvento = "";
                        int intIdVenta = 0;
                        int intNumeroCompetidor = 0;
                        if (blActualiza)
                        {

                            switch (strIdConfigPagoPreregistro)
                            {
                                case "2":
                                    intIdVenta = fnGuardaVentaPago(0, 12);
                                    break;
                                case "3":
                                    intIdVenta = fnGuardaVentaPago(100, 14);
                                    break;
                                default :

                                    break;
                            }
                            string strIdClienteContactoEventoPapa = "";
                            if (cCompetidorGeneral.DtCompetidor.Columns.Contains("idProducto"))
                            {
                                for (int i = 0; i < cCompetidorGeneral.DtCompetidor.Rows.Count; i++)
                                {
                                    string strDsClaveProducto = cCompetidorGeneral.DtCompetidor.Rows[i]["dsClave"].ToString();
                                    if (strDsClaveProducto != "TXHO" || strDsClaveProducto != "TXHS" || strDsClaveProducto != "TXHI" || strDsClaveProducto != "TXHRS" || strDsClaveProducto != "TXHRO" || strDsClaveProducto != "TXHE" || strDsClaveProducto != "TXHSN" || strDsClaveProducto != "TXHCD" || strDsClaveProducto != "TXHVIP")
                                    {
                                        if (i == 0)
                                        {
                                            string idClienteContactoEventoRecorriendo = cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteContactoEvento"].ToString();
                                            strIdClienteContactoEventoPapa = cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteContactoEvento"].ToString();
                                            strPrimerIdClienteContactoEvento = cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteContactoEvento"].ToString();
                                            wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                                            wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                                            clienteContactoEventoClient.Open();
                                            cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(idClienteContactoEventoRecorriendo), getDsSession());
                                            cCompetidor.dsAlias = (objectParticipante.dsAlias != null) ? objectParticipante.dsAlias : cCompetidor.dsAlias;
                                            cCompetidor.dsFolioFmtri = (objectParticipante.dsFolioFmtri);
                                            cCompetidor.dsPadecimiento = objectParticipante.dsPadecimientos;
                                            cCompetidor.idEventoClasificacion = objectParticipante.idEventoClasificacion;
                                            cCompetidor.idSexo = objectParticipante.idSexo;
                                            cCompetidor.idTipoSangre = objectParticipante.idTipoSangre;
                                            cCompetidor.noFrecuencia = objectParticipante.vecesParticipaciones;
                                            if (objectParticipante.idTallaPlayera != null && objectParticipante.idTallaPlayera >0)
                                                cCompetidor.idTalla = objectParticipante.idTallaPlayera;
                                            cCompetidor.cnCarrera = false;
                                            cCompetidor.cnNatacion = false;
                                            cCompetidor.cnCiclismo = false;
                                            if (cbCorredor1.Checked)
                                                cCompetidor.cnCarrera = true;
                                            if (cbNadador1.Checked)
                                                cCompetidor.cnNatacion = true;
                                            if (cbCiclista1.Checked)
                                                cCompetidor.cnCiclismo = true;
                                            if (cCompetidor.idVenta == 0)
                                            {
                                                cCompetidor.idVenta = null;
                                            }
                                            if (cCompetidor.idEventoModalidad == 0)
                                            {
                                                cCompetidor.idEventoModalidad = null;
                                            }

                                            if (intIdVenta > 0)
                                            {
                                                actualizaCompetidorDetalle(int.Parse(cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteDetalle"].ToString()));
                                                wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                                                // wsTransactions.IkVentaClient ikventa2 = new wsTransactions.IkVentaClient();
                                                wsTransactions.ListakVentaDetalles listaKVentaDetalles2 = new wsTransactions.ListakVentaDetalles();
                                                listaKVentaDetalles2 = ikventa.SeleccionarkVentasDetallePorIdVenta(intIdVenta, getDsSession());
                                                for (int k = 0; k < listaKVentaDetalles2.Count(); k++)
                                                {

                                                    cCompetidor.idVenta = intIdVenta;
                                                    // entonces tambien tenemos que mandar a actualizar su detalle con idEstatus inscrito
                                                    actualizaCompetidorDetalle(int.Parse(cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteDetalle"].ToString()));
                                                    intNumeroCompetidor = getNumeroCompetidor(objectParticipante.idEventoClasificacion.ToString());
                                                    cCompetidorGeneral.DtCompetidor.Rows[i]["noCompetidor"] = intNumeroCompetidor;
                                                    cCompetidor.NoCompetidor = intNumeroCompetidor;
                                                    cCompetidor.idVentaDetalle = listaKVentaDetalles2[k].idVentaDetalle;
                                                    wscClienteContacto.IcClienteContactoClient clienteContactoClient = new wscClienteContacto.IcClienteContactoClient();
                                                    wscClienteContacto.cClienteContacto cClienteContacto;
                                                    cClienteContacto = clienteContactoClient.SeleccionarcClienteContactoPorId(int.Parse(cCompetidor.idClienteContacto.ToString()), getDsSession());
                                                    cClienteContacto.dsCelular = objectParticipante.telefono2;
                                                    cClienteContacto.dsTelefono = objectParticipante.telefono1;
                                                    cClienteContacto.feNacimiento = DateTime.Parse(objectParticipante.fechaNacimiento);
                                                    cClienteContacto.idEstado = objectParticipante.idEstado;
                                                    cClienteContacto.idPais = objectParticipante.idPais;
                                                    cClienteContacto.idSexo = objectParticipante.idSexo;
                                                    cClienteContacto.dsCorreoElectronico = objectParticipante.email;
                                                    cClienteContacto.idTipoMoneda = 2;

                                                    clienteContactoEventoClient.Close();
                                                    clienteContactoClient.Close();
                                                    resultadoAtualizar = cParticipante.actualizaCompetidor(cCompetidor, cClienteContacto, true);
                                                }

                                            }// fin de if (intIdVenta > 0)
                                            else
                                            {
                                                //if (intIdVenta > 0)
                                                    
                                                //intNumeroCompetidor = getNumeroCompetidor(objectParticipante.idEventoClasificacion.ToString());
                                                    //cCompetidorGeneral.DtCompetidor.Rows[i]["noCompetidor"] = intNumeroCompetidor;
                                                //cCompetidor.NoCompetidor = intNumeroCompetidor;
                                               // cCompetidor.idVentaDetalle = listaKVentaDetalles2[k].idVentaDetalle;


                                                wscClienteContacto.IcClienteContactoClient clienteContactoClient = new wscClienteContacto.IcClienteContactoClient();
                                                wscClienteContacto.cClienteContacto cClienteContacto;
                                                cClienteContacto = clienteContactoClient.SeleccionarcClienteContactoPorId(int.Parse(cCompetidor.idClienteContacto.ToString()), getDsSession());
                                                cClienteContacto.dsCelular = objectParticipante.telefono2;
                                                cClienteContacto.dsTelefono = objectParticipante.telefono1;
                                                cClienteContacto.feNacimiento = DateTime.Parse(objectParticipante.fechaNacimiento);
                                                cClienteContacto.idEstado = objectParticipante.idEstado;
                                                cClienteContacto.idPais = objectParticipante.idPais;
                                                cClienteContacto.idSexo = objectParticipante.idSexo;
                                                cClienteContacto.dsCorreoElectronico = objectParticipante.email;
                                                cClienteContacto.idTipoMoneda = 2;

                                                clienteContactoEventoClient.Close();
                                                clienteContactoClient.Close();
                                                resultadoAtualizar = cParticipante.actualizaCompetidor(cCompetidor, cClienteContacto, true);
                                            }                                                                                                                        
                                        }
                                        if (i == 1)
                                        {
                                            string idClienteContactoEventoRecorriendo = cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteContactoEvento"].ToString();                                            
                                            wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                                            wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                                            clienteContactoEventoClient.Open();
                                            cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(idClienteContactoEventoRecorriendo), getDsSession());
                                            cCompetidor.dsAlias = (objectParticipante2.dsAlias != null) ? objectParticipante2.dsAlias : cCompetidor.dsAlias;
                                            cCompetidor.dsFolioFmtri = (objectParticipante2.dsFolioFmtri);
                                            cCompetidor.dsPadecimiento = objectParticipante2.dsPadecimientos;
                                            cCompetidor.idEventoClasificacion = objectParticipante2.idEventoClasificacion;
                                            cCompetidor.idSexo = objectParticipante2.idSexo;
                                            cCompetidor.idTipoSangre = objectParticipante2.idTipoSangre;                                            
                                            cCompetidor.cnCarrera = false;
                                            cCompetidor.cnNatacion = false;
                                            cCompetidor.cnCiclismo = false;
                                            if (cbCorredor2.Checked)
                                                cCompetidor.cnCarrera = true;
                                            if (cbNadador2.Checked)
                                                cCompetidor.cnNatacion = true;
                                            if (cbCiclista2.Checked)
                                                cCompetidor.cnCiclismo = true;
                                            if (objectParticipante2.idTallaPlayera != null && objectParticipante2.idTallaPlayera > 0)
                                                cCompetidor.idTalla = objectParticipante2.idTallaPlayera;
                                            if (cCompetidor.idVenta == 0)
                                            {
                                                cCompetidor.idVenta = null;
                                            }

                                            if (intIdVenta > 0)
                                            {
                                                cCompetidor.idVenta = intIdVenta;
                                                // entonces tambien tenemos que mandar a actualizar su detalle con idEstatus inscrito
                                                actualizaCompetidorDetalle(int.Parse(cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteDetalle"].ToString()));
                                                cCompetidor.NoCompetidor = intNumeroCompetidor;
                                            }

                                            wscClienteContacto.IcClienteContactoClient clienteContactoClient = new wscClienteContacto.IcClienteContactoClient();
                                            wscClienteContacto.cClienteContacto cClienteContacto;
                                            cClienteContacto = clienteContactoClient.SeleccionarcClienteContactoPorId(int.Parse(cCompetidor.idClienteContacto.ToString()), getDsSession());                                            
                                            cClienteContacto.idSexo = objectParticipante2.idSexo;
                                            cClienteContacto.idTipoMoneda = 2;

                                            clienteContactoEventoClient.Close();
                                            clienteContactoClient.Close();
                                            resultadoAtualizar = cParticipante.actualizaCompetidor(cCompetidor, cClienteContacto, true);                                         
                                        }
                                        if (i == 2)
                                        {
                                            string idClienteContactoEventoRecorriendo = cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteContactoEvento"].ToString();                                           
                                            wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                                            wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                                            clienteContactoEventoClient.Open();
                                            cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(idClienteContactoEventoRecorriendo), getDsSession());
                                            cCompetidor.dsAlias = (objectParticipante3.dsAlias != null) ? objectParticipante3.dsAlias : cCompetidor.dsAlias;
                                            cCompetidor.dsFolioFmtri = (objectParticipante3.dsFolioFmtri);
                                            cCompetidor.dsPadecimiento = objectParticipante3.dsPadecimientos;
                                            cCompetidor.idEventoClasificacion = objectParticipante3.idEventoClasificacion;
                                            cCompetidor.idSexo = objectParticipante3.idSexo;
                                            cCompetidor.idTipoSangre = objectParticipante3.idTipoSangre;
                                            if (objectParticipante3.idTallaPlayera != null && objectParticipante3.idTallaPlayera > 0)
                                                cCompetidor.idTalla = objectParticipante3.idTallaPlayera;
                                            cCompetidor.cnCarrera = false;
                                            cCompetidor.cnNatacion = false;
                                            cCompetidor.cnCiclismo = false;
                                            if (cbCorredor3.Checked)
                                                cCompetidor.cnCarrera = true;
                                            if (cbNadador3.Checked)
                                                cCompetidor.cnNatacion = true;
                                            if (cbCiclista3.Checked)
                                                cCompetidor.cnCiclismo = true;
                                            if (cCompetidor.idVenta == 0)
                                            {
                                                cCompetidor.idVenta = null;
                                            }

                                            if (intIdVenta > 0)
                                            {
                                                cCompetidor.idVenta = intIdVenta;
                                                // entonces tambien tenemos que mandar a actualizar su detalle con idEstatus inscrito
                                                actualizaCompetidorDetalle(int.Parse(cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteDetalle"].ToString()));
                                                cCompetidor.NoCompetidor = intNumeroCompetidor;
                                            }

                                            wscClienteContacto.IcClienteContactoClient clienteContactoClient = new wscClienteContacto.IcClienteContactoClient();
                                            wscClienteContacto.cClienteContacto cClienteContacto;
                                            cClienteContacto = clienteContactoClient.SeleccionarcClienteContactoPorId(int.Parse(cCompetidor.idClienteContacto.ToString()), getDsSession());                                            
                                            cClienteContacto.idSexo = objectParticipante3.idSexo;
                                            cClienteContacto.idTipoMoneda = 2;

                                            clienteContactoEventoClient.Close();
                                            clienteContactoClient.Close();
                                            resultadoAtualizar = cParticipante.actualizaCompetidor(cCompetidor, cClienteContacto, true);
                                        }// fin del if (i == 2)
                                        
                                        // entonce ahora tengo que ver el tamaño de la lista, si es mayor a uno los siguientes que vienen son de relevos
                                        // no debemos tomar el primero ya que ya lo actualizamos
                                        // los demas son inserciones
                                        int intRecorriendo = 0;
                                        //var respuestaGuardaRelevos = "";
                                        cParticipante cParticimenteRelevos = new cParticipante();
                                        if (objectParticipante.listCompetidores.Count > 1)
                                        {
                                            
                                            foreach (cParticipante elemento in objectParticipante.listCompetidores)
                                            {
                                                if (intRecorriendo != 0)
                                                {
                                                    //List<cParticipante> listCompetidores = new List<cParticipante>()
                                                    elemento.idClienteContactoEventoPapa = int.Parse(strIdClienteContactoEventoPapa);
                                                    cParticimenteRelevos.llenarLista(elemento);                                                    
                                                }
                                                intRecorriendo++;
                                            }
                                            var respuestaGuardaRelevos = cParticimenteRelevos.guardarCompetidor(cParticimenteRelevos.listCompetidores, getDsSession(), getIdCliente(), getIdUsuario());
                                        }

                                    } // fin del if (strDsClaveProducto != "TXHO" || strDsClaveProducto != "TXHS" || strDsClaveProducto != "TXHI" || strDsClaveProducto != "TXHRS" || strDsClaveProducto != "TXHRO" || strDsClaveProducto != "TXHE" || strDsClaveProducto != "TXHSN" || strDsClaveProducto != "TXHCD" || strDsClaveProducto != "TXHVIP")                               
                                } // fin del for (int i = 0; i < cCompetidorGeneral.DtCompetidor.Rows.Count; i++)
                            }
                            objectParticipante.VaciarLista();
                            if (hdCena.Value == "1")
                                cParticipante.cnCena = 1;                        
                            // si es uno es porque actualizo bien
                            if (resultadoAtualizar == "1")
                            {
                                if (intIdVenta > 0)
                                {
                                    bool respCorreoConfirmacion = correoPago(intIdVenta.ToString(), "", "1", cCompetidorGeneral.DtCompetidor.Rows[0]["dsCorreoElectronico"].ToString(), cCompetidorGeneral.DtCompetidor);
                                    cCompetidorGeneral.DtCompetidor = null;
                                    Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = null;
                                    Response.Redirect("respPago.aspx?token=" + intIdVenta.ToString());
                                }

                                else
                                {
                                    cCompetidorGeneral.DtCompetidor = null;
                                    Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = null;
                                    Response.Redirect(loc + "?contEvent=" + strPrimerIdClienteContactoEvento);
                                }
                            }
                        }
                        #endregion
                        // sino pues guarda
                        else
                        {

                            var listResp = objectParticipante.guardarCompetidor(objectParticipante.listCompetidores, getDsSession(), getIdCliente(), getIdUsuario());
                            int respIdClienteContactoEvento = 0;
                            respIdClienteContactoEvento = int.Parse(listResp[0].ToString());
                            objectParticipante.VaciarLista();
                            string strServEsp = "";
                            strServEsp = Request.QueryString["servEsp"];
                            if (strServEsp == "cen")
                            {
                                strServEsp = "&servEsp=cen";
                            }
                            if (hdCena.Value == "1")
                                cParticipante.cnCena = 1;
                            if (respIdClienteContactoEvento > 0)
                            {
                                cCompetidorGeneral.DtCompetidor = null;
                                Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = null;
                                Response.Redirect(loc + "?contEvent=" + respIdClienteContactoEvento.ToString() + strServEsp);
                            }
                        }
                    }// fin de if(blPaga)
                }
                #endregion
                else
                {//lblMensaje
                    lblMensaje.Text = "Seleccione categoría";
                    //cbRegistrar.JSProperties["cpIsMessage"] = "1 / Seleccione categoria";
                }
            }
            catch (Exception ex)
            {
                //   mensaje("Problema en carga de pagina", exx);
                //cbRegistrar.JSProperties["cpIsMessage"] = ex.ToString();
                lblMensaje.Text = ex.ToString();                
            }
        }

        protected void ddlGralMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            utils.llenaDias(int.Parse(ddlGralAnio.SelectedValue.ToString()), int.Parse(ddlGralMes.SelectedValue.ToString()), ddlGralDia);
        }

        protected void ddlGralAnio_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlGralAnio.SelectedValue.ToString() != "")
                utils.llenaDias(int.Parse(ddlGralAnio.SelectedValue.ToString()), int.Parse(ddlGralMes.SelectedValue.ToString()), ddlGralDia);
        }

        protected void btnContinuarA_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                string strCat = "";
                string strDsClave = "";
                strDsClave = hdDsClave.Value;
                DateTime fechaEvento = new DateTime(2015, 12, 31);
                int anio = 1900;
                int mes = 01;
                int dia = 01;
                int.TryParse(ddlGralAnio.SelectedValue.ToString(), out anio);
                int.TryParse(ddlGralMes.SelectedValue.ToString(), out mes);
                int.TryParse(ddlGralDia.SelectedValue.ToString(), out dia);

                anio = (anio > 0) ? anio : 1900;
                mes = (mes > 0) ? mes : 01;
                dia = (dia > 0) ? dia : 01;
                DateTime fechaNacimiento = new DateTime(anio, mes, dia);
                int intSexo = ((ddlGralSexo.SelectedValue) == "") ? 2 : int.Parse(ddlGralSexo.SelectedValue);
                int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;
                strCat = hdStrCat.Value;


                if (strDsClave != "")
                {

                    if (strDsClave == "TXSPR" || strDsClave == "TXOLR")
                    {
                            utils.getCategorias_en(getDsSession(), intSexo, edad, ddlGralCategoria, null, strDsClave.ToUpper().Trim(),1);
                    }
                    else
                    {
                        utils.getCategorias_en(getDsSession(), intSexo, edad, ddlGralCategoria, null, strDsClave.ToUpper().Trim());
                    }
                        
                }
                else if (strCat != "")

                    switch (strCat.ToUpper())
                    {
                        case "NOV":
                            utils.getCategorias_en(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXN", 1);
                            break;
                        case "CP":
                            utils.getCategorias_en(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXCP", 1);
                            break;
                        case "PES":
                            utils.getCategorias_en(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXC", 1);
                            break;
                        case "CENAESP":
                            utils.getCategorias_en(getDsSession(), intSexo, edad, ddlGralCategoria, null, "TXI", 1);
                            break;
                        default:
                            break;
                    }
                else
                {
                    if (cnIdContactoPrecargado.Value == "0")
                        utils.getCategorias_en(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXI", 1);
                    else
                        utils.getCategorias_en(getDsSession(), intSexo, edad, ddlGralCategoria, null, "TXI", 1,1);
                }
                

                if (edad > 11)
                {
                    /*ddlGralCategoria.Items.FindByText("Olimpico Relevo  Femenino - W").Enabled = true;
                    ddlGralCategoria.Items.FindByText("Sprint Relevo  Femenino - SW").Enabled = true;
                    ddlGralCategoria.Items.FindByText("Olimpico Relevo  Masculino - M").Enabled = true;
                    ddlGralCategoria.Items.FindByText("Sprint Relevo Masculino - SM").Enabled = true;*/
                    ListItem crItem = null;
                    if (ddlGralCategoria.Items.FindByText("Olimpico Relevo  Femenino / W") != null)
                        ddlGralCategoria.Items.FindByText("Olimpico Relevo  Femenino / W").Enabled = true;

                    if (ddlGralCategoria.Items.FindByText("Sprint Relevo  Femenino / SW") != null)
                        ddlGralCategoria.Items.FindByText("Sprint Relevo  Femenino / SW").Enabled = true;

                    if (ddlGralCategoria.Items.FindByText("Olimpico Relevo  Masculino / M") != null)
                        ddlGralCategoria.Items.FindByText("Olimpico Relevo  Masculino / M").Enabled = true;

                    if (ddlGralCategoria.Items.FindByText("Sprint Relevo Masculino / SM") != null)
                        ddlGralCategoria.Items.FindByText("Sprint Relevo Masculino / SM").Enabled = true;

                    if (intSexo == 2)
                    {
                        if (ddlGralCategoria.Items.FindByText("Olimpico Relevo  Femenino / W") != null)
                            ddlGralCategoria.Items.FindByText("Olimpico Relevo  Femenino / W").Enabled = false;
                        if (ddlGralCategoria.Items.FindByText("Sprint Relevo  Femenino / SW") != null)
                            ddlGralCategoria.Items.FindByText("Sprint Relevo  Femenino / SW").Enabled = false;
                    }
                    if (intSexo == 3)
                    {
                        if (ddlGralCategoria.Items.FindByText("Olimpico Relevo  Masculino / M") != null)
                            ddlGralCategoria.Items.FindByText("Olimpico Relevo  Masculino / M").Enabled = false;
                        if (ddlGralCategoria.Items.FindByText("Sprint Relevo Masculino / SM") != null)
                            ddlGralCategoria.Items.FindByText("Sprint Relevo Masculino / SM").Enabled = false;
                    }
                }

                // masculino                

                //aqui vamos a preguntar si la edad es mayor de once para que se muestre el combo de tallas de playeras
                if (edad > 11)
                {
                    ddlGralTallasPlayeras.Visible = true;
                    lblTamanioPlayera.Visible = true;
                    ddlGralTallasPlayeras.Enabled = true;
                    lblSujetoDisponibilidad.Visible = true;
                    //Nos hace falta llenar el combo cuanto esten en la base las tallasx
                    utils.getTallas_en(ddlGralTallasPlayeras, getDsSession());
                    //lblPlayeraRelevo2.Visible = true;
                    //ddlPlayera2.Visible = true;
                    utils.getTallas_en(ddlPlayera2, getDsSession());
                    //lblPlayeraRelevo3.Visible = true;
                    //ddlPlayera3.Visible = true;
                    utils.getTallas_en(ddlPlayera3, getDsSession());
                }
                if (cnIdContactoPrecargado.Value != "0")
                {
                    llenaFormulario(getDatatableCompetidor(), true);
                }
                
            }
            catch (Exception ex)
            {
                mensaje("Error al momento de continuar formulario B " + ex.Message);
            }
        }

        public DataTable getDatatableCompetidor()
        {
            if (cCompetidorGeneral.DtCompetidor == null)
                cCompetidorGeneral.DtCompetidor = (DataTable)Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value];
            return cCompetidorGeneral.DtCompetidor;
        }

        protected void ddlGralCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] strIdCategoria = ddlGralCategoria.SelectedValue.ToString().Split('-');
                ddlSexo2.Items.FindByValue("2").Enabled = true;
                ddlSexo2.Items.FindByValue("3").Enabled = true;
                ddlSexo3.Items.FindByValue("2").Enabled = true;
                ddlSexo3.Items.FindByValue("3").Enabled = true;
                if (strIdCategoria[0] == "24" || strIdCategoria[0] == "25" || strIdCategoria[0] == "26" || strIdCategoria[0] == "55" || strIdCategoria[0] == "56" || strIdCategoria[0] == "57")
                {
                    divRelevo2.Visible = true;
                    divRelevo3.Visible = true;
                    cbCorredor1.InputAttributes["class"] = "Corredor";
                    cbNadador1.InputAttributes["class"] = "Nadador";
                    cbCiclista1.InputAttributes["class"] = "Ciclista";

                    cbCorredor2.InputAttributes["class"] = "Corredor";
                    cbNadador2.InputAttributes["class"] = "Nadador";
                    cbCiclista2.InputAttributes["class"] = "Ciclista";

                    cbCorredor3.InputAttributes["class"] = "Corredor";
                    cbNadador3.InputAttributes["class"] = "Nadador";
                    cbCiclista3.InputAttributes["class"] = "Ciclista";
                    lblNadador1.Visible = true;
                    cbNadador1.Visible = true;
                    lblCorredor1.Visible = true;
                    cbCorredor1.Visible = true;
                    //cbCorredor1.InputAttributes["class"] = "Corredor";
                    lblCiclista1.Visible = true;
                    cbCiclista1.Visible = true;

                    lblRelevo2.Visible = true;
                    lblNombreRelevo2.Visible = true;
                    txtNombre2.Visible = true;
                    lblPaternoRelevo2.Visible = true;
                    txtPaterno2.Visible = true;
                    lblMaternoRelevo2.Visible = true;
                    txtMaterno2.Visible = true;
                    lblSexoRelevo2.Visible = true;
                    ddlSexo2.Visible = true;
                    lblPlayeraRelevo2.Visible = true;
                    ddlPlayera2.Visible = true;
                    lblNoFmtri2.Visible = true;
                    txtNoFmtri2.Visible = true;
                    lblAlias2.Visible = true;
                    txtAlias2.Visible = true;
                    lblTipoSangre2.Visible = true;
                    ddlTipoSangre2.Visible = true;
                    lblNadador2.Visible = true;
                    cbNadador2.Visible = true;
                    lblCorredor2.Visible = true;
                    cbCorredor2.Visible = true;
                    lblCiclista2.Visible = true;
                    cbCiclista2.Visible = true;

                    lblRelevo3.Visible = true;
                    lblNombreRelevo3.Visible = true;
                    txtNombre3.Visible = true;
                    lblPaternoRelevo3.Visible = true;
                    txtPaterno3.Visible = true;
                    lblMaternoRelevo3.Visible = true;
                    txtMaterno3.Visible = true;
                    lblSexoRelevo3.Visible = true;
                    ddlSexo3.Visible = true;
                    lblPlayeraRelevo3.Visible = true;
                    ddlPlayera3.Visible = true;
                    lblNoFmtri3.Visible = true;
                    txtNoFmtri3.Visible = true;
                    lblAlias3.Visible = true;
                    txtAlias3.Visible = true;
                    lblTipoSangre3.Visible = true;
                    ddlTipoSangre3.Visible = true;
                    lblNadador3.Visible = true;
                    cbNadador3.Visible = true;
                    lblCorredor3.Visible = true;
                    cbCorredor3.Visible = true;
                    lblCiclista3.Visible = true;
                    cbCiclista3.Visible = true;
                    // es relevo femenino
                    if (strIdCategoria[0] == "25" || strIdCategoria[0] == "56")
                    {
                        ddlSexo2.Items.FindByValue("2").Enabled = false;
                        ddlSexo3.Items.FindByValue("2").Enabled = false;
                        //ddlSexo2.Items.FindByText("Olimpico Relevo  Masculino - M").Enabled = false
                    }
                    // es relevo masculino
                    if (strIdCategoria[0] == "26" || strIdCategoria[0] == "57")
                    {
                        ddlSexo2.Items.FindByValue("3").Enabled = false;
                        ddlSexo3.Items.FindByValue("3").Enabled = false;
                        //ddlSexo2.Items.FindByText("Olimpico Relevo  Masculino - M").Enabled = false
                    }
                }
                else
                {
                    divRelevo2.Visible = false;
                    divRelevo3.Visible = false;

                    lblNadador1.Visible = false;
                    cbNadador1.Visible = false;
                    lblCorredor1.Visible = false;
                    cbCorredor1.Visible = false;
                    lblCiclista1.Visible = false;
                    cbCiclista1.Visible = false;

                    lblRelevo2.Visible = false;
                    lblNombreRelevo2.Visible = false;
                    txtNombre2.Visible = false;
                    lblPaternoRelevo2.Visible = false;
                    txtPaterno2.Visible = false;
                    lblMaternoRelevo2.Visible = false;
                    txtMaterno2.Visible = false;
                    lblSexoRelevo2.Visible = false;
                    ddlSexo2.Visible = false;
                    lblPlayeraRelevo2.Visible = false;
                    ddlPlayera2.Visible = false;
                    lblNoFmtri2.Visible = false;
                    txtNoFmtri2.Visible = false;
                    lblAlias2.Visible = false;
                    txtAlias2.Visible = false;
                    lblTipoSangre2.Visible = false;
                    ddlTipoSangre2.Visible = false;
                    lblNadador2.Visible = false;
                    cbNadador2.Visible = false;
                    lblCorredor2.Visible = false;
                    cbCorredor2.Visible = false;
                    lblCiclista2.Visible = false;
                    cbCiclista2.Visible = false;

                    lblRelevo3.Visible = false;
                    lblNombreRelevo3.Visible = false;
                    txtNombre3.Visible = false;
                    lblPaternoRelevo3.Visible = false;
                    txtPaterno3.Visible = false;
                    lblMaternoRelevo3.Visible = false;
                    txtMaterno3.Visible = false;
                    lblSexoRelevo3.Visible = false;
                    ddlSexo3.Visible = false;
                    lblPlayeraRelevo3.Visible = false;
                    ddlPlayera3.Visible = false;
                    lblNoFmtri3.Visible = false;
                    txtNoFmtri3.Visible = false;
                    lblAlias3.Visible = false;
                    txtAlias3.Visible = false;
                    lblTipoSangre3.Visible = false;
                    ddlTipoSangre3.Visible = false;
                    lblNadador3.Visible = false;
                    cbNadador3.Visible = false;
                    lblCorredor3.Visible = false;
                    cbCorredor3.Visible = false;
                    lblCiclista3.Visible = false;
                    cbCiclista3.Visible = false;
                }                
            }
            catch (Exception ex)
            { }
        }

        protected void btnTerminar_Init(object sender, EventArgs e)
        {
            ASPxButton btnTerminar = sender as ASPxButton;
            btnTerminar.Attributes.Add("onClick", String.Format(callbackRegistrarCompetidor));
        }

       

        protected void cbValida_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            try
            {                
                DateTime fechaEvento = new DateTime(2015, 12, 31);
                int anio = 1900;
                int mes = 01;
                int dia = 01;
                int.TryParse(ddlGralAnio.SelectedValue.ToString(), out anio);
                int.TryParse(ddlGralMes.SelectedValue.ToString(), out mes);
                int.TryParse(ddlGralDia.SelectedValue.ToString(), out dia);

                anio = (anio > 0) ? anio : 1900;
                mes = (mes > 0) ? mes : 01;
                dia = (dia > 0) ? dia : 01;
                DateTime fechaNacimiento = new DateTime(anio, mes, dia);

                int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;
                utils.getCategorias_en(getDsSession(),Convert.ToInt16(ddlGralSexo.SelectedValue), edad, ddlGralCategoria, null, "TXI", 1);
                bool respValidacion = utils.validaExisteRegistro(txtGralEmail.Text, getDsSession());                

                if (respValidacion)
                {
                    cbValida.JSProperties["cpValidacion"] = "true";
                }
                else 
                {
                    cbValida.JSProperties["cpValidacion"] = "false";
                }
            }
            catch (Exception ex)
            {
                //   mensaje("Problema en carga de pagina", exx);
                //cbRegistrar.JSProperties["cpIsMessage"] = ex.ToString();
                cbValida.JSProperties["cpValidacion"] = "";
            }
        }

        protected void btnContinuarA_Init(object sender, EventArgs e)
        {
            ASPxButton btnContinuarA = sender as ASPxButton;
            btnContinuarA.Attributes.Add("onClick", String.Format(callbackValidaCompetidor));
        }

        protected void txtGralEmail2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtGralEmail.Text != txtGralEmail2.Text)
                {
                    lblValidacionEmail.Text = "Los correos, no coinciden, favor de validarlo";
                }
                else
                {
                    lblValidacionEmail.Text = "";                    
                }
            }
            catch (Exception ex)
            { 
            }
        }

        public void mensaje(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('errorMsg','Ha ocurrido un error','" + errMensaje + "');</script>", false);
        }
        public void alert(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert(" + errMensaje + ");</script>", false);
        }

        protected void ddlGralTallasPlayeras_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbCorredor1.InputAttributes["class"] = "Corredor";
            cbNadador1.InputAttributes["class"] = "Nadador";
            cbCiclista1.InputAttributes["class"] = "Ciclista";

            cbCorredor2.InputAttributes["class"] = "Corredor";
            cbNadador2.InputAttributes["class"] = "Nadador";
            cbCiclista2.InputAttributes["class"] = "Ciclista";

            cbCorredor3.InputAttributes["class"] = "Corredor";
            cbNadador3.InputAttributes["class"] = "Nadador";
            cbCiclista3.InputAttributes["class"] = "Ciclista"; 
        }

        public int fnGuardaVentaPago(decimal dcPorcentajeDesc, int intIdFormaPago)
        {
            string[] strDsEventoClasificacion = ddlGralCategoria.SelectedItem.ToString().Split('/');
            string strDsCodigo = strDsEventoClasificacion[1].ToString().Trim();
            DataSet result = utils.getPrecio(getDsSession(),141,strDsCodigo);
            DataTable dtProductoInscripcion = null;
            DataTable dtCompetidor = getDatatableCompetidor();
            //cParticipante.dtDataCobro = 
            int sizeDataSet = result.Tables.Count;
            dtProductoInscripcion = result.Tables[0];
            dtProductoInscripcion.Columns.Add("ordenador", typeof(decimal)).DefaultValue = 1;
            dtProductoInscripcion.Columns.Add("cantidad", typeof(int)).DefaultValue = 1;
            dtProductoInscripcion.Columns.Add("mnPrecioVenta", typeof(decimal)).DefaultValue = 1;
            dtProductoInscripcion.Columns.Add("idMonedaPagar", typeof(decimal)).DefaultValue = 1;
            string strCanalVentaId = (ConfigurationManager.AppSettings["idCanalVenta"].ToString());

            for (int i = 0; i < sizeDataSet; i++)
            {
                dtProductoInscripcion.Rows[i]["mnPrecioVenta"] = (decimal)dtProductoInscripcion.Rows[i]["mnPrecio_2"] - ((dcPorcentajeDesc * (decimal)dtProductoInscripcion.Rows[i]["mnPrecio_2"]) / 100 );
                dtProductoInscripcion.Rows[i]["idMonedaPagar"] = 2;
                if (bool.Parse(dtProductoInscripcion.Rows[i]["cnEsFacturable"].ToString()) == true)
                {
                    dtProductoInscripcion.Rows[i]["ordenador"] = 1;
                    dtProductoInscripcion.Rows[i]["cantidad"] = 1;
                }
                else
                {
                    dtProductoInscripcion.Rows[i]["ordenador"] = 2;
                    dtProductoInscripcion.Rows[i]["cantidad"] = 0;
                }
            }

            cParticipante.dtDataCobro = dtProductoInscripcion;

            cResultadoGuardadoPago.strIdEstatusPago = "2";
            cResultadoGuardadoPago.strAfiliacion = "";
            cResultadoGuardadoPago.strDsRespuesta = "Aprobado";
            cResultadoGuardadoPago.strDsJustificacion = "Aprobado";
            cResultadoGuardadoPago.strDsReferencia = "";
            cResultadoGuardadoPago.strDsTransaccion = "";
            cResultadoGuardadoPago.strDsCorrelacion = "";
            cResultadoGuardadoPago.strIdBancoReceptor = "";

            decimal dcTotalVenta = getTotalVenta(dtProductoInscripcion);

            wsTransactions.IkVentaClient vc = new wsTransactions.IkVentaClient();
            vc.Open();

            wsTransactions.kVenta laventan = new wsTransactions.kVenta();

            laventan.idClienteDetalle = int.Parse(dtCompetidor.Rows[0]["idClienteDetalle"].ToString());
            laventan.idClienteClasificador = 5; // Directos
            laventan.idVenta = 0;
            laventan.idCliente = Int32.Parse(dtCompetidor.Rows[0]["idCliente"].ToString());
            laventan.feVenta = Int32.Parse(DateTime.Now.ToString("yyyyMMdd")); // DateTime.Now;
            laventan.hrVenta = DateTime.Now;
            laventan.mnMontoTotal = dcTotalVenta; //dcTotal;
            //laventan.mnDescuento = 0; //dcDescuento;
            //laventan.cnPaquete = (hdEsPaquete.Value == "1");
            laventan.cnPaquete = false;
            //laventan.dsClaveVenta
            //laventan.dsNotas = strNotas;
            if (intIdFormaPago == 14)
                laventan.idEstatusVenta = 23;
            else
                laventan.idEstatusVenta = 7;  // 
            laventan.cnRequiereFactura = false;
            laventan.idCanalVenta = int.Parse(strCanalVentaId);
            laventan.cnEvento = false;
            laventan.feCaducidad = Int32.Parse(DateTime.Now.AddYears(1).ToString("yyyyMMdd"));
            laventan.feAlta = DateTime.Now;
            laventan.idClienteUsuarioAlta = Int32.Parse(cGlobals.idUsuario.ToString());
            laventan.cnPromocion = false;  //false;

            laventan.dsDispositivo = "PC / LAPTOP";
            laventan.dsSistemaOperativo = "Windows NT APLICATION";

            if (ddlGralPais.SelectedValue != "")
                laventan.idPaisCompra = Int32.Parse(ddlGralPais.SelectedValue); //484;  // MEX cPais
            else
                laventan.idPaisCompra = 484;

            if (ddlGralEstado.SelectedValue != "1")
                laventan.idEstadoCompra = Int32.Parse(ddlGralEstado.SelectedValue);
            else
                laventan.idEstadoCompra = 272;
            laventan.idClienteContactoComprador = Int32.Parse(dtCompetidor.Rows[0]["idClienteContacto"].ToString());
            if (intIdFormaPago == 12)
                laventan.cnRequiereFactura = true;
            else
                laventan.cnRequiereFactura = false;
            // ahora los detalles
            bool blBanderaGuardo = true;
            wsTransactions.kVentaDetalle detalle; //, detallePadre;
            wsTransactions.ListakVentaDetalles listaParaEnvio = new wsTransactions.ListakVentaDetalles();


            for (int l = 0; l < dtProductoInscripcion.Rows.Count; l++)
            {
                if (bool.Parse(dtProductoInscripcion.Rows[l]["cnEsFacturable"].ToString()) == true && int.Parse(dtProductoInscripcion.Rows[l]["cantidad"].ToString()) > 0)
                {
                    detalle = new wsTransactions.kVentaDetalle();
                    string mnPrecio_ = "mnPrecio_";
                    string mnTipoCambio_ = "mnTipoCambio_";
                    decimal dcMnDescuento = 0;
                    decimal dcPrDescuento = 0;

                    mnPrecio_ += dtProductoInscripcion.Rows[l]["idMonedaPagar"].ToString();
                    mnTipoCambio_ += dtProductoInscripcion.Rows[l]["idMonedaPagar"].ToString();

                    detalle.feVisita = int.Parse(dtProductoInscripcion.Rows[l]["feVisita"].ToString());
                    detalle.feAlta = DateTime.Now;
                    //detalle.feVisitaLimite = Int32.Parse((DateTime.Now.AddYears(1)).AddMonths(6).ToString("yyyyMMdd"));  // fecha Alta + 1 año
                    //detalle.feVisitaLimite = Int32.Parse((DateTime.Parse(dtVentaOrdenador(dtVentaOrdenadorUpgradeSeleccionados.Rows[l]["fechaVisita"].ToString())).AddYears(1)).AddMonths(6).ToString("yyyyMMdd")); // fecha Visita + 1 año y medio. // validado con Lupita y Haumi 16 septiembre 2013
                    detalle.noAgrupadorVenta = 1; //? sacar el max  + 1
                    detalle.noPax = (int)dtProductoInscripcion.Rows[l]["cantidad"];
                    if (int.Parse(dtProductoInscripcion.Rows[l]["idMonedaPagar"].ToString()) > 4)
                        detalle.mnPrecioLista = (decimal)dtProductoInscripcion.Rows[l]["mnPrecio_MS"];
                    else
                        detalle.mnPrecioLista = (decimal)dtProductoInscripcion.Rows[l][mnPrecio_];
                    detalle.mnPrecioVenta = decimal.Parse(dtProductoInscripcion.Rows[l]["mnPrecioVenta"].ToString()) / detalle.noPax;

                    // cuando detalle.mnPrecioLista  es cero, es porque son cosas como donativos que no tienen precio
                    if (detalle.mnPrecioLista > 0)
                    {
                        dcMnDescuento = (decimal)(detalle.mnPrecioLista - detalle.mnPrecioVenta);
                        dcPrDescuento = dcPorcentajeDesc;
                    }
                    else
                    {
                        dcMnDescuento = 0;
                        dcPrDescuento = 0;
                    }
                    detalle.mnDescuento = dcMnDescuento; //detalle.mnPrecioLista - detalle.mnPrecioVenta; // se calcula de la resta de precioLista - precioVenta listo
                    detalle.prDescuento = dcPrDescuento; //((1 - (detalle.mnPrecioVenta / detalle.mnPrecioLista)) * 100); // se calcula listo

                    detalle.idProductoPrecio = (int)dtProductoInscripcion.Rows[l]["idProductoPrecio"];
                    detalle.cnAccesado = false;
                    detalle.cnControlInterno = false;
                    detalle.cnPaquete = (bool)dtProductoInscripcion.Rows[l]["cnEsPaquete"];
                    /*if ((bool)detalle.cnPaquete)
                    {
                        // Depende la el orde de como escojan los productos se guarda la venta con bandera de paquete.
                        laventan.cnPaquete = true; //(hdEsPaquete.Value == "1");
                        detalle.dsClavePaquete = dtVentaOrdenadorUpgradeSeleccionados.Rows[l]["dsClavePaquete"].ToString();
                    }*/
                    detalle.idTipoCliente = (int)dtProductoInscripcion.Rows[l]["idTipoCliente"];
                    detalle.cnPromocion = false;

                    detalle.dsNombreVisitante = dtCompetidor.Rows[0]["dsContacto"].ToString(); // llenar con campo de texto por cada registro - listo
                    detalle.dsApellidoPaternoVisitante = dtCompetidor.Rows[0]["dsApellidoPaterno"].ToString();
                    detalle.dsApellidoMaternoVisitante = dtCompetidor.Rows[0]["dsApellidoMaterno"].ToString();

                    if (intIdFormaPago == 14)
                        detalle.idEstatusVenta = 23;
                    else
                        detalle.idEstatusVenta = 7; //  

                    int intLocacion = (int)dtProductoInscripcion.Rows[l]["idLocacion"];

                    if (intLocacion != 0)
                        detalle.idLocacion = intLocacion;


                    detalle.mnComision = 0;
                    detalle.prComision = 0;                    
                    // En idMoneda del detalle se debe guardar el idTipoMoneda (Gabriel)
                    //detalle.idTipoMoneda = (int)dtVentaOrdenador.Rows[l]["idTipoMoneda"];
                    detalle.idTipoMoneda = int.Parse(dtProductoInscripcion.Rows[l]["idMonedaPagar"].ToString());

                    detalle.mnTipoCambio = (decimal)dtProductoInscripcion.Rows[l][mnTipoCambio_]; // ? sacar del día listo

                    // Gabriel Granados pide que el campo mnTipoCambio se guarde el tipo de cambio en pesos del dia. correo(09-07-2013): Ventas en Dolares y PEsos
                    if (detalle.idTipoMoneda == 1)
                    {
                    }

                    detalle.idSegmento = 4;
                    detalle.idClienteUsuarioAlta = Int32.Parse(cGlobals.idUsuario.ToString());
                    //detalle.idVentaDetalle = int.Parse(dtVentaOrdenador.Rows[l]["consecutivo"].ToString());
                    detalle.idVentaDetalle = 0;
                    detalle.idCanalVenta = int.Parse(strCanalVentaId);
                    detalle.mnIva = null;
                    listaParaEnvio.Add(detalle);
                }                
            }// fin del for   

            int intIdVenta = vc.InsertarkVenta(laventan, listaParaEnvio, null, null, getDsSession());
            vc.Close();

            //------------------------------------------------------------------------------------------------
            // PAGO
            //------------------------------------------------------------------------------------------------

            wsTransactions.IkPagoClient IkPago = new wsTransactions.IkPagoClient();
            wsTransactions.ListakPagoTransacciones lstkPagoTransa = new wsTransactions.ListakPagoTransacciones();
            wsTransactions.kPagoTransaccion PagoTran = new wsTransactions.kPagoTransaccion();
            wsTransactions.kPago objkPago = new wsTransactions.kPago();

            objkPago.idPago = 0;
            objkPago.idVenta = intIdVenta;
            objkPago.idCanalPago = int.Parse(strCanalVentaId); //11; // fijo ?
            objkPago.feTransaccionTotal = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
            objkPago.hrTransaccionTotal = DateTime.Now;
            //objkPago.mnTransaccionTotal = Math.Round(dcTotal, 2);
            //objkPago.mnTransaccionTotal = decimal.Parse(totalPagar);
            objkPago.mnTransaccionTotal = dcTotalVenta;
            objkPago.cnEsPrepago = false;
            objkPago.noAgrupadorVenta = 1;
            objkPago.idFormaPago = intIdFormaPago; // 52 CxC y 14 Cortesias
            objkPago.idEstatusPago = Int32.Parse(cResultadoGuardadoPago.strIdEstatusPago); //intIdEstatusPago;
            objkPago.feAlta = DateTime.Now;
            objkPago.idClienteUsuarioAlta = (cGlobals.idUsuario);

            PagoTran.idPago = objkPago.idPago; //
            // En idMoneda se guarda el IdTipoMoneda (Gabriel 20082013)
            PagoTran.idTipoMoneda = int.Parse(dtProductoInscripcion.Rows[0]["idMonedaPagar"].ToString()); //Int32.Parse(cmbMoneda.SelectedItem.Value);

            PagoTran.feTransaccion = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
            PagoTran.hrTransaccion = DateTime.Now;
            PagoTran.dsRespuesta = cResultadoGuardadoPago.strDsRespuesta; // strDsRespuesta;
            PagoTran.dsReferencia = cResultadoGuardadoPago.strDsReferencia; // strDsReferencia;
            PagoTran.dsJustificacion = cResultadoGuardadoPago.strDsJustificacion; // strDsJustificacion;

            //PagoTran.mnTransaccion = Math.Round(dcTotal, 2);
            //PagoTran.mnTransaccion = decimal.Parse(totalPagar);
            PagoTran.mnTransaccion = dcTotalVenta;

            PagoTran.dsTransaccion = cResultadoGuardadoPago.strDsTransaccion; // strDsTransaccion;
            PagoTran.dsCorrelacion = cResultadoGuardadoPago.strDsCorrelacion; // strDsCorrelacion;
            PagoTran.noPagosMSI = 0;
            objkPago.cnPlanDePagos = false;

            PagoTran.cnPrepago = false;
            //if (intIdClienteTarjeta > 0)
            //if (Int32.Parse(cResultadoGuardadoPago.strIdClienteTarjeta) > 0)
                PagoTran.idClienteTarjeta = null; // intIdClienteTarjeta;
            PagoTran.mnBancoComision = 0;
            PagoTran.prBancoComision = 0;
            PagoTran.feAlta = DateTime.Now;
            PagoTran.idClienteUsuarioAlta = (cGlobals.idUsuario);
            PagoTran.idPagoTransaccion = 0;
            PagoTran.idEstatusPago = Int32.Parse(cResultadoGuardadoPago.strIdEstatusPago);            

            PagoTran.dsAfiliacion = cResultadoGuardadoPago.strAfiliacion;

            if (intIdFormaPago == 52) // si el tipo de pago es CxC  se tiene que guardar el idBanco y idBancoReceptor  Bancomer (5)
            {
                PagoTran.idBanco = 5;
                PagoTran.idBancoReceptor = 5;
            }
            else
            {
                PagoTran.idBanco = null;
                PagoTran.idBancoReceptor = null;
            }

            lstkPagoTransa.Add(PagoTran);
            int intIdPago;
            try
            {
                // 1 er intento
                intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
            }
            catch (Exception err)
            {
                // mensaje("OE fallo primer intento, intIdTipoMoneda=" + intIdTipoMoneda.ToString() + strCadenaResultadoPagoTarjeta, err);
                try
                {
                    // 2 er intento
                    intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                }
                catch (Exception err2)
                {
                    // 3 er intento
                    // mensaje("OE fallo segundo intento, intIdTipoMoneda=" + intIdTipoMoneda.ToString() + strCadenaResultadoPagoTarjeta, err);
                    intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                }
            }
            IkPago.Close();

            return intIdVenta;
        }

        public decimal getTotalVenta(DataTable dtVentaOrdenador)
        {
            decimal respuesta = 0;

            for (int i = 0; i < dtVentaOrdenador.Rows.Count; i++)
            {
                if (bool.Parse(dtVentaOrdenador.Rows[i]["cnEsFacturable"].ToString()) == true)
                    respuesta += (decimal)dtVentaOrdenador.Rows[i]["mnPrecioVenta"];
            }
            return respuesta;
        }

        public void actualizaCompetidorDetalle(int idClienteDetalle)
        {
            wscClienteDetalle.IcClienteDetalleClient cteCLiente = new wscClienteDetalle.IcClienteDetalleClient();
            wscClienteDetalle.cClienteDetalle ClienteDet;
            wscClienteDetalle.cClienteDetalle ClienteDetalle;
            ClienteDetalle = cteCLiente.SeleccionarcClienteDetallePorId(idClienteDetalle, getDsSession());

            // cambiamos el estatus del participante a inscrito
            ClienteDetalle.idEstatusCliente = 15;
            int resModificaDetalles = cteCLiente.ModificarcClienteDetalle(ClienteDetalle, getDsSession());
        }
        public int getNumeroCompetidor(string strIdEventoClasificacion)
        {
            // obtenemos el numero de competidor que le corresponde    
            int intNumeroCompetidor = 0;            
            wsBusinessRules.BusinessRulesServicesClient ruleOper = new wsBusinessRules.BusinessRulesServicesClient();
            Dictionary<string, string> dicret = new Dictionary<string, string>();
            dicret.Add("@idEventoClasificacion", strIdEventoClasificacion);
            DataSet dtResultNumero = ruleOper.ExecuteRule("spGetNumeroCompetidorTriatlon", dicret, getDsSession());
            if (dtResultNumero.Tables[0].Rows[0]["retValue"].ToString() != "0")
            {
                intNumeroCompetidor = int.Parse(dtResultNumero.Tables[0].Rows[0]["numero"].ToString());
            }
            return intNumeroCompetidor;
        }

        protected bool correoPago(string strIdVenta, string strFolios, string strIdioma, string strCorreoCliente, DataTable dtCompedidor)
        {
            //string strParametros = "<br>Parametros pasados a la funcion correoPago: strIdVenta:" + strIdVenta + "<br>strFolios:" + strFolios + "<br>strIdioma:" + strIdioma + "<br>strCorreoCliente:" + strCorreoCliente + "<br>lblMensajeCorreo:" + lblMensajeCorreo.Text;
            int intIdClienteContacto = 0;
            try
            {
                wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();
                iGeneral.Open();
                // Textos Correos
                wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                icConfiguracion.Open();

                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto2CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto2CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto10CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto10CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto11CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto11CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto12CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto12CorreoPagoIdioma_" + strIdioma, getDsSession());
                // wscConfiguracionAplicacion.cConfiguracionAplicacion Texto13CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto13CorreoPagoIdioma_" + strIdioma, Globals.dsSession);
                wscConfiguracionAplicacion.cConfiguracionAplicacion TextoUrlCuponIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-URLCUPON_CanalVenta_27_Id_" + strIdioma, getDsSession());

                icConfiguracion.Close();
                string nombre = dtCompedidor.Rows[0]["dsContacto"].ToString() + ' ' + dtCompedidor.Rows[0]["dsApellidoPaterno"].ToString() + ' ' + dtCompedidor.Rows[0]["dsApellidoMaterno"].ToString();
                string numeroCompetidor = dtCompedidor.Rows[0]["noCompetidor"].ToString();
                string strCuerpoCorreo = "<table>";
                string strCuerpoCorreoLinksFolios = "";
                //strIdVenta = strIdVenta.Remove(strIdVenta.Length - 1);
                string[] arrStrIdVenta = strIdVenta.Split(',');

                for (int ab = 0; ab < arrStrIdVenta.Length; ab++)
                {
                    wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                    ikventa.Open();
                    wsTransactions.kVenta laVenta = ikventa.SeleccionarkVentaPorIdVenta(int.Parse(arrStrIdVenta[ab]), getDsSession());
                    strFolios += laVenta.dsClaveVenta + ",";
                    strCuerpoCorreoLinksFolios += "<a href=\"" + TextoUrlCuponIdioma.dsValor.Replace("<idVenta>", arrStrIdVenta[ab]) + "\" >" + laVenta.dsClaveVenta + "</a>" + ((arrStrIdVenta.Length == 1 || ab == (arrStrIdVenta.Length - 1)) ? "" : ",");
                }
                strFolios = strFolios.Remove(strFolios.Length - 1);
                strCuerpoCorreo += "<tr><td colspan=\"2\"> Estimado/a  <b>" + nombre + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"> Te informamos que de acuerdo a tu solicitud, hemos confirmado tu inscripción. </td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto2CorreoPagoIdioma.dsValor + " " + strFolios + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>Numero Competidor: </b>" + dtCompedidor.Rows[0]["noCompetidor"].ToString() + "</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + "Importante: Por favor lee detenidamente e imprime el documento del enlace (";
                strCuerpoCorreo += strCuerpoCorreoLinksFolios;


                //string[] arrStrFolios = strFolios.Split(',');


                strFolios = strFolios.Remove(strFolios.Length - 1);

                strCuerpoCorreo += ") y presentalo al momento de recoger tu paquete de competidor  </b></td></tr>";

                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">Si deseas una factura, por favor entra a: https://www.aolxcaret.com/core/facturacion, recuerda que tienes 24 horas para solicitar tu factura </td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                //strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto10CorreoPagoIdioma.dsValor + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto11CorreoPagoIdioma.dsValor + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">" + Texto12CorreoPagoIdioma.dsValor + "</td></tr>";

                strCuerpoCorreo += "</table>";

                string strCorreoAgente = "";
                
                bool blnBanderaCorreo = iGeneral.SendEmail(strCorreoCliente, "Confirmación de Inscripción Triatlón Xel-Há", utils.armaHTMLBaseCorreo("Confirmación de Inscripción Triatlón Xel-Há", strCuerpoCorreo), true /*es html*/, "", false, 0, strCorreoAgente);
                //bool blnBanderaCorreo = iGeneral.SendEmail("irojas@experienciasxcaret.com.mx", TituloCorreoIdioma.dsValor, Utils.armaHTMLcorreoCancelacion(TituloCorreoIdioma.dsValor + " XCARET", strCuerpoCorreo), true /*es html*/, "", false, 0, strCorreoAgente);

                if (blnBanderaCorreo)
                {
                    //    lblMensajeCorreo.Text = lblMensajeCorreo.Text.Trim() + ((lblMensajeCorreo.Text.Trim().Length > 0) ? " y " : "") + "Correo Enviado";
                }

                iGeneral.Close();

                return blnBanderaCorreo;
            }
            catch (Exception err)
            {
                //mensaje("correoPago <br>intIdClienteContacto pasado a SeleccionarcClienteContactoPorId:" + intIdClienteContacto.ToString() + " " + strParametros, err);
                return false;
            }
        }

        public void alertRedirect(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + errMensaje + "'); window.location = 'http://www.triatlonxelha.com'</script>", false);
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
                hdDsSession.Value = cGlobals.dsSession;
            }
            return cGlobals.dsSession;
        }

        public int getIdUsuario()
        {
            if (cGlobals.idUsuario == null || cGlobals.idUsuario == 0)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.idUsuario;
        }

        public int getIdCliente()
        {
            if (cGlobals.idCliente == null || cGlobals.idCliente == 0)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.idCliente;
        }
        
    }
}