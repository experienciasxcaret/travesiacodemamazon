﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Windows.Forms;
using System.Data;
using System.Threading;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace travesia
{
    public partial class detallePago : System.Web.UI.Page
    {
        cParticipante cCompetidorPagar = new cParticipante();
        cRespuestaPago cRespuestaPagoCompetidor = new cRespuestaPago();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string prevPage = "";
                if (!IsPostBack)
                {
                    DataSet result;
                    DataSet dtPrecio;
                    hdIV.Value = "";
                    int intCnCena = 0;
                    //int intCnCena = 0;
                    bool cnCruzRoja = false;
                   
                    hdCnCena.Value = intCnCena.ToString();
                    int intIdClienteContactoEvento = 0;
                    utils.Login("usrTriatlon", "eXperiencias");
                    int intIdEstatusVenta = 0;
                    int intIdEstatusCliente = 0;
                    //cGlobals.dsSession = "e0gbam2hoay2myk21x54ajcy";     // Guardar la variable global con el numero de sesion          
                    //cGlobals.dsUsuario = seguridadObject.dsUsuario;       // Guardar la variable global con el usuario
                   // cGlobals.idUsuario = 9; // GUardar la variable global con el id del usuario;
                    //cGlobals.idCliente = 1;       //Guardar la variable global con el id del cliente;
                    //cGlobals.dsNombreUsuario = seguridadObject.dsNombreUsuario;
                    //cGlobals.dsBasedeDatos = seguridadObject.dsBasedeDatos;

                    utils.llenaAnioUp(ddlAnioExpira);
                    // si intCnCena es igual a uno, quiere decir que viene de cena y se tiene que hacer los ajuster
                    // para que pague producto especial
                    if (Request.UrlReferrer != null)
                    {
                        prevPage = Request.UrlReferrer.AbsolutePath.ToString();
                    }
                    int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                    //buscamos informacion del competidor
                    cnIdContactoEvento.Value = intIdClienteContactoEvento.ToString();
                    cCompetidorPagar.DtCobro = null; ;
                    Session["dtCompetidorCobro" + cnIdContactoEvento.Value] = null;
                    Session["cRespuestaPagoCobro" + cnIdContactoEvento.Value] = null;

                    int intIdConfigPago = 0;
                    int intIdVenta = 0;
                    if (intIdClienteContactoEvento > 0)
                    {                       
                        result = null;
                        //result.Clear();
                        try
                        {
                            result = utils.buscarCompetidor(getDsSession(), "", "", "", 0, "", "", 0, 0, 0, intIdClienteContactoEvento);
                        }
                        catch (Exception ex)
                        {
                            result = utils.buscarCompetidor(getDsSession(), "", "", "", 0, "", "", 0, 0, 0, intIdClienteContactoEvento);
                            string msg = ex.Message.ToString();
                            string strginnermessage = "";
                            string strCuerpoCorreo = "detallePago. Page_load(). El Primer intento carga de competidor falló " + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                            string strExcepcion = "";
                            if (ex.ToString().Length > 799)
                                strExcepcion = ex.ToString().Substring(0, 999);
                            else
                                strExcepcion = ex.ToString();
                            strCuerpoCorreo += strExcepcion;
                            MensajeAlert(strCuerpoCorreo);
                        }
                        if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                        {
                            int.TryParse(result.Tables[0].Rows[0]["idEstatusVenta"].ToString(), out intIdEstatusVenta);
                            int.TryParse(result.Tables[0].Rows[0]["idConfigPagoPreregistro"].ToString(), out intIdConfigPago);
                            int.TryParse(result.Tables[0].Rows[0]["idEstatusCliente"].ToString(), out intIdEstatusCliente);
                            
                            // si el intIdConfigPago es igual a 3 significa es cortesia y no va por el proceso de pago                                                       

                            // si el estatus de venta es 7 o 3  no lo dejamos seguir
                            if (intIdEstatusVenta == 7 || intIdEstatusVenta == 3)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "myScript", "showPopUP('Su folio ya ha sido pagado. Favor de validarlo.');", true);
                                return;
                            }

                            // si el estatus del cliente es distinto de 17, es decir registrado, no va poder realizar el pago, solo los registrados podran registrarse
                            if (intIdEstatusCliente != 17)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "myScript", "showPopUP('Liga inactivada');", true);
                                return;
                            }
                            //cParticipante.dtCompedidor.Clear();
                            Session["dtCompetidorPago" + cnIdContactoEvento.Value] = null;
                            cCompetidorPagar.DtCompetidor = null;
                            cCompetidorPagar.DtCompetidor = result.Tables[0];
                            Session["dtCompetidorPago" + cnIdContactoEvento.Value] = result.Tables[0];
                            //cParticipante.dtCompedidor = result.Tables[0];
                            hdIdPais.Value = result.Tables[0].Rows[0]["idPais"].ToString();
                            hdIdEstado.Value = result.Tables[0].Rows[0]["idEstado"].ToString();
                            hdCorreoCompetidor.Value = result.Tables[0].Rows[0]["dsCorreoElectronico"].ToString();
                            if (result.Tables[0].Rows[0]["idConfigPagoPreregistro"] != null)
                                hdIdConfigPago.Value = result.Tables[0].Rows[0]["idConfigPagoPreregistro"].ToString();
                            if (result.Tables[0].Rows[0]["dsAgrupador"].ToString().Trim().ToUpper() == "CENA")
                                hdCnCena.Value = "1";

                            if (hdCnCena.Value == "1")
                            {
                                cnCruzRoja = true;                                
                            }

                            getAndDrowPrecios(result.Tables[0], cnCruzRoja,intIdConfigPago);                            

                            if (hdCnCena.Value == "1")
                            {                                
                                // ahora debemos de ajustar el datatable para que solo se cobre el total de la cena principal
                                ajustaTablaPorCena();
                            }
                            cCompetidorPagar.DtCobro = getDatatableCompetidorPago();                      
                            //cParticipante.dtDataCobro.Columns.Add("mnTotalVenta", typeof(int), "1");                            
                            String jsonDetalleUpgrade = utils.getJson(cCompetidorPagar.DtCobro);
                            // bueno ya tenemos el datatabel, ahora la enviamos al javascript para pintar la tabla
                            // el primer parametro es el datatable
                            // el parametro 1 = es con cena; parametro 0 sin cena                            
                            ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>javascript:construyeTablaDetalle(" + jsonDetalleUpgrade + ", 1);</script>");
                        }
                    }

                    // tenemos que checar que la pagina que venga de la cena entonces quiere decir que va agarrar cena
                    // ademas de que por parametro vamos a indicar si viene de cena, usar parametro productEsp

                    utils.getFormasPago(ddlFormaPago, 27, getDsSession());
                    utils.getBancos(ddlBanco, getDsSession());
                    utils.getMoneda(ddlCambioMoneda, getDsSession());                    
                    ddlCambioMoneda.Attributes.Add("readonly", "readonly");
                    ddlCambioMoneda.Attributes.Add("disabled", "disabled");
                    
                    int feVisitaMin = 0;
                    string strIdTipoMoneda = "2";
                    decimal dcMaxMnTotal = 0;
                    decimal dcMaxMnTotalParcial = 0;
                    feVisitaMin = getFechaMinVisita(cCompetidorPagar.DtCobro, getDsSession());
                    dcMaxMnTotal = getMontoTotal(cCompetidorPagar.DtCobro, getDsSession());
                    dcMaxMnTotalParcial = getMontoTotalParcial(cCompetidorPagar.DtCobro, getDsSession());
                    int intFeActual = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                    if (((feVisitaMin - intFeActual) >= 4) && strIdTipoMoneda == "2")
                    {
                        var resultComproPagoEstablishments = comproPagoCheckout.getBusinessEstablishments2(getDsSession(), ConfigurationManager.AppSettings["bdProduccion"].ToString(), dcMaxMnTotal.ToString());
                        //rblTipoTarjeta.Items.Add();
                        foreach (Dictionary<string, string> provider in resultComproPagoEstablishments)
                        {
                            divComproPago.Visible = true;
                            ListItem ls = new ListItem("<a href='#' alt='' class='' ></a><img src='" + provider["image_medium"].ToString().Trim() + "' style='margin: -6px,0,10px,0; height:40px;width:60px'/>", provider["name"].ToLower().Trim() + '|' + provider["internal_name"].ToLower().Trim(), true);
                            ls.Attributes["prueba"] = "atributoPrueba";
                            rblComproPago.Items.Add(ls);
                        }
                    }
                    /*if (dcMaxMnTotalParcial <= 0)
                    {
                        //panel1DatosTarjetas.Visible = false;
                        //panelDatosTarjeta.Visible = false;
                        divModTarjeta.Style.Add("display", "none");
                        rvTipoTarjeta.Enabled = false;
                        rvFormaPago.Enabled = false;
                        rvBanco.Enabled = false;
                        rvNombreTH.Enabled = false;
                        rvNoTarjeta.Enabled = false;
                        rvMesExpira.Enabled = false;
                        rvAnioExpira.Enabled = false;
                        rvCVV.Enabled = false;
                        divComproPago.Style.Add("display", "none");
                        divRadioFormasPago.Style.Add("display", "none");
                        //divModTarjeta.Visible = false;
                        //panelDatosTarjeta.Attributes.Add("display", "none");
                        btnPagarFinal.Text = "Finalizar";
                    }*/
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                string strginnermessage = "";
                string strCuerpoCorreo =  msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                string strExcepcion = "";
                if (ex.ToString().Length > 799)
                    strExcepcion = ex.ToString().Substring(0, 999);
                else
                    strExcepcion = ex.ToString();
                strCuerpoCorreo += strExcepcion;
                MensajeAlert(strCuerpoCorreo);
            }
        }

        public DataTable getDatatableCompetidor()
        {
            if (cCompetidorPagar.DtCompetidor == null)                
                cCompetidorPagar.DtCompetidor = (DataTable)Session["dtCompetidorPago" + cnIdContactoEvento.Value];                
            return cCompetidorPagar.DtCompetidor;
        }

        public void getAndDrowPrecios(DataTable dtCompetidor, bool cnCruzRoja = false, int intIdConfigPago = 0)
        {
            string strDsCodigo = dtCompetidor.Rows[0]["dsCodigo"].ToString();
            int intIdLocacion = int.Parse(dtCompetidor.Rows[0]["idLocacion"].ToString());
            DataSet result = null;
            try
            {
                // getPrecio(string dsSession, int idLocacion, string dsCodigo, int idTipoCliente = 1, bool cnConRemo = true, string dsIsoMoneda = "MXN")
                result = utils.getPrecio(getDsSession(), intIdLocacion, strDsCodigo, 1, true);
            }
            catch (Exception ex)
            {
                result = utils.getPrecio(getDsSession(), intIdLocacion, strDsCodigo, 1, true);
                string msg = ex.Message.ToString();
                string strginnermessage = "";
                string strCuerpoCorreo = "getAndDrowPrecios(). El Primer intento carga de precios falló " + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                string strExcepcion = "";
                if (ex.ToString().Length > 799)
                    strExcepcion = ex.ToString().Substring(0, 999);
                else
                    strExcepcion = ex.ToString();
                strCuerpoCorreo += strExcepcion;
                MensajeAlert(strCuerpoCorreo);
            }
            DataTable dtCena = null;
            DataTable dtProductoInscripcion = null;
            //  preguntamos si la tabla 2 trae info para esto saber el tamaño del datatable
            int sizeDataSet = result.Tables.Count;
            dtProductoInscripcion = result.Tables[0];
            if (!dtProductoInscripcion.Columns.Contains("ordenador"))
            {
                dtProductoInscripcion.Columns.Add("ordenador", typeof(decimal)).DefaultValue = 1;
            }
            if (!dtProductoInscripcion.Columns.Contains("cantidad"))
            {
                dtProductoInscripcion.Columns.Add("cantidad", typeof(int)).DefaultValue = 1;
            }
            if (!dtProductoInscripcion.Columns.Contains("mnPrecioVenta"))
            {
                dtProductoInscripcion.Columns.Add("mnPrecioVenta", typeof(decimal)).DefaultValue = 1;
            }
            if (!dtProductoInscripcion.Columns.Contains("idMonedaPagar"))
            {
                dtProductoInscripcion.Columns.Add("idMonedaPagar", typeof(decimal)).DefaultValue = 1;
            }
            
            // si es mayor a uno quiere decir que trae tabla para la info de cena
            if (sizeDataSet > 2)
            {
                dtCena = result.Tables[1];
                if (!dtCena.Columns.Contains("ordenador"))
                {
                    dtCena.Columns.Add("ordenador", typeof(decimal)).DefaultValue = 2;
                }
                if (!dtCena.Columns.Contains("cantidad"))
                {
                    dtCena.Columns.Add("cantidad", typeof(int)).DefaultValue = 1;
                }
                if (!dtCena.Columns.Contains("mnPrecioVenta"))
                {
                    dtCena.Columns.Add("mnPrecioVenta", typeof(decimal)).DefaultValue = 1;
                }
                if (!dtCena.Columns.Contains("idMonedaPagar"))
                {
                    dtCena.Columns.Add("idMonedaPagar", typeof(decimal)).DefaultValue = 1;
                }

                for (int i = 0; i < dtCena.Rows.Count; i++)
                {
                    dtCena.Rows[i]["ordenador"] = 2;
                    dtCena.Rows[i]["cantidad"] = 1;
                    dtCena.Rows[i]["idMonedaPagar"] = 2;
                    dtCena.Rows[i]["mnPrecioVenta"] = decimal.Parse(dtCena.Rows[i]["mnPrecio_2"].ToString());
                }
                for (int k = 0; k < dtProductoInscripcion.Rows.Count; k++)
                {
                    dtProductoInscripcion.Rows[k]["ordenador"] = 1;
                    dtProductoInscripcion.Rows[k]["cantidad"] = 1;
                    dtProductoInscripcion.Rows[k]["idMonedaPagar"] = 2;
                    dtProductoInscripcion.Rows[k]["mnPrecioVenta"] = decimal.Parse(dtProductoInscripcion.Rows[k]["mnPrecio_2"].ToString());
                }
            }
            else
            {
                // si no tenemos que ver que dentro de esta tabla existe producto no facturables para enviarlo con otro ordenador
                DataRow[] drNosonFacturables = dtProductoInscripcion.Select("cnEsFacturable = 0");
                DataRow[] drSonFacturables = dtProductoInscripcion.Select("cnEsFacturable = 1");

                foreach (DataRow dtrow in drSonFacturables)
                {
                    if (dtrow["dsClave"].ToString() == "TSMR")
                    {
                        dtrow["ordenador"] = 1;
                        dtrow["cantidad"] = 0;
                        dtrow["idMonedaPagar"] = 2;
                        dtrow["mnPrecioVenta"] = decimal.Parse(dtrow["mnPrecio_2"].ToString());
                    }
                    else
                    { 
                        dtrow["ordenador"] = 1;                    
                        dtrow["cantidad"] = 1;
                        dtrow["idMonedaPagar"] = 2;
                        // si donfigpago es 3, es porque es cortesia la inscripcion
                        if (intIdConfigPago == 3)
                            dtrow["mnPrecioVenta"] = decimal.Parse(dtrow["mnPrecio_2"].ToString()) - decimal.Parse(dtrow["mnPrecio_2"].ToString());
                        else
                            dtrow["mnPrecioVenta"] = decimal.Parse(dtrow["mnPrecio_2"].ToString());
                    }
                }
                foreach (DataRow dtrow in drNosonFacturables)
                {
                    if (dtrow["dsClave"].ToString() == "TSMR")
                    {
                        dtrow["ordenador"] = 2;
                        dtrow["cantidad"] = 0;
                        dtrow["idMonedaPagar"] = 2;
                        dtrow["mnPrecioVenta"] = decimal.Parse(dtrow["mnPrecio_2"].ToString());
                    }
                    else
                    {
                        dtrow["ordenador"] = 2;
                        dtrow["cantidad"] = 1;
                        dtrow["idMonedaPagar"] = 2;
                        dtrow["mnPrecioVenta"] = decimal.Parse(dtrow["mnPrecio_2"].ToString());
                    }
                }
            }

           /* for (int k = 0; k < dtProductoInscripcion.Rows.Count; k++)
            {
                dtProductoInscripcion.Rows[k]["ordenador"] = 1;
                dtProductoInscripcion.Rows[k]["cantidad"] = 1;
                dtProductoInscripcion.Rows[k]["idMonedaPagar"] = 2;
                dtProductoInscripcion.Rows[k]["mnPrecioVenta"] = decimal.Parse(dtProductoInscripcion.Rows[k]["mnPrecio_2"].ToString()); 
            }*/

            if (dtCena != null)
                dtProductoInscripcion.Merge(dtCena);

            //cParticipante.dtDataCobro = dtProductoInscripcion;
            cCompetidorPagar.DtCobro = dtProductoInscripcion;
            Session["dtCompetidorCobro" + cnIdContactoEvento.Value] = dtProductoInscripcion;
        }

        public DataTable getDatatableCompetidorPago()
        {
            if (cCompetidorPagar.DtCobro == null)
                cCompetidorPagar.DtCobro = (DataTable)Session["dtCompetidorCobro" + cnIdContactoEvento.Value];
            return cCompetidorPagar.DtCobro;
        }

        public void getPrecioCena()
        {
            /*DataTable dtResult = utils.getPrecio("Cena");
            dtResult.Columns.Add("cantidad", typeof(int)).DefaultValue = 1;
            cParticipante.dtDataCobro.Merge(dtResult);*/
        }

        public void ajustaTablaPorCena(bool blAjustaCantidad = true)
        {
            //DataTable dtwork = cParticipante.dtDataCobro;
            DataTable dtwork = getDatatableCompetidorPago();
            for (int i = 0; i < dtwork.Rows.Count; i++)
            {
                // todo lo que sea distinto del CENA BENEFICIO CRUZ ROJA (5,000)
                // ponerles en el campo mnPrecioVenta = 0
                if (dtwork.Rows[i]["idProducto"].ToString() != "1613")
                {                    
                    // es el donativo, o las cenas acompañantes la cantidad a cero y entrada xelha menor a cero
                    if (dtwork.Rows[i]["idProducto"].ToString() == "305" && dtwork.Rows[i]["idTipoCliente"].ToString() == "2")
                    {
                        if (blAjustaCantidad)
                        {
                            dtwork.Rows[i]["cantidad"] = 0;
                            dtwork.Rows[i]["mnPrecioVenta"] = 0;
                        }
                    }
                    else if (dtwork.Rows[i]["idProducto"].ToString() == "1614" || dtwork.Rows[i]["idProducto"].ToString() == "1616" || dtwork.Rows[i]["idProducto"].ToString() == "1615")
                    {
                        if (blAjustaCantidad)
                        {
                            dtwork.Rows[i]["cantidad"] = 0;
                            dtwork.Rows[i]["mnPrecioVenta"] = 0;
                        }                        
                    }
                    else
                    {
                        dtwork.Rows[i]["mnPrecioVenta"] = 0;
                    }                    
                }
                //cParticipante.dtDataCobro.Rows[i]["cantidad"] = 1;
            }
            //cParticipante.dtDataCobro = dtwork;
            cCompetidorPagar.DtCobro = dtwork;
            Session["dtCompetidorCobro" + cnIdContactoEvento.Value] =dtwork;
        }

        protected void rblTipoTarjeta_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                /*
                 * si el valor es :
                 * 1 = visa
                 * 2 = mastercard
                 * 3 = amex   
                 * <option value="">Seleccionar Forma de pago</option>
	                <option value="36">American Express TPV Virtual</option>
	                <option value="28">Tarjeta Credito / Mastercard TPV Virtual</option>
	                <option value="27">Tarjeta Credito / Visa TPV Virtual</option>
	                <option value="30">Tarjeta Debito / Mastercard TPV Virtual</option>
	                <option value="29">Tarjeta Debito / Visa TPV Virtual</option>
                 * */
                string strTipoTarjeta = rblTipoTarjeta.SelectedValue.ToString();
                divModTarjeta.Visible = true;
                foreach (ListItem li in ddlFormaPago.Items)
                {
                    li.Enabled = true;                    
                }
                ddlFormaPago.SelectedIndex = -1;
                ddlBanco.Attributes.Remove("readonly");
                ddlBanco.Attributes.Remove("disabled");
                ddlBanco.Items.FindByValue("35").Enabled = true;
                ddlBanco.SelectedIndex = -1;

                switch (strTipoTarjeta)
                {   // si el seleccionado es 1 tenemos que dejar solo visible en las formas de pago las que son de mastercard
                    case "1":
                        //ddlFormaPago.Items.FindByValue("36").Attributes.Add("style", "visibility: hidden");
                        //ddlFormaPago.Items.FindByValue("27").Attributes.Add("style", "visibility: hidden");
                        //ddlFormaPago.Items.FindByValue("29").Attributes.Add("style", "visibility: hidden");
                        ddlFormaPago.Items.FindByValue("36").Enabled = false;
                        ddlFormaPago.Items.FindByValue("27").Enabled = false;
                        ddlFormaPago.Items.FindByValue("29").Enabled = false;
                        ddlFormaPago.Items.FindByValue("28").Selected = true;
                        ddlBanco.Items.FindByValue("35").Enabled = false;
                        break;
                    // si el seleccionado es 2 tenemos que dejar solo visible en las formas de pago las que son de visa
                    case "2":
                        ddlFormaPago.Items.FindByValue("36").Enabled = false;
                        ddlFormaPago.Items.FindByValue("28").Enabled = false;
                        ddlFormaPago.Items.FindByValue("30").Enabled = false;
                        ddlFormaPago.Items.FindByValue("27").Selected = true;
                        ddlBanco.Items.FindByValue("35").Enabled = false;
                        break;
                    // si el seleccionado es 3 tenemos que dejar solo visible en las formas de pago las que son de amex
                    case "3":
                        ddlFormaPago.Items.FindByValue("27").Enabled = false;
                        ddlFormaPago.Items.FindByValue("29").Enabled = false; 
                        ddlFormaPago.Items.FindByValue("28").Enabled = false;
                        ddlFormaPago.Items.FindByValue("30").Enabled = false;
                        ddlFormaPago.Items.FindByValue("36").Selected = true;
                        rblTipoTarjeta.Items.FindByValue("3").Selected = true;
                        ddlBanco.Items.FindByValue("35").Selected = true;
                        ddlBanco.Attributes.Add("readonly", "readonly");
                        ddlBanco.Attributes.Add("disabled", "disabled");
                        break;
                    default:
                        break;
                }
                string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
                string dsIsoMoneda = selectedValueMoneda[1].ToString();
                string idMoneda = selectedValueMoneda[0].ToString();
                rvTipoTarjeta.Enabled = true;
                rvBanco.Enabled = true;
                rvNombreTH.Enabled = true;
                rvNoTarjeta.Enabled = true;
                rvAnioExpira.Enabled = true;
                rvCVV.Enabled = true;
                hdCnCompropago.Value = "0";
                rblComproPago.ClearSelection();
                //if (ddlBanco.SelectedValue.ToString() != "") /**/
               
                ddlFormaPago_SelectedIndexChanged(sender, e);
                if (ddlBanco.SelectedValue.ToString() != "" && ddlFormaPago.SelectedValue.ToString() != "")
                {
                    validaCamposGlobal(idMoneda, ddlBanco.SelectedValue.ToString());
                    //validaMsi();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void ddlFormaPago_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                /*                   
                 * <option value="">Seleccionar Forma de pago</option>
                    <option value="36">American Express TPV Virtual</option>
                    <option value="28">Tarjeta Credito / Mastercard TPV Virtual</option>
                    <option value="27">Tarjeta Credito / Visa TPV Virtual</option>
                    <option value="30">Tarjeta Debito / Mastercard TPV Virtual</option>
                    <option value="29">Tarjeta Debito / Visa TPV Virtual</option>
                 * */
                string strIdFormaPago = ddlFormaPago.SelectedValue.ToString();
                bool cnValidaGlobal = true;
                lblNumTarjeta.Visible = false;
                txtNoTarjeta.Visible = false;
                lblFechaExpira.Visible = false;
                //txtMes.Visible = true;
                //txtAnio.Visible = true;
                ddlAnioExpira.Visible = false;
                ddlMesExpira.Visible = false;
                lblCVV.Visible = false;
                txtCVV.Visible = false;
                lblNombreTh.Visible = false;
                txtNombreTH.Visible = false;
                //txtApellidoTH.Visible = false;
                lblReferencia.Visible = true;
                txtReferencia.Visible = true;
                rvReferencia.Enabled = true;
                lblBanco.Visible = false;
                ddlBanco.Visible = false;

                rblTipoTarjeta.ClearSelection();
                ddlFormaPago.Enabled = true;

                ddlBanco.Attributes.Remove("readonly");
                ddlBanco.Attributes.Remove("disabled");
                ddlBanco.Items.FindByValue("35").Enabled = true;
                ddlBanco.SelectedIndex = -1;
                string strIdCanalVenta = "27";
                wscFormaPago.ListacFormaPagos lstFormaPagos;
                if (Session["lstFormaPagosAltaCotizacion" + strIdCanalVenta] == null)
                {
                    wscFormaPago.IcFormaPagoClient IcFormaPago = new wscFormaPago.IcFormaPagoClient();
                    IcFormaPago.Open();
                    lstFormaPagos = IcFormaPago.SeleccionarcFormaPagosPorIdCanalVenta(int.Parse(strIdCanalVenta), cGlobals.dsSession);
                    Session["lstFormaPagosAltaCotizacion" + strIdCanalVenta] = lstFormaPagos;
                    IcFormaPago.Close();

                }
                else
                {
                    lstFormaPagos = (wscFormaPago.ListacFormaPagos)Session["lstFormaPagosAltaCotizacion" + strIdCanalVenta];
                }

                #region listFormaPagos
                for (int cnt = 0; cnt < lstFormaPagos.Count; cnt++)
                {
                    if (lstFormaPagos[cnt].idFormaPago.ToString() == ddlFormaPago.SelectedItem.Value) // idFormaPago
                    {
                        if (lstFormaPagos[cnt].idTipoPago == 2) // si es pago con tarjeta
                        {
                            lblBanco.Visible = true;
                            ddlBanco.Visible = true;
                            //rfvBanco.Enabled = true;
                            lblNumTarjeta.Visible = true;
                            txtNoTarjeta.Visible = true;
                            //PanelPago.Visible = true;
                            //rfvNoTarjeta16dig.Enabled = true;
                            //rfvNoTarjeta2.Enabled = true;
                            lblFechaExpira.Visible = true;
                            ddlAnioExpira.Visible = true;
                            ddlMesExpira.Visible = true;
                            lblCVV.Visible = true;
                            txtCVV.Visible = true;
                            lblNombreTh.Visible = true;
                            txtNombreTH.Visible = true;
                            //txtApellidoTH.Visible = true;

                            // Seria mejor si en la tabla cFormaPago se agrega la referencia al campo idTipoTarjeta de la tabla cTipoTarjeta
                            wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                            icConfiguracion.Open();
                            wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionFormaPagoVisa = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("BACKOFFICE_Visa_idFormaPago-idTipoTarjeta", cGlobals.dsSession);
                            wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionFormaPagoMasterCard = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("BACKOFFICE_MasterCard_idFormaPago-idTipoTarjeta", cGlobals.dsSession);
                            wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionFormaPagoAmex = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("BACKOFFICE_Amex_idFormaPago-idTipoTarjeta", cGlobals.dsSession);

                            wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionFormaPagoDevitoMasterCard = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("BACKOFFICE_Dev_MasterCard_idFormaPago-idTipoTarjeta", cGlobals.dsSession);
                            wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionFormaPagoDevitoVisa = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("BACKOFFICE_Dev_Visa_idFormaPago-idTipoTarjeta", cGlobals.dsSession);


                            icConfiguracion.Close();

                            ddlBanco.Enabled = true;

                            //ddlFormaPago.Enabled = true;
                            ddlBanco.Items.FindByValue("35").Enabled = true;
                            //ddlFormaPago.Items.FindByValue("3").Enabled = true;
                            //  American Express
                            if (ddlFormaPago.SelectedItem.Value == "5" || ddlFormaPago.SelectedItem.Value == "36")
                            {
                                //ddlBanco.SelectedValue = "35";
                                ///ddlBanco.Enabled = false;
                                ddlBanco.SelectedIndex = -1;
                                rblTipoTarjeta.Items.FindByValue("3").Selected = true;
                                ddlBanco.Items.FindByValue("35").Selected = true;
                                ddlBanco.Attributes.Add("readonly", "readonly");
                                ddlBanco.Attributes.Add("disabled", "disabled");
                            }
                            else
                            {
                                ddlBanco.Items.FindByValue("35").Enabled = false;
                                // ddlFormaPago.Items.FindByValue("3").Enabled = false;

                                ddlBanco.SelectedIndex = -1;
                                //ddlFormaPago.SelectedIndex = -1;

                                if (ddlBanco.SelectedItem.Value == "48")
                                {
                                    // habilitaValidaciónLongitudTarjeta(3); // Diners Club
                                }
                                else
                                {
                                    //  habilitaValidaciónLongitudTarjeta(1); //  Normal
                                }
                            }

                            if (lstFormaPagos[cnt].idTipoTarjeta.ToString().Length > 0)
                            {
                                //cmbTipoTarjeta.SelectedValue = lstFormaPagos[cnt].idTipoTarjeta.ToString();
                                //cmbTipoTarjeta.Enabled = false;
                                hdEsPagoTarjeta.Value = "2"; // Si es de tipo tarjera de inicio todos son terminal
                            }

                            string[] strVisaIdFormaPagoIdTipoTarjeta = cConfiguracionFormaPagoVisa.dsValor.Split('-');
                            string[] strMasterIdFormaPagoIdTipoTarjeta = cConfiguracionFormaPagoMasterCard.dsValor.Split('-');
                            string[] strAmexIdFormaPagoIdTipoTarjeta = cConfiguracionFormaPagoAmex.dsValor.Split('-');

                            string[] strMasterIdFormaPagoDevitoIdTipoTarjeta = cConfiguracionFormaPagoDevitoMasterCard.dsValor.Split('-');
                            string[] strVisaIdFormaPagoDevitoIdTipoTarjeta = cConfiguracionFormaPagoDevitoVisa.dsValor.Split('-');

                            if (ddlFormaPago.SelectedItem.Value == strVisaIdFormaPagoIdTipoTarjeta[0] || ddlFormaPago.SelectedItem.Value == strMasterIdFormaPagoIdTipoTarjeta[0] || ddlFormaPago.SelectedItem.Value == strAmexIdFormaPagoIdTipoTarjeta[0] || ddlFormaPago.SelectedItem.Value == strMasterIdFormaPagoDevitoIdTipoTarjeta[0] || ddlFormaPago.SelectedItem.Value == strVisaIdFormaPagoDevitoIdTipoTarjeta[0]) // Tipo de Tarjeta Credito: Visa o Master Lectora : Amex
                            {
                                lblBanco.Visible = true;
                                ddlBanco.Visible = true;
                                lblNumTarjeta.Visible = true;
                                txtNoTarjeta.Visible = true;
                                lblFechaExpira.Visible = true;
                                ddlAnioExpira.Visible = true;
                                ddlMesExpira.Visible = true;
                                lblCVV.Visible = true;
                                txtCVV.Visible = true;
                                lblReferencia.Visible = false;
                                txtReferencia.Visible = false;
                                rvReferencia.Enabled = false;
                                hdEsPagoTarjeta.Value = "1"; // Se cambia a tarjeta por que son por TPV Virtual

                                if (ddlFormaPago.SelectedItem.Value == strAmexIdFormaPagoIdTipoTarjeta[0]) //  Amex Tarjeta Credito
                                {
                                    //cmbTipoTarjeta.SelectedValue = strAmexIdFormaPagoIdTipoTarjeta[1];
                                    //cmbTipoTarjeta.Enabled = false;
                                    //rfvNoTarjeta15dig.Enabled = true;//  Amex Tarjeta Credito
                                    //rfvNoTarjeta16dig.Enabled = false;
                                    //rfvNoTarjeta14dig.Enabled = false;
                                    //habilitaValidaciónLongitudTarjeta(2); //  American Express
                                }
                                // -----------------------------------------------------------------------
                                // -----      Global collect
                                // -----------------------------------------------------------------------
                                //if (cConfigGlobalCollectActivo.dsValor == "1")  // Esta activo Global collect
                                //{                                ;
                                //validaCamposGlobal(string strIdMoneda, string idBanco);
                                //}
                            } // if Tipo de Tarjeta Credito
                        }// if idTipoPago == 2 Tarjeta
                        else //if (lstFormaPagos[cnt].idTipoPago == 1)
                        {
                            // se deshabilita el campo de referencia
                            List<int> lstFormaPagoEfectivo = new List<int>(new int[] { 1, 2, 3, 4 });
                            if (lstFormaPagoEfectivo.Contains(int.Parse(ddlFormaPago.SelectedItem.Value)))
                            {
                                lblReferencia.Visible = false;
                                txtReferencia.Visible = false;
                                rvReferencia.Enabled = false;
                                lblNombreTh.Visible = false;
                                txtNombreTH.Visible = false;
                                rvTipoTarjeta.Enabled = false;
                                hdEsPagoTarjeta.Value = "3";
                            }
                        }
                        cnt = lstFormaPagos.Count;
                    }
                }// for
                #endregion listFormaPagos

                /*if (strIdFormaPago == "36" || strIdFormaPago == "5")
		{
			ddlBanco.SelectedIndex = -1;
			rblTipoTarjeta.Items.FindByValue("3").Selected = true;
			ddlBanco.Items.FindByValue("35").Selected = true;
			ddlBanco.Attributes.Add("readonly", "readonly");
			ddlBanco.Attributes.Add("disabled", "disabled");
			//ddlBanco.Enabled = false;
		}
		else*/
                if (strIdFormaPago == "28" || strIdFormaPago == "30" || strIdFormaPago == "7" || strIdFormaPago == "18")
                {
                    rblTipoTarjeta.Items.FindByValue("1").Selected = true;
                    ddlBanco.Items.FindByValue("35").Enabled = false;
                }
                else if (strIdFormaPago == "27" || strIdFormaPago == "29" || strIdFormaPago == "6" || strIdFormaPago == "17")
                {
                    rblTipoTarjeta.Items.FindByValue("2").Selected = true;
                    ddlBanco.Items.FindByValue("35").Enabled = false;
                }
                string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
                string dsIsoMoneda = selectedValueMoneda[1].ToString();
                string idMoneda = selectedValueMoneda[0].ToString();
                string idBanco = ddlBanco.SelectedValue.ToString();
                string idFormaPago = ddlFormaPago.SelectedValue.ToString();

                // formas de pago de deposito o cortesía
                if (strIdFormaPago == "21" || strIdFormaPago == "22" || strIdFormaPago == "23" || strIdFormaPago == "24" || strIdFormaPago == "14")
                {
                    lblReferencia.Visible = true;
                    txtReferencia.Visible = true;
                    cnValidaGlobal = false;
                    rblTipoTarjeta.Visible = false;
                    rvTipoTarjeta.Enabled = false;
                    hdEsPagoTarjeta.Value = "3";
                }
                // si son terminal
                if (strIdFormaPago == "5" || strIdFormaPago == "50" || strIdFormaPago == "7" || strIdFormaPago == "6" || strIdFormaPago == "18" || strIdFormaPago == "17")
                {
                    /*lblFechaExpira.Visible = false;
                    ddlAnioExpira.Visible = false;
                    ddlMesExpira.Visible = false;*/
                    lblCVV.Visible = false;
                    txtCVV.Visible = false;
                    /*lblReferencia.Visible = true;
                    txtReferencia.Visible = true;*/
                    cnValidaGlobal = false;
                }
                hdCnGlobalCollect.Value = "0";
                hdCnGlobalCollectI.Value = "0";
                if (idBanco != "" && idBanco != "35" && idFormaPago != "" && idFormaPago != "36" && cnValidaGlobal)
                    validaCamposGlobal(idMoneda, idBanco);
            }
            catch (Exception ex)
            {
            }
        }

        protected void ddlBanco_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
            string dsIsoMoneda = selectedValueMoneda[1].ToString();
            string idMoneda = selectedValueMoneda[0].ToString();
            string idBanco = ddlBanco.SelectedValue.ToString();
            lblRespuestaPago.Text = "";
            validaCamposGlobal(idMoneda, idBanco);
        }

        protected void ddlCambioMoneda_SelectedIndexChanged(object sender, EventArgs e)
        {
           // cParticipante.dtCompedidor = result.Tables[0];
            bool cnCruzRoja = false;
           // wscMoneda.IcMonedaClient IcMoneda = new wscMoneda.IcMonedaClient();
            //IcMoneda.Open();
            //wscMoneda.ListacMonedas lstMonedas = IcMoneda.SeleccionarcMonedaPorId();
            //IcMoneda.Close();
            if (hdCnCena.Value == "1")
            {
                cnCruzRoja = true;
            }
            DataTable dt = getDatatableCompetidorPago();
            DataTable dtCompetidor = getDatatableCompetidor();
            string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
            string dsIsoMoneda = selectedValueMoneda[1].ToString();
            string strDsCodigo = dtCompetidor.Rows[0]["dsCodigo"].ToString();
            string idMoneda = selectedValueMoneda[2].ToString();
            string idBanco = ddlBanco.SelectedValue.ToString();
            string idFormaPago = ddlFormaPago.SelectedValue.ToString();
            bool cnInternational = false;
            decimal dcTipoCambioNuevo = 0;
            decimal dcTipoCambioPesoDolar = 0;
            string idTipoMoneda = "";
            // Ahora tengo que investigar el precio del nuevo tipo de moneda para que se actualice en la pantalla
            DataSet result = utils.getPrecio(getDsSession(), 145, strDsCodigo, 1, cnCruzRoja, dsIsoMoneda);
            DataTable dtCena = null;
            DataTable dtProductoInscripcion = null;
            dtProductoInscripcion = result.Tables[0];
            //  preguntamos si la tabla 2 trae info para esto saber el tamaño del datatable
            int sizeDataSet = result.Tables.Count;
            if (sizeDataSet > 1)
            {
                dtProductoInscripcion.Merge(result.Tables[1]);
            }

            ///////////*******************************************************************////////////////////////
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int idProductoPrecioDataCobro = (int)dt.Rows[i]["idProductoPrecio"];
                for (int k = 0; k < dtProductoInscripcion.Rows.Count; k++)
                {
                    if (idProductoPrecioDataCobro == (int)dtProductoInscripcion.Rows[k]["idProductoPrecio"])
                    {
                        dt.Rows[i]["RetValue"] = dtProductoInscripcion.Rows[k]["RetValue"];
                        dt.Rows[i]["idMoneda_1"] = dtProductoInscripcion.Rows[k]["idMoneda_1"];
                        dt.Rows[i]["ClaveMoneda_1"] = dtProductoInscripcion.Rows[k]["ClaveMoneda_1"];
                        dt.Rows[i]["mnPrecio_1"] = dtProductoInscripcion.Rows[k]["mnPrecio_1"];
                        dt.Rows[i]["mnAhorro_1"] = dtProductoInscripcion.Rows[k]["mnAhorro_1"];
                        dt.Rows[i]["mnPrecioDescuento_1"] = dtProductoInscripcion.Rows[k]["mnPrecioDescuento_1"];
                        dt.Rows[i]["mnTipoCambio_1"] = dtProductoInscripcion.Rows[k]["mnTipoCambio_1"];

                        dt.Rows[i]["idMoneda_2"] = dtProductoInscripcion.Rows[k]["idMoneda_2"];
                        dt.Rows[i]["ClaveMoneda_2"] = dtProductoInscripcion.Rows[k]["ClaveMoneda_2"];
                        dt.Rows[i]["mnPrecio_2"] = dtProductoInscripcion.Rows[k]["mnPrecio_2"];
                        dt.Rows[i]["mnAhorro_2"] = dtProductoInscripcion.Rows[k]["mnAhorro_2"];
                        dt.Rows[i]["mnPrecioDescuento_2"] = dtProductoInscripcion.Rows[k]["mnPrecioDescuento_2"];
                        dt.Rows[i]["mnTipoCambio_2"] = dtProductoInscripcion.Rows[k]["mnTipoCambio_2"];

                        dt.Rows[i]["idMoneda_3"] = dtProductoInscripcion.Rows[k]["idMoneda_3"];
                        dt.Rows[i]["ClaveMoneda_3"] = dtProductoInscripcion.Rows[k]["ClaveMoneda_3"];
                        dt.Rows[i]["mnPrecio_3"] = dtProductoInscripcion.Rows[k]["mnPrecio_3"];
                        dt.Rows[i]["mnAhorro_3"] = dtProductoInscripcion.Rows[k]["mnAhorro_3"];
                        dt.Rows[i]["mnPrecioDescuento_3"] = dtProductoInscripcion.Rows[k]["mnPrecioDescuento_3"];
                        dt.Rows[i]["mnTipoCambio_3"] = dtProductoInscripcion.Rows[k]["mnTipoCambio_3"];

                        dt.Rows[i]["idMoneda_4"] = dtProductoInscripcion.Rows[k]["idMoneda_4"];
                        dt.Rows[i]["ClaveMoneda_4"] = dtProductoInscripcion.Rows[k]["ClaveMoneda_4"];
                        dt.Rows[i]["mnPrecio_4"] = dtProductoInscripcion.Rows[k]["mnPrecio_4"];
                        dt.Rows[i]["mnAhorro_4"] = dtProductoInscripcion.Rows[k]["mnAhorro_4"];
                        dt.Rows[i]["mnPrecioDescuento_4"] = dtProductoInscripcion.Rows[k]["mnPrecioDescuento_4"];
                        dt.Rows[i]["mnTipoCambio_4"] = dtProductoInscripcion.Rows[k]["mnTipoCambio_4"];

                        dt.Rows[i]["prDescuentoPrecompra"] = dtProductoInscripcion.Rows[k]["prDescuentoPrecompra"];

                        dt.Rows[i]["idMoneda_MS"] = dtProductoInscripcion.Rows[k]["idMoneda_MS"];
                        dt.Rows[i]["ClaveMoneda_MS"] = dtProductoInscripcion.Rows[k]["ClaveMoneda_MS"];
                        dt.Rows[i]["mnPrecio_MS"] = dtProductoInscripcion.Rows[k]["mnPrecio_MS"];
                        dt.Rows[i]["mnAhorro_MS"] = dtProductoInscripcion.Rows[k]["mnAhorro_MS"];
                        dt.Rows[i]["mnPrecioDescuento_MS"] = dtProductoInscripcion.Rows[k]["mnPrecioDescuento_MS"];
                        dt.Rows[i]["mnTipoCambio_MS"] = dtProductoInscripcion.Rows[k]["mnTipoCambio_MS"];

                        if (dtProductoInscripcion.Rows[k]["mnPrecio_MS"].ToString() != "")
                        {
                            dt.Rows[i]["mnPrecioVenta"] = (decimal)((decimal)dtProductoInscripcion.Rows[k]["mnPrecio_MS"] * decimal.Parse(dt.Rows[i]["cantidad"].ToString()));
                            cnInternational = true;
                            dcTipoCambioNuevo = (decimal)dt.Rows[i]["mnTipoCambio_MS"];
                            idTipoMoneda = dt.Rows[i]["idMoneda_MS"].ToString();
                        }
                        else
                        {
                            // si uno de los campos de _MS viene vacío quiere decir que no es internaciontal y buscamos el nuevo precio con los campos de prefijo _1, _2,_3,_4
                            string mnPrecio_ = "mnPrecio_";
                            string mnTipoCambio_ = "mnTipoCambio_";
                            string idMoneda_ = "idMoneda_";
                            mnPrecio_ += idMoneda.ToString();
                            mnTipoCambio_ += idMoneda.ToString();
                            idMoneda_ += idMoneda.ToString();
                            dt.Rows[i]["mnPrecioVenta"] = (decimal)((decimal)dtProductoInscripcion.Rows[k][mnPrecio_] * decimal.Parse(dt.Rows[i]["cantidad"].ToString()));
                            dcTipoCambioNuevo = (decimal)dtProductoInscripcion.Rows[k][mnTipoCambio_];
                            idTipoMoneda = dt.Rows[i][idMoneda_].ToString();
                        }
                        dcTipoCambioPesoDolar = (decimal)dt.Rows[i]["mnTipoCambio_2"];
                        dt.Rows[i]["idMonedaPagar"] = int.Parse(idMoneda.ToString());

                        if (dt.Rows[i]["dsClave"].ToString() == "TXHFFC")
                        {
                            dt.Rows[i]["cantidad"] = 0;
                        }
                    }// fin del if (idProductoPrecioDataCobro == (int)result.Rows[i]["idProductoPrecio"]) 
                } // fin del for (int k = 0; k < result.Rows.Count; k++)
            }

            if (hdCnCena.Value == "1")
            {
                // ahora debemos de ajustar el datatable para que solo se cobre el total de la cena principal
                ajustaTablaPorCena(false);
            }

            if (idBanco != "" && idFormaPago != "" && idFormaPago != "36")
                validaCamposGlobal(idMoneda, idBanco);

            // Ahora voy a cambiar el valor de dropdownlist, al valor que le corresponde al nuevo tipo de cambio
            decimal dcValorDdlDonativo = 0;
            DropDownList ResultsLabel = FindControl("1616") as DropDownList;            
                
            String jsonDetalleUpgrade = utils.getJson(dt);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "myScript", "construyeTablaDetalle(" + jsonDetalleUpgrade + ", 1);actualizaCombo('" + cnInternational + "'," + dcTipoCambioNuevo + "," + dcTipoCambioPesoDolar + ","+idTipoMoneda+")", true);
        }

        protected void validaCamposGlobal(string strIdMoneda, string idBanco)
        {
            try
            {
                DataSet dsConfiguracionPagosGC;
                string strIdTipoMoneda = "";
                //string strIdCanalVenta = ConfigurationManager.AppSettings["idCanalVenta"].ToString();
                string strIdCanalVenta = "11";
                bool blBanderaGlobalCollect = false;
                bool blBanderaGlobalCollectInternationalLove = false;
               
                lblNumTarjeta.Visible = true;                
                txtNoTarjeta.Visible = true;
                lblFechaExpira.Visible = true;
                //txtMes.Visible = true;
                //txtAnio.Visible = true;
                ddlAnioExpira.Visible = true;
                ddlMesExpira.Visible = true;
                lblCVV.Visible = true;
                

                wscMoneda.IcMonedaClient IcMoneda = new wscMoneda.IcMonedaClient();
                IcMoneda.Open();
                wscMoneda.ListacMonedas lstMonedas = IcMoneda.SeleccionarcMonedas(getDsSession());
                IcMoneda.Close();

                hdCnGlobalCollectI.Value = "0";
                hdCnGlobalCollect.Value = "0";

                for (int cntMoneda = 0; cntMoneda < lstMonedas.Count; cntMoneda++)
                {
                    if ((int)lstMonedas[cntMoneda].idMoneda == Int32.Parse(strIdMoneda))
                    {
                        strIdTipoMoneda = lstMonedas[cntMoneda].idTipoMoneda.ToString();
                    }
                }
             
                string strPromosionMSI = "0";
                bool blBanderaWorldpay = false;
                bool blBanderaIngenico = false;

                wsBusinessRules.BusinessRulesServicesClient ruleOper = new wsBusinessRules.BusinessRulesServicesClient();
                Dictionary<string, string> dicret = new Dictionary<string, string>();
                dicret.Clear();
                //dicret.Add("@TIPOCONSULTA", "CANAL");
                dicret.Add("@TIPOCONSULTA", "VALIDA");
                dicret.Add("@idCanalVenta", strIdCanalVenta);
                dicret.Add("@idTipoMoneda", strIdTipoMoneda);
                dicret.Add("@idBanco", ddlBanco.SelectedItem.Value);
                dicret.Add("@noPagosMSI", strPromosionMSI); 
                dsConfiguracionPagosGC = ruleOper.ExecuteRule("spGETcConfiguracionPagosGC", dicret, getDsSession());
                Session["cConfiguracionPagosGC" + strIdTipoMoneda] = dsConfiguracionPagosGC;
                dicret.Clear();
                ruleOper.Close();
                if (dsConfiguracionPagosGC.Tables[0].Rows[0]["RetValue"].ToString() == "1")
                {
                    //string strPromosionMSI = (cmbPromosionMSI.SelectedItem.Value == "1") ? "0" : cmbPromosionMSI.SelectedItem.Value;
                    hddPagoGateWay.Value = "1"; // Tarjetas (Amex, Bancomer, Banamex)                    
                    DataRow[] dtArrComponentes = dsConfiguracionPagosGC.Tables[0].Select("idBanco=" + idBanco + " and idTipoMoneda=" + strIdTipoMoneda + " and idCanalVenta=" + strIdCanalVenta + " and noPagosMSI=" + strPromosionMSI);
                    switch (dtArrComponentes[0]["dsGateway"].ToString())
                    {
                        case "GLOBAL COLLECT / INGENICO":
                            wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                            icConfiguracion.Open();
                            wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionIngenicoActivo = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-IngenicoActivo", getDsSession());
                            icConfiguracion.Close();
                            if (cConfiguracionIngenicoActivo != null)
                            {
                                if (cConfiguracionIngenicoActivo.dsValor == "0")
                                {
                                    blBanderaGlobalCollect = true;
                                    hdCnGlobalCollect.Value = "1";
                                    lblEtiGlobalCollect.Text = "Global Collect";
                                    if (dtArrComponentes[0]["cnInternacional"].ToString() == "True")
                                    {
                                        hdCnGlobalCollectI.Value = "1";
                                        lblEtiGlobalCollect.Text = "Global Collect Internacional";
                                    }
                                    hddPagoGateWay.Value = "2"; // GLOBALCOLLECT
                                }
                                else
                                {
                                    blBanderaIngenico = true;
                                    hddPagoGateWay.Value = "4"; // Ingenico
                                    //hddPagoGateWay.Value = "3"; // WORLD PAY Para pruebas
                                    //blBanderaWorldpay = true;
                                }
                            }
                            break;
                        case "WORLDPAY":
                            hddPagoWorldPay.Value = "1";
                            blBanderaWorldpay = true;
                            lblEtiGlobalCollect.Text = "WORLD PAY";
                            hddPagoGateWay.Value = "3"; // WORLD PAY                                    
                            break;
                        default:
                            hddPagoGateWay.Value = "1"; // Tarjetas (Amex, Bancomer, Banamex)
                            break;
                    }                    
                }
                else
                {
                    hddPagoGateWay.Value = "1";  // Tarjetas (Amex, Bancomer, Banamex)
                }
                if (blBanderaGlobalCollect || blBanderaWorldpay)
                {
                    lblNumTarjeta.Visible = false;
                    txtNoTarjeta.Visible = false;
                    lblFechaExpira.Visible = false;
                   // txtMes.Visible = false;
                   // txtAnio.Visible = false;
                    ddlAnioExpira.Visible = false;
                    ddlMesExpira.Visible = false;
                    lblCVV.Visible = false;
                    txtCVV.Visible = false;
                }
            }
            catch (Exception ex)
            {
 
            }
        }

        public cRespuestaPago getObjetoRespuestaPago()
        {
            if (cRespuestaPagoCompetidor == null)
                cRespuestaPagoCompetidor = (cRespuestaPago)Session["cRespuestaPagoCobro" + cnIdContactoEvento.Value];
            return cRespuestaPagoCompetidor;
        }

        protected void btnPagarFinal_Click(object sender, EventArgs e)
        {
            try
            {
                //btnPagarFinal.Visible = false;
                if (Session["dtCompetidorPago" + cnIdContactoEvento.Value] == null)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "myScript", "reloadPage('Se terminó su session, se recargará la pagina para reanudar su proceso');", true);
                    return;
                }

                hdIV.Value = "";
                int intIdCliente = cGlobals.idUsuario;
                decimal dcTotal = decimal.Parse(hdToTalPagar.Value);
//                string idMoneda = ddlCambioMoneda.SelectedValue.ToString();
                string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
                string dsIsoMoneda = selectedValueMoneda[1].ToString();
                string idMoneda = selectedValueMoneda[0].ToString();
                string strIdClienteCompetidor = "";
                bool respGuardaPago = false;
                int intRespIdVenta = fnGuardarVenta(intIdCliente, dcTotal, idMoneda);
                hdIdVenta.Value = intRespIdVenta.ToString();
                string strFolios = "";
                string strIdVenta = "";
                string[] strIdVentas = hdIV.Value.Split(',');
                int respActualizaCompetidor = 0;

                cRespuestaPagoCompetidor.strIdEstatusPago = "2";
                cRespuestaPagoCompetidor.strAfiliacion = "";
                cRespuestaPagoCompetidor.strDsRespuesta = "Aprobado";
                cRespuestaPagoCompetidor.strDsJustificacion = "";
                cRespuestaPagoCompetidor.strDsReferencia = txtReferencia.Text;
                cRespuestaPagoCompetidor.strDsTransaccion = "";
                cRespuestaPagoCompetidor.strDsCorrelacion = "";
                cRespuestaPagoCompetidor.strIdBancoReceptor = ddlBanco.SelectedValue;               

                Session["cRespuestaPagoCobro" + cnIdContactoEvento.Value] = cRespuestaPagoCompetidor;
                //compropago
                Dictionary<string, string> response = null;
                string responseOrder = "";
                if (hdCnCompropago.Value == "1")
                {
                    //getOrder(string dsSessionId, string db, string idVenta, string provider, string strTotal, string idCanalVenta)                   
                    responseOrder = comproPagoCheckout.getOrder(getDsSession(), ConfigurationManager.AppSettings["bdProduccion"].ToString(), intRespIdVenta.ToString(), rblComproPago.SelectedValue.Trim(), dcTotal.ToString(), "73");
                    if (responseOrder != "500")
                    {
                        btnPagarFinal.Attributes.Add("readonly", "readonly");
                        btnPagarFinal.Attributes.Add("disabled", "disabled");
                        //guardar el idVenta en el clienteContactoEvento
                        string strIdClienteContactoCompetidorEvento = "";
                        DataTable dtCompetidor = getDatatableCompetidor();
                        int intSizeDtCompedidor = dtCompetidor.Rows.Count;
                        int respuestaModificaContactoEvento = 0;
                        for (int i = 0; i < dtCompetidor.Rows.Count; i++)
                        {
                            // por si las flias solo dejamos tomamos el primero
                            if (i == 0)
                            {
                                strIdClienteContactoCompetidorEvento = dtCompetidor.Rows[i]["idClienteContactoEvento"].ToString();
                                wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                                wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                                clienteContactoEventoClient.Open();
                                cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(strIdClienteContactoCompetidorEvento), getDsSession());
                                cCompetidor.idVenta = intRespIdVenta;
                                try
		                        {
			                        respuestaModificaContactoEvento = clienteContactoEventoClient.ModificarcClienteContactoEvento(cCompetidor, getDsSession());
		                        }
                                catch (Exception ex) {
                                    clienteContactoEventoClient.Close();
                                }
                            }
                        }
                        ltlResultCompropago.Text = responseOrder;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "myScript", "showPopUPCompropago();", true);
                    }
                    else
                    {
                        lblRespuestaPago.Text = "Error intento de pago por medio de deposito.";
                    }
                    return;
                }
                // si la forma de pago es distinta de 3 que quiere decir distinta de deposito
                if (hdEsPagoTarjeta.Value != "3" && dcTotal >0)
                {
                    if (cRespuestaPagoCompetidor.strGlobalCollectActivo != "1")
                        fnGuardaDatosTarjeta(intIdCliente, dcTotal, idMoneda);
                    if (hdEsPagoTarjeta.Value == "1")
                        fnPagar(intIdCliente, dcTotal, idMoneda, intRespIdVenta);
                }
                //fnPagar(intIdCliente, dcTotal, idMoneda, intRespIdVenta);                

                cRespuestaPagoCompetidor = getObjetoRespuestaPago();
                cCompetidorPagar.DtCompetidor = getDatatableCompetidor();
                int resultActualizar = 0;
                #region noEsGlobal
                if (cRespuestaPagoCompetidor.strGlobalCollectActivo != "1" && cRespuestaPagoCompetidor.strGlobalCollectActivo != "2")
                {
                    if (hddPagoGateWay.Value != "4")// si es algo distindo de ingenico entonces si guarda los datos, como antes
                    {
                        respGuardaPago = fnGuardarPago(intIdCliente, intRespIdVenta, dcTotal, idMoneda);
                        if (respGuardaPago)
                        {
                            try
                            {
                                // aqui voy a meter un try catch si cae en catch paso no termino de actualizar de un pago que si pasó
                                resultActualizar = actualizar(intIdCliente, dcTotal, idMoneda, intRespIdVenta);
                                int intIdClienteContactoEvento = 0;
                                int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                                // si el resultado es mayor a cero quiere decir que actualizo
                                if (resultActualizar > 0)
                                {
                                    respActualizaCompetidor = ActualizarCompetidor(intIdCliente, dcTotal, idMoneda, intRespIdVenta);
                                    DataTable dtCompetidor = getDatatableCompetidor();
                                    if (respActualizaCompetidor > 0)
                                    {
                                        // si todo pasó bien ahora enviamos correo al competidor
                                        // El idVenta que se envía es el primero ya que es el que 
                                        // es el de la inscripción
                                        //protected bool correoPago(string strIdVenta, string strFolios, string strIdioma, string strCorreoCliente, DataTable dtCompedidor)
                                        string strIdClienteContactoCompetidorEvento = dtCompetidor.Rows[0]["idClienteContactoEvento"].ToString();
                                        wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                                        wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                                        //clienteContactoEventoClient.Open();
                                        cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(strIdClienteContactoCompetidorEvento), getDsSession());


                                        bool respCorreoConfirmacion = correoPago(hdIV.Value, "", "1", hdCorreoCompetidor.Value, cCompetidorPagar.DtCompetidor, cCompetidor);
                                        //cParticipante.dtCompedidor = null;
                                        clienteContactoEventoClient.Close();
                                        Session["dtCompetidorPago" + cnIdContactoEvento.Value] = null;
                                        cCompetidorPagar.DtCompetidor = null;
                                        string idVentasDetalles = hdIV.Value.Remove(hdIV.Value.Length - 1);
                                        //Response.Redirect("respPago.aspx?token=" + strIdVentas[0].ToString());
                                        try
                                        {
                                            Response.Redirect("respPago.aspx?token=" + idVentasDetalles);
                                        }
                                        catch (ThreadAbortException ex1)
                                        {
                                            // do nothing
                                        }
                                    }
                                    else
                                    {
                                        lblRespuestaPago.Text = "Pago realizado con cargo a su tarjeta, pero ocurrió error al actualizar al competidor. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                                    }
                                }
                                else
                                {
                                    lblRespuestaPago.Text = "Pago realizado, pero ocurrió error en conexión, favor de contactarnos. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                                }
                            }// fin del try
                            catch (Exception ex)
                            {
                                lblRespuestaPago.Text = "Pago realizado con cargo a su tarjeta, pero ocurrió error al actualizar al competidor. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                                btnPagarFinal.Attributes.Add("readonly", "readonly");
                                btnPagarFinal.Attributes.Add("disabled", "disabled");

                                string msg = ex.Message.ToString();
                                string strginnermessage = "";
                                string strCuerpoCorreo = msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                                string strExcepcion = "";
                                if (ex.ToString().Length > 799)
                                    strExcepcion = ex.ToString().Substring(0, 999);
                                else
                                    strExcepcion = ex.ToString();
                                strCuerpoCorreo += strExcepcion;
                                MensajeAlert(strCuerpoCorreo);
                            }
                        }
                    }
                    else
                    {
                        if (cRespuestaPagoCompetidor.strIdEstatusPago == "2")
                        {

                            DataTable dtCompetidor = getDatatableCompetidor();
                            respActualizaCompetidor = ActualizarCompetidor(intIdCliente, dcTotal, idMoneda, intRespIdVenta);
                            if (respActualizaCompetidor > 0)
                            {
                                // si todo pasó bien ahora enviamos correo al competidor
                                // El idVenta que se envía es el primero ya que es el que 
                                // es el de la inscripción
                                //protected bool correoPago(string strIdVenta, string strFolios, string strIdioma, string strCorreoCliente, DataTable dtCompedidor)
                                string strIdClienteContactoCompetidorEvento = dtCompetidor.Rows[0]["idClienteContactoEvento"].ToString();
                                wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                                wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                                //clienteContactoEventoClient.Open();
                                cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(strIdClienteContactoCompetidorEvento), getDsSession());


                                bool respCorreoConfirmacion = correoPago(hdIV.Value, "", "1", hdCorreoCompetidor.Value, cCompetidorPagar.DtCompetidor, cCompetidor);
                                //cParticipante.dtCompedidor = null;
                                clienteContactoEventoClient.Close();
                                Session["dtCompetidorPago" + cnIdContactoEvento.Value] = null;
                                cCompetidorPagar.DtCompetidor = null;
                                string idVentasDetalles = hdIV.Value.Remove(hdIV.Value.Length - 1);
                                //Response.Redirect("respPago.aspx?token=" + strIdVentas[0].ToString());
                                try
                                {
                                    Response.Redirect("respPago.aspx?token=" + idVentasDetalles);
                                }
                                catch (ThreadAbortException ex1)
                                {
                                    // do nothing
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                            else
                            {
                                lblRespuestaPago.Text = "Pago realizado con cargo a su tarjeta, pero ocurrió error al actualizar al competidor. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                            }
                        }
                    }
                }
                if (cRespuestaPagoCompetidor.strGlobalCollectActivo != "1")
                {                    

                    //fnGuardaDatosTarjeta(intIdCliente, dcTotal, idMoneda);
                }
                #endregion 
            }
            catch (Exception ex)
            {
                lblRespuestaPago.Text = ex.Message.ToString();
            }
        }

        protected int fnGuardarVenta(int intIdCliente, decimal dcTotal, string idMoneda)
        {
            wsTransactions.IkVentaClient vc = new wsTransactions.IkVentaClient();
            vc.Open();
            DataTable dtCompeditor = getDatatableCompetidor();
            DataTable dtVenta = getDatatableCompetidorPago();
            string strCanalVentaId = (ConfigurationManager.AppSettings["idCanalVenta"].ToString());

            wscMoneda.IcMonedaClient IcMoneda = new wscMoneda.IcMonedaClient();
            IcMoneda.Open();
            wscMoneda.ListacMonedas lstMonedas = IcMoneda.SeleccionarcMonedas(getDsSession());            
            IcMoneda.Close();
            decimal dcTipoCambio = 0;
            int intTipoMoneda = 0;
            string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
            string dsIsoMoneda = selectedValueMoneda[1].ToString();
            string idMonedaSelected = selectedValueMoneda[0].ToString();
            for (int cntMoneda = 0; cntMoneda < lstMonedas.Count; cntMoneda++)
            {
                if (int.Parse(idMonedaSelected) == (int)lstMonedas[cntMoneda].idMoneda)
                {
                    dcTipoCambio = (decimal)lstMonedas[cntMoneda].noConversion;
                    intTipoMoneda = (int)lstMonedas[cntMoneda].idTipoMoneda;
                    cntMoneda = lstMonedas.Count;
                }
            }

            int intOrdenadorVenta = 1;
            //DataRow[] dtListDatos = dtVenta.Select("ordenador = " + intOrdenadorVenta);

            int maxOrdenador = Convert.ToInt32(dtVenta.Compute("max(ordenador)", string.Empty));
            int intIdVenta = 0;
            decimal dcTotalVentaOrdenador = 0;
            // Aqui tenemos el maximo ordenador que hay en nuestra tabla
            DataRow[] dtListDatos = null;
            DataTable dtVentaOrdenador = null;
            for (int k = 1; k <= maxOrdenador; k++)
            {
                dtListDatos = dtVenta.Select("ordenador = " + k +" and cantidad > 0");
                if (dtListDatos.Length > 0)
                {
                    dtVentaOrdenador = dtListDatos.CopyToDataTable();                

                    wsTransactions.kVenta laventan = new wsTransactions.kVenta();

                    // una funcion que regrese el total de la venta del folio que se está generando

                    dcTotalVentaOrdenador = getTotalVenta(dtVentaOrdenador);

                    for (int i = 0; i < dtCompeditor.Rows.Count; i++)
                    {

                        //laventan.idClienteDetalle = int.Parse(dtCompeditor.Rows[0]["idClienteDetalle"].ToString());
                        laventan.idClienteDetalle = null;
                        laventan.idClienteClasificador = 5; // Directos
                        laventan.idVenta = 0;
                        laventan.idCliente = Int32.Parse(dtCompeditor.Rows[0]["idCliente"].ToString());
                        laventan.feVenta = Int32.Parse(DateTime.Now.ToString("yyyyMMdd")); // DateTime.Now;
                        laventan.hrVenta = DateTime.Now;
                        laventan.mnMontoTotal = dcTotalVentaOrdenador; //dcTotal;
                        //laventan.mnDescuento = 0; //dcDescuento;
                        //laventan.cnPaquete = (hdEsPaquete.Value == "1");
                        laventan.cnPaquete = false;
                        //laventan.dsClaveVenta
                        //laventan.dsNotas = strNotas;
                        laventan.idEstatusVenta = 24;  // En Proceso
                        laventan.cnRequiereFactura = false;
                        laventan.idCanalVenta = int.Parse(strCanalVentaId);
                        laventan.cnEvento = false;
                        laventan.feCaducidad = Int32.Parse(DateTime.Now.AddYears(1).ToString("yyyyMMdd"));
                        laventan.feAlta = DateTime.Now;
                        laventan.idClienteUsuarioAlta = Int32.Parse(cGlobals.idUsuario.ToString());
                        laventan.cnPromocion = false;  //false;

                        laventan.dsDispositivo = "PC / LAPTOP";
                        laventan.dsSistemaOperativo = "Windows NT APLICATION";

                        if (dtCompeditor.Rows[0]["idPais"].ToString().Trim().Length > 0)
                            laventan.idPaisCompra = Int32.Parse(dtCompeditor.Rows[0]["idPais"].ToString().Trim()); //484;  // MEX cPais
                        if (dtCompeditor.Rows[0]["idEstado"].ToString().Trim().Length > 0)
                            laventan.idEstadoCompra = Int32.Parse(dtCompeditor.Rows[0]["idEstado"].ToString().Trim());
                        laventan.idClienteContactoComprador = Int32.Parse(dtCompeditor.Rows[0]["idClienteContacto"].ToString());

                        // ahora los detalles
                        bool blBanderaGuardo = true;
                        wsTransactions.kVentaDetalle detalle; //, detallePadre;
                        wsTransactions.ListakVentaDetalles listaParaEnvio = new wsTransactions.ListakVentaDetalles();

                        //DataTable dtProductos = cParticipante.dtDataCobro;
                        decimal dcContadorMnDescuentoTotal = 0;
                        decimal dcMnDescuento = 0;
                        for (int l = 0; l < dtVentaOrdenador.Rows.Count; l++)
                        {
                            detalle = new wsTransactions.kVentaDetalle();
                            string mnPrecio_ = "mnPrecio_";
                            string mnTipoCambio_ = "mnTipoCambio_";
                            dcMnDescuento = 0;
                            decimal dcPrDescuento = 0;
                            if (int.Parse(dtVentaOrdenador.Rows[l]["cantidad"].ToString()) > 0)
                            {
                                mnPrecio_ += dtVentaOrdenador.Rows[l]["idMonedaPagar"].ToString();
                                mnTipoCambio_ += dtVentaOrdenador.Rows[l]["idMonedaPagar"].ToString();
                                detalle.feVisita = int.Parse(dtVentaOrdenador.Rows[l]["feVisita"].ToString());
                                detalle.feAlta = DateTime.Now;
                                //detalle.feVisitaLimite = Int32.Parse((DateTime.Now.AddYears(1)).AddMonths(6).ToString("yyyyMMdd"));  // fecha Alta + 1 año
                                //detalle.feVisitaLimite = Int32.Parse((DateTime.Parse(dtVentaOrdenador(dtVentaOrdenadorUpgradeSeleccionados.Rows[l]["fechaVisita"].ToString())).AddYears(1)).AddMonths(6).ToString("yyyyMMdd")); // fecha Visita + 1 año y medio. // validado con Lupita y Haumi 16 septiembre 2013
                                detalle.noAgrupadorVenta = 1; //? sacar el max  + 1
                                detalle.noPax = (int)dtVentaOrdenador.Rows[l]["cantidad"];
                                if (int.Parse(dtVentaOrdenador.Rows[l]["idMonedaPagar"].ToString()) > 4)
                                    detalle.mnPrecioLista = (decimal)dtVentaOrdenador.Rows[l]["mnPrecio_MS"];
                                else
                                    detalle.mnPrecioLista = (decimal)dtVentaOrdenador.Rows[l][mnPrecio_];
                                detalle.mnPrecioVenta = decimal.Parse(dtVentaOrdenador.Rows[l]["mnPrecioVenta"].ToString()) / detalle.noPax;

                                // cuando detalle.mnPrecioLista  es cero, es porque son cosas como donativos que no tienen precio
                                if (detalle.mnPrecioLista > 0)
                                {
                                    dcMnDescuento = (decimal)(detalle.mnPrecioLista - detalle.mnPrecioVenta);
                                    dcPrDescuento = (decimal)((dcMnDescuento * 100) / detalle.mnPrecioLista);
                                }
                                else
                                {
                                    dcMnDescuento = 0;
                                    dcPrDescuento = 0;
                                }
                                dcContadorMnDescuentoTotal += dcMnDescuento;
                                detalle.mnDescuento = dcMnDescuento; //detalle.mnPrecioLista - detalle.mnPrecioVenta; // se calcula de la resta de precioLista - precioVenta listo
                                detalle.prDescuento = dcPrDescuento; //((1 - (detalle.mnPrecioVenta / detalle.mnPrecioLista)) * 100); // se calcula listo

                                detalle.idProductoPrecio = (int)dtVentaOrdenador.Rows[l]["idProductoPrecio"];
                                detalle.cnAccesado = false;
                                detalle.cnControlInterno = false;
                                detalle.cnPaquete = (bool)dtVentaOrdenador.Rows[l]["cnEsPaquete"];
                                
                                detalle.idTipoCliente = (int)dtVentaOrdenador.Rows[l]["idTipoCliente"];
                                detalle.cnPromocion = false;

                                detalle.dsNombreVisitante = dtCompeditor.Rows[0]["dsContacto"].ToString(); // llenar con campo de texto por cada registro - listo
                                detalle.dsApellidoPaternoVisitante = dtCompeditor.Rows[0]["dsApellidoPaterno"].ToString();
                                detalle.dsApellidoMaternoVisitante = dtCompeditor.Rows[0]["dsApellidoMaterno"].ToString();

                                detalle.idEstatusVenta = 24; // En proceso listo pru y pro

                                int intLocacion = (int)dtVentaOrdenador.Rows[l]["idLocacion"];

                                if (intLocacion != 0)
                                    detalle.idLocacion = intLocacion;


                                detalle.mnComision = 0;
                                detalle.prComision = 0;
                                //detalle.mnComision = ((decimal)dtVentaOrdenadorUpgradeSeleccionados.Rows[l]["precioVenta"] * (decimal)dtVentaOrdenadorUpgradeSeleccionados.Rows[l]["prComision"]); // ? ccomisioninterna % multiplicar sale por product y dsAgrupador = comisioncc 
                                //detalle.prComision = (decimal)dtVentaOrdenadorUpgradeSeleccionados.Rows[l]["prComision"];

                                // En idMoneda del detalle se debe guardar el idTipoMoneda (Gabriel)
                                //detalle.idTipoMoneda = (int)dtVentaOrdenador.Rows[l]["idTipoMoneda"];
                                detalle.idTipoMoneda = intTipoMoneda;

                                detalle.mnTipoCambio = dcTipoCambio; // ? sacar del día listo

                                // Gabriel Granados pide que el campo mnTipoCambio se guarde el tipo de cambio en pesos del dia. correo(09-07-2013): Ventas en Dolares y PEsos
                                if (detalle.idTipoMoneda == 1)
                                {
                                    ListItem cmbMonedass = ddlCambioMoneda.Items.FindByText("MXPESO");
                                    //IcMoneda.SeleccionarcMonedas(cGlobals.dsSession);
                                    //wscMoneda.ListacMonedas lstMonedas = (wscMoneda.ListacMonedas)ViewState["lstMonedasAltaCotizacion"];
                                    string[] selectedValueMonedaMX = ddlCambioMoneda.SelectedValue.Split('-');
                                    string dsIsoMonedaMX = selectedValueMoneda[1].ToString();
                                    string idMonedaMX = selectedValueMoneda[0].ToString();
                                    for (int cntMoneda = 0; cntMoneda < lstMonedas.Count; cntMoneda++)
                                    {
                                        if (int.Parse(idMonedaMX) == (int)lstMonedas[cntMoneda].idMoneda)
                                        {
                                            detalle.mnTipoCambio = (decimal)lstMonedas[cntMoneda].noConversion;
                                            cntMoneda = lstMonedas.Count;
                                        }
                                    }
                                }

                                //detalle.noConversionTipoMoneda = (decimal)dtVentaOrdenador.Rows[l]["mnTipoCambio_"];//0;

                                detalle.idSegmento = 4;
                                detalle.idClienteUsuarioAlta = Int32.Parse(cGlobals.idUsuario.ToString());
                                //detalle.idVentaDetalle = int.Parse(dtVentaOrdenador.Rows[l]["consecutivo"].ToString());
                                detalle.idVentaDetalle = 0;
                                detalle.idCanalVenta = int.Parse(strCanalVentaId);
                                detalle.mnIva = null;
                                listaParaEnvio.Add(detalle);
                            }
                        }
                        try
                        {
                            //Primer Intento de pago
                            laventan.mnDescuento = dcContadorMnDescuentoTotal;
                            intIdVenta = vc.InsertarkVenta(laventan, listaParaEnvio, null, null, getDsSession());
                            hdIV.Value += intIdVenta.ToString() + ",";
                            i = dtCompeditor.Rows.Count;
                        }
                        catch (Exception ex)
                        {
                            //segundo intento de pago
                            intIdVenta = vc.InsertarkVenta(laventan, listaParaEnvio, null, null, getDsSession());
                            hdIV.Value += intIdVenta.ToString() + ",";
                            i = dtCompeditor.Rows.Count;
                            string msg = ex.Message.ToString();
                            string strginnermessage = "";
                            string strCuerpoCorreo = "El Primer intento de pago falló " +msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                            string strExcepcion = "";
                            if (ex.ToString().Length > 799)
                                strExcepcion = ex.ToString().Substring(0, 999);
                            else
                                strExcepcion = ex.ToString();
                            strCuerpoCorreo += strExcepcion;
                            MensajeAlert(strCuerpoCorreo);
                        }
                    }
                }// fin del if al arreglo mayor a cero
            }
            //hdIV.Value = hdIV.Value.Remove(hdIV.Value.Length - 1);                        
            return intIdVenta;
        }

        protected void fnGuardaDatosTarjeta(int intIdCliente, decimal dcTotal, string idMoneda, string strNumeroTarjeta="")
        {
            string strIdClienteTarjeta;
            string idTipoTarjeta = rblTipoTarjeta.SelectedValue.ToString();
            if (rblTipoTarjeta.SelectedValue.ToString() == "2")
                idTipoTarjeta = "1";
            else if (rblTipoTarjeta.SelectedValue.ToString() == "1")
                idTipoTarjeta = "2";
            // Se guarda datos en cliente tarjeta
            wscClienteTarjeta.IcClienteTarjetaClient IcClienteTarjeta = new wscClienteTarjeta.IcClienteTarjetaClient();
            IcClienteTarjeta.Open();
            wscClienteTarjeta.cClienteTarjeta cClienteTarj = new wscClienteTarjeta.cClienteTarjeta();
            cClienteTarj.idCliente = cGlobals.idCliente;
            cClienteTarj.idEstatusCliente = 1; // Activo
            cClienteTarj.dsTarjeta = (strNumeroTarjeta == "") ? txtNoTarjeta.Text.Trim() : strNumeroTarjeta;
            cClienteTarj.idBanco = Int32.Parse(ddlBanco.SelectedItem.Value);
            cClienteTarj.idTipoTarjeta = int.Parse(idTipoTarjeta);
            cClienteTarj.dsNombre = txtNombreTH.Text.Trim().ToUpper(); //+ " " + txtApellidoTH.Text.Trim().ToUpper();
            cClienteTarj.feAlta = DateTime.Now;
            cClienteTarj.idClienteUsuarioAlta = (cGlobals.idUsuario);
            cClienteTarj.idClienteTarjeta = 0;
            cClienteTarj.cnActivo = true;

            strIdClienteTarjeta = IcClienteTarjeta.InsertarcClienteTarjeta(cClienteTarj, getDsSession()).ToString();
            cRespuestaPagoCompetidor = getObjetoRespuestaPago();
            cRespuestaPagoCompetidor.strIdClienteTarjeta = strIdClienteTarjeta;
            Session["cRespuestaPagoCobro" + cnIdContactoEvento.Value] = cRespuestaPagoCompetidor;
            IcClienteTarjeta.Close();
        }

        protected void fnPagar(int intIdCliente, decimal dcTotal, string idMoneda, int idVenta)
        {
            int intFechaHoy = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
            string strDcTotalDolar = "0";
            decimal dcTotalPesos = 0;
            int intIdTipoMonedaPagar = 0;
            string strIdTipoMoneda = "";
            string strFolios = "";
            string strIdVenta = "";
            string[] strIdVentas = hdIV.Value.Split(',');
            string strIdVentasEnviar = hdIV.Value.TrimEnd(',');

            string strCanalVentaId = (ConfigurationManager.AppSettings["idCanalVenta"].ToString());
            string strIdEstatusPago = "";
            wsTransactions.IkVentaClient vc = new wsTransactions.IkVentaClient();
            vc.Open();
            dcTotal = 0;
            for (int l = 0; l < (strIdVentas.Length - 1); l++)
            {
                wsTransactions.kVenta laVenta = vc.SeleccionarkVentaPorIdVenta(int.Parse(strIdVentas[l].ToString()), getDsSession());
                strFolios += laVenta.dsClaveVenta + ",";
                strIdVenta += laVenta.idVenta.ToString() + "|";
                dcTotal += (decimal)laVenta.mnMontoTotal;
            }

            strFolios = strFolios.Remove(strFolios.Length - 1);
            strIdVenta = strIdVenta.Remove(strIdVenta.Length - 1);
           
            vc.Close();
            wscMoneda.IcMonedaClient IcMoneda = new wscMoneda.IcMonedaClient();
            IcMoneda.Open();

            wscMoneda.ListacMonedas lstMonedas = IcMoneda.SeleccionarcMonedas(getDsSession());
            IcMoneda.Close();

            decimal dcTipoCambioPesosDolar = 0;
            decimal dcTipoCambioMonedaPagar = 0;
            
            for (int cntMoneda = 0; cntMoneda < lstMonedas.Count; cntMoneda++)
            {
                if ((int)lstMonedas[cntMoneda].idMoneda == Int32.Parse(idMoneda))
                {
                    strIdTipoMoneda = lstMonedas[cntMoneda].idTipoMoneda.ToString();
                    dcTipoCambioMonedaPagar = (decimal)lstMonedas[cntMoneda].noConversion;
                }              
            }

            // tengo que averiguar el valor de dolar actual
            DataSet listValorPesosDolar = null;
            wsBusinessRules.BusinessRulesServicesClient ruleOper2 = new wsBusinessRules.BusinessRulesServicesClient();
            ruleOper2.Open();
            Dictionary<string, string> dicret2 = new Dictionary<string, string>();
            dicret2.Clear();
            dicret2.Add("@TIPOCONSULTA", "TIPOCAMBIO");
            listValorPesosDolar = ruleOper2.ExecuteRule("spGETcMoneda", dicret2, getDsSession());
            dicret2.Clear();
            ruleOper2.Close();
            dcTipoCambioPesosDolar = decimal.Parse(listValorPesosDolar.Tables[0].Rows[0]["noConversion"].ToString().Trim());
            // si el id tipo de moneda es distinto de 2 Pesos, tenemos que pasar el total a pesos, por los casos de amex
            dcTotalPesos = dcTotal;
            if (strIdTipoMoneda != "2")
            {
                dcTotalPesos = Math.Round(((dcTotal * dcTipoCambioPesosDolar) / dcTipoCambioMonedaPagar), 2, MidpointRounding.ToEven);
            }
            //cRespuestaPagoCompetidor
            wsTransactions.IkCajeroClient IkCajero = new wsTransactions.IkCajeroClient();
            IkCajero.Open();
            //cResultadoGuardadoPago.strGlobalCollectActivo = "0";
            cRespuestaPagoCompetidor.strGlobalCollectActivo = "0";
            cRespuestaPagoCompetidor.strGlobalCollectActivo = "0";
            bool cnCayoError = false;
            wsSecurity.IsSecurityClient security = new wsSecurity.IsSecurityClient();
            Response.BufferOutput = true;            
            string strCadenaEncriptada = "";
            string strDsRespuesta = "";
            switch (hddPagoGateWay.Value)
            {
                case "1":
                    #region BANCOS
                    switch (ddlBanco.SelectedItem.Value)
                    {
                        case "35":
                            // Amex
                            // SIEMPRE EN PESOS

                            strDcTotalDolar = dcTotalPesos.ToString("N2");
                            strDcTotalDolar = strDcTotalDolar.Replace(",", "");
                            //strDcTotalDolar = strDcTotalDolar.Replace(".", "");
                            wsTransactions.Amex amex = new wsTransactions.Amex();
                            wsTransactions.AmexResponse amexresponse = new wsTransactions.AmexResponse();

                            amex.anioExpiracion = ddlAnioExpira.SelectedItem.Value.Substring(ddlAnioExpira.SelectedItem.Value.Length - 2);
                            amex.mesExpiracion = ddlMesExpira.SelectedItem.Value;

                            amex.codigoSeguridad = txtCVV.Text;
                            /* * * * * * * * * * * * * * * * * * MONTO A PAGAR - total * * * * * * * * * * * * */
                            //amex.importe = "3000.00";
                            amex.importe = strDcTotalDolar;
                            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

                            amex.ip = "192.168.24.19";
                            amex.moneda = "MXN";
                            amex.numeroTarjeta = txtNoTarjeta.Text;
                            amex.ordenId = strFolios; // Folio Venta
                            amex.referencia = strFolios; // Folio Venta
                            amex.referenciaOrden = strFolios; // Folio Venta
                            amex.transaccionId = strFolios; // Folio Venta
                            //si no es a MSI estos dos campos no se llenas ó se pasan en nulo.
                            //if (Int32.Parse(cmbPromosionMSI.SelectedItem.Value) > 1)
                            //{
                            // amex.planPagos = "AMEX_PLANN";
                            // amex.numeroPagos = 1;
                            //}

                            try
                            {
                                //checaCreaSessionBD();
                                //amexresponse = IkCajero.PagoAMEX(amex, getDsSession());
                                wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                                icConfiguracion.Open();
                                //wscConfiguracionAplicacion.cConfiguracionAplicacion strConfiguracionTServicio = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-TSERVICIOBANCOMER_" + strCanalVentaId, getDsSession());
                                wscConfiguracionAplicacion.cConfiguracionAplicacion strConfiguracionMidAmex = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-MIDAMEX_" + strCanalVentaId, getDsSession());
                                icConfiguracion.Close();

                                string AMEXmerchantID = strConfiguracionMidAmex.dsValor.ToString();
                                //amexresponse = IkCajero.PagoAMEXM(amex, getDsSession(), "9351670303");
                                amexresponse = IkCajero.PagoAMEXM(amex, getDsSession(), AMEXmerchantID);

                            }
                            catch (Exception err)
                            {
                                strIdEstatusPago = "1" /*Declinado*/;
                                string strParametros = "<b>Folios : " + strFolios + "PagoAMEX - Amex 1 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                                MensajeAlert(strParametros);
                                //mensaje(strParametros, err);
                                lblRespuestaPago.Text = err.Message.ToString();
                                cRespuestaPagoCompetidor.strIdEstatusPago = strIdEstatusPago;
                                cnCayoError = true;
                                // Aqui voy a meter de guardar los datos de la transaccion que no fue realizada                            
                                int idClienteContactoCompetidorEvento = 0;
                                DataTable dtCompetidor = getDatatableCompetidor();
                                int intSizeDtCompedidor = dtCompetidor.Rows.Count;

                                for (int i = 0; i < dtCompetidor.Rows.Count; i++)
                                {
                                    // obtenemos el numero de competidor que le corresponde
                                    if (i == 0)
                                    {
                                        idClienteContactoCompetidorEvento = int.Parse(dtCompetidor.Rows[i]["idClienteContactoEvento"].ToString());
                                    }
                                }
                                //utils.saveVentasFallidas(getDsSession(), (txtNoTarjeta.Text.Trim()), (txtCVV.Text.Trim()), (ddlAnioExpira.SelectedItem.Value), (ddlMesExpira.SelectedItem.Value), txtNombreTH.Text, idClienteContactoCompetidorEvento, idVentaPrincipal);
                            }

                            if (!cnCayoError)
                                lblRespuestaPago.Text = ((amexresponse.gatewayCode != null) ? amexresponse.gatewayCode : "") + " " + ((amexresponse.authorizationCode != null) ? amexresponse.authorizationCode : "") + " " + ((amexresponse.errorCode != null) ? amexresponse.errorCode : "") + " " + ((amexresponse.errorMessage != null) ? amexresponse.errorMessage : "");

                            strIdEstatusPago = "1" /*Declinado*/;

                            if (amexresponse.gatewayCode.ToString().Trim().Length > 0)
                            {
                                if (amexresponse.gatewayCode.ToString().Trim() == "APPROVED")
                                {
                                    strIdEstatusPago = "2" /*Aceptado*/;
                                }
                            }
                            cRespuestaPagoCompetidor.strIdEstatusPago = strIdEstatusPago;
                            cRespuestaPagoCompetidor.strAfiliacion = (amexresponse.dsAfiliacion != null) ? amexresponse.dsAfiliacion : "";
                            cRespuestaPagoCompetidor.strDsRespuesta = (amexresponse.gatewayCode != null) ? amexresponse.gatewayCode.ToString() : "";
                            cRespuestaPagoCompetidor.strDsJustificacion = (amexresponse.gatewayCode != null) ? amexresponse.gatewayCode.ToString() : "";
                            cRespuestaPagoCompetidor.strDsReferencia = (amexresponse.gatewayCode != null) ? amexresponse.gatewayCode.ToString() : "";
                            cRespuestaPagoCompetidor.strDsTransaccion = (amexresponse.authorizationCode != null) ? amexresponse.authorizationCode.ToString() : "";
                            cRespuestaPagoCompetidor.strDsCorrelacion = ((amexresponse.recipient != null) ? amexresponse.recipient.ToString().Trim() : "") + "-" + ((amexresponse.terminal != null) ? amexresponse.terminal.ToString().Trim() : "");
                            cRespuestaPagoCompetidor.strIdBancoReceptor = "35";
                            break;
                        case "5":
                            // Bancomer
                            //IkCajeroClient cajero = new IkCajeroClient();                    
                            string strDsJustificacion = "";
                            string strDsTransaccion = "";
                            string strDsReferencia = "";
                            string strDsCorrelacion = "";
                            string srtdsAfiliacion = "";
                            try
                            {

                                //wskTransactiones.IkCajeroClient cajero = new wskTransactiones.IkCajeroClient();
                                // strDcTotal agregadó el 03/08/2015 segun hay que enviar siempre los 2 decimales sin el punto, aunque sea .00
                                string strDcTotal = dcTotal.ToString("N2");
                                strDcTotal = strDcTotal.Replace(",", "");
                                //strDcTotal = strDcTotal.Replace(".", "");
                                wsTransactions.Adquira_EMV adquiraemv = new wsTransactions.Adquira_EMV();
                                wsTransactions.Adquira_EMVResponse resp = new wsTransactions.Adquira_EMVResponse();

                                wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                                icConfiguracion.Open();
                                //wscConfiguracionAplicacion.cConfiguracionAplicacion strConfiguracionTServicio = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-TSERVICIOBANCOMER_" + strCanalVentaId, getDsSession());
                                wscConfiguracionAplicacion.cConfiguracionAplicacion strConfiguracionBancomerVAL1 = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-BancomerVAL1", getDsSession());
                                icConfiguracion.Close();

                                adquiraemv.s_transm = DateTime.Now.ToString("yyyyMMddHHmmss"); // aaaammddhhmmss
                                adquiraemv.c_referencia = strFolios; // Folio Venta
                                //adquiraemv.val_1 = 12; //unidad de negocio - 12= computadora
                                //adquiraemv.val_1 = 19;  
                                //adquiraemv.val_1 = 0; // solicitado por felipe el 30082016
                                adquiraemv.val_1 = 12;
                                if (strConfiguracionBancomerVAL1 != null && strConfiguracionBancomerVAL1.dsValor != null)
                                    adquiraemv.val_1 = int.Parse(strConfiguracionBancomerVAL1.dsValor.Trim());
                                adquiraemv.clave_entidad = 10778;
                                //adquiraemv.t_servicio = "1121"; //cc
                                //adquiraemv.t_servicio = "6"; 
                                adquiraemv.t_servicio = "1204"; //solicitado por felipe el 30082016
                                /* * * * * * * * * * * * * * * * * * MONTO A PAGAR - total * * * * * * * * * * * * * * * * */
                                if (strIdTipoMoneda != "2") // diferente de Pesos
                                {
                                    adquiraemv.c_cur = 1; // 0-PESOS, 1-DOLARES 
                                    adquiraemv.t_importe = strDcTotal.ToString();
                                }
                                else //pesos
                                {
                                    adquiraemv.c_cur = 0; //?? 0-PESOS, 1-DOLARES
                                    adquiraemv.t_importe = strDcTotal.ToString();
                                }
                                /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

                                adquiraemv.tarjetahabiente = txtNombreTH.Text.Trim();// +" " + txtApellidoTH.Text.Trim(); // nombre tarjetahabiente
                                adquiraemv.val_3 = txtNoTarjeta.Text.Trim(); //No de tarjeta
                                adquiraemv.val_4 = ddlAnioExpira.SelectedItem.Value.Substring(ddlAnioExpira.SelectedItem.Value.Length - 2) + ddlMesExpira.SelectedItem.Value; // fecha vencimiento yymm
                                adquiraemv.val_5 = txtCVV.Text.Trim(); // codigo seguridad
                                adquiraemv.val_6 = adquiraemv.s_transm + adquiraemv.c_referencia + adquiraemv.t_importe + txtCVV.Text;
                                adquiraemv.val_11 = ""; // correo electronico Cliente
                                adquiraemv.val_12 = ""; // Telefono Cliente
                                //adquiraemv.clave_entidad = int.Parse(clave_entidad.Text) // 1730 ? 
                                adquiraemv.val_16 = rblTipoTarjeta.SelectedValue; //"1"; //? 1:Visa 2:mastercard  
                                adquiraemv.val_17 = "0"; // 0: digitada 
                                adquiraemv.val_18 = ""; // vacio

                                adquiraemv.val_19 = "0"; //si tiene msi 1 sino 0
                                adquiraemv.val_20 = "0"; // no. de meses
                                /* if (Int32.Parse(cmbPromosionMSI.SelectedItem.Value) > 1)
                                 {
                                     adquiraemv.val_19 = "1"; //si tiene msi 1 sino 0
                                     adquiraemv.val_20 = cmbPromosionMSI.SelectedItem.Value; // no. de meses
                                 }*/

                                adquiraemv.email_admin = "faraujo@experienciasxcaret.com.mx";
                                adquiraemv.accion = "PAGO";
                                adquiraemv.nu_afiliacion = "";
                                adquiraemv.nu_plataforma = "7";
                                try
                                {
                                    //resp = cajero.PagoAdquiraEMV(adquiraemv, strDsSession, Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"])); // true:produccion, false:pruebas
                                    resp = IkCajero.PagoAdquiraEMV(adquiraemv, getDsSession(), Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"])); // true:produccion, false:pruebas
                                }
                                catch (Exception err)
                                {
                                    strIdEstatusPago = "1" /*Declinado*/;
                                    string strParametros = "<b>Folios : " + strFolios + "PagoAdquiraEMV - Bancomer 1 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + strDcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                                    MensajeAlert(strParametros);
                                    lblRespuestaPago.Text = err.Message.ToString();
                                    cRespuestaPagoCompetidor.strIdEstatusPago = strIdEstatusPago;
                                    cnCayoError = true;
                                    // Aqui voy a meter de guardar los datos de la transaccion que no fue realizada                            
                                    int idClienteContactoCompetidorEvento = 0;
                                    DataTable dtCompetidor = getDatatableCompetidor();
                                    int intSizeDtCompedidor = dtCompetidor.Rows.Count;

                                    for (int i = 0; i < dtCompetidor.Rows.Count; i++)
                                    {
                                        // obtenemos el numero de competidor que le corresponde
                                        if (i == 0)
                                        {
                                            idClienteContactoCompetidorEvento = int.Parse(dtCompetidor.Rows[i]["idClienteContactoEvento"].ToString());
                                        }
                                    }
                                    //utils.saveVentasFallidas(getDsSession(), (txtNoTarjeta.Text.Trim()), (txtCVV.Text.Trim()), (ddlAnioExpira.SelectedItem.Value), (ddlMesExpira.SelectedItem.Value), txtNombreTH.Text, idClienteContactoCompetidorEvento, idVentaPrincipal);
                                }

                                if (!cnCayoError)
                                    lblRespuestaPago.Text = ((resp.mensaje != null) ? resp.mensaje : "") + " " + ((resp.autorizacion != null) ? resp.autorizacion : "") + " - " + ((resp.status != null) ? resp.status : "");

                                string strIdBancoReceptor = ddlBanco.SelectedItem.Value;

                                strIdEstatusPago = "1" /*Declinado*/;
                                if (resp != null)
                                {
                                    //    MessageBox.Show("Autorizacion : " + resp.autorizacion + " Mensaje :" + resp.mensaje + " Status :" + resp.status + " Imprimir: " + resp.imprimir + " Fecha : " + resp.fecha + " Hora:" + resp.hora);
                                    if (resp.mensaje == "APROBADA")
                                    {
                                        strIdEstatusPago = "2" /*Aceptado*/;
                                        if (resp.autorizacion != null && resp.autorizacion == "000000")
                                        {
                                            strIdEstatusPago = "1" /*Declinado*/;
                                            lblRespuestaPago.Text += " - No se cobro hasta su autorización";
                                        }
                                    }
                                }

                                strDsRespuesta = (resp.mensaje != null) ? resp.mensaje.ToString() : "";
                                strDsJustificacion = (resp.mensaje != null) ? resp.mensaje.ToString() : "";
                                strDsTransaccion = (resp.autorizacion != null) ? resp.autorizacion.ToString() : "";
                                strDsReferencia = (resp.mensaje != null) ? resp.mensaje.ToString() : "";
                                strDsCorrelacion = ((resp.autorizacion != null) ? resp.autorizacion.ToString() : "");
                                srtdsAfiliacion = ((resp.dsAfiliacion != null) ? resp.dsAfiliacion.ToString() : "");

                                cRespuestaPagoCompetidor.strIdEstatusPago = strIdEstatusPago;
                                cRespuestaPagoCompetidor.strAfiliacion = srtdsAfiliacion;
                                cRespuestaPagoCompetidor.strDsRespuesta = strDsRespuesta;
                                cRespuestaPagoCompetidor.strDsJustificacion = strDsJustificacion;
                                cRespuestaPagoCompetidor.strDsReferencia = strDsReferencia;
                                cRespuestaPagoCompetidor.strDsTransaccion = strDsTransaccion;
                                cRespuestaPagoCompetidor.strDsCorrelacion = strDsCorrelacion;
                                cRespuestaPagoCompetidor.strIdBancoReceptor = "5";
                            }
                            catch (Exception err)
                            {
                                lblRespuestaPago.Text = "";
                                strIdEstatusPago = "1";
                                strDsJustificacion = "Revisar";
                                strDsJustificacion = err.Message;
                                string strParametros = "GuardaPagoTarjeta - Bancomer 2 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                                //mensaje(strParametros, err);
                            }
                            break;
                        default:
                            try
                            {
                                string strDcTotal = dcTotal.ToString("N2");
                                strDcTotal = strDcTotal.Replace(",", "");
                                //string strDcTotal = dcTotal.ToString().Replace(".", "");
                                strDcTotal = strDcTotal.Replace(".", "");
                                strDcTotalDolar = strDcTotal.Replace(".", "");
                                wsTransactions.CSVPC2Party C2Party = new wsTransactions.CSVPC2Party();
                                wsTransactions.CSVPC2PartyResponse C2PartyRespo = new wsTransactions.CSVPC2PartyResponse();


                                /* * * * * * * * * * * * * * * * * * MONTO A PAGAR - total * * * * * * * * * * * * * * * * */
                                //adquiraemv.t_importe = "300000"; //txtMontoTemporal.Text; // importe 10000
                                string strCurrency = "MXN";

                                if (strIdTipoMoneda != "2") // diferente de Pesos
                                {
                                    strCurrency = "USD";
                                    C2Party.vpc_Amount = strDcTotalDolar;
                                }
                                else //pesos
                                {
                                    strCurrency = "MXN";
                                    C2Party.vpc_Amount = strDcTotal;
                                }
                                /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
                                ///* * * * * * * * * * * * * * * * * * MONTO A PAGAR - total * * * * * * * * * * * * * * * * */
                                //C2Party.vpc_Amount = strDcTotal;
                                ///* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

                                C2Party.vpc_CardExp = ddlAnioExpira.SelectedItem.Value.Substring(ddlAnioExpira.SelectedItem.Value.Length - 2) + ddlMesExpira.SelectedItem.Value;
                                C2Party.vpc_CardNum = txtNoTarjeta.Text;
                                C2Party.vpc_MerchTxnRef = strFolios; // Folio Venta
                                C2Party.vpc_OrderInfo = C2Party.vpc_MerchTxnRef; //txtNoReferencia.Text; // cualquier texto
                                C2Party.vpc_Version = "1";
                                C2Party.vpc_CardSecurityCode = txtCVV.Text;
                                C2Party.vpc_TicketNo = strFolios; // Folio Venta
                                C2Party.vpc_Type = "bmx";
                                //C2Party.vpc_Type = cmbTipoTarjeta.SelectedItem.Text;
                                C2Party.vpc_NumPayments = "1";

                                try
                                {
                                    //C2PartyRespo = IkCajero.PagoCSVPC2Party(C2Party, strDsSession);
                                    C2PartyRespo = IkCajero.PagoBanamexM(C2Party, getDsSession(), "4010641", strCurrency);//merchan de produccion
                                    //C2PartyRespo = IkCajero.PagoBanamexM(C2Party, getDsSession(), "4045802", strCurrency); //merchan de pruebas
                                }
                                catch (Exception err)
                                {
                                    strIdEstatusPago = "1";
                                    string strParametros = "<b>Folios : " + strFolios + "PagoBanamexM - Banamex 1 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                                    MensajeAlert(strParametros);
                                    lblRespuestaPago.Text = err.Message.ToString();
                                    cRespuestaPagoCompetidor.strIdEstatusPago = strIdEstatusPago;
                                    cnCayoError = true;
                                    // Aqui voy a meter de guardar los datos de la transaccion que no fue realizada                            
                                    int idClienteContactoCompetidorEvento = 0;
                                    DataTable dtCompetidor = getDatatableCompetidor();
                                    int intSizeDtCompedidor = dtCompetidor.Rows.Count;

                                    for (int i = 0; i < dtCompetidor.Rows.Count; i++)
                                    {
                                        // obtenemos el numero de competidor que le corresponde
                                        if (i == 0)
                                        {
                                            idClienteContactoCompetidorEvento = int.Parse(dtCompetidor.Rows[i]["idClienteContactoEvento"].ToString());
                                        }
                                    }
                                    //utils.saveVentasFallidas(getDsSession(), (txtNoTarjeta.Text.Trim()), (txtCVV.Text.Trim()), (ddlAnioExpira.SelectedItem.Value), (ddlMesExpira.SelectedItem.Value), txtNombreTH.Text, idClienteContactoCompetidorEvento, idVentaPrincipal);
                                }

                                if (!cnCayoError)
                                    lblRespuestaPago.Text = ((C2PartyRespo.vpc_TxnResponseCode != null) ? C2PartyRespo.vpc_TxnResponseCode.ToString() : "") + " " + ((C2PartyRespo.vpc_AuthorizeId != null) ? C2PartyRespo.vpc_AuthorizeId.ToString() : "") + " - " + ((C2PartyRespo.vpc_Message != null) ? C2PartyRespo.vpc_Message.ToString() : "") + " - " + ((C2PartyRespo.vpc_TxnResponseDescription != null) ? C2PartyRespo.vpc_TxnResponseDescription.ToString() : "");

                                string strIdBancoReceptor = "108"; // Banamex
                                string strDsAfiliacionBanamex = (C2PartyRespo.dsAfiliacion != null) ? C2PartyRespo.dsAfiliacion.ToString() : "";

                                strIdEstatusPago = "1";
                                if (C2PartyRespo != null)
                                {
                                    if (C2PartyRespo.vpc_TxnResponseCode != null)
                                        strIdEstatusPago = (C2PartyRespo.vpc_TxnResponseCode.ToString() == "0") ? "2" /*Aceptado*/: "1" /*Declinado*/;

                                    if (strIdEstatusPago == "2" && C2PartyRespo.vpc_AuthorizeId != null && C2PartyRespo.vpc_AuthorizeId == "000000")
                                    {
                                        strIdEstatusPago = "1";/*Declinado*/;
                                        lblRespuestaPago.Text += " - No se cobro hasta su autorización";
                                    }
                                }

                                strDsRespuesta = (C2PartyRespo.vpc_AVSResultDescription != null) ? C2PartyRespo.vpc_AVSResultDescription.ToString() : "";
                                strDsJustificacion = (C2PartyRespo.vpc_AVSResultDescription != null) ? C2PartyRespo.vpc_AVSResultDescription.ToString() : "";
                                strDsTransaccion = (C2PartyRespo.vpc_AuthorizeId != null) ? C2PartyRespo.vpc_AuthorizeId.ToString() : "";
                                strDsReferencia = (C2PartyRespo.vpc_AVSResultDescription != null) ? C2PartyRespo.vpc_AVSResultDescription.ToString() : "";
                                strDsCorrelacion = ((C2PartyRespo.vpc_ReceiptNo != null) ? C2PartyRespo.vpc_ReceiptNo.ToString() : "") + "-" + ((C2PartyRespo.vpc_TransactionNo != null) ? C2PartyRespo.vpc_TransactionNo.ToString() : ""); //C2PartyRespo.vpc_AuthorizeId + "-" + C2PartyRespo.vpc_TransactionNo;

                                cRespuestaPagoCompetidor.strIdEstatusPago = strIdEstatusPago;
                                cRespuestaPagoCompetidor.strAfiliacion = strDsAfiliacionBanamex;
                                cRespuestaPagoCompetidor.strDsRespuesta = strDsRespuesta;
                                cRespuestaPagoCompetidor.strDsJustificacion = strDsJustificacion;
                                cRespuestaPagoCompetidor.strDsReferencia = strDsReferencia;
                                cRespuestaPagoCompetidor.strDsTransaccion = strDsTransaccion;
                                cRespuestaPagoCompetidor.strDsCorrelacion = strDsCorrelacion;
                                cRespuestaPagoCompetidor.strIdBancoReceptor = strIdBancoReceptor;

                            }
                            catch (Exception err)
                            {
                                lblRespuestaPago.Text = "";
                                strIdEstatusPago = "1";
                                strDsJustificacion = "Revisar";
                                strDsJustificacion = err.Message;
                                string strParametros = "GuardaPagoTarjeta - Banamex 2 - intIdCliente=" + intIdCliente.ToString() + " dcTotal=" + dcTotal.ToString() + " idMoneda=" + idMoneda.ToString();
                                //mensaje(strParametros, err);
                            }
                            break;

                    }
                    #endregion Bancos
                    break;

                case "2":
                    #region global
                    if (hdCnGlobalCollect.Value == "1")
                    {
                        if (hdCnGlobalCollectI.Value == "1")
                        {
                            strCanalVentaId = strCanalVentaId.ToString() + "_INT"; //"11_INT";
                        }
                        cRespuestaPagoCompetidor.strGlobalCollectActivo = "1";
                        wscTipoMoneda.IcTipoMonedaClient IcTipoMoneda = new wscTipoMoneda.IcTipoMonedaClient();
                        IcTipoMoneda.Open();
                        wscTipoMoneda.cTipoMoneda cTipoMoneda = IcTipoMoneda.SeleccionarcTipoMonedaPorId(int.Parse(strIdTipoMoneda), getDsSession());
                        IcTipoMoneda.Close();

                        wscPais.IcPaisClient IcPaisClient = new wscPais.IcPaisClient();
                        IcPaisClient.Open();
                        wscPais.cPais cPais = IcPaisClient.SeleccionarcPaisPorId(Int32.Parse(hdIdPais.Value), getDsSession());
                        IcPaisClient.Close();

                        string strDcTotal = dcTotal.ToString("N2").Replace(".", "");// string strDcTotal = Math.Round(dcTotal, 2).ToString("N2");
                        strDcTotal = strDcTotal.Replace(",", "");
                        wsTransactions.GlobalCollect globalCollect = new wsTransactions.GlobalCollect();
                        wsTransactions.GlobalCollectResponse globalCollectResponse = new wsTransactions.GlobalCollectResponse();

                        wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                        icConfiguracion.Open();
                        wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionIPAddressGlobalCollect = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-XCRM-ClientIPAddressGlobalCollect", getDsSession());
                        icConfiguracion.Close();

                        string nombre = "";
                        string Apellidos = "";
                        string[] arrNomBre = txtNombreTH.Text.Trim().Split(' ');
                        nombre = arrNomBre[0].ToString();
                        if (arrNomBre.Count() > 1)
                            for (int l = 1; l < arrNomBre.Count(); l++)
                                Apellidos += arrNomBre[l].ToString() + " ";

                        globalCollect.Apellido = Apellidos;
                        globalCollect.Nombre = nombre;
                        globalCollect.CanalVentaId = strCanalVentaId;
                        globalCollect.ClienteId = intIdCliente.ToString();
                        globalCollect.CodigoIdioma = "es";
                        globalCollect.CodigoMoneda = cTipoMoneda.dsIso;  // "USD";  "MXN"
                        globalCollect.CodigoPais = cPais.dsIso2; // "US";
                        globalCollect.CorreoElectronico = hdCorreoCompetidor.Value; //"faraujo@experienciasxcaret.com.mx"; // del cliente
                        globalCollect.Monto = strDcTotal;
                        globalCollect.NumeroPagos = 1;
                        globalCollect.ReferenciaComercio = strFolios; // Folio Venta
                        globalCollect.IdVenta = strIdVenta; // string con pipes
                        globalCollect.DeviceType = "7"; // 7-Desktop
                        globalCollect.CustomerIPAddress = cConfiguracionIPAddressGlobalCollect.dsValor.Trim(); //Utils.GetIpAddress();
                        globalCollect.DireccionIP = utils.GetIpAddress();

                        string strAbsoluteUri = HttpContext.Current.Request.Url.AbsoluteUri;
                        string strHost = HttpContext.Current.Request.Url.Host;
                        string[] strspliturl = strAbsoluteUri.Split('/');
                        strAbsoluteUri = strAbsoluteUri.Replace(strspliturl[strspliturl.Length - 1], "");
                        strAbsoluteUri += "respuestaGlobal.aspx";

                        /*if (Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]) && strHost != "localhost")
                        {
                            strAbsoluteUri = strAbsoluteUri.Replace("http", "https");
                        }*/
                        string strPrimerosCaracteres = strAbsoluteUri.Substring(0, 5);
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]) && strHost != "localhost")
                        {
                            if (strPrimerosCaracteres == "http:")
                                strAbsoluteUri = strAbsoluteUri.Replace("http", "https");
                            else if (strPrimerosCaracteres != "https")
                                strAbsoluteUri.Trim().Insert(0, "https://");
                        }

                        globalCollect.UrlRetorno = strAbsoluteUri;

                        //lblMensajeError.Text = strAbsoluteUri;

                        // TipoProducto 2=american 1=visa 3=master

                        if (ddlBanco.SelectedItem.Value == "36")// Amex
                        {
                            globalCollect.TipoProducto = "2";
                        }
                        else
                        {
                            //cmbTipoTarjeta.SelectedValue 1:Visa 2:mastercard

                            if (ddlFormaPago.SelectedValue == "27" || ddlFormaPago.SelectedValue == "29") // Visa
                                globalCollect.TipoProducto = "1";
                            if (ddlFormaPago.SelectedValue == "28" || ddlFormaPago.SelectedValue == "30") // mastercard
                                globalCollect.TipoProducto = "3";
                        }

                        try
                        {
                            globalCollectResponse = IkCajero.GlobalCollect_Pago(globalCollect, getDsSession());
                        }
                        catch (Exception err)
                        {
                            hdCnGlobalCollect.Value = "0";
                            hdCnGlobalCollectI.Value = "0";

                            // volvemos a mostrar los campos para ingresar datos de tarjeta
                            lblNumTarjeta.Visible = true;
                            txtNoTarjeta.Visible = true;
                            lblFechaExpira.Visible = true;
                            //txtMes.Visible = true;
                            //txtAnio.Visible = true;
                            ddlAnioExpira.Visible = true;
                            ddlMesExpira.Visible = true;
                            lblCVV.Visible = true;
                            txtCVV.Visible = true;
                            btnPagarFinal.Attributes.Remove("disabled");
                            btnPagarFinal.Enabled = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>console.log(\"" + err.Message.ToString() + "\");</script>", false);
                            MensajeAlert("<b>Folios : " + strFolios + " </b> <br/>Error al intentar IkCajero.GlobalCollect_Pago <br>\"" + err.Message.ToString().Trim());
                            //mensaje("GuardaPagoTarjeta GlobalCollect_Pago<br>cmbPromosionMSI:" + cmbPromosionMSI.SelectedItem.Value + "<br>hdIdPais:" + hdIdPais.Value, err);
                            return;
                        }

                        // si todo bien hasta aqui
                        if (globalCollectResponse.CodigoError == "0")
                        {
                            //btnGuardarPago.Visible = false;
                            //btnGlobalCollect.Visible = true;
                            pnGlobalCollect.Visible = true;
                            upGC.Update();

                            lblRespuestaPagoGlobalCollect.Text = globalCollectResponse.OrderId;
                            hddIdOrdenGlobal.Value = globalCollectResponse.OrderId;
                            // validando que Global regrese URL
                            if (globalCollectResponse.URL.Trim().Length > 0)
                            {
                                ifrGlobalCollect.Attributes["src"] = globalCollectResponse.URL;
                                upGC.Update();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "myScript", "showGlobal();", true);
                                //UpdatePanelGlobalCollect.Update();
                            }
                            else
                            {
                                //lblMensajeError.Text = "Global Collect no regreso URL de cobro";
                                hdCnGlobalCollect.Value = "0";
                                hdCnGlobalCollectI.Value = "0";

                                // volvemos a mostrar los campos para ingresar datos de tarjeta
                                lblNumTarjeta.Visible = true;
                                txtNoTarjeta.Visible = true;
                                lblFechaExpira.Visible = true;
                                //txtMes.Visible = true;
                                //txtAnio.Visible = true;
                                ddlAnioExpira.Visible = true;
                                ddlMesExpira.Visible = true;
                                lblCVV.Visible = true;
                                txtCVV.Visible = true;
                                btnPagarFinal.Attributes.Remove("disabled");
                                btnPagarFinal.Enabled = true;
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>console.log('Global Collect no regreso URL de cobro');</script>", false);
                                //MensajeAlert("<b>Folios : " + strFolios + " </b> <br/>Global Collect no regreso URL de cobro");
                            }
                            //cbGlobalCollect.JSProperties["cp_pagoGuargadoGlobalCollect"] = "0"; // bandera pago Globalcollect
                        }
                        else
                        {
                            hdCnGlobalCollect.Value = "0";
                            hdCnGlobalCollectI.Value = "0";

                            // volvemos a mostrar los campos para ingresar datos de tarjeta
                            lblNumTarjeta.Visible = true;
                            txtNoTarjeta.Visible = true;
                            lblFechaExpira.Visible = true;
                            //txtMes.Visible = true;
                            //txtAnio.Visible = true;
                            ddlAnioExpira.Visible = true;
                            ddlMesExpira.Visible = true;
                            lblCVV.Visible = true;
                            txtCVV.Visible = true;
                            btnPagarFinal.Attributes.Remove("disabled");
                            btnPagarFinal.Enabled = true;
                            //lblRespuestaPagoGlobalCollect.Text = globalCollectResponse.CodigoError + " " + globalCollectResponse.DescripcionError;
                            //UpdatePanelGlobalCollect.Update();
                        }
                    }
                    #endregion
                    break;
                case "3":
                    #region WORLDPAY
                    // se manda el pago a Worldpay

                    btnPagarFinal.Enabled = false;

                    pnGlobalCollect.Visible = true;

                    pnGlobalCollect.Visible = true;
                    //upGC.Update();
                    lblEtiGlobalCollect.Text = "World pay";
                    cRespuestaPagoCompetidor.strGlobalCollectActivo = "2";  // World pay

                    wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracionApp = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                    icConfiguracionApp.Open();
                    wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionURLWorldPay = icConfiguracionApp.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-URLWorldPay", getDsSession());
                    icConfiguracionApp.Close();

                    string strCadena = "";
                    string strUrlComproPago = "";
                    string strDcTotalWorld = Math.Round(dcTotal, 2).ToString("N2");
                    strDcTotalWorld = strDcTotalWorld.Replace(",", "");

                    if (cConfiguracionURLWorldPay != null)
                        strUrlComproPago = cConfiguracionURLWorldPay.dsValor.Trim();

                    //para pruebas
                    //strUrlComproPago = strUrlComproPago.Replace("www", "ww4");

                    strCadena += "id=" + strIdVentasEnviar;
                    strCadena += "&base=" + ConfigurationManager.AppSettings["bdProduccion"];
                    strCadena += "&noAGV=" + "1";

                    if (intIdTipoMonedaPagar != 2) // diferente de Pesos
                    {
                        strCadena += "&amount=" + strDcTotalWorld;
                    }
                    else //pesos
                    {
                        strCadena += "&amount=" + strDcTotalWorld;
                    }

                    strCadena += "&session=" + getDsSession();

                    strCadena += "&tNombre=" + txtNombreTH.Text.Trim(); //tNombre
                    strCadena += "&email=" + hdCorreoCompetidor.Value.Trim(); //a@b.com
                    strCadena += "&ipComprador=1.1.1.11"; //
                    strCadena += "&idPais=" + hdIdPais.Value; //1
                    strCadena += "&idEstado=" + hdIdEstado.Value; //1
                    strCadena += "&idCanalVenta=" + ConfigurationManager.AppSettings["idCanalVenta"].ToString();
                    strCadena += "&idTipoMoneda=" + strIdTipoMoneda; //1
                    strCadena += "&idFormaPago=" + ddlFormaPago.SelectedValue.Trim();
                    strCadena += "&noPagosMSI=0";//0
                    strCadena += "&idBanco=" + ddlBanco.SelectedValue.Trim();
                    strCadena += "&idIdioma=1"; // 1 - español, 2 ingles

                    string strAbsoluteUriWP = HttpContext.Current.Request.Url.AbsoluteUri;
                    string strHostWP = HttpContext.Current.Request.Url.Host;
                    string[] strspliturlWP = strAbsoluteUriWP.Split('/');
                    strAbsoluteUriWP = strAbsoluteUriWP.Replace(strspliturlWP[strspliturlWP.Length - 1], "");
                    strAbsoluteUriWP += "respuestaWorldPay.aspx";
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]) && strHostWP != "localhost")
                    {
                        if (strAbsoluteUriWP.Substring(0, 5) == "http:")
                            strAbsoluteUriWP = strAbsoluteUriWP.Replace("http:", "https:");
                    }

                    strCadena += "&respuestaURL=" + strAbsoluteUriWP; //{loquesea}

                    //Response.Redirect(strUrlComproPago + "?data=" + security.Encrypt(strCadena), false);
                    strCadenaEncriptada = security.Encrypt(strCadena);
                    ifrGlobalCollect.Attributes["src"] = strUrlComproPago + "?data=" + strCadenaEncriptada;
                    lblEtiGlobalCollect.ToolTip = strUrlComproPago + "?data=" + strCadenaEncriptada;
                    upGC.Update();
                    //string cadenaNoEncriptada = security.Decrypt(strCadenaEncriptada);

                    // temporal
                    strIdEstatusPago = "1" /*Declinado*/;
                    cRespuestaPagoCompetidor.strIdEstatusPago = strIdEstatusPago;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "myScript", "showGlobal();", true);
                    #endregion WORLDPAY
                    break;

                case "4":
                    #region Ingenico

                    // temporal
                    strIdEstatusPago = "1" /*Declinado*/;
                    lblRespuestaPago.Text = "";
                    //strDsRespuesta = "error";

                    string strCadenaIngenico = "";
                    wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracionAp = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                    icConfiguracionAp.Open();

                    wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionUrlIngenico = icConfiguracionAp.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-UrlIngenico", getDsSession());
                    wscConfiguracionAplicacion.cConfiguracionAplicacion cConfiguracionCodigosErrorGlobal = icConfiguracionAp.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-CodigosErrorGlobal", getDsSession());

                    icConfiguracionAp.Close();

                    //string strUrlIngenico = cConfiguracionUrlIngenico.dsValor.ToString();
                    string strUrlIngenico = "http://prueba.bexcaret.com/ingenicoV2/webService/Payments.svc/createpayment"; //pruebas
                    List<string> lstCodigosErrorGlobal = cConfiguracionCodigosErrorGlobal.dsValor.Split(',').ToList();

                    /*strDcTotal = dcTotal.ToString("N2");
                    strDcTotal = strDcTotal.Replace(",", "");
                    strDcTotal = strDcTotal.ToString().Replace(".", "");*/
                    string strDcTotalIng = dcTotal.ToString("N2").Replace(".", "");// string strDcTotal = Math.Round(dcTotal, 2).ToString("N2");
                    strDcTotalIng = strDcTotalIng.Replace(",", "");

                    //string strDcTotaal = utils.fnRedondeaMonto(strDcTotalWorld);

                    string strAmount = strDcTotalIng;


                    strCadenaIngenico += "id=" + strIdVentasEnviar;
                    strCadenaIngenico += "&locale=es_MX";
                    strCadenaIngenico += "&env=" + ConfigurationManager.AppSettings["bdProduccion"];
                    strCadenaIngenico += "&noAGV=" + "1";

                    //if (intIdTipoMonedaPagar != 2) // diferente de Pesos
                    //{
                    //    strCadenaIngenico += "&amount=" + strDcTotalDolar;
                    //}
                    //else //pesos
                    //{
                    strCadenaIngenico += "&amount=" + strAmount; //strDcTotal;
                    //}

                    strCadenaIngenico += "&cname=" + txtNombreTH.Text.Trim();
                    strCadenaIngenico += "&email=" + hdCorreoCompetidor.Value.Trim();
                    //strCadenaIngenico += "&buyerip=10.10.0.0"; //utils.GetIpAddress()
                    strCadenaIngenico += "&buyerip=" + utils.GetIpAddress();
                    strCadenaIngenico += "&countryid=" + hdIdPais.Value;
                    strCadenaIngenico += "&stateid=" + hdIdEstado.Value;
                    //strCadenaIngenico += "&saleschannel=" + ConfigurationManager.AppSettings["idCanalVenta"].ToString();
                    strCadenaIngenico += "&saleschannel=11"; // para prueba
                    strCadenaIngenico += "&cointypeid=" + strIdTipoMoneda;
                    strCadenaIngenico += "&paywayid=" + ddlFormaPago.SelectedItem.Value;
                    strCadenaIngenico += "&nmsi=0";
                    strCadenaIngenico += "&bankid=" + ddlBanco.SelectedItem.Value;
                    strCadenaIngenico += "&CardNumber=" + txtNoTarjeta.Text;
                    strCadenaIngenico += "&expiryDate=" + ddlMesExpira.SelectedItem.Value + "/" + ddlAnioExpira.SelectedItem.Value.Substring(ddlAnioExpira.SelectedItem.Value.Length - 2);
                    strCadenaIngenico += "&cvv=" + txtCVV.Text.Trim();
                    strCadenaIngenico += "&session=" + getDsSession();
                    strCadenaIngenico += "&cnupgrade=false"; //  true - es upgrade solo guarda kpago y kpagotransaccion, false - es alta actualiza estatus, allotment, kventa, kventadetalle
                    strCadenaIngenico += "&returnurl=";
                    //strCadena += "&netpay=XXX

                    strCadenaEncriptada = security.Encrypt(strCadenaIngenico);

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(strUrlIngenico);
                    httpWebRequest.ContentType = "application/json; charset=utf-8";
                    httpWebRequest.Headers.Add("Authorization", "TBsmtsDRvL6JpElMq2zYrdVS0EazVjgvTmg180uxlYo=");
                    httpWebRequest.Method = "POST";

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string json = "{\"qsvars\":\"" + strCadenaEncriptada + "\"}";

                        streamWriter.Write(json);
                        streamWriter.Flush();
                    }

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    string strResulJson = "";
                    rootIngenico dtjson = new rootIngenico();
                    try
                    {
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            strResulJson = streamReader.ReadToEnd();
                        }
                        dtjson = JsonConvert.DeserializeObject<rootIngenico>(strResulJson);
                    }
                    catch (Exception ex)
                    {
                        //WebRoleProductos.XRM.utilXcrm.mensaje("Ingenico strResulJson: " + strResulJson, exc);
                        //result = utils.buscarCompetidor(getDsSession(), "", "", 0, 0, "", "", 0, 0, 0, intIdClienteContactoEvento);
                        string msg = ex.Message.ToString();
                        string strginnermessage = " Error intento Pago triatlon por ingenico";
                        string strCuerpoCorreo = "Ingenico strResulJson: " + strResulJson + "<br>Error intento Pago triatlon por ingenico." + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                        string strExcepcion = "";
                        if (ex.ToString().Length > 799)
                            strExcepcion = ex.ToString().Substring(0, 999);
                        else
                            strExcepcion = ex.ToString();
                        strCuerpoCorreo += strExcepcion;
                        MensajeAlert(strCuerpoCorreo);

                    }

                    if (dtjson.Payment == null)
                    {
                        strIdEstatusPago = "1" /*Declinado*/;
                        lblRespuestaPago.Text = "404 - error en la transacción";
                        strDsRespuesta = "404 - error en la transacción. Sin respuesta del servicio.";
                    }
                    else if (Int32.Parse(dtjson.Payment.StatusOutput.StatusCode.ToString()) >= 625 && !lstCodigosErrorGlobal.Contains(dtjson.Payment.StatusOutput.StatusCode.ToString()))
                    {
                        strIdEstatusPago = "2" /*Aceptado*/;
                        lblRespuestaPago.Text = "Pago aprobado";
                        strDsRespuesta = dtjson.Payment.StatusOutput.StatusCode.ToString();

                        if (dtjson.Payment.PaymentOutput.CardPaymentMethodSpecificOutput.AuthorisationCode != null)
                        {
                            if (dtjson.Payment.PaymentOutput.CardPaymentMethodSpecificOutput.AuthorisationCode.ToString().Trim().Length == 0) // si noy código de autorizacion se declina
                            {
                                strIdEstatusPago = "1" /*Declinado*/;
                                lblRespuestaPago.Text = "No hay código de Autorización";
                            }
                        }
                    }
                    if (dtjson.Payment != null)
                    {
                        if (dtjson.Payment.StatusOutput.Errors != null)
                        {
                            lblRespuestaPago.Text = dtjson.Payment.StatusOutput.Errors[0].Message.ToString();
                            strDsRespuesta = lblRespuestaPago.Text;
                        }
                    }
                    cRespuestaPagoCompetidor.strIdEstatusPago = strIdEstatusPago;
                    cRespuestaPagoCompetidor.strAfiliacion = "";
                    cRespuestaPagoCompetidor.strDsRespuesta = strDsRespuesta;
                    cRespuestaPagoCompetidor.strDsJustificacion = "";
                    cRespuestaPagoCompetidor.strDsReferencia = "";
                    cRespuestaPagoCompetidor.strDsTransaccion = "";
                    cRespuestaPagoCompetidor.strDsCorrelacion = "";
                    cRespuestaPagoCompetidor.strIdBancoReceptor = "";
                    #endregion Ingenico
                    break;

                default:
                    break;

            }           

            /*if (ddlBanco.SelectedItem.Value != "35") // distinto del banco amex
            {
                cResultadoGuardadoPago.strIdBancoReceptor = "32";
            }*/
            /*switch (ddlBanco.SelectedItem.Value)
            {
                case "35":
                    // Amex
                    cRespuestaPagoCompetidor.strIdBancoReceptor = ddlBanco.SelectedItem.Value;
                    break;
                case "5":
                    // Bancomer
                    cRespuestaPagoCompetidor.strIdBancoReceptor = ddlBanco.SelectedItem.Value;
                    break;
                case "108":
                    // Banamex
                    cRespuestaPagoCompetidor.strIdBancoReceptor = ddlBanco.SelectedItem.Value;
                    break;
                default:
                    // Otros
                    cRespuestaPagoCompetidor.strIdBancoReceptor = "32"; // Banorte
                    break;
            }*/
            Session["cRespuestaPagoCobro" + cnIdContactoEvento.Value] = cRespuestaPagoCompetidor;
        }

        protected bool fnGuardarPago(int intIdCliente, int intIdVenta, decimal dcTotal, string idMoneda)
        {
            int intIdTipoMoneda = 0;
            bool banderaPago = true;
            int intIdCanalVenta = int.Parse(ConfigurationManager.AppSettings["idCanalVenta"]);
            string totalPagar = dcTotal.ToString("N2");

            string strFolios = "";
            string strIdVenta = "";
            string[] strIdVentas = hdIV.Value.Split(',');
            string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
            string dsIsoMoneda = selectedValueMoneda[1].ToString();
           // string idMoneda = selectedValueMoneda[0].ToString();            
            string idTipoMoneda = selectedValueMoneda[2].ToString();

            cRespuestaPagoCompetidor = getObjetoRespuestaPago();
            for (int l = 0; l < (strIdVentas.Length - 1); l++)
            {
                wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                wsTransactions.kVenta laVenta = new wsTransactions.kVenta();

                ikventa.Open();
                
                laVenta = ikventa.SeleccionarkVentaPorIdVenta(int.Parse(strIdVentas[l].ToString()), getDsSession());
                //wsTransactions.ListakVentaDetalles laVentaDetalles = ikventa.SeleccionarkVentasDetallePorIdVenta(int.Parse(strIdVentas[l].ToString()), cGlobals.dsSession);
                ikventa.Close();
                intIdTipoMoneda = int.Parse(idTipoMoneda);

                //------------------------------------------------------------------------------------------------
                // PAGO
                //------------------------------------------------------------------------------------------------

                wsTransactions.IkPagoClient IkPago = new wsTransactions.IkPagoClient();
                wsTransactions.ListakPagoTransacciones lstkPagoTransa = new wsTransactions.ListakPagoTransacciones();
                wsTransactions.kPagoTransaccion PagoTran = new wsTransactions.kPagoTransaccion();
                wsTransactions.kPago objkPago = new wsTransactions.kPago();

                objkPago.idPago = 0;
                objkPago.idVenta = int.Parse(strIdVentas[l].ToString());
                objkPago.idCanalPago = intIdCanalVenta; //11; // fijo ?
                objkPago.feTransaccionTotal = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
                objkPago.hrTransaccionTotal = DateTime.Now;
                //objkPago.mnTransaccionTotal = Math.Round(dcTotal, 2);
                //objkPago.mnTransaccionTotal = decimal.Parse(totalPagar);
                objkPago.mnTransaccionTotal = laVenta.mnMontoTotal;
                objkPago.cnEsPrepago = false;
                objkPago.noAgrupadorVenta = 1;
                // si el montototal es cero lo ponemos forma pago cortesia = 14
                if (laVenta.mnMontoTotal == 0)
                    objkPago.idFormaPago = 14;
                else
                    objkPago.idFormaPago = Int32.Parse(ddlFormaPago.SelectedItem.Value);
                objkPago.idEstatusPago = Int32.Parse(cRespuestaPagoCompetidor.strIdEstatusPago); //intIdEstatusPago;
                objkPago.feAlta = DateTime.Now;
                objkPago.idClienteUsuarioAlta = (cGlobals.idUsuario);
                
                PagoTran.idPago = objkPago.idPago; //
                // En idMoneda se guarda el IdTipoMoneda (Gabriel 20082013)
                PagoTran.idTipoMoneda = intIdTipoMoneda; //Int32.Parse(cmbMoneda.SelectedItem.Value);

                PagoTran.feTransaccion = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
                PagoTran.hrTransaccion = DateTime.Now;
                PagoTran.dsRespuesta = cRespuestaPagoCompetidor.strDsRespuesta; // strDsRespuesta;
                PagoTran.dsReferencia = cRespuestaPagoCompetidor.strDsReferencia; // strDsReferencia;
                PagoTran.dsJustificacion = cRespuestaPagoCompetidor.strDsJustificacion; // strDsJustificacion;

                //PagoTran.mnTransaccion = Math.Round(dcTotal, 2);
                //PagoTran.mnTransaccion = decimal.Parse(totalPagar);
                PagoTran.mnTransaccion = laVenta.mnMontoTotal;

                PagoTran.dsTransaccion = cRespuestaPagoCompetidor.strDsTransaccion; // strDsTransaccion;
                PagoTran.dsCorrelacion = cRespuestaPagoCompetidor.strDsCorrelacion; // strDsCorrelacion;
                PagoTran.noPagosMSI = 0;
                objkPago.cnPlanDePagos = false;

                PagoTran.cnPrepago = false;
                //if (intIdClienteTarjeta > 0)
                if (cRespuestaPagoCompetidor.strIdClienteTarjeta != null)
                    if (Int32.Parse(cRespuestaPagoCompetidor.strIdClienteTarjeta) > 0)
                        PagoTran.idClienteTarjeta = Int32.Parse(cRespuestaPagoCompetidor.strIdClienteTarjeta); // intIdClienteTarjeta;
                PagoTran.mnBancoComision = 0;
                PagoTran.prBancoComision = 0;
                PagoTran.feAlta = DateTime.Now;
                PagoTran.idClienteUsuarioAlta = (cGlobals.idUsuario);
                PagoTran.idPagoTransaccion = 0;
                PagoTran.idEstatusPago = Int32.Parse(cRespuestaPagoCompetidor.strIdEstatusPago);
                if (cRespuestaPagoCompetidor.strIdBancoReceptor != "")
                    if (Int32.Parse(cRespuestaPagoCompetidor.strIdBancoReceptor) > 0)
                        PagoTran.idBancoReceptor = Int32.Parse(cRespuestaPagoCompetidor.strIdBancoReceptor);

                PagoTran.dsAfiliacion = cRespuestaPagoCompetidor.strAfiliacion;
                if (ddlBanco.SelectedItem.Value != "")
                if (Int32.Parse(ddlBanco.SelectedItem.Value) > 0) // temporal en lo que se corrige la clase
                    PagoTran.idBanco = Int32.Parse(ddlBanco.SelectedItem.Value);
                
                lstkPagoTransa.Add(PagoTran);
                int intIdPago;
                try
                {
                    // 1 er intento
                    intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());

                }
                catch (Exception err)
                {
                    // mensaje("OE fallo primer intento, intIdTipoMoneda=" + intIdTipoMoneda.ToString() + strCadenaResultadoPagoTarjeta, err);
                    try
                    {
                        // 2 er intento
                        intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                    }
                    catch (Exception err2)
                    {
                        // 3 er intento
                        // mensaje("OE fallo segundo intento, intIdTipoMoneda=" + intIdTipoMoneda.ToString() + strCadenaResultadoPagoTarjeta, err);
                        intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                    }
                }
                IkPago.Close();
            }                                   
            
            //if (intIdEstatusPago == 1) // Pago con tarjeta aprobada  --  2 /*Aceptado*/: 1 /*Declinado*/
            if (Int32.Parse(cRespuestaPagoCompetidor.strIdEstatusPago) == 1)
            {
                banderaPago = false;
            }

            return banderaPago;
        }

        protected int actualizar(int intIdCliente, decimal dcTotal, string idMoneda, int idVenta)
        {
            //leemo si el pago paso
            int resultadoActualizar = 0;
            string strFolios = "";
            string strIdVenta = "";
            string[] strIdVentas = hdIV.Value.Split(',');
            cRespuestaPagoCompetidor = getObjetoRespuestaPago();
            if (cRespuestaPagoCompetidor.strIdEstatusPago == "2")
            {
                for (int l = 0; l < (strIdVentas.Length - 1); l++)
                {                
                    // actualizamos la venta a pagado
                    wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                    ikventa.Open();
                    wsTransactions.kVenta laVenta = ikventa.SeleccionarkVentaPorIdVenta(int.Parse(strIdVentas[l].ToString()), getDsSession());
                    if (laVenta.mnMontoTotal == 0)
                        laVenta.idEstatusVenta = 23;
                    else
                        laVenta.idEstatusVenta = 7; // pagado
                    //laVenta.feAlta = DateTime.Now;
                    laVenta.feVenta = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
                    laVenta.hrVenta = DateTime.Now;

                    wsTransactions.ListakVentaDetalles listaDetalle = ikventa.SeleccionarkVentasDetallePorIdVenta(int.Parse(strIdVentas[l].ToString()), getDsSession());

                    for (int i = 0; i < listaDetalle.Count(); i++)
                    {
                        decimal prDescuento = decimal.Parse(listaDetalle[i].prDescuento.ToString());
                        if (prDescuento == 100)
                        {
                            listaDetalle[i].idEstatusVenta = 23;
                        }
                        else
                        {
                            listaDetalle[i].idEstatusVenta = 7; // pagada
                        }
                        listaDetalle[i].feAlta = DateTime.Now;
                    }
                    resultadoActualizar = ikventa.ModificarkVenta(laVenta, listaDetalle, null, null, getDsSession());                
                }
            }
            return resultadoActualizar;
        }

        protected int ActualizarCompetidor(int intIdCliente, decimal dcTotal, string idMoneda, int idVenta)
        {
            // AHORA TENEMOS QUE ACTUALIZAR A INSCRITO AL COMPETIDOR Y DARLE SU NUMERO DE COMPETIDOR
            string strFolios = "";
            string strIdVenta = "";
            string[] strIdVentas = hdIV.Value.Split(',');
            string strIdClienteContactoCompetidorEvento = "";
            string strIdClienteDetalle = "";
            string strIdEventoClasificacion = "";
            int intNumeroCompetidor = 0;
            int respuestaModificaContactoEvento = 0;
            int intIdEventoModalidad = 0;
            string strDsClaveEventoModalidad = "";
            DataTable dtCompetidor = getDatatableCompetidor();
            int intSizeDtCompedidor = dtCompetidor.Rows.Count;

            for (int i = 0; i < dtCompetidor.Rows.Count; i++)
            {
                // obtenemos el numero de competidor que le corresponde
                if (i == 0)
                {
                    strIdEventoClasificacion = dtCompetidor.Rows[i]["idEventoClasificacion"].ToString();
                    intIdEventoModalidad = (dtCompetidor.Rows[i]["idEventoModalidad"].ToString()!= "")? int.Parse(dtCompetidor.Rows[i]["idEventoModalidad"].ToString()): 0;
                    dtCompetidor.Rows[i]["noCompetidor"] = intNumeroCompetidor;
                    strIdClienteContactoCompetidorEvento = dtCompetidor.Rows[i]["idClienteContactoEvento"].ToString();
                    strIdClienteDetalle = dtCompetidor.Rows[i]["idClienteDetalle"].ToString();

                    wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                    wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                    //clienteContactoEventoClient.Open();
                    

                    // buscamos los idventasdetalles
                    wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();

                    wsTransactions.ListakVentaDetalles listaKVentaDetalles = new wsTransactions.ListakVentaDetalles();
                    listaKVentaDetalles = ikventa.SeleccionarkVentasDetallePorIdVenta(int.Parse(strIdVentas[0].Trim()), getDsSession());

                    for (int k = 0; k < listaKVentaDetalles.Count(); k++)
                    {
                        cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(strIdClienteContactoCompetidorEvento), getDsSession());
                        if (k == 0)
                        {
                            cCompetidor.idVenta = int.Parse(strIdVentas[0].Trim());
                            //cCompetidor.NoCompetidor = intNumeroCompetidor;
                            cCompetidor.idVentaDetalle = listaKVentaDetalles[k].idVentaDetalle;
                            if (cCompetidor.idEventoModalidad == 0)
                                cCompetidor.idEventoModalidad = null;
                            if (cCompetidor.idTipoSangre == 0)
                            {
                                cCompetidor.idTipoSangre = null;
                            }
                            try
                            {
                                respuestaModificaContactoEvento = clienteContactoEventoClient.ModificarcClienteContactoEvento(cCompetidor, getDsSession());
                                string strfac = "";
                                // agregamos uno al contador de usados
                                int intIdLocacion = (int)cCompetidor.idLocacion;
                                //cParticipante.agregaNoUsados(getDsSession(), 1, intIdLocacion);
                            }
                            // si es mayor a uno significa que se craron folios ya sea por donativo o por incripcion a cena   
                            #region catches
                            catch (Exception ex)
                            {
                                lblRespuestaPago.Text = "Pago realizado con cargo a su tarjeta, pero ocurrió error al actualizar al competidor. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                                string msg = ex.Message.ToString();
                                string strginnermessage = "";
                                string strCuerpoCorreo = "ActualizarCompetidor actualizaCompetidor " + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                                string strExcepcion = "";
                                if (ex.ToString().Length > 799)
                                    strExcepcion = ex.ToString().Substring(0, 999);
                                else
                                    strExcepcion = ex.ToString();
                                strCuerpoCorreo += strExcepcion;
                                MensajeAlert(strCuerpoCorreo);
                            }
                            #endregion
                        }// si k no es cero, entonces insertamos nuevo clientecontacto evento
                        #region elses
                        else
                        {
                            cCompetidor.idClienteContactoEvento = 0;
                            cCompetidor.idVenta = int.Parse(strIdVentas[0].Trim());
                            //cCompetidor.NoCompetidor = intNumeroCompetidor;
                            cCompetidor.idVentaDetalle = listaKVentaDetalles[k].idVentaDetalle;
                            if (cCompetidor.idEventoModalidad == 0)
                                cCompetidor.idEventoModalidad = null;
                            if (cCompetidor.idTipoSangre == 0)
                            {
                                cCompetidor.idTipoSangre = null;
                            }
                           
                            cCompetidor.dsAgrupador = "Travesia";
                            
                            try
                            {
                                respuestaModificaContactoEvento = clienteContactoEventoClient.InsertarcClienteContactoEvento(cCompetidor, getDsSession());
                                
                            }
                            // si es mayor a uno significa que se craron folios ya sea por donativo o por incripcion a cena                
                            catch (Exception ex)
                            {
                                lblRespuestaPago.Text = "Pago realizado con cargo a su tarjeta, pero ocurrió error al actualizar al competidor. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                                string msg = ex.Message.ToString();
                                string strginnermessage = "";
                                string strCuerpoCorreo = "ActualizarCompetidor actualizaCompetidor " + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                                string strExcepcion = "";
                                if (ex.ToString().Length > 799)
                                    strExcepcion = ex.ToString().Substring(0, 999);
                                else
                                    strExcepcion = ex.ToString();
                                strCuerpoCorreo += strExcepcion;
                                MensajeAlert(strCuerpoCorreo);
                            }
                        }
                        #endregion
                    }
                    wscClienteDetalle.IcClienteDetalleClient cteCLiente = new wscClienteDetalle.IcClienteDetalleClient();
                    wscClienteDetalle.cClienteDetalle ClienteDet;
                    ClienteDet = cteCLiente.SeleccionarcClienteDetallePorId(int.Parse(strIdClienteDetalle), getDsSession());
                    // cambiamos el estatus del participante a inscrito
                    ClienteDet.idEstatusCliente = 15;

                    try
                    {
                        int resModificaDetalle = cteCLiente.ModificarcClienteDetalle(ClienteDet, getDsSession());
                    }
                    catch (Exception ex)
                    {
                        lblRespuestaPago.Text = "Pago realizado con cargo a su tarjeta, pero ocurrió error al actualizar al competidor. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                        string msg = ex.Message.ToString();
                        string strginnermessage = "";
                        string strCuerpoCorreo = "ActualizarCompetidor actualizaCompetidor " + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                        string strExcepcion = "";
                        if (ex.ToString().Length > 799)
                            strExcepcion = ex.ToString().Substring(0, 999);
                        else
                            strExcepcion = ex.ToString();
                        strCuerpoCorreo += strExcepcion;
                        MensajeAlert(strCuerpoCorreo);
                    }
                    int intTamanioArregloIdVentas = strIdVentas.Length;
                    if (intTamanioArregloIdVentas > 2 && strIdVentas[(intTamanioArregloIdVentas - 2)].ToString() != "")
                    {
                        wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient2 = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                        wscClienteContactoEvento.cClienteContactoEvento cCompetidorDonativo;
                        clienteContactoEventoClient2.Open();
                        cCompetidorDonativo = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(strIdClienteContactoCompetidorEvento), getDsSession());
                        // buscamos los idventasdetalles
                        wsTransactions.IkVentaClient ikventa2 = new wsTransactions.IkVentaClient();

                        wsTransactions.ListakVentaDetalles listaKVentaDetalles2 = new wsTransactions.ListakVentaDetalles();
                        listaKVentaDetalles2 = ikventa.SeleccionarkVentasDetallePorIdVenta(int.Parse(strIdVentas[1].Trim()), getDsSession());

                        for (int k = 0; k < listaKVentaDetalles2.Count(); k++)
                        {
                            cCompetidorDonativo.idClienteContactoEvento = 0;
                            cCompetidorDonativo.idVenta = int.Parse(strIdVentas[1].ToString());
                            cCompetidorDonativo.idVentaDetalle = listaKVentaDetalles2[k].idVentaDetalle;
                            if (cCompetidorDonativo.idEventoModalidad == 0)
                                cCompetidorDonativo.idEventoModalidad = null;
                            if (cCompetidorDonativo.idTipoSangre == 0)
                            {
                                cCompetidorDonativo.idTipoSangre = null;
                            }
                            int idClienteContactoEvento = 0;
                            try
                            {
                                idClienteContactoEvento = clienteContactoEventoClient2.InsertarcClienteContactoEvento(cCompetidorDonativo, getDsSession());
                            }
                            // si es mayor a uno significa que se craron folios ya sea por donativo o por incripcion a cena                
                            catch (Exception ex)
                            {
                                lblRespuestaPago.Text = "Pago realizado con cargo a su tarjeta, pero ocurrió error al actualizar al competidor. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                                string msg = ex.Message.ToString();
                                string strginnermessage = "";
                                string strCuerpoCorreo = "ActualizarCompetidor actualizaCompetidor " + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                                string strExcepcion = "";
                                if (ex.ToString().Length > 799)
                                    strExcepcion = ex.ToString().Substring(0, 999);
                                else
                                    strExcepcion = ex.ToString();
                                strCuerpoCorreo += strExcepcion;
                                MensajeAlert(strCuerpoCorreo);
                            }
                        }                    
                        clienteContactoEventoClient2.Close();
                    }
                    #region relevos
                    // Ahora vamos a investigar si el competidor tiene compañeros relevos para que tambien se actualize sus datos de los respectivos
                    wscClienteContactoEvento.ListacClienteContactoEventos listContactoEventos = new wscClienteContactoEvento.ListacClienteContactoEventos();
                    listContactoEventos = clienteContactoEventoClient.SeleccionarcClienteContactoEventosporIdEquipo(int.Parse(strIdClienteContactoCompetidorEvento), getDsSession());                                        
                    if (listContactoEventos.Count > 0)
                    {
                        wscClienteContacto.IcClienteContactoClient clienteContacto = new wscClienteContacto.IcClienteContactoClient();
                        foreach (wscClienteContactoEvento.cClienteContactoEvento cCompetidores in listContactoEventos)
                        {
                            cCompetidores.idVenta = int.Parse(strIdVentas[0].Trim()); ;
                            cCompetidores.NoCompetidor = intNumeroCompetidor;
                            if (cCompetidores.idEventoModalidad == 0)
                                cCompetidores.idEventoModalidad = null;
                            if (cCompetidores.idTipoSangre == 0)
                                cCompetidores.idTipoSangre = null;
                            int respuestaModificaContactosEventos = 0;
                            try
                            {
                                respuestaModificaContactosEventos = clienteContactoEventoClient.ModificarcClienteContactoEvento(cCompetidores, getDsSession());
                            }
                            catch (Exception ex)
                            {
                                lblRespuestaPago.Text = "Pago realizado con cargo a su tarjeta, pero ocurrió error al actualizar al competidor. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                                string msg = ex.Message.ToString();
                                string strginnermessage = "";
                                string strCuerpoCorreo = "ActualizarCompetidor actualizaCompetidor " + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                                string strExcepcion = "";
                                if (ex.ToString().Length > 799)
                                    strExcepcion = ex.ToString().Substring(0, 999);
                                else
                                    strExcepcion = ex.ToString();
                                strCuerpoCorreo += strExcepcion;
                                MensajeAlert(strCuerpoCorreo);
                            }

                            wscClienteContacto.cClienteContacto cClienteContacto;
                            cClienteContacto = clienteContacto.SeleccionarcClienteContactoPorId((int)cCompetidores.idClienteContacto, getDsSession());

                            wscClienteDetalle.cClienteDetalle ClienteDetalle;
                            ClienteDetalle = cteCLiente.SeleccionarcClienteDetallePorId((int)cClienteContacto.idClienteDetalle, getDsSession());

                            // cambiamos el estatus del participante a inscrito
                            ClienteDetalle.idEstatusCliente = 15;
                            int resModificaDetalles = cteCLiente.ModificarcClienteDetalle(ClienteDetalle, getDsSession());
                        }
                    }
                    #endregion
                }                               
            }
            // ahora tengo que actualizar la noUsados
            // si son distinto de rifa y e infantil 9-11
            // 6 Solo Novatas y tenga 
            if (intIdEventoModalidad != 8 && intIdEventoModalidad != 9 && intIdEventoModalidad != 0)
            {
                // si es novata y tiene forma de idformapago no lo cuentes
                if (intIdEventoModalidad != 6 && hdIdConfigPago.Value != "")
                {
                    DataTable dtCEventoModalidad = utils.getCEventoModalidad(getDsSession(), intIdEventoModalidad.ToString(), "", "ID");
                    strDsClaveEventoModalidad = dtCEventoModalidad.Rows[0]["dsClave"].ToString();
                    //cParticipante.agregaNoUsados(intSizeDtCompedidor, strDsClaveEventoModalidad);
                }
            }
            Session["dtCompetidorPago" + cnIdContactoEvento.Value] = dtCompetidor;
            return respuestaModificaContactoEvento;
        }

        protected void btnGlobalCollect_Click(object sender, EventArgs e)
        {
            try
            {
                if (hddResultadoPagoGlobalCollect.Value.Trim().Length > 0)
                {
                    String[] strParametros = hddResultadoPagoGlobalCollect.Value.Split('|');
                    lblRespuestaPago.Visible = false;
                    string strIdEstatusPago = "1";  /**/
                    string strCodigoError = strParametros[0];
                    string strDescripcionError = strParametros[1];
                    string strCodigoAutorizacion = strParametros[2];
                    string strNumeroTarjeta = strParametros[3];
                    string strReferenciaGlobalColect = strParametros[3];
                    int intRespIdVenta = int.Parse(hdIdVenta.Value.ToString());
                    string strFolios = "";
                    string strIdVenta = "";
                    string[] strIdVentas = hdIV.Value.Split(',');

                    string strDsCorrelacion = strParametros[6]; // no de referencia

                    string strIdBancoReceptor = "32";	// Banorte

                    if (hdCnGlobalCollectI.Value != "0")
                    {
                        strIdBancoReceptor = "112"; // GlobalCollect Internacional
                    }

                    int intCodigoErrorGlobal = 0;
                    if (Int32.TryParse(strCodigoError.Trim(), out intCodigoErrorGlobal))
                    {

                        if (intCodigoErrorGlobal >= 625)
                        {
                            strIdEstatusPago = "2" /*Aceptado*/;
                            if (strCodigoAutorizacion.Trim().Length == 0) // si noy código de autorizacion se declina
                            {
                                strIdEstatusPago = "1" /*Declinado*/;
                                lblRespuestaPago.Text = "No hay código de Autorización";
                            }
                        }
                        else
                        {
                            strIdEstatusPago = "1" /*Declinado*/;
                            btnPagarFinal.Visible = true;
                        }
                    }
                    cRespuestaPagoCompetidor = getObjetoRespuestaPago();
                    cRespuestaPagoCompetidor.strIdEstatusPago = strIdEstatusPago;
                    //cResultadoGuardadoPago.strAfiliacion = (amexresponse.dsAfiliacion != null) ? amexresponse.dsAfiliacion : "";
                    cRespuestaPagoCompetidor.strDsRespuesta = strDescripcionError;
                    cRespuestaPagoCompetidor.strDsJustificacion = strDescripcionError;
                    cRespuestaPagoCompetidor.strDsReferencia = strDescripcionError;
                    cRespuestaPagoCompetidor.strDsTransaccion = strCodigoAutorizacion;
                    cRespuestaPagoCompetidor.strDsCorrelacion = strDsCorrelacion;
                    cRespuestaPagoCompetidor.strIdBancoReceptor = strIdBancoReceptor;
                    getDsSession();
                    int intIdCliente = cGlobals.idUsuario;
                    decimal dcTotal = decimal.Parse(hdToTalPagar.Value);
                    //                string idMoneda = ddlCambioMoneda.SelectedValue.ToString();
                    string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
                    string dsIsoMoneda = selectedValueMoneda[1].ToString();
                    string idMoneda = selectedValueMoneda[0].ToString();
                    int respActualizaCompetidor = 0;
                    fnGuardaDatosTarjeta(intIdCliente, dcTotal, idMoneda, strNumeroTarjeta);

                    bool respGuardaPago = fnGuardarPago(intIdCliente, intRespIdVenta, dcTotal, idMoneda);
                    cCompetidorPagar.DtCompetidor = getDatatableCompetidor();
                    if (respGuardaPago)
                    {
                        int resultActualizar = actualizar(intIdCliente, dcTotal, idMoneda, intRespIdVenta);
                        int intIdClienteContactoEvento = 0;
                        int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                        // si el resultado es mayor a cero quiere decir que actualizo
                        if (resultActualizar > 0)
                        {
                            respActualizaCompetidor = ActualizarCompetidor(intIdCliente, dcTotal, idMoneda, intRespIdVenta);
                            DataTable dtCompetidor = getDatatableCompetidor();
                            if (respActualizaCompetidor > 0)
                            {
                                string strIdClienteContactoCompetidorEvento = dtCompetidor.Rows[0]["idClienteContactoEvento"].ToString();
                                //strIdClienteDetalle = dtCompetidor.Rows[i]["idClienteDetalle"].ToString();

                                wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                                wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                                //clienteContactoEventoClient.Open();
                                cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(strIdClienteContactoCompetidorEvento), getDsSession());
                                clienteContactoEventoClient.Close();
                                // si todo pasó bien ahora enviamos correo al competidor
                                bool respCorreoConfirmacion = correoPago(hdIV.Value, "", "1", hdCorreoCompetidor.Value, cCompetidorPagar.DtCompetidor, cCompetidor);
                                //cParticipante.dtCompedidor = null;
                                Session["dtCompetidorPago" + cnIdContactoEvento.Value] = null;
                                cCompetidorPagar.DtCompetidor = null;
                                string idVentasDetalles = hdIV.Value.Remove(hdIV.Value.Length - 1);
                                Response.Redirect("respPago.aspx?token=" + idVentasDetalles + "&contEvent=" + intIdClienteContactoEvento.ToString());
                            }
                            else
                            {

                            }
                        }
                        else
                        {

                        }
                    }// fin if (respGuardaPago)
                    else
                    {
                        lblRespuestaPago.Visible = true;
                        lblRespuestaPago.Text = "Pago declinado. " + cRespuestaPagoCompetidor.strDsRespuesta;
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                string strginnermessage = " btnGlobalCollect_Click ";
                string strCuerpoCorreo = msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                string strExcepcion = "";
                if (ex.ToString().Length > 799)
                    strExcepcion = ex.ToString().Substring(0, 999);
                else
                    strExcepcion = ex.ToString();
                strCuerpoCorreo += strExcepcion;
                MensajeAlert(strCuerpoCorreo);
            }
        }

        decimal getTotalVenta(DataTable dtVentaOrdenador)
        {
            decimal respuesta = 0;

            for (int i = 0; i < dtVentaOrdenador.Rows.Count; i++)
            {
                respuesta += (decimal)dtVentaOrdenador.Rows[i]["mnPrecioVenta"];
            }
            return respuesta;
        }

        protected bool correoPago(string strIdVenta, string strFolios, string strIdioma, string strCorreoCliente, DataTable dtCompedidor, wscClienteContactoEvento.cClienteContactoEvento cCompetidor)
        {
            //string strParametros = "<br>Parametros pasados a la funcion correoPago: strIdVenta:" + strIdVenta + "<br>strFolios:" + strFolios + "<br>strIdioma:" + strIdioma + "<br>strCorreoCliente:" + strCorreoCliente + "<br>lblMensajeCorreo:" + lblMensajeCorreo.Text;
            int intIdClienteContacto = 0;
            try
            {
                wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();
                iGeneral.Open();
                // Textos Correos
                wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                icConfiguracion.Open();

                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto2CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto2CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto10CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto10CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto11CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto11CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto12CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto12CorreoPagoIdioma_" + strIdioma, getDsSession());
                // wscConfiguracionAplicacion.cConfiguracionAplicacion Texto13CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto13CorreoPagoIdioma_" + strIdioma, Globals.dsSession);
                wscConfiguracionAplicacion.cConfiguracionAplicacion TextoUrlCuponIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-URLCUPON_CanalVenta_27_Id_" + strIdioma, getDsSession());

                icConfiguracion.Close();
                string nombre = dtCompedidor.Rows[0]["dsContacto"].ToString() + ' ' + dtCompedidor.Rows[0]["dsApellidoPaterno"].ToString() + ' '+ dtCompedidor.Rows[0]["dsApellidoMaterno"].ToString();
                string numeroCompetidor = dtCompedidor.Rows[0]["noCompetidor"].ToString();
                string strCuerpoCorreo = "<table>";
                string strCuerpoCorreoLinksFolios = "";
                strIdVenta = strIdVenta.Remove(strIdVenta.Length - 1);
                string[] arrStrIdVenta = strIdVenta.Split(',');

                for (int ab = 0; ab < arrStrIdVenta.Length; ab++)
                {
                    wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                    ikventa.Open();
                    wsTransactions.kVenta laVenta = ikventa.SeleccionarkVentaPorIdVenta(int.Parse(arrStrIdVenta[ab]), getDsSession());
                    strFolios += laVenta.dsClaveVenta + ",";
                    strCuerpoCorreoLinksFolios += "<a href=\"" + TextoUrlCuponIdioma.dsValor.Replace("<idVenta>", arrStrIdVenta[ab]) + "\" >" + laVenta.dsClaveVenta + "</a>" + ((arrStrIdVenta.Length == 1 || ab == (arrStrIdVenta.Length - 1)) ? "" : ",");
                }
                strFolios = strFolios.Remove(strFolios.Length - 1);
                strCuerpoCorreo += "<tr><td colspan=\"2\"> Estimado/a  <b>" + nombre + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"> Te informamos que de acuerdo a tu solicitud, hemos confirmado tu inscripción. </td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto2CorreoPagoIdioma.dsValor + " " + strFolios + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>Número Canoero: </b>" + cCompetidor.dsNumeroCompetidor.ToString().Trim() + "</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + "Importante: Por favor lee detenidamente e imprime el documento del enlace (";
                strCuerpoCorreo += strCuerpoCorreoLinksFolios;

                
                //string[] arrStrFolios = strFolios.Split(',');
                

                strFolios = strFolios.Remove(strFolios.Length - 1);

                strCuerpoCorreo += ") y entregalo el primer día de entrenamiento</b></td></tr>";

                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">Si deseas una factura, por favor entra a: https://www.xperienciasxcaret.mx/core/facturacion/, recuerda que tienes 24 horas para solicitar tu factura </td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                //strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto10CorreoPagoIdioma.dsValor + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto11CorreoPagoIdioma.dsValor + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">" + Texto12CorreoPagoIdioma.dsValor + "</td></tr>";

                strCuerpoCorreo += "</table>";

                string strCorreoAgente = "";

                /* wscClienteUsuario.IcClienteUsuarioClient IcclienteUsuario = new wscClienteUsuario.IcClienteUsuarioClient();
                 IcclienteUsuario.Open();
                 wscClienteContacto.IcClienteContactoClient IcClienteContacto = new wscClienteContacto.IcClienteContactoClient();
                 IcClienteContacto.Open();

                 wscClienteUsuario.cClienteUsuario cclienteUsuario = IcclienteUsuario.SeleccionarcClienteUsuarioPorId(Int32.Parse(cGlobals.idUsuario.ToString()), cGlobals.dsSession);
                 intIdClienteContacto = (int)cclienteUsuario.idClienteContacto;

                 wscClienteContacto.cClienteContacto cClienteContacto = IcClienteContacto.SeleccionarcClienteContactoPorId(intIdClienteContacto, cGlobals.dsSession);
                 strCorreoAgente = cClienteContacto.dsCorreoElectronico;

                 IcclienteUsuario.Close();
                 IcClienteContacto.Close();*/
                //
                string strMsg = utils.armaHTMLBaseCorreo("Confirmación de Inscripción Travesía Sagrada Maya", strCuerpoCorreo);
                iGeneral.Close();
                wscEmail.EmailingQueueServiceClient wsEmail = new wscEmail.EmailingQueueServiceClient();
                wscEmail.cEmailingQueue cEmail = new wscEmail.cEmailingQueue();
                cEmail.dsBody = strMsg;
                cEmail.dsClasificacion = "Confirmación inscripción";
                cEmail.dsRecipients = strCorreoCliente;
                //cEmail.dsReply_To = "";
                cEmail.dsSenderMail = "sivex@experienciasxcaret.com.mx";
                cEmail.dsSubject = "Confirmación de Inscripción Travesía Sagrada Maya";
                cEmail.idVenta = int.Parse(arrStrIdVenta[0].ToString());
                cEmail.noPriority = 3;
                wsEmail.Open();
                var response = wsEmail.InsertEmailQueue(cEmail, cGlobals.dsSession);
                wsEmail.Close();
                //bool blnBanderaCorreo = iGeneral.SendEmail(strCorreoCliente, "Confirmación de Inscripción Travesía Sagrada Maya", utils.armaHTMLBaseCorreo("Confirmación de Inscripción Travesía Sagrada Maya", strCuerpoCorreo), true /*es html*/, "", false, 0, strCorreoAgente);
                //bool blnBanderaCorreo = iGeneral.SendEmail("irojas@experienciasxcaret.com.mx", TituloCorreoIdioma.dsValor, Utils.armaHTMLcorreoCancelacion(TituloCorreoIdioma.dsValor + " XCARET", strCuerpoCorreo), true /*es html*/, "", false, 0, strCorreoAgente);

                iGeneral.Close();

                return true;
            }
            catch (Exception err)
            {
                //mensaje("correoPago <br>intIdClienteContacto pasado a SeleccionarcClienteContactoPorId:" + intIdClienteContacto.ToString() + " " + strParametros, err);
                return false;
            }
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
                //cGlobals.dsSession = "e0gbam2hoay2myk21x54ajcy";     // Guardar la variable global con el numero de sesion          
                
                //cGlobals.idUsuario = 9; // GUardar la variable global con el id del usuario;
                //cGlobals.idCliente = 1;
            }
            return cGlobals.dsSession;
        }

        public void MensajeAlert(string errMensaje)
        {
            
            wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();
            iGeneral.Open();
            string strCorreos = "nfigueroa@experienciasxcaret.com.mx";
            //bool blnBanderaCorreo = iGeneral.SendEmail(strCorreos, "Error en Aplicacion Travesia", errMensaje, true /*es html*/, "", false, 0, "");
            iGeneral.Close();
            wscEmail.EmailingQueueServiceClient wsEmail = new wscEmail.EmailingQueueServiceClient();
            wscEmail.cEmailingQueue cEmail = new wscEmail.cEmailingQueue();
            cEmail.dsBody = errMensaje;
            cEmail.dsClasificacion = "Error";
            cEmail.dsRecipients = strCorreos;
            //cEmail.dsReply_To = "";
            cEmail.dsSenderMail = "sivex4@experienciasxcaret.com.mx";
            cEmail.dsSubject = "Error en Aplicacion Travesia";
            //cEmail.idVenta = "";
            cEmail.noPriority = 3;
            wsEmail.Open();
            var response = wsEmail.InsertEmailQueue(cEmail, cGlobals.dsSession);
            wsEmail.Close();
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + strCuerpoCorreo + "');", false);
        }

        protected int getFechaMinVisita(DataTable dtVentaDetalleResume, string strSession)
        {
            List<int> levels = dtVentaDetalleResume.AsEnumerable().Select(al => (al.Field<int>("feVisita"))).Distinct().ToList();

            int feMinVista = 0;
            feMinVista = levels.Min();
            //var first = dtVentaDetalleResume.AsEnumerable().Min(r => r.Field<int>(DateTime.Parse(r.Field<string>("feVisita")).ToString("yyyyMMdd")));
            // var first2 = dtVentaDetalleResume.AsEnumerable().Min(r => r.Field<int>(DateTime.Parse(r.Field<string>("feVisita")).ToString("yyyyMMdd")));
            return feMinVista;
        }

        public decimal getMontoTotal(DataTable dtVentaDetalleResume, string strSession)
        {
            decimal dcMnTotal = 0;
            //dcMnTotal = dtVentaDetalleResume.AsEnumerable().Where(p => p.Field<int>("cantidad") > 0).Sum(r => r.Field<decimal>("mnPrecioVenta"));
            dcMnTotal = dtVentaDetalleResume.AsEnumerable().Sum(r => r.Field<decimal>("mnPrecioVenta"));
            //dcMnTotal = decimal.Parse(dtVentaDetalleResume.Compute("SUM(mnPrecioVenta)", "cantidad > 1").ToString());
            return dcMnTotal;

        }

        public decimal getMontoTotalParcial(DataTable dtVentaDetalleResume, string strSession)
        {
            decimal dcMnTotal = 0;
            dcMnTotal = dtVentaDetalleResume.AsEnumerable().Where(p => p.Field<int>("cantidad") > 0).Sum(r => r.Field<decimal>("mnPrecioVenta"));
            //dcMnTotal = dtVentaDetalleResume.AsEnumerable().Sum(r => r.Field<decimal>("mnPrecioVenta"));
            //dcMnTotal = decimal.Parse(dtVentaDetalleResume.Compute("SUM(mnPrecioVenta)", "cantidad > 1").ToString());
            return dcMnTotal;
        }

        protected void rblComproPago_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strTipoTarjeta = rblComproPago.SelectedValue.ToString();
                rvTipoTarjeta.Enabled = false;
                rvBanco.Enabled = false;
                rvNombreTH.Enabled = false;
                rvNoTarjeta.Enabled = false;
                rvAnioExpira.Enabled = false;
                rvCVV.Enabled = false;
                rblTipoTarjeta.ClearSelection();
                divModTarjeta.Visible = false;
                hdCnCompropago.Value = "1";
            }
            catch (Exception ex)
            { }
        }

        protected void btnWorldPay_Click(object sender, EventArgs e)
        {
            try
            {
                int respActualizaCompetidor = 0;
                int intIdCliente = cGlobals.idUsuario;
                decimal dcTotal = decimal.Parse(hdToTalPagar.Value);
                string[] selectedValueMoneda = ddlCambioMoneda.SelectedValue.Split('-');
                string dsIsoMoneda = selectedValueMoneda[1].ToString();
                string idMoneda = selectedValueMoneda[0].ToString();
                int intRespIdVenta = int.Parse(hdIdVenta.Value.ToString());
                string[] strIdVentas = hdIV.Value.Split(',');

                if (hddResultadoPagoWorldPay.Value.Trim().Length > 0)
                {
                    String[] strParametros = hddResultadoPagoWorldPay.Value.Split('|');
                    //strParametros[0] = "APROBADA";
                    if (strParametros[0] != "APROBADA")
                    {
                        lblRespuestaPago.Text = "Declinado. " + strParametros[0] + " " + strParametros[1];
                    }
                    else
                    {
                        respActualizaCompetidor = ActualizarCompetidor(intIdCliente, dcTotal, idMoneda, intRespIdVenta);
                        DataTable dtCompetidor = getDatatableCompetidor();
                        int intIdClienteContactoEvento = 0;
                        int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);

                        if (respActualizaCompetidor > 0)
                        {
                            string strIdClienteContactoCompetidorEvento = dtCompetidor.Rows[0]["idClienteContactoEvento"].ToString();
                            //strIdClienteDetalle = dtCompetidor.Rows[i]["idClienteDetalle"].ToString();

                            wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                            wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                            //clienteContactoEventoClient.Open();
                            cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(strIdClienteContactoCompetidorEvento), getDsSession());
                            clienteContactoEventoClient.Close();
                            // si todo pasó bien ahora enviamos correo al competidor
                            bool respCorreoConfirmacion = correoPago(hdIV.Value, "", "1", hdCorreoCompetidor.Value, cCompetidorPagar.DtCompetidor, cCompetidor);
                            //cParticipante.dtCompedidor = null;
                            Session["dtCompetidorPago" + cnIdContactoEvento.Value] = null;
                            cCompetidorPagar.DtCompetidor = null;
                            string idVentasDetalles = hdIV.Value.Remove(hdIV.Value.Length - 1);
                            try
                            {
                                Response.Redirect("respPago.aspx?token=" + idVentasDetalles + "&contEvent=" + intIdClienteContactoEvento.ToString());
                            }
                            catch (ThreadAbortException ex1)
                            {
                                // do nothing
                            }
                        }
                        else
                        {
                            lblRespuestaPago.Text = "Pago realizado con cargo a su tarjeta, pero ocurrió error al actualizar al competidor. <br/> Favor de contactarnos a infotriatlon@experienciasxcaret.com.mx";
                        }
                    }                                                         
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                string strginnermessage = " btnWorldPay_Click ";
                string strCuerpoCorreo = msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                string strExcepcion = "";
                if (ex.ToString().Length > 799)
                    strExcepcion = ex.ToString().Substring(0, 999);
                else
                    strExcepcion = ex.ToString();
                strCuerpoCorreo += strExcepcion;
                MensajeAlert(strCuerpoCorreo);
            }
        }
    }
}