﻿/*$.validator.addMethod(
    "regex",
    function (value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    },
    "Forma"
);*/

$(document).ready(function () {
     $('.classTxtRfc').change(function () {
         //alert($(this).val());
         
         var dataIn = '{' + '"rfc":"' + $(this).val() + '"}';
         $.ajax({
             url: "../ajax/ajaxAgregarCliente.svc/getRFC",
             type: "POST",
             //async: false,
             contentType: "application/json; charset=utf-8",
             data: dataIn,
             dataType: "json",
             success: function (data) {
                 var object = JSON.parse(data.d);
                 //console.log(object);
                 //console.log(object.Error);
                 // si todo ok chido
                 if (object.Error == '0') {
                     if (object.Response != '0') {
                         $('#lblMensajeRespuesta').css('color', '#B94A48');
                         $("#lblMensajeRespuesta").text("El RFC ingresado ya existe en el sistema.");
                         $("#txtRfc").focus();
                         $("#hdValidaRFC").val("1");
                         alert("El RFC ingresado ya existe en el sistema.");
                     }
                     else
                     {
                         $('#lblMensajeRespuesta').css('color', 'black');
                         $("#lblMensajeRespuesta").text("");
                         $("#hdValidaRFC").val("0");
                     }
                 }// si no
                 else {
                     $("#lblMensajeRespuesta").text(object.Response);
                 }
             },
             error: function (error) {
                 //alert("Error: " + error.Error);
                 $('#lblMensajeValidacion').text(error.Error);
             }
         });
     });

     $('body').on('click', '#btnAgregarCliente', function (event) {
         //$("#btnTerminar").click(function (event) {
         //alert('lp1');  
         window["pnlinfoproceso"].Show();
         setTimeout(function () { $('#formAgregarCliente').submit(); }, 1000);
         //event.preventDefault();
         //$('#formAgregarCliente').submit();
     });

     $('#formAgregarCliente').submit(function (e) {
         //alert('lp');
         //pnlinfoproceso.Show();
         //e.preventDefault();
        }).validate({   
        /*rules: {
            txtRfc: {
                required: true ,
                //change regexp to suit your needs
                regx: /^[a-zA-Z\&]{3,4}([\d]{2}(0[1-9]|1[0-2])([0][1-9]|[12][0-9]|3[01]))((\D|\d){3})$/               
            }
        },*/
        submitHandler: function (boton) {
            //alert('validate');            
            var lblMensaje = document.getElementsByName("lblMensaje");
            var id = lblMensaje[0].getAttribute('id');
            
            $('#' + id).text('');
            $('#' + id).css('color', 'black');
            var email = $('.classEmail1').val().trim();
            var email2 = $('.classEmail2').val().trim();
            var data = $("#formAgregarCliente").serialize()
            //data = JSON.stringify(data);

            var nombre = $('#txtNombre').val().trim();
            var paterno = $('#txtApellidoPaterno').val().trim();
            var materno = $('#txtApellidoMaterno').val().trim();
            var rfc = $('#txtRfc').val().trim();
            var razonSocial = $('#txtRazonSocial').val().trim();
            var razonComercial = $('#txtRazonComercial').val().trim();
            var telefono = $('#txtTelefono').val().trim();
            //var email = $('#email').val().trim();
            var pais = $('.classDdlPais option:selected').val().trim();
            var estado = $('#ddlEstado option:selected').val().trim();

            var calle = $('#txtCalle').val().trim();
            var colonia = $('#txtColonia').val().trim();
            var ciudad = $('#txtCiudad').val().trim();
            var interior = $('#txtNoInterior').val().trim();
            var exterior = $('#txtNoExterior').val().trim();
            var codigoPostal = $('#txtCodigoPostal').val().trim();
            //+ '"nombre":"' + nombre 
            var dataIN = '{' + '"nombre":"' + nombre + '"paterno":"' + paterno + '"materno":"' + materno + '"rfc":"' + rfc +
                               '"razonSocial":"' + razonSocial + '"razonComercial":"' + razonComercial +
                               '"telefono":"' + telefono + '"email":"' + email + '"pais":"' + pais + '"estado":"' + estado +
                               '"calle":"' + calle + '"colonia":"' + colonia + '"ciudad":"' + ciudad + '"interior":"' + interior +
                               '"exterior":"' + exterior + '"codigoPostal":"' + codigoPostal + '"}';
            var parametros2 = JSON.stringify({
                "nombre": nombre,
                "paterno": paterno,
                "materno": materno,
                "rfc": rfc,
                "razonSocial": razonSocial,
                "razonComercial": razonComercial,
                "telefono": telefono,
                "email": email,
                "pais": pais,
                "estado": estado,
                "calle": calle,
                "colonia": colonia,
                "ciudad": ciudad,
                "interior": interior,
                "exterior": exterior,
                "codigoPostal": codigoPostal
            });
            
            //console.log(data);
            //console.log(dataIN);
            // si el input hdValidaRFC tiene valor de cero quiere decir paso la validación y no existe en el sistema
            
            if ($("#hdValidaRFC").val() == "0") {
                // ahora comparamos de que los 2 correos sean iguales
                if (email.toUpperCase() == email2.toUpperCase()) {
                    //alert('iguales');
                    /* $.post("/ajax/ajaxAgregarCliente.svc/agregarCliente",
                         dataIN,
                         function (dataB)
                         {
                             alert("cots");
                         })
                          .fail(function () {
                              alert("error");
                          })*/                    
                    $.ajax({
                        url: "../ajax/ajaxAgregarCliente.svc/agregarCliente",
                        type: "POST",
                        //async: false,
                        contentType: "application/json; charset=utf-8",
                        data: parametros2,
                        dataType: "json",
                        async: false,
                        beforeSend: window["pnlinfoproceso"].Show(),
                        success: function (data) {
                            var object = JSON.parse(data.d);
                            //console.log(object);
                            //console.log(object.Error);
                            // si todo ok chido
                            if (object.Error == '0') {                               
                                $("#lblMensajeRespuesta").text("El cliente ya fue dado de alta con estatus inactivo, ingresos lo revisará y activirá");
                                pnlinfoproceso.Hide();
                            }// si no
                            else {
                                
                                $("#lblMensajeRespuesta").text(object.Response);
                                pnlinfoproceso.Hide();
                            }
                        },
                        error: function (error) {
                            //alert("Error: " + error.Error);
                            $('#lblMensajeValidacion').text(error.Error);
                            pnlinfoproceso.Hide();
                            return false;
                        }
                    });
                }
                else {
                    //console.log(lblMensaje);
                    //lblMensaje.InnerHTML = 'Los correos ingresados no coincides favor de validarlos';
                    $('#' + id).text('Los correos ingresados no coinciden, favor de validarlos');
                    $('#' + id).css('color', '#B94A48');
                    $('.classEmail2').focus();
                    pnlinfoproceso.Hide();
                    return false;
                }
            }// no paso la validacion, el rfc ya existe en el sistema
            else {
                $('#' + id).text('El RFC ingresado ya existe en el sistema, favor de revisarlo');
                $('#' + id).css('color', '#B94A48');
                $("#txtRfc").focus();
                $('html, body').animate({ scrollTop: $("#txtRfc").offset().top }, 500);
                pnlinfoproceso.Hide();
                return false;
            }
            pnlinfoproceso.Hide();
            return false;
        },
        errorPlacement: function (error, element) {
            ///alert('validate');
            error.appendTo(element.parent().append());
            pnlinfoproceso.Hide();
        }             
    });
});