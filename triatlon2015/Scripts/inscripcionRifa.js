﻿$(document).ready(function () {

    // detectar que botón se esta accionando
    $('body').on('click', '#cont_formularios a', function (elemento) {
        elemento.preventDefault();

        mostrar = $(this).attr('id');
        //console.log(mostrar);
        // Seleccionar la sección a mostrar
        if (mostrar == 'back_paso_a') {
            //alert('back');
            $('#form_paso_b').removeClass("show");
            $('#form_paso_a').removeClass("hide");
            $('#form_paso_a').addClass("show");
            $('#form_paso_b').addClass("hide");
            if ($("#paso").length) {
                //...
                //alert("si existe #paso")
                $('#paso').addClass("c1");
                $('#paso').removeClass("c2");
                $('#paso1').css("color", "#fff");
                $('#paso2').css("color", "#000000");
            }
        }
        else if (mostrar == 'back_paso_b') {
            $('#form_paso_c').removeClass("show");
            $('#form_paso_b').removeClass("hide");
            $('#form_paso_b').addClass("show");
            $('#form_paso_c').addClass("hide");
            if ($("#paso").length) {
                $('#paso').removeClass("c3");
                $('#paso').addClass("c2");
                $('#paso2').css("color", "#fff");
                $('#paso3').css("color", "#000000");
            }
        }
        else if (mostrar == 'go_terminar') {
            var dataForm = $('#form_a, #form_b').serialize();
            alert(dataForm);
        }
    });

    /*$("#back_paso_a").on("click",function () {
        alert('back');
        $('#form_paso_b').removeClass("show");
        $('#form_paso_a').removeClass("hide");
        $('#form_paso_a').addClass("show");
        $('#form_paso_b').addClass("hide");
    });*/
    $("#btnContinuarA").click(function (event) {
        event.preventDefault();
        $('#formRifa').submit();
        //$(this).trigger('click');
         //do stuff here
    });

    $('body').on('click', '#btnTerminar', function (event) {
    //$("#btnTerminar").click(function (event) {
        event.preventDefault();
        $('#btnTerminar').attr('disabled', true);
        $('#formRifa').submit();
    });

    $('body').on('click', '#btnContinuarB', function (elemento) {
        //$("#btnContinuarA").click(function (event) {
        //alert("hola");
        event.preventDefault();
        $('#formRifa').submit();
        //$(this).trigger('click');
        //do stuff here
    });

    $('body').on('click', '.btnPagarFinal', function (elemento) {
        //$('.btnPagarFinal').click(function () {
        //alert("btnPagarFinal");
        elemento.preventDefault();
        $('#formCambioCategoria').submit();
        //__doPostBack('btnPagarFinal', 'btnPagarFinal_Click');
    });

    $('body').on('click', '#lnkFolio', function (elemento) {
        //$('.btnPagarFinal').click(function () {
        //alert("btnPagarFinal");
        var link = $(this).attr("href");
        window.open(link, "_blank", "comma,delimited,list,of,window,features");
        
        //__doPostBack('btnPagarFinal', 'btnPagarFinal_Click');
    });

    $('body').on('click', '#lnkFolio2', function (elemento) {
        //$('.btnPagarFinal').click(function () {
        //alert("btnPagarFinal");
        var link = $(this).attr("href");
        window.open(link, "_blank", "comma,delimited,list,of,window,features");
        //__doPostBack('btnPagarFinal', 'btnPagarFinal_Click');
    });

    $('#formCambioCategoria').submit(function (e) {
        e.preventDefault();
    }).validate({
        submitHandler: function (boton) {
            __doPostBack('btnPagarFinal', 'btnPagarFinal_Click');
        },
        errorPlacement: function (error, element) {            
            error.appendTo(element.parent().append());
        }
    });

    $("#formRifa").submit(function(e) {
        e.preventDefault();
    }).validate({
    //('#formRifa').validate({        
        submitHandler: function(boton) {                        
            if ($("#form_paso_a").is(":visible") == true) {
                //alert('paso validacion');                
                //$("#ddlCategoria option[text=Olimpico]").attr("selected", "selected");  
                /*request = $.ajax({
                url: "forms.php",
                type: "post",
                data: serializedData
                });*/
                    
                var email = $('#txtEmail').val().trim();                
                var email2 = $('#txtEmail2').val().trim();
                var apellidoP = $('#txtApellidoPaterno').val().trim();
                var apellidoM = $('#txtApellidoMaterno').val().trim();
                var feNacimiento = $("#ddanio option:selected").val().trim() + '-' + $("#ddlMes option:selected").val().trim() + '-' + $("#ddldia option:selected").val().trim();
                var feNacimientoTexto = $("#ddldia option:selected").val().trim() + ' de ' + $("#ddlMes option:selected").text().trim() + ' de ' + $("#ddanio option:selected").val().trim();
                //console.log('apellidoP ' + apellidoP.toUpperCase() + ' apellidoM : ' + apellidoM.toUpperCase() + ' feNacimiento ' + feNacimiento.toUpperCase());
                if (email != 'infotriatlon@experienciasxcaret.com' && email != 'nfigueroa@experienciasxcaret.com' && email != 'hspech@gmail.com' && email != 'cgonzalezm86@aol.com' && email != 'rcarrillo@experienciasxcaret.com.mx' && email != 'lherrera@experienciasxcaret.com.mx' && email != 'cgonzalezm@experienciasxcaret.com.mx' && email != "faraujo@experienciasxcaret.com.mx" && email != 'lrecinos@experienciasxcaret.com.mx') {
                    if (email.toUpperCase() == email2.toUpperCase()) {
                        $('#lblValidacionEmail').text('');
                        //var dataIn = '{' + '"apellidoPaterno":"' + apellidoP + '"' + '"apellidoMaterno":"' + apellidoM + '"feNacimiento":"' + feNacimiento + '" }';
                        var dataIn = JSON.stringify({
                            "apellidoPaterno": apellidoP.toUpperCase(),
                            "apellidoMaterno": apellidoM.toUpperCase(),
                            "feNacimiento": feNacimiento                           
                        });
                        //console.log('antes del post');
                        $.ajax({
                            url: "ajax/ajaxRifa.svc/validaCompetidorRifaApellidosYFeNacimiento",
                            type: "POST",
                            //async: false,
                            contentType: "application/json; charset=utf-8",
                            data: dataIn,
                            dataType: "json",
                            success: function (data) {

                                var object = JSON.parse(data.d);
                                //console.log(object);
                                if (object.Error == '0') {
                                    //console.log(object.Response);
                                    // si pasó la validacion de existendia del correo ingresado nos vamos a la siguiente sección
                                    if (object.Response == 'True') {

                                        var dataIn = '{' + '"strEmail":"' + email + '"}';
                                        //console.log('antes del post');
                                        $.ajax({
                                            url: "ajax/ajaxRifa.svc/validaCompetidorRifa",
                                            type: "POST",
                                            //async: false,
                                            contentType: "application/json; charset=utf-8",
                                            data: dataIn,
                                            dataType: "json",
                                            success: function (data) {

                                                var object = JSON.parse(data.d);
                                                //console.log(object);
                                                if (object.Error == '0') {
                                                    //console.log(object.Response);
                                                    // si pasó la validacion de existendia del correo ingresado nos vamos a la siguiente sección
                                                    if (object.Response == 'True') {
                                                        if ($("#paso").length) {
                                                            $('#paso').removeClass("c1");
                                                            $('#paso').addClass("c2");
                                                            $('#paso1').css("color", "#000000"); // negro
                                                            $('#paso2').css("color", "#fff"); // banco
                                                            $('#paso3').css("color", "#000000");
                                                        }
                                                        $('#hdCnValidation').val("1");
                                                        $('#lblMensajeValidacion').text('');
                                                        $('#form_paso_a').removeClass("show");
                                                        $('#form_paso_a').addClass("hide");
                                                        $('#form_paso_b').addClass("show");
                                                        //// parte para el cambio dinamico de la barra de navegación de pasos                                        
                                                        ////
                                                        //pnlinfoproceso.Hide();
                                                        __doPostBack('btnContinuarA', 'btnContinuarA_Click')
                                                        return false;
                                                    }
                                                    else {
                                                        $('#lblMensajeValidacion').text('Ya existe registro con el correo ' + email);
                                                        pnlinfoproceso.Hide();
                                                        return false;
                                                    }
                                                    //Alert the persons name                                
                                                }
                                            },
                                            error: function (error) {
                                                //alert("Error: " + error.Error);
                                                $('#lblMensajeValidacion').text(error.Error);
                                                return false;
                                            }
                                        });                                        
//                                        __doPostBack('btnContinuarA', 'btnContinuarA_Click')
                                    }
                                    else {
                                        $('#lblMensajeValidacion').text('Ya existe registro con el apellido ' + apellidoP.toUpperCase() + ' ' + apellidoM.toUpperCase() + ' y fecha de nacimiento ' + feNacimientoTexto + '.');
                                        $('#hdCnValidation').val("0");
                                        pnlinfoproceso.Hide();
                                        return false;
                                    }
                                    //Alert the persons name                                
                                }
                            },
                            error: function (error) {
                                //alert("Error: " + error.Error);
                                $('#lblMensajeValidacion').text(error.Error);
                                $('#hdCnValidation').val("0");
                                return false;
                            }
                        });
                    }
                    else {
                        $('#lblValidacionEmail').text('Los correos ingresados no coinciden favor de validarlos');
                        $('#txtEmail2').focus();
                        $('#hdCnValidation').val("0");
                        pnlinfoproceso.Hide();
                        return false;
                    }
                }
                else {
                    /*
                    if (object.Response == 'True') {
                                        if ($("#paso").length) {                                            
                                            $('#paso').removeClass("c1");
                                            $('#paso').addClass("c2");
                                            $('#paso1').css("color", "#000000"); // negro
                                            $('#paso2').css("color", "#fff"); // banco
                                            $('#paso3').css("color", "#000000");
                                        }
                    */
                    if ($("#paso").length){
                        //...
                        //alert("si existe #paso")
                        $('#paso').removeClass("c1");
                        $('#paso').addClass("c2");
                        //$('.paso').addClass("c3");
                        $('#paso1').css("color", "#000000"); // negro
                        $('#paso2').css("color", "#fff"); // banco
                        $('#paso3').css("color", "#000000");
                    }
                    $('#hdCnValidation').val("1");
                    $('#lblMensajeValidacion').text('');
                    $('#form_paso_a').removeClass("show");
                    $('#form_paso_a').addClass("hide");
                    $('#form_paso_b').addClass("show");                    
                    __doPostBack('btnContinuarA', 'btnContinuarA_Click');
                    return false;
                }
                    //btnContinuarA.DoClick();
                    //__doPostBack('btnContinuarA', 'OnClick');                                                           
            }    
            
            if ($("#form_paso_b").is(":visible") == true) {
                //alert('paso validacion 2');
                if ($("#paso").length) {                
                    $('#paso').removeClass("c2");
                    $('#paso').addClass("c3");
                    $('#paso2').css("color", "#000000");
                    $('#paso3').css("color", "#fff");
                }
                $('#form_paso_b').removeClass("show");
                $('#form_paso_b').addClass("hide");
                $('#form_paso_c').addClass("show");
                return false;
            }            
            registrarCompetidor();            
        },        
        errorPlacement: function (error, element) {
            $('#btnTerminar').attr('disabled', false);
            error.appendTo(element.parent().append());            
        }
    });   

   /* $('#formRifa').validate({
        submitHandler: function () {
                alert('paso validacion 2');
            $('#form_paso_b').hide();
            $('#form_paso_c').addClass("show");
            return false;
        },
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().append());
        }
    });*/
     //$('body').on('click', '#divMs div', function (elemento) {
        $('#divMsg').on( "click",function () {
        //$('#divMsg').click(function () {
        //alert("hola");
        $(this).animate({ bottom: -$(this).outerHeight() }, 500);
        setTimeout(function () {
            // Add to document using html, rather than tmpContainer
            $(this).remove();
        }, 500); // 0 milliseconds

    });
});

function responseRifa(responseMesagge) {
    
    var elem = responseMesagge.split('-');
    console.log(elem);
    $('#lblMensaje').text(elem[0] + elem[1]);
    var type = "errorMsg";
    $('#btnTerminar').attr('disabled', true);
    //$('#back_paso_b').attr('disabled', true);
    if (elem[0].trim() == "1") {
        type = "errorMsg";
        $('#back_paso_b').attr('disabled', true);
        $('#btnTerminar').attr('disabled', true);
        $('#ddlCategoria').attr('disabled', true);
        alert(elem[1]);
        //showMessage(type, elem[1], elem[1]);
    }
    else if (elem[0].trim() == "2") {
        type = "errorMsg";
        $('#back_paso_b').attr('disabled', true);
        $('#btnTerminar').attr('disabled', true);
        $('#ddlCategoria').attr('disabled', true);
        alert(elem[1]);
        showMessage(type, elem[1], elem[1]);
    }
    else if (elem[0].trim() == "3") {
        type = "errorMsg";
        //$('#back_paso_b').attr('disabled', true);
        $('#btnTerminar').attr('disabled', true);
        $('#ddlCategoria').attr('disabled', true);
        alert(elem[1]);
        //showMessage(type, elem[1], elem[1]);
    }
    else {
        window.location = elem[2].trim();
    }
}

function mostrarOcultarDiv(divMostrar, divOcultar1, divOcultar2)
{
    console.log("mostrarOcultarDiv " + divMostrar + " " + divOcultar1 + " " + divOcultar2)
    /*$('#form_paso_b').removeClass("show");
    $('#form_paso_b').addClass("hide");
    $('#form_paso_c').addClass("show");
    */
    $('#' + divMostrar).removeClass("show");
    $('#' + divOcultar1).removeClass("show");
    $('#' + divOcultar2).removeClass("show");

    $('#' + divMostrar).removeClass("hide");
    $('#' + divOcultar1).removeClass("hide");
    $('#' + divOcultar2).removeClass("hide");

    $('#' + divMostrar).addClass("show");
    $('#' + divOcultar1).addClass("hide");
    $('#' + divOcultar2).addClass("hide");
}

var myMessages = ['info', 'warning', 'error', 'success']; // Mensajes definidos	 
function hideAllMessages() {
    var messagesHeights = new Array();

    for (i = 0; i < myMessages.length; i++) {
        messagesHeights[i] = $('.' + myMessages[i]).outerHeight();
        $('.' + myMessages[i]).css('bottom', -messagesHeights[i]); //mover el div fuera de la vista	  
    }
}

function showMessage(type, info, message) {
    //alert(message);
    //console.log(message);
    var dv = document.getElementById('divMsg'); // Busca el Div
    var lblInfo = document.getElementById('lblInfo'); // Busca el label de info
    var lblMessage = document.getElementById('lblMensajeException'); // Busca el label de mensaje
    $("#MessageError").html("");
    if (lblInfo != null) {
        lblInfo.innerHTML = info;
        lblMessage.innerHTML = message;

        dv.className = type + ' message'; // Asigna el Css del tipo de mensaje
    }
    else {
        var cuerpoHtml = '<div id="Message" class="scrollable">' +
                        '<div  id="divMsg" class="' + type + ' message" >' +
		                        '<table style="width:100%">' +
                                    '<tr>' +
                                        '<td style="width:85%">' +
                                        '<h3><asp:Label runat="server" ID="lblInfo" Text="' + info + '">' + info + '</asp:Label></h3>' +
                                        '</td>' +
                                        '<td style="width:15%" rowspan="2">' +
                                            '<asp:Image ID="imgok" runat="server" ImageAlign="AbsMiddle" ImageUrl="styles/icon-behavior-yes.png" style="display :block;" CssClass="arrowhand" /> ' +
                                        '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td style="width:85%">' +
                                            '<p><asp:Label runat="server" ID="lblMensajeException" Text="' + message + '">' + message + '</asp:Label></p>' +
                                        '</td>' +
                                    '</tr>' +
                                '</table>' +
                            '</div>' +
                        '</div>';
        $("body").append(cuerpoHtml);;
    }

    hideAllMessages();
    $('.' + type).animate({ bottom: "0" }, 500);
}

function pagaCambioCategoria()
{
    alert("hola");
    event.preventDefault();
}

function showMessageAndHide(type, info, message) {
    //alert(message);
    console.log("showMessageAndHide "+message);
    var dv = document.getElementById('divMsg'); // Busca el Div
    var lblInfo = document.getElementById('lblInfo'); // Busca el label de info
    var lblMessage = document.getElementById('lblMensajeException'); // Busca el label de mensaje
    $("#MessageError").html("");
    if (lblInfo != null) {
        lblInfo.innerHTML = info;
        lblMessage.innerHTML = message;

        dv.className = type + ' message'; // Asigna el Css del tipo de mensaje
    }
    else {
        var cuerpoHtml = '<div id="Message" class="scrollable">' +
                        '<div  id="divMsg" class="' + type + ' message" >' +
		                        '<table style="width:100%">' +
                                    '<tr>' +
                                        '<td style="width:85%">' +
                                        '<h3><asp:Label runat="server" ID="lblInfo" Text="' + info + '">' + info + '</asp:Label></h3>' +
                                        '</td>' +
                                        '<td style="width:15%" rowspan="2">' +
                                            '<asp:Image ID="imgok" runat="server" ImageAlign="AbsMiddle" ImageUrl="styles/icon-behavior-yes.png" style="display :block;" CssClass="arrowhand" /> ' +
                                        '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td style="width:85%">' +
                                            '<p><asp:Label runat="server" ID="lblMensajeException" Text="' + message + '">' + message + '</asp:Label></p>' +
                                        '</td>' +
                                    '</tr>' +
                                '</table>' +
                            '</div>' +
                        '</div>';
        $("body").append(cuerpoHtml);;
    }

    hideAllMessages();
    $('.' + type).animate({ bottom: "0" }, 500);
    $('#form_paso_b').removeClass("show");
    $('#form_paso_a').removeClass("hide");
    $('#form_paso_a').addClass("show");
    $('#form_paso_b').addClass("hide");
}