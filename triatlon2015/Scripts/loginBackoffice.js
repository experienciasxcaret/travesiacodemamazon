﻿$(document).ready(function () {
    $('#loginForm').validate({
        submitHandler: function (boton) {
            return false
        },
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().append());
            //error.css('background-color', 'gray');
            //error.css('left', offset.left + element.outerWidth() + 5);
            //error.css('top', offset.top - 3);
        }
    });

    $('#divMsg').click(function () {
        $(this).animate({ bottom: -$(this).outerHeight() }, 500);
    });
});

var myMessages = ['info', 'warning', 'error', 'success']; // Mensajes definidos	 
function hideAllMessages() {
    var messagesHeights = new Array();

    for (i = 0; i < myMessages.length; i++) {
        messagesHeights[i] = $('.' + myMessages[i]).outerHeight();
        $('.' + myMessages[i]).css('bottom', -messagesHeights[i]); //mover el div fuera de la vista	  
    }
}

function showMessage(type, info, message) {
    //alert(message);
    var dv = document.getElementById('divMsg'); // Busca el Div
    var lblInfo = document.getElementById('lblInfo'); // Busca el label de info
    var lblMessage = document.getElementById('lblMensaje'); // Busca el label de mensaje
    lblInfo.innerHTML = info;
    lblMessage.innerHTML = message;

    dv.className = type + ' message'; // Asigna el Css del tipo de mensaje

    hideAllMessages();
    $('.' + type).animate({ bottom: "0" }, 500);
}