﻿$(document).ready(function () {

    // detectar que botón se esta accionando
    $('body').on('click', '#cont_formularios a', function (elemento) {
        elemento.preventDefault();

        mostrar = $(this).attr('id');
        //console.log(mostrar);
        // Seleccionar la sección a mostrar
        if (mostrar == 'back_paso_a') {
            //alert('back');
            $('#form_paso_b').removeClass("show");
            $('#form_paso_a').removeClass("hide");
            $('#form_paso_a').addClass("show");
            $('#form_paso_b').addClass("hide");
        }
        else if (mostrar == 'back_paso_b') {
            $('#form_paso_c').removeClass("show");
            $('#form_paso_b').removeClass("hide");
            $('#form_paso_b').addClass("show");
            $('#form_paso_c').addClass("hide");
        }
        else if (mostrar == 'go_terminar') {
            var dataForm = $('#form_a, #form_b').serialize();
            alert(dataForm);
        }
    });

    /*$("#back_paso_a").on("click",function () {
        alert('back');
        $('#form_paso_b').removeClass("show");
        $('#form_paso_a').removeClass("hide");
        $('#form_paso_a').addClass("show");
        $('#form_paso_b').addClass("hide");
    });*/
    $("#btnContinuarA").click(function (event) {
        event.preventDefault();
        $('#formRifa').submit();
        //$(this).trigger('click');
        //do stuff here
    });

   /* $('body').on('click', '#btnPagarFinal', function (event) {
        //$("#btnTerminar").click(function (event) {
        event.preventDefault();
        $('#formRifa').submit();
    });*/

    $("#formRifa").submit(function (e) {
        e.preventDefault();
    }).validate({
        //('#formRifa').validate({        
        submitHandler: function (boton) {
            //btnPagarFinal.DoClick();
            //__doPostBack('btnPagarFinal', 'OnClick');
            //$('#btnPagarFinal').click();
        },
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().append());
        }
    });

    /* $('#formRifa').validate({
         submitHandler: function () {
                 alert('paso validacion 2');
             $('#form_paso_b').hide();
             $('#form_paso_c').addClass("show");
             return false;
         },
         errorPlacement: function (error, element) {
             error.appendTo(element.parent().append());
         }
     });*/
    //$('body').on('click', '#divMs div', function (elemento) {
    $('#divMsg').on("click", function () {
        //$('#divMsg').click(function () {
        //alert("hola");
        $(this).animate({ bottom: -$(this).outerHeight() }, 500);
        setTimeout(function () {
            // Add to document using html, rather than tmpContainer
            $(this).remove();
        }, 500); // 0 milliseconds

    });
});

function responseRifa(responseMesagge) {

    var elem = responseMesagge.split('/');
    console.log(elem);
    $('#lblMensaje').text(elem[1]);
    var type = "errorMsg";
    $('#btnTerminar').attr('disabled', true);
    $('#back_paso_b').attr('disabled', true);
    if (elem[0].trim() == "1") {
        type = "errorMsg";
        $('#back_paso_b').attr('disabled', true);
        $('#btnTerminar').attr('disabled', true);
        $('#ddlCategoria').attr('disabled', true);
        alert(elem[1]);
        //showMessage(type, elem[1], elem[1]);
    }
    if (elem[0].trim() == "2") {
        type = "errorMsg";
        $('#back_paso_b').attr('disabled', true);
        $('#btnTerminar').attr('disabled', true);
        $('#ddlCategoria').attr('disabled', true);
        alert(elem[1]);
        showMessage(type, elem[1], elem[1]);
    }
}

function mostrarOcultarDiv(divMostrar, divOcultar1, divOcultar2) {
    console.log("mostrarOcultarDiv " + divMostrar + " " + divOcultar1 + " " + divOcultar2)
    /*$('#form_paso_b').removeClass("show");
    $('#form_paso_b').addClass("hide");
    $('#form_paso_c').addClass("show");
    */
    $('#' + divMostrar).removeClass("show");
    $('#' + divOcultar1).removeClass("show");
    $('#' + divOcultar2).removeClass("show");

    $('#' + divMostrar).removeClass("hide");
    $('#' + divOcultar1).removeClass("hide");
    $('#' + divOcultar2).removeClass("hide");

    $('#' + divMostrar).addClass("show");
    $('#' + divOcultar1).addClass("hide");
    $('#' + divOcultar2).addClass("hide");
}

var myMessages = ['info', 'warning', 'error', 'success']; // Mensajes definidos	 
function hideAllMessages() {
    var messagesHeights = new Array();

    for (i = 0; i < myMessages.length; i++) {
        messagesHeights[i] = $('.' + myMessages[i]).outerHeight();
        $('.' + myMessages[i]).css('bottom', -messagesHeights[i]); //mover el div fuera de la vista	  
    }
}

function showMessage(type, info, message) {
    //alert(message);
    //console.log(message);
    var dv = document.getElementById('divMsg'); // Busca el Div
    var lblInfo = document.getElementById('lblInfo'); // Busca el label de info
    var lblMessage = document.getElementById('lblMensajeException'); // Busca el label de mensaje
    $("#MessageError").html("");
    if (lblInfo != null) {
        lblInfo.innerHTML = info;
        lblMessage.innerHTML = message;

        dv.className = type + ' message'; // Asigna el Css del tipo de mensaje
    }
    else {
        var cuerpoHtml = '<div id="Message" class="scrollable">' +
                        '<div  id="divMsg" class="' + type + ' message" >' +
		                        '<table style="width:100%">' +
                                    '<tr>' +
                                        '<td style="width:85%">' +
                                        '<h3><asp:Label runat="server" ID="lblInfo" Text="' + info + '">' + info + '</asp:Label></h3>' +
                                        '</td>' +
                                        '<td style="width:15%" rowspan="2">' +
                                            '<asp:Image ID="imgok" runat="server" ImageAlign="AbsMiddle" ImageUrl="styles/icon-behavior-yes.png" style="display :block;" CssClass="arrowhand" /> ' +
                                        '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td style="width:85%">' +
                                            '<p><asp:Label runat="server" ID="lblMensajeException" Text="' + message + '">' + message + '</asp:Label></p>' +
                                        '</td>' +
                                    '</tr>' +
                                '</table>' +
                            '</div>' +
                        '</div>';
        $("body").append(cuerpoHtml);;
    }

    hideAllMessages();
    $('.' + type).animate({ bottom: "0" }, 500);
}

function showMessageAndHide(type, info, message) {
    //alert(message);
    console.log("showMessageAndHide " + message);
    var dv = document.getElementById('divMsg'); // Busca el Div
    var lblInfo = document.getElementById('lblInfo'); // Busca el label de info
    var lblMessage = document.getElementById('lblMensajeException'); // Busca el label de mensaje
    $("#MessageError").html("");
    if (lblInfo != null) {
        lblInfo.innerHTML = info;
        lblMessage.innerHTML = message;

        dv.className = type + ' message'; // Asigna el Css del tipo de mensaje
    }
    else {
        var cuerpoHtml = '<div id="Message" class="scrollable">' +
                        '<div  id="divMsg" class="' + type + ' message" >' +
		                        '<table style="width:100%">' +
                                    '<tr>' +
                                        '<td style="width:85%">' +
                                        '<h3><asp:Label runat="server" ID="lblInfo" Text="' + info + '">' + info + '</asp:Label></h3>' +
                                        '</td>' +
                                        '<td style="width:15%" rowspan="2">' +
                                            '<asp:Image ID="imgok" runat="server" ImageAlign="AbsMiddle" ImageUrl="styles/icon-behavior-yes.png" style="display :block;" CssClass="arrowhand" /> ' +
                                        '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td style="width:85%">' +
                                            '<p><asp:Label runat="server" ID="lblMensajeException" Text="' + message + '">' + message + '</asp:Label></p>' +
                                        '</td>' +
                                    '</tr>' +
                                '</table>' +
                            '</div>' +
                        '</div>';
        $("body").append(cuerpoHtml);;
    }

    hideAllMessages();
    $('.' + type).animate({ bottom: "0" }, 500);
    $('#form_paso_b').removeClass("show");
    $('#form_paso_a').removeClass("hide");
    $('#form_paso_a').addClass("show");
    $('#form_paso_b').addClass("hide");
}

function showPopUP(msg) {
    $("#lblMensaje").text(msg);
    $(".dvPopUpOver").show();
}

function reloadPage(msg) {
    alert(msg);
    document.location.reload(true);
}