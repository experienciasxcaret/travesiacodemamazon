﻿function construyeTablaDetalle(datatable, cnCena) {
    //alert(cnCena);
    //console.log(datatable);
    //var dataDetalle = JSON.parse(datatable);
    //console.log(dataDetalle);
    var contador = 0;
    
    var divTitulo = "<div class='text-center'>" +
                        "<h3>Detalle</h3>" +
                   "</div>";

    var headerTabla = "<thead>"+
                            "<tr>"+
                                "<th class='col-xs-5 col-md-8 descripcion'>Producto</th>" +
                                "<th  class='col-xs-1 col-md-1 cantidad'>cantidad</th>" +
                                "<th class='col-xs-1 col-md-1 text-center'>Precio</th>" +
                                "<th class='col-xs-1 col-md-1 text-center'>Total $</th>" +
                            "</tr>"+
                        "</thead>";
    var tablaBody = "<tbody>";
    var total = 0;
    total = parseFloat(total);
    
    $.each(datatable, function (index, obj) {
        var mnPrecio_ = "mnPrecio_";       
        console.log(contador);
        mnPrecio_ += obj.idMonedaPagar;
        if (obj.idProducto != 305) {
            // vamos a validar si lo que viene es un remo, preguntar si quiere agregar/quitar remo
            if (obj.idProducto == 170) {
                tablaBody += "<tr>" +
                           "<td class='col-xs-5 col-md-8 descripcion'><em>" + obj.dsDescripcion + "<br>¿ Desea realizar una donación al Programa de Protección y Conservación de Tortugas Marinas encabezado por Flora, Fauna y Cultura de México, A.C.?</em></td>" +
                           "<td class='col-xs-1 col-md-1 cantidad' style='text-align: center'> " +
                               "<div class='col-sm-1'>" +
                                  // "<input type='text' class='form-control input-small inputsCantidad' idProducto='" + obj.idProducto + "' id='txtCantidad" + obj.idProducto + "' name='txtNombre' value='" + obj.cantidad + "'>" +
                               "</div>" +
                           "</td>" +
                           "<td class='col-xs-1 col-md-1 text-center'>" +
                           "</td>" +
                           "<td class='col-xs-1 col-md-1 text-center donativo'>" +
                                "<select id='" + obj.idProducto + "' class='form-control ddlDonativo' name='ddlDonativo'>" +
                                    "<option value='0'>0</option>" +                                    
                                    "<option value='100.00'>100</option>" +
                                    "<option value='500.00'>500</option>" +
                                    "<option value='1000.00'>1000</option>" +
                                "</select>" +
                           "</td>" +
                           "</tr>";
                //total += obj.cantidad * obj.mnPrecioVenta;
                total += parseFloat((obj.mnPrecioVenta).toFixed(3));
            }
            else {

                if ((obj.idProducto == 1706)) {
                    contador += 1;
                    if (contador == 1) {
                        tablaBody += "<tr id='trAgregarCena' >" +
                                 "<td class='col-xs-5 col-md-8'><button class='text-center btnMostrarRemo' idProducto='1706' idTipoCliente='"+obj.idTipoCliente+"' style='color: black; font:bold' name='btnMostrarRemo'>Agregar Remo</button></td>" +
                                 "<td class='col-xs-3 col-md-3' style='text-align: center'></td>" +
                                 "</tr>";
                    }
                    // si ya está visible dejarle que se siga viendo
                    if ($('.trRemo').is(':visible')) {
                        // si es menor
                        if (obj.idTipoCliente == 2) {
                            tablaBody += "<tr id='trRemo' class='trRemo' name='trRemo' style=''>" +
                               "<td class='col-xs-5 col-md-8 descripcionRemo'><em>" + obj.dsDescripcion + " (" + obj.dsTipoCliente + " 5-11)</em></td>" +
                               "<td class='col-xs-1 col-md-1 CantidadRemo' style='text-align: center'> " +
                                   "";
                        }
                        else {
                            tablaBody += "<tr id='trRemo' class='trRemo' name='trRemo' style=''>" +
                                "<td class='col-xs-5 col-md-8 descripcionRemo'><em>" + obj.dsDescripcion + "</em></td>" +
                                "<td class='col-xs-1 col-md-1 cantidadRemo' style='text-align: center'> " +
                                    "";
                        }
                    }
                    else {
                        // si es menor
                        if (obj.idTipoCliente == 2) {
                            tablaBody += "<tr id='trRemo' class='trRemo' name='trRemo' style='display:none'>" +
                                   "<td class='col-xs-5 col-md-8 descripcionRemo'><em>" + obj.dsDescripcion + "</em></td>" +
                                   "<td class='col-xs-1 col-md-1 CantidadRemo' style='text-align: center'> " +
                                       "";
                        }
                        else {
                            tablaBody += "<tr id='trRemo' class='trRemo' name='trRemo' style='display:none'>" +
                                   "<td class='col-xs-5 col-md-8 descripcionRemo'><em>" + obj.dsDescripcion + "</em></td>" +
                                   "<td class='col-xs-1 col-md-1 CantidadRemo' style='text-align: center'> " +
                                       "";
                        }
                    }
                }
                else {// aqui pinta producto principal
                    tablaBody += "<tr id='trCena' style=''>" +
                            "<td class='col-xs-5 col-md-8 descipcion'><em>" + obj.dsDescripcion + "</em></td>" +
                            "<td class='col-xs-1 col-md-1 cantidad' style='text-align: center'> " +
                                "";
                }
                               
                tablaBody += obj.cantidad;

                var mnPrecio = "";
                var dsClaveMoneda = "";
                var mnPrecio_;
                var dsClaveMoneda_;
                var idMonedaPagar = obj.idMonedaPagar;
               // console.log("idMonedaPagar " + idMonedaPagar);
                switch (idMonedaPagar) {
                    case 1:
                        mnPrecio_ = obj.mnPrecio_1;
                        dsClaveMoneda_ = obj.ClaveMoneda_1;
                        break;
                    case 2:
                        mnPrecio_ = obj.mnPrecio_2;
                        dsClaveMoneda_ = obj.ClaveMoneda_2;
                        break;
                    case 3:
                        mnPrecio_ = obj.mnPrecio_3
                        dsClaveMoneda_ = obj.ClaveMoneda_3;
                        break;
                    case 4:
                        mnPrecio_ = obj.mnPrecio_4;
                        dsClaveMoneda_ = obj.ClaveMoneda_4;
                        break;
                    default:
                        console.log("case default");
                        break;
                }

                //console.log("mnPrecio_ms" + obj.mnPrecio_MS);

                if (obj.mnPrecio_MS != "" && obj.mnPrecio_MS != null)
                    mnPrecio = obj.mnPrecio_MS;
                else
                    mnPrecio = mnPrecio_;

                if (obj.ClaveMoneda_MS != "" && obj.ClaveMoneda_MS != null) 
                    dsClaveMoneda = obj.ClaveMoneda_MS;
                else
                    dsClaveMoneda = dsClaveMoneda_;

                tablaBody += "" +
                            "</td>" +
                            "<td class='col-md-1 text-center precioUnitario'>" + accounting.formatMoney(mnPrecio) + " " + dsClaveMoneda + "</td>" +
                            "<td class='col-md-1 text-center precioTotal'>" + accounting.formatMoney(obj.mnPrecioVenta) + " " + dsClaveMoneda + "</td>" +
                            "</tr>";
                //total += obj.cantidad * obj.mnPrecioVenta;
                total += parseFloat((obj.mnPrecioVenta).toFixed(3)) * obj.cantidad;
            }
        }
        console.log(total);
        console.log("obj.mnPrecioVenta " + obj.mnPrecioVenta);
    });
    total = Number(Math.round(total + "e+2") + "e-2");
    tablaBody += "<tr>"+
                    "<td>   </td>"+
                    "<td>   </td>"+
                    "<td class='text-right'><h4><strong>Total: </strong></h4></td>"+
                    "<td class='text-center text-danger'><h4><strong>" +
                        //"<asp:Label ID='lbltotal' runat='server' Text=" + total + "></asp:Label>" +
                        "<label id=lbltotal>" + accounting.formatMoney(total) + "</label>" +
                     "</strong></h4></td>"+
                "</tr>"+
                "</tbody>";
    
    console.log("total final " + total)
    $("#tblDetalleCompra").html("");
    $("#hdToTalPagar").val(total);

    $("#tblDetalleCompra").append(headerTabla + tablaBody);
    if (total <= 0) {
        $("#divModTarjeta").hide();
        $("#divComproPago").hide();
        $("#divRadioFormasPago").hide();
        document.getElementById("rvTipoTarjeta").enabled = false;
        document.getElementById("rvFormaPago").enabled = false;
        document.getElementById("rvBanco").enabled = false;
        document.getElementById("rvNombreTH").enabled = false;
        document.getElementById("rvNoTarjeta").enabled = false;
        document.getElementById("rvMesExpira").enabled = false;
        document.getElementById("rvAnioExpira").enabled = false;
        document.getElementById("rvCVV").enabled = false;
        btnPagarName.SetText("Finalizar");
        //$("#panel1DatosTarjetas").hide();
    }
    else {
        $("#divModTarjeta").show();
        $("#divModTarjeta").attr("visible", true);
        $("#divComproPago").show();
        $("#divComproPago").attr("display", "block");
        //document.getElementById("divComproPago").style.display = 'block';
        $("#divRadioFormasPago").show();
        $("#divRadioFormasPago").attr("visible", true);
        /*if ($('#rvTipoTarjeta').length > 0)
            document.getElementById("rvTipoTarjeta").enabled = true;*/
        if ($('#rvFormaPago').length > 0)
            document.getElementById("rvFormaPago").enabled = true;
        if ($('#rvBanco').length > 0)
            document.getElementById("rvBanco").enabled = true;
        if ($('#rvNombreTH').length > 0)
            document.getElementById("rvNombreTH").enabled = true;
        if ($('#rvNoTarjeta').length > 0)
            document.getElementById("rvNoTarjeta").enabled = true;
        if ($('#rvMesExpira').length > 0)
            document.getElementById("rvMesExpira").enabled = true;
        if ($('#rvAnioExpira').length > 0)
            document.getElementById("rvAnioExpira").enabled = true;
        if ($('#rvCVV').length > 0)
            document.getElementById("rvCVV").enabled = true;
        //document.getElementById("btnPagarFinal").innerText = 'Pagar';
        btnPagarName.SetText("Pagar");
        //$("#panel1DatosTarjetas").show();
    }
}

function actualizaCombo(cnInternational, dcTipoCambioNuevo, dcTipoCambioPesoDolar, idTipoMoneda)
{
    console.log(cnInternational);
    console.log(dcTipoCambioNuevo);
    console.log(dcTipoCambioPesoDolar);
    //$(".ddlDonativo option[value=" + cantidad + "]").attr("selected", true);
    var numValueCombo = 0.0;
    var nuevoValueCombo = 0.0;
    if (idTipoMoneda != 2)
    {
        $(".ddlDonativo option").each(function () {
            // Add $(this).val() to your list
            //console.log($(this).text() + "valor : " + $(this).attr('value'));
            numValueCombo = $(this).attr('value');
            console.log(numValueCombo);
            //        nuevoValueCombo = Math.floor( ((numValueCombo / dcTipoCambioPesoDolar) * dcTipoCambioNuevo) * 100) / 100;
            if (idTipoMoneda != "9" && idTipoMoneda != "16") {
                nuevoValueCombo = (Number(Math.round((numValueCombo / dcTipoCambioPesoDolar) + 'e2') + 'e-2') * dcTipoCambioNuevo);
                nuevoValueCombo = Number(Math.round(nuevoValueCombo + 'e2') + 'e-2');
            }
            else {
                nuevoValueCombo = Math.ceil(Number(Math.round((numValueCombo / dcTipoCambioPesoDolar) + 'e2') + 'e-2') * dcTipoCambioNuevo);
            }
            //console.log(nuevoValueCombo);
            $(this).text(nuevoValueCombo);
            $(this).attr('value', nuevoValueCombo);
        });
    }
}

$(document).ready(function () {       
    $('body').on('change', '.inputsCantidad', function (elemento) {
        //alert($(this).attr('idProducto'));
        var idProducto = $(this).attr("idProducto");
        var idTipoCliente = $(this).attr("idTipoCliente");
        var cantidad = $(this).val();
        var idClienteContactoEvento = $("#cnIdContactoEvento").val();
        var dataIn = JSON.stringify({
            "idProducto": idProducto,
            "caso": "cantidad",
            "valor": cantidad,
            "idClienteContactoEvento": idClienteContactoEvento,
            "idTipoCliente": idTipoCliente
        });
        pnlinfoproceso.Show();
        $.ajax({
            url: "ajax/ajaxDetallePago.svc/recalcular",
            type: "POST",
            //async: false,
            contentType: "application/json; charset=utf-8",
            data: dataIn,
            dataType: "json",
            success: function (data) {
                var object = JSON.parse(data.d);
                //console.log(object);
                //console.log(object.Error);
                // si todo ok chido
                if (object.Error == '0') {
                    $("#lbltotal").text(object.Response);
                    $("#hdToTalPagar").val(object.Response);
                    construyeTablaDetalle(jQuery.parseJSON(object.Response2), 1);
                    pnlinfoproceso.Hide();
                }// si no
                else {
                    $("#lbltotal").text(object.Response);
                    $("#hdToTalPagar").val("0");
                    pnlinfoproceso.Hide();
                }
            },
            error: function (error) {
                //alert("Error: " + error.Error);
                $('#lbltotal').text(error.Error);
                $("#hdToTalPagar").val("0")
                pnlinfoproceso.Hide();
            }
        });
        pnlinfoproceso.Hide();
    });

    //$(".ddlDonativo ").change(function () {
    $('body').on('change', '.ddlDonativo', function (elemento) {
        var idProducto = $(this).attr("id");
        var cantidad = $(this).val();
        //console.log(cantidad);
        var idClienteContactoEvento = $("#cnIdContactoEvento").val();
        var dataIn = JSON.stringify({
            "idProducto": idProducto,
            "caso": "donativo",
            "valor": cantidad,
            "idClienteContactoEvento": idClienteContactoEvento
        });
        pnlinfoproceso.Show();
        $.ajax({
            url: "ajax/ajaxDetallePago.svc/recalcular",
            type: "POST",
            //async: false,
            contentType: "application/json; charset=utf-8",
            data: dataIn,
            dataType: "json",
            success: function (data) {
                var object = JSON.parse(data.d);
                //console.log(object);
                //console.log(object.Error);
                // si todo ok chido
                if (object.Error == '0') {
                    $("#lbltotal").text(object.Response);
                    $("#hdToTalPagar").val(object.Response);
                    console.log(object.Response);
                    //construyeTablaDetalle(jQuery.parseJSON(object.Response2), 1);
                    $(".ddlDonativo option[value=" + cantidad + "]").attr("selected", true);
                    pnlinfoproceso.Hide();
                }// si no
                else {
                    document.location.reload(true);
                    $("#lbltotal").text(object.Response);
                    pnlinfoproceso.Hide();
                }
            },
            error: function (error) {
                //alert("Error: " + error.Error);
                $('#lbltotal').text(error.Error);
                pnlinfoproceso.Hide();
            }
        });
        pnlinfoproceso.Hide();
    });

    $("#btnContinuar").click(function () {
        $("#dvSecPago").removeClass("hide");
        $("#dvSecPago").addClass("show");
        $('html, body').animate({ scrollTop: $(this).offset().top }, 500);

    });

    $('body').on('click', '#cancel', function (elemento) {
        $(".dvGlobalOver").hide();
    });

    $('body').on('click', '.btnMostrarRemo', function (elemento) {
        pnlinfoproceso.Show();
        elemento.preventDefault();
        var total = 0;
        
        var cantidad = 0;
        if ($('.trRemo').is(':visible')) {            
            cantidad = 0;           
        }
        else {
            cantidad = 1;            
        }

        // aqui voy a meter el recalculo por el remo
        var idProducto = $(this).attr("idProducto");        
        var idTipoCliente = $(this).attr("idTipoCliente");       
        //console.log(cantidad);
        var idClienteContactoEvento = $("#cnIdContactoEvento").val();
        var dataIn = JSON.stringify({
            "idProducto": idProducto,
            "caso": "cantidad",
            "valor": cantidad,
            "idClienteContactoEvento": idClienteContactoEvento,
            "idTipoCliente": idTipoCliente
        });
        pnlinfoproceso.Show();
        $.ajax({
            url: "ajax/ajaxDetallePago.svc/recalcular",
            type: "POST",
            async: false,
            contentType: "application/json; charset=utf-8",
            data: dataIn,
            dataType: "json",
            success: function (data) {
                var object = JSON.parse(data.d);
                //console.log(object);
                //console.log(object.Error);
                // si todo ok chido
                if (object.Error == '0') {
                    $("#lbltotal").text(object.Response);
                    $("#hdToTalPagar").val(object.Response);
                    console.log(object.Response);
                    construyeTablaDetalle(jQuery.parseJSON(object.Response2), 1);
                    total = parseFloat( $("#hdToTalPagar").val());
                    /*if (total <= 0) {
                        $("#divModTarjeta").hide();
                        document.getElementById("rvTipoTarjeta").enabled = false;
                        document.getElementById("rvFormaPago").enabled = false;
                        document.getElementById("rvBanco").enabled = false;
                        document.getElementById("rvNombreTH").enabled = false;
                        document.getElementById("rvNoTarjeta").enabled = false;
                        document.getElementById("rvMesExpira").enabled = false;
                        document.getElementById("rvAnioExpira").enabled = false;
                        document.getElementById("rvCVV").enabled = false;
                        //$("#panel1DatosTarjetas").hide();
                    }
                    else {
                        $("#divModTarjeta").show();
                        document.getElementById("rvTipoTarjeta").enabled = true;
                        document.getElementById("rvFormaPago").enabled = true;
                        document.getElementById("rvBanco").enabled = true;
                        document.getElementById("rvNombreTH").enabled = true;
                        document.getElementById("rvNoTarjeta").enabled = true;
                        document.getElementById("rvMesExpira").enabled = true;
                        document.getElementById("rvAnioExpira").enabled = true;
                        document.getElementById("rvCVV").enabled = true;
                        //$("#panel1DatosTarjetas").show();
                    }*/
                    //$(".ddlDonativo option[value=" + cantidad + "]").attr("selected", true);
                    pnlinfoproceso.Hide();
                }// si no
                else {
                    if (object.Response == 'DATATABLE NULL')
                        alert("Se terminó su session, se recargará la pagina para reanudar su proceso")
                    document.location.reload(true);
                    $("#lbltotal").text(object.Response);
                    pnlinfoproceso.Hide();
                }
            },
            error: function (error) {
                //alert("Error: " + error.Error);
                $('#lbltotal').text(error.Error);
                pnlinfoproceso.Hide();
            }
        });
        //ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>javascript:construyeTablaDetalle(" + jsonDetalleUpgrade + ", 1);</script>");
        pnlinfoproceso.Hide();
        if ($('.trRemo').is(':visible')) {
            $(".trRemo").hide("fast");
            $('.btnMostrarRemo').text('Agregar Remo');
        }
        else {
            $(".trRemo").show("slow");
            $('.btnMostrarRemo').text('Quitar Remo');
        }
        
    });
    $('body').on('change', "input[name$='rblComproPago']", function (elemento) {
        if ($(this).is(':checked'))
        {
            alert('<img src="img/Warning.png" width="50" height="50">&nbspLa opción de pago por deposito es unica por canoero e intransferible<br><p style="text-align: center;">Guardar su comprobante de pago para cualquier duda o aclaración.<p><br>');
        }       
    });
   // $('#ifrGlobalCollect').load(function () {
        $('body').on('load', '#ifrGlobalCollect', function (elemento) {
        if ($('#ifrGlobalCollect').contents().find("input[type='submit']") != undefined) {
            $('#ifrGlobalCollect').contents().find("input[type='submit']").attr("style", "background-color: #5cb85c;border-color : #5cb85c;")
        }
    });
});    

function showGlobal() {
    console.log("showg");

    $(".dvGlobalOver").show();
}
function closeGlobal() {
    $(".dvGlobalOver").hide();
}

function fnResultadoGlobalCollect(paremtrosGlobalCollect) {
    try {
        //alert(paremtrosGlobalCollect);
        console.log("fnResultadoGlobalCollect " + paremtrosGlobalCollect);
        //var control = $('[id$=_hddResultadoPagoGlobalCollect]').attr('id');
        //var btnGlobalCollect = $('[id$=_btnGlobalCollect]').attr('id');
        //console.log(control);
        var btnGlobalCollectOject = document.getElementById(btnGlobalCollect);
        //console.log(btnGlobalCollectOject);
        $('#hddResultadoPagoGlobalCollect').val(paremtrosGlobalCollect);
        //$('#' + btnGlobalCollect).click();
        btnGlobalCollect.DoClick();
        //btnGlobalCollect.DoClick();
    }
    catch (excptn) {
        console.log(excptn);
        //mandaExcepcion('fnResultadoGlobalCollect - AltaCotizacion<br>', excptn);
    }
}

function showPopUPCompropago() {

    $(".dvPopUpOverComproPago").show();
    //$("html, body").animate({ scrollTop: 2 }, "slow");
    $('html, body').animate({
        scrollTop: $(".dvPopUpOverComproPago").offset().top
    }, 2000);
    return false;
}

function fnResultadoWorldPay(paremtrosWorldPay) {
    try {

        var control = $('[id$=_hddResultadoPagoWorldPay]').attr('id');
        //console.log(paremtrosWorldPay);
        $('#hddResultadoPagoWorldPay').val(paremtrosWorldPay);
        $(".dvGlobalOver").hide();
        btnWorldPay.DoClick();
    }
    catch (excptn) {
        console.log(excptn);
    }
}