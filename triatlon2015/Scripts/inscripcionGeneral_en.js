﻿$(document).ready(function () {

    // detectar que botón se esta accionando
    $('body').on('click', '#cont_formularios a', function (elemento) {
        elemento.preventDefault();

        mostrar = $(this).attr('id');
        //console.log(mostrar);
        // Seleccionar la sección a mostrar
        if (mostrar == 'back_paso_a') {
            //alert('back');
            $('#form_paso_b').removeClass("show");
            $('#form_paso_a').removeClass("hide");
            $('#form_paso_a').addClass("show");
            $('#form_paso_b').addClass("hide");
            if ($("#paso").length) {
                //...
                //alert("si existe #paso")
                $('#paso').addClass("c1");
                $('#paso').removeClass("c2");
                $('#paso1').css("color", "#fff");
                $('#paso2').css("color", "#000000");
            }
            
        }
        else if (mostrar == 'back_paso_b') {
            $('#form_paso_c').removeClass("show");
            $('#form_paso_b').removeClass("hide");
            $('#form_paso_b').addClass("show");
            $('#form_paso_c').addClass("hide");
            if ($("#paso").length) {
                $('#paso').removeClass("c3");
                $('#paso').addClass("c2");
                $('#paso2').css("color", "#fff");
                $('#paso3').css("color", "#000000");
            }
        }
        else if (mostrar == 'go_terminar') {
            var dataForm = $('#form_a, #form_b').serialize();
            alert(dataForm);
        }
    });
    //$(".Corredor").click(function () {    
    //$("input[type=checkbox] .Corredor").click(function(){
    $('body').on('click', '.Corredor', function (elemento) {
        //elemento.preventDefault();
        mostrar = $(this).attr('id');
        var contadorCheked = 0;
       // console.log($(this));
        console.log(mostrar);
        //var checkboxPulsado = $(this).find('input:checkbox');
        //console.log("chek" + checkboxPulsado.id);

        $('.Corredor').each(function () {
            //$(this).//this is the checked checkbox
            //console.log($(this).attr('id'));
            if ($(this).is(":checked")) {
                contadorCheked = contadorCheked + 1;
                if (parseInt(contadorCheked) > 1) {
                    alert("Already have selected runner");
                    elemento.preventDefault()
                }
            }            
        });

       /* if (parseInt(contadorCheked) > 1) {            
            alert("mas de uno " + contadorCheked)
        }
        else {
            alert("solo uno " + contadorCheked);
        }*/


        /*if ($("#" + mostrar).is(':checked')) {
            alert('Se hizo check en el checkbox.');
        } else {
            alert('Se destildo el checkbox');
        }*/
    });

    $('body').on('click', '.Nadador', function (elemento) {        
        mostrar = $(this).attr('id');
        var contadorCheked = 0;        
        console.log(mostrar);
        $('.Nadador').each(function () {            
            if ($(this).is(":checked")) {
                contadorCheked = contadorCheked + 1;
                if (parseInt(contadorCheked) > 1) {
                    alert("Already have selected swimmer");
                    elemento.preventDefault()
                }
            }
        });        
    });

    $('body').on('click', '.Ciclista', function (elemento) {        
        mostrar = $(this).attr('id');
        var contadorCheked = 0;        
        console.log(mostrar);
        $('.Ciclista').each(function () {
            //$(this).//this is the checked checkbox
            //console.log($(this).attr('id'));
            if ($(this).is(":checked")) {
                contadorCheked = contadorCheked + 1;
                if (parseInt(contadorCheked) > 1) {
                    alert("Already have selected cyclist");
                    elemento.preventDefault()
                }
            }
        });
    });

    /*$("#back_paso_a").on("click",function () {
        alert('back');
        $('#form_paso_b').removeClass("show");
        $('#form_paso_a').removeClass("hide");
        $('#form_paso_a').addClass("show");
        $('#form_paso_b').addClass("hide");
    });*/

    $("#formRifa").submit(function (e) {
       // alert('lp');
        e.preventDefault();
    }).validate({
    //$('#formRifa').validate({
        submitHandler: function(boton) {                        

            if ($("#form_paso_a").is(":visible") == true) {
                //alert('paso validacion');                
                //$("#ddlCategoria option[text=Olimpico]").attr("selected", "selected");    
                

                /*request = $.ajax({
                url: "forms.php",
                type: "post",
                data: serializedData
                });*/
                var cnPrecargado = $("#cnIdContactoPrecargado").val().trim();
                var email = $('#txtGralEmail').val().trim();                
                var email2 = $('#txtGralEmail2').val().trim();
                //console.log('correo1 ' + email.toUpperCase() + ' correo2 : ' + email2.toUpperCase());
                // si cnPrecargado tiene distinto de cero quiere decir que tiene un idContactoEvento y no se valida los correos
                if (cnPrecargado == "0" && email != 'infotriatlon@experienciasxcaret.com.mx' && email != 'nfigueroa@experienciasxcaret.com.mx' && email != 'hspech@gmail.com' && email != 'cgonzalezm86@aol.com' && email != 'rcarrillo@experienciasxcaret.com.mx' && email != 'lherrera@experienciasxcaret.com.mx' && email != 'cgonzalezm@experienciasxcaret.com.mx' && email != "faraujo@experienciasxcaret.com" && email != 'lrecinos@experienciasxcaret.com.mx') {
                    if (email.toUpperCase() == email2.toUpperCase()) {
                        
                        $('#lblValidacionEmail').text('');
                        var dataIn = '{' + '"strEmail":"' + email + '"}';
                        //console.log('antes del post');
                        $.ajax({
                            url: "ajax/ajaxRifa.svc/validaCompetidorRifa",
                            type: "POST",
                            //async: false,
                            contentType: "application/json; charset=utf-8",
                            data: dataIn,
                            dataType: "json",
                            success: function (data) {

                                var object = JSON.parse(data.d);
                                //console.log(object);
                                if (object.Error == '0') {
                                    //console.log(object.Response);
                                    // si pasó la validacion de existendia del correo ingresado nos vamos a la siguiente sección
                                    if (object.Response == 'True') {
                                        if ($("#paso").length) {                                           
                                            //alert("si existe #paso")
                                            $('#paso').removeClass("c1");
                                            $('#paso').addClass("c2");
                                            $('#paso1').css("color", "#000000"); // negro
                                            $('#paso2').css("color", "#fff");
                                            $('#paso3').css("color", "#000000");
                                        }
                                        $('#lblMensajeValidacion').text('');
                                        $('#form_paso_a').removeClass("show");
                                        $('#form_paso_a').addClass("hide");//#
                                        $('#form_paso_b').addClass("show");
                                        //// parte para el cambio dinamico de la barra de navegación de pasos                                        
                                        ////
                                        pnlinfoproceso.Hide();
                                        __doPostBack('btnContinuarA', 'btnContinuarA_Click');
                                        return false;
                                    }
                                    else {
                                        $('#lblMensajeValidacion').text('Exist registration with email ' + email);
                                        pnlinfoproceso.Hide();
                                        return false;
                                    }

                                    //Alert the persons name                                
                                }
                            },
                            error: function (error) {
                                //alert("Error: " + error.Error);
                                $('#lblMensajeValidacion').text(error.Error);
                                return false;
                            }
                        });
                    }
                    else {
                        $('#lblValidacionEmail').text('Mails entered do not match. please validate them');
                        $('#txtGralEmail2').focus();
                        pnlinfoproceso.Hide();                        
                    }
                }
                else {
                    //var btn = document.getElementById("btnContinuarA");
                    //console.log(btn);                    
                    //btnContinuarA.DoClick();          
                    if ($("#paso").length) {
                        //... $('#paso').removeClass("c1");
                        /*;if ($("#paso").length) {                                           
                                            //alert("si existe #paso")
                                            $('#paso').removeClass("c1");
                                            $('#paso').addClass("c2");
                                            $('#paso1').css("color", "#000000"); // negro
                                            $('#paso2').css("color", "#fff");
                                            $('#paso3').css("color", "#000000");
                                        }
                        $('#paso1').css("color", "#000000"); // negro
                        $('#paso2').css("color", "#fff");
                        $('#paso3').css("color", "#000000");*/
                        //alert("si existe #paso")
                        $('#paso').removeClass("c1");
                        $('#paso').addClass("c2");
                       // $('.paso').addClass("c3");
                        $('#paso1').css("color", "#000000"); // negro
                        $('#paso2').css("color", "#fff");
                        $('#paso3').css("color", "#000000");
                    }
                    $('#lblMensajeValidacion').text('');
                    $('#form_paso_a').removeClass("show");
                    $('#form_paso_a').addClass("hide");
                    $('#form_paso_b').addClass("show");
                    //;
                    __doPostBack('btnContinuarA', 'btnContinuarA_Click');
                    return false;
                }
                    //btnContinuarA.DoClick();
                    //__doPostBack('btnContinuarA', 'OnClick');                                                           
            }    
            
            if ($("#form_paso_b").is(":visible") == true) {
                //alert('paso validacion 2');
                if ($("#paso").length) {
                    //...
                    //alert("si existe #paso")
                    $('#paso').removeClass("c2");
                    $('#paso').addClass("c3");
                    $('#paso2').css("color", "#000000");
                    $('#paso3').css("color", "#fff");
                }
                $('#form_paso_b').removeClass("show");
                $('#form_paso_b').addClass("hide");
                $('#form_paso_c').addClass("show");
                return false;
            }    
        },        
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().append());            
        }
    });   

   /* $('#formRifa').validate({
        submitHandler: function () {
            alert('paso validacion 2');
            $('#form_paso_b').hide();
            $('#form_paso_c').addClass("show");
            return false;
        },
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().append());
        }
    });*/    
    $('#divMsg').click(function () {
        $(this).animate({ bottom: -$(this).outerHeight() }, 500);
        setTimeout(function () {
            // Add to document using html, rather than tmpContainer
            $(this).remove();
        }, 500); // 0 milliseconds
    });

    $('body').on('click', '#btnContinuarA', function (elemento) {
    //$("#btnContinuarA").click(function (event) {
        //alert("hola");
        //event.preventDefault();        
        $('#formRifa').submit();
        //$(this).trigger('click');
        //do stuff here
    });

    $('body').on('click', '#btnContinuarB', function (elemento) {
        //$("#btnContinuarA").click(function (event) {
        //alert("hola");
        event.preventDefault();        
        $('#formRifa').submit();
        //$(this).trigger('click');
        //do stuff here
    });

    $('body').on('click', '#btnTerminar', function (event){
        //$("#btnTerminar").click(function (event) {
        //alert("prevent")
        event.preventDefault();
        $('#formRifa').submit();
    });
});

function responseRifa(responseMesagge) {
    
    var elem = responseMesagge.split('/');
    //console.log(elem);
    $('#lblMensaje').text(elem[1]);

    if (elem[0] == "1") {
        $('#back_paso_b').attr('disabled', true);
        $('#btnTerminar').attr('disabled', true);
        $('#ddlCategoria').attr('disabled', true);
    }    
}

var myMessages = ['info', 'warning', 'error', 'success']; // Mensajes definidos	 
function hideAllMessages() {
    var messagesHeights = new Array();

    for (i = 0; i < myMessages.length; i++) {
        messagesHeights[i] = $('.' + myMessages[i]).outerHeight();
        $('.' + myMessages[i]).css('bottom', -messagesHeights[i]); //mover el div fuera de la vista	  
    }
}

function showMessage(type, info, message) {
    //alert(message);
    console.log(message);
    var dv = document.getElementById('divMsg'); // Busca el Div
    var lblInfo = document.getElementById('lblInfo'); // Busca el label de info
    var lblMessage = document.getElementById('lblMensajeException'); // Busca el label de mensaje
    $("#MessageError").html("");
    if (lblInfo != null) {
        lblInfo.innerHTML = info;
        lblMessage.innerHTML = message;

        dv.className = type + ' message'; // Asigna el Css del tipo de mensaje
    }
    else {
        var cuerpoHtml = '<div id="Message" class="scrollable">'+
                        '<div  id="divMsg" class="' + type + ' message" >' +
		                        '<table style="width:100%">'+
                                    '<tr>'+
                                        '<td style="width:85%">'+
                                        '<h3><asp:Label runat="server" ID="lblInfo" Text="' + info + '">' + info + '</asp:Label></h3>' +
                                        '</td>'+
                                        '<td style="width:15%" rowspan="2">'+
                                            '<asp:Image ID="imgok" runat="server" ImageAlign="AbsMiddle" ImageUrl="styles/icon-behavior-yes.png" style="display :block;" CssClass="arrowhand" /> '+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td style="width:85%">'+
                                            '<p><asp:Label runat="server" ID="lblMensajeException" Text="' + message + '">' + message + '</asp:Label></p>' +
                                        '</td>'+
                                    '</tr>'+
                                '</table>'+
                            '</div>'+
                        '</div>';
        $("body").append(cuerpoHtml);;
    }

    hideAllMessages();
    $('.' + type).animate({ bottom: "0" }, 500);
}