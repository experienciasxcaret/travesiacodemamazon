﻿/*
Slider vertical
*/
velocidad = 2000;
tiempoEspera = 2500;
verificar = 1;
dif = 0;
timer = 0

function moverSlider(cnUltimo) {       
    // el bloque es el contenedor donde se muestran todas las notificaciones
    sliderAltura = $(".bloque-slider").height();
    // el modulo es el bloque de noticia independiente que va transitando
    moduloAltura = $(".modulo-slider").height() + parseFloat($(".modulo-slider").css("padding-top")) + parseFloat($(".modulo-slider").css("padding-bottom"));

    sliderTop = parseFloat($(".bloque-slider").css("top")); /*sliderTop defaul en css tiene cero */
    dif = sliderAltura + sliderTop;
    //console.log("moduloAltura =" + moduloAltura);
    //console.log("sliderAltura =" + sliderAltura);
    //console.log("sliderTop =" + sliderTop);
    //console.log("dif =" + dif);
    if (verificar == 1) {
        if (dif > moduloAltura) {            
            //esto hace que se vaya haciendo hacia arriba
            $(".bloque-slider").animate({ top: "-=" + moduloAltura }, velocidad);
            if (sliderAltura = dif)
               //alert("Son penultimos");
            timer = setTimeout('moverSlider(0)', tiempoEspera);
        }// cuando son iguales se setea
        else {
            //console.log("entro en else");
            clearTimeout(timer);
            //lo regresa a la posicion original                                     
            //$(".bloque-slider").animate({ top: 0 }, 500);
            //$(".bloque-slider").animate({ top: "-=30" }, velocidad);
            //timer = setTimeout('', tiempoEspera);
            $(".bloque-slider").css({ top: 0 });
            timer = setTimeout('moverSlider(1)', 500);
        }
    }
    else {
        timer = setTimeout('moverSlider(0)', 1000);
    }
}
function bajarSlider() {
    if (dif >= moduloAltura * 2) {
        $(".bloque-slider").animate({ top: "-=" + moduloAltura }, velocidad);
    }
    else {
        $(".bloque-slider").css({ top: 0 });
        $(".bloque-slider").animate({ top: "-=" + moduloAltura }, velocidad);
    }
}
function subirSlider() {
    if (sliderTop <= -moduloAltura) {
        $(".bloque-slider").animate({ top: "+=" + moduloAltura }, velocidad);
    }
    else {
        $(".bloque-slider").css({ top: -sliderAltura + moduloAltura });
        $(".bloque-slider").animate({ top: "+=" + moduloAltura }, velocidad);
    }
}