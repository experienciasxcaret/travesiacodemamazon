﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxEditors;
using System.Configuration;

namespace travesia
{
    public partial class inscripcionNovatas : System.Web.UI.Page
    {
        
        const String callbackRegistrarCompetidor = "registrarCompetidor();";
        const String callbackValidaCompetidor = "validarCompetidor();";
        DataSet result;
        cParticipante cCompetidorNovatas = new cParticipante();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {                

                Page.MaintainScrollPositionOnPostBack = true;
                int intIdClienteContactoEvento = 0;
                string strCat = "";

                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "btnContinuarA_Click")
                    btnContinuarA_Click(sender, e);
                
                if (!IsPostBack)
                {
                    int noCapacidad = 0;
                    int noUsados = 0;
                    int lugaresDisponibles = 0;
                    cCompetidorNovatas.DtCompetidor = null;
                    Session["dtCompetidorNovatas" + cnIdContactoPrecargado.Value] = null;

                    int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                    if (Request.QueryString["cat"] != null)
                        strCat = Request.QueryString["cat"];
                    hdStrCat.Value = strCat;
                    cnIdContactoPrecargado.Value = intIdClienteContactoEvento.ToString();
                    utils.Login("usrTriatlon", "eXperiencias");

                    DataTable dtCEventoModalidad = utils.getCEventoModalidad(getDsSession(),null, "TXN", "DSCLAVE");
                    noCapacidad = int.Parse(dtCEventoModalidad.Rows[0]["noCapacidad"].ToString());
                    noUsados = int.Parse(dtCEventoModalidad.Rows[0]["noUsados"].ToString());
                    lugaresDisponibles = noCapacidad - noUsados;

                    if (cnIdContactoPrecargado.Value != "")
                    {
                        noCapacidad = 2;
                        noUsados = 1;
                    }

                    if ((noCapacidad > noUsados))
                    {
                        // si trae strCat entonces quiere decir que viene de una pagina especifica como la de novatas
                        cParticipante.cnCena = 0;
                        switch (strCat.ToUpper())
                        {
                            // solo novatas
                            case "NOV":
                                ddlGralSexo.Items.FindByValue("3").Selected = true;
                                ddlGralSexo.Attributes.Add("readonly", "readonly");
                                ddlGralSexo.Attributes.Add("disabled", "disabled");
                                utils.llenaAnio(ddlGralAnio, 15);
                                break;
                            case "PES":
                                utils.llenaAnio(ddlGralAnio);
                                cParticipante.cnCena = 1;
                                break;
                            default:
                                utils.llenaAnio(ddlGralAnio, 15);
                                //utils.llenaDias(int.Parse(ddlGralAnio.SelectedValue.ToString()), int.Parse(ddlGralMes.SelectedValue.ToString()), ddlGralDia);                           
                                break;
                        }
                        utils.llenaPais(ddlGralPais);
                        utils.LlenaTipoSangre(ddlTipoSangre);
                        //utils.LlenaTipoSangre(ddlTipoSangre2);
                        //utils.LlenaTipoSangre(ddlTipoSangre3);
                        utils.llenaEstados(ddlGralEstado, ddlGralPais.SelectedValue.ToString(), getDsSession());
                        // si intIdClienteContactoEvento es mayor a cero quiere decir que viene de una precarga
                        // y hay que poner los datos ya precargados
                        if (intIdClienteContactoEvento > 0)
                        {
                            result = utils.buscarCompetidor(getDsSession(),"", "", "", 0, "", "", 0, 0, 0, intIdClienteContactoEvento);
                            if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                            {

                                cCompetidorNovatas.DtCompetidor = result.Tables[0];
                                Session["dtCompetidorNovatas" + cnIdContactoPrecargado.Value] = null; 
                                Session["dtCompetidorNovatas" + cnIdContactoPrecargado.Value] = result.Tables[0];
                                llenaFormulario(result.Tables[0]);
                            }
                        }
                        btnTerminar.JSProperties["cpIsMessage"] = "";
                    }
                    else
                    {
                        btnContinuarA.Visible = false;
                        lblMensajeCabecera.Text = "Cupo agotado para inscripción a Solo Novatas";
                        mensaje("Cupo agotado para inscripción a Solo Novatas. Gracias por participar", "CUPOS AGOTADOS");
                    }
                }                
            }
            catch (Exception ex)
            {
                mensaje("Problema en carga de pagina "+ ex.Message, "Error generado");
            }
        }

        protected void llenaFormulario(DataTable dtCompetidor)
        {
            if (dtCompetidor.Rows[0]["idConfigPagoPreregistro"] != null)
                hdIdConfigPago.Value = dtCompetidor.Rows[0]["idConfigPagoPreregistro"].ToString();
            if (dtCompetidor.Rows[0]["dsContacto"].ToString() != "")
            {
                txtGralNombre.Text = dtCompetidor.Rows[0]["dsContacto"].ToString();
                txtGralNombre.ReadOnly = true;
            }
            txtGralApellidoPaterno.Text = ((dtCompetidor.Rows[0]["dsApellidoPaterno"].ToString() != "") ? dtCompetidor.Rows[0]["dsApellidoPaterno"].ToString() : "");
            txtGralApellidoPaterno.ReadOnly = true;
            txtGralApellidoMaterno.Text = ((dtCompetidor.Rows[0]["dsApellidoMaterno"].ToString() != "") ? dtCompetidor.Rows[0]["dsApellidoMaterno"].ToString() : "");
            txtGralApellidoMaterno.ReadOnly = true;
            txtGralEmail.Text = ((dtCompetidor.Rows[0]["dsCorreoElectronico"].ToString() != "") ? dtCompetidor.Rows[0]["dsCorreoElectronico"].ToString() : "");
            txtGralEmail.ReadOnly = true;
            txtGralEmail2.Text = ((dtCompetidor.Rows[0]["dsCorreoElectronico"].ToString() != "") ? dtCompetidor.Rows[0]["dsCorreoElectronico"].ToString() : "");
            txtGralEmail2.ReadOnly = true;
            int intIdSexo = ((dtCompetidor.Rows[0]["idSexo"].ToString() != "") ? int.Parse(dtCompetidor.Rows[0]["idSexo"].ToString()) : 0);
            if (intIdSexo > 0)
            {
                ddlGralSexo.Items.FindByValue(intIdSexo.ToString()).Selected = true;
                ddlGralSexo.Attributes.Add("readonly", "readonly");
                ddlGralSexo.Attributes.Add("disabled", "disabled");
                
            }
            DateTime dtFeNacimiento;
            int anio = 0;
            int mes = 0;
            int dia = 0;
            if (dtCompetidor.Rows[0]["feNacimiento"].ToString() != "")
            {
                dtFeNacimiento = DateTime.Parse(dtCompetidor.Rows[0]["feNacimiento"].ToString());
                anio = dtFeNacimiento.Year;
                mes = dtFeNacimiento.Month;
                dia = dtFeNacimiento.Day;
                ddlGralAnio.SelectedIndex = -1;
                ddlGralAnio.Items.FindByValue(anio.ToString()).Selected = true;
                ddlGralAnio.Attributes.Add("readonly", "readonly");
                ddlGralAnio.Attributes.Add("disabled", "disabled");
                ddlGralMes.SelectedIndex = -1;
                ddlGralMes.Items.FindByValue(mes.ToString()).Selected = true;
                ddlGralMes.Attributes.Add("readonly", "readonly");
                ddlGralMes.Attributes.Add("disabled", "disabled");
                ddlGralDia.SelectedIndex = -1;
                ddlGralDia.Items.FindByValue(dia.ToString()).Selected = true;
                ddlGralDia.Attributes.Add("readonly", "readonly");
                ddlGralDia.Attributes.Add("disabled", "disabled");
            }
            int intIdPais = (dtCompetidor.Rows[0]["idPais"].ToString() != "") ? int.Parse(dtCompetidor.Rows[0]["idPais"].ToString()) : 0;
            if (intIdPais > 0)
            {
                ddlGralPais.SelectedIndex = -1;
                ddlGralPais.Items.FindByValue(intIdPais.ToString()).Selected = true;
                utils.llenaEstados(ddlGralEstado, ddlGralPais.SelectedValue.ToString(), getDsSession());
                int intIdEstado = (dtCompetidor.Rows[0]["idEstado"].ToString() != "") ? int.Parse(dtCompetidor.Rows[0]["idEstado"].ToString()) : 0;
                if (intIdEstado > 0)
                {
                    ddlGralEstado.SelectedIndex = -1;
                    ddlGralEstado.Items.FindByValue(intIdEstado.ToString()).Selected = true;
                }
            }
            
        }

        protected void ddlGralPais_TextChanged(object sender, EventArgs e)
        {
            utils.llenaEstados(ddlGralEstado, ddlGralPais.SelectedValue.ToString(), getDsSession());
        }

        public DataTable getDatatableCompetidor()
        {
            if (cCompetidorNovatas.DtCompetidor == null)
                cCompetidorNovatas.DtCompetidor = (DataTable)Session["dtCompetidorNovatas" + cnIdContactoPrecargado.Value];
            return cCompetidorNovatas.DtCompetidor;
        }

        protected void btnTerminar_Click(object sender, EventArgs e)
        {
            try
            {
                int noCapacidad = 0;
                int noUsados = 0;
                int lugaresDisponibles = 0;               
                DataTable dtCEventoModalidad = utils.getCEventoModalidad(getDsSession(),null, "TXN", "DSCLAVE");
                noCapacidad = int.Parse(dtCEventoModalidad.Rows[0]["noCapacidad"].ToString());
                noUsados = int.Parse(dtCEventoModalidad.Rows[0]["noUsados"].ToString());
                lugaresDisponibles = noCapacidad - noUsados;

                if (cnIdContactoPrecargado.Value != "")
                {
                    noCapacidad = 2;
                    noUsados = 1;
                }

                if (noCapacidad > noUsados)
                {
                    cParticipante objectParticipante = new cParticipante();
                    cParticipante objectParticipante2 = new cParticipante();
                    cParticipante objectParticipante3 = new cParticipante();
                    string[] strDsEventoClasificacion;
                    bool blActualiza = false;
                    bool blPaga = true;
                    string strIdClienteContactoEvento = "";
                    DateTime fechaEvento = new DateTime(2015, 12, 31);
                    int anio = 1900;
                    int mes = 01;
                    int dia = 01;
                    int.TryParse(ddlGralAnio.SelectedValue.ToString(), out anio);
                    int.TryParse(ddlGralMes.SelectedValue.ToString(), out mes);
                    int.TryParse(ddlGralDia.SelectedValue.ToString(), out dia);
                    anio = (anio > 0) ? anio : 1900;
                    mes = (mes > 0) ? mes : 01;
                    dia = (dia > 0) ? dia : 01;
                    DateTime fechaNacimiento = new DateTime(anio, mes, dia);
                    int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;
                    string strIdConfigPagoPreregistro = hdIdConfigPago.Value;
                    //if (ddlGralCategoria.SelectedValue.ToString() != "")              
                    // SI HAY VALOR EN LA PROPIEDAD dtCompetidor de nuestra clase cParticipante
                    // QUIERE DECIR QUE VINO INFORMACION DE UN PRECARGADO.
                    cCompetidorNovatas.DtCompetidor = getDatatableCompetidor();
                    if (cCompetidorNovatas.DtCompetidor != null)
                    {
                        strIdClienteContactoEvento = cCompetidorNovatas.DtCompetidor.Rows[0]["idClienteContactoEvento"].ToString();
                        blActualiza = true;
                    }
                    /*string loc = "";
                    Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                    loc = myUri.AbsoluteUri.ToString();
                    loc = loc.Replace(myUri.PathAndQuery.ToString(), "/detallePago.aspx");*/
                    string loc = "";
                    Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                    loc = myUri.AbsoluteUri.ToString();
                    string[] strspliturl = loc.Split('/');
                    loc = loc.Replace("/" + strspliturl[strspliturl.Length - 1], "/detallePago.aspx");


                    if (ddlGralCategoria.SelectedValue.ToString() != "")
                    {
                        #region llenaObjetoCompetidor
                        string[] strIdEventoClasificacion = ddlGralCategoria.SelectedValue.ToString().Split('-');
                        int intIdEventoClasificacion = int.Parse(strIdEventoClasificacion[0].ToString());
                        objectParticipante.nombre = txtGralNombre.Text;
                        objectParticipante.apellidoPaterno = txtGralApellidoPaterno.Text;
                        if (txtGralApellidoMaterno.Text != "")
                            objectParticipante.apellidoMaterno = txtGralApellidoMaterno.Text;
                        objectParticipante.email = txtGralEmail.Text;
                        objectParticipante.idSexo = int.Parse(ddlGralSexo.SelectedValue.ToString());
                        if (txtGralCiudad.Text != "")
                            objectParticipante.municipio = txtGralCiudad.Text.ToString();
                        if (txtGralNoInterior.Text != "")
                            objectParticipante.numInterior = (txtGralNoInterior.Text.ToString());
                        if (txtGralNoExterior.Text != "")
                            objectParticipante.numExterior = (txtGralNoExterior.Text.ToString());
                        if (txtGralCalle.Text != "")
                            objectParticipante.calle = txtGralCalle.Text.ToString();
                        if (txtGralColonia.Text != "")
                            objectParticipante.colonia = txtGralColonia.Text.ToString();
                        objectParticipante.fechaNacimiento = ddlGralAnio.SelectedValue.ToString() + "/" + ddlGralMes.SelectedValue.ToString() + "/" + ddlGralDia.SelectedValue.ToString();
                        if (ddlGralPais.SelectedValue.ToString() != "")
                        {
                            objectParticipante.idPais = int.Parse(ddlGralPais.SelectedValue.ToString());
                            objectParticipante.pais = ddlGralPais.SelectedItem.ToString();
                        }
                        if (ddlGralEstado.SelectedValue.ToString() != "")
                        {
                            objectParticipante.idEstado = int.Parse(ddlGralEstado.SelectedValue.ToString());
                            objectParticipante.estado = ddlGralEstado.SelectedItem.ToString();
                        }
                        if (txtGralTelefonofijo1.Text != "")
                            objectParticipante.telefono1 = txtGralTelefonofijo1.Text.ToString() + txtGralTelefonofijo2.Text.ToString();
                        objectParticipante.telefono2 = txtGralCelular1.Text.ToString() + txtGralCelular2.Text.ToString();
                        objectParticipante.idEventoClasificacion = intIdEventoClasificacion;
                        strDsEventoClasificacion = ddlGralCategoria.SelectedItem.ToString().Split('/');
                        objectParticipante.dsEventoClasificacion = strDsEventoClasificacion[0].ToString().Trim();
                        objectParticipante.dsCodigoEventoClasificacion = strDsEventoClasificacion[1].ToString().Trim();
                        objectParticipante.vecesParticipaciones = int.Parse(ddlGralNoParticipaciones.SelectedValue.ToString());
                        objectParticipante.idTipoMoneda = 2;
                        //objectParticipante.noRifa = utils.getNoRifa(intIdEventoClasificacion);
                        if (ddlGralTallasPlayeras.SelectedValue != null && ddlGralTallasPlayeras.SelectedValue != "")
                            objectParticipante.idTallaPlayera = int.Parse(ddlGralTallasPlayeras.SelectedValue.ToString());
                        if (ddlTipoSangre.SelectedValue != null && ddlTipoSangre.SelectedValue != "")
                            objectParticipante.idTipoSangre = int.Parse(ddlTipoSangre.SelectedValue.ToString());
                        objectParticipante.dsTallaPlayera = ddlGralTallasPlayeras.SelectedItem.ToString();
                        objectParticipante.dsFolioFmtri = txtNoFmtri.Text;
                        objectParticipante.dsAlias = txtAlias.Text;
                        objectParticipante.dsPadecimientos = txtDsPadecimientos.Text;
                        objectParticipante.idTipoSangre = int.Parse(ddlTipoSangre.SelectedValue.ToString());
                        objectParticipante.dsAgrupador = "Triatlon";
                        objectParticipante.idEstatusCliente = 16; // PreAsignado;
                        objectParticipante.edad = edad; // se agregó el 29042015
                        objectParticipante.idEventoModalidad = 6;
                        if (cbCorredor1.Checked)
                            objectParticipante.cnCarrera = 1;
                        if (cbNadador1.Checked)
                            objectParticipante.cnNatacion = 1;
                        if (cbCiclista1.Checked)
                            objectParticipante.cnCiclismo = 1;
                        // YA TENEMOS LLENA LAS PROPIEDADES DE NUESTRO OBJETO PARTICIPANTE.
                        objectParticipante.llenarLista(objectParticipante);
                        #endregion
                        // si paga quiere decir que antes de ir a la ventana de pagar, se guarda el competidor
                        // pero con estatus en proceso y sin numero y ni nada
                        if (blPaga)
                        {
                            //antes de ir a pagar tenemos que ver si es nuevo se inserta y si no es nuevo se actualiza
                            // pero el estatus original no se toca
                            #region actualiza
                            string resultadoAtualizar = "";
                            string strPrimerIdClienteContactoEvento = "";
                            int intIdVenta = 0;
                            int intNumeroCompetidor = 0;
                            if (blActualiza)
                            {
                                switch (strIdConfigPagoPreregistro)
                                {
                                    case "2":
                                        intIdVenta = fnGuardaVentaPago(0, 12);
                                        break;
                                    case "3":
                                        intIdVenta = fnGuardaVentaPago(100, 14);
                                        break;
                                    default:

                                        break;
                                }
                                wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                                wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                                clienteContactoEventoClient.Open();
                                cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(strIdClienteContactoEvento), getDsSession());
                                cCompetidor.dsAlias = (objectParticipante.dsAlias != null) ? objectParticipante.dsAlias : cCompetidor.dsAlias;
                                cCompetidor.dsFolioFmtri = (objectParticipante.dsFolioFmtri);
                                cCompetidor.dsPadecimiento = objectParticipante.dsPadecimientos;
                                cCompetidor.idEventoClasificacion = objectParticipante.idEventoClasificacion;
                                cCompetidor.idSexo = objectParticipante.idSexo;
                                cCompetidor.idTipoSangre = objectParticipante.idTipoSangre;
                                cCompetidor.noFrecuencia = objectParticipante.vecesParticipaciones;
                                cCompetidor.idTalla = objectParticipante.idTallaPlayera;
                                cCompetidor.idEventoModalidad = 6;
                                if (cCompetidor.idVenta == 0)
                                {
                                    cCompetidor.idVenta = null;
                                }
                                
                                wscClienteContacto.IcClienteContactoClient clienteContactoClient = new wscClienteContacto.IcClienteContactoClient();
                                wscClienteContacto.cClienteContacto cClienteContacto;
                                cClienteContacto = clienteContactoClient.SeleccionarcClienteContactoPorId(int.Parse(cCompetidor.idClienteContacto.ToString()), getDsSession());
                                //cClienteContacto.dsApellidoMaterno = objectParticipante.apellidoMaterno;
                                //cClienteContacto.dsApellidoPaterno = objectParticipante.apellidoPaterno;
                                cClienteContacto.dsCelular = objectParticipante.telefono2;
                                cClienteContacto.dsTelefono = objectParticipante.telefono1;
                                //cClienteContacto.dsContacto = objectParticipante.nombre;
                                //cClienteContacto.dsCorreoElectronico = objectParticipante.email;
                                cClienteContacto.feNacimiento = DateTime.Parse(objectParticipante.fechaNacimiento);
                                cClienteContacto.idEstado = objectParticipante.idEstado;
                                cClienteContacto.idPais = objectParticipante.idPais;
                                cClienteContacto.idSexo = objectParticipante.idSexo;
                                cClienteContacto.idTipoMoneda = 2;

                                if (intIdVenta > 0)
                                {
                                    // entonces tambien tenemos que mandar a actualizar su detalle con idEstatus inscrito
                                    actualizaCompetidorDetalle(int.Parse(cCompetidorNovatas.DtCompetidor.Rows[0]["idClienteDetalle"].ToString()));
                                    wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                                    // wsTransactions.IkVentaClient ikventa2 = new wsTransactions.IkVentaClient();
                                    wsTransactions.ListakVentaDetalles listaKVentaDetalles2 = new wsTransactions.ListakVentaDetalles();
                                    listaKVentaDetalles2 = ikventa.SeleccionarkVentasDetallePorIdVenta(intIdVenta, getDsSession());
                                    for (int k = 0; k < listaKVentaDetalles2.Count(); k++)
                                    {

                                        cCompetidor.idVenta = intIdVenta;                                                                                
                                       // intNumeroCompetidor = getNumeroCompetidor(objectParticipante.idEventoClasificacion.ToString());
                                        //cCompetidorGeneral.DtCompetidor.Rows[i]["noCompetidor"] = intNumeroCompetidor;
                                        cCompetidor.NoCompetidor = intNumeroCompetidor;
                                        cCompetidor.idVentaDetalle = listaKVentaDetalles2[k].idVentaDetalle;
                                        clienteContactoEventoClient.Close();
                                        clienteContactoClient.Close();
                                        resultadoAtualizar = cParticipante.actualizaCompetidor(cCompetidor, cClienteContacto, true);
                                    }

                                }// fin de if (intIdVenta > 0)
                                else
                                {
                                    clienteContactoEventoClient.Close();
                                    clienteContactoClient.Close();
                                    resultadoAtualizar = cParticipante.actualizaCompetidor(cCompetidor, cClienteContacto, true);
                                    objectParticipante.VaciarLista();
                                }
                                // si es uno es porque actualizo bien
                                if (resultadoAtualizar == "1")
                                {
                                    if (intIdVenta > 0)
                                    {
                                        bool respCorreoConfirmacion = correoPago(intIdVenta.ToString(), "", "1", cCompetidorNovatas.DtCompetidor.Rows[0]["dsCorreoElectronico"].ToString(), cCompetidorNovatas.DtCompetidor);
                                        cCompetidorNovatas.DtCompetidor = null;
                                        Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = null;
                                        Response.Redirect("respPago.aspx?token=" + intIdVenta.ToString());
                                    }
                                    else
                                    {
                                        cCompetidorNovatas.DtCompetidor = null;
                                        Session["dtCompetidorNovatas" + cnIdContactoPrecargado.Value] = null;
                                        Response.Redirect(loc + "?contEvent=" + cCompetidor.idClienteContactoEvento.ToString());
                                    }
                                }
                            }
                            #endregion
                            // sino pues guarda
                            else
                            {
                                var listResp = objectParticipante.guardarCompetidor(objectParticipante.listCompetidores, getDsSession(), getIdCliente(), getIdUsuario());
                                int respIdClienteContactoEvento = int.Parse(listResp[0].ToString());
                                objectParticipante.VaciarLista();
                                if (respIdClienteContactoEvento > 0)
                                {
                                    cCompetidorNovatas.DtCompetidor = null;
                                    Session["dtCompetidorNovatas" + cnIdContactoPrecargado.Value] = null;
                                    Response.Redirect(loc + "?contEvent=" + respIdClienteContactoEvento.ToString());
                                }
                            }
                        }
                    }
                    else
                    {//lblMensaje
                        lblMensaje.Text = "Seleccione categoría";
                        //cbRegistrar.JSProperties["cpIsMessage"] = "1 / Seleccione categoria";
                    }
                }
                else
                {
                    btnTerminar.Visible = false; 
                    lblMensajeCabecera.Text = "Cupo agotado para inscripción a Solo Novatas";
                    mensaje("Cupo agotado para inscripción a Solo Novatas. Gracias por participar", "CUPOS AGOTADOS");
                }
            }
            catch (Exception ex)
            {
                //   mensaje("Problema en carga de pagina", exx);
                //cbRegistrar.JSProperties["cpIsMessage"] = ex.ToString();
                lblMensaje.Text = ex.ToString();                
            }
        }

        protected void ddlGralMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            utils.llenaDias(int.Parse(ddlGralAnio.SelectedValue.ToString()), int.Parse(ddlGralMes.SelectedValue.ToString()), ddlGralDia);
        }

        protected void ddlGralAnio_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlGralAnio.SelectedValue.ToString() != "")
                utils.llenaDias(int.Parse(ddlGralAnio.SelectedValue.ToString()), int.Parse(ddlGralMes.SelectedValue.ToString()), ddlGralDia);
        }

        protected void btnContinuarA_Click(object sender, EventArgs e)
        {
            try
            {
                string strCat = "";
                DateTime fechaEvento = new DateTime(2015, 12, 31);
                int anio = 1900;
                int mes = 01;
                int dia = 01;
                int.TryParse(ddlGralAnio.SelectedValue.ToString(), out anio);
                int.TryParse(ddlGralMes.SelectedValue.ToString(), out mes);
                int.TryParse(ddlGralDia.SelectedValue.ToString(), out dia);

                anio = (anio > 0) ? anio : 1900;
                mes = (mes > 0) ? mes : 01;
                dia = (dia > 0) ? dia : 01;
                DateTime fechaNacimiento = new DateTime(anio, mes, dia);
                int intSexo = ((ddlGralSexo.SelectedValue) == "") ? 2 : int.Parse(ddlGralSexo.SelectedValue);
                int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;
                strCat = hdStrCat.Value;

                if (strCat != "")

                    switch (strCat.ToUpper())
                    {
                        case "NOV":
                            utils.getCategorias(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXN", 1);
                            break;
                        case "CP":
                            utils.getCategorias(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXCP", 1);
                            break;
                        case "PES":
                            utils.getCategorias(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXC", 1);
                            break;
                        default:
                            utils.getCategorias(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXN", 1);
                            break;
                    }
                else
                {
                    utils.getCategorias(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXN", 1);
                }

                ListItem crItem = null;
                crItem = ddlGralCategoria.Items.FindByText("Olimpico Relevo  Femenino - W");

                if (crItem != null)
                    ddlGralCategoria.Items.FindByText("Olimpico Relevo  Femenino - W").Enabled = true;

                crItem = ddlGralCategoria.Items.FindByText("Sprint Relevo  Femenino - SW");
                if (crItem != null)
                    ddlGralCategoria.Items.FindByText("Sprint Relevo  Femenino - SW").Enabled = true;

                crItem = ddlGralCategoria.Items.FindByText("Olimpico Relevo  Masculino - M");
                if (crItem != null)
                    ddlGralCategoria.Items.FindByText("Olimpico Relevo  Masculino - M").Enabled = true;

                crItem = ddlGralCategoria.Items.FindByText("Sprint Relevo Masculino - SM");
                if (crItem != null)
                    ddlGralCategoria.Items.FindByText("Sprint Relevo Masculino - SM").Enabled = true;
                
                // masculino
                if (intSexo == 2)
                {
                    crItem = ddlGralCategoria.Items.FindByText("Olimpico Relevo  Femenino - W");
                    if (crItem != null)
                        ddlGralCategoria.Items.FindByText("Olimpico Relevo  Femenino - W").Enabled = false;

                    crItem = ddlGralCategoria.Items.FindByText("Sprint Relevo  Femenino - SW");
                    if (crItem != null)
                        ddlGralCategoria.Items.FindByText("Sprint Relevo  Femenino - SW").Enabled = false;
                }

                if (intSexo == 3)
                {
                    crItem = ddlGralCategoria.Items.FindByText("Olimpico Relevo  Masculino - M");
                    if (crItem != null)
                        ddlGralCategoria.Items.FindByText("Olimpico Relevo  Masculino - M").Enabled = false;

                    crItem = ddlGralCategoria.Items.FindByText("Sprint Relevo Masculino - SM");
                    if (crItem != null)
                        ddlGralCategoria.Items.FindByText("Sprint Relevo Masculino - SM").Enabled = false;
                }

                //aqui vamos a preguntar si la edad es mayor de once para que se muestre el combo de tallas de playeras
                if (edad > 11)
                {
                    ddlGralTallasPlayeras.Visible = true;
                    lblTamanioPlayera.Visible = true;
                    ddlGralTallasPlayeras.Enabled = true;
                    lblSujetoDisponibilidad.Visible = true;
                    //Nos hace falta llenar el combo cuanto esten en la base las tallasx
                    utils.getTallas(ddlGralTallasPlayeras, getDsSession());
                    //lblPlayeraRelevo2.Visible = true;
                    //ddlPlayera2.Visible = true;
                    //utils.getTallas(ddlPlayera2);
                    //lblPlayeraRelevo3.Visible = true;
                    //ddlPlayera3.Visible = true;
                    //utils.getTallas(ddlPlayera3);
                }
            }
            catch (Exception ex)
            {
                mensaje("Error al momento de continuar formulario B " + ex.Message, "Error generado");
            }
        }

        protected bool correoPago(string strIdVenta, string strFolios, string strIdioma, string strCorreoCliente, DataTable dtCompedidor)
        {
            //string strParametros = "<br>Parametros pasados a la funcion correoPago: strIdVenta:" + strIdVenta + "<br>strFolios:" + strFolios + "<br>strIdioma:" + strIdioma + "<br>strCorreoCliente:" + strCorreoCliente + "<br>lblMensajeCorreo:" + lblMensajeCorreo.Text;
            int intIdClienteContacto = 0;
            try
            {
                wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();
                iGeneral.Open();
                // Textos Correos
                wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                icConfiguracion.Open();

                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto2CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto2CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto10CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto10CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto11CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto11CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto12CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto12CorreoPagoIdioma_" + strIdioma, getDsSession());
                // wscConfiguracionAplicacion.cConfiguracionAplicacion Texto13CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto13CorreoPagoIdioma_" + strIdioma, Globals.dsSession);
                wscConfiguracionAplicacion.cConfiguracionAplicacion TextoUrlCuponIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-URLCUPON_CanalVenta_27_Id_" + strIdioma, getDsSession());

                icConfiguracion.Close();
                string nombre = dtCompedidor.Rows[0]["dsContacto"].ToString() + ' ' + dtCompedidor.Rows[0]["dsApellidoPaterno"].ToString() + ' ' + dtCompedidor.Rows[0]["dsApellidoMaterno"].ToString();
                string numeroCompetidor = dtCompedidor.Rows[0]["noCompetidor"].ToString();
                string strCuerpoCorreo = "<table>";
                string strCuerpoCorreoLinksFolios = "";
                //strIdVenta = strIdVenta.Remove(strIdVenta.Length - 1);
                string[] arrStrIdVenta = strIdVenta.Split(',');

                for (int ab = 0; ab < arrStrIdVenta.Length; ab++)
                {
                    wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                    ikventa.Open();
                    wsTransactions.kVenta laVenta = ikventa.SeleccionarkVentaPorIdVenta(int.Parse(arrStrIdVenta[ab]), getDsSession());
                    strFolios += laVenta.dsClaveVenta + ",";
                    strCuerpoCorreoLinksFolios += "<a href=\"" + TextoUrlCuponIdioma.dsValor.Replace("<idVenta>", arrStrIdVenta[ab]) + "\" >" + laVenta.dsClaveVenta + "</a>" + ((arrStrIdVenta.Length == 1 || ab == (arrStrIdVenta.Length - 1)) ? "" : ",");
                }
                strFolios = strFolios.Remove(strFolios.Length - 1);
                strCuerpoCorreo += "<tr><td colspan=\"2\"> Estimado/a  <b>" + nombre + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"> Te informamos que de acuerdo a tu solicitud, hemos confirmado tu inscripción. </td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto2CorreoPagoIdioma.dsValor + " " + strFolios + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>Numero Competidor: </b>" + dtCompedidor.Rows[0]["noCompetidor"].ToString() + "</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + "Importante: Por favor lee detenidamente e imprime el documento del enlace (";
                strCuerpoCorreo += strCuerpoCorreoLinksFolios;


                //string[] arrStrFolios = strFolios.Split(',');


                strFolios = strFolios.Remove(strFolios.Length - 1);

                strCuerpoCorreo += ") y presentalo al momento de recoger tu paquete de competidor  </b></td></tr>";

                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">Si deseas una factura, por favor entra a: https://www.aolxcaret.com/core/facturacion, recuerda que tienes 24 horas para solicitar tu factura </td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                //strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto10CorreoPagoIdioma.dsValor + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto11CorreoPagoIdioma.dsValor + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">" + Texto12CorreoPagoIdioma.dsValor + "</td></tr>";

                strCuerpoCorreo += "</table>";

                string strCorreoAgente = "";

                bool blnBanderaCorreo = iGeneral.SendEmail(strCorreoCliente, "Confirmación de Inscripción Triatlón Xel-Há", utils.armaHTMLBaseCorreo("Confirmación de Inscripción Triatlón Xel-Há", strCuerpoCorreo), true /*es html*/, "", false, 0, strCorreoAgente);
                //bool blnBanderaCorreo = iGeneral.SendEmail("irojas@experienciasxcaret.com.mx", TituloCorreoIdioma.dsValor, Utils.armaHTMLcorreoCancelacion(TituloCorreoIdioma.dsValor + " XCARET", strCuerpoCorreo), true /*es html*/, "", false, 0, strCorreoAgente);

                if (blnBanderaCorreo)
                {
                    //    lblMensajeCorreo.Text = lblMensajeCorreo.Text.Trim() + ((lblMensajeCorreo.Text.Trim().Length > 0) ? " y " : "") + "Correo Enviado";
                }

                iGeneral.Close();

                return blnBanderaCorreo;
            }
            catch (Exception err)
            {
                //mensaje("correoPago <br>intIdClienteContacto pasado a SeleccionarcClienteContactoPorId:" + intIdClienteContacto.ToString() + " " + strParametros, err);
                return false;
            }
        }

        protected void ddlGralCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] strIdCategoria = ddlGralCategoria.SelectedValue.ToString().Split('-');                                                
            }
            catch (Exception ex)
            { }
        }

        protected void btnTerminar_Init(object sender, EventArgs e)
        {
            ASPxButton btnTerminar = sender as ASPxButton;
            btnTerminar.Attributes.Add("onClick", String.Format(callbackRegistrarCompetidor));
        }

        public void actualizaCompetidorDetalle(int idClienteDetalle)
        {
            wscClienteDetalle.IcClienteDetalleClient cteCLiente = new wscClienteDetalle.IcClienteDetalleClient();
            wscClienteDetalle.cClienteDetalle ClienteDet;
            wscClienteDetalle.cClienteDetalle ClienteDetalle;
            ClienteDetalle = cteCLiente.SeleccionarcClienteDetallePorId(idClienteDetalle, getDsSession());

            // cambiamos el estatus del participante a inscrito
            ClienteDetalle.idEstatusCliente = 15;
            int resModificaDetalles = cteCLiente.ModificarcClienteDetalle(ClienteDetalle, getDsSession());
        }

        protected void cbRegistrar_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            try
            {
                cParticipante objectParticipante = new cParticipante();               
                string[] strDsEventoClasificacion;
                bool blActualiza = false;
                bool blPaga = true;
                cCompetidorNovatas.DtCompetidor = getDatatableCompetidor();
                string strIdClienteContactoEvento = cCompetidorNovatas.DtCompetidor.Rows[0]["idClienteContactoEvento"].ToString();                
                // SI HAY VALOR EN LA PROPIEDAD dtCompetidor de nuestra clase cParticipante
                // QUIERE DECIR QUE VINO INFORMACION DE UN PRECARGADO.
                if (cCompetidorNovatas.DtCompetidor != null)
                    blActualiza = true;

                DateTime fechaEvento = new DateTime(2015, 12, 31);
                int anio = 1900;
                int mes = 01;
                int dia = 01;
                int.TryParse(ddlGralAnio.SelectedValue.ToString(), out anio);
                int.TryParse(ddlGralMes.SelectedValue.ToString(), out mes);
                int.TryParse(ddlGralDia.SelectedValue.ToString(), out dia);

                anio = (anio > 0) ? anio : 1900;
                mes = (mes > 0) ? mes : 01;
                dia = (dia > 0) ? dia : 01;
                DateTime fechaNacimiento = new DateTime(anio, mes, dia);                
                int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;
                int contParticipantes = 0;
                string strIdConfigPagoPreregistro = hdIdConfigPago.Value;

                if (ddlGralCategoria.SelectedValue.ToString() != "")
                {
                    #region llenaObjetoCompetidor
                    contParticipantes++;
                    string[] strIdEventoClasificacion = ddlGralCategoria.SelectedValue.ToString().Split('-');
                    int intIdEventoClasificacion = int.Parse(strIdEventoClasificacion[0].ToString());
                    objectParticipante.nombre = txtGralNombre.Text;
                    objectParticipante.apellidoPaterno = txtGralApellidoPaterno.Text;
                    if (txtGralApellidoMaterno.Text != "")
                        objectParticipante.apellidoMaterno = txtGralApellidoMaterno.Text;
                    objectParticipante.email = txtGralEmail.Text;
                    objectParticipante.idSexo = int.Parse(ddlGralSexo.SelectedValue.ToString());
                    if (txtGralCiudad.Text != "")
                        objectParticipante.municipio = txtGralCiudad.Text.ToString();
                    if (txtGralNoInterior.Text != "")
                        objectParticipante.numInterior = (txtGralNoInterior.Text.ToString());
                    if (txtGralNoExterior.Text != "")
                        objectParticipante.numExterior = (txtGralNoExterior.Text.ToString());
                    if (txtGralCalle.Text != "")
                        objectParticipante.calle = txtGralCalle.Text.ToString();
                    if (txtGralColonia.Text != "")
                        objectParticipante.colonia = txtGralColonia.Text.ToString();                    
                    objectParticipante.fechaNacimiento = ddlGralAnio.SelectedValue.ToString() + "/" + ddlGralMes.SelectedValue.ToString() + "/" + ddlGralDia.SelectedValue.ToString();
                    if (ddlGralPais.SelectedValue.ToString() != "")
                    {
                        objectParticipante.idPais = int.Parse(ddlGralPais.SelectedValue.ToString());
                        objectParticipante.pais = ddlGralPais.SelectedItem.ToString();
                    }
                    if (ddlGralEstado.SelectedValue.ToString() != "")
                    {
                        objectParticipante.idEstado = int.Parse(ddlGralEstado.SelectedValue.ToString());
                        objectParticipante.estado = ddlGralEstado.SelectedItem.ToString();
                    }
                    if (txtGralTelefonofijo1.Text != "")
                        objectParticipante.telefono1 = txtGralTelefonofijo1.Text + txtGralTelefonofijo2.Text;
                    objectParticipante.telefono2 = txtGralCelular1.Text + txtGralCelular2.Text;
                    objectParticipante.idEventoClasificacion = intIdEventoClasificacion;
                    strDsEventoClasificacion = ddlGralCategoria.SelectedItem.ToString().Split('/');                    
                    objectParticipante.dsEventoClasificacion = strDsEventoClasificacion[0].ToString().Trim();
                    objectParticipante.dsCodigoEventoClasificacion = strDsEventoClasificacion[1].ToString().Trim();
                    objectParticipante.vecesParticipaciones = int.Parse(ddlGralNoParticipaciones.SelectedValue.ToString());
                    objectParticipante.idTipoMoneda = 2;
                    //objectParticipante.noRifa = utils.getNoRifa(intIdEventoClasificacion);
                    objectParticipante.idTallaPlayera = int.Parse(ddlGralTallasPlayeras.SelectedValue.ToString());
                    objectParticipante.dsTallaPlayera = ddlGralTallasPlayeras.SelectedItem.ToString();
                    objectParticipante.dsFolioFmtri = txtNoFmtri.Text;
                    objectParticipante.dsAlias = txtAlias.Text;
                    objectParticipante.dsPadecimientos = txtDsPadecimientos.Text;
                    objectParticipante.idTipoSangre = int.Parse(ddlTipoSangre.SelectedValue.ToString());
                    objectParticipante.edad = edad;
                    objectParticipante.idEstatusCliente = 16; //objectParticipante PreAsignado
                    // YA TENEMOS LLENA LAS PROPIEDADES DE NUESTRO OBJETO PARTICIPANTE.
                    objectParticipante.llenarLista(objectParticipante);
                    #endregion

                    // si paga quiere decir que antes de ir a la ventana de pagar, se guarda el competidor
                    // pero con estatus en proceso y sin numero y ni nada
                    if (blPaga)
                    {
                        //antes de ir a pagar tenemos que ver si es nuevo se inserta y si no es nuevo se actualiza
                        // pero el estatus original no se toca
                        #region actualiza
                        string resultadoAtualizar = "";
                        string strPrimerIdClienteContactoEvento = "";
                        int intIdVenta = 0;
                        int intNumeroCompetidor = 0;
                        if (blActualiza)
                        {
                            switch (strIdConfigPagoPreregistro)
                            {
                                case "2":
                                    intIdVenta = fnGuardaVentaPago(0, 12);
                                    break;
                                case "3":
                                    intIdVenta = fnGuardaVentaPago(100, 14);
                                    break;
                                default:

                                    break;
                            }
                            wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                            wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                            clienteContactoEventoClient.Open();
                            cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(strIdClienteContactoEvento), getDsSession());
                            cCompetidor.dsAlias = (objectParticipante.dsAlias != null) ? objectParticipante.dsAlias : cCompetidor.dsAlias;
                            cCompetidor.dsFolioFmtri = (objectParticipante.dsFolioFmtri);
                            cCompetidor.dsPadecimiento = objectParticipante.dsPadecimientos;
                            cCompetidor.idEventoClasificacion = objectParticipante.idEventoClasificacion;
                            cCompetidor.idSexo = objectParticipante.idSexo;
                            cCompetidor.idTipoSangre = objectParticipante.idTipoSangre;
                            cCompetidor.noFrecuencia = objectParticipante.vecesParticipaciones;
                            cCompetidor.idTalla = objectParticipante.idTallaPlayera;
                            if (cCompetidor.idVenta == 0)
                            {
                                cCompetidor.idVenta = null;
                            }

                            wscClienteContacto.IcClienteContactoClient clienteContactoClient = new wscClienteContacto.IcClienteContactoClient();
                            wscClienteContacto.cClienteContacto cClienteContacto;
                            cClienteContacto = clienteContactoClient.SeleccionarcClienteContactoPorId(int.Parse(cCompetidor.idClienteContacto.ToString()), getDsSession());
                            //cClienteContacto.dsApellidoMaterno = objectParticipante.apellidoMaterno;
                            //cClienteContacto.dsApellidoPaterno = objectParticipante.apellidoPaterno;
                            cClienteContacto.dsCelular = objectParticipante.telefono2;
                            cClienteContacto.dsTelefono = objectParticipante.telefono1;
                            //cClienteContacto.dsContacto = objectParticipante.nombre;
                            //cClienteContacto.dsCorreoElectronico = objectParticipante.email;
                            cClienteContacto.feNacimiento = DateTime.Parse(objectParticipante.fechaNacimiento);
                            cClienteContacto.idEstado = objectParticipante.idEstado;
                            cClienteContacto.idPais = objectParticipante.idPais;
                            cClienteContacto.idSexo = objectParticipante.idSexo;
                            cClienteContacto.idTipoMoneda = 2;
                            
                            clienteContactoEventoClient.Close();
                            clienteContactoClient.Close();
                            resultadoAtualizar = cParticipante.actualizaCompetidor(cCompetidor, cClienteContacto, true);
                            // si es uno es porque actualizo bien
                            if (resultadoAtualizar == "1")
                            {

                            }
                        }
                        #endregion
                        else
                        {
                        }
                    }
                }                
                else
                {
                    cbRegistrar.JSProperties["cpIsMessage"] = "1 / Seleccione categoria";
                }
            }
            catch (Exception ex)
            {
             //   mensaje("Problema en carga de pagina", exx);
                cbRegistrar.JSProperties["cpIsMessage"] = ex.ToString();
            }
        }
        public int fnGuardaVentaPago(decimal dcPorcentajeDesc, int intIdFormaPago)
        {
            string[] strDsEventoClasificacion = ddlGralCategoria.SelectedItem.ToString().Split('/');
            string strDsCodigo = strDsEventoClasificacion[1].ToString().Trim();
            DataSet result = utils.getPrecio(getDsSession(), 141,strDsCodigo);
            DataTable dtProductoInscripcion = null;
            DataTable dtCompetidor = getDatatableCompetidor();
            //cParticipante.dtDataCobro = 
            int sizeDataSet = result.Tables.Count;
            dtProductoInscripcion = result.Tables[0];
            dtProductoInscripcion.Columns.Add("ordenador", typeof(decimal)).DefaultValue = 1;
            dtProductoInscripcion.Columns.Add("cantidad", typeof(int)).DefaultValue = 1;
            dtProductoInscripcion.Columns.Add("mnPrecioVenta", typeof(decimal)).DefaultValue = 1;
            dtProductoInscripcion.Columns.Add("idMonedaPagar", typeof(decimal)).DefaultValue = 1;
            string strCanalVentaId = (ConfigurationManager.AppSettings["idCanalVenta"].ToString());

            for (int i = 0; i < sizeDataSet; i++)
            {
                dtProductoInscripcion.Rows[i]["mnPrecioVenta"] = (decimal)dtProductoInscripcion.Rows[i]["mnPrecio_2"] - ((dcPorcentajeDesc * (decimal)dtProductoInscripcion.Rows[i]["mnPrecio_2"]) / 100);
                dtProductoInscripcion.Rows[i]["idMonedaPagar"] = 2;
                if (bool.Parse(dtProductoInscripcion.Rows[i]["cnEsFacturable"].ToString()) == true)
                {
                    dtProductoInscripcion.Rows[i]["ordenador"] = 1;
                    dtProductoInscripcion.Rows[i]["cantidad"] = 1;
                }
                else
                {
                    dtProductoInscripcion.Rows[i]["ordenador"] = 2;
                    dtProductoInscripcion.Rows[i]["cantidad"] = 0;
                }
            }

            cParticipante.dtDataCobro = dtProductoInscripcion;

            cResultadoGuardadoPago.strIdEstatusPago = "2";
            cResultadoGuardadoPago.strAfiliacion = "";
            cResultadoGuardadoPago.strDsRespuesta = "Aprobado";
            cResultadoGuardadoPago.strDsJustificacion = "Aprobado";
            cResultadoGuardadoPago.strDsReferencia = "";
            cResultadoGuardadoPago.strDsTransaccion = "";
            cResultadoGuardadoPago.strDsCorrelacion = "";
            cResultadoGuardadoPago.strIdBancoReceptor = "";

            decimal dcTotalVenta = getTotalVenta(dtProductoInscripcion);

            wsTransactions.IkVentaClient vc = new wsTransactions.IkVentaClient();
            vc.Open();

            wsTransactions.kVenta laventan = new wsTransactions.kVenta();

            laventan.idClienteDetalle = int.Parse(dtCompetidor.Rows[0]["idClienteDetalle"].ToString());
            laventan.idClienteClasificador = 5; // Directos
            laventan.idVenta = 0;
            laventan.idCliente = Int32.Parse(dtCompetidor.Rows[0]["idCliente"].ToString());
            laventan.feVenta = Int32.Parse(DateTime.Now.ToString("yyyyMMdd")); // DateTime.Now;
            laventan.hrVenta = DateTime.Now;
            laventan.mnMontoTotal = dcTotalVenta; //dcTotal;
            //laventan.mnDescuento = 0; //dcDescuento;
            //laventan.cnPaquete = (hdEsPaquete.Value == "1");
            laventan.cnPaquete = false;
            //laventan.dsClaveVenta
            //laventan.dsNotas = strNotas;
            if (intIdFormaPago == 14)
                laventan.idEstatusVenta = 23;
            else
                laventan.idEstatusVenta = 7;  // 
            laventan.cnRequiereFactura = false;
            laventan.idCanalVenta = int.Parse(strCanalVentaId);
            laventan.cnEvento = false;
            laventan.feCaducidad = Int32.Parse(DateTime.Now.AddYears(1).ToString("yyyyMMdd"));
            laventan.feAlta = DateTime.Now;
            laventan.idClienteUsuarioAlta = Int32.Parse(cGlobals.idUsuario.ToString());
            laventan.cnPromocion = false;  //false;

            laventan.dsDispositivo = "PC / LAPTOP";
            laventan.dsSistemaOperativo = "Windows NT APLICATION";

            if (ddlGralPais.SelectedValue != "")
                laventan.idPaisCompra = Int32.Parse(ddlGralPais.SelectedValue); //484;  // MEX cPais
            else
                laventan.idPaisCompra = 484;

            if (ddlGralEstado.SelectedValue != "1")
                laventan.idEstadoCompra = Int32.Parse(ddlGralEstado.SelectedValue);
            else
                laventan.idEstadoCompra = 272;
            laventan.idClienteContactoComprador = Int32.Parse(dtCompetidor.Rows[0]["idClienteContacto"].ToString());
            if (intIdFormaPago == 12)
                laventan.cnRequiereFactura = true;
            else
                laventan.cnRequiereFactura = false;
            // ahora los detalles
            bool blBanderaGuardo = true;
            wsTransactions.kVentaDetalle detalle; //, detallePadre;
            wsTransactions.ListakVentaDetalles listaParaEnvio = new wsTransactions.ListakVentaDetalles();


            for (int l = 0; l < dtProductoInscripcion.Rows.Count; l++)
            {
                if (bool.Parse(dtProductoInscripcion.Rows[l]["cnEsFacturable"].ToString()) == true && int.Parse(dtProductoInscripcion.Rows[l]["cantidad"].ToString()) > 0)
                {
                    detalle = new wsTransactions.kVentaDetalle();
                    string mnPrecio_ = "mnPrecio_";
                    string mnTipoCambio_ = "mnTipoCambio_";
                    decimal dcMnDescuento = 0;
                    decimal dcPrDescuento = 0;

                    mnPrecio_ += dtProductoInscripcion.Rows[l]["idMonedaPagar"].ToString();
                    mnTipoCambio_ += dtProductoInscripcion.Rows[l]["idMonedaPagar"].ToString();

                    detalle.feVisita = int.Parse(dtProductoInscripcion.Rows[l]["feVisita"].ToString());
                    detalle.feAlta = DateTime.Now;
                    //detalle.feVisitaLimite = Int32.Parse((DateTime.Now.AddYears(1)).AddMonths(6).ToString("yyyyMMdd"));  // fecha Alta + 1 año
                    //detalle.feVisitaLimite = Int32.Parse((DateTime.Parse(dtVentaOrdenador(dtVentaOrdenadorUpgradeSeleccionados.Rows[l]["fechaVisita"].ToString())).AddYears(1)).AddMonths(6).ToString("yyyyMMdd")); // fecha Visita + 1 año y medio. // validado con Lupita y Haumi 16 septiembre 2013
                    detalle.noAgrupadorVenta = 1; //? sacar el max  + 1
                    detalle.noPax = (int)dtProductoInscripcion.Rows[l]["cantidad"];
                    if (int.Parse(dtProductoInscripcion.Rows[l]["idMonedaPagar"].ToString()) > 4)
                        detalle.mnPrecioLista = (decimal)dtProductoInscripcion.Rows[l]["mnPrecio_MS"];
                    else
                        detalle.mnPrecioLista = (decimal)dtProductoInscripcion.Rows[l][mnPrecio_];
                    detalle.mnPrecioVenta = decimal.Parse(dtProductoInscripcion.Rows[l]["mnPrecioVenta"].ToString()) / detalle.noPax;

                    // cuando detalle.mnPrecioLista  es cero, es porque son cosas como donativos que no tienen precio
                    if (detalle.mnPrecioLista > 0)
                    {
                        dcMnDescuento = (decimal)(detalle.mnPrecioLista - detalle.mnPrecioVenta);
                        dcPrDescuento = dcPorcentajeDesc;
                    }
                    else
                    {
                        dcMnDescuento = 0;
                        dcPrDescuento = 0;
                    }
                    detalle.mnDescuento = dcMnDescuento; //detalle.mnPrecioLista - detalle.mnPrecioVenta; // se calcula de la resta de precioLista - precioVenta listo
                    detalle.prDescuento = dcPrDescuento; //((1 - (detalle.mnPrecioVenta / detalle.mnPrecioLista)) * 100); // se calcula listo

                    detalle.idProductoPrecio = (int)dtProductoInscripcion.Rows[l]["idProductoPrecio"];
                    detalle.cnAccesado = false;
                    detalle.cnControlInterno = false;
                    detalle.cnPaquete = (bool)dtProductoInscripcion.Rows[l]["cnEsPaquete"];
                    /*if ((bool)detalle.cnPaquete)
                    {
                        // Depende la el orde de como escojan los productos se guarda la venta con bandera de paquete.
                        laventan.cnPaquete = true; //(hdEsPaquete.Value == "1");
                        detalle.dsClavePaquete = dtVentaOrdenadorUpgradeSeleccionados.Rows[l]["dsClavePaquete"].ToString();
                    }*/
                    detalle.idTipoCliente = (int)dtProductoInscripcion.Rows[l]["idTipoCliente"];
                    detalle.cnPromocion = false;

                    detalle.dsNombreVisitante = dtCompetidor.Rows[0]["dsContacto"].ToString(); // llenar con campo de texto por cada registro - listo
                    detalle.dsApellidoPaternoVisitante = dtCompetidor.Rows[0]["dsApellidoPaterno"].ToString();
                    detalle.dsApellidoMaternoVisitante = dtCompetidor.Rows[0]["dsApellidoMaterno"].ToString();

                    if (intIdFormaPago == 14)
                        detalle.idEstatusVenta = 23;
                    else
                        detalle.idEstatusVenta = 7; //  

                    int intLocacion = (int)dtProductoInscripcion.Rows[l]["idLocacion"];

                    if (intLocacion != 0)
                        detalle.idLocacion = intLocacion;


                    detalle.mnComision = 0;
                    detalle.prComision = 0;
                    // En idMoneda del detalle se debe guardar el idTipoMoneda (Gabriel)
                    //detalle.idTipoMoneda = (int)dtVentaOrdenador.Rows[l]["idTipoMoneda"];
                    detalle.idTipoMoneda = int.Parse(dtProductoInscripcion.Rows[l]["idMonedaPagar"].ToString());

                    detalle.mnTipoCambio = (decimal)dtProductoInscripcion.Rows[l][mnTipoCambio_]; // ? sacar del día listo

                    // Gabriel Granados pide que el campo mnTipoCambio se guarde el tipo de cambio en pesos del dia. correo(09-07-2013): Ventas en Dolares y PEsos
                    if (detalle.idTipoMoneda == 1)
                    {
                    }

                    detalle.idSegmento = 4;
                    detalle.idClienteUsuarioAlta = Int32.Parse(cGlobals.idUsuario.ToString());
                    //detalle.idVentaDetalle = int.Parse(dtVentaOrdenador.Rows[l]["consecutivo"].ToString());
                    detalle.idVentaDetalle = 0;
                    detalle.idCanalVenta = int.Parse(strCanalVentaId);
                    detalle.mnIva = null;
                    listaParaEnvio.Add(detalle);
                }
            }// fin del for   

            int intIdVenta = vc.InsertarkVenta(laventan, listaParaEnvio, null, null, getDsSession());
            vc.Close();

            //------------------------------------------------------------------------------------------------
            // PAGO
            //------------------------------------------------------------------------------------------------

            wsTransactions.IkPagoClient IkPago = new wsTransactions.IkPagoClient();
            wsTransactions.ListakPagoTransacciones lstkPagoTransa = new wsTransactions.ListakPagoTransacciones();
            wsTransactions.kPagoTransaccion PagoTran = new wsTransactions.kPagoTransaccion();
            wsTransactions.kPago objkPago = new wsTransactions.kPago();

            objkPago.idPago = 0;
            objkPago.idVenta = intIdVenta;
            objkPago.idCanalPago = int.Parse(strCanalVentaId); //11; // fijo ?
            objkPago.feTransaccionTotal = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
            objkPago.hrTransaccionTotal = DateTime.Now;
            //objkPago.mnTransaccionTotal = Math.Round(dcTotal, 2);
            //objkPago.mnTransaccionTotal = decimal.Parse(totalPagar);
            objkPago.mnTransaccionTotal = dcTotalVenta;
            objkPago.cnEsPrepago = false;
            objkPago.noAgrupadorVenta = 1;
            objkPago.idFormaPago = intIdFormaPago; // 52 CxC y 14 Cortesias
            objkPago.idEstatusPago = Int32.Parse(cResultadoGuardadoPago.strIdEstatusPago); //intIdEstatusPago;
            objkPago.feAlta = DateTime.Now;
            objkPago.idClienteUsuarioAlta = (cGlobals.idUsuario);

            PagoTran.idPago = objkPago.idPago; //
            // En idMoneda se guarda el IdTipoMoneda (Gabriel 20082013)
            PagoTran.idTipoMoneda = int.Parse(dtProductoInscripcion.Rows[0]["idMonedaPagar"].ToString()); //Int32.Parse(cmbMoneda.SelectedItem.Value);

            PagoTran.feTransaccion = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
            PagoTran.hrTransaccion = DateTime.Now;
            PagoTran.dsRespuesta = cResultadoGuardadoPago.strDsRespuesta; // strDsRespuesta;
            PagoTran.dsReferencia = cResultadoGuardadoPago.strDsReferencia; // strDsReferencia;
            PagoTran.dsJustificacion = cResultadoGuardadoPago.strDsJustificacion; // strDsJustificacion;

            //PagoTran.mnTransaccion = Math.Round(dcTotal, 2);
            //PagoTran.mnTransaccion = decimal.Parse(totalPagar);
            PagoTran.mnTransaccion = dcTotalVenta;

            PagoTran.dsTransaccion = cResultadoGuardadoPago.strDsTransaccion; // strDsTransaccion;
            PagoTran.dsCorrelacion = cResultadoGuardadoPago.strDsCorrelacion; // strDsCorrelacion;
            PagoTran.noPagosMSI = 0;
            objkPago.cnPlanDePagos = false;

            PagoTran.cnPrepago = false;
            //if (intIdClienteTarjeta > 0)
            //if (Int32.Parse(cResultadoGuardadoPago.strIdClienteTarjeta) > 0)
            PagoTran.idClienteTarjeta = null; // intIdClienteTarjeta;
            PagoTran.mnBancoComision = 0;
            PagoTran.prBancoComision = 0;
            PagoTran.feAlta = DateTime.Now;
            PagoTran.idClienteUsuarioAlta = (cGlobals.idUsuario);
            PagoTran.idPagoTransaccion = 0;
            PagoTran.idEstatusPago = Int32.Parse(cResultadoGuardadoPago.strIdEstatusPago);

            PagoTran.dsAfiliacion = cResultadoGuardadoPago.strAfiliacion;

            if (intIdFormaPago == 52) // si el tipo de pago es CxC  se tiene que guardar el idBanco y idBancoReceptor  Bancomer (5)
            {
                PagoTran.idBanco = 5;
                PagoTran.idBancoReceptor = 5;
            }
            else
            {
                PagoTran.idBanco = null;
                PagoTran.idBancoReceptor = null;
            }

            lstkPagoTransa.Add(PagoTran);
            int intIdPago;
            try
            {
                // 1 er intento
                intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
            }
            catch (Exception err)
            {
                // mensaje("OE fallo primer intento, intIdTipoMoneda=" + intIdTipoMoneda.ToString() + strCadenaResultadoPagoTarjeta, err);
                try
                {
                    // 2 er intento
                    intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                }
                catch (Exception err2)
                {
                    // 3 er intento
                    // mensaje("OE fallo segundo intento, intIdTipoMoneda=" + intIdTipoMoneda.ToString() + strCadenaResultadoPagoTarjeta, err);
                    intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                }
            }
            IkPago.Close();

            return intIdVenta;
        }
        protected void cbValida_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            try
            {                
                DateTime fechaEvento = new DateTime(2015, 12, 31);
                int anio = 1900;
                int mes = 01;
                int dia = 01;
                int.TryParse(ddlGralAnio.SelectedValue.ToString(), out anio);
                int.TryParse(ddlGralMes.SelectedValue.ToString(), out mes);
                int.TryParse(ddlGralDia.SelectedValue.ToString(), out dia);

                anio = (anio > 0) ? anio : 1900;
                mes = (mes > 0) ? mes : 01;
                dia = (dia > 0) ? dia : 01;
                DateTime fechaNacimiento = new DateTime(anio, mes, dia);

                int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;
                utils.getCategorias(getDsSession(),Convert.ToInt16(ddlGralSexo.SelectedValue), edad, ddlGralCategoria, null, "TXI", 1);
                bool respValidacion = utils.validaExisteRegistro(txtGralEmail.Text, getDsSession());                

                if (respValidacion)
                {
                    cbValida.JSProperties["cpValidacion"] = "true";
                }
                else 
                {
                    cbValida.JSProperties["cpValidacion"] = "false";
                }
            }
            catch (Exception ex)
            {
                //   mensaje("Problema en carga de pagina", exx);
                //cbRegistrar.JSProperties["cpIsMessage"] = ex.ToString();
                cbValida.JSProperties["cpValidacion"] = "";
            }
        }

        public decimal getTotalVenta(DataTable dtVentaOrdenador)
        {
            decimal respuesta = 0;

            for (int i = 0; i < dtVentaOrdenador.Rows.Count; i++)
            {
                if (bool.Parse(dtVentaOrdenador.Rows[i]["cnEsFacturable"].ToString()) == true)
                    respuesta += (decimal)dtVentaOrdenador.Rows[i]["mnPrecioVenta"];
            }
            return respuesta;
        }
        protected void btnContinuarA_Init(object sender, EventArgs e)
        {
            //ASPxButton btnContinuarA = sender as ASPxButton;
            //btnContinuarA.Attributes.Add("onClick", String.Format(callbackValidaCompetidor));
        }

        protected void txtGralEmail2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtGralEmail.Text != txtGralEmail2.Text)
                {
                    lblValidacionEmail.Text = "Los correos, no coinciden, favor de validarlo";
                }
                else
                {
                    lblValidacionEmail.Text = "";                    
                }
            }
            catch (Exception ex)
            { 
            }
        }

        public void mensaje(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }

        public void alert(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert(" + errMensaje + ");</script>", false);
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.dsSession;
        }

        public int getIdUsuario()
        {
            if (cGlobals.idUsuario == null || cGlobals.idUsuario == 0)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.idUsuario;
        }

        public int getIdCliente()
        {
            if (cGlobals.idCliente == null || cGlobals.idCliente == 0)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.idCliente;
        }
    }
}