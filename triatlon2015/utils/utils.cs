﻿using System;   
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Data;

using System.Net;
using System.ServiceModel;
using System.Windows;
using System.Configuration;
using System.Text.RegularExpressions;
using DevExpress.Web.ASPxEditors;

namespace travesia
{
    public static class utils
    {
        public static void llenaAnio(DropDownList ListaAnio, int minEdad = 9)
        {
            int MaxYear = DateTime.Now.Year;
            int iyear = DateTime.Now.Year;
            int yearmax = iyear - minEdad;
            int yearmin = iyear - 100;
            
            ListaAnio.Items.Clear();
            ListItem itemdefault = new ListItem("Selecciona un año", "");
            ListaAnio.Items.Add(itemdefault);
            for (iyear = yearmax; iyear >= yearmin; iyear--)
            {
                ListItem item = new ListItem(iyear.ToString(), iyear.ToString());
                if (iyear < MaxYear)
                {
                    ListaAnio.Items.Add(item);
                }
                ListaAnio.SelectedIndex = 0;
            }
        }

        public static void llenaAniosTravesia(ASPxListBox ListaAnios)
        {
            int MaxYear = DateTime.Now.Year;
            int iyear = DateTime.Now.Year;
            int startYear = 2007;            
            int yearmin = iyear - 100;
            string strAnios = "";
            //ListaAnios.Items.Clear();
            ListItem itemdefault = new ListItem("Selecciona un año", "");  
            DataTable dt = new DataTable("MyDataTable");
            dt.Columns.Add("Value", typeof(Int32));
            dt.Columns.Add("Text", typeof(string));
            dt.Rows.Add(new object[] { 0, "(Select all)" });
            //ListaAnios.Items.Add(itemdefault);
            for (iyear = MaxYear; iyear >= startYear; iyear--)
            {
                ListItem item = new ListItem(iyear.ToString(), iyear.ToString());

                dt.Rows.Add(new object[] { iyear, iyear.ToString() });
                //ListaAnios.Items.Add(item);                             
            }
            ListaAnios.DataSource = dt;
            ListaAnios.DataBind();
            //ListaAnios.SelectedIndex = 0;
        }

        public static void llenaAnio_en(DropDownList ListaAnio, int minEdad = 9)
        {
            int MaxYear = DateTime.Now.Year;
            int iyear = DateTime.Now.Year;
            int yearmax = iyear - minEdad;
            int yearmin = iyear - 100;

            ListaAnio.Items.Clear();
            ListItem itemdefault = new ListItem("Select year", "");
            ListaAnio.Items.Add(itemdefault);
            for (iyear = yearmax; iyear >= yearmin; iyear--)
            {
                ListItem item = new ListItem(iyear.ToString(), iyear.ToString());
                if (iyear < MaxYear)
                {
                    ListaAnio.Items.Add(item);
                }
                ListaAnio.SelectedIndex = 0;
            }
        }

        public static void llenaAnioUp(DropDownList ListaAnio, int maxYear = 10)
        {
            int minYear = DateTime.Now.Year;
            int maxYearTop = minYear + maxYear;
            ListaAnio.Items.Clear();            
            for (int iyear = minYear; iyear <= maxYearTop; iyear++)
            {
                ListItem item = new ListItem(iyear.ToString(), iyear.ToString());
                ListaAnio.Items.Add(item);                
            }
            ListaAnio.SelectedIndex = 0;
        }

        public static void llenaDias(int Anio, int MesNo, DropDownList Dia)
        {
            int days = System.DateTime.DaysInMonth(Anio, MesNo);
            Dia.Items.Clear();
            ListItem itemdefault = new ListItem("Seleccione día", "");
            for (int idays = 1; idays <= days; idays++)
            {
                ListItem item = new ListItem(idays.ToString(), idays.ToString());
                Dia.Items.Add(item);
            }
            Dia.SelectedIndex = 0;
        }

        public static void llenaPais(DropDownList listaPais)
        {
            wsGeneralServices.GeneralServicesClient gralCliente = new wsGeneralServices.GeneralServicesClient();
            wsGeneralServices.ListacCombo listaC = new wsGeneralServices.ListacCombo();
            listaC = gralCliente.SeleccionarcCombo("cPaisIdioma", "idPais", "dsPais", "where idIdioma = 1", "order by dsPais", cGlobals.dsSession);
            listaPais.DataSource = listaC.cCombo;
            listaPais.Items.Add(new ListItem("-- Selecciona un pais --", ""));
            for (int cnt = 0; cnt < listaC.cCombo.Count(); cnt++)
            {
                listaPais.Items.Add(new ListItem(listaC.cCombo[cnt].dsValor, listaC.cCombo[cnt].idValor.ToString()));
                //if (listaC.cCombo[cnt].idValor == 484)                    
            }
            gralCliente.Close();
            listaPais.SelectedValue = "484";
        }


        public static void llenaEstados(DropDownList ListaEstado, string idPais, string dsSession)
        {
            try
            {
                if (idPais != null)
                {
                    wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();
                    Dictionary<String, String> dicrets = new Dictionary<String, String>();
                    dicrets.Add("@idPais", idPais.ToString());
                    DataSet resultSets = reglas.ExecuteRule("spGETEstadosPorPais", dicrets, dsSession);
                    ListaEstado.Items.Clear();
                    //ListaEstado.Items.Add(new ListItem("Seleccione Estado", ""));
                    ListaEstado.DataSource = resultSets.Tables[0];
                    for (int cnt = 0; cnt < resultSets.Tables[0].Rows.Count; cnt++)
                    {
                        if (!(idPais == "484" && resultSets.Tables[0].Rows[cnt]["idValor"].ToString() == "600"))
                            ListaEstado.Items.Add(new ListItem(resultSets.Tables[0].Rows[cnt]["dsValor"].ToString(), resultSets.Tables[0].Rows[cnt]["idValor"].ToString()));
                    }
                    reglas.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void getCategorias(string dsSession, int idSexo, int edad, DropDownList Categoria, string dsCategoria, string dsModalidad, int cnRelevo = 0, int cnCD = 0)
        {
            string[] strDsValorCombo = null;
            /*
             *  strDsValorCombo[0] -> dsNombre
             *  strDsValorCombo[1] -> idEventoClasificacion
             *  strDsValorCombo[2] -> dsCodigo
             */
            wsGeneralServices.GeneralServicesClient gralCliente = new wsGeneralServices.GeneralServicesClient();
            wsGeneralServices.ListacCombo listaC = new wsGeneralServices.ListacCombo();
            listaC = gralCliente.SeleccionarcCombo("cEventoClasificacion", "idEventoClasificacion", "CONCAT(dsNombre,'|',idLocacion,'|',dsCodigo)", "where dsAgrupador = 'Travesia Sagrada'", "order by idEventoClasificacion", cGlobals.dsSession);
            //Categoria.DataSource = listaC.cCombo;
            //Categoria.Items.Add(new ListItem("Selecciona sede", ""));
            for (int cnt = 0; cnt < listaC.cCombo.Count(); cnt++)
            {
                strDsValorCombo = listaC.cCombo[cnt].dsValor.ToString().Trim().Split('|');
                //Categoria.Items.Add(new ListItem(strDsValorCombo[0].ToString() + "/" + strDsValorCombo[2].ToString(), listaC.cCombo[cnt].idValor.ToString() +"|" + strDsValorCombo[1].ToString() ));
                Categoria.Items.Add(new ListItem(strDsValorCombo[0].ToString() + "/" + strDsValorCombo[2].ToString(), listaC.cCombo[cnt].idValor.ToString() ));
            }
            Categoria.DataBind();
        }

        public static int getIdLocacionByIdEventoClasificacion(string dsSession, string idEventoClasificacion)
        {
            wsGeneralServices.GeneralServicesClient gralCliente = new wsGeneralServices.GeneralServicesClient();
            wsGeneralServices.ListacCombo listaC = new wsGeneralServices.ListacCombo();
            listaC = gralCliente.SeleccionarcCombo("cEventoClasificacion", "idLocacion", "CONCAT(dsNombre,'|',idLocacion,'|',dsCodigo)", "where dsAgrupador = 'Travesia Sagrada' and idEventoClasificacion = " + idEventoClasificacion.ToString(), "order by idEventoClasificacion", cGlobals.dsSession);
            return listaC.cCombo[0].idValor;
        }

        public static List<string> getLocaciones(string dsSession, DropDownList locacion)
        {
            wsGeneralServices.GeneralServicesClient gralCliente = new wsGeneralServices.GeneralServicesClient();
            wsGeneralServices.ListacCombo listaC = new wsGeneralServices.ListacCombo();
            //wsGeneralServices.ListacCombo listaAgotados = new wsGeneralServices.ListacCombo();
            List<string> listaAgotados = new List<string>();
            listaC = gralCliente.SeleccionarcCombo("cLocacion", "idLocacion", "dsLocacion", "where idUnidadNegocio = 13 and idLocacion in (141,142,143,145) and cnActivo=1", "order by idLocacion", cGlobals.dsSession);
            //locacion.DataSource = listaC.cCombo;
            locacion.Items.Add(new ListItem("Selecciona sede", ""));
            for (int cnt = 0; cnt < listaC.cCombo.Count(); cnt++)
            {
                // Aqui voy a ir a investigar cuanto espacio queda de espacio por la locacion,
                // si ya no hay ya no muestro en el combo la opcion
                //DataTable dtDisponibilidadLocacion = getDisponibilidadLocaciones(dsSession, listaC.cCombo[cnt].idValor.ToString());
                //if (dtDisponibilidadLocacion.Rows[0]["retValue"].ToString() == "1")
                locacion.Items.Add(new ListItem(listaC.cCombo[cnt].dsValor.ToString().Trim(), listaC.cCombo[cnt].idValor.ToString()));
                //else
                   // listaAgotados.Add(listaC.cCombo[cnt].dsValor.ToString().Trim());
            }
            locacion.DataBind();
            return listaAgotados;
        }

        public static DataTable getDisponibilidadLocaciones(string dsSession, string strIdLocacion = "0", string strIdEventoClasificacion = "0")
        {
            wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();

            Dictionary<string, string> dicret = new Dictionary<string, string>();
            dicret.Add("@idLocacion", strIdLocacion);
            dicret.Add("@idEventoClasificacion", strIdEventoClasificacion);           
            DataSet datos = reglas.ExecuteRule("spGetDisponibilidadLocacion", dicret, dsSession);          
            //if (datos != null && datos.Tables[0].Rows[0]["retValue"].ToString() == "1")
            return datos.Tables[0];
        }

        public static void getParentescos(string dsSession, DropDownList parentesco)
        {
            wsGeneralServices.GeneralServicesClient gralCliente = new wsGeneralServices.GeneralServicesClient();
            wsGeneralServices.ListacCombo listaC = new wsGeneralServices.ListacCombo();
            listaC = gralCliente.SeleccionarcCombo("cParentesco", "idParentesco", "dsParentesco", "where cnActivo = 1", "order by idParentesco", cGlobals.dsSession);
            //locacion.DataSource = listaC.cCombo;
            parentesco.Items.Add(new ListItem("Selecciona parentesco", ""));
            for (int cnt = 0; cnt < listaC.cCombo.Count(); cnt++)
            {
                parentesco.Items.Add(new ListItem(listaC.cCombo[cnt].dsValor.ToString().Trim(), listaC.cCombo[cnt].idValor.ToString()));
            }
            parentesco.DataBind();
        }

        public static void getUnidadNegocioColaborador(string dsSession, DropDownList unidadNegocioColaborador)
        {
            wsGeneralServices.GeneralServicesClient gralCliente = new wsGeneralServices.GeneralServicesClient();
            wsGeneralServices.ListacCombo listaC = new wsGeneralServices.ListacCombo();
            listaC = gralCliente.SeleccionarcCombo("cUnidadNegocioColaborador", "idUnidadNegocioColaborador", "dsUnidadNegocioColaborador", "where cnActivo = 1", "order by dsUnidadNegocioColaborador", cGlobals.dsSession);
            //locacion.DataSource = listaC.cCombo;
            unidadNegocioColaborador.Items.Add(new ListItem("Selecciona unidad negocio", ""));
            for (int cnt = 0; cnt < listaC.cCombo.Count(); cnt++)
            {
                unidadNegocioColaborador.Items.Add(new ListItem(listaC.cCombo[cnt].dsValor.ToString().Trim(), listaC.cCombo[cnt].idValor.ToString()));
            }
            unidadNegocioColaborador.DataBind();
        }

        public static void getCategorias_en(string dsSession, int idSexo, int edad, DropDownList Categoria, string dsCategoria, string dsModalidad, int cnRelevo = 0, int cnCD = 0)
        {
            wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();

            Dictionary<string, string> dicret = new Dictionary<string, string>();
            dicret.Add("@idSexo", idSexo.ToString());
            dicret.Add("@dsModalidad", dsModalidad);
            dicret.Add("@edad", edad.ToString());
            dicret.Add("@cnRelevo", cnRelevo.ToString());
            dicret.Add("@cnCD", cnCD.ToString());
            DataSet datos = reglas.ExecuteRule("spGetCategorias", dicret, dsSession);
            Categoria.Items.Clear();
            ListItem itemDefault = new ListItem("Select category", "");
            Categoria.Items.Add(itemDefault);
            if (datos != null && datos.Tables[0].Rows[0]["retValue"].ToString() == "1")
            {
                for (int cnt = 0; cnt < datos.Tables[0].Rows.Count; cnt++)
                {
                    ListItem item = new ListItem(datos.Tables[0].Rows[cnt]["dsDescripcion"].ToString() + " / " + datos.Tables[0].Rows[cnt]["dsCodigo"].ToString(), datos.Tables[0].Rows[cnt]["idEventoClasificacion"].ToString() + "-" + datos.Tables[0].Rows[cnt]["idProducto"].ToString());
                    item.Attributes["SysCode"] = "Code-" + (cnt + 1).ToString();
                    item.Attributes.Add("Evento", datos.Tables[0].Rows[cnt]["idEvento"].ToString());
                    item.Attributes["Sexo"] = datos.Tables[0].Rows[cnt]["idSexo"].ToString();
                    Categoria.Items.Add(item);
                    //Categoria.Items.(cnt).Attributes.Add("data-siteId","");
                    //Categoria.Attributes.Add("idProducto", datos.Tables[0].Rows[cnt]["idProducto"].ToString());                        
                }
                reglas.Close();
                Categoria.DataBind();
                //Now, add a "SysCode" attribute to each item in the dropdown list
                /*for (int i = 0; i < Categoria.Items.Count; i++)
                {
                    ListItem item = Categoria.Items[i];
                    item.Attributes["SysCode"] = "Code-" + (i + 1).ToString();
                }*/
                Categoria.DataBind();
            }
        }

        public static wsGeneralServices.ListacCombo getCategoriaEspecifica(int idSexo, int idProducto, int edad)
        {
            wsGeneralServices.GeneralServicesClient general = new wsGeneralServices.GeneralServicesClient();
            wsGeneralServices.ListacCombo listaEstatusVenta = new wsGeneralServices.ListacCombo();
            listaEstatusVenta = general.SeleccionarcCombo("cEventoClasificacion", "idEventoClasificacion", "dsNombre", "where idProducto = " + idProducto.ToString() + " and idsexo = " + idSexo.ToString() + " and " + edad.ToString() + "  BETWEEN noEdadInf and noEdadSup", "", cGlobals.dsSession);
            return listaEstatusVenta;
        }

        public static int getNoRifa(int idEventoClasificacion)
        {
            try
            {
                wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();

                Dictionary<string, string> dicret = new Dictionary<string, string>();
                dicret.Clear();
                dicret.Add("@idEventoClasificacion", idEventoClasificacion.ToString());
                DataSet datos = reglas.ExecuteRule("spGetNumeroRifa", dicret, cGlobals.dsSession);
                reglas.Close();
                dicret.Clear();
                if (datos != null && datos.Tables[0].Rows[0]["retValue"].ToString() == "1")
                {
                    return int.Parse(datos.Tables[0].Rows[0]["numero"].ToString());
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static string Login(string strUser, string strPass)
        {
            wsSecurity.IsSecurityClient seguridad = new wsSecurity.IsSecurityClient();
            wsSecurity.Security seguridadObject = new wsSecurity.Security();
            //  "usrTriatlon", "eXperiencias"
            try
            {
                //seguridadObject = seguridad.Login(strUser, strPass, getDevice(), getResolution(), Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]));

                //if (seguridadObject != null)
               // {
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]))
                {
                    cGlobals.dsSession = "4heeon35j3bt4aqzqbuhsx1v";     // Guardar la variable global con el numero de sesion produccion
                    cGlobals.dsBasedeDatos = "dbSivex";
                    cGlobals.dsUsuario = "usrTravesia";       // Guardar la variable global con el usuario
                    cGlobals.idUsuario = 116346; // GUardar la variable global con el id del usuario;
                    cGlobals.idCliente = 1;       //Guardar la variable global con el id del cliente;
                    cGlobals.dsNombreUsuario = "Usuario Travesia";   
                }
               // }
                else
                {
                    //e.Authenticated = false;
                    cGlobals.dsSession = "e0gbam2hoay2myk21x54ajcy";     // Guardar la variable global con el numero de sesion pruebas
                    cGlobals.dsBasedeDatos = "dbSivexPruebas";
                    cGlobals.dsUsuario = "usrTravesia";       // Guardar la variable global con el usuario
                    cGlobals.idUsuario = 74770; // GUardar la variable global con el id del usuario;
                    cGlobals.idCliente = 1;       //Guardar la variable global con el id del cliente;
                    cGlobals.dsNombreUsuario = "Usuario Travesia";
                }                                           
                
                //seguridad.Close();
                // Todo Correcto, pasamos al menu principal
                return "true";
            }
            catch (Exception ex)
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "msgLoad", "<script type='text/javascript'>showMessage('error','Ha ocurrido un error','" + ex.Message.ToString().Replace("'", "") + "');</script>", false);
                return ex.Message;
            }            
        }

        public static string LoginU(string strUser, string strPass)
        {
            wsSecurity.IsSecurityClient seguridad = new wsSecurity.IsSecurityClient();
            wsSecurity.Security seguridadObject = new wsSecurity.Security();
            //  "usrTriatlon", "eXperiencias"
            try
            {
                seguridadObject = seguridad.Login(strUser, strPass, getDevice(), getResolution(), Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]),false);

                if (seguridadObject != null)
                {
                    cGlobals.dsSession = seguridadObject.dsSessionId;     // Guardar la variable global con el numero de sesion          
                    cGlobals.dsUsuario = seguridadObject.dsUsuario;       // Guardar la variable global con el usuario
                    cGlobals.idUsuario = seguridadObject.idClienteUsuario; // GUardar la variable global con el id del usuario;
                    cGlobals.idCliente = seguridadObject.idCliente;       //Guardar la variable global con el id del cliente;
                    cGlobals.dsNombreUsuario = seguridadObject.dsNombreUsuario;
                    cGlobals.dsBasedeDatos = seguridadObject.dsBasedeDatos;
                    return "true";
                }
                else
                {
                    //e.Authenticated = false;
                    return "";
                }
                seguridad.Close();
                // Todo Correcto, pasamos al menu principal
            }
            catch (Exception ex)
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "msgLoad", "<script type='text/javascript'>showMessage('error','Ha ocurrido un error','" + ex.Message.ToString().Replace("'", "") + "');</script>", false);
                return ex.Message;
            }
        }

        public static bool validaExisteRegistro( string email, string dsSession)
        {
            try
            {
                wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();
                Dictionary<string, string> dicret = new Dictionary<string, string>();
                dicret.Add("@email", email.ToString());
                DataSet datos = reglas.ExecuteRule("spValidaInscritoTravesia", dicret, dsSession);

                if (datos != null && int.Parse(datos.Tables[0].Rows[0]["coincidencia"].ToString()) > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool validaExisteRegistroApellidosYFeNacimiento(string apellidoPaterno, string apellidoMaterno, string feNacimiento, string dsSession)
        {
            try
            {
                wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();
                Dictionary<string, string> dicret = new Dictionary<string, string>();
                dicret.Clear();
                dicret.Add("@ApellidoP", apellidoPaterno);
                dicret.Add("@ApellidoM", apellidoMaterno);
                dicret.Add("@FeNacimiento", feNacimiento);
                DataSet datos = reglas.ExecuteRule("spValidaInscritoRifaApellidosYFeNacimiento", dicret, dsSession);

                if (datos != null && int.Parse(datos.Tables[0].Rows[0]["coincidencia"].ToString()) > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void getEstatusVenta(DropDownList cmbEstatus)
        {
            try
            {
                wsGeneralServices.GeneralServicesClient general = new wsGeneralServices.GeneralServicesClient();
                wsGeneralServices.ListacCombo listaEstatusVenta = general.SeleccionarcCombo("cEstatusVenta", "idEstatusVenta", "dsEstatusVenta", "", "", cGlobals.dsSession);
                
                //cmbEstatus.DataSource = listaEstatusVenta.cCombo;
                //cmbEstatus.DataValueField = "idValor";
                //cmbEstatus.DataTextField = "dsValor";
                /*ListItem itemDefault = new ListItem("Seleccione Estatus", "");
                cmbEstatus.Items.Insert(0, itemDefault);*/
                cmbEstatus.Items.Clear();
                ListItem itemDefault = new ListItem("Seleccione Estatus", "");
                cmbEstatus.Items.Add(itemDefault);
                foreach(var elemento in listaEstatusVenta.cCombo)
                {
                    ListItem item = new ListItem(elemento.dsValor, elemento.idValor.ToString());
                    cmbEstatus.Items.Add(item);  
                }

                cmbEstatus.DataBind();                

            }
            catch (Exception err)
            {
                //mensaje(err.Message);
            }
        }

        public static void getEstatusParticipante(DropDownList cmbEstatusParticipante)
        {
            try
            {
                wsGeneralServices.GeneralServicesClient general = new wsGeneralServices.GeneralServicesClient();
                wsGeneralServices.ListacCombo listaEstatusParticipante = general.SeleccionarcCombo("cEstatusCliente", "idEstatusCliente", "dsEstatusCliente", "where idEstatusCliente in (14,17,15)", "", cGlobals.dsSession);
                
                //cmbEstatusParticipante.DataSource = listaEstatusParticipante.cCombo;
                //cmbEstatusParticipante.DataValueField = "idValor";
                //cmbEstatusParticipante.DataTextField = "dsValor";
                /*ListItem itemDefault = new ListItem("Seleccione Estatus", "");
                cmbEstatusParticipante.Items.Add(itemDefault);*/
                cmbEstatusParticipante.Items.Clear();
                ListItem itemDefault = new ListItem("Seleccione Estatus", "");
                cmbEstatusParticipante.Items.Add(itemDefault);
                foreach (var elemento in listaEstatusParticipante.cCombo)
                {
                    ListItem item = new ListItem(elemento.dsValor, elemento.idValor.ToString());
                    cmbEstatusParticipante.Items.Add(item);
                }                
                cmbEstatusParticipante.DataBind();

            }
            catch (Exception err)
            {
                //mensaje(err.Message);
            }
        }

        public static void getTallas(DropDownList tallas, string dsSession)
        {
           /* wscTalla.IcTallaClient cTalla = new wscTalla.IcTallaClient();
            cTalla.Open();
            wscTalla.ListacTallas listaTallas = cTalla.SeleccionarcTallas(dsSession);

            tallas.Items.Clear();
            ListItem itemDefault = new ListItem("Selecciona Talla", "");
            tallas.Items.Add(itemDefault);
            foreach (var elemento in listaTallas)
            {
                ListItem item = new ListItem(elemento.dsTalla.ToString(), elemento.idTalla.ToString());
                tallas.Items.Add(item);
            }

            tallas.DataBind();    */
            
        }

        public static void getTallas_en(DropDownList tallas, string dsSession)
        {
            /*wscTalla.IcTallaClient cTalla = new wscTalla.IcTallaClient();
            cTalla.Open();
            wscTalla.ListacTallas listaTallas = cTalla.SeleccionarcTallas(dsSession);

            tallas.Items.Clear();
            ListItem itemDefault = new ListItem("Select size", "");
            tallas.Items.Add(itemDefault);
            foreach (var elemento in listaTallas)
            {
                ListItem item = new ListItem(elemento.dsTalla.ToString(), elemento.idTalla.ToString());
                tallas.Items.Add(item);
            }

            tallas.DataBind();*/

        }

        public static void getFormasPago(DropDownList ddlFormasPago, int idCanalVenta, string dsSession)       
        {            
                /*
                     6	Tarjeta Credito / Visa Terminal
                     7	Tarjeta Credito / Mastercard Terminal
                     17	Tarjeta Debito / Visa Terminal
                     18	Tarjeta Debito / Mastercard Terminal
                     21	Deposito Moneda Nacional
                     22	Deposito Dolares Americanos
                     23	Tranferencia Moneda Nacional
                     24	Transferencia Dolares Americano
                     27	Tarjeta Credito / Visa TPV Virtual
                     28	Tarjeta Credito / Mastercard TPV Virtual
                     29	Tarjeta Debito / Visa TPV Virtual
                     30	Tarjeta Debito / Mastercard TPV Virtual
                     36	American Express TPV Virtual
                     42	Cheque Moneda Nacional
                     43	Cheque Dolares Americanos
                */
                wscFormaPago.ListacFormaPagos lstFormaPagos;
                ddlFormasPago.Items.Clear();
                ddlFormasPago.Items.Add(new ListItem("Seleccionar Forma de pago", ""));

                wscFormaPago.IcFormaPagoClient IcFormaPago = new wscFormaPago.IcFormaPagoClient();
                IcFormaPago.Open();
                lstFormaPagos = IcFormaPago.SeleccionarcFormaPagosPorIdCanalVenta(idCanalVenta, dsSession);                    
               
                for (int cnt = 0; cnt < lstFormaPagos.Count; cnt++)
                {
                    //if (lstFormaPagos[cnt].idFormaPago != 45 && lstFormaPagos[cnt].idFormaPago != 53 && lstFormaPagos[cnt].idFormaPago != 52 && lstFormaPagos[cnt].idFormaPago != 50 && lstFormaPagos[cnt].idFormaPago != 46 && lstFormaPagos[cnt].idFormaPago != 48 && lstFormaPagos[cnt].idFormaPago != 49 && lstFormaPagos[cnt].idFormaPago != 24 && lstFormaPagos[cnt].idFormaPago != 23)
                       // if (lstFormaPagos[cnt].idFormaPago == 27 || lstFormaPagos[cnt].idFormaPago == 28 || lstFormaPagos[cnt].idFormaPago == 29 || lstFormaPagos[cnt].idFormaPago == 30 || lstFormaPagos[cnt].idFormaPago == 36 )
                            ddlFormasPago.Items.Add(new ListItem(lstFormaPagos[cnt].dsFormaPago, lstFormaPagos[cnt].idFormaPago.ToString()));
                }
                IcFormaPago.Close();
                ddlFormasPago.DataBind();
        }

        public static void getBancos(DropDownList ddlBancos, string dsSession)
        {

            wscBanco.ListacBancos lstBancos;
            ddlBancos.Items.Clear();
            ddlBancos.Items.Add(new ListItem("Seleccione su Banco", ""));
            wscBanco.IcBancoClient IcBanco = new wscBanco.IcBancoClient();
            IcBanco.Open();
            lstBancos = IcBanco.SeleccionarcBancos(dsSession);
                               
            for (int cnt = 0; cnt < lstBancos.Count; cnt++)
            {
                ddlBancos.Items.Add(new ListItem(lstBancos[cnt].dsBanco, lstBancos[cnt].idBanco.ToString()));
            }

            IcBanco.Close();
            ddlBancos.DataBind();      
            // Trae todos los meses sin intereses
        }

         
        public static void getMoneda(DropDownList ddlMoneda, string dsSession)
        {
            try
            {
                ddlMoneda.Items.Clear();
                ddlMoneda.Items.Add(new ListItem("Seleccione tipo Moneda", ""));
                int intFechaHoy = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));

                wscMoneda.IcMonedaClient IcMoneda = new wscMoneda.IcMonedaClient();
                IcMoneda.Open();
                wscMoneda.ListacMonedas lstMonedas = IcMoneda.SeleccionarcMonedas(dsSession);
                IcMoneda.Close();

                //Session["lstMonedasAltaCotizacion"] = lstMonedas;
                //ViewState["lstMonedasAltaCotizacion"] = lstMonedas;

                wscTipoMoneda.IcTipoMonedaClient IcTipoMoneda = new wscTipoMoneda.IcTipoMonedaClient();
                IcTipoMoneda.Open();
                wscTipoMoneda.ListacTipoMoneda lstTipoMoneda = IcTipoMoneda.SeleccionarcTipoMoneda(dsSession);
                IcTipoMoneda.Close();

               //ViewState["lstTipoMonedaAltaCotizacion"] = lstTipoMoneda;

                int intConteo = 0;

                for (int cntMoneda = 0; cntMoneda < lstMonedas.Count; cntMoneda++)
                {
                    if (lstMonedas[cntMoneda].feInicial <= intFechaHoy && intFechaHoy <= lstMonedas[cntMoneda].feFinal)
                    {

                        for (int cntTipoMoneda = 0; cntTipoMoneda < lstTipoMoneda.Count; cntTipoMoneda++)
                        {
                            if (lstMonedas[cntMoneda].idTipoMoneda == lstTipoMoneda[cntTipoMoneda].idTipoMoneda)
                            {

                                ddlMoneda.Items.Add(new ListItem(lstTipoMoneda[cntTipoMoneda].dsTipoMoneda, lstMonedas[cntMoneda].idMoneda.ToString() + "-" + lstTipoMoneda[cntTipoMoneda].dsIso + "-" + lstTipoMoneda[cntTipoMoneda].idTipoMoneda));
                                // de un inicio está seleccionado que sea en pesos
                                if (lstMonedas[cntMoneda].idTipoMoneda == 2)
                                {
                                    ddlMoneda.SelectedIndex = intConteo + 1;                                     
                                }

                                intConteo++;                                
                            }
                        }

                    }
                }

                //if (Session["idMonedaAltaCotizacion" + hdConsecutivoSession.Value] != null)
                  //  ddlMoneda.SelectedValue = Session["idMonedaAltaCotizacion" + hdConsecutivoSession.Value].ToString();

            }
            catch (Exception err)
            {
               // mensaje("llenaddlMoneda", err);
            }
        }

        public static void LlenaTipoSangre(DropDownList ListaSangre)
        {
            wsGeneralServices.GeneralServicesClient gralCliente = new wsGeneralServices.GeneralServicesClient();
            wsGeneralServices.ListacCombo listaC = new wsGeneralServices.ListacCombo();
            listaC = gralCliente.SeleccionarcCombo("cTipoSangre", "idTipoSangre", "dsTipoSangre + dsfactor", "", "order by idTipoSangre", cGlobals.dsSession);
            
            ListaSangre.Items.Add(new ListItem("-- Seleccione Tipo Sangre--", ""));
            foreach (var elemento in listaC.cCombo)
            {
                ListItem item = new ListItem(elemento.dsValor, elemento.idValor.ToString());
                ListaSangre.Items.Add(item);
            }
            ListaSangre.DataBind();
            ListaSangre.SelectedIndex = 0;
        }

        public static void LlenaTipoSangre_en(DropDownList ListaSangre)
        {
            wsGeneralServices.GeneralServicesClient gralCliente = new wsGeneralServices.GeneralServicesClient();
            wsGeneralServices.ListacCombo listaC = new wsGeneralServices.ListacCombo();
            listaC = gralCliente.SeleccionarcCombo("cTipoSangre", "idTipoSangre", "dsTipoSangre + dsfactor", "", "order by idTipoSangre", cGlobals.dsSession);

            ListaSangre.Items.Add(new ListItem("-- Select blood type --", ""));
            foreach (var elemento in listaC.cCombo)
            {
                ListItem item = new ListItem(elemento.dsValor, elemento.idValor.ToString());
                ListaSangre.Items.Add(item);
            }
            ListaSangre.DataBind();
            ListaSangre.SelectedIndex = 0;
        }

        public static void getClientes(int idClientePadre, DropDownList clientes)
        {
            //wscNivelCliente.IcNivelClienteClient coneXion = new wscNivelCliente.IcNivelClienteClient();
            wscCliente.IcClienteClient conexion = new wscCliente.IcClienteClient();
            wscCliente.ListacClientes listClientes = new wscCliente.ListacClientes();
            //wscNivelCliente.ListacNivelClientes listNivelCliente = new wscNivelCliente.ListacNivelClientes();
            conexion.Open();
            listClientes = conexion.SeleccionarcClientesPorTipoConsulta("EVENTOS", cGlobals.dsSession);
            //listNivelCliente = coneXion.SeleccionarcNivelClientesPorIdNivelClientePadre(idClientePadre, cGlobals.dsSession);
            //listNivelCliente = coneXion.sele
            conexion.Close();
            clientes.Items.Clear();
            ListItem itemDefault = new ListItem("Seleccione Cliente", "");
            clientes.Items.Add(itemDefault);
            //clientes.DataSource = cGlobals.ListaNivelCliente;
            foreach (var data in listClientes)
            {
                if (data.idEstatusCliente == 1)
                {
                    ListItem listItem = new ListItem(data.dsRazonComercial, data.idCliente.ToString());
                    clientes.Items.Add(listItem);
                }
            }
            clientes.DataBind();
        }

        public static string getRFC(string strRFC, int intIdCliente =0)
        {
            wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();
            Dictionary<string, string> dicret = new Dictionary<string, string>();
            dicret.Add("@dsRFC", strRFC);
            dicret.Add("@idCliente", intIdCliente.ToString());
            DataSet datos = reglas.ExecuteRule("spGETRFCClientes", dicret, cGlobals.dsSession);
            return datos.Tables[0].Rows[0]["RetValue"].ToString();
        }

        public static bool enviaCorreo(string strCorreoCliente, string strTituloCorreo, string strMensajeMail, string strCopia)
        {
            try
            {
                wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();
                iGeneral.Open();
                bool blnBanderaCorreo = iGeneral.SendEmail(strCorreoCliente, strTituloCorreo, strMensajeMail, true /*es html*/,"", false, 0, strCopia);
                iGeneral.Close();
                return blnBanderaCorreo;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static DataSet buscarCompetidor(string dsSession, string apellidoPaterno, string nombre, string noCompetidor, int idVenta, string dsNoRifa, string dsCorreo, int idEventoClasificacion, int idEstatusVenta, int idEstatusParticipante = 15 /*15 inscrito*/, int idClienteContactoEvento = 0, string strFechaAlta = "")
        {
            wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();
            Dictionary<string, string> dicret = new Dictionary<string, string>();
            dicret.Clear();
            dicret.Add("@apellidoPaterno", apellidoPaterno);
            dicret.Add("@nombre", nombre);
            dicret.Add("@noCompetidor", noCompetidor.ToString());
            dicret.Add("@idVenta", idVenta.ToString());
            dicret.Add("@dsNoRifa", dsNoRifa);
            dicret.Add("@dsCorreo", dsCorreo);
            dicret.Add("@idEventoClasificacion", idEventoClasificacion.ToString());
            dicret.Add("@idEstatusVenta", idEstatusVenta.ToString());
            dicret.Add("@idEstatusParticipante", idEstatusParticipante.ToString());
            dicret.Add("@idClienteContactoEvento", idClienteContactoEvento.ToString());
            dicret.Add("@feAlta", strFechaAlta.ToString());
            DataSet datos = reglas.ExecuteRule("spGetCompetidoresTravesia", dicret, dsSession);
            dicret.Clear();
            reglas.Close();
            return datos;
        }

        public static wsTransactions.ListakVentaDetalles modificarVenta(wsTransactions.ListakVentaDetalles listaKventadetalle, int idVenta, int idVentaDetalle, int montoDiferencia = 0)
        {
            wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
            ikventa.Open();
            wsTransactions.kVenta laVenta = new wsTransactions.kVenta();
            wsTransactions.ListakVentaDetalles listaDetalle = new wsTransactions.ListakVentaDetalles();
            wsTransactions.ListakVentaDetalles listKventandetalleRespuesta;
            laVenta = ikventa.SeleccionarkVentaPorIdVenta(idVenta, cGlobals.dsSession);
            return listKventandetalleRespuesta = ikventa.ModificarkVentaLista(laVenta, listaKventadetalle, null, null, cGlobals.dsSession);
        }

        public static bool validaRFC(string strRFC)
        {
           string strCorrecta;
           Regex strvalid;
		   strCorrecta = strRFC;
		   if (strRFC.Length == 12){
		        strvalid = new Regex("^(([A-Z]|[a-z]|\\&){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))");
		   }else{
			    strvalid = new Regex("^(([A-Z]|[a-z]|\\s|\\&){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))");
		   }
		   //var validRfc =new RegExp(valid);
		   //var matchArray=strCorrecta.match(validRfc);
		   if (strvalid.IsMatch(strCorrecta))
		   {
			   return false;
		   }
		   else
		   {
			   return true;
		   }
        }        

        public static DataSet getPrecio(string dsSession, int idLocacion, string dsCodigo, int idTipoCliente = 1, bool cnConRemo = true, string dsIsoMoneda = "MXN")
        {
            wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();
            Dictionary<string, string> dicret = new Dictionary<string, string>();
            dicret.Clear();
            dicret.Add("@idCat", dsCodigo);
            dicret.Add("@idLocacion", idLocacion.ToString());
            dicret.Add("@idTipoCliente", idTipoCliente.ToString());
            dicret.Add("@cnProductoRemo", cnConRemo.ToString());
            dicret.Add("@isoMoneda", dsIsoMoneda);
            DataSet datos = reglas.ExecuteRule("spGetPrecioInscripcionTravesia", dicret, dsSession);
            dicret.Clear();
            reglas.Close();
            return datos;
        }

        
        public static string getJson(DataTable dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();

            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName.Trim(), dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        //Obtiene el dispositivo del usuario
        public static string getDevice()
        {
            HttpContext context = HttpContext.Current;
            Regex regExp;
            Regex regExpNeg;

            string agentUser = context.Request.ServerVariables["HTTP_USER_AGENT"].ToString();
            Dictionary<string, string> tablets = new Dictionary<string, string>();

            tablets.Add("Ipad", "Ipad");
            tablets.Add("Android", "Android");

            foreach (KeyValuePair<string, string> values in tablets)
            {

                regExp = new Regex(@values.Value, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                regExpNeg = new Regex(@"mobile", RegexOptions.IgnoreCase | RegexOptions.Multiline);

                if (regExp.IsMatch(agentUser) && !regExpNeg.IsMatch(agentUser))
                {
                    return "TABLETS";
                }
            }

            regExp = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            regExpNeg = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);

            if (regExp.IsMatch(agentUser) || regExpNeg.IsMatch(agentUser.Substring(0, 4)))
            {
                return "MOVIL";
            }

            return "PC / LAPTOP";
        }

        public static string GetIpAddress()  // Get IP Address
        {
            string strHostName = System.Net.Dns.GetHostName();
            //return System.Net.Dns.GetHostAddresses(strHostName).GetValue(1).ToString();            
            //string ip = "";
            string localIP = "";
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            //IPAddress[] addr = ipEntry.AddressList;
            //ip = addr[1].ToString();
            foreach (IPAddress ip2 in ipEntry.AddressList)
            {
                // si es una ip version 4, la guardamos en la ip a regresar
                if (ip2.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    localIP = ip2.ToString();
                    break;
                }
            }
            return localIP;
        }

        public static string getResolution()
        {
            return System.Windows.Forms.SystemInformation.PrimaryMonitorSize.Width.ToString() + "x" + System.Windows.Forms.SystemInformation.PrimaryMonitorSize.Height.ToString();            
        }

        public static DataTable getCEventoModalidad(string dsSession,string idModalidad = "", string dsClave = "", string tipoConsulta = "ALL")
        {
            DataTable result = new DataTable();
            wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();
            Dictionary<string, string> dicret = new Dictionary<string, string>();
            dicret.Add("@TIPOCONSULTA", tipoConsulta);
            dicret.Add("@idEventoModalidad", idModalidad);
            dicret.Add("@dsClave", dsClave);
            DataSet datos = reglas.ExecuteRule("spGETcEventoModalidad", dicret, dsSession);
            result = datos.Tables[0];
            return result;
        }

        public static string armaHTMLBaseCorreo(string strTitulo, string strCuerpo)
        {

            string strPagina = "";
            strPagina +=
            "<html xmlns=\"http://www.w3.org/1999/xhtml\" >";
            strPagina +=
            "<head>";
            strPagina +=
            "<title>";
            strPagina += strTitulo;
            strPagina +=
            "</title>";

            strPagina +=
            "<STYLE type=\"text/css\">";

            strPagina +=
            "body";

            strPagina +=
            "{";
            strPagina +=
            "font-family:\"arial\";";
            strPagina +=
            "}";
            strPagina +=
            "table.reference";
            strPagina +=
            "{";

            strPagina +=
            "background-color:#ffffff;";
            strPagina +=
            "border:1px solid #c3c3c3;";

            strPagina +=
            "border-collapse:collapse;";
            strPagina +=
            "width:100%;";
            strPagina +=
            "}";
            strPagina +=
            "table.reference th";

            strPagina +=
            "{";
            strPagina +=
            "background-color:#e5eecc;";
            strPagina +=
            "border:1px solid #c3c3c3;";
            strPagina +=
            "padding:3px;";
            strPagina +=
            "vertical-align:top;";
            strPagina +=
            "text-align:left";
            strPagina +=
            "font-size:80%;";
            strPagina +=
            "}";
            strPagina +=
            "table.reference td";
            strPagina +=
            "{";
            strPagina +=
            "border:1px solid #c3c3c3;";
            strPagina +=
            "padding:3px;";
            strPagina +=
            "vertical-align:top;";
            strPagina +=
            "font-size:70%;";
            strPagina +=
            "}";
            strPagina +=
            "</STYLE>";
            strPagina +=
            "</head>";
            strPagina +=
            "<body>";
            strPagina += "<table style=\"width: 654px; height: 101px\">";
            strPagina += "<tr>";
            strPagina += "<td colspan=\"3\" style=\"background-color: #89a55a; text-align: center\">";
            strPagina += "<strong><span style=\"color: #ede899\">";
            strPagina += strTitulo;
            strPagina += "</span></strong></td>";
            strPagina += "</tr>";
            strPagina += "<tr style=\"color: #000000\">";
            strPagina += "<td style=\"height: 33px\">";
            strPagina += "</td>";
            strPagina += "<td style=\"width: 214px; height: 33px\">";
            strPagina += "</td>";
            strPagina += "<td style=\"height: 33px\">";
            strPagina += "</td>";
            strPagina += "</tr>";
            strPagina += "<tr style=\"color: #000000\">";
            strPagina += "<td colspan=\"3\" style=\"height: 21px; background-color: #ffffff; \">";
            strPagina += strCuerpo;
            strPagina += "</td>";
            strPagina += "</tr>";
            strPagina += "<tr style=\"color: #000000\">";
            strPagina += "<td style=\"height: 33px\">";
            strPagina +=
            "</td>";
            strPagina +=
            "<td style=\"width: 214px; height: 63px\">";
            strPagina +=
            "</td>";
            strPagina +=
            "<td style=\"height: 33px\">";
            strPagina +=
            "</td>";
            strPagina +=
            "</tr>";
            strPagina += "<tr style=\"color: #000000\">";
            strPagina += "<td colspan=\"3\" style=\"background-color: #618442; text-align: right\">";
            strPagina += "<strong><span style=\"color: #ede899\">EXPERIENCIAS XCARET</span></strong></td>";
            strPagina += "</tr>";
            strPagina += "</table>";
            strPagina += "</body>";
            strPagina += "</html>";
            return strPagina;
        }

        public static int fnKillCompetidores(string dsSession)
        {
            wsBusinessRules.BusinessRulesServicesClient reglas = new wsBusinessRules.BusinessRulesServicesClient();
            Dictionary<string, string> dicret = new Dictionary<string, string>();
            dicret.Clear();            
            DataSet datos = reglas.ExecuteRule("spKillCanoerosEstatusRegistrado", dicret, dsSession);
            
            dicret.Clear();
            reglas.Close();
            if (datos.Tables[0].Rows[0]["retValue"].ToString() != "")
                return int.Parse(datos.Tables[0].Rows[0]["rowsAffected"].ToString());
            else
                return 0;
        }

        public static string strGetUrlCupon(string strsession, string strIdioma)
        {
            // Textos Correos
            wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
            icConfiguracion.Open();
            // wscConfiguracionAplicacion.cConfiguracionAplicacion Texto13CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto13CorreoPagoIdioma_" + strIdioma, Globals.dsSession);
            wscConfiguracionAplicacion.cConfiguracionAplicacion TextoUrlCuponIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-URLCUPON_CanalVenta_27_Id_" + strIdioma, strsession);
            //https://www.xperienciasxcaret.com/core/cupon/cuponnew.aspx?id=<idVenta>&base=true&lan=ES_MX
            icConfiguracion.Close();
            return TextoUrlCuponIdioma.dsValor.Trim();
        }
    }
}