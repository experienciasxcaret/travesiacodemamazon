﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace travesia
{
    public partial class respuestaGlobal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String strDsSession = "";
            String strOrderid = "";
            String strCanalventa = "";
            if (cGlobals.dsSession == null)
            {
                if (Request.QueryString["S"] == null)
                {
                    utils.Login("usrTriatlon", "eXperiencias");
                }
                else
                {
                    strDsSession = Request.QueryString["S"];
                    // Globals.dsSession = strDsSession;
                }
            }
            else
            {
                strDsSession = cGlobals.dsSession.ToString();
                ViewState["strDsSession"] = strDsSession;
            }
            if (Request.QueryString["O"] != null)
            {
                strOrderid = Request.QueryString["O"];
            }

            if (Request.QueryString["C"] != null)
            {
                strCanalventa = Request.QueryString["C"];
            }
            else
            {
                strCanalventa = ConfigurationManager.AppSettings["idCanalVenta"].ToString();
            }

            if (strOrderid.Length > 0)
            {
                wsTransactions.IkCajeroClient ikCajeroClient = new wsTransactions.IkCajeroClient();
                wsTransactions.GlobalCollect globalCollect = new wsTransactions.GlobalCollect();
                wsTransactions.GlobalCollectResponse globalCollectResponse = new wsTransactions.GlobalCollectResponse();

                string strNumTarjeta = "";
                globalCollect.CanalVentaId = strCanalventa;
                globalCollect.OrdenId = strOrderid;

                try
                {
                    globalCollectResponse = ikCajeroClient.GlobalCollect_GetOrderStatus(globalCollect, strDsSession);
                    //Globals.dsSession
                }
                catch (Exception err)
                {
                    try
                    {
                        globalCollectResponse = ikCajeroClient.GlobalCollect_GetOrderStatus(globalCollect, strDsSession);
                    }
                    catch (Exception ex)
                    {
                        lblUrl.Text = "Problemas Con el pago, intente de nuevo";
                        throw new Exception("Error en Pago GlobalCollect ", ex);
                    }

                    //lblUrl.Text = "Problemas Con el pago, intente de nuevo";// lblUrl.Text = err.Message + err.StackTrace;
                    //mensaje("Page_Load GlobalCollect_GetOrderStatus", err);
                    //return;
                }
                strNumTarjeta = globalCollectResponse.NumeroTarjeta;
                lblCalificacionFraude.Text = globalCollectResponse.CalificacionFraude;
                lblCodigoAutorizacion.Text = globalCollectResponse.CodigoAutorizacion;
                lblCodigoError.Text = globalCollectResponse.CodigoError;
                lblDescripcionError.Text = globalCollectResponse.DescripcionError;
                lblResultadoCVV.Text = globalCollectResponse.ResultadoCVV;
                lblNumeroTarjeta.Text = globalCollectResponse.NumeroTarjeta;
                lblNoReferencia.Text = globalCollectResponse.ReferenciaGlobalCollect;
                //globalCollectResponse.OrderId;
            }
        }
    }
}