﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;

namespace travesia
{
    public partial class respPago : System.Web.UI.Page
    {
        string strUrlCupon = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int intIdClienteContactoEvento, intidVenta = 0;
                string strIdVenta = "";
                DataSet result, resultCompetidor;
                DataSet dsDetalleVenta;
                utils.Login("usrTriatlon", "eXperiencias");
                int intUltimoPago = 0;
                //int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                int.TryParse(Request.QueryString["token"], out intidVenta);
                strIdVenta = Request.QueryString["token"];
                string[] arrIdVenta = strIdVenta.Split(',');

                hdIdVenta.Value = strIdVenta;
                strUrlCupon = utils.strGetUrlCupon(getDsSession(), "1");

                for (int il = 0; il < arrIdVenta.Length; il++)
                {
                    intidVenta = int.Parse(arrIdVenta[il]);
                    
                    if (intidVenta > 0)
                    {                        
                        #region Triatlon
                        if (il == 0)
                        {
                            wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                            ikventa.Open();
                            wsTransactions.kVenta laVenta = ikventa.SeleccionarkVentaPorIdVenta(intidVenta, getDsSession());
                            decimal strTotalIns = (decimal)laVenta.mnMontoTotal;
                            lblTotal.Text = strTotalIns.ToString("N2");
                            lnkFolio.Text = "Imprimir " + laVenta.dsClaveVenta;

                            wsTransactions.IkPagoClient IkPago = new wsTransactions.IkPagoClient();
                            wsTransactions.ListakPagos ListakPagos = IkPago.SeleccionarkPagoPorIdVenta(intidVenta, getDsSession());
                            wsTransactions.kPago objkPago = new wsTransactions.kPago();

                            for (int intIndicePago = ListakPagos.Count - 1; intIndicePago > 0; intIndicePago--)
                            {
                                if ((ListakPagos[intIndicePago].idEstatusPago == 2 || ListakPagos[intIndicePago].idEstatusPago == 24))
                                {
                                    intUltimoPago = intIndicePago;
                                    break;
                                }
                            }

                            if (intUltimoPago >= 0)
                            {
                                objkPago = ListakPagos[intUltimoPago];
                            }
                            else
                            {
                                objkPago.idVenta = intidVenta;
                            }

                            int intUltimaTransaccion = -1;
                            //wsTransactions.kPagoTransaccion kPagoTransaccion = IkPago.SeleccionarkPagosTransaccionesPorIdVenta(int.Parse(objkPago.idPago.ToString()), getDsSession());
                            wsTransactions.ListakPagoTransacciones listTransacciones = new wsTransactions.ListakPagoTransacciones();
                            wsTransactions.kPagoTransaccion PagoTran = new wsTransactions.kPagoTransaccion();
                            listTransacciones = IkPago.SeleccionarkPagosTransaccionesPorIdPago(int.Parse(objkPago.idPago.ToString()), getDsSession());

                            // se identifica la ultima transacción del último pago
                            for (int intIndiceTransaccion = 0; intIndiceTransaccion < listTransacciones.Count; intIndiceTransaccion++)
                            {
                                if ((listTransacciones[intIndiceTransaccion].idEstatusPago == 2))
                                {
                                    intUltimaTransaccion = intIndiceTransaccion;
                                }
                            }

                            if (intUltimaTransaccion > -1)
                            {
                                PagoTran = listTransacciones[intUltimaTransaccion];
                            }

                            wscTipoMoneda.IcTipoMonedaClient IcTipoMoneda = new wscTipoMoneda.IcTipoMonedaClient();
                            IcTipoMoneda.Open();
                            wscTipoMoneda.cTipoMoneda cTipoMoneda = IcTipoMoneda.SeleccionarcTipoMonedaPorId((int)PagoTran.idTipoMoneda, getDsSession());
                            IcTipoMoneda.Close();
                            //cTipoMoneda.dsIso;
                            lblTotal.Text += " "+cTipoMoneda.dsIso;
                            lblNoReferencia.Text = PagoTran.dsTransaccion;
                            lblFechaPago.Text = DateTime.Parse(PagoTran.feAlta.ToString()).ToString("dd/MM/yyyy");
                            //result = utils.buscarCompetidor("", "", 0, 0, "", "", 0, 0, 0, intIdClienteContactoEvento);
                            resultCompetidor = utils.buscarCompetidor(getDsSession(),"", "", "", intidVenta, "", "", 0, 0, 0, 0);
                            if (resultCompetidor.Tables[0].Rows[0]["retValue"].ToString() == "1")
                            {
                                //cParticipante.dtCompedidor = resultCompetidor.Tables[0];
                                hdCorreoCompetidor.Value = resultCompetidor.Tables[0].Rows[0]["dsCorreoElectronico"].ToString();

                                wsBusinessRules.BusinessRulesServicesClient ruleOper = new wsBusinessRules.BusinessRulesServicesClient();
                                Dictionary<string, string> dicret = new Dictionary<string, string>();

                                lblNombre.Text = resultCompetidor.Tables[0].Rows[0]["dsContacto"].ToString() + " " + resultCompetidor.Tables[0].Rows[0]["dsApellidoPaterno"].ToString() + " " + resultCompetidor.Tables[0].Rows[0]["dsApellidoMaterno"].ToString();
                                lblCategoria.Text = resultCompetidor.Tables[0].Rows[0]["dsProducto"].ToString();
                                lblNumeroCompetidor.Text = resultCompetidor.Tables[0].Rows[0]["dsNumeroCompetidor"].ToString();

                                string strBaseDatos = (ConfigurationManager.AppSettings["bdProduccion"]);
                                string strUrlCuponFinal = "";
                                // https//www.aolxcaret.com/core/cupon/cuponnew.aspx?id=181236&base=false&lan=ES_MX
                                //lnkFolio.NavigateUrl = "https://www.aolxcaret.com/core/cupon/cuponnew.aspx?id=" + laVenta.idVenta.ToString() + "&base=" + strBaseDatos + "&lan=ES_MX";
                                strUrlCuponFinal = strUrlCupon.Replace("<idVenta>", laVenta.idVenta.ToString());
                                lnkFolio.NavigateUrl = strUrlCuponFinal;
                            }
                        }
                        #endregion
                        #region donativo
                        /*if (il == 1)
                        {
                            dvEncabezadoDonativo.Visible = true;
                            wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                            ikventa.Open();
                            wsTransactions.kVenta laVenta = ikventa.SeleccionarkVentaPorIdVenta(intidVenta, getDsSession());

                            lblTotalDonativo.Visible = true;
                            decimal strTotal = (decimal)laVenta.mnMontoTotal;
                            lblTotalPagoDonativo.Text = strTotal.ToString("N2");
                            lblLinkFolio.Visible = true;
                            lnkFolioDonativo.Text = "click para imprimir "+ laVenta.dsClaveVenta;

                            wsTransactions.IkPagoClient IkPago = new wsTransactions.IkPagoClient();
                            wsTransactions.ListakPagos ListakPagos = IkPago.SeleccionarkPagoPorIdVenta(intidVenta, getDsSession());
                            wsTransactions.kPago objkPago = new wsTransactions.kPago();

                            for (int intIndicePago = ListakPagos.Count - 1; intIndicePago > 0; intIndicePago--)
                            {
                                if ((ListakPagos[intIndicePago].idEstatusPago == 2 || ListakPagos[intIndicePago].idEstatusPago == 24))
                                {
                                    intUltimoPago = intIndicePago;
                                    break;
                                }
                            }

                            if (intUltimoPago >= 0)
                            {
                                objkPago = ListakPagos[intUltimoPago];
                            }
                            else
                            {
                                objkPago.idVenta = intidVenta;
                            }

                            int intUltimaTransaccion = -1;
                            //wsTransactions.kPagoTransaccion kPagoTransaccion = IkPago.SeleccionarkPagosTransaccionesPorIdVenta(int.Parse(objkPago.idPago.ToString()), getDsSession());
                            wsTransactions.ListakPagoTransacciones listTransacciones = new wsTransactions.ListakPagoTransacciones();
                            wsTransactions.kPagoTransaccion PagoTran = new wsTransactions.kPagoTransaccion();
                            listTransacciones = IkPago.SeleccionarkPagosTransaccionesPorIdPago(int.Parse(objkPago.idPago.ToString()), getDsSession());

                            // se identifica la ultima transacción del último pago
                            for (int intIndiceTransaccion = 0; intIndiceTransaccion < listTransacciones.Count; intIndiceTransaccion++)
                            {
                                if ((listTransacciones[intIndiceTransaccion].idEstatusPago == 2))
                                {
                                    intUltimaTransaccion = intIndiceTransaccion;
                                }
                            }

                            if (intUltimaTransaccion > -1)
                            {
                                PagoTran = listTransacciones[intUltimaTransaccion];
                            }

                            wscTipoMoneda.IcTipoMonedaClient IcTipoMoneda = new wscTipoMoneda.IcTipoMonedaClient();
                            IcTipoMoneda.Open();
                            wscTipoMoneda.cTipoMoneda cTipoMoneda = IcTipoMoneda.SeleccionarcTipoMonedaPorId((int)PagoTran.idTipoMoneda, getDsSession());
                            IcTipoMoneda.Close();
                            //cTipoMoneda.dsIso;
                            lblTotalPagoDonativo.Text += " " + cTipoMoneda.dsIso;

                            lblAutorizacionDonativo.Visible = true;
                            lblFechaDonativo.Visible = true;
                            lblAutorizacionPagoDonativo.Text = PagoTran.dsTransaccion;
                            lblFechaPagoDonativo.Text = DateTime.Parse(PagoTran.feAlta.ToString()).ToString("dd/MM/yyyy");
                            string strBaseDatos = (ConfigurationManager.AppSettings["bdProduccion"]);
                            //result = utils.buscarCompetidor("", "", 0, 0, "", "", 0, 0, 0, intIdClienteContactoEvento);
                           /* resultCompetidor = utils.buscarCompetidor("", "", 0, intidVenta, "", "", 0, 0, 0, 0);
                            if (resultCompetidor.Tables[0].Rows[0]["retValue"].ToString() == "1")
                            {
                                //cParticipante.dtCompedidor = resultCompetidor.Tables[0];
                                hdCorreoCompetidor.Value = resultCompetidor.Tables[0].Rows[0]["dsCorreoElectronico"].ToString();

                                wsBusinessRules.BusinessRulesServicesClient ruleOper = new wsBusinessRules.BusinessRulesServicesClient();
                                Dictionary<string, string> dicret = new Dictionary<string, string>();

                                lblNombre.Text = resultCompetidor.Tables[0].Rows[0]["dsContacto"].ToString() + " " + resultCompetidor.Tables[0].Rows[0]["dsApellidoPaterno"].ToString() + " " + resultCompetidor.Tables[0].Rows[0]["dsApellidoMaterno"].ToString();
                                lblCategoria.Text = resultCompetidor.Tables[0].Rows[0]["dsProducto"].ToString();
                                lblNumeroCompetidor.Text = resultCompetidor.Tables[0].Rows[0]["noCompetidor"].ToString();

                                
                                // https//www.aolxcaret.com/core/cupon/cuponnew.aspx?id=181236&base=false&lan=ES_MX
                                //lnkFolio.NavigateUrl = "https://www.aolxcaret.com/core/cupon/cuponnew.aspx?id=" + laVenta.idVenta.ToString() + "&base=" + strBaseDatos + "&lan=ES_MX";
                            }*/
                       /*     lnkFolioDonativo.Visible = true;
                            lnkFolioDonativo.NavigateUrl = "https://www.aolxcaret.com/core/cupon/cuponnew.aspx?id=" + laVenta.idVenta.ToString() + "&base=" + strBaseDatos + "&lan=ES_MX";
                        }*/
                        #endregion
                    }                        
                }                                
            }
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
                //hdDsSession.Value = getDsSession();
            }
            return cGlobals.dsSession;
        }
    }
}