﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxEditors;
using System.Configuration;
using DevExpress.Web.ASPxGridView;

namespace travesia
{
    public partial class buscaPreinscritos : System.Web.UI.Page
    {
        DataSet result;
        DataTable dtCompetidor;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.MaintainScrollPositionOnPostBack = true;
                int noCapacidad = 0;
                int noUsados = 0;
                int intIdClienteContactoEvento = 0;
                string strDsNoRifa = "";
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "btnContinuarA_Click")
                { }
                if (!IsPostBack)
                {
                    utils.Login("usrTriatlon", "eXperiencias");
                    Session["dsSession"] = cGlobals.dsSession;
                    hdDsSession.Value = cGlobals.dsSession;
                    gvCompetidoresPre.Visible = false;
                }
            }
            catch (Exception ex)
            {
                mensaje("Problema en carga de pagina "+ ex.Message, "Ha ocurrido un error");
            }
        }

        

        protected int getMaxAgrupador(DataTable dtCompetidor)
        {
            int maxAgrupador = 0;
            maxAgrupador = Convert.ToInt32(dtCompetidor.Compute("max(noAgrupadorVenta)", "cnModificacion = 0"));
            return maxAgrupador;
        }
    
       
        public void mensaje(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }

        public void mensajeyOcultarDiv(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessageAndHide('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }
        
        public void mostrarOcultarDiv(string divMostrar, string divOcultar, string divOcultar2)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>mostrarOcultarDiv('" + divMostrar + "','" + divOcultar + "','" + divOcultar2 + "');</script>", false);
        }
        public void alert(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + errMensaje + "');</script>", false);
        }

        public string getDsSession()
        {

            /*Session["dsSession"] = cGlobals.dsSession;
                    hdDsSession.Value = cGlobals.dsSession;
             */
            if (Session["dsSession"] == null)
            {
                Session["dsSession"] = hdDsSession.Value;
            }
            return Session["dsSession"].ToString();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                string strPaterno = "";
                string strMaterno = "";
                strPaterno = txtPaterno.Text;
                lblMensajeRespuesta.Text = "";
                //lblMensajeRespuesta.Text = "El competidor " + strPaterno + " " + strMaterno + " no está en la lista de pre inscritos.";
                DataSet result = cParticipante.getPreInscritos(getDsSession(),strPaterno,strMaterno);
                dtCompetidor = result.Tables[0];
                gvCompetidoresPre.Visible = true;
                gvCompetidoresPre.DataSource = result.Tables[0];
                if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                {
                    gvCompetidoresPre.DataBind();

                }
                else
                {
                    lblMensajeRespuesta.Text = result.Tables[0].Rows[0]["dsNombre"].ToString();
                }
            }
            catch(Exception ex)
            { 
            }
        }

        protected void gvCompetidoresPre_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                /*if (e.RowType == GridViewRowType.Data)
                {
                    int Fila = e.VisibleIndex;
                    string dsLiga = "";
                    ASPxGridView grid = sender as ASPxGridView;
                    dsLiga = grid.GetRowValues(Fila, new string[] { "dsLiga" }).ToString().Trim();
                    ASPxHyperLink lnkLiga = new ASPxHyperLink();
                    ASPxHyperLink txtDescuentoSelecc = grid.FindRowCellTemplateControl(Fila, grid.Columns["dsLiga"] as GridViewDataColumn, "lnkLiga") as ASPxHyperLink;
                    lnkLiga.Text = dsLiga;
                    lnkLiga.NavigateUrl = dsLiga;
                    
                }*/                               
            }
            catch (Exception ex)
            { 
            }
        }

        protected void btnLink_Click(object sender, EventArgs e)
        {
            ASPxButton btnEditar = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = btnEditar.NamingContainer as GridViewDataItemTemplateContainer;
            string strMasterRowKeyValue = container.KeyValue.ToString();
            if (strMasterRowKeyValue != "")
            {
                Response.Redirect(strMasterRowKeyValue);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>window.open('" + strMasterRowKeyValue + "', '_blank')</script>", false);
            }
        }               
    }
}