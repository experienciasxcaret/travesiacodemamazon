﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxEditors;
using System.Configuration;

namespace travesia
{
    public partial class envioNoGanadores : System.Web.UI.Page
    {
        DataSet result;
        DataTable dtCompetidor;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.MaintainScrollPositionOnPostBack = true;
                int noCapacidad = 0;
                int noUsados = 0;
                int intIdClienteContactoEvento = 0;
                string strDsNoRifa = "";
                string parameter = Request["__EVENTARGUMENT"];
                string[] strValor;
                if (parameter == "btnContinuarA_Click")
                { }
                if (!IsPostBack)
                {
                    dtCompetidor = null;
                    int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                    strDsNoRifa = Request.QueryString["token"];
                    //cnPrecargado.Value = intIdClienteContactoEvento.ToString();
                    //hdIdClienteContactoEventoPrincipales.Value = intIdClienteContactoEvento.ToString();
                    utils.Login("usrTriatlon", "eXperiencias");
                    wsGeneralServices.GeneralServicesClient gralCliente = new wsGeneralServices.GeneralServicesClient();
                    wsGeneralServices.ListacCombo listaC = new wsGeneralServices.ListacCombo();
                    listaC = gralCliente.SeleccionarcCombo("cClienteContactoEvento CE", "idClienteContactoEvento", "UPPER(isnull(CC.dsContacto,'')) + ' ' + UPPER(isnull(CC.dsApellidoPaterno,'')) +' ' + UPPER(isnull(CC.dsApellidoMaterno,'')) + '|'+ CC.dsCorreoElectronico + '|'+ dsNoRifa", "inner join  cClienteContacto CC(nolock) on CE.idClienteContacto = CC.idClienteContacto inner join cClienteDetalle cCD (noLock) on cCD.idClienteDetalle = CC.idClienteDetalle where YEAR(CE.feAlta)=2015 and ISNULL(dsnorifa,'')<>'' and cCD.idEstatusCliente = 17 AND CE.idEquipo IS NULL", "order by CE.idClienteContactoEvento", cGlobals.dsSession);
                   
                    for (int cnt = 0; cnt < listaC.cCombo.Count(); cnt++)
                    {
                        //listaPais.Items.Add(new ListItem(listaC.cCombo[cnt].dsValor, listaC.cCombo[cnt].idValor.ToString()));
                        
                            strValor = listaC.cCombo[cnt].dsValor.ToString().Trim().Split('|');
                            string mensajeMail = cGlobals.notifNoGanadorRifa2;
                            mensajeMail = mensajeMail.Replace("[nombre]", strValor[0].ToString().Trim());
                            mensajeMail = mensajeMail.Replace("[numero]", strValor[2].ToString().Trim());
                            bool respEnvioCorreo = utils.enviaCorreo(strValor[1].ToString().Trim(), "Sorteo de la rifa Triatlón Xel-Há", mensajeMail, "");
                        
                        //if (listaC.cCombo[cnt].idValor == 484)                    
                    }
                    gralCliente.Close();
                }
            }
            catch (Exception ex)
            {
                mensaje("Problema en carga de pagina "+ ex.Message, "Ha ocurrido un error");
            }
        }
       
        public void mensaje(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }

        public void mensajeyOcultarDiv(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessageAndHide('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }
        
        public void mostrarOcultarDiv(string divMostrar, string divOcultar, string divOcultar2)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>mostrarOcultarDiv('" + divMostrar + "','" + divOcultar + "','" + divOcultar2 + "');</script>", false);
        }
        public void alert(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + errMensaje + "');</script>", false);
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.dsSession;
        }               
    }
}