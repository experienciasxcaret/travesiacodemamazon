﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testActivex.aspx.cs" Inherits="travesia.testActivex" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10" />
    <title></title>
</head>
<script>
    function LaunchApp() {
        if (!document.all) {
            alert("Available only with Internet Explorer.");
            return;
        }
        
        alert("abriendo, favor de esperar");
        var objShell = new ActiveXObject("shell.application");

        objShell.ShellExecute("C:\\ejecutable\\mitFormApp.exe", '441340 16 28 2 e0gbam2hoay2myk21x54ajcy', "", "open", 1);
    }
</script>
<body>
    <form id="form1" runat="server">
    <div>                
        <asp:Button ID="btnLaunchActivex" runat="server" Text="Click launch activex" class="btn btn-info btn-mini" OnClientClick="javascript:LaunchApp()"  UseSubmitBehavior="true" AutoPostBack="False" name="NameBtncontinuarB" />
    </div>
    </form>
</body>
</html>
