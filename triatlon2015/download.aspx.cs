﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
namespace travesia
{
    public partial class download : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getFiles();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Create a stream for the file
            Stream stream = null;
            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 30000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];
            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create("https://www.xperienciasxcaret.mx/core/archivos/Travesia/Storage/pictures/folio prueba occidental 190884.pdf");

                //File Path and File Name                
                string _DownloadableProductFileName = "folio prueba occidental 190884.pdf";

                //Create a response for this request
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileResp.ContentLength > 0)
                {
                    //fileResp.ContentLength = fileReq.ContentLength;
                    //Get the Stream returned from the response
                    stream = fileResp.GetResponseStream();

                    // prepare the response to the client. resp is the client Response
                    var resp = HttpContext.Current.Response;

                    //Indicate the type of data being sent
                    resp.ContentType = "application/octet-stream";

                    //Name the file 
                    resp.AddHeader("Content-Disposition", "attachment; filename=\"" + _DownloadableProductFileName + "\"");
                    resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                    int length;
                    do
                    {
                        // Verify that the client is connected.
                        if (resp.IsClientConnected)
                        {
                            // Read data into the buffer.
                            length = stream.Read(buffer, 0, bytesToRead);

                            // and write it out to the response's output stream
                            resp.OutputStream.Write(buffer, 0, length);

                            // Flush the data
                            resp.Flush();

                            //Clear the buffer
                            buffer = new Byte[bytesToRead];
                        }
                        else
                        {
                            // cancel the download if client has disconnected
                            length = -1;
                        }
                    } while (length > 0); //Repeat until no data is read

                }
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
            
        }
        //Function for File Download in ASP.Net in C# and 
        //Tracking the status of success/failure of Download.
        private bool DownloadableProduct_Tracking()
        {
            string filePath = Server.MapPath("https://www.xperienciasxcaret.mx/core/archivos/Travesia/Storage/pictures");
            string _DownloadableProductFileName = "folio prueba occidental 190884.pdf";
            System.IO.FileInfo FileName = new System.IO.FileInfo(filePath + "\\" + _DownloadableProductFileName);
            FileStream myFile = new FileStream(filePath + "\\" + _DownloadableProductFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            //Reads file as binary values
            BinaryReader _BinaryReader = new BinaryReader(myFile);

            //Ckeck whether user is eligible to download the file
            if (true)
            {
                //Check whether file exists in specified location
                if (FileName.Exists)
                {
                    try
                    {
                        long startBytes = 0;
                        string lastUpdateTiemStamp = File.GetLastWriteTimeUtc(filePath).ToString("r");
                        string _EncodedData = HttpUtility.UrlEncode(_DownloadableProductFileName, Encoding.UTF8) + lastUpdateTiemStamp;

                        Response.Clear();
                        Response.Buffer = false;
                        Response.AddHeader("Accept-Ranges", "bytes");
                        //Response.AppendHeader("ETag", "\"" + _EncodedData + "\"");
                        Response.AppendHeader("Last-Modified", lastUpdateTiemStamp);
                        Response.AppendHeader("content-disposition", "attachment; filename=" + filePath);
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName.Name);
                        Response.AddHeader("Content-Length", (FileName.Length - startBytes).ToString());
                        Response.AddHeader("Connection", "Keep-Alive");
                        Response.ContentEncoding = Encoding.UTF8;

                        //Send data
                        _BinaryReader.BaseStream.Seek(startBytes, SeekOrigin.Begin);

                        //Dividing the data in 1024 bytes package
                        int maxCount = (int)Math.Ceiling((FileName.Length - startBytes + 0.0) / 1024);

                        //Download in block of 1024 bytes
                        int i;
                        for (i = 0; i < maxCount && Response.IsClientConnected; i++)
                        {
                            Response.BinaryWrite(_BinaryReader.ReadBytes(1024));
                            Response.Flush();
                        }
                        //if blocks transfered not equals total number of blocks
                        if (i < maxCount)
                            return false;
                        return true;
                    }
                    catch
                    {
                        return false;
                    }
                    finally
                    {
                        Response.End();
                        _BinaryReader.Close();
                        myFile.Close();
                    }
                }
                else System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(),
                    "FileNotFoundWarning", "alert('File is not available now!')", true);
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(),
                    "NotEligibleWarning", "alert('Sorry! File is not available for you')", true);
            }
            return false;
        }

        public static string GetDirectoryListingRegexForUrl(string url)
        {
            if (url.Equals("https://www.xperienciasxcaret.mx/core/archivos/Travesia/Storage/files/17500/"))
            {
                //return "<a href=\".*\">(?<name>.*)</a>";
                return "\\\"([^\"]*)\\\"";
            }
            throw new NotSupportedException();
        }

        public static void getFiles()
        {
            string url = "https://www.xperienciasxcaret.mx/core/archivos/Travesia/Storage/files/17500/";
            Uri uri = new Uri("https://www.xperienciasxcaret.mx/core/archivos/Travesia/Storage/files/17500/Folio 180394 tour xplor costco.pdf");
            //string url = "http://ServerDirPath/";

            //string baseURL = "http://download.example.org/export/dump/";
            WebClient client = new WebClient();
            string content = client.DownloadString(url);
            //byte[] data = client.DownloadData(uri);

            //var files = Directory.GetFiles(url);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            int intContMatch = 0;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    string html = reader.ReadToEnd();
                    
                    Regex regex = new Regex(GetDirectoryListingRegexForUrl(url));
                    MatchCollection matches = regex.Matches(html);
                    if (matches.Count > 0)
                    {
                        foreach (Match match in matches)
                        {                            
                            // vamos a ignorar el primer match, ya que el primero es la raiz
                            if (intContMatch > 0)
                            {
                                if (match.Success)
                                {
                                    string[] arrMatches = match.ToString().Trim().Replace("\"", "").Split('/');
                                    int intSizeArrayMatches = arrMatches.Length;
                                    string fileName = arrMatches[intSizeArrayMatches - 1].ToString().Trim();
                                    var extension = Path.GetExtension(fileName).ToLower();
                                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url + fileName);
                                    HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                                    DateTime abcd = res.LastModified;
                                    //Console.WriteLine(match.ToString());                                    
                                }
                            }
                            intContMatch = intContMatch + 1;
                        }
                    }
                    //reader.Close();
                }
                //Console.ReadLine();
            }
        }
       
    }
}