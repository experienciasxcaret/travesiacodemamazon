﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace travesia {
    
    
    public partial class respuestaGlobal {
        
        /// <summary>
        /// form1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// lblEtiquetaCalfificacionFraude control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblEtiquetaCalfificacionFraude;
        
        /// <summary>
        /// lblCalificacionFraude control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblCalificacionFraude;
        
        /// <summary>
        /// lblEtiquetaCodigoAutorizacion control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblEtiquetaCodigoAutorizacion;
        
        /// <summary>
        /// lblCodigoAutorizacion control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblCodigoAutorizacion;
        
        /// <summary>
        /// lblEtiquetaCodigoError control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblEtiquetaCodigoError;
        
        /// <summary>
        /// lblCodigoError control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblCodigoError;
        
        /// <summary>
        /// lblEtiquetaDescripcionError control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblEtiquetaDescripcionError;
        
        /// <summary>
        /// lblDescripcionError control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblDescripcionError;
        
        /// <summary>
        /// lblEtiquetaResultadoCVV control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblEtiquetaResultadoCVV;
        
        /// <summary>
        /// lblResultadoCVV control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblResultadoCVV;
        
        /// <summary>
        /// lblEtiquetaNumeroTarjeta control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblEtiquetaNumeroTarjeta;
        
        /// <summary>
        /// lblNumeroTarjeta control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblNumeroTarjeta;
        
        /// <summary>
        /// lblEtiquetaNoReferencia control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblEtiquetaNoReferencia;
        
        /// <summary>
        /// lblNoReferencia control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblNoReferencia;
        
        /// <summary>
        /// lblUrl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblUrl;
    }
}
