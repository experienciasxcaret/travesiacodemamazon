﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxEditors;
using System.Configuration;

namespace travesia
{
    public partial class inscripcionRifa : System.Web.UI.Page
    {
        
        const String callbackRegistrarCompetidor = "registrarCompetidor();";
        const String callbackValidaCompetidor = "validarCompetidor();";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.MaintainScrollPositionOnPostBack = true;
                int noCapacidad = 0;
                int noUsados = 0;
                int lugaresDisponibles = 0;
                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "btnContinuarA_Click")
                    btnContinuarA_Click(sender, e);
                
                    if (!IsPostBack)
                    {
                        utils.Login("usrTriatlon", "eXperiencias");
                        DataTable dtCEventoModalidad = utils.getCEventoModalidad(getDsSession(),null, "TXRI", "DSCLAVE");
                        noCapacidad = int.Parse(dtCEventoModalidad.Rows[0]["noCapacidad"].ToString());
                        noUsados = int.Parse(dtCEventoModalidad.Rows[0]["noUsados"].ToString());
                        lugaresDisponibles = noCapacidad - noUsados;

                        if (noCapacidad > noUsados)
                        {
                            utils.llenaAnio(ddanio);
                            //utils.llenaDias(int.Parse(ddanio.SelectedValue.ToString()), int.Parse(ddlMes.SelectedValue.ToString()), ddldia);
                            utils.llenaPais(ddlPais);
                            utils.llenaEstados(ddlEstado, ddlPais.SelectedValue.ToString(), getDsSession());
                            btnTerminar.JSProperties["cpIsMessage"] = "";
                        }
                        else
                        {
                            btnContinuarA.Visible = false;
                            lblMensajeCabecera.Text = "Cupo agotado para inscripción a Solo Novatas";
                            mensaje("Cupo agotado para inscripción Solo Novatas. Gracias por tu visita", "CUPOS AGOTADOS");
                        }
                    }                
            }
            catch (Exception ex)
            {
                mensaje("Problema en carga de pagina "+ ex.Message, "Ha ocurrido un error");
            }
        }

        protected void ddlPais_TextChanged(object sender, EventArgs e)
        {            
            utils.llenaEstados(ddlEstado ,ddlPais.SelectedValue.ToString(), getDsSession());
        }

        protected void btnTerminar_Click(object sender, EventArgs e)
        {
            cParticipante objectParticipante = new cParticipante();        

            string[] strIdEventoClasificacion = ddlCategoria.SelectedValue.ToString().Split('-');
            int intIdEventoClasificacion = int.Parse(strIdEventoClasificacion[0].ToString());
            objectParticipante.nombre = txtNombre.Text;
            objectParticipante.apellidoPaterno = txtApellidoPaterno.Text;
            objectParticipante.apellidoMaterno = txtApellidoMaterno.Text;
            objectParticipante.email = txtEmail.Text;
            objectParticipante.idSexo = int.Parse(ddSexo.SelectedValue.ToString());
            objectParticipante.municipio = txtCiudad.Text.ToString();
            objectParticipante.numInterior = (txtNoInterior.Text.ToString());
            objectParticipante.numExterior = (txtNoExterior.Text.ToString());
            objectParticipante.calle = txtCalle.Text.ToString();
            objectParticipante.colonia = txtColonia.Text.ToString();
            objectParticipante.ciudad = txtCiudad.Text.ToString();
            objectParticipante.fechaNacimiento = ddanio.SelectedValue.ToString() + "/" + ddlMes.SelectedValue.ToString() + "/" + ddldia.SelectedValue.ToString();
            objectParticipante.idPais = int.Parse(ddlPais.SelectedValue.ToString());
            objectParticipante.pais = ddlPais.SelectedItem.ToString();
            objectParticipante.idEstado = int.Parse(ddlEstado.SelectedValue.ToString());
            objectParticipante.estado = ddlEstado.SelectedItem.ToString();
            objectParticipante.telefono1 = txtTelefonofijo1.Text + txtTelefonofijo2.Text;
            objectParticipante.telefono2 = txtCelular1.Text + txtCelular1.Text;
            objectParticipante.idEventoClasificacion = intIdEventoClasificacion;
            objectParticipante.dsEventoClasificacion = ddlCategoria.SelectedItem.ToString();
            objectParticipante.vecesParticipaciones = int.Parse(ddlNoParticipaciones.SelectedValue.ToString());
            objectParticipante.idTipoMoneda = 2;
            objectParticipante.noRifa = utils.getNoRifa(intIdEventoClasificacion);

            // YA TENEMOS LLENA LAS PROPIEDADES DE NUESTRO OBJETO PARTICIPANTE.
            // AHORA HAY QUE GUARDARLAS.  
            //var listResp = objectParticipante.guardarCompetidor();
            /*if (listResp.Count > 0)
            {
                //btnTerminar.JSProperties    
                btnTerminar.JSProperties["cpIsMessage"] = "hola";
            }
            else
            {
                
            }*/
        }

        protected void ddlMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            utils.llenaDias(int.Parse(ddanio.SelectedValue.ToString()), int.Parse(ddlMes.SelectedValue.ToString()),ddldia);
        }

        protected void ddanio_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddanio.SelectedValue.ToString() != "")
            utils.llenaDias(int.Parse(ddanio.SelectedValue.ToString()), int.Parse(ddlMes.SelectedValue.ToString()), ddldia);
            DateTime fechaEvento = new DateTime(2015, 12, 31);
            int anio = 1900;
            int mes = 01;
            int dia = 01;
            int.TryParse(ddanio.SelectedValue.ToString(), out anio);
            int.TryParse(ddlMes.SelectedValue.ToString(), out mes);
            int.TryParse(ddldia.SelectedValue.ToString(), out dia);
            anio = (anio > 0) ? anio : 1900;
            mes = (mes > 0) ? mes : 01;
            dia = (dia > 0) ? dia : 01;
            DateTime fechaNacimiento = new DateTime(anio, mes, dia);
            int intSexo = ((ddSexo.SelectedValue) == "") ? 2 : int.Parse(ddSexo.SelectedValue);
            int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;
            btnContinuarA.Visible = true;
            lblMensajeCabecera.Text = "";
            /*if (edad <= 11)
            {
                DataTable dtCEventoModalidad = utils.getCEventoModalidad(null, "TXIN", "DSCLAVE");
                int noCapacidad = int.Parse(dtCEventoModalidad.Rows[0]["noCapacidad"].ToString());
                int noUsados = int.Parse(dtCEventoModalidad.Rows[0]["noUsados"].ToString());
                int lugaresDisponibles = noCapacidad - noUsados;

                if (noCapacidad <= noUsados)
                {
                    //btnContinuarA.Visible = false;
                    //lblMensajeCabecera.Visible = true;
                    lblMensajeCabecera.Text = "Cupo agotado para inscripción a rifa para Infantiles 9-11 años.";  
                    btnContinuarA.Visible = false;
                    alert("Cupo agotado para inscripción a rifa para Infantiles 9-11 años.");                   
                }
            }*/
        }

        protected void btnContinuarA_Click(object sender, EventArgs e)
        {
            string strHdCnValidation = hdCnValidation.Value;

            //if (strHdCnValidation != "0")
            //{
                string strhola = "hola";
                DateTime fechaEvento = new DateTime(2015, 12, 31);
                int anio = 1900;
                int mes = 01;
                int dia = 01;
                int.TryParse(ddanio.SelectedValue.ToString(), out anio);
                int.TryParse(ddlMes.SelectedValue.ToString(), out mes);
                int.TryParse(ddldia.SelectedValue.ToString(), out dia);

                anio = (anio > 0) ? anio : 1900;
                mes = (mes > 0) ? mes : 01;
                dia = (dia > 0) ? dia : 01;
                DateTime fechaNacimiento = new DateTime(anio, mes, dia);
                int intSexo = ((ddSexo.SelectedValue) == "") ? 2 : int.Parse(ddSexo.SelectedValue);
                int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;
                utils.getCategorias(getDsSession(),intSexo, edad, ddlCategoria, null, "TXRI", 1);

                ListItem crItem = null;
                crItem = ddlCategoria.Items.FindByText("Olimpico Relevo  Femenino / W");

                if (crItem != null)
                    ddlCategoria.Items.FindByText("Olimpico Relevo  Femenino / W").Enabled = true;

                crItem = ddlCategoria.Items.FindByText("Sprint Relevo  Femenino / SW");
                if (crItem != null)
                    ddlCategoria.Items.FindByText("Sprint Relevo  Femenino / SW").Enabled = true;

                crItem = ddlCategoria.Items.FindByText("Olimpico Relevo  Masculino / M");
                if (crItem != null)
                    ddlCategoria.Items.FindByText("Olimpico Relevo  Masculino / M").Enabled = true;

                crItem = ddlCategoria.Items.FindByText("Sprint Relevo Masculino / SM");
                if (crItem != null)
                    ddlCategoria.Items.FindByText("Sprint Relevo Masculino / SM").Enabled = true;

                // masculino
                if (intSexo == 2)
                {
                    crItem = ddlCategoria.Items.FindByText("Olimpico Relevo  Femenino / W");
                    if (crItem != null)
                        ddlCategoria.Items.FindByText("Olimpico Relevo  Femenino / W").Enabled = false;

                
                    crItem = ddlCategoria.Items.FindByText("Sprint Relevo  Femenino / SW");
                    if (crItem != null)
                        ddlCategoria.Items.FindByText("Sprint Relevo  Femenino / SW").Enabled = false;
                }
                if (intSexo == 3)
                {
                    crItem = ddlCategoria.Items.FindByText("Olimpico Relevo  Masculino / M");
                    if (crItem != null)
                        ddlCategoria.Items.FindByText("Olimpico Relevo  Masculino / M").Enabled = false;

                    crItem = ddlCategoria.Items.FindByText("Sprint Relevo Masculino / SM");
                    if (crItem != null)
                        ddlCategoria.Items.FindByText("Sprint Relevo Masculino / SM").Enabled = false;
                }
                lblMensajeCabecera.Text = "";
                btnContinuarB.Visible = true;
                //aqui vamos a preguntar si la edad es mayor de once para que se muestre el combo de tallas de playeras
                if (edad > 11)
                {
                    ddlTallasPlayeras.Visible = true;
                    lblTamanioPlayera.Visible = true;
                    //Nos hace falta llenar el combo cuanto esten en la base las tallasx
                    utils.getTallas(ddlTallasPlayeras, getDsSession());
                    utils.getTallas(ddlPlayera2, getDsSession());
                    utils.getTallas(ddlPlayera3, getDsSession());
                }
                else
                {
                    ddlTallasPlayeras.Visible = false;
                    lblTamanioPlayera.Visible = false;
                }
                /*else 
                {
                    //
                    DataTable dtCEventoModalidad = utils.getCEventoModalidad(null, "TXIN", "DSCLAVE");
                    int noCapacidad = int.Parse(dtCEventoModalidad.Rows[0]["noCapacidad"].ToString());
                    int noUsados = int.Parse(dtCEventoModalidad.Rows[0]["noUsados"].ToString());
                    int lugaresDisponibles = noCapacidad - noUsados;

                    if (noCapacidad <= noUsados)
                    {
                        //btnContinuarA.Visible = false;
                        //lblMensajeCabecera.Visible = true;                        
                        btnContinuarB.Visible = false;
                        //mensaje("Cupo agotado para inscripción a rifa para Infantiles 9-11 años. Gracias por participar", "CUPOS AGOTADOS");
                        mostrarOcultarDiv("form_paso_a", "form_paso_b", "form_paso_c");
                    }

                }*/
            //}
                       
        }

        protected void ddlCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] strIdCategoria = ddlCategoria.SelectedValue.ToString().Split('-');
                ddlSexo2.Items.FindByValue("2").Enabled = true;
                ddlSexo2.Items.FindByValue("3").Enabled = true;
                ddlSexo3.Items.FindByValue("2").Enabled = true;
                ddlSexo3.Items.FindByValue("3").Enabled = true;
                if (strIdCategoria[0] == "24" || strIdCategoria[0] == "25" || strIdCategoria[0] == "26" || strIdCategoria[0] == "55" || strIdCategoria[0] == "56" || strIdCategoria[0] == "57")
                {
                    lblRelevo2.Visible = true;
                    lblNombreRelevo2.Visible = true;
                    txtNombre2.Visible = true;
                    lblPaternoRelevo2.Visible = true;
                    txtPaterno2.Visible = true;
                    lblMaternoRelevo2.Visible = true;
                    txtMaterno2.Visible = true;
                    lblSexoRelevo2.Visible = true;
                    ddlSexo2.Visible = true;
                    lblPlayeraRelevo2.Visible = true;
                    ddlPlayera2.Visible = true;

                    lblRelevo3.Visible = true;
                    lblNombreRelevo3.Visible = true;
                    txtNombre3.Visible = true;
                    lblPaternoRelevo3.Visible = true;
                    txtPaterno3.Visible = true;
                    lblMaternoRelevo3.Visible = true;
                    txtMaterno3.Visible = true;
                    lblSexoRelevo3.Visible = true;
                    ddlSexo3.Visible = true;
                    lblPlayeraRelevo3.Visible = true;
                    ddlPlayera3.Visible = true;
                    // es relevo femenino
                    if (strIdCategoria[0] == "25" || strIdCategoria[0] == "56")
                    {
                        ddlSexo2.Items.FindByValue("2").Enabled = false;
                        ddlSexo3.Items.FindByValue("2").Enabled = false;
                        //ddlSexo2.Items.FindByText("Olimpico Relevo  Masculino - M").Enabled = false
                    }
                    // es relevo masculino
                    if (strIdCategoria[0] == "26" || strIdCategoria[0] == "57")
                    {
                        ddlSexo2.Items.FindByValue("3").Enabled = false;
                        ddlSexo3.Items.FindByValue("3").Enabled = false;
                        //ddlSexo2.Items.FindByText("Olimpico Relevo  Masculino - M").Enabled = false
                    }
                }
                else
                {
                    
                    lblRelevo2.Visible = false;
                    lblNombreRelevo2.Visible = false;
                    txtNombre2.Visible = false;
                    lblPaternoRelevo2.Visible = false;
                    txtPaterno2.Visible = false;
                    lblMaternoRelevo2.Visible = false;
                    txtMaterno2.Visible = false;
                    lblSexoRelevo2.Visible = false;
                    ddlSexo2.Visible = false;
                    lblPlayeraRelevo2.Visible = false;
                    ddlPlayera2.Visible = false;

                    lblRelevo3.Visible = false;
                    lblNombreRelevo3.Visible = false;
                    txtNombre3.Visible = false;
                    lblPaternoRelevo3.Visible = false;
                    txtPaterno3.Visible = false;
                    lblMaternoRelevo3.Visible = false;
                    txtMaterno3.Visible = false;
                    lblSexoRelevo3.Visible = false;
                    ddlSexo3.Visible = false;
                    lblPlayeraRelevo3.Visible = false;
                    ddlPlayera3.Visible = false;
                }
            }
            catch(Exception ex)
            {}
        }

        protected void btnTerminar_Init(object sender, EventArgs e)
        {
            ASPxButton btnTerminar = sender as ASPxButton;
            btnTerminar.Attributes.Add("onClick", String.Format(callbackRegistrarCompetidor));
        }

        protected void cbRegistrar_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            try
            {
                btnTerminar.Enabled = false;
                cParticipante objectParticipante = new cParticipante();
                cParticipante objectParticipante2 = new cParticipante();
                cParticipante objectParticipante3 = new cParticipante();
                int intContParticipantes = 0;
                string[] strDsEventoClasificacion;
                bool cnValidaApellidoFeNacimiento = true;
                string apellidoPaterno = txtApellidoPaterno.Text.Trim();
                string apellidoMaterno = txtApellidoMaterno.Text.Trim();
                string feNacimiento = ddanio.SelectedValue.Trim() + '-' + ddlMes.SelectedValue.Trim() + '-' + ddldia.SelectedValue.Trim();
                string strFeNacimiento = ddldia.SelectedValue.Trim() + " de " + ddlMes.SelectedItem.Text + " de " + ddanio.SelectedValue.Trim(); 
                cnValidaApellidoFeNacimiento = utils.validaExisteRegistroApellidosYFeNacimiento(apellidoPaterno, apellidoMaterno, feNacimiento, getDsSession());
                if (cnValidaApellidoFeNacimiento)
                {
                    #region categoriaSeleccionada
                    if (ddlCategoria.SelectedValue.ToString() != "")
                    {
                        intContParticipantes++;
                        string[] strIdEventoClasificacion = ddlCategoria.SelectedValue.ToString().Split('-');
                        int intIdEventoClasificacion = int.Parse(strIdEventoClasificacion[0].ToString());
                        int intNoRifa = utils.getNoRifa(intIdEventoClasificacion);
                        objectParticipante.nombre = txtNombre.Text;
                        objectParticipante.apellidoPaterno = txtApellidoPaterno.Text;
                        if (txtApellidoMaterno.Text != "")
                            objectParticipante.apellidoMaterno = txtApellidoMaterno.Text;
                        objectParticipante.email = txtEmail.Text;
                        objectParticipante.idSexo = int.Parse(ddSexo.SelectedValue.ToString());
                        if (txtCiudad.Text != "")
                            objectParticipante.municipio = txtCiudad.Text.ToString();
                        if (txtNoInterior.Text != "")
                            objectParticipante.numInterior = (txtNoInterior.Text.ToString());
                        if (txtNoExterior.Text != "")
                            objectParticipante.numExterior = (txtNoExterior.Text.ToString());
                        if (txtCalle.Text != "")
                            objectParticipante.calle = txtCalle.Text.ToString();
                        if (txtColonia.Text != "")
                            objectParticipante.colonia = txtColonia.Text.ToString();
                        if (txtCiudad.Text != "")
                            objectParticipante.ciudad = txtCiudad.Text.ToString();
                        objectParticipante.fechaNacimiento = ddanio.SelectedValue.ToString() + "/" + ddlMes.SelectedValue.ToString() + "/" + ddldia.SelectedValue.ToString();
                        if (ddlPais.SelectedValue.ToString() != "")
                        {
                            objectParticipante.idPais = int.Parse(ddlPais.SelectedValue.ToString());
                            objectParticipante.pais = ddlPais.SelectedItem.ToString();
                        }
                        if (ddlEstado.SelectedValue.ToString() != "")
                        {
                            objectParticipante.idEstado = int.Parse(ddlEstado.SelectedValue.ToString());
                            objectParticipante.estado = ddlEstado.SelectedItem.ToString();
                        }
                        if (txtTelefonofijo1.Text != "")
                            objectParticipante.telefono1 = txtTelefonofijo1.Text + txtTelefonofijo2.Text;
                        objectParticipante.telefono2 = txtCelular1.Text + txtCelular2.Text;
                        objectParticipante.idEventoClasificacion = intIdEventoClasificacion;
                        strDsEventoClasificacion = ddlCategoria.SelectedItem.ToString().Split('/');
                        objectParticipante.dsEventoClasificacion = strDsEventoClasificacion[0].ToString().Trim();
                        objectParticipante.dsCodigoEventoClasificacion = strDsEventoClasificacion[1].ToString().Trim();
                        objectParticipante.vecesParticipaciones = int.Parse(ddlNoParticipaciones.SelectedValue.ToString());
                        objectParticipante.idTipoMoneda = 2;
                        objectParticipante.noRifa = intNoRifa;
                        if (ddlTallasPlayeras.SelectedValue != "")
                        {
                            objectParticipante.idTallaPlayera = int.Parse(ddlTallasPlayeras.SelectedValue.ToString());
                            objectParticipante.dsTallaPlayera = ddlTallasPlayeras.SelectedItem.ToString();
                        }
                        objectParticipante.dsAgrupador = "Triatlon";
                        objectParticipante.idEstatusCliente = 17; // registrado
                        objectParticipante.dsAgrupador = "rifa";
                        objectParticipante.llenarLista(objectParticipante);
                        // YA TENEMOS LLENA LAS PROPIEDADES DE NUESTRO OBJETO PARTICIPANTE.

                        // CHECAMOS SI TIENE RELEVOS2
                        if (txtNombre2.Visible == true)
                        {
                            intContParticipantes++;
                            objectParticipante2.nombre = txtNombre2.Text;
                            objectParticipante2.apellidoMaterno = txtMaterno2.Text;
                            objectParticipante2.apellidoPaterno = txtPaterno2.Text;
                            objectParticipante2.idSexo = int.Parse(ddlSexo2.SelectedValue.ToString());
                            objectParticipante2.idTipoMoneda = 2;
                            objectParticipante2.noRifa = intNoRifa;
                            objectParticipante2.dsAgrupador = "Triatlon";
                            objectParticipante2.idEventoClasificacion = intIdEventoClasificacion;
                            objectParticipante2.idTallaPlayera = int.Parse(ddlPlayera2.SelectedValue.ToString());
                            objectParticipante2.dsTallaPlayera = ddlPlayera2.SelectedItem.ToString();
                            objectParticipante2.dsEventoClasificacion = strDsEventoClasificacion[0].ToString().Trim();
                            objectParticipante2.dsCodigoEventoClasificacion = strDsEventoClasificacion[1].ToString().Trim();
                            objectParticipante2.idEstatusCliente = 17; // registrado
                            objectParticipante2.dsAgrupador = "rifa";
                            objectParticipante.llenarLista(objectParticipante2);
                        }
                        // CHECAMOS SI TIENE RELEVOS3
                        if (txtNombre3.Visible == true)
                        {
                            intContParticipantes++;
                            objectParticipante3.nombre = txtNombre3.Text;
                            objectParticipante3.apellidoMaterno = txtMaterno3.Text;
                            objectParticipante3.apellidoPaterno = txtPaterno3.Text;
                            objectParticipante3.idSexo = int.Parse(ddlSexo3.SelectedValue.ToString());
                            objectParticipante3.idTipoMoneda = 2;
                            objectParticipante3.noRifa = intNoRifa;
                            objectParticipante3.dsAgrupador = "Triatlon";
                            objectParticipante3.idEventoClasificacion = intIdEventoClasificacion;
                            objectParticipante3.idTallaPlayera = int.Parse(ddlPlayera3.SelectedValue.ToString());
                            objectParticipante3.dsTallaPlayera = ddlPlayera3.SelectedItem.ToString();
                            objectParticipante3.dsEventoClasificacion = strDsEventoClasificacion[0].ToString().Trim();
                            objectParticipante3.dsCodigoEventoClasificacion = strDsEventoClasificacion[1].ToString().Trim();
                            objectParticipante3.idEstatusCliente = 17; // registrado
                            objectParticipante3.dsAgrupador = "rifa";
                            objectParticipante.llenarLista(objectParticipante3);
                        }
                        // AHORA HAY QUE GUARDARLAS. 

                        // Antes de guardar al competidor, checar si todavía hay lugares
                        DataTable dtCEventoModalidad = utils.getCEventoModalidad(getDsSession(), null, "TXRI", "DSCLAVE");
                        int noCapacidad = int.Parse(dtCEventoModalidad.Rows[0]["noCapacidad"].ToString());
                        int noUsados = int.Parse(dtCEventoModalidad.Rows[0]["noUsados"].ToString());
                        int lugaresDisponibles = noCapacidad - noUsados;
                        bool blDisponibilidadLugares = true;
                        if (lugaresDisponibles >= intContParticipantes)
                        {
                            blDisponibilidadLugares = true;
                        }
                        else
                        {
                            blDisponibilidadLugares = false;
                        }
                        //
                        if (blDisponibilidadLugares)
                        {
                            
                            List<object> listResp = new List<object>();

                            try
                            {
                                listResp = objectParticipante.guardarCompetidor(objectParticipante.listCompetidores, getDsSession(), getIdCliente(), getIdUsuario());
                            }
                            catch (Exception ex)
                            {
                                string msg = ex.Message.ToString();
                                string strginnermessage = "";
                                string strCuerpoCorreo = "";

                                strCuerpoCorreo = "session = " + cGlobals.dsSession + "<br>IdCliente " + cGlobals.idCliente + "<br> idUsuario " + cGlobals.idUsuario + "<br>bd " + ConfigurationManager.AppSettings["bdProduccion"].ToString() + "<br>inscripcionRifa. guardarCompetidor() " + msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                                string strExcepcion = "";
                                if (ex.ToString().Length > 999)
                                    strExcepcion = ex.ToString().Substring(0, 1000);
                                else
                                    strExcepcion = ex.ToString();
                                strCuerpoCorreo += strExcepcion;
                                MensajeAlert(strCuerpoCorreo);
                                string strCuerpoCorreo2 = "nombre: " + objectParticipante.nombre+ " " + objectParticipante.apellidoPaterno + " " + objectParticipante.apellidoMaterno;
                                strCuerpoCorreo2 += "<br>email " + objectParticipante.email;
                                strCuerpoCorreo2 += "<br>sexo " + objectParticipante.idSexo;
                                strCuerpoCorreo2 += "<br>fenacimiento " + objectParticipante.fechaNacimiento;
                                strCuerpoCorreo2 += "<br>fenacimiento " + objectParticipante.fechaNacimiento;
                                strCuerpoCorreo2 += "<br>colonia " + objectParticipante.colonia;
                                strCuerpoCorreo2 += "<br>calle " + objectParticipante.calle;
                                strCuerpoCorreo2 += "<br>interior " + objectParticipante.numInterior;
                                strCuerpoCorreo2 += "<br>exterior " + objectParticipante.numExterior;
                                strCuerpoCorreo2 += "<br>pais " + objectParticipante.idPais;
                                strCuerpoCorreo2 += "<br>estado " + objectParticipante.idEstado;
                                strCuerpoCorreo2 += "<br>ciudad " + objectParticipante.ciudad;
                                strCuerpoCorreo2 += "<br>telefono1 " + objectParticipante.telefono1;
                                strCuerpoCorreo2 += "<br>telefono2 " + objectParticipante.telefono2;
                                strCuerpoCorreo2 += "<br>telefono2 " + objectParticipante.telefono2;
                                strCuerpoCorreo2 += "<br>categoria " + objectParticipante.idEventoClasificacion;
                                strCuerpoCorreo2 += "<br>talla " + objectParticipante.idTallaPlayera;
                                strCuerpoCorreo2 += "<br>participaciones " + objectParticipante.vecesParticipaciones;
                                MensajeAlert(strCuerpoCorreo2);
                                cbRegistrar.JSProperties["cpIsMessage"] = ex.ToString();
                            }
                            objectParticipante.VaciarLista();
                            int respIdClienteContactoEvento = int.Parse(listResp[0].ToString());
                            int respNoRifaComperidor = int.Parse(listResp[1].ToString());
                            string respDsNoRifaCompetidor = listResp[2].ToString();
                            if (respNoRifaComperidor > 0)
                            {
                                // ahora vamos a contabilizar el numero de inscritos por rifa y guardarlo en 
                                // campo noUsados de la tabla cEventoModalidad
                                //cParticipante.agregaNoUsados(intContParticipantes, "TXRI");

                                // Ahora preguntamos si es infantil de 9-11, para agregarlos a noUsados
                                if (intIdEventoClasificacion == 53)
                                    //cParticipante.agregaNoUsados(intContParticipantes, "TXIN");

                                //btnTerminar.JSProperties                                        
                                if (txtEmail.Text != "")
                                {
                                    string strNombreCompleto = objectParticipante.nombre + " " + objectParticipante.apellidoPaterno + " " + objectParticipante.apellidoMaterno;
                                    string mensajeMail = cGlobals.notifRegistroRifa_1;
                                    mensajeMail = mensajeMail.Replace("[nombre]", strNombreCompleto).Replace("[numero]", respDsNoRifaCompetidor);
                                    bool respEnvioCorreo = utils.enviaCorreo(txtEmail.Text, "Inscripcion a la rifa Triatlón Xel-Há", mensajeMail, "fimox.reformer@gmail.com");
                                    //iGeneral.SendEmail(Email, "Inscripcion Triatlón Xel-Há", mensajeMail, true, "", true, 0, "infotriatlon@experienciasxcaret.com.mx");
                                    //iGeneral.Close();
                                    if (respEnvioCorreo)
                                    {
                                        string loc = "";
                                        Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                                        loc = myUri.AbsoluteUri.ToString();
                                        loc = loc.Replace("inscripcionRifa.aspx", "");
                                        loc += "respRifa.aspx?token=" + respDsNoRifaCompetidor.ToString() + "&contEvent=" + respIdClienteContactoEvento.ToString();
                                        btnTerminar.Visible = false;
                                        btnTerminar.ClientVisible = false;
                                        //Response.Redirect("respRifa.aspx?token=" + respDsNoRifaCompetidor.ToString() + "&contEvent=" + respIdClienteContactoEvento.ToString());
                                        cbRegistrar.JSProperties["cpIsMessage"] = "0 - Número de rifa generado : " + respDsNoRifaCompetidor + ". Se le envió correo a : " + txtEmail.Text + "-" + loc;

                                    }
                                }
                            }
                            else
                            {
                                btnTerminar.Enabled = true;
                                cbRegistrar.JSProperties["cpIsMessage"] = "1- Error al generar número de rifa, vuelva a intentarlo";
                            }
                        }
                        else
                        {
                            //mensaje("Cupo agotado para inscripción a rifa. Gracias por participar", "CUPOS AGOTADOS");
                            btnTerminar.Enabled = true;
                            cbRegistrar.JSProperties["cpIsMessage"] = "2- Espacios agotados. Gracias por participar";
                        }
                    }
                    else
                    {
                        btnTerminar.Enabled = true;
                        cbRegistrar.JSProperties["cpIsMessage"] = "1- Seleccione categoria";
                    }
                    #endregion
                }
                else {
                    btnTerminar.Enabled = true;
                    cbRegistrar.JSProperties["cpIsMessage"] = "3- Ya Existe registro con apellido " + apellidoPaterno + apellidoMaterno + " y fecha de nacimiento " + strFeNacimiento;
                }
            }
            catch (Exception ex)
            {
             //   mensaje("Problema en carga de pagina", exx);
                string msg = ex.Message.ToString();
                string strginnermessage = "";
                string strCuerpoCorreo = msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                string strExcepcion = "";
                if (ex.ToString().Length > 999)
                    strExcepcion = ex.ToString().Substring(0, 1000);
                else
                    strExcepcion = ex.ToString();
                strCuerpoCorreo += strExcepcion;
                MensajeAlert(strCuerpoCorreo);
                cbRegistrar.JSProperties["cpIsMessage"] = ex.ToString();
            }
        }        

        protected void cbValida_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            try
            {   
                DateTime fechaEvento = new DateTime(2015, 12, 31);
                int anio = 1900;
                int mes = 01;
                int dia = 01;
                int.TryParse(ddanio.SelectedValue.ToString(), out anio);
                int.TryParse(ddlMes.SelectedValue.ToString(), out mes);
                int.TryParse(ddldia.SelectedValue.ToString(), out dia);

                anio = (anio > 0) ? anio : 1900;
                mes = (mes > 0) ? mes : 01;
                dia = (dia > 0) ? dia : 01;
                DateTime fechaNacimiento = new DateTime(anio, mes, dia);

                int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;
                utils.getCategorias(getDsSession(),Convert.ToInt16(ddSexo.SelectedValue), edad, ddlCategoria, null, "TXI", 1);
                bool respValidacion = utils.validaExisteRegistro(txtEmail.Text, getDsSession());                

                if (respValidacion)
                {
                    cbValida.JSProperties["cpValidacion"] = "true";
                }
                else 
                {
                    cbValida.JSProperties["cpValidacion"] = "false";
                }
            }
            catch (Exception ex)
            {
                //   mensaje("Problema en carga de pagina", exx);
                //cbRegistrar.JSProperties["cpIsMessage"] = ex.ToString();
                cbValida.JSProperties["cpValidacion"] = "";
            }
        }

        protected void btnContinuarA_Init(object sender, EventArgs e)
        {
            ASPxButton btnContinuarA = sender as ASPxButton;
            btnContinuarA.Attributes.Add("onClick", String.Format(callbackValidaCompetidor));
        }

        protected void txtEmail2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtEmail.Text != txtEmail2.Text)
                {
                    lblValidacionEmail.Text = "Los correos, no coinciden, favor de validarlo";
                }
                else
                {
                    lblValidacionEmail.Text = "";                    
                }
            }
            catch (Exception ex)
            { 
            }
        }
        public void mensaje(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }

        public void mensajeyOcultarDiv(string errMensaje, string encabezado)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessageAndHide('errorMsg','" + encabezado + "','" + errMensaje + "');</script>", false);
        }
        
        public void mostrarOcultarDiv(string divMostrar, string divOcultar, string divOcultar2)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>mostrarOcultarDiv('" + divMostrar + "','" + divOcultar + "','" + divOcultar2 + "');</script>", false);
        }
        public void alert(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + errMensaje + "');</script>", false);
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.dsSession;
        }

        public int getIdUsuario()
        {
            if (cGlobals.idUsuario == null || cGlobals.idUsuario == 0)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.idUsuario;
        }

        public int getIdCliente()
        {
            if (cGlobals.idCliente == null || cGlobals.idCliente == 0)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.idCliente;
        }

        public void MensajeAlert(string errMensaje)
        {

            wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();
            iGeneral.Open();
            string strCorreos = "faraujo@experienciasxcaret.com.mx; nfigueroa@experienciasxcaret.com.mx";
            bool blnBanderaCorreo = iGeneral.SendEmail(strCorreos, "Error en Aplicacion Triatlon", errMensaje, true /*es html*/, "", false, 0, "");
            iGeneral.Close();
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + strCuerpoCorreo + "');", false);
        }
    }
}