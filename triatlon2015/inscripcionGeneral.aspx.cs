﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using DevExpress.Web.ASPxEditors;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Image = System.Drawing.Image;
using System.Web.Services;
using System.Globalization;
using System.Threading;
using System.Net;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Text;

namespace travesia
{
    public partial class inscripcionGeneral : System.Web.UI.Page
    {
        
        const String callbackRegistrarCompetidor = "registrarCompetidor();";
        const String callbackValidaCompetidor = "validarCompetidor();";
        cParticipante cCompetidorGeneral = new cParticipante();
      
     
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                DataSet result;
                result = null;
                Page.MaintainScrollPositionOnPostBack = true;                
                int intIdClienteContactoEvento = 0;
                string strCat = "";

                string parameter = Request["__EVENTARGUMENT"];
                if (parameter == "btnContinuarA_Click")
                    btnContinuarA_Click(sender, e);

                if (parameter == "uploadImage")
                    uploadImage();

                /*if (parameter == "uploadFiles")
                    uploadFiles();

                if (parameter == "btnTerminar_Click")
                    btnTerminar_Click(sender, e);*/

                if (parameter == "btnSubmit_Click1")
                    btnSubmit_Click1(sender, e);

                if (Request["submit"] == "submit")
                {
                    Response.Write(Request.Files.Count);
                }
                


                if (!IsPostBack)
                {

                    string loc = "";
                    string loc2 = "";
                    Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                    loc = myUri.AbsoluteUri.ToString();
                    loc2 = myUri.AbsoluteUri.ToString();
                    string[] strspliturl = loc.Split('/');                    
                    string strHost = HttpContext.Current.Request.Url.Host;
                    string strPrimerosCaracteres = loc.Substring(0, 5);
                    string requestType = myUri.Scheme;

                    /*HttpContext context = HttpContext.Current;
                    if (!context.Request.IsSecureConnection)
                    {
                        UriBuilder secureUrl = new UriBuilder(loc);
                        secureUrl.Scheme = "https";
                        secureUrl.Port = 443;
                        context.Response.Redirect(secureUrl.ToString(), false);
                    }*/
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + requestType + "');</script>", false);
                    /*Uri address8 = new Uri(loc);
                    if (address8.Scheme != Uri.UriSchemeHttps)
                    {
                        loc = loc.Replace("http", "https");
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>window.location ='" + loc + "'</script>", false);
                        return;
                    }*/
                    /*if (!Request.IsSecureConnection)
                    {
                        loc = loc.Replace("http", "https");
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + loc + "');window.location ='" + loc + "'</script>", false);
                        return;
                    }*/
                    //if (Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]) && strHost != "localhost")
                    //if (strHost != "localhost")
                   // {
                       /* if (strPrimerosCaracteres.ToUpper() == "HTTP:")
                        {
                            //loc = loc.Replace("http", "https");
                            loc.Trim().Insert(0, "https://");
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + strPrimerosCaracteres + "');alert('" + loc + "');</script>", false);
                            return;
                            //Response.Redirect(loc, true);
                            //loc2 = loc2.Replace("http", "https");
                            //Response.Redirect(Request.RawUrl, true);
                        }*/
                        /*else if (strPrimerosCaracteres != "https")
                        {
                            loc.Trim().Insert(0, "https://");
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>window.location ='" + loc + "'</script>", false);
                            return;
                            //Response.Redirect(loc, true);
                            //loc2.Trim().Insert(0, "https://");
                        }*/
                   // }

                    Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = null;
                    cParticipante.cnCena = 0;
                    cCompetidorGeneral.DtCompetidor = null;
                    string prevPage = "";
                    string[] arrPre;
                    int intlengArrPre = 0;

                    DateTime dt = DateTime.Now;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("es-MX");                    
                    //Console.WriteLine(dt.ToString("d"));
                    if (!Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]))
                        lblFechaActual.InnerText = "Ambiente para testing. " + dt.ToString("D");
                    else
                        lblFechaActual.InnerText = dt.ToString("D")+".";

                    if (Request.UrlReferrer != null)
                    {
                        prevPage = Request.UrlReferrer.AbsolutePath.ToString();
                        arrPre = prevPage.Split('/');
                        intlengArrPre = arrPre.Length;
                        prevPage = arrPre[intlengArrPre - 1];
                        //lblMensajeValidacion.Text = prevPage;
                        Console.WriteLine(prevPage);
                        if (prevPage == "cena.aspx")
                        {
                            cParticipante.cnCena = 1;
                            strCat = "PES";
                            hdCena.Value = "1";
                        }
                        if (prevPage == "cenaEsp.aspx")
                        {
                            cParticipante.cnCena = 1;
                            strCat = "CENAESP";
                            hdCena.Value = "1";
                        }
                    }
                    cCompetidorGeneral.DtCompetidor = null;
                    int.TryParse(Request.QueryString["contEvent"], out intIdClienteContactoEvento);
                    if (Request.QueryString["cat"] != null)
                        strCat = Request.QueryString["cat"];
                    hdStrCat.Value = strCat;                    
                    cnIdContactoPrecargado.Value = intIdClienteContactoEvento.ToString();
                    utils.getTallas(ddlGralTallasPlayeras, getDsSession());
                    ASPxListBox lstbx = (ASPxListBox)ddlGralNoParticipaciones3.FindControl("lbxAniosParticipaciones");
                    utils.llenaAniosTravesia(lstbx);                   

                    // Aqui debe de preguntar si viene de cena O con intIdClienteContactoEvento > 0, 
                    // así si y solo si lo dejo seguir en la pantalla general
                    #region validacionDeCenaOPrecargado
                   // if (hdCena.Value == "1" || intIdClienteContactoEvento > 0)
                    //{
                        utils.Login("usrTriatlon", "eXperiencias");
                        hdDsSession.Value = cGlobals.dsSession;
                        utils.llenaAnio(ddlGralAnio);
                        utils.llenaPais(ddlGralPais);
                        utils.LlenaTipoSangre(ddlTipoSangre);
                        utils.getCategorias(getDsSession(),1, 1, ddlGralCategoria, null, "ALL", 1);
                        utils.llenaEstados(ddlGralEstado, ddlGralPais.SelectedValue.ToString(), getDsSession());
                        List<string> listaAgotados = new List<string>();
                        string strCuerpoListaAgotados = "";
                        listaAgotados = utils.getLocaciones(getDsSession(), ddlLocaciones);
                        if (listaAgotados != null)
                        {
                            if (listaAgotados.Count > 0)
                            {
                                foreach (string strAgotado in listaAgotados)
                                {
                                    strCuerpoListaAgotados += "<div class='modulo-slider'>";
                                    strCuerpoListaAgotados += "<p>" + strAgotado.Trim() + "</p>";
                                    strCuerpoListaAgotados += "</div>";
                                }
                                //strCuerpoListaAgotados += "<div class='modulo-slider'>";
                                //strCuerpoListaAgotados += "<p></p>";
                                //strCuerpoListaAgotados += "</div>";
                                divSliderAgotados.Visible = true;
                                divBloqueAgotados.InnerHtml = strCuerpoListaAgotados;
                            }
                        }
                        utils.getParentescos(getDsSession(), ddlParentesco);
                        utils.getUnidadNegocioColaborador(getDsSession(), ddlUnidadNegocioColaborador);
                        // si intIdClienteContactoEvento es mayor a cero quiere decir que viene de una precarga
                        // y hay que poner los datos ya precargados
                        if (intIdClienteContactoEvento > 0)
                        {
                            result = utils.buscarCompetidor(getDsSession(), "", "", "", 0, "", "", 0, 0, 0, intIdClienteContactoEvento);

                            if (result.Tables[0].Rows[0]["retValue"].ToString() == "1")
                            {
                                // si es un cliente con estatus de preasignado, entonces podrá seguir con el proceso
                                // if (result.Tables[0].Rows[0]["retValue"].ToString() == "18")
                                if (result.Tables[0].Rows[0]["idEstatusCliente"].ToString() == "16")
                                {
                                    cCompetidorGeneral.DtCompetidor = null;
                                    cCompetidorGeneral.DtCompetidor = result.Tables[0];
                                    Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = null;
                                    Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = result.Tables[0];
                                    try
                                    {
                                        llenaFormulario(result.Tables[0]);
                                    }
                                    catch (Exception ex)
                                    {
                                        mensaje("Problema en el llenado del formulario " + ex.Message);
                                    }
                                }
                                else if (result.Tables[0].Rows[0]["idEstatusCliente"].ToString() == "15")
                                {
                                    alertRedirect("El competidor ya se encuentra inscrito al Triatlon. Gracias");
                                }
                                else
                                {
                                    alertRedirect("Lo sentimos, no has sido Pre-Asignado para seguir el proceso. Gracias");
                                }
                            }
                        }
                    #endregion
                        ViewState["filesDocuments0"] = null;
                }                
            }
            catch (Exception ex)
            {
                mensaje("Problema en la carga de pagina "+ ex.Message);
            }
        }

        protected void llenaFormulario(DataTable dtCompetidor, bool cnLlenaCategoria = false)
        {
            if (!cnLlenaCategoria)
            {
                if (dtCompetidor.Rows[0]["retValue"].ToString() == "1")
                {
                    for (int i = 0; i < dtCompetidor.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            if (dtCompetidor.Rows[i]["idConfigPagoPreregistro"] != null)
                                hdIdConfigPago.Value = dtCompetidor.Rows[i]["idConfigPagoPreregistro"].ToString();
                            if (dtCompetidor.Rows[i]["dsContacto"].ToString() != "")
                            {
                                txtGralNombre.Text = dtCompetidor.Rows[i]["dsContacto"].ToString();
                                txtGralNombre.ReadOnly = true;
                            }
                            txtGralApellidoPaterno.Text = ((dtCompetidor.Rows[i]["dsApellidoPaterno"].ToString() != "") ? dtCompetidor.Rows[i]["dsApellidoPaterno"].ToString() : "");
                            txtGralApellidoPaterno.ReadOnly = true;
                            txtGralApellidoMaterno.Text = ((dtCompetidor.Rows[i]["dsApellidoMaterno"].ToString() != "") ? dtCompetidor.Rows[i]["dsApellidoMaterno"].ToString() : "");
                            txtGralApellidoMaterno.ReadOnly = true;
                            txtGralEmail.Text = ((dtCompetidor.Rows[i]["dsCorreoElectronico"].ToString() != "") ? dtCompetidor.Rows[i]["dsCorreoElectronico"].ToString() : "");
                            if (dtCompetidor.Rows[i]["dsCorreoElectronico"].ToString() != "")
                                    txtGralEmail.ReadOnly = true;
                            txtGralEmail2.Text = ((dtCompetidor.Rows[i]["dsCorreoElectronico"].ToString() != "") ? dtCompetidor.Rows[i]["dsCorreoElectronico"].ToString() : "");
                            if (dtCompetidor.Rows[i]["dsCorreoElectronico"].ToString() != "")
                                    txtGralEmail2.ReadOnly = true;
                            int intIdSexo = ((dtCompetidor.Rows[i]["idSexo"].ToString() != "") ? int.Parse(dtCompetidor.Rows[i]["idSexo"].ToString()) : 0);
                            if (intIdSexo > 0)
                            {
                                ddlGralSexo.Items.FindByValue(intIdSexo.ToString()).Selected = true;
                                ddlGralSexo.Attributes.Add("readonly", "readonly");
                                ddlGralSexo.Attributes.Add("disabled", "disabled");

                            }
                            DateTime dtFeNacimiento = new DateTime();
                            int anio = 0;
                            int mes = 0;
                            int dia = 0;
                            if (dtCompetidor.Rows[0]["feNacimiento"].ToString() != "")
                            {
                                try
                                {
                                    dtFeNacimiento = DateTime.Parse(dtCompetidor.Rows[i]["feNacimiento"].ToString());
                                }
                                catch (Exception ex)
                                {
                                    mensaje("Problema al leer la fecha " + dtCompetidor.Rows[i]["feNacimiento"].ToString()+ " " + ex.Message);
                                }
                                anio = dtFeNacimiento.Year;
                                mes = dtFeNacimiento.Month;
                                dia = dtFeNacimiento.Day;
                                ddlGralAnio.SelectedIndex = -1;
                                ddlGralAnio.Items.FindByValue(anio.ToString()).Selected = true;
                                ddlGralAnio.Attributes.Add("readonly", "readonly");
                                ddlGralAnio.Attributes.Add("disabled", "disabled");
                                ddlGralMes.SelectedIndex = -1;
                                ddlGralMes.Items.FindByValue(mes.ToString()).Selected = true;
                                ddlGralMes.Attributes.Add("readonly", "readonly");
                                ddlGralMes.Attributes.Add("disabled", "disabled");
                                ddlGralDia.SelectedIndex = -1;
                                ddlGralDia.Items.FindByValue(dia.ToString()).Selected = true;
                                ddlGralDia.Attributes.Add("readonly", "readonly");
                                ddlGralDia.Attributes.Add("disabled", "disabled");
                            }
                            int intIdPais = (dtCompetidor.Rows[0]["idPais"].ToString() != "") ? int.Parse(dtCompetidor.Rows[0]["idPais"].ToString()) : 0;
                            if (intIdPais > 0)
                            {
                                ddlGralPais.SelectedIndex = -1;
                                ddlGralPais.Items.FindByValue(intIdPais.ToString()).Selected = true;
                                utils.llenaEstados(ddlGralEstado, ddlGralPais.SelectedValue.ToString(), getDsSession());
                                int intIdEstado = (dtCompetidor.Rows[i]["idEstado"].ToString() != "") ? int.Parse(dtCompetidor.Rows[i]["idEstado"].ToString()) : 0;
                                if (intIdEstado > 0)
                                {
                                    ddlGralEstado.SelectedIndex = -1;
                                    ddlGralEstado.Items.FindByValue(intIdEstado.ToString()).Selected = true;
                                }
                            }
                            txtGralColonia.Text = (dtCompetidor.Rows[i]["dsColonia"].ToString() != "") ? dtCompetidor.Rows[i]["dsColonia"].ToString() : "";
                            txtGralCiudad.Text = (dtCompetidor.Rows[i]["dsCiudad"].ToString() != "") ? dtCompetidor.Rows[i]["dsCiudad"].ToString() : "";
                            txtGralCalle.Text = (dtCompetidor.Rows[i]["dsCalle"].ToString() != "") ? dtCompetidor.Rows[i]["dsCalle"].ToString() : "";
                            txtGralNoInterior.Text = (dtCompetidor.Rows[i]["dsNumInterior"].ToString() != "") ? dtCompetidor.Rows[i]["dsNumInterior"].ToString() : "";
                            txtGralNoExterior.Text = (dtCompetidor.Rows[i]["dsNumExterior"].ToString() != "") ? dtCompetidor.Rows[i]["dsNumExterior"].ToString() : "";
                            string strTelefono = (dtCompetidor.Rows[i]["dsTelefono"].ToString() != "") ? dtCompetidor.Rows[i]["dsTelefono"].ToString() : "";
                            string strCelular = (dtCompetidor.Rows[i]["dsCelular"].ToString() != "") ? dtCompetidor.Rows[i]["dsCelular"].ToString() : "";
                            if (strTelefono.Length > 0)
                            {
                                txtGralTelefonofijo1.Text = strTelefono.Remove(3, strTelefono.Length - 3);
                                txtGralTelefonofijo2.Text = strTelefono.Remove(0, 2);
                            }

                            if (strCelular.Length > 0)
                            {
                                txtGralCelular1.Text = strCelular.Remove(3, strCelular.Length - 3);
                                txtGralCelular2.Text = strCelular.Remove(0, 3);
                            }
                            hdDsClave.Value = dtCompetidor.Rows[i]["dsAlergia"].ToString();
                        }
                        
                    }                    
                }               
                
            }// fin if cnLlenaCategoria
            else
            {
                if (dtCompetidor.Rows[0]["idEventoClasificacion"].ToString() != "")
                {
                    string idCategoria = dtCompetidor.Rows[0]["idEventoClasificacion"].ToString() + "-" + dtCompetidor.Rows[0]["idProducto"].ToString();
                    //ddlGralCategoria.Items.FindByValue(idCategoria).Selected = true;
                    //ddlGralCategoria.Attributes.Add("readonly", "readonly");
                    //ddlGralCategoria.Attributes.Add("disabled", "disabled");
                }
            }
        }

        protected void ddlGralPais_TextChanged(object sender, EventArgs e)
        {
            utils.llenaEstados(ddlGralEstado, ddlGralPais.SelectedValue.ToString(), getDsSession());
        }

        protected void btnTerminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (cGlobals.dsSession == null)
                    getDsSession();

                bool respUpload = true;
                valDdlParentesco.InnerText = "";
                valTelContacto.InnerText = "";

                if (!chkMayorEdad.Checked)
                {
                    respUpload = false;
                    chkMayorEdad.Focus();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('true');alert('Debe aceptar de que es mayor de edad');$('.chkMayorEdad').focus();</script>", false);
                    return;
                }
                if (!chkHeLeido.Checked)
                {
                    respUpload = false;
                    chkMayorEdad.Focus();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('true');alert('Debe aceptar las politicas y condiciones');$('.chkHeLeido').focus();</script>", false);
                    return;
                }

                if(rblColaborador.SelectedValue.ToString() == "")
                {
                    respUpload = false;
                    rblColaborador.Focus();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('true');</script>", false);
                    return;
                }

                if (txtNombreContacto.Text.Trim().Length < 3)
                {
                    respUpload = false;
                    txtNombreContacto.Focus();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('true');</script>", false);
                    return;
                }

                if (txtPaternoContacto.Text.Trim().Length < 3)
                {
                    respUpload = false;
                    txtPaternoContacto.Focus();                    
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('true');</script>", false);
                    return;
                }

                if (ddlParentesco.SelectedValue.ToString() == "")
                {
                    respUpload = false;
                    ddlParentesco.Focus();
                    valDdlParentesco.InnerText = "Obligatorio";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('true');</script>", false);
                    return;
                }

                if (txtTelefonoParentesco.Text.Trim().Length <= 6)
                {
                    respUpload = false;
                    txtTelefonoParentesco.Focus();
                    valTelContacto.InnerText = "Obligatorio";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('true');</script>", false);
                    return;
                }

                if (hdValidationSizeFiles1.Value == "1" || hdValidationSizeFiles2.Value == "1" || hdValidationSizeFiles3.Value == "1")
                {
                    respUpload = false;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('true'," + hdValidationSizeFiles1.Value + "," + hdValidationSizeFiles2.Value + "," + hdValidationSizeFiles3.Value + ");</script>", false);
                    return;
                }
                //Session["filesDocuments0"] = null;
                if (Session["filesDocuments0"] == null)
                {
                    if (ViewState["filesDocuments0"] != null)
                    {
                        Session["filesDocuments0"] = ViewState["filesDocuments0"];
                    }
                    else{
                        respUpload = false;
                        lblMensaje.Text = "Favor de regresar al paso uno y subir su fotografia";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('true');</script>", false);
                        return;
                    }
                }
                // uploadFiles regresa false si falla si no estan todos los archivos
                respUpload = validateFilesNotEmpty();
                if (!respUpload)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('" + respUpload + "');</script>", false);
                    return;
                }
                else
                {
                    #region participantes
                    cParticipante objectParticipante = new cParticipante();
                    string[] strDsEventoClasificacion;
                    bool blActualiza = false;
                    bool blTodoOK = true;
                    string strIdClienteContactoEvento = "";
                    string strIdConfigPagoPreregistro = hdIdConfigPago.Value;

                    DateTime fechaEvento = new DateTime(2015, 12, 31);
                    int anio = 1900;
                    int mes = 01;
                    int dia = 01;
                    int.TryParse(ddlGralAnio.SelectedValue.ToString(), out anio);
                    int.TryParse(ddlGralMes.SelectedValue.ToString(), out mes);
                    int.TryParse(ddlGralDia.SelectedValue.ToString(), out dia);

                    anio = (anio > 0) ? anio : 1900;
                    mes = (mes > 0) ? mes : 01;
                    dia = (dia > 0) ? dia : 01;
                    DateTime fechaNacimiento = new DateTime(anio, mes, dia);
                    int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;

                    //if (ddlGralCategoria.SelectedValue.ToString() != "")
              
                    // SI HAY VALOR EN LA PROPIEDAD dtCompetidor de nuestra clase cParticipante
                    // QUIERE DECIR QUE VINO INFORMACION DE UN PRECARGADO.
                    cCompetidorGeneral.DtCompetidor = getDatatableCompetidor();
                    if (cCompetidorGeneral.DtCompetidor != null)
                    {
                        strIdClienteContactoEvento = cCompetidorGeneral.DtCompetidor.Rows[0]["idClienteContactoEvento"].ToString();
                        blActualiza = true;
                    }
                    #region categoriaSeleccionada
                    string loc = "";
                    string loc2 = "";
                    Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                    loc = myUri.AbsoluteUri.ToString();
                    loc2 = myUri.AbsoluteUri.ToString();
                    string[] strspliturl = loc.Split('/');
                    loc = loc.Replace("/"+strspliturl[strspliturl.Length - 1] , "/detallePago.aspx");
                    loc2 = loc2.Replace("/" + strspliturl[strspliturl.Length - 1], "/respRegistro.aspx");
                    string strHost = HttpContext.Current.Request.Url.Host;
                    string strPrimerosCaracteres = loc.Substring(0, 5);

                    //if (Convert.ToBoolean(ConfigurationManager.AppSettings["bdProduccion"]) && strHost != "localhost")
                    if (strHost != "localhost")
                    {
                        if (strPrimerosCaracteres == "http:")
                        {
                            loc = loc.Replace("http", "https");
                            loc2 = loc2.Replace("http", "https");
                        }
                        else if (strPrimerosCaracteres != "https")
                        { 
                            loc.Trim().Insert(0, "https://");
                            loc2.Trim().Insert(0, "https://");
                        }
                    }
                    //if (ddlGralCategoria.SelectedValue.ToString() != "")
                    //{
                        #region llenaObjetoCompetidor

                    // antes de llenar al competidor principal, tengo que ir a insertar al clienteContacto de emergencias
                    #region ContactoEmergencia
                    // guardarContacto
                    cParticipante cContactoCompetidor = new cParticipante();
                    cContactoCompetidor.nombre = txtNombreContacto.Text.Trim();
                    cContactoCompetidor.apellidoPaterno = txtPaternoContacto.Text.Trim();
                    cContactoCompetidor.apellidoMaterno = txtMaternoContacto.Text.Trim();
                    cContactoCompetidor.idEstatusCliente = 1;
                    cContactoCompetidor.telefono1 = txtTelefonoParentesco.Text;
                    cContactoCompetidor.telefono2 = txtTelefonoParentesco.Text;
                    cContactoCompetidor.llenarLista(cContactoCompetidor);
                    var respGuardarContactoCompetidor = cContactoCompetidor.guardarContacto(cContactoCompetidor.listCompetidores,getDsSession(),getIdCliente(), getIdUsuario());                    
                    int idClienteContactoEmergencia = int.Parse(respGuardarContactoCompetidor[0].ToString());
                    cContactoCompetidor.VaciarLista();
                    #endregion


                    //string[] strIdEventoClasificacion = ddlGralCategoria.SelectedValue.ToString().Split('-');
                        int intIdEventoClasificacion = int.Parse(ddlGralCategoria.SelectedValue.ToString());
                        objectParticipante.nombre = txtGralNombre.Text;
                        objectParticipante.apellidoPaterno = txtGralApellidoPaterno.Text;
                        if (txtGralApellidoMaterno.Text != "")
                            objectParticipante.apellidoMaterno = txtGralApellidoMaterno.Text;
                        objectParticipante.email = txtGralEmail.Text;
                        objectParticipante.idSexo = int.Parse(ddlGralSexo.SelectedValue.ToString());
                        if (txtGralCiudad.Text != "")
                            objectParticipante.municipio = txtGralCiudad.Text.ToString();
                        if (txtGralNoInterior.Text != "")
                            objectParticipante.numInterior = (txtGralNoInterior.Text.ToString());
                        if (txtGralNoExterior.Text != "")
                            objectParticipante.numExterior = (txtGralNoExterior.Text.ToString());
                        if (txtGralCalle.Text != "")
                            objectParticipante.calle = txtGralCalle.Text.ToString();
                        if (txtGralColonia.Text != "")
                            objectParticipante.colonia = txtGralColonia.Text.ToString();
                        objectParticipante.fechaNacimiento = ddlGralAnio.SelectedValue.ToString() + "/" + ddlGralMes.SelectedValue.ToString() + "/" + ddlGralDia.SelectedValue.ToString();
                        if (ddlGralPais.SelectedValue.ToString() != "")
                        {
                            objectParticipante.idPais = int.Parse(ddlGralPais.SelectedValue.ToString());
                            objectParticipante.pais = ddlGralPais.SelectedItem.ToString();
                        }
                        if (ddlGralEstado.SelectedValue.ToString() != "")
                        {
                            objectParticipante.idEstado = int.Parse(ddlGralEstado.SelectedValue.ToString());
                            objectParticipante.estado = ddlGralEstado.SelectedItem.ToString();
                        }
                        if (txtGralTelefonofijo1.Text != "")
                            objectParticipante.telefono1 = txtGralTelefonofijo1.Text.ToString() + txtGralTelefonofijo2.Text.ToString();
                        objectParticipante.telefono2 = txtGralCelular1.Text.ToString() + txtGralCelular2.Text.ToString();
                        objectParticipante.idEventoClasificacion = intIdEventoClasificacion;
                        //strDsEventoClasificacion = ddlGralCategoria.SelectedItem.ToString().Split('/');
                       // objectParticipante.dsEventoClasificacion = strDsEventoClasificacion[0].ToString().Trim();
                        //objectParticipante.dsCodigoEventoClasificacion = strDsEventoClasificacion[1].ToString().Trim();
                        //objectParticipante.vecesParticipaciones = int.Parse(ddlGralNoParticipaciones.SelectedValue.ToString());
                        objectParticipante.idTipoMoneda = 2;
                        //objectParticipante.noRifa = utils.getNoRifa(intIdEventoClasificacion);
                        if (ddlGralTallasPlayeras.SelectedValue != null && ddlGralTallasPlayeras.SelectedValue != "")
                            objectParticipante.idTallaPlayera = int.Parse(ddlGralTallasPlayeras.SelectedValue.ToString());                                            
                        if (ddlTipoSangre.SelectedValue != null && ddlTipoSangre.SelectedValue != "")
                            objectParticipante.idTipoSangre = int.Parse(ddlTipoSangre.SelectedValue.ToString());
                        objectParticipante.dsTallaPlayera = ddlGralTallasPlayeras.SelectedItem.ToString();
                       // objectParticipante.dsFolioFmtri = txtNoFmtri.Text;

                        objectParticipante.dsPadecimientos = txtPadecimiento.Text;
                        objectParticipante.idTipoSangre = int.Parse(ddlTipoSangre.SelectedValue.ToString());
                        if (hdCena.Value == "1")
                            objectParticipante.dsAgrupador = "cena";
                        else
                            objectParticipante.dsAgrupador = "Travesia";
                        objectParticipante.idEstatusCliente = 3; // Inactivo;
                        objectParticipante.edad = edad; // se agregó el 29042015
                        objectParticipante.peso = decimal.Parse(txtPeso.Text.Trim());
                        objectParticipante.cnPracticaDeporte =  (int.Parse(rblSiNo.SelectedValue.ToString().Trim()) == 1 )? true:false;
                        if (objectParticipante.cnPracticaDeporte)
                        { 
                            objectParticipante.dsDetalleDeporte = txtDeportes.Text.Trim();
                            objectParticipante.dsFrecuenciaDeporte = rblFrecuencia.SelectedValue.ToString().Trim();
                        }
                        objectParticipante.cnEsColaborador = (int.Parse(rblColaborador.SelectedValue.ToString().Trim()) == 1)? true :false;
                        if (objectParticipante.cnEsColaborador)
                        {
                            objectParticipante.noColaborador = (txtNoColaborador.Text.Trim());
                            objectParticipante.idUnidadNegocioColaborador = int.Parse(ddlUnidadNegocioColaborador.SelectedValue.ToString().Trim());
                        }
                        objectParticipante.idLocacion = int.Parse(ddlLocaciones.SelectedValue.ToString().Trim());
                        //objectParticipante.idClienteContactoEmergencia
                        objectParticipante.idParentesco = int.Parse(ddlParentesco.SelectedValue.ToString().Trim());
                        string strAniosParticipaciones = "";
                        ASPxListBox lbxAniosParticipaciones = ddlGralNoParticipaciones3.FindControl("lbxAniosParticipaciones") as ASPxListBox;
                        int contNoParticipaciones = 0;
                        foreach (ListEditItem li in lbxAniosParticipaciones.SelectedItems)
                        {
                            strAniosParticipaciones += li.Value.ToString().Trim() + ";";
                            contNoParticipaciones = contNoParticipaciones + 1;
                        }
                        if (strAniosParticipaciones.Length > 0)
                        {
                            strAniosParticipaciones = strAniosParticipaciones.Remove(strAniosParticipaciones.Length - 1);
                        }
                        objectParticipante.vecesParticipaciones = contNoParticipaciones;
                        objectParticipante.aniosParticipaciones = strAniosParticipaciones;
                        objectParticipante.idClienteContactoEmergencia = idClienteContactoEmergencia;
                        objectParticipante.dsMotivoParticipacion = txtRazonParticipacion.Text.Trim();
                        objectParticipante.dsSignificadoTsm = txtSignificadoTSM.Text.Trim();
                        objectParticipante.idNivelNatacion = int.Parse(rblSabeNadar.SelectedValue.ToString().Trim());
                        objectParticipante.cnVegetariano = (int.Parse(rblVegetariano.SelectedValue.ToString().Trim()) == 1) ? true : false;
                        // YA TENEMOS LLENA LAS PROPIEDADES DE NUESTRO OBJETO PARTICIPANTE.
                        objectParticipante.llenarLista(objectParticipante);
                        #endregion                    
                    
                        // si paga quiere decir que antes de ir a la ventana de pagar, se guarda el competidor
                        // pero con estatus en proceso y sin numero y ni nada
                        if (blTodoOK)
                        {
                            //antes de ir a pagar tenemos que ver si es nuevo se inserta y si no es nuevo se actualiza
                            // pero el estatus original no se toca
                            #region actualiza
                            string resultadoAtualizar = "";
                            string strPrimerIdClienteContactoEvento = "";
                            int intIdVenta = 0;
                            int intNumeroCompetidor = 0;
                            if (blActualiza)
                            {

                                switch (strIdConfigPagoPreregistro)
                                {
                                    case "2":
                                        intIdVenta = fnGuardaVentaPago(0, 12);
                                        break;
                                    case "3":
                                        intIdVenta = fnGuardaVentaPago(100, 14);
                                        break;
                                    default :

                                        break;
                                }
                                string strIdClienteContactoEventoPapa = "";
                                if (cCompetidorGeneral.DtCompetidor.Columns.Contains("idProducto"))
                                {
                                    for (int i = 0; i < cCompetidorGeneral.DtCompetidor.Rows.Count; i++)
                                    {
                                        string strDsClaveProducto = cCompetidorGeneral.DtCompetidor.Rows[i]["dsClave"].ToString();
                                        if (strDsClaveProducto != "TXHO" || strDsClaveProducto != "TXHS" || strDsClaveProducto != "TXHI" || strDsClaveProducto != "TXHRS" || strDsClaveProducto != "TXHRO" || strDsClaveProducto != "TXHE" || strDsClaveProducto != "TXHSN" || strDsClaveProducto != "TXHCD" || strDsClaveProducto != "TXHVIP")
                                        {
                                            if (i == 0)
                                            {
                                                string idClienteContactoEventoRecorriendo = cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteContactoEvento"].ToString();
                                                strIdClienteContactoEventoPapa = cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteContactoEvento"].ToString();
                                                strPrimerIdClienteContactoEvento = cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteContactoEvento"].ToString();
                                                wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                                                wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                                                clienteContactoEventoClient.Open();
                                                cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(int.Parse(idClienteContactoEventoRecorriendo), getDsSession());
                                                cCompetidor.dsAlias = (objectParticipante.dsAlias != null) ? objectParticipante.dsAlias : cCompetidor.dsAlias;
                                                cCompetidor.dsFolioFmtri = (objectParticipante.dsFolioFmtri);
                                                cCompetidor.dsPadecimiento = objectParticipante.dsPadecimientos;
                                                cCompetidor.idEventoClasificacion = objectParticipante.idEventoClasificacion;
                                                cCompetidor.idSexo = objectParticipante.idSexo;
                                                cCompetidor.idTipoSangre = objectParticipante.idTipoSangre;
                                                cCompetidor.noFrecuencia = objectParticipante.vecesParticipaciones;
                                                if (objectParticipante.idTallaPlayera != null && objectParticipante.idTallaPlayera >0)
                                                    cCompetidor.idTalla = objectParticipante.idTallaPlayera;
                                                cCompetidor.cnCarrera = false;
                                                cCompetidor.cnNatacion = false;
                                                cCompetidor.cnCiclismo = false;                                            
                                                cCompetidor.cnCarrera = false;                                            
                                                cCompetidor.cnNatacion = false;                                            
                                                cCompetidor.cnCiclismo = false;
                                                if (cCompetidor.idVenta == 0)
                                                {
                                                    cCompetidor.idVenta = null;
                                                }
                                                if (cCompetidor.idEventoModalidad == 0)
                                                {
                                                    cCompetidor.idEventoModalidad = null;
                                                }

                                                if (intIdVenta > 0)
                                                {
                                                    actualizaCompetidorDetalle(int.Parse(cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteDetalle"].ToString()));
                                                    wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                                                    // wsTransactions.IkVentaClient ikventa2 = new wsTransactions.IkVentaClient();
                                                    wsTransactions.ListakVentaDetalles listaKVentaDetalles2 = new wsTransactions.ListakVentaDetalles();
                                                    listaKVentaDetalles2 = ikventa.SeleccionarkVentasDetallePorIdVenta(intIdVenta, getDsSession());
                                                    for (int k = 0; k < listaKVentaDetalles2.Count(); k++)
                                                    {

                                                        cCompetidor.idVenta = intIdVenta;
                                                        // entonces tambien tenemos que mandar a actualizar su detalle con idEstatus inscrito
                                                        actualizaCompetidorDetalle(int.Parse(cCompetidorGeneral.DtCompetidor.Rows[i]["idClienteDetalle"].ToString()));
                                                        intNumeroCompetidor = getNumeroCompetidor(objectParticipante.idEventoClasificacion.ToString());
                                                        cCompetidorGeneral.DtCompetidor.Rows[i]["noCompetidor"] = intNumeroCompetidor;
                                                        cCompetidor.NoCompetidor = intNumeroCompetidor;
                                                        cCompetidor.idVentaDetalle = listaKVentaDetalles2[k].idVentaDetalle;
                                                        wscClienteContacto.IcClienteContactoClient clienteContactoClient = new wscClienteContacto.IcClienteContactoClient();
                                                        wscClienteContacto.cClienteContacto cClienteContacto;
                                                        cClienteContacto = clienteContactoClient.SeleccionarcClienteContactoPorId(int.Parse(cCompetidor.idClienteContacto.ToString()), getDsSession());
                                                        cClienteContacto.dsCelular = objectParticipante.telefono2;
                                                        cClienteContacto.dsTelefono = objectParticipante.telefono1;
                                                        cClienteContacto.feNacimiento = DateTime.Parse(objectParticipante.fechaNacimiento);
                                                        cClienteContacto.idEstado = objectParticipante.idEstado;
                                                        cClienteContacto.idPais = objectParticipante.idPais;
                                                        cClienteContacto.idSexo = objectParticipante.idSexo;
                                                        cClienteContacto.dsCorreoElectronico = objectParticipante.email;
                                                        cClienteContacto.idTipoMoneda = 2;

                                                        clienteContactoEventoClient.Close();
                                                        clienteContactoClient.Close();
                                                        resultadoAtualizar = cParticipante.actualizaCompetidor(cCompetidor, cClienteContacto, true);
                                                    }

                                                }// fin de if (intIdVenta > 0)
                                                else
                                                {
                                                    //if (intIdVenta > 0)
                                                    
                                                    //intNumeroCompetidor = getNumeroCompetidor(objectParticipante.idEventoClasificacion.ToString());
                                                        //cCompetidorGeneral.DtCompetidor.Rows[i]["noCompetidor"] = intNumeroCompetidor;
                                                    //cCompetidor.NoCompetidor = intNumeroCompetidor;
                                                   // cCompetidor.idVentaDetalle = listaKVentaDetalles2[k].idVentaDetalle;


                                                    wscClienteContacto.IcClienteContactoClient clienteContactoClient = new wscClienteContacto.IcClienteContactoClient();
                                                    wscClienteContacto.cClienteContacto cClienteContacto;
                                                    cClienteContacto = clienteContactoClient.SeleccionarcClienteContactoPorId(int.Parse(cCompetidor.idClienteContacto.ToString()), getDsSession());
                                                    cClienteContacto.dsCelular = objectParticipante.telefono2;
                                                    cClienteContacto.dsTelefono = objectParticipante.telefono1;
                                                    cClienteContacto.feNacimiento = DateTime.Parse(objectParticipante.fechaNacimiento);
                                                    cClienteContacto.idEstado = objectParticipante.idEstado;
                                                    cClienteContacto.idPais = objectParticipante.idPais;
                                                    cClienteContacto.idSexo = objectParticipante.idSexo;
                                                    cClienteContacto.dsCorreoElectronico = objectParticipante.email;
                                                    cClienteContacto.idTipoMoneda = 2;

                                                    clienteContactoEventoClient.Close();
                                                    clienteContactoClient.Close();
                                                    resultadoAtualizar = cParticipante.actualizaCompetidor(cCompetidor, cClienteContacto, true);
                                                }                                                                                                                        
                                            }                                        
                                        
                                            // entonce ahora tengo que ver el tamaño de la lista, si es mayor a uno los siguientes que vienen son de relevos
                                            // no debemos tomar el primero ya que ya lo actualizamos
                                            // los demas son inserciones
                                            int intRecorriendo = 0;
                                            //var respuestaGuardaRelevos = "";
                                            cParticipante cParticimenteRelevos = new cParticipante();
                                            if (objectParticipante.listCompetidores.Count > 1)
                                            {
                                            
                                                foreach (cParticipante elemento in objectParticipante.listCompetidores)
                                                {
                                                    if (intRecorriendo != 0)
                                                    {
                                                        //List<cParticipante> listCompetidores = new List<cParticipante>()
                                                        elemento.idClienteContactoEventoPapa = int.Parse(strIdClienteContactoEventoPapa);
                                                        cParticimenteRelevos.llenarLista(elemento);                                                    
                                                    }
                                                    intRecorriendo++;
                                                }
                                                var respuestaGuardaRelevos = cParticimenteRelevos.guardarCompetidor(cParticimenteRelevos.listCompetidores, getDsSession(), getIdCliente(), getIdUsuario());
                                            }

                                        } // fin del if (strDsClaveProducto != "TXHO" || strDsClaveProducto != "TXHS" || strDsClaveProducto != "TXHI" || strDsClaveProducto != "TXHRS" || strDsClaveProducto != "TXHRO" || strDsClaveProducto != "TXHE" || strDsClaveProducto != "TXHSN" || strDsClaveProducto != "TXHCD" || strDsClaveProducto != "TXHVIP")                               
                                    } // fin del for (int i = 0; i < cCompetidorGeneral.DtCompetidor.Rows.Count; i++)
                                }
                                objectParticipante.VaciarLista();
                                if (hdCena.Value == "1")
                                    cParticipante.cnCena = 1;                        
                                // si es uno es porque actualizo bien
                                if (resultadoAtualizar == "1")
                                {
                                    if (intIdVenta > 0)
                                    {
                                        bool respCorreoConfirmacion = correoPago(intIdVenta.ToString(), "", "1", cCompetidorGeneral.DtCompetidor.Rows[0]["dsCorreoElectronico"].ToString(), cCompetidorGeneral.DtCompetidor);
                                        cCompetidorGeneral.DtCompetidor = null;
                                        Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = null;
                                        Response.Redirect("respPago.aspx?token=" + intIdVenta.ToString());
                                    }

                                    else
                                    {
                                        cCompetidorGeneral.DtCompetidor = null;
                                        Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = null;
                                        Response.Redirect(loc + "?contEvent=" + strPrimerIdClienteContactoEvento);
                                    }
                                }
                            }
                            #endregion
                            // sino pues guarda
                            else
                            {

                                var listResp = objectParticipante.guardarCompetidor(objectParticipante.listCompetidores, getDsSession(), getIdCliente(), getIdUsuario());
                                int respIdClienteContactoEvento = 0;
                                respIdClienteContactoEvento = int.Parse(listResp[0].ToString());
                                objectParticipante.VaciarLista();
                                string strServEsp = "";
                                strServEsp = Request.QueryString["servEsp"];

                                // Antes de hacer el ridereccionamiento ahora si vamos a subir los archivos
                                string nombreCarpeta = listResp[0].ToString();
                                string finalPath = GlobalConstants.Imaging.SaveFilesFolderNameServerPath +"/"+ nombreCarpeta +"/";
                                bool cnUploadFiles = false;
                                try
                                {
                                    //throw new System.ArgumentException("Generado aproposito", "original");
                                    cnUploadFiles = uploadFiles(finalPath);
                                }
                                catch (Exception ex)
                                {
                                    string msg = ex.Message.ToString();
                                    string strginnermessage = "";
                                    string strCuerpoCorreo = msg + "<br>" + strginnermessage + "<br>" + ex.Source.ToString() + "<br>";
                                    string strExcepcion = "";
                                    if (ex.ToString().Length > 799)
                                        strExcepcion = ex.ToString().Substring(0, 999);
                                    else
                                        strExcepcion = ex.ToString();
                                    strCuerpoCorreo += strExcepcion;   
                                    lblMensaje.Text = ex.ToString();
                                    MensajeAlert(strCuerpoCorreo);
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('true');alert('Su sesión ha expirado');window.location ='http://www.travesiasagradamaya.com.mx/'</script>", false);
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('true');reloadPage('Los archivos ya no existe en cache, favor de subir de nuevo');</script>", false);
                                    //Response.Redirect(Request.Url.AbsoluteUri);
                                    return;
                                }
                                if (strServEsp == "cen")
                                {
                                    strServEsp = "&servEsp=cen";
                                }
                                if (hdCena.Value == "1")
                                    cParticipante.cnCena = 1;
                                if (respIdClienteContactoEvento > 0)
                                {
                                    cCompetidorGeneral.DtCompetidor = null;
                                    Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value] = null;
                                    // actualizar en cClienteContactoEvento la liga en dsLiga
                                    wscClienteContactoEvento.IcClienteContactoEventoClient clienteContactoEventoClient = new wscClienteContactoEvento.IcClienteContactoEventoClient();
                                    wscClienteContactoEvento.cClienteContactoEvento cCompetidor;
                                    clienteContactoEventoClient.Open();
                                    cCompetidor = clienteContactoEventoClient.SeleccionarcClienteContactoEventoPorId(respIdClienteContactoEvento, getDsSession());
                                    cCompetidor.dsLiga = loc + "?contEvent=" + respIdClienteContactoEvento.ToString() + strServEsp;
                                    cCompetidor.dsFolioFmtri = respIdClienteContactoEvento.ToString();
                                    int respuestaModificaContactoEvento = clienteContactoEventoClient.ModificarcClienteContactoEvento(cCompetidor,getDsSession());
                                    clienteContactoEventoClient.Close();
                                    Response.Redirect(loc2 + "?contEvent=" + respIdClienteContactoEvento.ToString() + strServEsp);
                                }
                            }
                        }// fin de if(blPaga)                
                    #endregion
                    #endregion
                }
            }
            catch (Exception ex)
            {
                //   mensaje("Problema en carga de pagina", exx);
                //cbRegistrar.JSProperties["cpIsMessage"] = ex.ToString();
                lblMensaje.Text = ex.ToString();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('true');</script>", false);
                return;
            }
        }

        protected void ddlGralMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            utils.llenaDias(int.Parse(ddlGralAnio.SelectedValue.ToString()), int.Parse(ddlGralMes.SelectedValue.ToString()), ddlGralDia);
        }

        protected void ddlGralAnio_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlGralAnio.SelectedValue.ToString() != "")
                utils.llenaDias(int.Parse(ddlGralAnio.SelectedValue.ToString()), int.Parse(ddlGralMes.SelectedValue.ToString()), ddlGralDia);
        }

        protected void btnContinuarA_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                string strCat = "";
                string strDsClave = "";
                strDsClave = hdDsClave.Value;
                DateTime fechaEvento = new DateTime(2015, 12, 31);
                int anio = 1900;
                int mes = 01;
                int dia = 01;
                int.TryParse(ddlGralAnio.SelectedValue.ToString(), out anio);
                int.TryParse(ddlGralMes.SelectedValue.ToString(), out mes);
                int.TryParse(ddlGralDia.SelectedValue.ToString(), out dia);

                anio = (anio > 0) ? anio : 1900;
                mes = (mes > 0) ? mes : 01;
                dia = (dia > 0) ? dia : 01;
                DateTime fechaNacimiento = new DateTime(anio, mes, dia);
                int intSexo = ((ddlGralSexo.SelectedValue) == "") ? 2 : int.Parse(ddlGralSexo.SelectedValue);
                int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;
                strCat = hdStrCat.Value;


                /*if (strDsClave != "")
                {

                    if (strDsClave == "TXSPR" || strDsClave == "TXOLR")
                    {
                            utils.getCategorias(getDsSession(), intSexo, edad, ddlGralCategoria, null, strDsClave.ToUpper().Trim(),1);
                    }
                    else
                    {
                        utils.getCategorias(getDsSession(), intSexo, edad, ddlGralCategoria, null, strDsClave.ToUpper().Trim());
                    }
                        
                }*/
               /* else if (strCat != "")

                    switch (strCat.ToUpper())
                    {
                        case "NOV":
                            utils.getCategorias(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXN", 1);
                            break;
                        case "CP":
                            utils.getCategorias(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXCP", 1);
                            break;
                        case "PES":
                            utils.getCategorias(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXC", 1);
                            break;
                        case "CENAESP":
                            utils.getCategorias(getDsSession(), intSexo, edad, ddlGralCategoria, null, "TXI", 1);
                            break;
                        default:
                            break;
                    }
                else
                {
                    if (cnIdContactoPrecargado.Value == "0")
                        utils.getCategorias(getDsSession(),intSexo, edad, ddlGralCategoria, null, "TXI", 1);
                    else
                        utils.getCategorias(getDsSession(), intSexo, edad, ddlGralCategoria, null, "TXI", 1,1);
                }*/               
                if (cnIdContactoPrecargado.Value != "0")
                {
                    llenaFormulario(getDatatableCompetidor(), true);
                }
                
            }
            catch (Exception ex)
            {
                mensaje("Error al momento de continuar formulario B " + ex.Message);
            }
        }

        public DataTable getDatatableCompetidor()
        {
            if (cCompetidorGeneral.DtCompetidor == null)
                cCompetidorGeneral.DtCompetidor = (DataTable)Session["dtCompetidorGeneral" + cnIdContactoPrecargado.Value];
            return cCompetidorGeneral.DtCompetidor;
        }

        protected void ddlGralCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //string[] strIdCategoria = ddlGralCategoria.SelectedValue.ToString().Split('-');
                                                              
            }
            catch (Exception ex)
            { }
        }

        protected void btnTerminar_Init(object sender, EventArgs e)
        {
            ASPxButton btnTerminar = sender as ASPxButton;
            btnTerminar.Attributes.Add("onClick", String.Format(callbackRegistrarCompetidor));
        }

       

        protected void cbValida_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
        {
            try
            {                
                DateTime fechaEvento = new DateTime(2015, 12, 31);
                int anio = 1900;
                int mes = 01;
                int dia = 01;
                int.TryParse(ddlGralAnio.SelectedValue.ToString(), out anio);
                int.TryParse(ddlGralMes.SelectedValue.ToString(), out mes);
                int.TryParse(ddlGralDia.SelectedValue.ToString(), out dia);

                anio = (anio > 0) ? anio : 1900;
                mes = (mes > 0) ? mes : 01;
                dia = (dia > 0) ? dia : 01;
                DateTime fechaNacimiento = new DateTime(anio, mes, dia);

                int edad = (fechaEvento).AddTicks(-fechaNacimiento.Ticks).Year - 1;
                //utils.getCategorias(getDsSession(),Convert.ToInt16(ddlGralSexo.SelectedValue), edad, ddlGralCategoria, null, "TXI", 1);
                bool respValidacion = utils.validaExisteRegistro(txtGralEmail.Text, getDsSession());                

                if (respValidacion)
                {
                    cbValida.JSProperties["cpValidacion"] = "true";
                }
                else 
                {
                    cbValida.JSProperties["cpValidacion"] = "false";
                }
            }
            catch (Exception ex)
            {
                //   mensaje("Problema en carga de pagina", exx);
                //cbRegistrar.JSProperties["cpIsMessage"] = ex.ToString();
                cbValida.JSProperties["cpValidacion"] = "";
            }
        }

        protected void btnContinuarA_Init(object sender, EventArgs e)
        {
            ASPxButton btnContinuarA = sender as ASPxButton;
            btnContinuarA.Attributes.Add("onClick", String.Format(callbackValidaCompetidor));
        }

        protected void txtGralEmail2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtGralEmail.Text != txtGralEmail2.Text)
                {
                    lblValidacionEmail.Text = "Los correos, no coinciden, favor de validarlo";
                }
                else
                {
                    lblValidacionEmail.Text = "";                    
                }
            }
            catch (Exception ex)
            { 
            }
        }

        public void mensaje(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showMessage('errorMsg','Ha ocurrido un error','" + errMensaje + "');</script>", false);
        }
        public void alert(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert(" + errMensaje + ");</script>", false);
        }

        protected void ddlGralTallasPlayeras_SelectedIndexChanged(object sender, EventArgs e)
        {           
        }

        public int fnGuardaVentaPago(decimal dcPorcentajeDesc, int intIdFormaPago)
        {
            //string[] strDsEventoClasificacion = ddlGralCategoria.SelectedItem.ToString().Split('/');
            //string strDsCodigo = strDsEventoClasificacion[1].ToString().Trim();
            DataSet result = utils.getPrecio(getDsSession(),141,"");
            DataTable dtProductoInscripcion = null;
            DataTable dtCompetidor = getDatatableCompetidor();
            //cParticipante.dtDataCobro = 
            int sizeDataSet = result.Tables.Count;
            dtProductoInscripcion = result.Tables[0];
            dtProductoInscripcion.Columns.Add("ordenador", typeof(decimal)).DefaultValue = 1;
            dtProductoInscripcion.Columns.Add("cantidad", typeof(int)).DefaultValue = 1;
            dtProductoInscripcion.Columns.Add("mnPrecioVenta", typeof(decimal)).DefaultValue = 1;
            dtProductoInscripcion.Columns.Add("idMonedaPagar", typeof(decimal)).DefaultValue = 1;
            string strCanalVentaId = (ConfigurationManager.AppSettings["idCanalVenta"].ToString());

            for (int i = 0; i < sizeDataSet; i++)
            {
                dtProductoInscripcion.Rows[i]["mnPrecioVenta"] = (decimal)dtProductoInscripcion.Rows[i]["mnPrecio_2"] - ((dcPorcentajeDesc * (decimal)dtProductoInscripcion.Rows[i]["mnPrecio_2"]) / 100 );
                dtProductoInscripcion.Rows[i]["idMonedaPagar"] = 2;
                if (bool.Parse(dtProductoInscripcion.Rows[i]["cnEsFacturable"].ToString()) == true)
                {
                    dtProductoInscripcion.Rows[i]["ordenador"] = 1;
                    dtProductoInscripcion.Rows[i]["cantidad"] = 1;
                }
                else
                {
                    dtProductoInscripcion.Rows[i]["ordenador"] = 2;
                    dtProductoInscripcion.Rows[i]["cantidad"] = 0;
                }
            }

            cParticipante.dtDataCobro = dtProductoInscripcion;

            cResultadoGuardadoPago.strIdEstatusPago = "2";
            cResultadoGuardadoPago.strAfiliacion = "";
            cResultadoGuardadoPago.strDsRespuesta = "Aprobado";
            cResultadoGuardadoPago.strDsJustificacion = "Aprobado";
            cResultadoGuardadoPago.strDsReferencia = "";
            cResultadoGuardadoPago.strDsTransaccion = "";
            cResultadoGuardadoPago.strDsCorrelacion = "";
            cResultadoGuardadoPago.strIdBancoReceptor = "";

            decimal dcTotalVenta = getTotalVenta(dtProductoInscripcion);

            wsTransactions.IkVentaClient vc = new wsTransactions.IkVentaClient();
            vc.Open();

            wsTransactions.kVenta laventan = new wsTransactions.kVenta();

            laventan.idClienteDetalle = int.Parse(dtCompetidor.Rows[0]["idClienteDetalle"].ToString());
            laventan.idClienteClasificador = 5; // Directos
            laventan.idVenta = 0;
            laventan.idCliente = Int32.Parse(dtCompetidor.Rows[0]["idCliente"].ToString());
            laventan.feVenta = Int32.Parse(DateTime.Now.ToString("yyyyMMdd")); // DateTime.Now;
            laventan.hrVenta = DateTime.Now;
            laventan.mnMontoTotal = dcTotalVenta; //dcTotal;
            //laventan.mnDescuento = 0; //dcDescuento;
            //laventan.cnPaquete = (hdEsPaquete.Value == "1");
            laventan.cnPaquete = false;
            //laventan.dsClaveVenta
            //laventan.dsNotas = strNotas;
            if (intIdFormaPago == 14)
                laventan.idEstatusVenta = 23;
            else
                laventan.idEstatusVenta = 7;  // 
            laventan.cnRequiereFactura = false;
            laventan.idCanalVenta = int.Parse(strCanalVentaId);
            laventan.cnEvento = false;
            laventan.feCaducidad = Int32.Parse(DateTime.Now.AddYears(1).ToString("yyyyMMdd"));
            laventan.feAlta = DateTime.Now;
            laventan.idClienteUsuarioAlta = Int32.Parse(cGlobals.idUsuario.ToString());
            laventan.cnPromocion = false;  //false;

            laventan.dsDispositivo = "PC / LAPTOP";
            laventan.dsSistemaOperativo = "Windows NT APLICATION";

            if (ddlGralPais.SelectedValue != "")
                laventan.idPaisCompra = Int32.Parse(ddlGralPais.SelectedValue); //484;  // MEX cPais
            else
                laventan.idPaisCompra = 484;

            if (ddlGralEstado.SelectedValue != "1")
                laventan.idEstadoCompra = Int32.Parse(ddlGralEstado.SelectedValue);
            else
                laventan.idEstadoCompra = 272;
            laventan.idClienteContactoComprador = Int32.Parse(dtCompetidor.Rows[0]["idClienteContacto"].ToString());
            if (intIdFormaPago == 12)
                laventan.cnRequiereFactura = true;
            else
                laventan.cnRequiereFactura = false;
            // ahora los detalles
            bool blBanderaGuardo = true;
            wsTransactions.kVentaDetalle detalle; //, detallePadre;
            wsTransactions.ListakVentaDetalles listaParaEnvio = new wsTransactions.ListakVentaDetalles();


            for (int l = 0; l < dtProductoInscripcion.Rows.Count; l++)
            {
                if (bool.Parse(dtProductoInscripcion.Rows[l]["cnEsFacturable"].ToString()) == true && int.Parse(dtProductoInscripcion.Rows[l]["cantidad"].ToString()) > 0)
                {
                    detalle = new wsTransactions.kVentaDetalle();
                    string mnPrecio_ = "mnPrecio_";
                    string mnTipoCambio_ = "mnTipoCambio_";
                    decimal dcMnDescuento = 0;
                    decimal dcPrDescuento = 0;

                    mnPrecio_ += dtProductoInscripcion.Rows[l]["idMonedaPagar"].ToString();
                    mnTipoCambio_ += dtProductoInscripcion.Rows[l]["idMonedaPagar"].ToString();

                    detalle.feVisita = int.Parse(dtProductoInscripcion.Rows[l]["feVisita"].ToString());
                    detalle.feAlta = DateTime.Now;
                    //detalle.feVisitaLimite = Int32.Parse((DateTime.Now.AddYears(1)).AddMonths(6).ToString("yyyyMMdd"));  // fecha Alta + 1 año
                    //detalle.feVisitaLimite = Int32.Parse((DateTime.Parse(dtVentaOrdenador(dtVentaOrdenadorUpgradeSeleccionados.Rows[l]["fechaVisita"].ToString())).AddYears(1)).AddMonths(6).ToString("yyyyMMdd")); // fecha Visita + 1 año y medio. // validado con Lupita y Haumi 16 septiembre 2013
                    detalle.noAgrupadorVenta = 1; //? sacar el max  + 1
                    detalle.noPax = (int)dtProductoInscripcion.Rows[l]["cantidad"];
                    if (int.Parse(dtProductoInscripcion.Rows[l]["idMonedaPagar"].ToString()) > 4)
                        detalle.mnPrecioLista = (decimal)dtProductoInscripcion.Rows[l]["mnPrecio_MS"];
                    else
                        detalle.mnPrecioLista = (decimal)dtProductoInscripcion.Rows[l][mnPrecio_];
                    detalle.mnPrecioVenta = decimal.Parse(dtProductoInscripcion.Rows[l]["mnPrecioVenta"].ToString()) / detalle.noPax;

                    // cuando detalle.mnPrecioLista  es cero, es porque son cosas como donativos que no tienen precio
                    if (detalle.mnPrecioLista > 0)
                    {
                        dcMnDescuento = (decimal)(detalle.mnPrecioLista - detalle.mnPrecioVenta);
                        dcPrDescuento = dcPorcentajeDesc;
                    }
                    else
                    {
                        dcMnDescuento = 0;
                        dcPrDescuento = 0;
                    }
                    detalle.mnDescuento = dcMnDescuento; //detalle.mnPrecioLista - detalle.mnPrecioVenta; // se calcula de la resta de precioLista - precioVenta listo
                    detalle.prDescuento = dcPrDescuento; //((1 - (detalle.mnPrecioVenta / detalle.mnPrecioLista)) * 100); // se calcula listo

                    detalle.idProductoPrecio = (int)dtProductoInscripcion.Rows[l]["idProductoPrecio"];
                    detalle.cnAccesado = false;
                    detalle.cnControlInterno = false;
                    detalle.cnPaquete = (bool)dtProductoInscripcion.Rows[l]["cnEsPaquete"];
                    /*if ((bool)detalle.cnPaquete)
                    {
                        // Depende la el orde de como escojan los productos se guarda la venta con bandera de paquete.
                        laventan.cnPaquete = true; //(hdEsPaquete.Value == "1");
                        detalle.dsClavePaquete = dtVentaOrdenadorUpgradeSeleccionados.Rows[l]["dsClavePaquete"].ToString();
                    }*/
                    detalle.idTipoCliente = (int)dtProductoInscripcion.Rows[l]["idTipoCliente"];
                    detalle.cnPromocion = false;

                    detalle.dsNombreVisitante = dtCompetidor.Rows[0]["dsContacto"].ToString(); // llenar con campo de texto por cada registro - listo
                    detalle.dsApellidoPaternoVisitante = dtCompetidor.Rows[0]["dsApellidoPaterno"].ToString();
                    detalle.dsApellidoMaternoVisitante = dtCompetidor.Rows[0]["dsApellidoMaterno"].ToString();

                    if (intIdFormaPago == 14)
                        detalle.idEstatusVenta = 23;
                    else
                        detalle.idEstatusVenta = 7; //  

                    int intLocacion = (int)dtProductoInscripcion.Rows[l]["idLocacion"];

                    if (intLocacion != 0)
                        detalle.idLocacion = intLocacion;


                    detalle.mnComision = 0;
                    detalle.prComision = 0;                    
                    // En idMoneda del detalle se debe guardar el idTipoMoneda (Gabriel)
                    //detalle.idTipoMoneda = (int)dtVentaOrdenador.Rows[l]["idTipoMoneda"];
                    detalle.idTipoMoneda = int.Parse(dtProductoInscripcion.Rows[l]["idMonedaPagar"].ToString());

                    detalle.mnTipoCambio = (decimal)dtProductoInscripcion.Rows[l][mnTipoCambio_]; // ? sacar del día listo

                    // Gabriel Granados pide que el campo mnTipoCambio se guarde el tipo de cambio en pesos del dia. correo(09-07-2013): Ventas en Dolares y PEsos
                    if (detalle.idTipoMoneda == 1)
                    {
                    }

                    detalle.idSegmento = 4;
                    detalle.idClienteUsuarioAlta = Int32.Parse(cGlobals.idUsuario.ToString());
                    //detalle.idVentaDetalle = int.Parse(dtVentaOrdenador.Rows[l]["consecutivo"].ToString());
                    detalle.idVentaDetalle = 0;
                    detalle.idCanalVenta = int.Parse(strCanalVentaId);
                    detalle.mnIva = null;
                    listaParaEnvio.Add(detalle);
                }      
            }// fin del for

            int intIdVenta = vc.InsertarkVenta(laventan, listaParaEnvio, null, null, getDsSession());
            vc.Close();

            //------------------------------------------------------------------------------------------------
            // PAGO
            //------------------------------------------------------------------------------------------------

            wsTransactions.IkPagoClient IkPago = new wsTransactions.IkPagoClient();
            wsTransactions.ListakPagoTransacciones lstkPagoTransa = new wsTransactions.ListakPagoTransacciones();
            wsTransactions.kPagoTransaccion PagoTran = new wsTransactions.kPagoTransaccion();
            wsTransactions.kPago objkPago = new wsTransactions.kPago();

            objkPago.idPago = 0;
            objkPago.idVenta = intIdVenta;
            objkPago.idCanalPago = int.Parse(strCanalVentaId); //11; // fijo ?
            objkPago.feTransaccionTotal = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
            objkPago.hrTransaccionTotal = DateTime.Now;
            //objkPago.mnTransaccionTotal = Math.Round(dcTotal, 2);
            //objkPago.mnTransaccionTotal = decimal.Parse(totalPagar);
            objkPago.mnTransaccionTotal = dcTotalVenta;
            objkPago.cnEsPrepago = false;
            objkPago.noAgrupadorVenta = 1;
            objkPago.idFormaPago = intIdFormaPago; // 52 CxC y 14 Cortesias
            objkPago.idEstatusPago = Int32.Parse(cResultadoGuardadoPago.strIdEstatusPago); //intIdEstatusPago;
            objkPago.feAlta = DateTime.Now;
            objkPago.idClienteUsuarioAlta = (cGlobals.idUsuario);

            PagoTran.idPago = objkPago.idPago; //
            // En idMoneda se guarda el IdTipoMoneda (Gabriel 20082013)
            PagoTran.idTipoMoneda = int.Parse(dtProductoInscripcion.Rows[0]["idMonedaPagar"].ToString()); //Int32.Parse(cmbMoneda.SelectedItem.Value);

            PagoTran.feTransaccion = Int32.Parse(DateTime.Now.ToString("yyyyMMdd"));
            PagoTran.hrTransaccion = DateTime.Now;
            PagoTran.dsRespuesta = cResultadoGuardadoPago.strDsRespuesta; // strDsRespuesta;
            PagoTran.dsReferencia = cResultadoGuardadoPago.strDsReferencia; // strDsReferencia;
            PagoTran.dsJustificacion = cResultadoGuardadoPago.strDsJustificacion; // strDsJustificacion;

            //PagoTran.mnTransaccion = Math.Round(dcTotal, 2);
            //PagoTran.mnTransaccion = decimal.Parse(totalPagar);
            PagoTran.mnTransaccion = dcTotalVenta;

            PagoTran.dsTransaccion = cResultadoGuardadoPago.strDsTransaccion; // strDsTransaccion;
            PagoTran.dsCorrelacion = cResultadoGuardadoPago.strDsCorrelacion; // strDsCorrelacion;
            PagoTran.noPagosMSI = 0;
            objkPago.cnPlanDePagos = false;

            PagoTran.cnPrepago = false;
            //if (intIdClienteTarjeta > 0)
            //if (Int32.Parse(cResultadoGuardadoPago.strIdClienteTarjeta) > 0)
                PagoTran.idClienteTarjeta = null; // intIdClienteTarjeta;
            PagoTran.mnBancoComision = 0;
            PagoTran.prBancoComision = 0;
            PagoTran.feAlta = DateTime.Now;
            PagoTran.idClienteUsuarioAlta = (cGlobals.idUsuario);
            PagoTran.idPagoTransaccion = 0;
            PagoTran.idEstatusPago = Int32.Parse(cResultadoGuardadoPago.strIdEstatusPago);            

            PagoTran.dsAfiliacion = cResultadoGuardadoPago.strAfiliacion;

            if (intIdFormaPago == 52) // si el tipo de pago es CxC  se tiene que guardar el idBanco y idBancoReceptor  Bancomer (5)
            {
                PagoTran.idBanco = 5;
                PagoTran.idBancoReceptor = 5;
            }
            else
            {
                PagoTran.idBanco = null;
                PagoTran.idBancoReceptor = null;
            }

            lstkPagoTransa.Add(PagoTran);
            int intIdPago;
            try
            {
                // 1 er intento
                intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
            }
            catch (Exception err)
            {
                // mensaje("OE fallo primer intento, intIdTipoMoneda=" + intIdTipoMoneda.ToString() + strCadenaResultadoPagoTarjeta, err);
                try
                {
                    // 2 er intento
                    intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                }
                catch (Exception err2)
                {
                    // 3 er intento
                    // mensaje("OE fallo segundo intento, intIdTipoMoneda=" + intIdTipoMoneda.ToString() + strCadenaResultadoPagoTarjeta, err);
                    intIdPago = IkPago.InsertarkPago(objkPago, lstkPagoTransa, getDsSession());
                }
            }
            IkPago.Close();

            return intIdVenta;
        }

        public decimal getTotalVenta(DataTable dtVentaOrdenador)
        {
            decimal respuesta = 0;

            for (int i = 0; i < dtVentaOrdenador.Rows.Count; i++)
            {
                if (bool.Parse(dtVentaOrdenador.Rows[i]["cnEsFacturable"].ToString()) == true)
                    respuesta += (decimal)dtVentaOrdenador.Rows[i]["mnPrecioVenta"];
            }
            return respuesta;
        }

        public void actualizaCompetidorDetalle(int idClienteDetalle)
        {
            wscClienteDetalle.IcClienteDetalleClient cteCLiente = new wscClienteDetalle.IcClienteDetalleClient();
            wscClienteDetalle.cClienteDetalle ClienteDet;
            wscClienteDetalle.cClienteDetalle ClienteDetalle;
            ClienteDetalle = cteCLiente.SeleccionarcClienteDetallePorId(idClienteDetalle, getDsSession());

            // cambiamos el estatus del participante a inscrito
            ClienteDetalle.idEstatusCliente = 15;
            int resModificaDetalles = cteCLiente.ModificarcClienteDetalle(ClienteDetalle, getDsSession());
        }
        public int getNumeroCompetidor(string strIdEventoClasificacion)
        {
            // obtenemos el numero de competidor que le corresponde    
            int intNumeroCompetidor = 0;            
            wsBusinessRules.BusinessRulesServicesClient ruleOper = new wsBusinessRules.BusinessRulesServicesClient();
            Dictionary<string, string> dicret = new Dictionary<string, string>();
            dicret.Add("@idEventoClasificacion", strIdEventoClasificacion);
            DataSet dtResultNumero = ruleOper.ExecuteRule("spGetNumeroCompetidorTriatlon", dicret, getDsSession());
            if (dtResultNumero.Tables[0].Rows[0]["retValue"].ToString() != "0")
            {
                intNumeroCompetidor = int.Parse(dtResultNumero.Tables[0].Rows[0]["numero"].ToString());
            }
            return intNumeroCompetidor;
        }

        protected bool correoPago(string strIdVenta, string strFolios, string strIdioma, string strCorreoCliente, DataTable dtCompedidor)
        {
            //string strParametros = "<br>Parametros pasados a la funcion correoPago: strIdVenta:" + strIdVenta + "<br>strFolios:" + strFolios + "<br>strIdioma:" + strIdioma + "<br>strCorreoCliente:" + strCorreoCliente + "<br>lblMensajeCorreo:" + lblMensajeCorreo.Text;
            int intIdClienteContacto = 0;
            try
            {
                wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();
                iGeneral.Open();
                // Textos Correos
                wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
                icConfiguracion.Open();

                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto2CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto2CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto10CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto10CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto11CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto11CorreoPagoIdioma_" + strIdioma, getDsSession());
                wscConfiguracionAplicacion.cConfiguracionAplicacion Texto12CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto12CorreoPagoIdioma_" + strIdioma, getDsSession());
                // wscConfiguracionAplicacion.cConfiguracionAplicacion Texto13CorreoPagoIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("Texto13CorreoPagoIdioma_" + strIdioma, Globals.dsSession);
                wscConfiguracionAplicacion.cConfiguracionAplicacion TextoUrlCuponIdioma = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("SIVEX-CRM-URLCUPON_CanalVenta_27_Id_" + strIdioma, getDsSession());

                icConfiguracion.Close();
                string nombre = dtCompedidor.Rows[0]["dsContacto"].ToString() + ' ' + dtCompedidor.Rows[0]["dsApellidoPaterno"].ToString() + ' ' + dtCompedidor.Rows[0]["dsApellidoMaterno"].ToString();
                string numeroCompetidor = dtCompedidor.Rows[0]["noCompetidor"].ToString();
                string strCuerpoCorreo = "<table>";
                string strCuerpoCorreoLinksFolios = "";
                //strIdVenta = strIdVenta.Remove(strIdVenta.Length - 1);
                string[] arrStrIdVenta = strIdVenta.Split(',');

                for (int ab = 0; ab < arrStrIdVenta.Length; ab++)
                {
                    wsTransactions.IkVentaClient ikventa = new wsTransactions.IkVentaClient();
                    ikventa.Open();
                    wsTransactions.kVenta laVenta = ikventa.SeleccionarkVentaPorIdVenta(int.Parse(arrStrIdVenta[ab]), getDsSession());
                    strFolios += laVenta.dsClaveVenta + ",";
                    strCuerpoCorreoLinksFolios += "<a href=\"" + TextoUrlCuponIdioma.dsValor.Replace("<idVenta>", arrStrIdVenta[ab]) + "\" >" + laVenta.dsClaveVenta + "</a>" + ((arrStrIdVenta.Length == 1 || ab == (arrStrIdVenta.Length - 1)) ? "" : ",");
                }
                strFolios = strFolios.Remove(strFolios.Length - 1);
                strCuerpoCorreo += "<tr><td colspan=\"2\"> Estimado/a  <b>" + nombre + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"> Te informamos que de acuerdo a tu solicitud, hemos confirmado tu inscripción. </td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto2CorreoPagoIdioma.dsValor + " " + strFolios + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>Numero Competidor: </b>" + dtCompedidor.Rows[0]["noCompetidor"].ToString() + "</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + "Importante: Por favor lee detenidamente e imprime el documento del enlace (";
                strCuerpoCorreo += strCuerpoCorreoLinksFolios;


                //string[] arrStrFolios = strFolios.Split(',');


                strFolios = strFolios.Remove(strFolios.Length - 1);

                strCuerpoCorreo += ") y presentalo al momento de recoger tu paquete de competidor  </b></td></tr>";

                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">Si deseas una factura, por favor entra a: https://www.aolxcaret.com/core/facturacion, recuerda que tienes 24 horas para solicitar tu factura </td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                //strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto10CorreoPagoIdioma.dsValor + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">&nbsp;</td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\"><b>" + Texto11CorreoPagoIdioma.dsValor + "</b></td></tr>";
                strCuerpoCorreo += "<tr><td colspan=\"2\">" + Texto12CorreoPagoIdioma.dsValor + "</td></tr>";

                strCuerpoCorreo += "</table>";

                string strCorreoAgente = "";
                
                bool blnBanderaCorreo = iGeneral.SendEmail(strCorreoCliente, "Confirmación de Inscripción Triatlón Xel-Há", utils.armaHTMLBaseCorreo("Confirmación de Inscripción Triatlón Xel-Há", strCuerpoCorreo), true /*es html*/, "", false, 0, strCorreoAgente);
                //bool blnBanderaCorreo = iGeneral.SendEmail("irojas@experienciasxcaret.com.mx", TituloCorreoIdioma.dsValor, Utils.armaHTMLcorreoCancelacion(TituloCorreoIdioma.dsValor + " XCARET", strCuerpoCorreo), true /*es html*/, "", false, 0, strCorreoAgente);

                if (blnBanderaCorreo)
                {
                    //    lblMensajeCorreo.Text = lblMensajeCorreo.Text.Trim() + ((lblMensajeCorreo.Text.Trim().Length > 0) ? " y " : "") + "Correo Enviado";
                }

                iGeneral.Close();

                return blnBanderaCorreo;
            }
            catch (Exception err)
            {
                //mensaje("correoPago <br>intIdClienteContacto pasado a SeleccionarcClienteContactoPorId:" + intIdClienteContacto.ToString() + " " + strParametros, err);
                return false;
            }
        }

        public void alertRedirect(string errMensaje)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + errMensaje + "'); window.location = 'http://www.triatlonxelha.com'</script>", false);
        }

        public string getDsSession()
        {
            if (cGlobals.dsSession == null)
            {
                utils.Login("usrTriatlon", "eXperiencias");
                hdDsSession.Value = cGlobals.dsSession;
            }
            return cGlobals.dsSession;
        }

        public int getIdUsuario()
        {
            if (cGlobals.idUsuario == null || cGlobals.idUsuario == 0)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.idUsuario;
        }

        public int getIdCliente()
        {
            if (cGlobals.idCliente == null || cGlobals.idCliente == 0)
            {
                utils.Login("usrTriatlon", "eXperiencias");
            }
            return cGlobals.idCliente;
        }

        public void uploadImage()
        {
                var uploadFilCol = Request.Files[0];
            //for (var i = 0; i < uploadFilCol.Count; i++)
            //{
                // chuck the current file in the loop in a variable called file
                var file = uploadFilCol;
                HttpPostedFile fileAux = Request.Files[0];
                var msg = CheckFileIsAnImage(file);
                
                // if msg is empty we know our file is an ok image, if not we need to show a 
                // message to the user
                if (!string.IsNullOrEmpty(msg))
                { 
                    //ShowMessage(msg, i);
                }
                else
                {
                    // our file is ok. so now we get the filename of the file we are looking at
                    Session["filesDocuments0"] = file;
                    ViewState["filesDocuments0"] = file;
                    var fileName = Path.GetFileName(file.FileName);
                     
                    // then we call CheckAndCreateDirectory which sees if the directory 
                    // structure we want to save to exists.
                    // if it doesn't exist it creates the directory for us.
                    // either way, the method will return the path to save the image to.
                    //
                    // this is where you would do some work on creating a custom folder root 
                    // if that's what you wanted (for example if you wanted another folder 
                    // within the storage system that was the artists name, or a gallery
                    // id, etc, etc.
                    //var savePath = CheckAndCreateDirectory(Server.MapPath(GlobalConstants.Imaging.SaveFolderNameServerPath));
                    // now we save the image to the server. in the process we allocate a new 
                    // filename (in this case a guid).
                    // you can use whatever you like here but remember all image names must 
                    // be unique.
                    //var newFilename = SaveUploadedImageToServer(savePath, file);   
                    SavePostedFile(fileAux);
                    /*ViewState["filesDocuments0"] = "https://www.xperienciasxcaret.mx/core/archivos/Travesia/Storage/pictures/" + fileName;
                    imgUpload.ImageUrl = "https://www.xperienciasxcaret.mx/core/archivos/Travesia/Storage/pictures/" + fileName;*/
                    ViewState["filesDocuments0"] = "https://xcaret-sivex-01-archivos.s3.amazonaws.com/Travesia/Storage/pictures/" + fileName;
                    imgUpload.ImageUrl = "https://xcaret-sivex-01-archivos.s3.amazonaws.com/Travesia/Storage/pictures/" + fileName;
                }
            //}
        }

        protected string SavePostedFile(HttpPostedFile uploadedFile)
        {

            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(uploadedFile.InputStream))
            {                
                fileData = binaryReader.ReadBytes(uploadedFile.ContentLength);
            }
            Session["byteSessionfilesDocuments0"] = fileData;
            //uploadedFile.ContentLength = BitConverter.ToInt32(fileData,0);
            string UploadDirectory = GlobalConstants.Imaging.SaveFolderNameServerPath;                

            // fptprueba
            /* wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
             icConfiguracion.Open();

             wscConfiguracionAplicacion.cConfiguracionAplicacion cConfFptMx = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("confFptMx", getDsSession());
             icConfiguracion.Close();

             List<ftpSetting> listPagosInfo = (new JavaScriptSerializer()).Deserialize<List<ftpSetting>>(cConfFptMx.dsValor);*/

            string fileName = uploadedFile.FileName;
            string filePath = Path.GetFullPath(fileName);
            string PureFileName = new FileInfo(uploadedFile.FileName).Name;
            string strpath = Path.GetFileName(fileName).ToString();
            //UploadDirectory = "core/archivos/Travesia/Storage/pictures";
            //string strFtpIp = "ftp://";
            string FolderToSearch = System.IO.Path.GetDirectoryName(fileName).ToString();

            wscFtpUpload.UploadClient ftpUpload = new wscFtpUpload.UploadClient();
           /* List<string> result = ftpUploadfiles(fileData, PureFileName, UploadDirectory, false, getDsSession()).ToList<string>();
            if (result.Count > 0)
            {
                hdImg.Value = "1";
            }
            else
            {
                hdImg.Value = "0";
            }*/          
            
           
            //byte[] data = File.ReadAllBytes(fileName);            
            //return res.StatusDescription;
            try
            {
                if (PureFileName.Trim().Length > 0)
                {
                    using (var client = new wscFtpUpload.UploadClient())
                    {
                        /* using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                            file.CopyTo(ms);*/
                        using (MemoryStream ms = new MemoryStream(fileData))
                        {
                            //client.Uploadfiles(ms, PureFileName.Trim(), UploadDirectory, true, "","4heeon35j3bt4aqzqbuhsx1v");
                            client.Uploadfiles(ms, PureFileName.Trim(), UploadDirectory, true, "", cGlobals.dsSession);
                            hdImg.Value = "1";                           
                        }                                     
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            return uploadedFile.FileName;                
            //}            
        }

        protected string SavePostedDocuments(HttpPostedFile uploadedFile, string UploadDirectory, int contFile)
        {
            #region oldway
           byte[] fileData = null;
           bool boolCrearFolio = false;
            //string fileName = uploadedFile.FileName;
            if (uploadedFile.ContentLength == 0)
                 fileData = (byte[])Session["byteSessionfilesDocuments" + contFile.ToString()];
            else
            {
                using (var binaryReader = new BinaryReader(uploadedFile.InputStream))
                {
                    fileData = binaryReader.ReadBytes(uploadedFile.ContentLength);
                } 
            }
            var fileName = new FileInfo(uploadedFile.FileName).Name;
            if (contFile == 0)
            {
                fileName = "AVATAR.jpg";
                boolCrearFolio = true;
            }
            /*wsBlobStorage.BlobStorageClient blob = new wsBlobStorage.BlobStorageClient();          
           
           try
           {
               if (!blob.SubirArchivo(fileData, fileName, UploadDirectory, false, getDsSession()))
               {
                   blob.Close();
                   throw new Exception("El archivo " + uploadedFile.FileName + " ya ha sido cargado en el sistema, utilice otro nombre de archivo");
               }
               blob.Close();
           }
           catch (Exception ex)
           {

               throw new Exception("Error al subir el archivo..." + ex.Message);
           }*/
            #endregion
            //string fileName = uploadedFile.FileName;//f
            // PureFileName = new FileInfo(uploadedFile.FileName).Name;
            //return res.StatusDescription;            
            wscFtpUpload.UploadClient ftpUpload = new wscFtpUpload.UploadClient();
            //List<string> result = ftpUploadfiles(fileData, fileName, UploadDirectory, boolCrearFolio, getDsSession()).ToList<string>();
            //}
            try
            {
                if (fileName.Trim().Length > 0)
                {
                    using (var client = new wscFtpUpload.UploadClient())
                    {
                        /* using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                            file.CopyTo(ms);*/
                        using (MemoryStream ms = new MemoryStream(fileData))
                        {
                            client.Uploadfiles(ms, fileName.Trim(), UploadDirectory, true, "", cGlobals.dsSession);                           
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            return uploadedFile.FileName;
        }


        [WebMethod]
        public static void uploadFiles2()
        {            
            inscripcionGeneral general = new inscripcionGeneral();            
            var File = HttpContext.Current.Request.Files; // Page.Request           

            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
            }

            //general.uploadFiles();
        }

        public bool validateFilesNotEmpty()
        {
            var uploadFilCol = Request.Files;            
            bool result = true;
            if (uploadFilCol.Count < 0)
                result = false;
            // voy a meter una validacion porque parece que la Session["filesDocuments0"] se está perdiendo
            // si es null quiere decir que lo tenemos que volver a llenar
            var filesDocuments0 = Session["filesDocuments0"];
            //Session["filesDocuments0"] = null;
            if (Session["filesDocuments0"] == null)
            {
                /*WebClient myWebClient = new WebClient();
                var file = myWebClient.DownloadData(ViewState["filesDocuments0"].ToString());
                Stream stream = null;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(ViewState["filesDocuments0"].ToString());
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                stream = res.GetResponseStream();
                Session["filesDocuments0"] = file;*/                
            }
            //for (var i = 1; i < uploadFilCol.Count; i++)
            for (var i = 1; i < uploadFilCol.Count; i++)
            {
                // chuck the current file in the loop in a variable called file
                var file = uploadFilCol[i];
                var msg = CheckFileIsAnImage(file);
                // if msg is empty we know our file is an ok image, if not we need to show a 
                // message to the user
                if (!string.IsNullOrEmpty(msg))
                {
                    // si el archivo que se está intententando subir en ese momento está vacio 
                    // checamos en la sesion si hay valor, ya que quiere decir que anteriormente
                    // ya se habia seleccionado el archivo, si igual  está vació, pos ahi si le avisamos 
                    // al cliente que en esa posicion del arreglo de archivos, falta por seleccionar archivo
                    //if (Session["filesDocuments" + i.ToString()] == null)
                    //{
                        ShowMessage(msg, i);                        
                    //}
                    result = false;
                }
                else
                {
                    Session["filesDocuments" + i.ToString()] = file;
                    ShowMessage("Archivo: "+file.FileName.ToString(), i);
                }

            }
            return result;
        }

        public bool uploadFiles(string pathDirectorySave)
        {
            //HttpPostedFile files = Request.Files["uploadFiles"];
            var uploadFilCol = Request.Files;            
            bool result = true;
            if (uploadFilCol.Count < 0)
                result = false;
            for (var i = 0; i < uploadFilCol.Count; i++)
            {
                // chuck the current file in the loop in a variable called file
                if (Session["filesDocuments" + i.ToString()] != null)
                {
                    var file = (HttpPostedFile)Session["filesDocuments" + i.ToString()];
                    var msg = CheckFileIsAnImage(file);
                    // if msg is empty we know our file is an ok image, if not we need to show a 
                    // message to the user
                    if (!string.IsNullOrEmpty(msg) && i != 0)
                    {
                        ShowMessage(msg, i);
                        result = false;
                        //return false;
                    }
                    else
                    {
                        // our file is ok. so now we get the filename of the file we are looking at
                        var fileName = Path.GetFileName(file.FileName);
                        if (i == 0)
                        {
                            fileName = i.ToString();
                        }
                        //var savePath = CheckAndCreateDirectory(pathDirectorySave);
                        // now we save the image to the server. in the process we allocate a new 
                        // filename (in this case a guid).
                        // you can use whatever you like here but remember all image names must 
                        // be unique.
                        //var newFilename = SaveUploadedFilesToServer(savePath, file, fileName);
                        SavePostedDocuments(file, pathDirectorySave, i);
                        ShowMessage("uploaded successfully", i);
                        //result = true;                    
                    }
                }
                else
                    result = false;
            }
            return result;
        }
        public static string CheckFileIsAnImage(HttpPostedFile file)
        {
            // if the file is null or has no content it's obviously a dirty wrongun
            if (file == null) return "file is invalid, upload must have failed";
            if (file.ContentLength == 0) return "no file specified";

            // get the filename extension (e.g .jpg, .gif, etc) from the filename
            var extension = Path.GetExtension(file.FileName).ToLower();

            // we're only supporting jpg, png and gif in this (you can support more if you 
            // like though)
            if (extension != ".jpg" && extension != ".gif" && extension != ".png" && extension != ".pdf" && extension != ".jpeg")
                return "Tipo de archivo invalido";

            // if we got this far the file looks good, so return an empty string (which 
            // means everything ok)
            return string.Empty;
        }

        private void ShowMessage(string message, int fileUploadPos)
        {
            // show messages for each file being uploaded (3 files, so have to have a 
            // message for each)
            switch (fileUploadPos)
            {
                case 1:
                    lblMsgFile1.Text = message;
                    break;
                case 2:
                    lblMsgFile2.Text = message;
                    break;
                case 3:
                    //lblMsgFile3.Text = message;
                    break;
            }
        }

        public string SaveUploadedImageToServer(String filePath, HttpPostedFile file)
        {
            // first thing to do is create a new filename root by using a guid and the 
            // current file extension.
            // i use the guid as the filename and as the unique media id in the database, 
            // but you don't have to do it this way
            var mediaId = Guid.NewGuid();
            var fileNameExtension = Path.GetExtension(file.FileName).ToLower();
            // now we iterate through our image dimensions list so we can create the image 
            // sizes we want from the image uploaded.
            // we are creating 3 images - 100x100, 250x250 and 450x450
            foreach (var imageDim in GlobalConstants.Imaging.ImageDims)
            {
                // get the file as an image
                var uploadedImage = Image.FromStream(file.InputStream);
                // create our new filename using the media id, file extension and image 
                // dimensions.
                var fileName = file.FileName;
                // now get the full server path and add on the filename
                var fullSavePath = filePath + "\\" + fileName;
                // we need to get the right ImageFormat so we don't try to save a jpg as a 
                // png, etc...
                var imageType = GetImageType(fileNameExtension);
                // if the height and width of the uploaded image are less that the size we 
                // are aiming for (e.g we want 100x100, they uploaded 80x80), just save the 
                // image as is.
                if (uploadedImage.Height <= imageDim && uploadedImage.Width <= imageDim)
                {
                    // save the image
                    uploadedImage.Save(fullSavePath, imageType);
                    // then get rid of it from memory
                    uploadedImage.Dispose();
                }
                else
                {
                    var newWidth = imageDim;
                    var newHeight = imageDim;

                    // check the heigh and width ratio. if we are making a 100x100 image, 
                    // we may need to make it 70x100 or 100x70 depending
                    // whether the image is landscape or portrait
                    if (uploadedImage.Height > uploadedImage.Width)
                        newWidth = (Int32)Math.Ceiling(
                            ((double)uploadedImage.Width / uploadedImage.Height) * imageDim);
                    else
                        newHeight = (Int32)Math.Ceiling(
                            ((double)uploadedImage.Height / uploadedImage.Width) * imageDim);

                    // now write a new image out using the dimensions we've calculated
                    var smallerImg = new Bitmap(newWidth, newHeight);
                    var g = Graphics.FromImage(smallerImg);

                    // a few options, no need to worry about these
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    foreach (var pItem in uploadedImage.PropertyItems)
                    {
                        smallerImg.SetPropertyItem(pItem);
                    }

                    // draw the image
                    g.DrawImage(uploadedImage, new Rectangle(0, 0, newWidth, newHeight));

                    // get rid of the uploaded image stream from memory
                    uploadedImage.Dispose();

                    // save the new file
                    smallerImg.Save(fullSavePath, imageType);

                    // get rid of the new file from memory
                    smallerImg.Dispose();
                }
            }
            // return the new filename
            return mediaId + fileNameExtension;
        }

        public string SaveUploadedFilesToServer(String filePath, HttpPostedFile file, string strFileName)
        {
            // first thing to do is create a new filename root by using a guid and the 
            // current file extension.
            // i use the guid as the filename and as the unique media id in the database, 
            // but you don't have to do it this way
            //var mediaId = Guid.NewGuid();
            var fileNameExtension = Path.GetExtension(strFileName).ToLower();
            // get the file as an image
           // var uploadedImage = Image.FromStream(file.InputStream);
            // create our new filename using the media id, file extension and image 
            // dimensions.
            var fileName = strFileName;
            // now get the full server path and add on the filename
            var fullSavePath = filePath + "\\" + fileName;
            // we need to get the right ImageFormat so we don't try to save a jpg as a 
            // png, etc...
            var imageType = GetImageType(fileNameExtension);

            //uploadedImage.Save(fullSavePath, imageType);
            file.SaveAs(fullSavePath);            
            // then get rid of it from memory
            //uploadedImage.Dispose();
            // return the new filename
            return fileNameExtension;
        }

        private static ImageFormat GetImageType(string fileExt)
        {
            switch (fileExt)
            {
                case ".jpg":
                    return ImageFormat.Jpeg;
                case ".gif":
                    return ImageFormat.Gif;
                default: // (png)
                    return ImageFormat.Png;
            }
        }

        public static string CheckAndCreateDirectory(string directory)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            return directory;
        }        

        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            try
            {
                string strResp = "";
                bool respUpload = true;
                var File = HttpContext.Current.Request.Files;
                /*if (!uploadFiles())
                    respUpload = false;*/

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>showStepTres('" + respUpload + "');</script>", false);
            }
            catch (Exception ex)
            { }
        }

        protected void rblSiNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            string rblSiNoDeporte = rblSiNo.SelectedValue.ToString();
            if (rblSiNoDeporte == "1")
            {
                //divDeportes.Visible = true;
                dvNadar.Visible = true;
            }
            else
            {
                //divDeportes.Visible = false;
                dvNadar.Visible = false;
            }
        }

        protected void rblColaborador_SelectedIndexChanged(object sender, EventArgs e)
        {
            string rblSiNoColaborador = rblColaborador.SelectedValue.ToString();
            if (rblSiNoColaborador == "1")
                divInfoColaborador.Visible = true;
            else
                divInfoColaborador.Visible = false;
        }

        protected void ddlLocaciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
                <option value="141">Cancún - Playa Langosta</option>
			    <option value="142">Playa del Carmen - Parque Xcaret</option>
			    <option value="143">Tulum - Parque Xel-Há</option>
			    <option value="144">Puerto Morelos - CLub de playa Único</option>
			    <option value="145">Cozumel</option>
             */
            string strIdLocacion = ddlLocaciones.SelectedValue.ToString();            
            ddlGralCategoria.SelectedIndex = -1;
            switch (strIdLocacion)
            {
                case "142":
                    //ddlGralCategoria.Items.FindByText().Selected = true;
                    ddlGralCategoria.Items.FindByValue("62").Selected = true; //Travesia Sagrada Xcaret
                    break;
                case "143":
                    ddlGralCategoria.Items.FindByValue("63").Selected = true; //Travesia Sagrada Xelha
                    break;
                case "141":
                    ddlGralCategoria.Items.FindByValue("65").Selected = true; //Travesia Sagrada Cancun
                    break;
                case "144":
                    ddlGralCategoria.Items.FindByValue("66").Selected = true; //Travesia Sagrada Puerto Morelos
                    break;
                case "145":
                    ddlGralCategoria.Items.FindByValue("64").Selected = true; //Travesia Sagrada Cozumel
                    break;
            }
        }

        protected void rblSignificadoTsm_SelectedIndexChanged(object sender, EventArgs e)
        {
            string rblSignificadoTsmValue = rblSignificadoTsm.SelectedValue.ToString();
            if (rblSignificadoTsmValue == "1")
            {
                //divDeportes.Visible = true;
                divSignificadoTsm.Visible = true;
            }
            else
            {
                //divDeportes.Visible = false;
                divSignificadoTsm.Visible = false;
                txtSignificadoTSM.Text = "";
            }
        }

        public void MensajeAlert(string errMensaje)
        {

            wsGeneralServices.GeneralServicesClient iGeneral = new wsGeneralServices.GeneralServicesClient();
            iGeneral.Open();
            string strCorreos = "nfigueroa@experienciasxcaret.com.mx";
            bool blnBanderaCorreo = iGeneral.SendEmail(strCorreos, "Error en Aplicación Travesia", errMensaje, true /*es html*/, "", false, 0, "");
            iGeneral.Close();
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp2", "<script>alert('" + strCuerpoCorreo + "');", false);
        }

        public List<string> ftpUploadfiles(byte[] archivo, string nombrearchivo, string contenedor, bool cnCrearCarpeta, string sesion)
        {
            List<string> result = new List<string>();
            string strFinalContenedor = "/httpdocs/" + contenedor;
            wscConfiguracionAplicacion.IcConfiguracionAplicacionClient icConfiguracion = new wscConfiguracionAplicacion.IcConfiguracionAplicacionClient();
            icConfiguracion.Open();

            wscConfiguracionAplicacion.cConfiguracionAplicacion cConfFptMx = icConfiguracion.SeleccionarcConfiguracionAplicacionPorId("confFptMx", sesion);
            icConfiguracion.Close();
            string strResponseCreateFolder = "";

            List<ftpSetting> listPagosInfo = (new JavaScriptSerializer()).Deserialize<List<ftpSetting>>(cConfFptMx.dsValor);

            if (cnCrearCarpeta)
            {
                foreach (ftpSetting ftObject in listPagosInfo)
                {
                    WebRequest request = WebRequest.Create("ftp://" + ftObject.server.Trim() + strFinalContenedor);
                    request.Method = WebRequestMethods.Ftp.MakeDirectory;
                    request.Credentials = new NetworkCredential(ftObject.username, ftObject.password);
                    using (var resp = (FtpWebResponse)request.GetResponse())
                    {
                        strResponseCreateFolder = resp.StatusCode.ToString();
                    }
                }
            }

            string[] strIp;
            foreach (ftpSetting ftObject in listPagosInfo)
            {
                strIp = ftObject.server.Split('.');

                String uploadUrl = String.Format("{0}{1}/{2}", "ftp://" + ftObject.server.Trim(), strFinalContenedor, nombrearchivo);
                FtpWebRequest req = (FtpWebRequest)FtpWebRequest.Create(uploadUrl);
                req.Proxy = null;
                req.Method = WebRequestMethods.Ftp.UploadFile;
                req.Credentials = new NetworkCredential(ftObject.username, ftObject.password);
                req.UseBinary = true;
                req.UsePassive = true;
                byte[] data = archivo;
                req.ContentLength = data.Length;
                Stream stream = req.GetRequestStream();
                try
                {
                    stream.Write(data, 0, data.Length);
                    stream.Close();
                    FtpWebResponse res = (FtpWebResponse)req.GetResponse();
                    result.Add(strIp[strIp.Length - 1]);
                }
                catch (Exception ex)
                {
                    stream.Close();
                }
            }
            return result;
        }

        public struct ftpSetting
        {
            public string server { get; set; }
            public string username { get; set; }
            public string password { get; set; }
        }
    }
}